import { Directive, ElementRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { ValueAccessor } from './value-accessor';
import * as i0 from "@angular/core";
export class TextValueAccessor extends ValueAccessor {
    constructor(el) {
        super(el);
    }
}
TextValueAccessor.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "14.1.3", ngImport: i0, type: TextValueAccessor, deps: [{ token: i0.ElementRef }], target: i0.ɵɵFactoryTarget.Directive });
TextValueAccessor.ɵdir = i0.ɵɵngDeclareDirective({ minVersion: "14.0.0", version: "14.1.3", type: TextValueAccessor, selector: "digi-form-input, digi-form-input[afType=text], digi-form-input[afType=email], digi-form-input[afType=color], digi-form-input[afType=date], digi-form-input[afType=datetime-local], digi-form-input[afType=month], digi-form-input[afType=password], digi-form-input[afType=search], digi-form-input[afType=tel], digi-form-input[afType=time], digi-form-input[afType=url], digi-form-input[afType=week], digi-form-textarea, digi-form-input-search, digi-calendar", host: { listeners: { "afOnInput": "handleChangeEvent($event.target.value)", "afOnDateSelectedChange": "handleChangeEvent($event.target.afSelectedDate)" } }, providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: TextValueAccessor,
            multi: true
        }
    ], usesInheritance: true, ngImport: i0 });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "14.1.3", ngImport: i0, type: TextValueAccessor, decorators: [{
            type: Directive,
            args: [{
                    /* tslint:disable-next-line:directive-selector */
                    selector: 'digi-form-input, digi-form-input[afType=text], digi-form-input[afType=email], digi-form-input[afType=color], digi-form-input[afType=date], digi-form-input[afType=datetime-local], digi-form-input[afType=month], digi-form-input[afType=password], digi-form-input[afType=search], digi-form-input[afType=tel], digi-form-input[afType=time], digi-form-input[afType=url], digi-form-input[afType=week], digi-form-textarea, digi-form-input-search, digi-calendar',
                    host: {
                        '(afOnInput)': 'handleChangeEvent($event.target.value)',
                        '(afOnDateSelectedChange)': 'handleChangeEvent($event.target.afSelectedDate)'
                    },
                    providers: [
                        {
                            provide: NG_VALUE_ACCESSOR,
                            useExisting: TextValueAccessor,
                            multi: true
                        }
                    ]
                }]
        }], ctorParameters: function () { return [{ type: i0.ElementRef }]; } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGV4dC12YWx1ZS1hY2Nlc3Nvci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL2xpYnMvc2tvbHZlcmtldC9hbmd1bGFyL3NyYy9saWIvc3RlbmNpbC1nZW5lcmF0ZWQvdGV4dC12YWx1ZS1hY2Nlc3Nvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN0RCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUVuRCxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sa0JBQWtCLENBQUM7O0FBaUJqRCxNQUFNLE9BQU8saUJBQWtCLFNBQVEsYUFBYTtJQUNsRCxZQUFZLEVBQWM7UUFDeEIsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ1osQ0FBQzs7OEdBSFUsaUJBQWlCO2tHQUFqQixpQkFBaUIsMm5CQVJqQjtRQUNUO1lBQ0UsT0FBTyxFQUFFLGlCQUFpQjtZQUMxQixXQUFXLEVBQUUsaUJBQWlCO1lBQzlCLEtBQUssRUFBRSxJQUFJO1NBQ1o7S0FDRjsyRkFFVSxpQkFBaUI7a0JBZjdCLFNBQVM7bUJBQUM7b0JBQ1QsaURBQWlEO29CQUNqRCxRQUFRLEVBQUUscWNBQXFjO29CQUMvYyxJQUFJLEVBQUU7d0JBQ0osYUFBYSxFQUFFLHdDQUF3Qzt3QkFDdkQsMEJBQTBCLEVBQUUsaURBQWlEO3FCQUM5RTtvQkFDRCxTQUFTLEVBQUU7d0JBQ1Q7NEJBQ0UsT0FBTyxFQUFFLGlCQUFpQjs0QkFDMUIsV0FBVyxtQkFBbUI7NEJBQzlCLEtBQUssRUFBRSxJQUFJO3lCQUNaO3FCQUNGO2lCQUNGIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGlyZWN0aXZlLCBFbGVtZW50UmVmIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBOR19WQUxVRV9BQ0NFU1NPUiB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcblxuaW1wb3J0IHsgVmFsdWVBY2Nlc3NvciB9IGZyb20gJy4vdmFsdWUtYWNjZXNzb3InO1xuXG5ARGlyZWN0aXZlKHtcbiAgLyogdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOmRpcmVjdGl2ZS1zZWxlY3RvciAqL1xuICBzZWxlY3RvcjogJ2RpZ2ktZm9ybS1pbnB1dCwgZGlnaS1mb3JtLWlucHV0W2FmVHlwZT10ZXh0XSwgZGlnaS1mb3JtLWlucHV0W2FmVHlwZT1lbWFpbF0sIGRpZ2ktZm9ybS1pbnB1dFthZlR5cGU9Y29sb3JdLCBkaWdpLWZvcm0taW5wdXRbYWZUeXBlPWRhdGVdLCBkaWdpLWZvcm0taW5wdXRbYWZUeXBlPWRhdGV0aW1lLWxvY2FsXSwgZGlnaS1mb3JtLWlucHV0W2FmVHlwZT1tb250aF0sIGRpZ2ktZm9ybS1pbnB1dFthZlR5cGU9cGFzc3dvcmRdLCBkaWdpLWZvcm0taW5wdXRbYWZUeXBlPXNlYXJjaF0sIGRpZ2ktZm9ybS1pbnB1dFthZlR5cGU9dGVsXSwgZGlnaS1mb3JtLWlucHV0W2FmVHlwZT10aW1lXSwgZGlnaS1mb3JtLWlucHV0W2FmVHlwZT11cmxdLCBkaWdpLWZvcm0taW5wdXRbYWZUeXBlPXdlZWtdLCBkaWdpLWZvcm0tdGV4dGFyZWEsIGRpZ2ktZm9ybS1pbnB1dC1zZWFyY2gsIGRpZ2ktY2FsZW5kYXInLFxuICBob3N0OiB7XG4gICAgJyhhZk9uSW5wdXQpJzogJ2hhbmRsZUNoYW5nZUV2ZW50KCRldmVudC50YXJnZXQudmFsdWUpJyxcbiAgICAnKGFmT25EYXRlU2VsZWN0ZWRDaGFuZ2UpJzogJ2hhbmRsZUNoYW5nZUV2ZW50KCRldmVudC50YXJnZXQuYWZTZWxlY3RlZERhdGUpJ1xuICB9LFxuICBwcm92aWRlcnM6IFtcbiAgICB7XG4gICAgICBwcm92aWRlOiBOR19WQUxVRV9BQ0NFU1NPUixcbiAgICAgIHVzZUV4aXN0aW5nOiBUZXh0VmFsdWVBY2Nlc3NvcixcbiAgICAgIG11bHRpOiB0cnVlXG4gICAgfVxuICBdXG59KVxuZXhwb3J0IGNsYXNzIFRleHRWYWx1ZUFjY2Vzc29yIGV4dGVuZHMgVmFsdWVBY2Nlc3NvciB7XG4gIGNvbnN0cnVjdG9yKGVsOiBFbGVtZW50UmVmKSB7XG4gICAgc3VwZXIoZWwpO1xuICB9XG59XG4iXX0=