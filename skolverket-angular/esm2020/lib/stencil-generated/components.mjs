import { __decorate, __metadata } from "tslib";
/* tslint:disable */
/* auto-generated angular directive proxies */
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  NgZone,
} from "@angular/core";
import { ProxyCmp, proxyOutputs } from "./angular-component-lib/utils";
import { defineCustomElement as defineDigiButton } from "../../../../skolverket/components/digi-button.js";
import { defineCustomElement as defineDigiCalendar } from "../../../../skolverket/components/digi-calendar.js";
import { defineCustomElement as defineDigiCalendarWeekView } from "../../../../skolverket/components/digi-calendar-week-view.js";
import { defineCustomElement as defineDigiCardBox } from "../../../../skolverket/components/digi-card-box.js";
import { defineCustomElement as defineDigiCardLink } from "../../../../skolverket/components/digi-card-link.js";
import { defineCustomElement as defineDigiCode } from "../../../../skolverket/components/digi-code.js";
import { defineCustomElement as defineDigiCodeBlock } from "../../../../skolverket/components/digi-code-block.js";
import { defineCustomElement as defineDigiCodeExample } from "../../../../skolverket/components/digi-code-example.js";
import { defineCustomElement as defineDigiDialog } from "../../../../skolverket/components/digi-dialog.js";
import { defineCustomElement as defineDigiExpandableAccordion } from "../../../../skolverket/components/digi-expandable-accordion.js";
import { defineCustomElement as defineDigiFormCheckbox } from "../../../../skolverket/components/digi-form-checkbox.js";
import { defineCustomElement as defineDigiFormFieldset } from "../../../../skolverket/components/digi-form-fieldset.js";
import { defineCustomElement as defineDigiFormFileUpload } from "../../../../skolverket/components/digi-form-file-upload.js";
import { defineCustomElement as defineDigiFormFilter } from "../../../../skolverket/components/digi-form-filter.js";
import { defineCustomElement as defineDigiFormInput } from "../../../../skolverket/components/digi-form-input.js";
import { defineCustomElement as defineDigiFormInputSearch } from "../../../../skolverket/components/digi-form-input-search.js";
import { defineCustomElement as defineDigiFormLabel } from "../../../../skolverket/components/digi-form-label.js";
import { defineCustomElement as defineDigiFormProcessStep } from "../../../../skolverket/components/digi-form-process-step.js";
import { defineCustomElement as defineDigiFormProcessSteps } from "../../../../skolverket/components/digi-form-process-steps.js";
import { defineCustomElement as defineDigiFormRadiobutton } from "../../../../skolverket/components/digi-form-radiobutton.js";
import { defineCustomElement as defineDigiFormRadiogroup } from "../../../../skolverket/components/digi-form-radiogroup.js";
import { defineCustomElement as defineDigiFormSelect } from "../../../../skolverket/components/digi-form-select.js";
import { defineCustomElement as defineDigiFormTextarea } from "../../../../skolverket/components/digi-form-textarea.js";
import { defineCustomElement as defineDigiFormValidationMessage } from "../../../../skolverket/components/digi-form-validation-message.js";
import { defineCustomElement as defineDigiIcon } from "../../../../skolverket/components/digi-icon.js";
import { defineCustomElement as defineDigiIconBars } from "../../../../skolverket/components/digi-icon-bars.js";
import { defineCustomElement as defineDigiIconCheck } from "../../../../skolverket/components/digi-icon-check.js";
import { defineCustomElement as defineDigiIconCheckCircle } from "../../../../skolverket/components/digi-icon-check-circle.js";
import { defineCustomElement as defineDigiIconCheckCircleReg } from "../../../../skolverket/components/digi-icon-check-circle-reg.js";
import { defineCustomElement as defineDigiIconCheckCircleRegAlt } from "../../../../skolverket/components/digi-icon-check-circle-reg-alt.js";
import { defineCustomElement as defineDigiIconChevronDown } from "../../../../skolverket/components/digi-icon-chevron-down.js";
import { defineCustomElement as defineDigiIconChevronLeft } from "../../../../skolverket/components/digi-icon-chevron-left.js";
import { defineCustomElement as defineDigiIconChevronRight } from "../../../../skolverket/components/digi-icon-chevron-right.js";
import { defineCustomElement as defineDigiIconChevronUp } from "../../../../skolverket/components/digi-icon-chevron-up.js";
import { defineCustomElement as defineDigiIconCopy } from "../../../../skolverket/components/digi-icon-copy.js";
import { defineCustomElement as defineDigiIconDangerOutline } from "../../../../skolverket/components/digi-icon-danger-outline.js";
import { defineCustomElement as defineDigiIconDownload } from "../../../../skolverket/components/digi-icon-download.js";
import { defineCustomElement as defineDigiIconExclamationCircle } from "../../../../skolverket/components/digi-icon-exclamation-circle.js";
import { defineCustomElement as defineDigiIconExclamationCircleFilled } from "../../../../skolverket/components/digi-icon-exclamation-circle-filled.js";
import { defineCustomElement as defineDigiIconExclamationTriangle } from "../../../../skolverket/components/digi-icon-exclamation-triangle.js";
import { defineCustomElement as defineDigiIconExclamationTriangleWarning } from "../../../../skolverket/components/digi-icon-exclamation-triangle-warning.js";
import { defineCustomElement as defineDigiIconExternalLinkAlt } from "../../../../skolverket/components/digi-icon-external-link-alt.js";
import { defineCustomElement as defineDigiIconMinus } from "../../../../skolverket/components/digi-icon-minus.js";
import { defineCustomElement as defineDigiIconPaperclip } from "../../../../skolverket/components/digi-icon-paperclip.js";
import { defineCustomElement as defineDigiIconPlus } from "../../../../skolverket/components/digi-icon-plus.js";
import { defineCustomElement as defineDigiIconSearch } from "../../../../skolverket/components/digi-icon-search.js";
import { defineCustomElement as defineDigiIconSpinner } from "../../../../skolverket/components/digi-icon-spinner.js";
import { defineCustomElement as defineDigiIconTrash } from "../../../../skolverket/components/digi-icon-trash.js";
import { defineCustomElement as defineDigiIconX } from "../../../../skolverket/components/digi-icon-x.js";
import { defineCustomElement as defineDigiLayoutBlock } from "../../../../skolverket/components/digi-layout-block.js";
import { defineCustomElement as defineDigiLayoutColumns } from "../../../../skolverket/components/digi-layout-columns.js";
import { defineCustomElement as defineDigiLayoutContainer } from "../../../../skolverket/components/digi-layout-container.js";
import { defineCustomElement as defineDigiLayoutGrid } from "../../../../skolverket/components/digi-layout-grid.js";
import { defineCustomElement as defineDigiLayoutMediaObject } from "../../../../skolverket/components/digi-layout-media-object.js";
import { defineCustomElement as defineDigiLayoutRows } from "../../../../skolverket/components/digi-layout-rows.js";
import { defineCustomElement as defineDigiLayoutStackedBlocks } from "../../../../skolverket/components/digi-layout-stacked-blocks.js";
import { defineCustomElement as defineDigiLink } from "../../../../skolverket/components/digi-link.js";
import { defineCustomElement as defineDigiLinkExternal } from "../../../../skolverket/components/digi-link-external.js";
import { defineCustomElement as defineDigiLinkIcon } from "../../../../skolverket/components/digi-link-icon.js";
import { defineCustomElement as defineDigiLinkInternal } from "../../../../skolverket/components/digi-link-internal.js";
import { defineCustomElement as defineDigiListLink } from "../../../../skolverket/components/digi-list-link.js";
import { defineCustomElement as defineDigiLoaderSpinner } from "../../../../skolverket/components/digi-loader-spinner.js";
import { defineCustomElement as defineDigiLogo } from "../../../../skolverket/components/digi-logo.js";
import { defineCustomElement as defineDigiLogoService } from "../../../../skolverket/components/digi-logo-service.js";
import { defineCustomElement as defineDigiLogoSister } from "../../../../skolverket/components/digi-logo-sister.js";
import { defineCustomElement as defineDigiMediaFigure } from "../../../../skolverket/components/digi-media-figure.js";
import { defineCustomElement as defineDigiMediaImage } from "../../../../skolverket/components/digi-media-image.js";
import { defineCustomElement as defineDigiNavigationBreadcrumbs } from "../../../../skolverket/components/digi-navigation-breadcrumbs.js";
import { defineCustomElement as defineDigiNavigationContextMenu } from "../../../../skolverket/components/digi-navigation-context-menu.js";
import { defineCustomElement as defineDigiNavigationContextMenuItem } from "../../../../skolverket/components/digi-navigation-context-menu-item.js";
import { defineCustomElement as defineDigiNavigationMainMenu } from "../../../../skolverket/components/digi-navigation-main-menu.js";
import { defineCustomElement as defineDigiNavigationMainMenuPanel } from "../../../../skolverket/components/digi-navigation-main-menu-panel.js";
import { defineCustomElement as defineDigiNavigationPagination } from "../../../../skolverket/components/digi-navigation-pagination.js";
import { defineCustomElement as defineDigiNavigationSidebar } from "../../../../skolverket/components/digi-navigation-sidebar.js";
import { defineCustomElement as defineDigiNavigationSidebarButton } from "../../../../skolverket/components/digi-navigation-sidebar-button.js";
import { defineCustomElement as defineDigiNavigationTab } from "../../../../skolverket/components/digi-navigation-tab.js";
import { defineCustomElement as defineDigiNavigationTabInABox } from "../../../../skolverket/components/digi-navigation-tab-in-a-box.js";
import { defineCustomElement as defineDigiNavigationTabs } from "../../../../skolverket/components/digi-navigation-tabs.js";
import { defineCustomElement as defineDigiNavigationToc } from "../../../../skolverket/components/digi-navigation-toc.js";
import { defineCustomElement as defineDigiNavigationVerticalMenu } from "../../../../skolverket/components/digi-navigation-vertical-menu.js";
import { defineCustomElement as defineDigiNavigationVerticalMenuItem } from "../../../../skolverket/components/digi-navigation-vertical-menu-item.js";
import { defineCustomElement as defineDigiNotificationAlert } from "../../../../skolverket/components/digi-notification-alert.js";
import { defineCustomElement as defineDigiNotificationCookie } from "../../../../skolverket/components/digi-notification-cookie.js";
import { defineCustomElement as defineDigiNotificationDetail } from "../../../../skolverket/components/digi-notification-detail.js";
import { defineCustomElement as defineDigiPage } from "../../../../skolverket/components/digi-page.js";
import { defineCustomElement as defineDigiPageBlockCards } from "../../../../skolverket/components/digi-page-block-cards.js";
import { defineCustomElement as defineDigiPageBlockHero } from "../../../../skolverket/components/digi-page-block-hero.js";
import { defineCustomElement as defineDigiPageBlockLists } from "../../../../skolverket/components/digi-page-block-lists.js";
import { defineCustomElement as defineDigiPageBlockSidebar } from "../../../../skolverket/components/digi-page-block-sidebar.js";
import { defineCustomElement as defineDigiPageFooter } from "../../../../skolverket/components/digi-page-footer.js";
import { defineCustomElement as defineDigiPageHeader } from "../../../../skolverket/components/digi-page-header.js";
import { defineCustomElement as defineDigiProgressStep } from "../../../../skolverket/components/digi-progress-step.js";
import { defineCustomElement as defineDigiProgressSteps } from "../../../../skolverket/components/digi-progress-steps.js";
import { defineCustomElement as defineDigiProgressbar } from "../../../../skolverket/components/digi-progressbar.js";
import { defineCustomElement as defineDigiTable } from "../../../../skolverket/components/digi-table.js";
import { defineCustomElement as defineDigiTag } from "../../../../skolverket/components/digi-tag.js";
import { defineCustomElement as defineDigiTypography } from "../../../../skolverket/components/digi-typography.js";
import { defineCustomElement as defineDigiTypographyHeadingSection } from "../../../../skolverket/components/digi-typography-heading-section.js";
import { defineCustomElement as defineDigiTypographyMeta } from "../../../../skolverket/components/digi-typography-meta.js";
import { defineCustomElement as defineDigiTypographyPreamble } from "../../../../skolverket/components/digi-typography-preamble.js";
import { defineCustomElement as defineDigiTypographyTime } from "../../../../skolverket/components/digi-typography-time.js";
import { defineCustomElement as defineDigiUtilBreakpointObserver } from "../../../../skolverket/components/digi-util-breakpoint-observer.js";
import { defineCustomElement as defineDigiUtilDetectClickOutside } from "../../../../skolverket/components/digi-util-detect-click-outside.js";
import { defineCustomElement as defineDigiUtilDetectFocusOutside } from "../../../../skolverket/components/digi-util-detect-focus-outside.js";
import { defineCustomElement as defineDigiUtilIntersectionObserver } from "../../../../skolverket/components/digi-util-intersection-observer.js";
import { defineCustomElement as defineDigiUtilKeydownHandler } from "../../../../skolverket/components/digi-util-keydown-handler.js";
import { defineCustomElement as defineDigiUtilKeyupHandler } from "../../../../skolverket/components/digi-util-keyup-handler.js";
import { defineCustomElement as defineDigiUtilMutationObserver } from "../../../../skolverket/components/digi-util-mutation-observer.js";
import { defineCustomElement as defineDigiUtilResizeObserver } from "../../../../skolverket/components/digi-util-resize-observer.js";
import * as i0 from "@angular/core";
let DigiButton = class DigiButton {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnClick", "afOnFocus", "afOnBlur"]);
  }
};
DigiButton.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiButton,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiButton.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiButton,
  selector: "digi-button",
  inputs: {
    afAriaControls: "afAriaControls",
    afAriaExpanded: "afAriaExpanded",
    afAriaHaspopup: "afAriaHaspopup",
    afAriaLabel: "afAriaLabel",
    afAriaLabelledby: "afAriaLabelledby",
    afAriaPressed: "afAriaPressed",
    afAutofocus: "afAutofocus",
    afDir: "afDir",
    afForm: "afForm",
    afFullWidth: "afFullWidth",
    afId: "afId",
    afLang: "afLang",
    afSize: "afSize",
    afTabindex: "afTabindex",
    afType: "afType",
    afVariation: "afVariation",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiButton = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiButton,
      inputs: [
        "afAriaControls",
        "afAriaExpanded",
        "afAriaHaspopup",
        "afAriaLabel",
        "afAriaLabelledby",
        "afAriaPressed",
        "afAutofocus",
        "afDir",
        "afForm",
        "afFullWidth",
        "afId",
        "afLang",
        "afSize",
        "afTabindex",
        "afType",
        "afVariation",
      ],
      methods: ["afMGetButtonElement"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiButton
);
export { DigiButton };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiButton,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-button",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afAriaControls",
            "afAriaExpanded",
            "afAriaHaspopup",
            "afAriaLabel",
            "afAriaLabelledby",
            "afAriaPressed",
            "afAutofocus",
            "afDir",
            "afForm",
            "afFullWidth",
            "afId",
            "afLang",
            "afSize",
            "afTabindex",
            "afType",
            "afVariation",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiCalendar = class DigiCalendar {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, [
      "afOnDateSelectedChange",
      "afOnFocusOutside",
      "afOnClickOutside",
      "afOnDirty",
    ]);
  }
};
DigiCalendar.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiCalendar,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiCalendar.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiCalendar,
  selector: "digi-calendar",
  inputs: {
    afActive: "afActive",
    afId: "afId",
    afInitSelectedMonth: "afInitSelectedMonth",
    afMultipleDates: "afMultipleDates",
    afSelectedDate: "afSelectedDate",
    afShowWeekNumber: "afShowWeekNumber",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiCalendar = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiCalendar,
      inputs: [
        "afActive",
        "afId",
        "afInitSelectedMonth",
        "afMultipleDates",
        "afSelectedDate",
        "afShowWeekNumber",
      ],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiCalendar
);
export { DigiCalendar };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiCalendar,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-calendar",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afActive",
            "afId",
            "afInitSelectedMonth",
            "afMultipleDates",
            "afSelectedDate",
            "afShowWeekNumber",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiCalendarWeekView = class DigiCalendarWeekView {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnWeekChange", "afOnDateChange"]);
  }
};
DigiCalendarWeekView.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiCalendarWeekView,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiCalendarWeekView.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiCalendarWeekView,
  selector: "digi-calendar-week-view",
  inputs: {
    afDates: "afDates",
    afHeadingLevel: "afHeadingLevel",
    afId: "afId",
    afMaxWeek: "afMaxWeek",
    afMinWeek: "afMinWeek",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiCalendarWeekView = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiCalendarWeekView,
      inputs: ["afDates", "afHeadingLevel", "afId", "afMaxWeek", "afMinWeek"],
      methods: ["afMSetActiveDate"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiCalendarWeekView
);
export { DigiCalendarWeekView };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiCalendarWeekView,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-calendar-week-view",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afDates",
            "afHeadingLevel",
            "afId",
            "afMaxWeek",
            "afMinWeek",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiCardBox = class DigiCardBox {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiCardBox.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiCardBox,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiCardBox.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiCardBox,
  selector: "digi-card-box",
  inputs: { afGutter: "afGutter", afWidth: "afWidth" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiCardBox = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiCardBox,
      inputs: ["afGutter", "afWidth"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiCardBox
);
export { DigiCardBox };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiCardBox,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-card-box",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afGutter", "afWidth"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiCardLink = class DigiCardLink {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiCardLink.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiCardLink,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiCardLink.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiCardLink,
  selector: "digi-card-link",
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiCardLink = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiCardLink,
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiCardLink
);
export { DigiCardLink };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiCardLink,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-card-link",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiCode = class DigiCode {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiCode.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiCode,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiCode.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiCode,
  selector: "digi-code",
  inputs: {
    afCode: "afCode",
    afLang: "afLang",
    afLanguage: "afLanguage",
    afVariation: "afVariation",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiCode = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiCode,
      inputs: ["afCode", "afLang", "afLanguage", "afVariation"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiCode
);
export { DigiCode };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiCode,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-code",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afCode", "afLang", "afLanguage", "afVariation"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiCodeBlock = class DigiCodeBlock {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiCodeBlock.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiCodeBlock,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiCodeBlock.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiCodeBlock,
  selector: "digi-code-block",
  inputs: {
    afCode: "afCode",
    afHideToolbar: "afHideToolbar",
    afLang: "afLang",
    afLanguage: "afLanguage",
    afVariation: "afVariation",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiCodeBlock = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiCodeBlock,
      inputs: [
        "afCode",
        "afHideToolbar",
        "afLang",
        "afLanguage",
        "afVariation",
      ],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiCodeBlock
);
export { DigiCodeBlock };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiCodeBlock,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-code-block",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afCode",
            "afHideToolbar",
            "afLang",
            "afLanguage",
            "afVariation",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiCodeExample = class DigiCodeExample {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiCodeExample.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiCodeExample,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiCodeExample.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiCodeExample,
  selector: "digi-code-example",
  inputs: {
    afCode: "afCode",
    afCodeBlockVariation: "afCodeBlockVariation",
    afExampleVariation: "afExampleVariation",
    afHideCode: "afHideCode",
    afHideControls: "afHideControls",
    afLanguage: "afLanguage",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiCodeExample = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiCodeExample,
      inputs: [
        "afCode",
        "afCodeBlockVariation",
        "afExampleVariation",
        "afHideCode",
        "afHideControls",
        "afLanguage",
      ],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiCodeExample
);
export { DigiCodeExample };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiCodeExample,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-code-example",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afCode",
            "afCodeBlockVariation",
            "afExampleVariation",
            "afHideCode",
            "afHideControls",
            "afLanguage",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiDialog = class DigiDialog {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnOpen", "afOnClose"]);
  }
};
DigiDialog.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiDialog,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiDialog.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiDialog,
  selector: "digi-dialog",
  inputs: { afHideCloseButton: "afHideCloseButton", afOpen: "afOpen" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiDialog = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiDialog,
      inputs: ["afHideCloseButton", "afOpen"],
      methods: ["showModal", "close"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiDialog
);
export { DigiDialog };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiDialog,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-dialog",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afHideCloseButton", "afOpen"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiExpandableAccordion = class DigiExpandableAccordion {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnClick"]);
  }
};
DigiExpandableAccordion.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiExpandableAccordion,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiExpandableAccordion.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiExpandableAccordion,
  selector: "digi-expandable-accordion",
  inputs: {
    afAnimation: "afAnimation",
    afExpanded: "afExpanded",
    afHeading: "afHeading",
    afHeadingLevel: "afHeadingLevel",
    afId: "afId",
    afVariation: "afVariation",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiExpandableAccordion = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiExpandableAccordion,
      inputs: [
        "afAnimation",
        "afExpanded",
        "afHeading",
        "afHeadingLevel",
        "afId",
        "afVariation",
      ],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiExpandableAccordion
);
export { DigiExpandableAccordion };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiExpandableAccordion,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-expandable-accordion",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afAnimation",
            "afExpanded",
            "afHeading",
            "afHeadingLevel",
            "afId",
            "afVariation",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiFormCheckbox = class DigiFormCheckbox {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, [
      "afOnChange",
      "afOnBlur",
      "afOnFocus",
      "afOnFocusout",
      "afOnInput",
      "afOnDirty",
      "afOnTouched",
    ]);
  }
};
DigiFormCheckbox.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormCheckbox,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiFormCheckbox.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiFormCheckbox,
  selector: "digi-form-checkbox",
  inputs: {
    afAriaDescribedby: "afAriaDescribedby",
    afAriaLabel: "afAriaLabel",
    afAriaLabelledby: "afAriaLabelledby",
    afAutofocus: "afAutofocus",
    afChecked: "afChecked",
    afDescription: "afDescription",
    afId: "afId",
    afIndeterminate: "afIndeterminate",
    afLabel: "afLabel",
    afLayout: "afLayout",
    afName: "afName",
    afRequired: "afRequired",
    afValidation: "afValidation",
    afValue: "afValue",
    afVariation: "afVariation",
    checked: "checked",
    value: "value",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiFormCheckbox = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiFormCheckbox,
      inputs: [
        "afAriaDescribedby",
        "afAriaLabel",
        "afAriaLabelledby",
        "afAutofocus",
        "afChecked",
        "afDescription",
        "afId",
        "afIndeterminate",
        "afLabel",
        "afLayout",
        "afName",
        "afRequired",
        "afValidation",
        "afValue",
        "afVariation",
        "checked",
        "value",
      ],
      methods: ["afMGetFormControlElement"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiFormCheckbox
);
export { DigiFormCheckbox };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormCheckbox,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-form-checkbox",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afAriaDescribedby",
            "afAriaLabel",
            "afAriaLabelledby",
            "afAutofocus",
            "afChecked",
            "afDescription",
            "afId",
            "afIndeterminate",
            "afLabel",
            "afLayout",
            "afName",
            "afRequired",
            "afValidation",
            "afValue",
            "afVariation",
            "checked",
            "value",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiFormFieldset = class DigiFormFieldset {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiFormFieldset.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormFieldset,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiFormFieldset.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiFormFieldset,
  selector: "digi-form-fieldset",
  inputs: {
    afForm: "afForm",
    afId: "afId",
    afLegend: "afLegend",
    afName: "afName",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiFormFieldset = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiFormFieldset,
      inputs: ["afForm", "afId", "afLegend", "afName"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiFormFieldset
);
export { DigiFormFieldset };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormFieldset,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-form-fieldset",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afForm", "afId", "afLegend", "afName"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiFormFileUpload = class DigiFormFileUpload {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, [
      "afOnUploadFile",
      "afOnRemoveFile",
      "afOnCancelFile",
      "afOnRetryFile",
    ]);
  }
};
DigiFormFileUpload.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormFileUpload,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiFormFileUpload.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiFormFileUpload,
  selector: "digi-form-file-upload",
  inputs: {
    afAnnounceIfOptional: "afAnnounceIfOptional",
    afAnnounceIfOptionalText: "afAnnounceIfOptionalText",
    afFileMaxSize: "afFileMaxSize",
    afFileTypes: "afFileTypes",
    afHeadingFiles: "afHeadingFiles",
    afHeadingLevel: "afHeadingLevel",
    afId: "afId",
    afLabel: "afLabel",
    afLabelDescription: "afLabelDescription",
    afMaxFiles: "afMaxFiles",
    afName: "afName",
    afRequired: "afRequired",
    afRequiredText: "afRequiredText",
    afUploadBtnText: "afUploadBtnText",
    afValidation: "afValidation",
    afVariation: "afVariation",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiFormFileUpload = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiFormFileUpload,
      inputs: [
        "afAnnounceIfOptional",
        "afAnnounceIfOptionalText",
        "afFileMaxSize",
        "afFileTypes",
        "afHeadingFiles",
        "afHeadingLevel",
        "afId",
        "afLabel",
        "afLabelDescription",
        "afMaxFiles",
        "afName",
        "afRequired",
        "afRequiredText",
        "afUploadBtnText",
        "afValidation",
        "afVariation",
      ],
      methods: [
        "afMValidateFile",
        "afMGetAllFiles",
        "afMImportFiles",
        "afMGetFormControlElement",
      ],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiFormFileUpload
);
export { DigiFormFileUpload };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormFileUpload,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-form-file-upload",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afAnnounceIfOptional",
            "afAnnounceIfOptionalText",
            "afFileMaxSize",
            "afFileTypes",
            "afHeadingFiles",
            "afHeadingLevel",
            "afId",
            "afLabel",
            "afLabelDescription",
            "afMaxFiles",
            "afName",
            "afRequired",
            "afRequiredText",
            "afUploadBtnText",
            "afValidation",
            "afVariation",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiFormFilter = class DigiFormFilter {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, [
      "afOnFocusout",
      "afOnSubmitFilters",
      "afOnFilterClosed",
      "afOnResetFilters",
      "afOnChange",
    ]);
  }
};
DigiFormFilter.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormFilter,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiFormFilter.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiFormFilter,
  selector: "digi-form-filter",
  inputs: {
    afAlignRight: "afAlignRight",
    afAutofocus: "afAutofocus",
    afFilterButtonAriaDescribedby: "afFilterButtonAriaDescribedby",
    afFilterButtonAriaLabel: "afFilterButtonAriaLabel",
    afFilterButtonText: "afFilterButtonText",
    afHideResetButton: "afHideResetButton",
    afId: "afId",
    afName: "afName",
    afResetButtonText: "afResetButtonText",
    afResetButtonTextAriaLabel: "afResetButtonTextAriaLabel",
    afSubmitButtonText: "afSubmitButtonText",
    afSubmitButtonTextAriaLabel: "afSubmitButtonTextAriaLabel",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiFormFilter = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiFormFilter,
      inputs: [
        "afAlignRight",
        "afAutofocus",
        "afFilterButtonAriaDescribedby",
        "afFilterButtonAriaLabel",
        "afFilterButtonText",
        "afHideResetButton",
        "afId",
        "afName",
        "afResetButtonText",
        "afResetButtonTextAriaLabel",
        "afSubmitButtonText",
        "afSubmitButtonTextAriaLabel",
      ],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiFormFilter
);
export { DigiFormFilter };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormFilter,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-form-filter",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afAlignRight",
            "afAutofocus",
            "afFilterButtonAriaDescribedby",
            "afFilterButtonAriaLabel",
            "afFilterButtonText",
            "afHideResetButton",
            "afId",
            "afName",
            "afResetButtonText",
            "afResetButtonTextAriaLabel",
            "afSubmitButtonText",
            "afSubmitButtonTextAriaLabel",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiFormInput = class DigiFormInput {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, [
      "afOnChange",
      "afOnBlur",
      "afOnKeyup",
      "afOnFocus",
      "afOnFocusout",
      "afOnInput",
      "afOnDirty",
      "afOnTouched",
    ]);
  }
};
DigiFormInput.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormInput,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiFormInput.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiFormInput,
  selector: "digi-form-input",
  inputs: {
    afAnnounceIfOptional: "afAnnounceIfOptional",
    afAnnounceIfOptionalText: "afAnnounceIfOptionalText",
    afAriaActivedescendant: "afAriaActivedescendant",
    afAriaAutocomplete: "afAriaAutocomplete",
    afAriaDescribedby: "afAriaDescribedby",
    afAriaLabelledby: "afAriaLabelledby",
    afAutocomplete: "afAutocomplete",
    afAutofocus: "afAutofocus",
    afButtonVariation: "afButtonVariation",
    afDirname: "afDirname",
    afId: "afId",
    afLabel: "afLabel",
    afLabelDescription: "afLabelDescription",
    afList: "afList",
    afMax: "afMax",
    afMaxlength: "afMaxlength",
    afMin: "afMin",
    afMinlength: "afMinlength",
    afName: "afName",
    afRequired: "afRequired",
    afRequiredText: "afRequiredText",
    afRole: "afRole",
    afStep: "afStep",
    afType: "afType",
    afValidation: "afValidation",
    afValidationText: "afValidationText",
    afValue: "afValue",
    afVariation: "afVariation",
    value: "value",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiFormInput = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiFormInput,
      inputs: [
        "afAnnounceIfOptional",
        "afAnnounceIfOptionalText",
        "afAriaActivedescendant",
        "afAriaAutocomplete",
        "afAriaDescribedby",
        "afAriaLabelledby",
        "afAutocomplete",
        "afAutofocus",
        "afButtonVariation",
        "afDirname",
        "afId",
        "afLabel",
        "afLabelDescription",
        "afList",
        "afMax",
        "afMaxlength",
        "afMin",
        "afMinlength",
        "afName",
        "afRequired",
        "afRequiredText",
        "afRole",
        "afStep",
        "afType",
        "afValidation",
        "afValidationText",
        "afValue",
        "afVariation",
        "value",
      ],
      methods: ["afMGetFormControlElement"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiFormInput
);
export { DigiFormInput };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormInput,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-form-input",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afAnnounceIfOptional",
            "afAnnounceIfOptionalText",
            "afAriaActivedescendant",
            "afAriaAutocomplete",
            "afAriaDescribedby",
            "afAriaLabelledby",
            "afAutocomplete",
            "afAutofocus",
            "afButtonVariation",
            "afDirname",
            "afId",
            "afLabel",
            "afLabelDescription",
            "afList",
            "afMax",
            "afMaxlength",
            "afMin",
            "afMinlength",
            "afName",
            "afRequired",
            "afRequiredText",
            "afRole",
            "afStep",
            "afType",
            "afValidation",
            "afValidationText",
            "afValue",
            "afVariation",
            "value",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiFormInputSearch = class DigiFormInputSearch {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, [
      "afOnFocusOutside",
      "afOnFocusInside",
      "afOnClickOutside",
      "afOnClickInside",
      "afOnChange",
      "afOnBlur",
      "afOnKeyup",
      "afOnFocus",
      "afOnFocusout",
      "afOnInput",
      "afOnClick",
    ]);
  }
};
DigiFormInputSearch.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormInputSearch,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiFormInputSearch.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiFormInputSearch,
  selector: "digi-form-input-search",
  inputs: {
    afAriaActivedescendant: "afAriaActivedescendant",
    afAriaAutocomplete: "afAriaAutocomplete",
    afAriaDescribedby: "afAriaDescribedby",
    afAriaLabelledby: "afAriaLabelledby",
    afAutocomplete: "afAutocomplete",
    afAutofocus: "afAutofocus",
    afButtonAriaLabel: "afButtonAriaLabel",
    afButtonAriaLabelledby: "afButtonAriaLabelledby",
    afButtonText: "afButtonText",
    afButtonType: "afButtonType",
    afButtonVariation: "afButtonVariation",
    afHideButton: "afHideButton",
    afId: "afId",
    afLabel: "afLabel",
    afLabelDescription: "afLabelDescription",
    afName: "afName",
    afType: "afType",
    afValue: "afValue",
    afVariation: "afVariation",
    value: "value",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiFormInputSearch = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiFormInputSearch,
      inputs: [
        "afAriaActivedescendant",
        "afAriaAutocomplete",
        "afAriaDescribedby",
        "afAriaLabelledby",
        "afAutocomplete",
        "afAutofocus",
        "afButtonAriaLabel",
        "afButtonAriaLabelledby",
        "afButtonText",
        "afButtonType",
        "afButtonVariation",
        "afHideButton",
        "afId",
        "afLabel",
        "afLabelDescription",
        "afName",
        "afType",
        "afValue",
        "afVariation",
        "value",
      ],
      methods: ["afMGetFormControlElement"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiFormInputSearch
);
export { DigiFormInputSearch };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormInputSearch,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-form-input-search",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afAriaActivedescendant",
            "afAriaAutocomplete",
            "afAriaDescribedby",
            "afAriaLabelledby",
            "afAutocomplete",
            "afAutofocus",
            "afButtonAriaLabel",
            "afButtonAriaLabelledby",
            "afButtonText",
            "afButtonType",
            "afButtonVariation",
            "afHideButton",
            "afId",
            "afLabel",
            "afLabelDescription",
            "afName",
            "afType",
            "afValue",
            "afVariation",
            "value",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiFormLabel = class DigiFormLabel {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiFormLabel.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormLabel,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiFormLabel.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiFormLabel,
  selector: "digi-form-label",
  inputs: {
    afAnnounceIfOptional: "afAnnounceIfOptional",
    afAnnounceIfOptionalText: "afAnnounceIfOptionalText",
    afDescription: "afDescription",
    afFor: "afFor",
    afId: "afId",
    afLabel: "afLabel",
    afRequired: "afRequired",
    afRequiredText: "afRequiredText",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiFormLabel = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiFormLabel,
      inputs: [
        "afAnnounceIfOptional",
        "afAnnounceIfOptionalText",
        "afDescription",
        "afFor",
        "afId",
        "afLabel",
        "afRequired",
        "afRequiredText",
      ],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiFormLabel
);
export { DigiFormLabel };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormLabel,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-form-label",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afAnnounceIfOptional",
            "afAnnounceIfOptionalText",
            "afDescription",
            "afFor",
            "afId",
            "afLabel",
            "afRequired",
            "afRequiredText",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiFormProcessStep = class DigiFormProcessStep {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afClick"]);
  }
};
DigiFormProcessStep.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormProcessStep,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiFormProcessStep.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiFormProcessStep,
  selector: "digi-form-process-step",
  inputs: {
    afContext: "afContext",
    afHref: "afHref",
    afLabel: "afLabel",
    afType: "afType",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiFormProcessStep = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiFormProcessStep,
      inputs: ["afContext", "afHref", "afLabel", "afType"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiFormProcessStep
);
export { DigiFormProcessStep };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormProcessStep,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-form-process-step",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afContext", "afHref", "afLabel", "afType"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiFormProcessSteps = class DigiFormProcessSteps {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiFormProcessSteps.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormProcessSteps,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiFormProcessSteps.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiFormProcessSteps,
  selector: "digi-form-process-steps",
  inputs: { afCurrentStep: "afCurrentStep", afId: "afId" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiFormProcessSteps = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiFormProcessSteps,
      inputs: ["afCurrentStep", "afId"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiFormProcessSteps
);
export { DigiFormProcessSteps };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormProcessSteps,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-form-process-steps",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afCurrentStep", "afId"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiFormRadiobutton = class DigiFormRadiobutton {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, [
      "afOnChange",
      "afOnBlur",
      "afOnFocus",
      "afOnFocusout",
      "afOnInput",
      "afOnDirty",
      "afOnTouched",
    ]);
  }
};
DigiFormRadiobutton.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormRadiobutton,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiFormRadiobutton.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiFormRadiobutton,
  selector: "digi-form-radiobutton",
  inputs: {
    afAriaDescribedby: "afAriaDescribedby",
    afAriaLabelledby: "afAriaLabelledby",
    afAutofocus: "afAutofocus",
    afChecked: "afChecked",
    afId: "afId",
    afLabel: "afLabel",
    afLayout: "afLayout",
    afName: "afName",
    afRequired: "afRequired",
    afValidation: "afValidation",
    afValue: "afValue",
    afVariation: "afVariation",
    checked: "checked",
    value: "value",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiFormRadiobutton = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiFormRadiobutton,
      inputs: [
        "afAriaDescribedby",
        "afAriaLabelledby",
        "afAutofocus",
        "afChecked",
        "afId",
        "afLabel",
        "afLayout",
        "afName",
        "afRequired",
        "afValidation",
        "afValue",
        "afVariation",
        "checked",
        "value",
      ],
      methods: ["afMGetFormControlElement"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiFormRadiobutton
);
export { DigiFormRadiobutton };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormRadiobutton,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-form-radiobutton",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afAriaDescribedby",
            "afAriaLabelledby",
            "afAutofocus",
            "afChecked",
            "afId",
            "afLabel",
            "afLayout",
            "afName",
            "afRequired",
            "afValidation",
            "afValue",
            "afVariation",
            "checked",
            "value",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiFormRadiogroup = class DigiFormRadiogroup {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnGroupChange"]);
  }
};
DigiFormRadiogroup.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormRadiogroup,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiFormRadiogroup.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiFormRadiogroup,
  selector: "digi-form-radiogroup",
  inputs: { afName: "afName", afValue: "afValue", value: "value" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiFormRadiogroup = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiFormRadiogroup,
      inputs: ["afName", "afValue", "value"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiFormRadiogroup
);
export { DigiFormRadiogroup };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormRadiogroup,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-form-radiogroup",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afName", "afValue", "value"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiFormSelect = class DigiFormSelect {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, [
      "afOnChange",
      "afOnFocus",
      "afOnFocusout",
      "afOnBlur",
      "afOnDirty",
    ]);
  }
};
DigiFormSelect.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormSelect,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiFormSelect.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiFormSelect,
  selector: "digi-form-select",
  inputs: {
    afAnnounceIfOptional: "afAnnounceIfOptional",
    afAnnounceIfOptionalText: "afAnnounceIfOptionalText",
    afAutofocus: "afAutofocus",
    afDescription: "afDescription",
    afDisableValidation: "afDisableValidation",
    afId: "afId",
    afLabel: "afLabel",
    afName: "afName",
    afPlaceholder: "afPlaceholder",
    afRequired: "afRequired",
    afRequiredText: "afRequiredText",
    afStartSelected: "afStartSelected",
    afValidation: "afValidation",
    afValidationText: "afValidationText",
    afValue: "afValue",
    afVariation: "afVariation",
    value: "value",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiFormSelect = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiFormSelect,
      inputs: [
        "afAnnounceIfOptional",
        "afAnnounceIfOptionalText",
        "afAutofocus",
        "afDescription",
        "afDisableValidation",
        "afId",
        "afLabel",
        "afName",
        "afPlaceholder",
        "afRequired",
        "afRequiredText",
        "afStartSelected",
        "afValidation",
        "afValidationText",
        "afValue",
        "afVariation",
        "value",
      ],
      methods: ["afMGetFormControlElement"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiFormSelect
);
export { DigiFormSelect };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormSelect,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-form-select",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afAnnounceIfOptional",
            "afAnnounceIfOptionalText",
            "afAutofocus",
            "afDescription",
            "afDisableValidation",
            "afId",
            "afLabel",
            "afName",
            "afPlaceholder",
            "afRequired",
            "afRequiredText",
            "afStartSelected",
            "afValidation",
            "afValidationText",
            "afValue",
            "afVariation",
            "value",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiFormTextarea = class DigiFormTextarea {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, [
      "afOnChange",
      "afOnBlur",
      "afOnKeyup",
      "afOnFocus",
      "afOnFocusout",
      "afOnInput",
      "afOnDirty",
      "afOnTouched",
    ]);
  }
};
DigiFormTextarea.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormTextarea,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiFormTextarea.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiFormTextarea,
  selector: "digi-form-textarea",
  inputs: {
    afAnnounceIfOptional: "afAnnounceIfOptional",
    afAnnounceIfOptionalText: "afAnnounceIfOptionalText",
    afAriaActivedescendant: "afAriaActivedescendant",
    afAriaDescribedby: "afAriaDescribedby",
    afAriaLabelledby: "afAriaLabelledby",
    afAutofocus: "afAutofocus",
    afId: "afId",
    afLabel: "afLabel",
    afLabelDescription: "afLabelDescription",
    afMaxlength: "afMaxlength",
    afMinlength: "afMinlength",
    afName: "afName",
    afRequired: "afRequired",
    afRequiredText: "afRequiredText",
    afRole: "afRole",
    afValidation: "afValidation",
    afValidationText: "afValidationText",
    afValue: "afValue",
    afVariation: "afVariation",
    value: "value",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiFormTextarea = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiFormTextarea,
      inputs: [
        "afAnnounceIfOptional",
        "afAnnounceIfOptionalText",
        "afAriaActivedescendant",
        "afAriaDescribedby",
        "afAriaLabelledby",
        "afAutofocus",
        "afId",
        "afLabel",
        "afLabelDescription",
        "afMaxlength",
        "afMinlength",
        "afName",
        "afRequired",
        "afRequiredText",
        "afRole",
        "afValidation",
        "afValidationText",
        "afValue",
        "afVariation",
        "value",
      ],
      methods: ["afMGetFormControlElement"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiFormTextarea
);
export { DigiFormTextarea };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormTextarea,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-form-textarea",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afAnnounceIfOptional",
            "afAnnounceIfOptionalText",
            "afAriaActivedescendant",
            "afAriaDescribedby",
            "afAriaLabelledby",
            "afAutofocus",
            "afId",
            "afLabel",
            "afLabelDescription",
            "afMaxlength",
            "afMinlength",
            "afName",
            "afRequired",
            "afRequiredText",
            "afRole",
            "afValidation",
            "afValidationText",
            "afValue",
            "afVariation",
            "value",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiFormValidationMessage = class DigiFormValidationMessage {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiFormValidationMessage.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormValidationMessage,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiFormValidationMessage.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiFormValidationMessage,
  selector: "digi-form-validation-message",
  inputs: { afVariation: "afVariation" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiFormValidationMessage = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiFormValidationMessage,
      inputs: ["afVariation"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiFormValidationMessage
);
export { DigiFormValidationMessage };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormValidationMessage,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-form-validation-message",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afVariation"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIcon = class DigiIcon {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIcon.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIcon,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIcon.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIcon,
  selector: "digi-icon",
  inputs: {
    afDesc: "afDesc",
    afName: "afName",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIcon = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiIcon,
      inputs: ["afDesc", "afName", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIcon
);
export { DigiIcon };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIcon,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afName", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconBars = class DigiIconBars {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconBars.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconBars,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconBars.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconBars,
  selector: "digi-icon-bars",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconBars = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiIconBars,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconBars
);
export { DigiIconBars };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconBars,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-bars",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconCheck = class DigiIconCheck {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconCheck.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconCheck,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconCheck.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconCheck,
  selector: "digi-icon-check",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconCheck = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiIconCheck,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconCheck
);
export { DigiIconCheck };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconCheck,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-check",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconCheckCircle = class DigiIconCheckCircle {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconCheckCircle.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconCheckCircle,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconCheckCircle.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconCheckCircle,
  selector: "digi-icon-check-circle",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconCheckCircle = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiIconCheckCircle,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconCheckCircle
);
export { DigiIconCheckCircle };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconCheckCircle,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-check-circle",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconCheckCircleReg = class DigiIconCheckCircleReg {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconCheckCircleReg.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconCheckCircleReg,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconCheckCircleReg.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconCheckCircleReg,
  selector: "digi-icon-check-circle-reg",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconCheckCircleReg = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiIconCheckCircleReg,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconCheckCircleReg
);
export { DigiIconCheckCircleReg };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconCheckCircleReg,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-check-circle-reg",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconCheckCircleRegAlt = class DigiIconCheckCircleRegAlt {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconCheckCircleRegAlt.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconCheckCircleRegAlt,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconCheckCircleRegAlt.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconCheckCircleRegAlt,
  selector: "digi-icon-check-circle-reg-alt",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconCheckCircleRegAlt = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiIconCheckCircleRegAlt,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconCheckCircleRegAlt
);
export { DigiIconCheckCircleRegAlt };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconCheckCircleRegAlt,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-check-circle-reg-alt",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconChevronDown = class DigiIconChevronDown {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconChevronDown.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconChevronDown,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconChevronDown.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconChevronDown,
  selector: "digi-icon-chevron-down",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconChevronDown = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiIconChevronDown,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconChevronDown
);
export { DigiIconChevronDown };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconChevronDown,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-chevron-down",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconChevronLeft = class DigiIconChevronLeft {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconChevronLeft.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconChevronLeft,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconChevronLeft.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconChevronLeft,
  selector: "digi-icon-chevron-left",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconChevronLeft = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiIconChevronLeft,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconChevronLeft
);
export { DigiIconChevronLeft };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconChevronLeft,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-chevron-left",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconChevronRight = class DigiIconChevronRight {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconChevronRight.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconChevronRight,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconChevronRight.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconChevronRight,
  selector: "digi-icon-chevron-right",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconChevronRight = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiIconChevronRight,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconChevronRight
);
export { DigiIconChevronRight };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconChevronRight,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-chevron-right",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconChevronUp = class DigiIconChevronUp {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconChevronUp.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconChevronUp,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconChevronUp.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconChevronUp,
  selector: "digi-icon-chevron-up",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconChevronUp = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiIconChevronUp,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconChevronUp
);
export { DigiIconChevronUp };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconChevronUp,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-chevron-up",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconCopy = class DigiIconCopy {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconCopy.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconCopy,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconCopy.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconCopy,
  selector: "digi-icon-copy",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconCopy = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiIconCopy,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconCopy
);
export { DigiIconCopy };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconCopy,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-copy",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconDangerOutline = class DigiIconDangerOutline {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconDangerOutline.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconDangerOutline,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconDangerOutline.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconDangerOutline,
  selector: "digi-icon-danger-outline",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconDangerOutline = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiIconDangerOutline,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconDangerOutline
);
export { DigiIconDangerOutline };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconDangerOutline,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-danger-outline",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconDownload = class DigiIconDownload {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconDownload.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconDownload,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconDownload.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconDownload,
  selector: "digi-icon-download",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconDownload = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiIconDownload,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconDownload
);
export { DigiIconDownload };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconDownload,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-download",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconExclamationCircle = class DigiIconExclamationCircle {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconExclamationCircle.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconExclamationCircle,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconExclamationCircle.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconExclamationCircle,
  selector: "digi-icon-exclamation-circle",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconExclamationCircle = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiIconExclamationCircle,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconExclamationCircle
);
export { DigiIconExclamationCircle };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconExclamationCircle,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-exclamation-circle",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconExclamationCircleFilled = class DigiIconExclamationCircleFilled {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconExclamationCircleFilled.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconExclamationCircleFilled,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconExclamationCircleFilled.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconExclamationCircleFilled,
  selector: "digi-icon-exclamation-circle-filled",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconExclamationCircleFilled = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiIconExclamationCircleFilled,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconExclamationCircleFilled
);
export { DigiIconExclamationCircleFilled };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconExclamationCircleFilled,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-exclamation-circle-filled",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconExclamationTriangle = class DigiIconExclamationTriangle {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconExclamationTriangle.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconExclamationTriangle,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconExclamationTriangle.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconExclamationTriangle,
  selector: "digi-icon-exclamation-triangle",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconExclamationTriangle = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiIconExclamationTriangle,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconExclamationTriangle
);
export { DigiIconExclamationTriangle };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconExclamationTriangle,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-exclamation-triangle",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconExclamationTriangleWarning = class DigiIconExclamationTriangleWarning {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconExclamationTriangleWarning.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconExclamationTriangleWarning,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconExclamationTriangleWarning.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconExclamationTriangleWarning,
  selector: "digi-icon-exclamation-triangle-warning",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconExclamationTriangleWarning = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiIconExclamationTriangleWarning,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconExclamationTriangleWarning
);
export { DigiIconExclamationTriangleWarning };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconExclamationTriangleWarning,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-exclamation-triangle-warning",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconExternalLinkAlt = class DigiIconExternalLinkAlt {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconExternalLinkAlt.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconExternalLinkAlt,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconExternalLinkAlt.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconExternalLinkAlt,
  selector: "digi-icon-external-link-alt",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconExternalLinkAlt = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiIconExternalLinkAlt,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconExternalLinkAlt
);
export { DigiIconExternalLinkAlt };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconExternalLinkAlt,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-external-link-alt",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconMinus = class DigiIconMinus {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconMinus.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconMinus,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconMinus.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconMinus,
  selector: "digi-icon-minus",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconMinus = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiIconMinus,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconMinus
);
export { DigiIconMinus };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconMinus,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-minus",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconPaperclip = class DigiIconPaperclip {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconPaperclip.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconPaperclip,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconPaperclip.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconPaperclip,
  selector: "digi-icon-paperclip",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconPaperclip = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiIconPaperclip,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconPaperclip
);
export { DigiIconPaperclip };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconPaperclip,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-paperclip",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconPlus = class DigiIconPlus {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconPlus.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconPlus,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconPlus.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconPlus,
  selector: "digi-icon-plus",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconPlus = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiIconPlus,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconPlus
);
export { DigiIconPlus };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconPlus,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-plus",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconSearch = class DigiIconSearch {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconSearch.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconSearch,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconSearch.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconSearch,
  selector: "digi-icon-search",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconSearch = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiIconSearch,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconSearch
);
export { DigiIconSearch };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconSearch,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-search",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconSpinner = class DigiIconSpinner {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconSpinner.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconSpinner,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconSpinner.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconSpinner,
  selector: "digi-icon-spinner",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconSpinner = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiIconSpinner,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconSpinner
);
export { DigiIconSpinner };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconSpinner,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-spinner",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconTrash = class DigiIconTrash {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconTrash.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconTrash,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconTrash.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconTrash,
  selector: "digi-icon-trash",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconTrash = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiIconTrash,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconTrash
);
export { DigiIconTrash };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconTrash,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-trash",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconX = class DigiIconX {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconX.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconX,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconX.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconX,
  selector: "digi-icon-x",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconX = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiIconX,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconX
);
export { DigiIconX };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconX,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-x",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiLayoutBlock = class DigiLayoutBlock {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiLayoutBlock.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLayoutBlock,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiLayoutBlock.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiLayoutBlock,
  selector: "digi-layout-block",
  inputs: {
    afContainer: "afContainer",
    afMarginBottom: "afMarginBottom",
    afMarginTop: "afMarginTop",
    afVariation: "afVariation",
    afVerticalPadding: "afVerticalPadding",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiLayoutBlock = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiLayoutBlock,
      inputs: [
        "afContainer",
        "afMarginBottom",
        "afMarginTop",
        "afVariation",
        "afVerticalPadding",
      ],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiLayoutBlock
);
export { DigiLayoutBlock };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLayoutBlock,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-layout-block",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afContainer",
            "afMarginBottom",
            "afMarginTop",
            "afVariation",
            "afVerticalPadding",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiLayoutColumns = class DigiLayoutColumns {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiLayoutColumns.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLayoutColumns,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiLayoutColumns.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiLayoutColumns,
  selector: "digi-layout-columns",
  inputs: { afElement: "afElement", afVariation: "afVariation" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiLayoutColumns = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiLayoutColumns,
      inputs: ["afElement", "afVariation"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiLayoutColumns
);
export { DigiLayoutColumns };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLayoutColumns,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-layout-columns",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afElement", "afVariation"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiLayoutContainer = class DigiLayoutContainer {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiLayoutContainer.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLayoutContainer,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiLayoutContainer.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiLayoutContainer,
  selector: "digi-layout-container",
  inputs: {
    afMarginBottom: "afMarginBottom",
    afMarginTop: "afMarginTop",
    afNoGutter: "afNoGutter",
    afVariation: "afVariation",
    afVerticalPadding: "afVerticalPadding",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiLayoutContainer = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiLayoutContainer,
      inputs: [
        "afMarginBottom",
        "afMarginTop",
        "afNoGutter",
        "afVariation",
        "afVerticalPadding",
      ],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiLayoutContainer
);
export { DigiLayoutContainer };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLayoutContainer,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-layout-container",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afMarginBottom",
            "afMarginTop",
            "afNoGutter",
            "afVariation",
            "afVerticalPadding",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiLayoutGrid = class DigiLayoutGrid {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiLayoutGrid.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLayoutGrid,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiLayoutGrid.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiLayoutGrid,
  selector: "digi-layout-grid",
  inputs: { afVerticalSpacing: "afVerticalSpacing" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiLayoutGrid = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiLayoutGrid,
      inputs: ["afVerticalSpacing"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiLayoutGrid
);
export { DigiLayoutGrid };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLayoutGrid,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-layout-grid",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afVerticalSpacing"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiLayoutMediaObject = class DigiLayoutMediaObject {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiLayoutMediaObject.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLayoutMediaObject,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiLayoutMediaObject.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiLayoutMediaObject,
  selector: "digi-layout-media-object",
  inputs: { afAlignment: "afAlignment" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiLayoutMediaObject = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiLayoutMediaObject,
      inputs: ["afAlignment"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiLayoutMediaObject
);
export { DigiLayoutMediaObject };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLayoutMediaObject,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-layout-media-object",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afAlignment"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiLayoutRows = class DigiLayoutRows {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiLayoutRows.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLayoutRows,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiLayoutRows.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiLayoutRows,
  selector: "digi-layout-rows",
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiLayoutRows = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiLayoutRows,
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiLayoutRows
);
export { DigiLayoutRows };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLayoutRows,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-layout-rows",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiLayoutStackedBlocks = class DigiLayoutStackedBlocks {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiLayoutStackedBlocks.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLayoutStackedBlocks,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiLayoutStackedBlocks.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiLayoutStackedBlocks,
  selector: "digi-layout-stacked-blocks",
  inputs: { afVariation: "afVariation" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiLayoutStackedBlocks = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiLayoutStackedBlocks,
      inputs: ["afVariation"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiLayoutStackedBlocks
);
export { DigiLayoutStackedBlocks };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLayoutStackedBlocks,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-layout-stacked-blocks",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afVariation"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiLink = class DigiLink {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnClick"]);
  }
};
DigiLink.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLink,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiLink.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiLink,
  selector: "digi-link",
  inputs: {
    afHref: "afHref",
    afOverrideLink: "afOverrideLink",
    afTarget: "afTarget",
    afVariation: "afVariation",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiLink = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiLink,
      inputs: ["afHref", "afOverrideLink", "afTarget", "afVariation"],
      methods: ["afMGetLinkElement"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiLink
);
export { DigiLink };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLink,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-link",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afHref", "afOverrideLink", "afTarget", "afVariation"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiLinkExternal = class DigiLinkExternal {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnClick"]);
  }
};
DigiLinkExternal.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLinkExternal,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiLinkExternal.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiLinkExternal,
  selector: "digi-link-external",
  inputs: {
    afHref: "afHref",
    afOverrideLink: "afOverrideLink",
    afTarget: "afTarget",
    afVariation: "afVariation",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiLinkExternal = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiLinkExternal,
      inputs: ["afHref", "afOverrideLink", "afTarget", "afVariation"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiLinkExternal
);
export { DigiLinkExternal };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLinkExternal,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-link-external",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afHref", "afOverrideLink", "afTarget", "afVariation"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiLinkIcon = class DigiLinkIcon {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiLinkIcon.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLinkIcon,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiLinkIcon.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiLinkIcon,
  selector: "digi-link-icon",
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiLinkIcon = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiLinkIcon,
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiLinkIcon
);
export { DigiLinkIcon };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLinkIcon,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-link-icon",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiLinkInternal = class DigiLinkInternal {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnClick"]);
  }
};
DigiLinkInternal.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLinkInternal,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiLinkInternal.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiLinkInternal,
  selector: "digi-link-internal",
  inputs: {
    afHref: "afHref",
    afOverrideLink: "afOverrideLink",
    afVariation: "afVariation",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiLinkInternal = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiLinkInternal,
      inputs: ["afHref", "afOverrideLink", "afVariation"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiLinkInternal
);
export { DigiLinkInternal };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLinkInternal,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-link-internal",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afHref", "afOverrideLink", "afVariation"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiListLink = class DigiListLink {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiListLink.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiListLink,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiListLink.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiListLink,
  selector: "digi-list-link",
  inputs: {
    afLayout: "afLayout",
    afType: "afType",
    afVariation: "afVariation",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiListLink = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiListLink,
      inputs: ["afLayout", "afType", "afVariation"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiListLink
);
export { DigiListLink };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiListLink,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-list-link",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afLayout", "afType", "afVariation"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiLoaderSpinner = class DigiLoaderSpinner {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiLoaderSpinner.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLoaderSpinner,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiLoaderSpinner.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiLoaderSpinner,
  selector: "digi-loader-spinner",
  inputs: { afSize: "afSize", afText: "afText" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiLoaderSpinner = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiLoaderSpinner,
      inputs: ["afSize", "afText"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiLoaderSpinner
);
export { DigiLoaderSpinner };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLoaderSpinner,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-loader-spinner",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afSize", "afText"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiLogo = class DigiLogo {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiLogo.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLogo,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiLogo.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiLogo,
  selector: "digi-logo",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
    afTitleId: "afTitleId",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiLogo = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiLogo,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle", "afTitleId"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiLogo
);
export { DigiLogo };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLogo,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-logo",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle", "afTitleId"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiLogoService = class DigiLogoService {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiLogoService.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLogoService,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiLogoService.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiLogoService,
  selector: "digi-logo-service",
  inputs: {
    afDescription: "afDescription",
    afName: "afName",
    afNameId: "afNameId",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiLogoService = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiLogoService,
      inputs: ["afDescription", "afName", "afNameId"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiLogoService
);
export { DigiLogoService };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLogoService,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-logo-service",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDescription", "afName", "afNameId"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiLogoSister = class DigiLogoSister {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiLogoSister.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLogoSister,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiLogoSister.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiLogoSister,
  selector: "digi-logo-sister",
  inputs: { afName: "afName", afNameId: "afNameId" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiLogoSister = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiLogoSister,
      inputs: ["afName", "afNameId"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiLogoSister
);
export { DigiLogoSister };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLogoSister,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-logo-sister",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afName", "afNameId"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiMediaFigure = class DigiMediaFigure {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiMediaFigure.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiMediaFigure,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiMediaFigure.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiMediaFigure,
  selector: "digi-media-figure",
  inputs: { afAlignment: "afAlignment", afFigcaption: "afFigcaption" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiMediaFigure = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiMediaFigure,
      inputs: ["afAlignment", "afFigcaption"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiMediaFigure
);
export { DigiMediaFigure };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiMediaFigure,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-media-figure",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afAlignment", "afFigcaption"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiMediaImage = class DigiMediaImage {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnLoad"]);
  }
};
DigiMediaImage.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiMediaImage,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiMediaImage.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiMediaImage,
  selector: "digi-media-image",
  inputs: {
    afAlt: "afAlt",
    afAriaLabel: "afAriaLabel",
    afFullwidth: "afFullwidth",
    afHeight: "afHeight",
    afObserverOptions: "afObserverOptions",
    afSrc: "afSrc",
    afSrcset: "afSrcset",
    afTitle: "afTitle",
    afUnlazy: "afUnlazy",
    afWidth: "afWidth",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiMediaImage = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiMediaImage,
      inputs: [
        "afAlt",
        "afAriaLabel",
        "afFullwidth",
        "afHeight",
        "afObserverOptions",
        "afSrc",
        "afSrcset",
        "afTitle",
        "afUnlazy",
        "afWidth",
      ],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiMediaImage
);
export { DigiMediaImage };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiMediaImage,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-media-image",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afAlt",
            "afAriaLabel",
            "afFullwidth",
            "afHeight",
            "afObserverOptions",
            "afSrc",
            "afSrcset",
            "afTitle",
            "afUnlazy",
            "afWidth",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiNavigationBreadcrumbs = class DigiNavigationBreadcrumbs {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiNavigationBreadcrumbs.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationBreadcrumbs,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiNavigationBreadcrumbs.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiNavigationBreadcrumbs,
  selector: "digi-navigation-breadcrumbs",
  inputs: {
    afAriaLabel: "afAriaLabel",
    afCurrentPage: "afCurrentPage",
    afVariation: "afVariation",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiNavigationBreadcrumbs = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiNavigationBreadcrumbs,
      inputs: ["afAriaLabel", "afCurrentPage", "afVariation"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiNavigationBreadcrumbs
);
export { DigiNavigationBreadcrumbs };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationBreadcrumbs,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-navigation-breadcrumbs",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afAriaLabel", "afCurrentPage", "afVariation"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiNavigationContextMenu = class DigiNavigationContextMenu {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, [
      "afOnInactive",
      "afOnActive",
      "afOnBlur",
      "afOnChange",
      "afOnToggle",
      "afOnSelect",
    ]);
  }
};
DigiNavigationContextMenu.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationContextMenu,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiNavigationContextMenu.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiNavigationContextMenu,
  selector: "digi-navigation-context-menu",
  inputs: {
    afIcon: "afIcon",
    afId: "afId",
    afNavigationContextMenuItems: "afNavigationContextMenuItems",
    afStartSelected: "afStartSelected",
    afText: "afText",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiNavigationContextMenu = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiNavigationContextMenu,
      inputs: [
        "afIcon",
        "afId",
        "afNavigationContextMenuItems",
        "afStartSelected",
        "afText",
      ],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiNavigationContextMenu
);
export { DigiNavigationContextMenu };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationContextMenu,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-navigation-context-menu",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afIcon",
            "afId",
            "afNavigationContextMenuItems",
            "afStartSelected",
            "afText",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiNavigationContextMenuItem = class DigiNavigationContextMenuItem {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiNavigationContextMenuItem.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationContextMenuItem,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiNavigationContextMenuItem.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiNavigationContextMenuItem,
  selector: "digi-navigation-context-menu-item",
  inputs: {
    afDir: "afDir",
    afHref: "afHref",
    afLang: "afLang",
    afText: "afText",
    afType: "afType",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiNavigationContextMenuItem = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiNavigationContextMenuItem,
      inputs: ["afDir", "afHref", "afLang", "afText", "afType"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiNavigationContextMenuItem
);
export { DigiNavigationContextMenuItem };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationContextMenuItem,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-navigation-context-menu-item",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDir", "afHref", "afLang", "afText", "afType"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiNavigationMainMenu = class DigiNavigationMainMenu {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiNavigationMainMenu.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationMainMenu,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiNavigationMainMenu.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiNavigationMainMenu,
  selector: "digi-navigation-main-menu",
  inputs: { afId: "afId" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiNavigationMainMenu = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiNavigationMainMenu,
      inputs: ["afId"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiNavigationMainMenu
);
export { DigiNavigationMainMenu };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationMainMenu,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-navigation-main-menu",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afId"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiNavigationMainMenuPanel = class DigiNavigationMainMenuPanel {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnResize", "afOnClose"]);
  }
};
DigiNavigationMainMenuPanel.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationMainMenuPanel,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiNavigationMainMenuPanel.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiNavigationMainMenuPanel,
  selector: "digi-navigation-main-menu-panel",
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiNavigationMainMenuPanel = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiNavigationMainMenuPanel,
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiNavigationMainMenuPanel
);
export { DigiNavigationMainMenuPanel };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationMainMenuPanel,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-navigation-main-menu-panel",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiNavigationPagination = class DigiNavigationPagination {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnPageChange"]);
  }
};
DigiNavigationPagination.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationPagination,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiNavigationPagination.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiNavigationPagination,
  selector: "digi-navigation-pagination",
  inputs: {
    afCurrentResultEnd: "afCurrentResultEnd",
    afCurrentResultStart: "afCurrentResultStart",
    afId: "afId",
    afInitActivePage: "afInitActivePage",
    afResultName: "afResultName",
    afTotalPages: "afTotalPages",
    afTotalResults: "afTotalResults",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiNavigationPagination = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiNavigationPagination,
      inputs: [
        "afCurrentResultEnd",
        "afCurrentResultStart",
        "afId",
        "afInitActivePage",
        "afResultName",
        "afTotalPages",
        "afTotalResults",
      ],
      methods: ["afMSetCurrentPage"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiNavigationPagination
);
export { DigiNavigationPagination };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationPagination,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-navigation-pagination",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afCurrentResultEnd",
            "afCurrentResultStart",
            "afId",
            "afInitActivePage",
            "afResultName",
            "afTotalPages",
            "afTotalResults",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiNavigationSidebar = class DigiNavigationSidebar {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, [
      "afOnClose",
      "afOnEsc",
      "afOnBackdropClick",
      "afOnToggle",
    ]);
  }
};
DigiNavigationSidebar.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationSidebar,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiNavigationSidebar.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiNavigationSidebar,
  selector: "digi-navigation-sidebar",
  inputs: {
    afActive: "afActive",
    afBackdrop: "afBackdrop",
    afCloseButtonAriaLabel: "afCloseButtonAriaLabel",
    afCloseButtonPosition: "afCloseButtonPosition",
    afCloseButtonText: "afCloseButtonText",
    afCloseFocusableElement: "afCloseFocusableElement",
    afFocusableElement: "afFocusableElement",
    afHeading: "afHeading",
    afHeadingLevel: "afHeadingLevel",
    afHideHeader: "afHideHeader",
    afId: "afId",
    afMobilePosition: "afMobilePosition",
    afMobileVariation: "afMobileVariation",
    afPosition: "afPosition",
    afStickyHeader: "afStickyHeader",
    afVariation: "afVariation",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiNavigationSidebar = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiNavigationSidebar,
      inputs: [
        "afActive",
        "afBackdrop",
        "afCloseButtonAriaLabel",
        "afCloseButtonPosition",
        "afCloseButtonText",
        "afCloseFocusableElement",
        "afFocusableElement",
        "afHeading",
        "afHeadingLevel",
        "afHideHeader",
        "afId",
        "afMobilePosition",
        "afMobileVariation",
        "afPosition",
        "afStickyHeader",
        "afVariation",
      ],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiNavigationSidebar
);
export { DigiNavigationSidebar };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationSidebar,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-navigation-sidebar",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afActive",
            "afBackdrop",
            "afCloseButtonAriaLabel",
            "afCloseButtonPosition",
            "afCloseButtonText",
            "afCloseFocusableElement",
            "afFocusableElement",
            "afHeading",
            "afHeadingLevel",
            "afHideHeader",
            "afId",
            "afMobilePosition",
            "afMobileVariation",
            "afPosition",
            "afStickyHeader",
            "afVariation",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiNavigationSidebarButton = class DigiNavigationSidebarButton {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnToggle"]);
  }
};
DigiNavigationSidebarButton.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationSidebarButton,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiNavigationSidebarButton.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiNavigationSidebarButton,
  selector: "digi-navigation-sidebar-button",
  inputs: { afAriaLabel: "afAriaLabel", afId: "afId", afText: "afText" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiNavigationSidebarButton = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiNavigationSidebarButton,
      inputs: ["afAriaLabel", "afId", "afText"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiNavigationSidebarButton
);
export { DigiNavigationSidebarButton };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationSidebarButton,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-navigation-sidebar-button",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afAriaLabel", "afId", "afText"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiNavigationTab = class DigiNavigationTab {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnToggle"]);
  }
};
DigiNavigationTab.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationTab,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiNavigationTab.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiNavigationTab,
  selector: "digi-navigation-tab",
  inputs: { afActive: "afActive", afAriaLabel: "afAriaLabel", afId: "afId" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiNavigationTab = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiNavigationTab,
      inputs: ["afActive", "afAriaLabel", "afId"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiNavigationTab
);
export { DigiNavigationTab };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationTab,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-navigation-tab",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afActive", "afAriaLabel", "afId"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiNavigationTabInABox = class DigiNavigationTabInABox {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnToggle"]);
  }
};
DigiNavigationTabInABox.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationTabInABox,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiNavigationTabInABox.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiNavigationTabInABox,
  selector: "digi-navigation-tab-in-a-box",
  inputs: { afActive: "afActive", afAriaLabel: "afAriaLabel", afId: "afId" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiNavigationTabInABox = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiNavigationTabInABox,
      inputs: ["afActive", "afAriaLabel", "afId"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiNavigationTabInABox
);
export { DigiNavigationTabInABox };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationTabInABox,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-navigation-tab-in-a-box",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afActive", "afAriaLabel", "afId"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiNavigationTabs = class DigiNavigationTabs {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnChange", "afOnClick", "afOnFocus"]);
  }
};
DigiNavigationTabs.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationTabs,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiNavigationTabs.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiNavigationTabs,
  selector: "digi-navigation-tabs",
  inputs: {
    afAriaLabel: "afAriaLabel",
    afId: "afId",
    afInitActiveTab: "afInitActiveTab",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiNavigationTabs = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiNavigationTabs,
      inputs: ["afAriaLabel", "afId", "afInitActiveTab"],
      methods: ["afMSetActiveTab"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiNavigationTabs
);
export { DigiNavigationTabs };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationTabs,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-navigation-tabs",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afAriaLabel", "afId", "afInitActiveTab"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiNavigationToc = class DigiNavigationToc {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiNavigationToc.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationToc,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiNavigationToc.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiNavigationToc,
  selector: "digi-navigation-toc",
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiNavigationToc = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiNavigationToc,
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiNavigationToc
);
export { DigiNavigationToc };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationToc,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-navigation-toc",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiNavigationVerticalMenu = class DigiNavigationVerticalMenu {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiNavigationVerticalMenu.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationVerticalMenu,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiNavigationVerticalMenu.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiNavigationVerticalMenu,
  selector: "digi-navigation-vertical-menu",
  inputs: {
    afActiveIndicatorSize: "afActiveIndicatorSize",
    afAriaLabel: "afAriaLabel",
    afId: "afId",
    afVariation: "afVariation",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiNavigationVerticalMenu = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiNavigationVerticalMenu,
      inputs: ["afActiveIndicatorSize", "afAriaLabel", "afId", "afVariation"],
      methods: ["afMSetCurrentActiveLevel"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiNavigationVerticalMenu
);
export { DigiNavigationVerticalMenu };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationVerticalMenu,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-navigation-vertical-menu",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afActiveIndicatorSize",
            "afAriaLabel",
            "afId",
            "afVariation",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiNavigationVerticalMenuItem = class DigiNavigationVerticalMenuItem {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnClick"]);
  }
};
DigiNavigationVerticalMenuItem.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationVerticalMenuItem,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiNavigationVerticalMenuItem.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiNavigationVerticalMenuItem,
  selector: "digi-navigation-vertical-menu-item",
  inputs: {
    afActive: "afActive",
    afActiveSubnav: "afActiveSubnav",
    afHasSubnav: "afHasSubnav",
    afHref: "afHref",
    afId: "afId",
    afOverrideLink: "afOverrideLink",
    afText: "afText",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiNavigationVerticalMenuItem = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiNavigationVerticalMenuItem,
      inputs: [
        "afActive",
        "afActiveSubnav",
        "afHasSubnav",
        "afHref",
        "afId",
        "afOverrideLink",
        "afText",
      ],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiNavigationVerticalMenuItem
);
export { DigiNavigationVerticalMenuItem };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationVerticalMenuItem,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-navigation-vertical-menu-item",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afActive",
            "afActiveSubnav",
            "afHasSubnav",
            "afHref",
            "afId",
            "afOverrideLink",
            "afText",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiNotificationAlert = class DigiNotificationAlert {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnClose"]);
  }
};
DigiNotificationAlert.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNotificationAlert,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiNotificationAlert.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiNotificationAlert,
  selector: "digi-notification-alert",
  inputs: { afCloseable: "afCloseable", afVariation: "afVariation" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiNotificationAlert = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiNotificationAlert,
      inputs: ["afCloseable", "afVariation"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiNotificationAlert
);
export { DigiNotificationAlert };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNotificationAlert,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-notification-alert",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afCloseable", "afVariation"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiNotificationCookie = class DigiNotificationCookie {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnAcceptAllCookies", "afOnSubmitSettings"]);
  }
};
DigiNotificationCookie.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNotificationCookie,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiNotificationCookie.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiNotificationCookie,
  selector: "digi-notification-cookie",
  inputs: {
    afBannerHeadingText: "afBannerHeadingText",
    afBannerText: "afBannerText",
    afId: "afId",
    afModalHeadingText: "afModalHeadingText",
    afRequiredCookiesText: "afRequiredCookiesText",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiNotificationCookie = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiNotificationCookie,
      inputs: [
        "afBannerHeadingText",
        "afBannerText",
        "afId",
        "afModalHeadingText",
        "afRequiredCookiesText",
      ],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiNotificationCookie
);
export { DigiNotificationCookie };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNotificationCookie,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-notification-cookie",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afBannerHeadingText",
            "afBannerText",
            "afId",
            "afModalHeadingText",
            "afRequiredCookiesText",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiNotificationDetail = class DigiNotificationDetail {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiNotificationDetail.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNotificationDetail,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiNotificationDetail.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiNotificationDetail,
  selector: "digi-notification-detail",
  inputs: { afVariation: "afVariation" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiNotificationDetail = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiNotificationDetail,
      inputs: ["afVariation"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiNotificationDetail
);
export { DigiNotificationDetail };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNotificationDetail,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-notification-detail",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afVariation"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiPage = class DigiPage {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiPage.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiPage,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiPage.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiPage,
  selector: "digi-page",
  inputs: { afBackground: "afBackground" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiPage = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiPage,
      inputs: ["afBackground"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiPage
);
export { DigiPage };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiPage,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-page",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afBackground"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiPageBlockCards = class DigiPageBlockCards {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiPageBlockCards.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiPageBlockCards,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiPageBlockCards.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiPageBlockCards,
  selector: "digi-page-block-cards",
  inputs: { afVariation: "afVariation" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiPageBlockCards = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiPageBlockCards,
      inputs: ["afVariation"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiPageBlockCards
);
export { DigiPageBlockCards };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiPageBlockCards,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-page-block-cards",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afVariation"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiPageBlockHero = class DigiPageBlockHero {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiPageBlockHero.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiPageBlockHero,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiPageBlockHero.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiPageBlockHero,
  selector: "digi-page-block-hero",
  inputs: {
    afBackground: "afBackground",
    afBackgroundImage: "afBackgroundImage",
    afVariation: "afVariation",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiPageBlockHero = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiPageBlockHero,
      inputs: ["afBackground", "afBackgroundImage", "afVariation"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiPageBlockHero
);
export { DigiPageBlockHero };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiPageBlockHero,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-page-block-hero",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afBackground", "afBackgroundImage", "afVariation"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiPageBlockLists = class DigiPageBlockLists {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiPageBlockLists.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiPageBlockLists,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiPageBlockLists.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiPageBlockLists,
  selector: "digi-page-block-lists",
  inputs: { afVariation: "afVariation" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiPageBlockLists = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiPageBlockLists,
      inputs: ["afVariation"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiPageBlockLists
);
export { DigiPageBlockLists };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiPageBlockLists,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-page-block-lists",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afVariation"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiPageBlockSidebar = class DigiPageBlockSidebar {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiPageBlockSidebar.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiPageBlockSidebar,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiPageBlockSidebar.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiPageBlockSidebar,
  selector: "digi-page-block-sidebar",
  inputs: { afVariation: "afVariation" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiPageBlockSidebar = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiPageBlockSidebar,
      inputs: ["afVariation"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiPageBlockSidebar
);
export { DigiPageBlockSidebar };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiPageBlockSidebar,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-page-block-sidebar",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afVariation"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiPageFooter = class DigiPageFooter {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiPageFooter.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiPageFooter,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiPageFooter.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiPageFooter,
  selector: "digi-page-footer",
  inputs: { afVariation: "afVariation" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiPageFooter = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiPageFooter,
      inputs: ["afVariation"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiPageFooter
);
export { DigiPageFooter };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiPageFooter,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-page-footer",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afVariation"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiPageHeader = class DigiPageHeader {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiPageHeader.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiPageHeader,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiPageHeader.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiPageHeader,
  selector: "digi-page-header",
  inputs: { afId: "afId" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiPageHeader = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiPageHeader,
      inputs: ["afId"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiPageHeader
);
export { DigiPageHeader };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiPageHeader,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-page-header",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afId"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiProgressStep = class DigiProgressStep {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiProgressStep.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiProgressStep,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiProgressStep.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiProgressStep,
  selector: "digi-progress-step",
  inputs: {
    afHeading: "afHeading",
    afHeadingLevel: "afHeadingLevel",
    afId: "afId",
    afIsLast: "afIsLast",
    afStepStatus: "afStepStatus",
    afVariation: "afVariation",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiProgressStep = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiProgressStep,
      inputs: [
        "afHeading",
        "afHeadingLevel",
        "afId",
        "afIsLast",
        "afStepStatus",
        "afVariation",
      ],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiProgressStep
);
export { DigiProgressStep };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiProgressStep,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-progress-step",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afHeading",
            "afHeadingLevel",
            "afId",
            "afIsLast",
            "afStepStatus",
            "afVariation",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiProgressSteps = class DigiProgressSteps {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiProgressSteps.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiProgressSteps,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiProgressSteps.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiProgressSteps,
  selector: "digi-progress-steps",
  inputs: {
    afCurrentStep: "afCurrentStep",
    afHeadingLevel: "afHeadingLevel",
    afVariation: "afVariation",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiProgressSteps = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiProgressSteps,
      inputs: ["afCurrentStep", "afHeadingLevel", "afVariation"],
      methods: ["afMNext", "afMPrevious"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiProgressSteps
);
export { DigiProgressSteps };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiProgressSteps,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-progress-steps",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afCurrentStep", "afHeadingLevel", "afVariation"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiProgressbar = class DigiProgressbar {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiProgressbar.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiProgressbar,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiProgressbar.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiProgressbar,
  selector: "digi-progressbar",
  inputs: {
    afCompletedSteps: "afCompletedSteps",
    afId: "afId",
    afRole: "afRole",
    afStepsLabel: "afStepsLabel",
    afStepsSeparator: "afStepsSeparator",
    afTotalSteps: "afTotalSteps",
    afVariation: "afVariation",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiProgressbar = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiProgressbar,
      inputs: [
        "afCompletedSteps",
        "afId",
        "afRole",
        "afStepsLabel",
        "afStepsSeparator",
        "afTotalSteps",
        "afVariation",
      ],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiProgressbar
);
export { DigiProgressbar };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiProgressbar,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-progressbar",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afCompletedSteps",
            "afId",
            "afRole",
            "afStepsLabel",
            "afStepsSeparator",
            "afTotalSteps",
            "afVariation",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiTable = class DigiTable {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiTable.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiTable,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiTable.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiTable,
  selector: "digi-table",
  inputs: { afVariation: "afVariation" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiTable = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiTable,
      inputs: ["afVariation"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiTable
);
export { DigiTable };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiTable,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-table",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afVariation"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiTag = class DigiTag {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnClick"]);
  }
};
DigiTag.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiTag,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiTag.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiTag,
  selector: "digi-tag",
  inputs: { afNoIcon: "afNoIcon", afSize: "afSize", afText: "afText" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiTag = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiTag,
      inputs: ["afNoIcon", "afSize", "afText"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiTag
);
export { DigiTag };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiTag,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-tag",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afNoIcon", "afSize", "afText"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiTypography = class DigiTypography {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiTypography.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiTypography,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiTypography.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiTypography,
  selector: "digi-typography",
  inputs: { afVariation: "afVariation" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiTypography = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiTypography,
      inputs: ["afVariation"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiTypography
);
export { DigiTypography };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiTypography,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-typography",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afVariation"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiTypographyHeadingSection = class DigiTypographyHeadingSection {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiTypographyHeadingSection.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiTypographyHeadingSection,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiTypographyHeadingSection.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiTypographyHeadingSection,
  selector: "digi-typography-heading-section",
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiTypographyHeadingSection = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiTypographyHeadingSection,
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiTypographyHeadingSection
);
export { DigiTypographyHeadingSection };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiTypographyHeadingSection,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-typography-heading-section",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiTypographyMeta = class DigiTypographyMeta {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiTypographyMeta.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiTypographyMeta,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiTypographyMeta.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiTypographyMeta,
  selector: "digi-typography-meta",
  inputs: { afVariation: "afVariation" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiTypographyMeta = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiTypographyMeta,
      inputs: ["afVariation"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiTypographyMeta
);
export { DigiTypographyMeta };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiTypographyMeta,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-typography-meta",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afVariation"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiTypographyPreamble = class DigiTypographyPreamble {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiTypographyPreamble.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiTypographyPreamble,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiTypographyPreamble.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiTypographyPreamble,
  selector: "digi-typography-preamble",
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiTypographyPreamble = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiTypographyPreamble,
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiTypographyPreamble
);
export { DigiTypographyPreamble };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiTypographyPreamble,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-typography-preamble",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiTypographyTime = class DigiTypographyTime {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiTypographyTime.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiTypographyTime,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiTypographyTime.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiTypographyTime,
  selector: "digi-typography-time",
  inputs: { afDateTime: "afDateTime", afVariation: "afVariation" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiTypographyTime = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiTypographyTime,
      inputs: ["afDateTime", "afVariation"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiTypographyTime
);
export { DigiTypographyTime };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiTypographyTime,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-typography-time",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDateTime", "afVariation"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiUtilBreakpointObserver = class DigiUtilBreakpointObserver {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, [
      "afOnChange",
      "afOnSmall",
      "afOnMedium",
      "afOnLarge",
      "afOnXLarge",
    ]);
  }
};
DigiUtilBreakpointObserver.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiUtilBreakpointObserver,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiUtilBreakpointObserver.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiUtilBreakpointObserver,
  selector: "digi-util-breakpoint-observer",
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiUtilBreakpointObserver = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiUtilBreakpointObserver,
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiUtilBreakpointObserver
);
export { DigiUtilBreakpointObserver };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiUtilBreakpointObserver,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-util-breakpoint-observer",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiUtilDetectClickOutside = class DigiUtilDetectClickOutside {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnClickOutside", "afOnClickInside"]);
  }
};
DigiUtilDetectClickOutside.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiUtilDetectClickOutside,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiUtilDetectClickOutside.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiUtilDetectClickOutside,
  selector: "digi-util-detect-click-outside",
  inputs: { afDataIdentifier: "afDataIdentifier" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiUtilDetectClickOutside = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiUtilDetectClickOutside,
      inputs: ["afDataIdentifier"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiUtilDetectClickOutside
);
export { DigiUtilDetectClickOutside };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiUtilDetectClickOutside,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-util-detect-click-outside",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDataIdentifier"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiUtilDetectFocusOutside = class DigiUtilDetectFocusOutside {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnFocusOutside", "afOnFocusInside"]);
  }
};
DigiUtilDetectFocusOutside.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiUtilDetectFocusOutside,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiUtilDetectFocusOutside.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiUtilDetectFocusOutside,
  selector: "digi-util-detect-focus-outside",
  inputs: { afDataIdentifier: "afDataIdentifier" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiUtilDetectFocusOutside = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiUtilDetectFocusOutside,
      inputs: ["afDataIdentifier"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiUtilDetectFocusOutside
);
export { DigiUtilDetectFocusOutside };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiUtilDetectFocusOutside,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-util-detect-focus-outside",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDataIdentifier"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiUtilIntersectionObserver = class DigiUtilIntersectionObserver {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, [
      "afOnChange",
      "afOnIntersect",
      "afOnUnintersect",
    ]);
  }
};
DigiUtilIntersectionObserver.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiUtilIntersectionObserver,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiUtilIntersectionObserver.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiUtilIntersectionObserver,
  selector: "digi-util-intersection-observer",
  inputs: { afOnce: "afOnce", afOptions: "afOptions" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiUtilIntersectionObserver = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiUtilIntersectionObserver,
      inputs: ["afOnce", "afOptions"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiUtilIntersectionObserver
);
export { DigiUtilIntersectionObserver };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiUtilIntersectionObserver,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-util-intersection-observer",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afOnce", "afOptions"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiUtilKeydownHandler = class DigiUtilKeydownHandler {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, [
      "afOnEsc",
      "afOnEnter",
      "afOnTab",
      "afOnSpace",
      "afOnShiftTab",
      "afOnUp",
      "afOnDown",
      "afOnLeft",
      "afOnRight",
      "afOnHome",
      "afOnEnd",
      "afOnKeyDown",
    ]);
  }
};
DigiUtilKeydownHandler.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiUtilKeydownHandler,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiUtilKeydownHandler.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiUtilKeydownHandler,
  selector: "digi-util-keydown-handler",
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiUtilKeydownHandler = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiUtilKeydownHandler,
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiUtilKeydownHandler
);
export { DigiUtilKeydownHandler };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiUtilKeydownHandler,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-util-keydown-handler",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiUtilKeyupHandler = class DigiUtilKeyupHandler {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, [
      "afOnEsc",
      "afOnEnter",
      "afOnTab",
      "afOnSpace",
      "afOnShiftTab",
      "afOnUp",
      "afOnDown",
      "afOnLeft",
      "afOnRight",
      "afOnHome",
      "afOnEnd",
      "afOnKeyUp",
    ]);
  }
};
DigiUtilKeyupHandler.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiUtilKeyupHandler,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiUtilKeyupHandler.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiUtilKeyupHandler,
  selector: "digi-util-keyup-handler",
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiUtilKeyupHandler = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiUtilKeyupHandler,
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiUtilKeyupHandler
);
export { DigiUtilKeyupHandler };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiUtilKeyupHandler,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-util-keyup-handler",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiUtilMutationObserver = class DigiUtilMutationObserver {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnChange"]);
  }
};
DigiUtilMutationObserver.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiUtilMutationObserver,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiUtilMutationObserver.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiUtilMutationObserver,
  selector: "digi-util-mutation-observer",
  inputs: { afId: "afId", afOptions: "afOptions" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiUtilMutationObserver = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiUtilMutationObserver,
      inputs: ["afId", "afOptions"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiUtilMutationObserver
);
export { DigiUtilMutationObserver };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiUtilMutationObserver,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-util-mutation-observer",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afId", "afOptions"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiUtilResizeObserver = class DigiUtilResizeObserver {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnChange"]);
  }
};
DigiUtilResizeObserver.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiUtilResizeObserver,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiUtilResizeObserver.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiUtilResizeObserver,
  selector: "digi-util-resize-observer",
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiUtilResizeObserver = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineDigiUtilResizeObserver,
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiUtilResizeObserver
);
export { DigiUtilResizeObserver };
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiUtilResizeObserver,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-util-resize-observer",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50cy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL2xpYnMvc2tvbHZlcmtldC9hbmd1bGFyL3NyYy9saWIvc3RlbmNpbC1nZW5lcmF0ZWQvY29tcG9uZW50cy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsb0JBQW9CO0FBQ3BCLDhDQUE4QztBQUM5QyxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsaUJBQWlCLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBZ0IsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3hILE9BQU8sRUFBRSxRQUFRLEVBQUUsWUFBWSxFQUFFLE1BQU0sK0JBQStCLENBQUM7QUFJdkUsT0FBTyxFQUFFLG1CQUFtQixJQUFJLGdCQUFnQixFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFDckcsT0FBTyxFQUFFLG1CQUFtQixJQUFJLGtCQUFrQixFQUFFLE1BQU0sOENBQThDLENBQUM7QUFDekcsT0FBTyxFQUFFLG1CQUFtQixJQUFJLDBCQUEwQixFQUFFLE1BQU0sd0RBQXdELENBQUM7QUFDM0gsT0FBTyxFQUFFLG1CQUFtQixJQUFJLGlCQUFpQixFQUFFLE1BQU0sOENBQThDLENBQUM7QUFDeEcsT0FBTyxFQUFFLG1CQUFtQixJQUFJLGtCQUFrQixFQUFFLE1BQU0sK0NBQStDLENBQUM7QUFDMUcsT0FBTyxFQUFFLG1CQUFtQixJQUFJLGNBQWMsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQ2pHLE9BQU8sRUFBRSxtQkFBbUIsSUFBSSxtQkFBbUIsRUFBRSxNQUFNLGdEQUFnRCxDQUFDO0FBQzVHLE9BQU8sRUFBRSxtQkFBbUIsSUFBSSxxQkFBcUIsRUFBRSxNQUFNLGtEQUFrRCxDQUFDO0FBQ2hILE9BQU8sRUFBRSxtQkFBbUIsSUFBSSxnQkFBZ0IsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQ3JHLE9BQU8sRUFBRSxtQkFBbUIsSUFBSSw2QkFBNkIsRUFBRSxNQUFNLDBEQUEwRCxDQUFDO0FBQ2hJLE9BQU8sRUFBRSxtQkFBbUIsSUFBSSxzQkFBc0IsRUFBRSxNQUFNLG1EQUFtRCxDQUFDO0FBQ2xILE9BQU8sRUFBRSxtQkFBbUIsSUFBSSxzQkFBc0IsRUFBRSxNQUFNLG1EQUFtRCxDQUFDO0FBQ2xILE9BQU8sRUFBRSxtQkFBbUIsSUFBSSx3QkFBd0IsRUFBRSxNQUFNLHNEQUFzRCxDQUFDO0FBQ3ZILE9BQU8sRUFBRSxtQkFBbUIsSUFBSSxvQkFBb0IsRUFBRSxNQUFNLGlEQUFpRCxDQUFDO0FBQzlHLE9BQU8sRUFBRSxtQkFBbUIsSUFBSSxtQkFBbUIsRUFBRSxNQUFNLGdEQUFnRCxDQUFDO0FBQzVHLE9BQU8sRUFBRSxtQkFBbUIsSUFBSSx5QkFBeUIsRUFBRSxNQUFNLHVEQUF1RCxDQUFDO0FBQ3pILE9BQU8sRUFBRSxtQkFBbUIsSUFBSSxtQkFBbUIsRUFBRSxNQUFNLGdEQUFnRCxDQUFDO0FBQzVHLE9BQU8sRUFBRSxtQkFBbUIsSUFBSSx5QkFBeUIsRUFBRSxNQUFNLHVEQUF1RCxDQUFDO0FBQ3pILE9BQU8sRUFBRSxtQkFBbUIsSUFBSSwwQkFBMEIsRUFBRSxNQUFNLHdEQUF3RCxDQUFDO0FBQzNILE9BQU8sRUFBRSxtQkFBbUIsSUFBSSx5QkFBeUIsRUFBRSxNQUFNLHNEQUFzRCxDQUFDO0FBQ3hILE9BQU8sRUFBRSxtQkFBbUIsSUFBSSx3QkFBd0IsRUFBRSxNQUFNLHFEQUFxRCxDQUFDO0FBQ3RILE9BQU8sRUFBRSxtQkFBbUIsSUFBSSxvQkFBb0IsRUFBRSxNQUFNLGlEQUFpRCxDQUFDO0FBQzlHLE9BQU8sRUFBRSxtQkFBbUIsSUFBSSxzQkFBc0IsRUFBRSxNQUFNLG1EQUFtRCxDQUFDO0FBQ2xILE9BQU8sRUFBRSxtQkFBbUIsSUFBSSwrQkFBK0IsRUFBRSxNQUFNLDZEQUE2RCxDQUFDO0FBQ3JJLE9BQU8sRUFBRSxtQkFBbUIsSUFBSSxjQUFjLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUNqRyxPQUFPLEVBQUUsbUJBQW1CLElBQUksa0JBQWtCLEVBQUUsTUFBTSwrQ0FBK0MsQ0FBQztBQUMxRyxPQUFPLEVBQUUsbUJBQW1CLElBQUksbUJBQW1CLEVBQUUsTUFBTSxnREFBZ0QsQ0FBQztBQUM1RyxPQUFPLEVBQUUsbUJBQW1CLElBQUkseUJBQXlCLEVBQUUsTUFBTSx1REFBdUQsQ0FBQztBQUN6SCxPQUFPLEVBQUUsbUJBQW1CLElBQUksNEJBQTRCLEVBQUUsTUFBTSwyREFBMkQsQ0FBQztBQUNoSSxPQUFPLEVBQUUsbUJBQW1CLElBQUksK0JBQStCLEVBQUUsTUFBTSwrREFBK0QsQ0FBQztBQUN2SSxPQUFPLEVBQUUsbUJBQW1CLElBQUkseUJBQXlCLEVBQUUsTUFBTSx1REFBdUQsQ0FBQztBQUN6SCxPQUFPLEVBQUUsbUJBQW1CLElBQUkseUJBQXlCLEVBQUUsTUFBTSx1REFBdUQsQ0FBQztBQUN6SCxPQUFPLEVBQUUsbUJBQW1CLElBQUksMEJBQTBCLEVBQUUsTUFBTSx3REFBd0QsQ0FBQztBQUMzSCxPQUFPLEVBQUUsbUJBQW1CLElBQUksdUJBQXVCLEVBQUUsTUFBTSxxREFBcUQsQ0FBQztBQUNySCxPQUFPLEVBQUUsbUJBQW1CLElBQUksa0JBQWtCLEVBQUUsTUFBTSwrQ0FBK0MsQ0FBQztBQUMxRyxPQUFPLEVBQUUsbUJBQW1CLElBQUksMkJBQTJCLEVBQUUsTUFBTSx5REFBeUQsQ0FBQztBQUM3SCxPQUFPLEVBQUUsbUJBQW1CLElBQUksc0JBQXNCLEVBQUUsTUFBTSxtREFBbUQsQ0FBQztBQUNsSCxPQUFPLEVBQUUsbUJBQW1CLElBQUksK0JBQStCLEVBQUUsTUFBTSw2REFBNkQsQ0FBQztBQUNySSxPQUFPLEVBQUUsbUJBQW1CLElBQUkscUNBQXFDLEVBQUUsTUFBTSxvRUFBb0UsQ0FBQztBQUNsSixPQUFPLEVBQUUsbUJBQW1CLElBQUksaUNBQWlDLEVBQUUsTUFBTSwrREFBK0QsQ0FBQztBQUN6SSxPQUFPLEVBQUUsbUJBQW1CLElBQUksd0NBQXdDLEVBQUUsTUFBTSx1RUFBdUUsQ0FBQztBQUN4SixPQUFPLEVBQUUsbUJBQW1CLElBQUksNkJBQTZCLEVBQUUsTUFBTSw0REFBNEQsQ0FBQztBQUNsSSxPQUFPLEVBQUUsbUJBQW1CLElBQUksbUJBQW1CLEVBQUUsTUFBTSxnREFBZ0QsQ0FBQztBQUM1RyxPQUFPLEVBQUUsbUJBQW1CLElBQUksdUJBQXVCLEVBQUUsTUFBTSxvREFBb0QsQ0FBQztBQUNwSCxPQUFPLEVBQUUsbUJBQW1CLElBQUksa0JBQWtCLEVBQUUsTUFBTSwrQ0FBK0MsQ0FBQztBQUMxRyxPQUFPLEVBQUUsbUJBQW1CLElBQUksb0JBQW9CLEVBQUUsTUFBTSxpREFBaUQsQ0FBQztBQUM5RyxPQUFPLEVBQUUsbUJBQW1CLElBQUkscUJBQXFCLEVBQUUsTUFBTSxrREFBa0QsQ0FBQztBQUNoSCxPQUFPLEVBQUUsbUJBQW1CLElBQUksbUJBQW1CLEVBQUUsTUFBTSxnREFBZ0QsQ0FBQztBQUM1RyxPQUFPLEVBQUUsbUJBQW1CLElBQUksZUFBZSxFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFDcEcsT0FBTyxFQUFFLG1CQUFtQixJQUFJLHFCQUFxQixFQUFFLE1BQU0sa0RBQWtELENBQUM7QUFDaEgsT0FBTyxFQUFFLG1CQUFtQixJQUFJLHVCQUF1QixFQUFFLE1BQU0sb0RBQW9ELENBQUM7QUFDcEgsT0FBTyxFQUFFLG1CQUFtQixJQUFJLHlCQUF5QixFQUFFLE1BQU0sc0RBQXNELENBQUM7QUFDeEgsT0FBTyxFQUFFLG1CQUFtQixJQUFJLG9CQUFvQixFQUFFLE1BQU0saURBQWlELENBQUM7QUFDOUcsT0FBTyxFQUFFLG1CQUFtQixJQUFJLDJCQUEyQixFQUFFLE1BQU0seURBQXlELENBQUM7QUFDN0gsT0FBTyxFQUFFLG1CQUFtQixJQUFJLG9CQUFvQixFQUFFLE1BQU0saURBQWlELENBQUM7QUFDOUcsT0FBTyxFQUFFLG1CQUFtQixJQUFJLDZCQUE2QixFQUFFLE1BQU0sMkRBQTJELENBQUM7QUFDakksT0FBTyxFQUFFLG1CQUFtQixJQUFJLGNBQWMsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQ2pHLE9BQU8sRUFBRSxtQkFBbUIsSUFBSSxzQkFBc0IsRUFBRSxNQUFNLG1EQUFtRCxDQUFDO0FBQ2xILE9BQU8sRUFBRSxtQkFBbUIsSUFBSSxrQkFBa0IsRUFBRSxNQUFNLCtDQUErQyxDQUFDO0FBQzFHLE9BQU8sRUFBRSxtQkFBbUIsSUFBSSxzQkFBc0IsRUFBRSxNQUFNLG1EQUFtRCxDQUFDO0FBQ2xILE9BQU8sRUFBRSxtQkFBbUIsSUFBSSxrQkFBa0IsRUFBRSxNQUFNLCtDQUErQyxDQUFDO0FBQzFHLE9BQU8sRUFBRSxtQkFBbUIsSUFBSSx1QkFBdUIsRUFBRSxNQUFNLG9EQUFvRCxDQUFDO0FBQ3BILE9BQU8sRUFBRSxtQkFBbUIsSUFBSSxjQUFjLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUNqRyxPQUFPLEVBQUUsbUJBQW1CLElBQUkscUJBQXFCLEVBQUUsTUFBTSxrREFBa0QsQ0FBQztBQUNoSCxPQUFPLEVBQUUsbUJBQW1CLElBQUksb0JBQW9CLEVBQUUsTUFBTSxpREFBaUQsQ0FBQztBQUM5RyxPQUFPLEVBQUUsbUJBQW1CLElBQUkscUJBQXFCLEVBQUUsTUFBTSxrREFBa0QsQ0FBQztBQUNoSCxPQUFPLEVBQUUsbUJBQW1CLElBQUksb0JBQW9CLEVBQUUsTUFBTSxpREFBaUQsQ0FBQztBQUM5RyxPQUFPLEVBQUUsbUJBQW1CLElBQUksK0JBQStCLEVBQUUsTUFBTSw0REFBNEQsQ0FBQztBQUNwSSxPQUFPLEVBQUUsbUJBQW1CLElBQUksK0JBQStCLEVBQUUsTUFBTSw2REFBNkQsQ0FBQztBQUNySSxPQUFPLEVBQUUsbUJBQW1CLElBQUksbUNBQW1DLEVBQUUsTUFBTSxrRUFBa0UsQ0FBQztBQUM5SSxPQUFPLEVBQUUsbUJBQW1CLElBQUksNEJBQTRCLEVBQUUsTUFBTSwwREFBMEQsQ0FBQztBQUMvSCxPQUFPLEVBQUUsbUJBQW1CLElBQUksaUNBQWlDLEVBQUUsTUFBTSxnRUFBZ0UsQ0FBQztBQUMxSSxPQUFPLEVBQUUsbUJBQW1CLElBQUksOEJBQThCLEVBQUUsTUFBTSwyREFBMkQsQ0FBQztBQUNsSSxPQUFPLEVBQUUsbUJBQW1CLElBQUksMkJBQTJCLEVBQUUsTUFBTSx3REFBd0QsQ0FBQztBQUM1SCxPQUFPLEVBQUUsbUJBQW1CLElBQUksaUNBQWlDLEVBQUUsTUFBTSwrREFBK0QsQ0FBQztBQUN6SSxPQUFPLEVBQUUsbUJBQW1CLElBQUksdUJBQXVCLEVBQUUsTUFBTSxvREFBb0QsQ0FBQztBQUNwSCxPQUFPLEVBQUUsbUJBQW1CLElBQUksNkJBQTZCLEVBQUUsTUFBTSw2REFBNkQsQ0FBQztBQUNuSSxPQUFPLEVBQUUsbUJBQW1CLElBQUksd0JBQXdCLEVBQUUsTUFBTSxxREFBcUQsQ0FBQztBQUN0SCxPQUFPLEVBQUUsbUJBQW1CLElBQUksdUJBQXVCLEVBQUUsTUFBTSxvREFBb0QsQ0FBQztBQUNwSCxPQUFPLEVBQUUsbUJBQW1CLElBQUksZ0NBQWdDLEVBQUUsTUFBTSw4REFBOEQsQ0FBQztBQUN2SSxPQUFPLEVBQUUsbUJBQW1CLElBQUksb0NBQW9DLEVBQUUsTUFBTSxtRUFBbUUsQ0FBQztBQUNoSixPQUFPLEVBQUUsbUJBQW1CLElBQUksMkJBQTJCLEVBQUUsTUFBTSx3REFBd0QsQ0FBQztBQUM1SCxPQUFPLEVBQUUsbUJBQW1CLElBQUksNEJBQTRCLEVBQUUsTUFBTSx5REFBeUQsQ0FBQztBQUM5SCxPQUFPLEVBQUUsbUJBQW1CLElBQUksNEJBQTRCLEVBQUUsTUFBTSx5REFBeUQsQ0FBQztBQUM5SCxPQUFPLEVBQUUsbUJBQW1CLElBQUksY0FBYyxFQUFFLE1BQU0sMENBQTBDLENBQUM7QUFDakcsT0FBTyxFQUFFLG1CQUFtQixJQUFJLHdCQUF3QixFQUFFLE1BQU0sc0RBQXNELENBQUM7QUFDdkgsT0FBTyxFQUFFLG1CQUFtQixJQUFJLHVCQUF1QixFQUFFLE1BQU0scURBQXFELENBQUM7QUFDckgsT0FBTyxFQUFFLG1CQUFtQixJQUFJLHdCQUF3QixFQUFFLE1BQU0sc0RBQXNELENBQUM7QUFDdkgsT0FBTyxFQUFFLG1CQUFtQixJQUFJLDBCQUEwQixFQUFFLE1BQU0sd0RBQXdELENBQUM7QUFDM0gsT0FBTyxFQUFFLG1CQUFtQixJQUFJLG9CQUFvQixFQUFFLE1BQU0saURBQWlELENBQUM7QUFDOUcsT0FBTyxFQUFFLG1CQUFtQixJQUFJLG9CQUFvQixFQUFFLE1BQU0saURBQWlELENBQUM7QUFDOUcsT0FBTyxFQUFFLG1CQUFtQixJQUFJLHNCQUFzQixFQUFFLE1BQU0sbURBQW1ELENBQUM7QUFDbEgsT0FBTyxFQUFFLG1CQUFtQixJQUFJLHVCQUF1QixFQUFFLE1BQU0sb0RBQW9ELENBQUM7QUFDcEgsT0FBTyxFQUFFLG1CQUFtQixJQUFJLHFCQUFxQixFQUFFLE1BQU0saURBQWlELENBQUM7QUFDL0csT0FBTyxFQUFFLG1CQUFtQixJQUFJLGVBQWUsRUFBRSxNQUFNLDJDQUEyQyxDQUFDO0FBQ25HLE9BQU8sRUFBRSxtQkFBbUIsSUFBSSxhQUFhLEVBQUUsTUFBTSx5Q0FBeUMsQ0FBQztBQUMvRixPQUFPLEVBQUUsbUJBQW1CLElBQUksb0JBQW9CLEVBQUUsTUFBTSxnREFBZ0QsQ0FBQztBQUM3RyxPQUFPLEVBQUUsbUJBQW1CLElBQUksa0NBQWtDLEVBQUUsTUFBTSxnRUFBZ0UsQ0FBQztBQUMzSSxPQUFPLEVBQUUsbUJBQW1CLElBQUksd0JBQXdCLEVBQUUsTUFBTSxxREFBcUQsQ0FBQztBQUN0SCxPQUFPLEVBQUUsbUJBQW1CLElBQUksNEJBQTRCLEVBQUUsTUFBTSx5REFBeUQsQ0FBQztBQUM5SCxPQUFPLEVBQUUsbUJBQW1CLElBQUksd0JBQXdCLEVBQUUsTUFBTSxxREFBcUQsQ0FBQztBQUN0SCxPQUFPLEVBQUUsbUJBQW1CLElBQUksZ0NBQWdDLEVBQUUsTUFBTSw4REFBOEQsQ0FBQztBQUN2SSxPQUFPLEVBQUUsbUJBQW1CLElBQUksZ0NBQWdDLEVBQUUsTUFBTSwrREFBK0QsQ0FBQztBQUN4SSxPQUFPLEVBQUUsbUJBQW1CLElBQUksZ0NBQWdDLEVBQUUsTUFBTSwrREFBK0QsQ0FBQztBQUN4SSxPQUFPLEVBQUUsbUJBQW1CLElBQUksa0NBQWtDLEVBQUUsTUFBTSxnRUFBZ0UsQ0FBQztBQUMzSSxPQUFPLEVBQUUsbUJBQW1CLElBQUksNEJBQTRCLEVBQUUsTUFBTSwwREFBMEQsQ0FBQztBQUMvSCxPQUFPLEVBQUUsbUJBQW1CLElBQUksMEJBQTBCLEVBQUUsTUFBTSx3REFBd0QsQ0FBQztBQUMzSCxPQUFPLEVBQUUsbUJBQW1CLElBQUksOEJBQThCLEVBQUUsTUFBTSw0REFBNEQsQ0FBQztBQUNuSSxPQUFPLEVBQUUsbUJBQW1CLElBQUksNEJBQTRCLEVBQUUsTUFBTSwwREFBMEQsQ0FBQzs7SUE4QmxILFVBQVUsU0FBVixVQUFVO0lBRXJCLFlBQVksQ0FBb0IsRUFBRSxDQUFhLEVBQVksQ0FBUztRQUFULE1BQUMsR0FBRCxDQUFDLENBQVE7UUFDbEUsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ1gsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDO1FBQzFCLFlBQVksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLEVBQUUsRUFBRSxDQUFDLFdBQVcsRUFBRSxXQUFXLEVBQUUsVUFBVSxDQUFDLENBQUMsQ0FBQztJQUN0RSxDQUFDO0NBQ0YsQ0FBQTt1R0FQWSxVQUFVOzJGQUFWLFVBQVUsMmRBSFgsMkJBQTJCO0FBRzFCLFVBQVU7SUFYdEIsUUFBUSxDQUFDO1FBQ1IscUJBQXFCLEVBQUUsZ0JBQWdCO1FBQ3ZDLE1BQU0sRUFBRSxDQUFDLGdCQUFnQixFQUFFLGdCQUFnQixFQUFFLGdCQUFnQixFQUFFLGFBQWEsRUFBRSxrQkFBa0IsRUFBRSxlQUFlLEVBQUUsYUFBYSxFQUFFLE9BQU8sRUFBRSxRQUFRLEVBQUUsYUFBYSxFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxRQUFRLEVBQUUsYUFBYSxDQUFDO1FBQ3RPLE9BQU8sRUFBRSxDQUFDLHFCQUFxQixDQUFDO0tBQ2pDLENBQUM7cUNBU2UsaUJBQWlCLEVBQUssVUFBVSxFQUFlLE1BQU07R0FGekQsVUFBVSxDQU90QjtTQVBZLFVBQVU7MkZBQVYsVUFBVTtrQkFOdEIsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUsYUFBYTtvQkFDdkIsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07b0JBQy9DLFFBQVEsRUFBRSwyQkFBMkI7b0JBQ3JDLE1BQU0sRUFBRSxDQUFDLGdCQUFnQixFQUFFLGdCQUFnQixFQUFFLGdCQUFnQixFQUFFLGFBQWEsRUFBRSxrQkFBa0IsRUFBRSxlQUFlLEVBQUUsYUFBYSxFQUFFLE9BQU8sRUFBRSxRQUFRLEVBQUUsYUFBYSxFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxRQUFRLEVBQUUsYUFBYSxDQUFDO2lCQUN2Tzs7SUF5Q1ksWUFBWSxTQUFaLFlBQVk7SUFFdkIsWUFBWSxDQUFvQixFQUFFLENBQWEsRUFBWSxDQUFTO1FBQVQsTUFBQyxHQUFELENBQUMsQ0FBUTtRQUNsRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDWCxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUM7UUFDMUIsWUFBWSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsRUFBRSxFQUFFLENBQUMsd0JBQXdCLEVBQUUsa0JBQWtCLEVBQUUsa0JBQWtCLEVBQUUsV0FBVyxDQUFDLENBQUMsQ0FBQztJQUMvRyxDQUFDO0NBQ0YsQ0FBQTt5R0FQWSxZQUFZOzZGQUFaLFlBQVksNlBBSGIsMkJBQTJCO0FBRzFCLFlBQVk7SUFWeEIsUUFBUSxDQUFDO1FBQ1IscUJBQXFCLEVBQUUsa0JBQWtCO1FBQ3pDLE1BQU0sRUFBRSxDQUFDLFVBQVUsRUFBRSxNQUFNLEVBQUUscUJBQXFCLEVBQUUsaUJBQWlCLEVBQUUsZ0JBQWdCLEVBQUUsa0JBQWtCLENBQUM7S0FDN0csQ0FBQztxQ0FTZSxpQkFBaUIsRUFBSyxVQUFVLEVBQWUsTUFBTTtHQUZ6RCxZQUFZLENBT3hCO1NBUFksWUFBWTsyRkFBWixZQUFZO2tCQU54QixTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSxlQUFlO29CQUN6QixlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtvQkFDL0MsUUFBUSxFQUFFLDJCQUEyQjtvQkFDckMsTUFBTSxFQUFFLENBQUMsVUFBVSxFQUFFLE1BQU0sRUFBRSxxQkFBcUIsRUFBRSxpQkFBaUIsRUFBRSxnQkFBZ0IsRUFBRSxrQkFBa0IsQ0FBQztpQkFDN0c7O0lBa0NZLG9CQUFvQixTQUFwQixvQkFBb0I7SUFFL0IsWUFBWSxDQUFvQixFQUFFLENBQWEsRUFBWSxDQUFTO1FBQVQsTUFBQyxHQUFELENBQUMsQ0FBUTtRQUNsRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDWCxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUM7UUFDMUIsWUFBWSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsRUFBRSxFQUFFLENBQUMsZ0JBQWdCLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO0lBQ3BFLENBQUM7Q0FDRixDQUFBO2lIQVBZLG9CQUFvQjtxR0FBcEIsb0JBQW9CLCtMQUhyQiwyQkFBMkI7QUFHMUIsb0JBQW9CO0lBWGhDLFFBQVEsQ0FBQztRQUNSLHFCQUFxQixFQUFFLDBCQUEwQjtRQUNqRCxNQUFNLEVBQUUsQ0FBQyxTQUFTLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxFQUFFLFdBQVcsRUFBRSxXQUFXLENBQUM7UUFDdkUsT0FBTyxFQUFFLENBQUMsa0JBQWtCLENBQUM7S0FDOUIsQ0FBQztxQ0FTZSxpQkFBaUIsRUFBSyxVQUFVLEVBQWUsTUFBTTtHQUZ6RCxvQkFBb0IsQ0FPaEM7U0FQWSxvQkFBb0I7MkZBQXBCLG9CQUFvQjtrQkFOaEMsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUseUJBQXlCO29CQUNuQyxlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtvQkFDL0MsUUFBUSxFQUFFLDJCQUEyQjtvQkFDckMsTUFBTSxFQUFFLENBQUMsU0FBUyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sRUFBRSxXQUFXLEVBQUUsV0FBVyxDQUFDO2lCQUN4RTs7SUF1QlksV0FBVyxTQUFYLFdBQVc7SUFFdEIsWUFBWSxDQUFvQixFQUFFLENBQWEsRUFBWSxDQUFTO1FBQVQsTUFBQyxHQUFELENBQUMsQ0FBUTtRQUNsRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDWCxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUM7SUFDNUIsQ0FBQztDQUNGLENBQUE7d0dBTlksV0FBVzs0RkFBWCxXQUFXLDJHQUhaLDJCQUEyQjtBQUcxQixXQUFXO0lBVnZCLFFBQVEsQ0FBQztRQUNSLHFCQUFxQixFQUFFLGlCQUFpQjtRQUN4QyxNQUFNLEVBQUUsQ0FBQyxVQUFVLEVBQUUsU0FBUyxDQUFDO0tBQ2hDLENBQUM7cUNBU2UsaUJBQWlCLEVBQUssVUFBVSxFQUFlLE1BQU07R0FGekQsV0FBVyxDQU12QjtTQU5ZLFdBQVc7MkZBQVgsV0FBVztrQkFOdkIsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUsZUFBZTtvQkFDekIsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07b0JBQy9DLFFBQVEsRUFBRSwyQkFBMkI7b0JBQ3JDLE1BQU0sRUFBRSxDQUFDLFVBQVUsRUFBRSxTQUFTLENBQUM7aUJBQ2hDOztJQW9CWSxZQUFZLFNBQVosWUFBWTtJQUV2QixZQUFZLENBQW9CLEVBQUUsQ0FBYSxFQUFZLENBQVM7UUFBVCxNQUFDLEdBQUQsQ0FBQyxDQUFRO1FBQ2xFLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNYLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQztJQUM1QixDQUFDO0NBQ0YsQ0FBQTt5R0FOWSxZQUFZOzZGQUFaLFlBQVksc0RBRmIsMkJBQTJCO0FBRTFCLFlBQVk7SUFSeEIsUUFBUSxDQUFDO1FBQ1IscUJBQXFCLEVBQUUsa0JBQWtCO0tBQzFDLENBQUM7cUNBUWUsaUJBQWlCLEVBQUssVUFBVSxFQUFlLE1BQU07R0FGekQsWUFBWSxDQU14QjtTQU5ZLFlBQVk7MkZBQVosWUFBWTtrQkFMeEIsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUsZ0JBQWdCO29CQUMxQixlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtvQkFDL0MsUUFBUSxFQUFFLDJCQUEyQjtpQkFDdEM7O0lBc0JZLFFBQVEsU0FBUixRQUFRO0lBRW5CLFlBQVksQ0FBb0IsRUFBRSxDQUFhLEVBQVksQ0FBUztRQUFULE1BQUMsR0FBRCxDQUFDLENBQVE7UUFDbEUsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ1gsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDO0lBQzVCLENBQUM7Q0FDRixDQUFBO3FHQU5ZLFFBQVE7eUZBQVIsUUFBUSx1SkFIVCwyQkFBMkI7QUFHMUIsUUFBUTtJQVZwQixRQUFRLENBQUM7UUFDUixxQkFBcUIsRUFBRSxjQUFjO1FBQ3JDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRSxRQUFRLEVBQUUsWUFBWSxFQUFFLGFBQWEsQ0FBQztLQUMxRCxDQUFDO3FDQVNlLGlCQUFpQixFQUFLLFVBQVUsRUFBZSxNQUFNO0dBRnpELFFBQVEsQ0FNcEI7U0FOWSxRQUFROzJGQUFSLFFBQVE7a0JBTnBCLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLFdBQVc7b0JBQ3JCLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxRQUFRLEVBQUUsMkJBQTJCO29CQUNyQyxNQUFNLEVBQUUsQ0FBQyxRQUFRLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxhQUFhLENBQUM7aUJBQzFEOztJQXNCWSxhQUFhLFNBQWIsYUFBYTtJQUV4QixZQUFZLENBQW9CLEVBQUUsQ0FBYSxFQUFZLENBQVM7UUFBVCxNQUFDLEdBQUQsQ0FBQyxDQUFRO1FBQ2xFLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNYLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQztJQUM1QixDQUFDO0NBQ0YsQ0FBQTswR0FOWSxhQUFhOzhGQUFiLGFBQWEsNkxBSGQsMkJBQTJCO0FBRzFCLGFBQWE7SUFWekIsUUFBUSxDQUFDO1FBQ1IscUJBQXFCLEVBQUUsbUJBQW1CO1FBQzFDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRSxlQUFlLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxhQUFhLENBQUM7S0FDM0UsQ0FBQztxQ0FTZSxpQkFBaUIsRUFBSyxVQUFVLEVBQWUsTUFBTTtHQUZ6RCxhQUFhLENBTXpCO1NBTlksYUFBYTsyRkFBYixhQUFhO2tCQU56QixTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSxpQkFBaUI7b0JBQzNCLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxRQUFRLEVBQUUsMkJBQTJCO29CQUNyQyxNQUFNLEVBQUUsQ0FBQyxRQUFRLEVBQUUsZUFBZSxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsYUFBYSxDQUFDO2lCQUMzRTs7SUFzQlksZUFBZSxTQUFmLGVBQWU7SUFFMUIsWUFBWSxDQUFvQixFQUFFLENBQWEsRUFBWSxDQUFTO1FBQVQsTUFBQyxHQUFELENBQUMsQ0FBUTtRQUNsRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDWCxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUM7SUFDNUIsQ0FBQztDQUNGLENBQUE7NEdBTlksZUFBZTtnR0FBZixlQUFlLHFRQUhoQiwyQkFBMkI7QUFHMUIsZUFBZTtJQVYzQixRQUFRLENBQUM7UUFDUixxQkFBcUIsRUFBRSxxQkFBcUI7UUFDNUMsTUFBTSxFQUFFLENBQUMsUUFBUSxFQUFFLHNCQUFzQixFQUFFLG9CQUFvQixFQUFFLFlBQVksRUFBRSxnQkFBZ0IsRUFBRSxZQUFZLENBQUM7S0FDL0csQ0FBQztxQ0FTZSxpQkFBaUIsRUFBSyxVQUFVLEVBQWUsTUFBTTtHQUZ6RCxlQUFlLENBTTNCO1NBTlksZUFBZTsyRkFBZixlQUFlO2tCQU4zQixTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSxtQkFBbUI7b0JBQzdCLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxRQUFRLEVBQUUsMkJBQTJCO29CQUNyQyxNQUFNLEVBQUUsQ0FBQyxRQUFRLEVBQUUsc0JBQXNCLEVBQUUsb0JBQW9CLEVBQUUsWUFBWSxFQUFFLGdCQUFnQixFQUFFLFlBQVksQ0FBQztpQkFDL0c7O0lBaUNZLFVBQVUsU0FBVixVQUFVO0lBRXJCLFlBQVksQ0FBb0IsRUFBRSxDQUFhLEVBQVksQ0FBUztRQUFULE1BQUMsR0FBRCxDQUFDLENBQVE7UUFDbEUsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ1gsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDO1FBQzFCLFlBQVksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLEVBQUUsRUFBRSxDQUFDLFVBQVUsRUFBRSxXQUFXLENBQUMsQ0FBQyxDQUFDO0lBQ3pELENBQUM7Q0FDRixDQUFBO3VHQVBZLFVBQVU7MkZBQVYsVUFBVSx5SEFIWCwyQkFBMkI7QUFHMUIsVUFBVTtJQVh0QixRQUFRLENBQUM7UUFDUixxQkFBcUIsRUFBRSxnQkFBZ0I7UUFDdkMsTUFBTSxFQUFFLENBQUMsbUJBQW1CLEVBQUUsUUFBUSxDQUFDO1FBQ3ZDLE9BQU8sRUFBRSxDQUFDLFdBQVcsRUFBRSxPQUFPLENBQUM7S0FDaEMsQ0FBQztxQ0FTZSxpQkFBaUIsRUFBSyxVQUFVLEVBQWUsTUFBTTtHQUZ6RCxVQUFVLENBT3RCO1NBUFksVUFBVTsyRkFBVixVQUFVO2tCQU50QixTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSxhQUFhO29CQUN2QixlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtvQkFDL0MsUUFBUSxFQUFFLDJCQUEyQjtvQkFDckMsTUFBTSxFQUFFLENBQUMsbUJBQW1CLEVBQUUsUUFBUSxDQUFDO2lCQUN4Qzs7SUE2QlksdUJBQXVCLFNBQXZCLHVCQUF1QjtJQUVsQyxZQUFZLENBQW9CLEVBQUUsQ0FBYSxFQUFZLENBQVM7UUFBVCxNQUFDLEdBQUQsQ0FBQyxDQUFRO1FBQ2xFLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNYLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQztRQUMxQixZQUFZLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxFQUFFLEVBQUUsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO0lBQzdDLENBQUM7Q0FDRixDQUFBO29IQVBZLHVCQUF1Qjt3R0FBdkIsdUJBQXVCLHVPQUh4QiwyQkFBMkI7QUFHMUIsdUJBQXVCO0lBVm5DLFFBQVEsQ0FBQztRQUNSLHFCQUFxQixFQUFFLDZCQUE2QjtRQUNwRCxNQUFNLEVBQUUsQ0FBQyxhQUFhLEVBQUUsWUFBWSxFQUFFLFdBQVcsRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLEVBQUUsYUFBYSxDQUFDO0tBQzVGLENBQUM7cUNBU2UsaUJBQWlCLEVBQUssVUFBVSxFQUFlLE1BQU07R0FGekQsdUJBQXVCLENBT25DO1NBUFksdUJBQXVCOzJGQUF2Qix1QkFBdUI7a0JBTm5DLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLDJCQUEyQjtvQkFDckMsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07b0JBQy9DLFFBQVEsRUFBRSwyQkFBMkI7b0JBQ3JDLE1BQU0sRUFBRSxDQUFDLGFBQWEsRUFBRSxZQUFZLEVBQUUsV0FBVyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sRUFBRSxhQUFhLENBQUM7aUJBQzVGOztJQXNEWSxnQkFBZ0IsU0FBaEIsZ0JBQWdCO0lBRTNCLFlBQVksQ0FBb0IsRUFBRSxDQUFhLEVBQVksQ0FBUztRQUFULE1BQUMsR0FBRCxDQUFDLENBQVE7UUFDbEUsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ1gsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDO1FBQzFCLFlBQVksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLEVBQUUsRUFBRSxDQUFDLFlBQVksRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFLGNBQWMsRUFBRSxXQUFXLEVBQUUsV0FBVyxFQUFFLGFBQWEsQ0FBQyxDQUFDLENBQUM7SUFDaEksQ0FBQztDQUNGLENBQUE7NkdBUFksZ0JBQWdCO2lHQUFoQixnQkFBZ0IsOGZBSGpCLDJCQUEyQjtBQUcxQixnQkFBZ0I7SUFYNUIsUUFBUSxDQUFDO1FBQ1IscUJBQXFCLEVBQUUsc0JBQXNCO1FBQzdDLE1BQU0sRUFBRSxDQUFDLG1CQUFtQixFQUFFLGFBQWEsRUFBRSxrQkFBa0IsRUFBRSxhQUFhLEVBQUUsV0FBVyxFQUFFLGVBQWUsRUFBRSxNQUFNLEVBQUUsaUJBQWlCLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsWUFBWSxFQUFFLGNBQWMsRUFBRSxTQUFTLEVBQUUsYUFBYSxFQUFFLFNBQVMsRUFBRSxPQUFPLENBQUM7UUFDclAsT0FBTyxFQUFFLENBQUMsMEJBQTBCLENBQUM7S0FDdEMsQ0FBQztxQ0FTZSxpQkFBaUIsRUFBSyxVQUFVLEVBQWUsTUFBTTtHQUZ6RCxnQkFBZ0IsQ0FPNUI7U0FQWSxnQkFBZ0I7MkZBQWhCLGdCQUFnQjtrQkFONUIsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUsb0JBQW9CO29CQUM5QixlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtvQkFDL0MsUUFBUSxFQUFFLDJCQUEyQjtvQkFDckMsTUFBTSxFQUFFLENBQUMsbUJBQW1CLEVBQUUsYUFBYSxFQUFFLGtCQUFrQixFQUFFLGFBQWEsRUFBRSxXQUFXLEVBQUUsZUFBZSxFQUFFLE1BQU0sRUFBRSxpQkFBaUIsRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsY0FBYyxFQUFFLFNBQVMsRUFBRSxhQUFhLEVBQUUsU0FBUyxFQUFFLE9BQU8sQ0FBQztpQkFDdFA7O0lBdUJZLGdCQUFnQixTQUFoQixnQkFBZ0I7SUFFM0IsWUFBWSxDQUFvQixFQUFFLENBQWEsRUFBWSxDQUFTO1FBQVQsTUFBQyxHQUFELENBQUMsQ0FBUTtRQUNsRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDWCxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUM7SUFDNUIsQ0FBQztDQUNGLENBQUE7NkdBTlksZ0JBQWdCO2lHQUFoQixnQkFBZ0IsOElBSGpCLDJCQUEyQjtBQUcxQixnQkFBZ0I7SUFWNUIsUUFBUSxDQUFDO1FBQ1IscUJBQXFCLEVBQUUsc0JBQXNCO1FBQzdDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLFFBQVEsQ0FBQztLQUNqRCxDQUFDO3FDQVNlLGlCQUFpQixFQUFLLFVBQVUsRUFBZSxNQUFNO0dBRnpELGdCQUFnQixDQU01QjtTQU5ZLGdCQUFnQjsyRkFBaEIsZ0JBQWdCO2tCQU41QixTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSxvQkFBb0I7b0JBQzlCLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxRQUFRLEVBQUUsMkJBQTJCO29CQUNyQyxNQUFNLEVBQUUsQ0FBQyxRQUFRLEVBQUUsTUFBTSxFQUFFLFVBQVUsRUFBRSxRQUFRLENBQUM7aUJBQ2pEOztJQXlDWSxrQkFBa0IsU0FBbEIsa0JBQWtCO0lBRTdCLFlBQVksQ0FBb0IsRUFBRSxDQUFhLEVBQVksQ0FBUztRQUFULE1BQUMsR0FBRCxDQUFDLENBQVE7UUFDbEUsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ1gsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDO1FBQzFCLFlBQVksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLEVBQUUsRUFBRSxDQUFDLGdCQUFnQixFQUFFLGdCQUFnQixFQUFFLGdCQUFnQixFQUFFLGVBQWUsQ0FBQyxDQUFDLENBQUM7SUFDdkcsQ0FBQztDQUNGLENBQUE7K0dBUFksa0JBQWtCO21HQUFsQixrQkFBa0IsK2pCQUhuQiwyQkFBMkI7QUFHMUIsa0JBQWtCO0lBWDlCLFFBQVEsQ0FBQztRQUNSLHFCQUFxQixFQUFFLHdCQUF3QjtRQUMvQyxNQUFNLEVBQUUsQ0FBQyxzQkFBc0IsRUFBRSwwQkFBMEIsRUFBRSxlQUFlLEVBQUUsYUFBYSxFQUFFLGdCQUFnQixFQUFFLGdCQUFnQixFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsb0JBQW9CLEVBQUUsWUFBWSxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsZ0JBQWdCLEVBQUUsaUJBQWlCLEVBQUUsY0FBYyxFQUFFLGFBQWEsQ0FBQztRQUNuUixPQUFPLEVBQUUsQ0FBQyxpQkFBaUIsRUFBRSxnQkFBZ0IsRUFBRSxnQkFBZ0IsRUFBRSwwQkFBMEIsQ0FBQztLQUM3RixDQUFDO3FDQVNlLGlCQUFpQixFQUFLLFVBQVUsRUFBZSxNQUFNO0dBRnpELGtCQUFrQixDQU85QjtTQVBZLGtCQUFrQjsyRkFBbEIsa0JBQWtCO2tCQU45QixTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSx1QkFBdUI7b0JBQ2pDLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxRQUFRLEVBQUUsMkJBQTJCO29CQUNyQyxNQUFNLEVBQUUsQ0FBQyxzQkFBc0IsRUFBRSwwQkFBMEIsRUFBRSxlQUFlLEVBQUUsYUFBYSxFQUFFLGdCQUFnQixFQUFFLGdCQUFnQixFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsb0JBQW9CLEVBQUUsWUFBWSxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsZ0JBQWdCLEVBQUUsaUJBQWlCLEVBQUUsY0FBYyxFQUFFLGFBQWEsQ0FBQztpQkFDcFI7O0lBNkNZLGNBQWMsU0FBZCxjQUFjO0lBRXpCLFlBQVksQ0FBb0IsRUFBRSxDQUFhLEVBQVksQ0FBUztRQUFULE1BQUMsR0FBRCxDQUFDLENBQVE7UUFDbEUsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ1gsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDO1FBQzFCLFlBQVksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLEVBQUUsRUFBRSxDQUFDLGNBQWMsRUFBRSxtQkFBbUIsRUFBRSxrQkFBa0IsRUFBRSxrQkFBa0IsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDO0lBQzNILENBQUM7Q0FDRixDQUFBOzJHQVBZLGNBQWM7K0ZBQWQsY0FBYyw0aUJBSGYsMkJBQTJCO0FBRzFCLGNBQWM7SUFWMUIsUUFBUSxDQUFDO1FBQ1IscUJBQXFCLEVBQUUsb0JBQW9CO1FBQzNDLE1BQU0sRUFBRSxDQUFDLGNBQWMsRUFBRSxhQUFhLEVBQUUsK0JBQStCLEVBQUUseUJBQXlCLEVBQUUsb0JBQW9CLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxFQUFFLFFBQVEsRUFBRSxtQkFBbUIsRUFBRSw0QkFBNEIsRUFBRSxvQkFBb0IsRUFBRSw2QkFBNkIsQ0FBQztLQUN6USxDQUFDO3FDQVNlLGlCQUFpQixFQUFLLFVBQVUsRUFBZSxNQUFNO0dBRnpELGNBQWMsQ0FPMUI7U0FQWSxjQUFjOzJGQUFkLGNBQWM7a0JBTjFCLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLGtCQUFrQjtvQkFDNUIsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07b0JBQy9DLFFBQVEsRUFBRSwyQkFBMkI7b0JBQ3JDLE1BQU0sRUFBRSxDQUFDLGNBQWMsRUFBRSxhQUFhLEVBQUUsK0JBQStCLEVBQUUseUJBQXlCLEVBQUUsb0JBQW9CLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxFQUFFLFFBQVEsRUFBRSxtQkFBbUIsRUFBRSw0QkFBNEIsRUFBRSxvQkFBb0IsRUFBRSw2QkFBNkIsQ0FBQztpQkFDelE7O0lBMERZLGFBQWEsU0FBYixhQUFhO0lBRXhCLFlBQVksQ0FBb0IsRUFBRSxDQUFhLEVBQVksQ0FBUztRQUFULE1BQUMsR0FBRCxDQUFDLENBQVE7UUFDbEUsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ1gsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDO1FBQzFCLFlBQVksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLEVBQUUsRUFBRSxDQUFDLFlBQVksRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFLFdBQVcsRUFBRSxjQUFjLEVBQUUsV0FBVyxFQUFFLFdBQVcsRUFBRSxhQUFhLENBQUMsQ0FBQyxDQUFDO0lBQzdJLENBQUM7Q0FDRixDQUFBOzBHQVBZLGFBQWE7OEZBQWIsYUFBYSw2NEJBSGQsMkJBQTJCO0FBRzFCLGFBQWE7SUFYekIsUUFBUSxDQUFDO1FBQ1IscUJBQXFCLEVBQUUsbUJBQW1CO1FBQzFDLE1BQU0sRUFBRSxDQUFDLHNCQUFzQixFQUFFLDBCQUEwQixFQUFFLHdCQUF3QixFQUFFLG9CQUFvQixFQUFFLG1CQUFtQixFQUFFLGtCQUFrQixFQUFFLGdCQUFnQixFQUFFLGFBQWEsRUFBRSxtQkFBbUIsRUFBRSxXQUFXLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxvQkFBb0IsRUFBRSxRQUFRLEVBQUUsT0FBTyxFQUFFLGFBQWEsRUFBRSxPQUFPLEVBQUUsYUFBYSxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsZ0JBQWdCLEVBQUUsUUFBUSxFQUFFLFFBQVEsRUFBRSxRQUFRLEVBQUUsY0FBYyxFQUFFLGtCQUFrQixFQUFFLFNBQVMsRUFBRSxhQUFhLEVBQUUsT0FBTyxDQUFDO1FBQzFjLE9BQU8sRUFBRSxDQUFDLDBCQUEwQixDQUFDO0tBQ3RDLENBQUM7cUNBU2UsaUJBQWlCLEVBQUssVUFBVSxFQUFlLE1BQU07R0FGekQsYUFBYSxDQU96QjtTQVBZLGFBQWE7MkZBQWIsYUFBYTtrQkFOekIsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUsaUJBQWlCO29CQUMzQixlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtvQkFDL0MsUUFBUSxFQUFFLDJCQUEyQjtvQkFDckMsTUFBTSxFQUFFLENBQUMsc0JBQXNCLEVBQUUsMEJBQTBCLEVBQUUsd0JBQXdCLEVBQUUsb0JBQW9CLEVBQUUsbUJBQW1CLEVBQUUsa0JBQWtCLEVBQUUsZ0JBQWdCLEVBQUUsYUFBYSxFQUFFLG1CQUFtQixFQUFFLFdBQVcsRUFBRSxNQUFNLEVBQUUsU0FBUyxFQUFFLG9CQUFvQixFQUFFLFFBQVEsRUFBRSxPQUFPLEVBQUUsYUFBYSxFQUFFLE9BQU8sRUFBRSxhQUFhLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxnQkFBZ0IsRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFFLFFBQVEsRUFBRSxjQUFjLEVBQUUsa0JBQWtCLEVBQUUsU0FBUyxFQUFFLGFBQWEsRUFBRSxPQUFPLENBQUM7aUJBQzNjOztJQXNFWSxtQkFBbUIsU0FBbkIsbUJBQW1CO0lBRTlCLFlBQVksQ0FBb0IsRUFBRSxDQUFhLEVBQVksQ0FBUztRQUFULE1BQUMsR0FBRCxDQUFDLENBQVE7UUFDbEUsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ1gsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDO1FBQzFCLFlBQVksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLEVBQUUsRUFBRSxDQUFDLGtCQUFrQixFQUFFLGlCQUFpQixFQUFFLGtCQUFrQixFQUFFLGlCQUFpQixFQUFFLFlBQVksRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFLFdBQVcsRUFBRSxjQUFjLEVBQUUsV0FBVyxFQUFFLFdBQVcsQ0FBQyxDQUFDLENBQUM7SUFDNU0sQ0FBQztDQUNGLENBQUE7Z0hBUFksbUJBQW1CO29HQUFuQixtQkFBbUIsOHJCQUhwQiwyQkFBMkI7QUFHMUIsbUJBQW1CO0lBWC9CLFFBQVEsQ0FBQztRQUNSLHFCQUFxQixFQUFFLHlCQUF5QjtRQUNoRCxNQUFNLEVBQUUsQ0FBQyx3QkFBd0IsRUFBRSxvQkFBb0IsRUFBRSxtQkFBbUIsRUFBRSxrQkFBa0IsRUFBRSxnQkFBZ0IsRUFBRSxhQUFhLEVBQUUsbUJBQW1CLEVBQUUsd0JBQXdCLEVBQUUsY0FBYyxFQUFFLGNBQWMsRUFBRSxtQkFBbUIsRUFBRSxjQUFjLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxvQkFBb0IsRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFFLFNBQVMsRUFBRSxhQUFhLEVBQUUsT0FBTyxDQUFDO1FBQ3RWLE9BQU8sRUFBRSxDQUFDLDBCQUEwQixDQUFDO0tBQ3RDLENBQUM7cUNBU2UsaUJBQWlCLEVBQUssVUFBVSxFQUFlLE1BQU07R0FGekQsbUJBQW1CLENBTy9CO1NBUFksbUJBQW1COzJGQUFuQixtQkFBbUI7a0JBTi9CLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLHdCQUF3QjtvQkFDbEMsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07b0JBQy9DLFFBQVEsRUFBRSwyQkFBMkI7b0JBQ3JDLE1BQU0sRUFBRSxDQUFDLHdCQUF3QixFQUFFLG9CQUFvQixFQUFFLG1CQUFtQixFQUFFLGtCQUFrQixFQUFFLGdCQUFnQixFQUFFLGFBQWEsRUFBRSxtQkFBbUIsRUFBRSx3QkFBd0IsRUFBRSxjQUFjLEVBQUUsY0FBYyxFQUFFLG1CQUFtQixFQUFFLGNBQWMsRUFBRSxNQUFNLEVBQUUsU0FBUyxFQUFFLG9CQUFvQixFQUFFLFFBQVEsRUFBRSxRQUFRLEVBQUUsU0FBUyxFQUFFLGFBQWEsRUFBRSxPQUFPLENBQUM7aUJBQ3ZWOztJQXVCWSxhQUFhLFNBQWIsYUFBYTtJQUV4QixZQUFZLENBQW9CLEVBQUUsQ0FBYSxFQUFZLENBQVM7UUFBVCxNQUFDLEdBQUQsQ0FBQyxDQUFRO1FBQ2xFLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNYLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQztJQUM1QixDQUFDO0NBQ0YsQ0FBQTswR0FOWSxhQUFhOzhGQUFiLGFBQWEscVRBSGQsMkJBQTJCO0FBRzFCLGFBQWE7SUFWekIsUUFBUSxDQUFDO1FBQ1IscUJBQXFCLEVBQUUsbUJBQW1CO1FBQzFDLE1BQU0sRUFBRSxDQUFDLHNCQUFzQixFQUFFLDBCQUEwQixFQUFFLGVBQWUsRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsZ0JBQWdCLENBQUM7S0FDMUksQ0FBQztxQ0FTZSxpQkFBaUIsRUFBSyxVQUFVLEVBQWUsTUFBTTtHQUZ6RCxhQUFhLENBTXpCO1NBTlksYUFBYTsyRkFBYixhQUFhO2tCQU56QixTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSxpQkFBaUI7b0JBQzNCLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxRQUFRLEVBQUUsMkJBQTJCO29CQUNyQyxNQUFNLEVBQUUsQ0FBQyxzQkFBc0IsRUFBRSwwQkFBMEIsRUFBRSxlQUFlLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLGdCQUFnQixDQUFDO2lCQUMxSTs7SUE0QlksbUJBQW1CLFNBQW5CLG1CQUFtQjtJQUU5QixZQUFZLENBQW9CLEVBQUUsQ0FBYSxFQUFZLENBQVM7UUFBVCxNQUFDLEdBQUQsQ0FBQyxDQUFRO1FBQ2xFLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNYLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQztRQUMxQixZQUFZLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxFQUFFLEVBQUUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO0lBQzNDLENBQUM7Q0FDRixDQUFBO2dIQVBZLG1CQUFtQjtvR0FBbkIsbUJBQW1CLDBKQUhwQiwyQkFBMkI7QUFHMUIsbUJBQW1CO0lBVi9CLFFBQVEsQ0FBQztRQUNSLHFCQUFxQixFQUFFLHlCQUF5QjtRQUNoRCxNQUFNLEVBQUUsQ0FBQyxXQUFXLEVBQUUsUUFBUSxFQUFFLFNBQVMsRUFBRSxRQUFRLENBQUM7S0FDckQsQ0FBQztxQ0FTZSxpQkFBaUIsRUFBSyxVQUFVLEVBQWUsTUFBTTtHQUZ6RCxtQkFBbUIsQ0FPL0I7U0FQWSxtQkFBbUI7MkZBQW5CLG1CQUFtQjtrQkFOL0IsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUsd0JBQXdCO29CQUNsQyxlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtvQkFDL0MsUUFBUSxFQUFFLDJCQUEyQjtvQkFDckMsTUFBTSxFQUFFLENBQUMsV0FBVyxFQUFFLFFBQVEsRUFBRSxTQUFTLEVBQUUsUUFBUSxDQUFDO2lCQUNyRDs7SUF1Qlksb0JBQW9CLFNBQXBCLG9CQUFvQjtJQUUvQixZQUFZLENBQW9CLEVBQUUsQ0FBYSxFQUFZLENBQVM7UUFBVCxNQUFDLEdBQUQsQ0FBQyxDQUFRO1FBQ2xFLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNYLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQztJQUM1QixDQUFDO0NBQ0YsQ0FBQTtpSEFOWSxvQkFBb0I7cUdBQXBCLG9CQUFvQix5SEFIckIsMkJBQTJCO0FBRzFCLG9CQUFvQjtJQVZoQyxRQUFRLENBQUM7UUFDUixxQkFBcUIsRUFBRSwwQkFBMEI7UUFDakQsTUFBTSxFQUFFLENBQUMsZUFBZSxFQUFFLE1BQU0sQ0FBQztLQUNsQyxDQUFDO3FDQVNlLGlCQUFpQixFQUFLLFVBQVUsRUFBZSxNQUFNO0dBRnpELG9CQUFvQixDQU1oQztTQU5ZLG9CQUFvQjsyRkFBcEIsb0JBQW9CO2tCQU5oQyxTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSx5QkFBeUI7b0JBQ25DLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxRQUFRLEVBQUUsMkJBQTJCO29CQUNyQyxNQUFNLEVBQUUsQ0FBQyxlQUFlLEVBQUUsTUFBTSxDQUFDO2lCQUNsQzs7SUFxRFksbUJBQW1CLFNBQW5CLG1CQUFtQjtJQUU5QixZQUFZLENBQW9CLEVBQUUsQ0FBYSxFQUFZLENBQVM7UUFBVCxNQUFDLEdBQUQsQ0FBQyxDQUFRO1FBQ2xFLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNYLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQztRQUMxQixZQUFZLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxFQUFFLEVBQUUsQ0FBQyxZQUFZLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFBRSxjQUFjLEVBQUUsV0FBVyxFQUFFLFdBQVcsRUFBRSxhQUFhLENBQUMsQ0FBQyxDQUFDO0lBQ2hJLENBQUM7Q0FDRixDQUFBO2dIQVBZLG1CQUFtQjtvR0FBbkIsbUJBQW1CLGlhQUhwQiwyQkFBMkI7QUFHMUIsbUJBQW1CO0lBWC9CLFFBQVEsQ0FBQztRQUNSLHFCQUFxQixFQUFFLHlCQUF5QjtRQUNoRCxNQUFNLEVBQUUsQ0FBQyxtQkFBbUIsRUFBRSxrQkFBa0IsRUFBRSxhQUFhLEVBQUUsV0FBVyxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsY0FBYyxFQUFFLFNBQVMsRUFBRSxhQUFhLEVBQUUsU0FBUyxFQUFFLE9BQU8sQ0FBQztRQUNsTSxPQUFPLEVBQUUsQ0FBQywwQkFBMEIsQ0FBQztLQUN0QyxDQUFDO3FDQVNlLGlCQUFpQixFQUFLLFVBQVUsRUFBZSxNQUFNO0dBRnpELG1CQUFtQixDQU8vQjtTQVBZLG1CQUFtQjsyRkFBbkIsbUJBQW1CO2tCQU4vQixTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSx1QkFBdUI7b0JBQ2pDLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxRQUFRLEVBQUUsMkJBQTJCO29CQUNyQyxNQUFNLEVBQUUsQ0FBQyxtQkFBbUIsRUFBRSxrQkFBa0IsRUFBRSxhQUFhLEVBQUUsV0FBVyxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsY0FBYyxFQUFFLFNBQVMsRUFBRSxhQUFhLEVBQUUsU0FBUyxFQUFFLE9BQU8sQ0FBQztpQkFDbk07O0lBNkJZLGtCQUFrQixTQUFsQixrQkFBa0I7SUFFN0IsWUFBWSxDQUFvQixFQUFFLENBQWEsRUFBWSxDQUFTO1FBQVQsTUFBQyxHQUFELENBQUMsQ0FBUTtRQUNsRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDWCxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUM7UUFDMUIsWUFBWSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsRUFBRSxFQUFFLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDO0lBQ25ELENBQUM7Q0FDRixDQUFBOytHQVBZLGtCQUFrQjttR0FBbEIsa0JBQWtCLDhIQUhuQiwyQkFBMkI7QUFHMUIsa0JBQWtCO0lBVjlCLFFBQVEsQ0FBQztRQUNSLHFCQUFxQixFQUFFLHdCQUF3QjtRQUMvQyxNQUFNLEVBQUUsQ0FBQyxRQUFRLEVBQUUsU0FBUyxFQUFFLE9BQU8sQ0FBQztLQUN2QyxDQUFDO3FDQVNlLGlCQUFpQixFQUFLLFVBQVUsRUFBZSxNQUFNO0dBRnpELGtCQUFrQixDQU85QjtTQVBZLGtCQUFrQjsyRkFBbEIsa0JBQWtCO2tCQU45QixTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSxzQkFBc0I7b0JBQ2hDLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxRQUFRLEVBQUUsMkJBQTJCO29CQUNyQyxNQUFNLEVBQUUsQ0FBQyxRQUFRLEVBQUUsU0FBUyxFQUFFLE9BQU8sQ0FBQztpQkFDdkM7O0lBOENZLGNBQWMsU0FBZCxjQUFjO0lBRXpCLFlBQVksQ0FBb0IsRUFBRSxDQUFhLEVBQVksQ0FBUztRQUFULE1BQUMsR0FBRCxDQUFDLENBQVE7UUFDbEUsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ1gsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDO1FBQzFCLFlBQVksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLEVBQUUsRUFBRSxDQUFDLFlBQVksRUFBRSxXQUFXLEVBQUUsY0FBYyxFQUFFLFVBQVUsRUFBRSxXQUFXLENBQUMsQ0FBQyxDQUFDO0lBQ3BHLENBQUM7Q0FDRixDQUFBOzJHQVBZLGNBQWM7K0ZBQWQsY0FBYyx3a0JBSGYsMkJBQTJCO0FBRzFCLGNBQWM7SUFYMUIsUUFBUSxDQUFDO1FBQ1IscUJBQXFCLEVBQUUsb0JBQW9CO1FBQzNDLE1BQU0sRUFBRSxDQUFDLHNCQUFzQixFQUFFLDBCQUEwQixFQUFFLGFBQWEsRUFBRSxlQUFlLEVBQUUscUJBQXFCLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxRQUFRLEVBQUUsZUFBZSxFQUFFLFlBQVksRUFBRSxnQkFBZ0IsRUFBRSxpQkFBaUIsRUFBRSxjQUFjLEVBQUUsa0JBQWtCLEVBQUUsU0FBUyxFQUFFLGFBQWEsRUFBRSxPQUFPLENBQUM7UUFDM1IsT0FBTyxFQUFFLENBQUMsMEJBQTBCLENBQUM7S0FDdEMsQ0FBQztxQ0FTZSxpQkFBaUIsRUFBSyxVQUFVLEVBQWUsTUFBTTtHQUZ6RCxjQUFjLENBTzFCO1NBUFksY0FBYzsyRkFBZCxjQUFjO2tCQU4xQixTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSxrQkFBa0I7b0JBQzVCLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxRQUFRLEVBQUUsMkJBQTJCO29CQUNyQyxNQUFNLEVBQUUsQ0FBQyxzQkFBc0IsRUFBRSwwQkFBMEIsRUFBRSxhQUFhLEVBQUUsZUFBZSxFQUFFLHFCQUFxQixFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsUUFBUSxFQUFFLGVBQWUsRUFBRSxZQUFZLEVBQUUsZ0JBQWdCLEVBQUUsaUJBQWlCLEVBQUUsY0FBYyxFQUFFLGtCQUFrQixFQUFFLFNBQVMsRUFBRSxhQUFhLEVBQUUsT0FBTyxDQUFDO2lCQUM1Ujs7SUEwRFksZ0JBQWdCLFNBQWhCLGdCQUFnQjtJQUUzQixZQUFZLENBQW9CLEVBQUUsQ0FBYSxFQUFZLENBQVM7UUFBVCxNQUFDLEdBQUQsQ0FBQyxDQUFRO1FBQ2xFLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNYLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQztRQUMxQixZQUFZLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxFQUFFLEVBQUUsQ0FBQyxZQUFZLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFBRSxXQUFXLEVBQUUsY0FBYyxFQUFFLFdBQVcsRUFBRSxXQUFXLEVBQUUsYUFBYSxDQUFDLENBQUMsQ0FBQztJQUM3SSxDQUFDO0NBQ0YsQ0FBQTs2R0FQWSxnQkFBZ0I7aUdBQWhCLGdCQUFnQiw4cUJBSGpCLDJCQUEyQjtBQUcxQixnQkFBZ0I7SUFYNUIsUUFBUSxDQUFDO1FBQ1IscUJBQXFCLEVBQUUsc0JBQXNCO1FBQzdDLE1BQU0sRUFBRSxDQUFDLHNCQUFzQixFQUFFLDBCQUEwQixFQUFFLHdCQUF3QixFQUFFLG1CQUFtQixFQUFFLGtCQUFrQixFQUFFLGFBQWEsRUFBRSxNQUFNLEVBQUUsU0FBUyxFQUFFLG9CQUFvQixFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxnQkFBZ0IsRUFBRSxRQUFRLEVBQUUsY0FBYyxFQUFFLGtCQUFrQixFQUFFLFNBQVMsRUFBRSxhQUFhLEVBQUUsT0FBTyxDQUFDO1FBQ2hWLE9BQU8sRUFBRSxDQUFDLDBCQUEwQixDQUFDO0tBQ3RDLENBQUM7cUNBU2UsaUJBQWlCLEVBQUssVUFBVSxFQUFlLE1BQU07R0FGekQsZ0JBQWdCLENBTzVCO1NBUFksZ0JBQWdCOzJGQUFoQixnQkFBZ0I7a0JBTjVCLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLG9CQUFvQjtvQkFDOUIsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07b0JBQy9DLFFBQVEsRUFBRSwyQkFBMkI7b0JBQ3JDLE1BQU0sRUFBRSxDQUFDLHNCQUFzQixFQUFFLDBCQUEwQixFQUFFLHdCQUF3QixFQUFFLG1CQUFtQixFQUFFLGtCQUFrQixFQUFFLGFBQWEsRUFBRSxNQUFNLEVBQUUsU0FBUyxFQUFFLG9CQUFvQixFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxnQkFBZ0IsRUFBRSxRQUFRLEVBQUUsY0FBYyxFQUFFLGtCQUFrQixFQUFFLFNBQVMsRUFBRSxhQUFhLEVBQUUsT0FBTyxDQUFDO2lCQUNqVjs7SUF1QlkseUJBQXlCLFNBQXpCLHlCQUF5QjtJQUVwQyxZQUFZLENBQW9CLEVBQUUsQ0FBYSxFQUFZLENBQVM7UUFBVCxNQUFDLEdBQUQsQ0FBQyxDQUFRO1FBQ2xFLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNYLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQztJQUM1QixDQUFDO0NBQ0YsQ0FBQTtzSEFOWSx5QkFBeUI7MEdBQXpCLHlCQUF5Qiw0R0FIMUIsMkJBQTJCO0FBRzFCLHlCQUF5QjtJQVZyQyxRQUFRLENBQUM7UUFDUixxQkFBcUIsRUFBRSwrQkFBK0I7UUFDdEQsTUFBTSxFQUFFLENBQUMsYUFBYSxDQUFDO0tBQ3hCLENBQUM7cUNBU2UsaUJBQWlCLEVBQUssVUFBVSxFQUFlLE1BQU07R0FGekQseUJBQXlCLENBTXJDO1NBTlkseUJBQXlCOzJGQUF6Qix5QkFBeUI7a0JBTnJDLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLDhCQUE4QjtvQkFDeEMsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07b0JBQy9DLFFBQVEsRUFBRSwyQkFBMkI7b0JBQ3JDLE1BQU0sRUFBRSxDQUFDLGFBQWEsQ0FBQztpQkFDeEI7O0lBc0JZLFFBQVEsU0FBUixRQUFRO0lBRW5CLFlBQVksQ0FBb0IsRUFBRSxDQUFhLEVBQVksQ0FBUztRQUFULE1BQUMsR0FBRCxDQUFDLENBQVE7UUFDbEUsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ1gsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDO0lBQzVCLENBQUM7Q0FDRixDQUFBO3FHQU5ZLFFBQVE7eUZBQVIsUUFBUSx5SkFIVCwyQkFBMkI7QUFHMUIsUUFBUTtJQVZwQixRQUFRLENBQUM7UUFDUixxQkFBcUIsRUFBRSxjQUFjO1FBQ3JDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRSxRQUFRLEVBQUUsaUJBQWlCLEVBQUUsU0FBUyxDQUFDO0tBQzNELENBQUM7cUNBU2UsaUJBQWlCLEVBQUssVUFBVSxFQUFlLE1BQU07R0FGekQsUUFBUSxDQU1wQjtTQU5ZLFFBQVE7MkZBQVIsUUFBUTtrQkFOcEIsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUsV0FBVztvQkFDckIsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07b0JBQy9DLFFBQVEsRUFBRSwyQkFBMkI7b0JBQ3JDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRSxRQUFRLEVBQUUsaUJBQWlCLEVBQUUsU0FBUyxDQUFDO2lCQUMzRDs7SUFzQlksWUFBWSxTQUFaLFlBQVk7SUFFdkIsWUFBWSxDQUFvQixFQUFFLENBQWEsRUFBWSxDQUFTO1FBQVQsTUFBQyxHQUFELENBQUMsQ0FBUTtRQUNsRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDWCxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUM7SUFDNUIsQ0FBQztDQUNGLENBQUE7eUdBTlksWUFBWTs2RkFBWixZQUFZLDRJQUhiLDJCQUEyQjtBQUcxQixZQUFZO0lBVnhCLFFBQVEsQ0FBQztRQUNSLHFCQUFxQixFQUFFLGtCQUFrQjtRQUN6QyxNQUFNLEVBQUUsQ0FBQyxRQUFRLEVBQUUsaUJBQWlCLEVBQUUsU0FBUyxDQUFDO0tBQ2pELENBQUM7cUNBU2UsaUJBQWlCLEVBQUssVUFBVSxFQUFlLE1BQU07R0FGekQsWUFBWSxDQU14QjtTQU5ZLFlBQVk7MkZBQVosWUFBWTtrQkFOeEIsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUsZ0JBQWdCO29CQUMxQixlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtvQkFDL0MsUUFBUSxFQUFFLDJCQUEyQjtvQkFDckMsTUFBTSxFQUFFLENBQUMsUUFBUSxFQUFFLGlCQUFpQixFQUFFLFNBQVMsQ0FBQztpQkFDakQ7O0lBc0JZLGFBQWEsU0FBYixhQUFhO0lBRXhCLFlBQVksQ0FBb0IsRUFBRSxDQUFhLEVBQVksQ0FBUztRQUFULE1BQUMsR0FBRCxDQUFDLENBQVE7UUFDbEUsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ1gsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDO0lBQzVCLENBQUM7Q0FDRixDQUFBOzBHQU5ZLGFBQWE7OEZBQWIsYUFBYSw2SUFIZCwyQkFBMkI7QUFHMUIsYUFBYTtJQVZ6QixRQUFRLENBQUM7UUFDUixxQkFBcUIsRUFBRSxtQkFBbUI7UUFDMUMsTUFBTSxFQUFFLENBQUMsUUFBUSxFQUFFLGlCQUFpQixFQUFFLFNBQVMsQ0FBQztLQUNqRCxDQUFDO3FDQVNlLGlCQUFpQixFQUFLLFVBQVUsRUFBZSxNQUFNO0dBRnpELGFBQWEsQ0FNekI7U0FOWSxhQUFhOzJGQUFiLGFBQWE7a0JBTnpCLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLGlCQUFpQjtvQkFDM0IsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07b0JBQy9DLFFBQVEsRUFBRSwyQkFBMkI7b0JBQ3JDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRSxpQkFBaUIsRUFBRSxTQUFTLENBQUM7aUJBQ2pEOztJQXNCWSxtQkFBbUIsU0FBbkIsbUJBQW1CO0lBRTlCLFlBQVksQ0FBb0IsRUFBRSxDQUFhLEVBQVksQ0FBUztRQUFULE1BQUMsR0FBRCxDQUFDLENBQVE7UUFDbEUsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ1gsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDO0lBQzVCLENBQUM7Q0FDRixDQUFBO2dIQU5ZLG1CQUFtQjtvR0FBbkIsbUJBQW1CLG9KQUhwQiwyQkFBMkI7QUFHMUIsbUJBQW1CO0lBVi9CLFFBQVEsQ0FBQztRQUNSLHFCQUFxQixFQUFFLHlCQUF5QjtRQUNoRCxNQUFNLEVBQUUsQ0FBQyxRQUFRLEVBQUUsaUJBQWlCLEVBQUUsU0FBUyxDQUFDO0tBQ2pELENBQUM7cUNBU2UsaUJBQWlCLEVBQUssVUFBVSxFQUFlLE1BQU07R0FGekQsbUJBQW1CLENBTS9CO1NBTlksbUJBQW1COzJGQUFuQixtQkFBbUI7a0JBTi9CLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLHdCQUF3QjtvQkFDbEMsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07b0JBQy9DLFFBQVEsRUFBRSwyQkFBMkI7b0JBQ3JDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRSxpQkFBaUIsRUFBRSxTQUFTLENBQUM7aUJBQ2pEOztJQXNCWSxzQkFBc0IsU0FBdEIsc0JBQXNCO0lBRWpDLFlBQVksQ0FBb0IsRUFBRSxDQUFhLEVBQVksQ0FBUztRQUFULE1BQUMsR0FBRCxDQUFDLENBQVE7UUFDbEUsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ1gsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDO0lBQzVCLENBQUM7Q0FDRixDQUFBO21IQU5ZLHNCQUFzQjt1R0FBdEIsc0JBQXNCLHdKQUh2QiwyQkFBMkI7QUFHMUIsc0JBQXNCO0lBVmxDLFFBQVEsQ0FBQztRQUNSLHFCQUFxQixFQUFFLDRCQUE0QjtRQUNuRCxNQUFNLEVBQUUsQ0FBQyxRQUFRLEVBQUUsaUJBQWlCLEVBQUUsU0FBUyxDQUFDO0tBQ2pELENBQUM7cUNBU2UsaUJBQWlCLEVBQUssVUFBVSxFQUFlLE1BQU07R0FGekQsc0JBQXNCLENBTWxDO1NBTlksc0JBQXNCOzJGQUF0QixzQkFBc0I7a0JBTmxDLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLDRCQUE0QjtvQkFDdEMsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07b0JBQy9DLFFBQVEsRUFBRSwyQkFBMkI7b0JBQ3JDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRSxpQkFBaUIsRUFBRSxTQUFTLENBQUM7aUJBQ2pEOztJQXNCWSx5QkFBeUIsU0FBekIseUJBQXlCO0lBRXBDLFlBQVksQ0FBb0IsRUFBRSxDQUFhLEVBQVksQ0FBUztRQUFULE1BQUMsR0FBRCxDQUFDLENBQVE7UUFDbEUsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ1gsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDO0lBQzVCLENBQUM7Q0FDRixDQUFBO3NIQU5ZLHlCQUF5QjswR0FBekIseUJBQXlCLDRKQUgxQiwyQkFBMkI7QUFHMUIseUJBQXlCO0lBVnJDLFFBQVEsQ0FBQztRQUNSLHFCQUFxQixFQUFFLCtCQUErQjtRQUN0RCxNQUFNLEVBQUUsQ0FBQyxRQUFRLEVBQUUsaUJBQWlCLEVBQUUsU0FBUyxDQUFDO0tBQ2pELENBQUM7cUNBU2UsaUJBQWlCLEVBQUssVUFBVSxFQUFlLE1BQU07R0FGekQseUJBQXlCLENBTXJDO1NBTlkseUJBQXlCOzJGQUF6Qix5QkFBeUI7a0JBTnJDLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLGdDQUFnQztvQkFDMUMsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07b0JBQy9DLFFBQVEsRUFBRSwyQkFBMkI7b0JBQ3JDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRSxpQkFBaUIsRUFBRSxTQUFTLENBQUM7aUJBQ2pEOztJQXNCWSxtQkFBbUIsU0FBbkIsbUJBQW1CO0lBRTlCLFlBQVksQ0FBb0IsRUFBRSxDQUFhLEVBQVksQ0FBUztRQUFULE1BQUMsR0FBRCxDQUFDLENBQVE7UUFDbEUsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ1gsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDO0lBQzVCLENBQUM7Q0FDRixDQUFBO2dIQU5ZLG1CQUFtQjtvR0FBbkIsbUJBQW1CLG9KQUhwQiwyQkFBMkI7QUFHMUIsbUJBQW1CO0lBVi9CLFFBQVEsQ0FBQztRQUNSLHFCQUFxQixFQUFFLHlCQUF5QjtRQUNoRCxNQUFNLEVBQUUsQ0FBQyxRQUFRLEVBQUUsaUJBQWlCLEVBQUUsU0FBUyxDQUFDO0tBQ2pELENBQUM7cUNBU2UsaUJBQWlCLEVBQUssVUFBVSxFQUFlLE1BQU07R0FGekQsbUJBQW1CLENBTS9CO1NBTlksbUJBQW1COzJGQUFuQixtQkFBbUI7a0JBTi9CLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLHdCQUF3QjtvQkFDbEMsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07b0JBQy9DLFFBQVEsRUFBRSwyQkFBMkI7b0JBQ3JDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRSxpQkFBaUIsRUFBRSxTQUFTLENBQUM7aUJBQ2pEOztJQXNCWSxtQkFBbUIsU0FBbkIsbUJBQW1CO0lBRTlCLFlBQVksQ0FBb0IsRUFBRSxDQUFhLEVBQVksQ0FBUztRQUFULE1BQUMsR0FBRCxDQUFDLENBQVE7UUFDbEUsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ1gsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDO0lBQzVCLENBQUM7Q0FDRixDQUFBO2dIQU5ZLG1CQUFtQjtvR0FBbkIsbUJBQW1CLG9KQUhwQiwyQkFBMkI7QUFHMUIsbUJBQW1CO0lBVi9CLFFBQVEsQ0FBQztRQUNSLHFCQUFxQixFQUFFLHlCQUF5QjtRQUNoRCxNQUFNLEVBQUUsQ0FBQyxRQUFRLEVBQUUsaUJBQWlCLEVBQUUsU0FBUyxDQUFDO0tBQ2pELENBQUM7cUNBU2UsaUJBQWlCLEVBQUssVUFBVSxFQUFlLE1BQU07R0FGekQsbUJBQW1CLENBTS9CO1NBTlksbUJBQW1COzJGQUFuQixtQkFBbUI7a0JBTi9CLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLHdCQUF3QjtvQkFDbEMsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07b0JBQy9DLFFBQVEsRUFBRSwyQkFBMkI7b0JBQ3JDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRSxpQkFBaUIsRUFBRSxTQUFTLENBQUM7aUJBQ2pEOztJQXNCWSxvQkFBb0IsU0FBcEIsb0JBQW9CO0lBRS9CLFlBQVksQ0FBb0IsRUFBRSxDQUFhLEVBQVksQ0FBUztRQUFULE1BQUMsR0FBRCxDQUFDLENBQVE7UUFDbEUsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ1gsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDO0lBQzVCLENBQUM7Q0FDRixDQUFBO2lIQU5ZLG9CQUFvQjtxR0FBcEIsb0JBQW9CLHFKQUhyQiwyQkFBMkI7QUFHMUIsb0JBQW9CO0lBVmhDLFFBQVEsQ0FBQztRQUNSLHFCQUFxQixFQUFFLDBCQUEwQjtRQUNqRCxNQUFNLEVBQUUsQ0FBQyxRQUFRLEVBQUUsaUJBQWlCLEVBQUUsU0FBUyxDQUFDO0tBQ2pELENBQUM7cUNBU2UsaUJBQWlCLEVBQUssVUFBVSxFQUFlLE1BQU07R0FGekQsb0JBQW9CLENBTWhDO1NBTlksb0JBQW9COzJGQUFwQixvQkFBb0I7a0JBTmhDLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLHlCQUF5QjtvQkFDbkMsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07b0JBQy9DLFFBQVEsRUFBRSwyQkFBMkI7b0JBQ3JDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRSxpQkFBaUIsRUFBRSxTQUFTLENBQUM7aUJBQ2pEOztJQXNCWSxpQkFBaUIsU0FBakIsaUJBQWlCO0lBRTVCLFlBQVksQ0FBb0IsRUFBRSxDQUFhLEVBQVksQ0FBUztRQUFULE1BQUMsR0FBRCxDQUFDLENBQVE7UUFDbEUsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ1gsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDO0lBQzVCLENBQUM7Q0FDRixDQUFBOzhHQU5ZLGlCQUFpQjtrR0FBakIsaUJBQWlCLGtKQUhsQiwyQkFBMkI7QUFHMUIsaUJBQWlCO0lBVjdCLFFBQVEsQ0FBQztRQUNSLHFCQUFxQixFQUFFLHVCQUF1QjtRQUM5QyxNQUFNLEVBQUUsQ0FBQyxRQUFRLEVBQUUsaUJBQWlCLEVBQUUsU0FBUyxDQUFDO0tBQ2pELENBQUM7cUNBU2UsaUJBQWlCLEVBQUssVUFBVSxFQUFlLE1BQU07R0FGekQsaUJBQWlCLENBTTdCO1NBTlksaUJBQWlCOzJGQUFqQixpQkFBaUI7a0JBTjdCLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLHNCQUFzQjtvQkFDaEMsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07b0JBQy9DLFFBQVEsRUFBRSwyQkFBMkI7b0JBQ3JDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRSxpQkFBaUIsRUFBRSxTQUFTLENBQUM7aUJBQ2pEOztJQXNCWSxZQUFZLFNBQVosWUFBWTtJQUV2QixZQUFZLENBQW9CLEVBQUUsQ0FBYSxFQUFZLENBQVM7UUFBVCxNQUFDLEdBQUQsQ0FBQyxDQUFRO1FBQ2xFLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNYLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQztJQUM1QixDQUFDO0NBQ0YsQ0FBQTt5R0FOWSxZQUFZOzZGQUFaLFlBQVksNElBSGIsMkJBQTJCO0FBRzFCLFlBQVk7SUFWeEIsUUFBUSxDQUFDO1FBQ1IscUJBQXFCLEVBQUUsa0JBQWtCO1FBQ3pDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRSxpQkFBaUIsRUFBRSxTQUFTLENBQUM7S0FDakQsQ0FBQztxQ0FTZSxpQkFBaUIsRUFBSyxVQUFVLEVBQWUsTUFBTTtHQUZ6RCxZQUFZLENBTXhCO1NBTlksWUFBWTsyRkFBWixZQUFZO2tCQU54QixTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSxnQkFBZ0I7b0JBQzFCLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxRQUFRLEVBQUUsMkJBQTJCO29CQUNyQyxNQUFNLEVBQUUsQ0FBQyxRQUFRLEVBQUUsaUJBQWlCLEVBQUUsU0FBUyxDQUFDO2lCQUNqRDs7SUFzQlkscUJBQXFCLFNBQXJCLHFCQUFxQjtJQUVoQyxZQUFZLENBQW9CLEVBQUUsQ0FBYSxFQUFZLENBQVM7UUFBVCxNQUFDLEdBQUQsQ0FBQyxDQUFRO1FBQ2xFLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNYLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQztJQUM1QixDQUFDO0NBQ0YsQ0FBQTtrSEFOWSxxQkFBcUI7c0dBQXJCLHFCQUFxQixzSkFIdEIsMkJBQTJCO0FBRzFCLHFCQUFxQjtJQVZqQyxRQUFRLENBQUM7UUFDUixxQkFBcUIsRUFBRSwyQkFBMkI7UUFDbEQsTUFBTSxFQUFFLENBQUMsUUFBUSxFQUFFLGlCQUFpQixFQUFFLFNBQVMsQ0FBQztLQUNqRCxDQUFDO3FDQVNlLGlCQUFpQixFQUFLLFVBQVUsRUFBZSxNQUFNO0dBRnpELHFCQUFxQixDQU1qQztTQU5ZLHFCQUFxQjsyRkFBckIscUJBQXFCO2tCQU5qQyxTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSwwQkFBMEI7b0JBQ3BDLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxRQUFRLEVBQUUsMkJBQTJCO29CQUNyQyxNQUFNLEVBQUUsQ0FBQyxRQUFRLEVBQUUsaUJBQWlCLEVBQUUsU0FBUyxDQUFDO2lCQUNqRDs7SUFzQlksZ0JBQWdCLFNBQWhCLGdCQUFnQjtJQUUzQixZQUFZLENBQW9CLEVBQUUsQ0FBYSxFQUFZLENBQVM7UUFBVCxNQUFDLEdBQUQsQ0FBQyxDQUFRO1FBQ2xFLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNYLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQztJQUM1QixDQUFDO0NBQ0YsQ0FBQTs2R0FOWSxnQkFBZ0I7aUdBQWhCLGdCQUFnQixnSkFIakIsMkJBQTJCO0FBRzFCLGdCQUFnQjtJQVY1QixRQUFRLENBQUM7UUFDUixxQkFBcUIsRUFBRSxzQkFBc0I7UUFDN0MsTUFBTSxFQUFFLENBQUMsUUFBUSxFQUFFLGlCQUFpQixFQUFFLFNBQVMsQ0FBQztLQUNqRCxDQUFDO3FDQVNlLGlCQUFpQixFQUFLLFVBQVUsRUFBZSxNQUFNO0dBRnpELGdCQUFnQixDQU01QjtTQU5ZLGdCQUFnQjsyRkFBaEIsZ0JBQWdCO2tCQU41QixTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSxvQkFBb0I7b0JBQzlCLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxRQUFRLEVBQUUsMkJBQTJCO29CQUNyQyxNQUFNLEVBQUUsQ0FBQyxRQUFRLEVBQUUsaUJBQWlCLEVBQUUsU0FBUyxDQUFDO2lCQUNqRDs7SUFzQlkseUJBQXlCLFNBQXpCLHlCQUF5QjtJQUVwQyxZQUFZLENBQW9CLEVBQUUsQ0FBYSxFQUFZLENBQVM7UUFBVCxNQUFDLEdBQUQsQ0FBQyxDQUFRO1FBQ2xFLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNYLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQztJQUM1QixDQUFDO0NBQ0YsQ0FBQTtzSEFOWSx5QkFBeUI7MEdBQXpCLHlCQUF5QiwwSkFIMUIsMkJBQTJCO0FBRzFCLHlCQUF5QjtJQVZyQyxRQUFRLENBQUM7UUFDUixxQkFBcUIsRUFBRSwrQkFBK0I7UUFDdEQsTUFBTSxFQUFFLENBQUMsUUFBUSxFQUFFLGlCQUFpQixFQUFFLFNBQVMsQ0FBQztLQUNqRCxDQUFDO3FDQVNlLGlCQUFpQixFQUFLLFVBQVUsRUFBZSxNQUFNO0dBRnpELHlCQUF5QixDQU1yQztTQU5ZLHlCQUF5QjsyRkFBekIseUJBQXlCO2tCQU5yQyxTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSw4QkFBOEI7b0JBQ3hDLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxRQUFRLEVBQUUsMkJBQTJCO29CQUNyQyxNQUFNLEVBQUUsQ0FBQyxRQUFRLEVBQUUsaUJBQWlCLEVBQUUsU0FBUyxDQUFDO2lCQUNqRDs7SUFzQlksK0JBQStCLFNBQS9CLCtCQUErQjtJQUUxQyxZQUFZLENBQW9CLEVBQUUsQ0FBYSxFQUFZLENBQVM7UUFBVCxNQUFDLEdBQUQsQ0FBQyxDQUFRO1FBQ2xFLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNYLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQztJQUM1QixDQUFDO0NBQ0YsQ0FBQTs0SEFOWSwrQkFBK0I7Z0hBQS9CLCtCQUErQixpS0FIaEMsMkJBQTJCO0FBRzFCLCtCQUErQjtJQVYzQyxRQUFRLENBQUM7UUFDUixxQkFBcUIsRUFBRSxxQ0FBcUM7UUFDNUQsTUFBTSxFQUFFLENBQUMsUUFBUSxFQUFFLGlCQUFpQixFQUFFLFNBQVMsQ0FBQztLQUNqRCxDQUFDO3FDQVNlLGlCQUFpQixFQUFLLFVBQVUsRUFBZSxNQUFNO0dBRnpELCtCQUErQixDQU0zQztTQU5ZLCtCQUErQjsyRkFBL0IsK0JBQStCO2tCQU4zQyxTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSxxQ0FBcUM7b0JBQy9DLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxRQUFRLEVBQUUsMkJBQTJCO29CQUNyQyxNQUFNLEVBQUUsQ0FBQyxRQUFRLEVBQUUsaUJBQWlCLEVBQUUsU0FBUyxDQUFDO2lCQUNqRDs7SUFzQlksMkJBQTJCLFNBQTNCLDJCQUEyQjtJQUV0QyxZQUFZLENBQW9CLEVBQUUsQ0FBYSxFQUFZLENBQVM7UUFBVCxNQUFDLEdBQUQsQ0FBQyxDQUFRO1FBQ2xFLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNYLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQztJQUM1QixDQUFDO0NBQ0YsQ0FBQTt3SEFOWSwyQkFBMkI7NEdBQTNCLDJCQUEyQiw0SkFINUIsMkJBQTJCO0FBRzFCLDJCQUEyQjtJQVZ2QyxRQUFRLENBQUM7UUFDUixxQkFBcUIsRUFBRSxpQ0FBaUM7UUFDeEQsTUFBTSxFQUFFLENBQUMsUUFBUSxFQUFFLGlCQUFpQixFQUFFLFNBQVMsQ0FBQztLQUNqRCxDQUFDO3FDQVNlLGlCQUFpQixFQUFLLFVBQVUsRUFBZSxNQUFNO0dBRnpELDJCQUEyQixDQU12QztTQU5ZLDJCQUEyQjsyRkFBM0IsMkJBQTJCO2tCQU52QyxTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSxnQ0FBZ0M7b0JBQzFDLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxRQUFRLEVBQUUsMkJBQTJCO29CQUNyQyxNQUFNLEVBQUUsQ0FBQyxRQUFRLEVBQUUsaUJBQWlCLEVBQUUsU0FBUyxDQUFDO2lCQUNqRDs7SUFzQlksa0NBQWtDLFNBQWxDLGtDQUFrQztJQUU3QyxZQUFZLENBQW9CLEVBQUUsQ0FBYSxFQUFZLENBQVM7UUFBVCxNQUFDLEdBQUQsQ0FBQyxDQUFRO1FBQ2xFLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNYLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQztJQUM1QixDQUFDO0NBQ0YsQ0FBQTsrSEFOWSxrQ0FBa0M7bUhBQWxDLGtDQUFrQyxvS0FIbkMsMkJBQTJCO0FBRzFCLGtDQUFrQztJQVY5QyxRQUFRLENBQUM7UUFDUixxQkFBcUIsRUFBRSx3Q0FBd0M7UUFDL0QsTUFBTSxFQUFFLENBQUMsUUFBUSxFQUFFLGlCQUFpQixFQUFFLFNBQVMsQ0FBQztLQUNqRCxDQUFDO3FDQVNlLGlCQUFpQixFQUFLLFVBQVUsRUFBZSxNQUFNO0dBRnpELGtDQUFrQyxDQU05QztTQU5ZLGtDQUFrQzsyRkFBbEMsa0NBQWtDO2tCQU45QyxTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSx3Q0FBd0M7b0JBQ2xELGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxRQUFRLEVBQUUsMkJBQTJCO29CQUNyQyxNQUFNLEVBQUUsQ0FBQyxRQUFRLEVBQUUsaUJBQWlCLEVBQUUsU0FBUyxDQUFDO2lCQUNqRDs7SUFzQlksdUJBQXVCLFNBQXZCLHVCQUF1QjtJQUVsQyxZQUFZLENBQW9CLEVBQUUsQ0FBYSxFQUFZLENBQVM7UUFBVCxNQUFDLEdBQUQsQ0FBQyxDQUFRO1FBQ2xFLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNYLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQztJQUM1QixDQUFDO0NBQ0YsQ0FBQTtvSEFOWSx1QkFBdUI7d0dBQXZCLHVCQUF1Qix5SkFIeEIsMkJBQTJCO0FBRzFCLHVCQUF1QjtJQVZuQyxRQUFRLENBQUM7UUFDUixxQkFBcUIsRUFBRSw2QkFBNkI7UUFDcEQsTUFBTSxFQUFFLENBQUMsUUFBUSxFQUFFLGlCQUFpQixFQUFFLFNBQVMsQ0FBQztLQUNqRCxDQUFDO3FDQVNlLGlCQUFpQixFQUFLLFVBQVUsRUFBZSxNQUFNO0dBRnpELHVCQUF1QixDQU1uQztTQU5ZLHVCQUF1QjsyRkFBdkIsdUJBQXVCO2tCQU5uQyxTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSw2QkFBNkI7b0JBQ3ZDLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxRQUFRLEVBQUUsMkJBQTJCO29CQUNyQyxNQUFNLEVBQUUsQ0FBQyxRQUFRLEVBQUUsaUJBQWlCLEVBQUUsU0FBUyxDQUFDO2lCQUNqRDs7SUFzQlksYUFBYSxTQUFiLGFBQWE7SUFFeEIsWUFBWSxDQUFvQixFQUFFLENBQWEsRUFBWSxDQUFTO1FBQVQsTUFBQyxHQUFELENBQUMsQ0FBUTtRQUNsRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDWCxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUM7SUFDNUIsQ0FBQztDQUNGLENBQUE7MEdBTlksYUFBYTs4RkFBYixhQUFhLDZJQUhkLDJCQUEyQjtBQUcxQixhQUFhO0lBVnpCLFFBQVEsQ0FBQztRQUNSLHFCQUFxQixFQUFFLG1CQUFtQjtRQUMxQyxNQUFNLEVBQUUsQ0FBQyxRQUFRLEVBQUUsaUJBQWlCLEVBQUUsU0FBUyxDQUFDO0tBQ2pELENBQUM7cUNBU2UsaUJBQWlCLEVBQUssVUFBVSxFQUFlLE1BQU07R0FGekQsYUFBYSxDQU16QjtTQU5ZLGFBQWE7MkZBQWIsYUFBYTtrQkFOekIsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUsaUJBQWlCO29CQUMzQixlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtvQkFDL0MsUUFBUSxFQUFFLDJCQUEyQjtvQkFDckMsTUFBTSxFQUFFLENBQUMsUUFBUSxFQUFFLGlCQUFpQixFQUFFLFNBQVMsQ0FBQztpQkFDakQ7O0lBc0JZLGlCQUFpQixTQUFqQixpQkFBaUI7SUFFNUIsWUFBWSxDQUFvQixFQUFFLENBQWEsRUFBWSxDQUFTO1FBQVQsTUFBQyxHQUFELENBQUMsQ0FBUTtRQUNsRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDWCxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUM7SUFDNUIsQ0FBQztDQUNGLENBQUE7OEdBTlksaUJBQWlCO2tHQUFqQixpQkFBaUIsaUpBSGxCLDJCQUEyQjtBQUcxQixpQkFBaUI7SUFWN0IsUUFBUSxDQUFDO1FBQ1IscUJBQXFCLEVBQUUsdUJBQXVCO1FBQzlDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRSxpQkFBaUIsRUFBRSxTQUFTLENBQUM7S0FDakQsQ0FBQztxQ0FTZSxpQkFBaUIsRUFBSyxVQUFVLEVBQWUsTUFBTTtHQUZ6RCxpQkFBaUIsQ0FNN0I7U0FOWSxpQkFBaUI7MkZBQWpCLGlCQUFpQjtrQkFON0IsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUscUJBQXFCO29CQUMvQixlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtvQkFDL0MsUUFBUSxFQUFFLDJCQUEyQjtvQkFDckMsTUFBTSxFQUFFLENBQUMsUUFBUSxFQUFFLGlCQUFpQixFQUFFLFNBQVMsQ0FBQztpQkFDakQ7O0lBc0JZLFlBQVksU0FBWixZQUFZO0lBRXZCLFlBQVksQ0FBb0IsRUFBRSxDQUFhLEVBQVksQ0FBUztRQUFULE1BQUMsR0FBRCxDQUFDLENBQVE7UUFDbEUsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ1gsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDO0lBQzVCLENBQUM7Q0FDRixDQUFBO3lHQU5ZLFlBQVk7NkZBQVosWUFBWSw0SUFIYiwyQkFBMkI7QUFHMUIsWUFBWTtJQVZ4QixRQUFRLENBQUM7UUFDUixxQkFBcUIsRUFBRSxrQkFBa0I7UUFDekMsTUFBTSxFQUFFLENBQUMsUUFBUSxFQUFFLGlCQUFpQixFQUFFLFNBQVMsQ0FBQztLQUNqRCxDQUFDO3FDQVNlLGlCQUFpQixFQUFLLFVBQVUsRUFBZSxNQUFNO0dBRnpELFlBQVksQ0FNeEI7U0FOWSxZQUFZOzJGQUFaLFlBQVk7a0JBTnhCLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLGdCQUFnQjtvQkFDMUIsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07b0JBQy9DLFFBQVEsRUFBRSwyQkFBMkI7b0JBQ3JDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRSxpQkFBaUIsRUFBRSxTQUFTLENBQUM7aUJBQ2pEOztJQXNCWSxjQUFjLFNBQWQsY0FBYztJQUV6QixZQUFZLENBQW9CLEVBQUUsQ0FBYSxFQUFZLENBQVM7UUFBVCxNQUFDLEdBQUQsQ0FBQyxDQUFRO1FBQ2xFLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNYLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQztJQUM1QixDQUFDO0NBQ0YsQ0FBQTsyR0FOWSxjQUFjOytGQUFkLGNBQWMsOElBSGYsMkJBQTJCO0FBRzFCLGNBQWM7SUFWMUIsUUFBUSxDQUFDO1FBQ1IscUJBQXFCLEVBQUUsb0JBQW9CO1FBQzNDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRSxpQkFBaUIsRUFBRSxTQUFTLENBQUM7S0FDakQsQ0FBQztxQ0FTZSxpQkFBaUIsRUFBSyxVQUFVLEVBQWUsTUFBTTtHQUZ6RCxjQUFjLENBTTFCO1NBTlksY0FBYzsyRkFBZCxjQUFjO2tCQU4xQixTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSxrQkFBa0I7b0JBQzVCLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxRQUFRLEVBQUUsMkJBQTJCO29CQUNyQyxNQUFNLEVBQUUsQ0FBQyxRQUFRLEVBQUUsaUJBQWlCLEVBQUUsU0FBUyxDQUFDO2lCQUNqRDs7SUFzQlksZUFBZSxTQUFmLGVBQWU7SUFFMUIsWUFBWSxDQUFvQixFQUFFLENBQWEsRUFBWSxDQUFTO1FBQVQsTUFBQyxHQUFELENBQUMsQ0FBUTtRQUNsRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDWCxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUM7SUFDNUIsQ0FBQztDQUNGLENBQUE7NEdBTlksZUFBZTtnR0FBZixlQUFlLCtJQUhoQiwyQkFBMkI7QUFHMUIsZUFBZTtJQVYzQixRQUFRLENBQUM7UUFDUixxQkFBcUIsRUFBRSxxQkFBcUI7UUFDNUMsTUFBTSxFQUFFLENBQUMsUUFBUSxFQUFFLGlCQUFpQixFQUFFLFNBQVMsQ0FBQztLQUNqRCxDQUFDO3FDQVNlLGlCQUFpQixFQUFLLFVBQVUsRUFBZSxNQUFNO0dBRnpELGVBQWUsQ0FNM0I7U0FOWSxlQUFlOzJGQUFmLGVBQWU7a0JBTjNCLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLG1CQUFtQjtvQkFDN0IsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07b0JBQy9DLFFBQVEsRUFBRSwyQkFBMkI7b0JBQ3JDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRSxpQkFBaUIsRUFBRSxTQUFTLENBQUM7aUJBQ2pEOztJQXNCWSxhQUFhLFNBQWIsYUFBYTtJQUV4QixZQUFZLENBQW9CLEVBQUUsQ0FBYSxFQUFZLENBQVM7UUFBVCxNQUFDLEdBQUQsQ0FBQyxDQUFRO1FBQ2xFLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNYLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQztJQUM1QixDQUFDO0NBQ0YsQ0FBQTswR0FOWSxhQUFhOzhGQUFiLGFBQWEsNklBSGQsMkJBQTJCO0FBRzFCLGFBQWE7SUFWekIsUUFBUSxDQUFDO1FBQ1IscUJBQXFCLEVBQUUsbUJBQW1CO1FBQzFDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRSxpQkFBaUIsRUFBRSxTQUFTLENBQUM7S0FDakQsQ0FBQztxQ0FTZSxpQkFBaUIsRUFBSyxVQUFVLEVBQWUsTUFBTTtHQUZ6RCxhQUFhLENBTXpCO1NBTlksYUFBYTsyRkFBYixhQUFhO2tCQU56QixTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSxpQkFBaUI7b0JBQzNCLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxRQUFRLEVBQUUsMkJBQTJCO29CQUNyQyxNQUFNLEVBQUUsQ0FBQyxRQUFRLEVBQUUsaUJBQWlCLEVBQUUsU0FBUyxDQUFDO2lCQUNqRDs7SUFzQlksU0FBUyxTQUFULFNBQVM7SUFFcEIsWUFBWSxDQUFvQixFQUFFLENBQWEsRUFBWSxDQUFTO1FBQVQsTUFBQyxHQUFELENBQUMsQ0FBUTtRQUNsRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDWCxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUM7SUFDNUIsQ0FBQztDQUNGLENBQUE7c0dBTlksU0FBUzswRkFBVCxTQUFTLHlJQUhWLDJCQUEyQjtBQUcxQixTQUFTO0lBVnJCLFFBQVEsQ0FBQztRQUNSLHFCQUFxQixFQUFFLGVBQWU7UUFDdEMsTUFBTSxFQUFFLENBQUMsUUFBUSxFQUFFLGlCQUFpQixFQUFFLFNBQVMsQ0FBQztLQUNqRCxDQUFDO3FDQVNlLGlCQUFpQixFQUFLLFVBQVUsRUFBZSxNQUFNO0dBRnpELFNBQVMsQ0FNckI7U0FOWSxTQUFTOzJGQUFULFNBQVM7a0JBTnJCLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLGFBQWE7b0JBQ3ZCLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxRQUFRLEVBQUUsMkJBQTJCO29CQUNyQyxNQUFNLEVBQUUsQ0FBQyxRQUFRLEVBQUUsaUJBQWlCLEVBQUUsU0FBUyxDQUFDO2lCQUNqRDs7SUFzQlksZUFBZSxTQUFmLGVBQWU7SUFFMUIsWUFBWSxDQUFvQixFQUFFLENBQWEsRUFBWSxDQUFTO1FBQVQsTUFBQyxHQUFELENBQUMsQ0FBUTtRQUNsRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDWCxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUM7SUFDNUIsQ0FBQztDQUNGLENBQUE7NEdBTlksZUFBZTtnR0FBZixlQUFlLG1PQUhoQiwyQkFBMkI7QUFHMUIsZUFBZTtJQVYzQixRQUFRLENBQUM7UUFDUixxQkFBcUIsRUFBRSxxQkFBcUI7UUFDNUMsTUFBTSxFQUFFLENBQUMsYUFBYSxFQUFFLGdCQUFnQixFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsbUJBQW1CLENBQUM7S0FDN0YsQ0FBQztxQ0FTZSxpQkFBaUIsRUFBSyxVQUFVLEVBQWUsTUFBTTtHQUZ6RCxlQUFlLENBTTNCO1NBTlksZUFBZTsyRkFBZixlQUFlO2tCQU4zQixTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSxtQkFBbUI7b0JBQzdCLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxRQUFRLEVBQUUsMkJBQTJCO29CQUNyQyxNQUFNLEVBQUUsQ0FBQyxhQUFhLEVBQUUsZ0JBQWdCLEVBQUUsYUFBYSxFQUFFLGFBQWEsRUFBRSxtQkFBbUIsQ0FBQztpQkFDN0Y7O0lBc0JZLGlCQUFpQixTQUFqQixpQkFBaUI7SUFFNUIsWUFBWSxDQUFvQixFQUFFLENBQWEsRUFBWSxDQUFTO1FBQVQsTUFBQyxHQUFELENBQUMsQ0FBUTtRQUNsRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDWCxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUM7SUFDNUIsQ0FBQztDQUNGLENBQUE7OEdBTlksaUJBQWlCO2tHQUFqQixpQkFBaUIsMkhBSGxCLDJCQUEyQjtBQUcxQixpQkFBaUI7SUFWN0IsUUFBUSxDQUFDO1FBQ1IscUJBQXFCLEVBQUUsdUJBQXVCO1FBQzlDLE1BQU0sRUFBRSxDQUFDLFdBQVcsRUFBRSxhQUFhLENBQUM7S0FDckMsQ0FBQztxQ0FTZSxpQkFBaUIsRUFBSyxVQUFVLEVBQWUsTUFBTTtHQUZ6RCxpQkFBaUIsQ0FNN0I7U0FOWSxpQkFBaUI7MkZBQWpCLGlCQUFpQjtrQkFON0IsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUscUJBQXFCO29CQUMvQixlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtvQkFDL0MsUUFBUSxFQUFFLDJCQUEyQjtvQkFDckMsTUFBTSxFQUFFLENBQUMsV0FBVyxFQUFFLGFBQWEsQ0FBQztpQkFDckM7O0lBc0JZLG1CQUFtQixTQUFuQixtQkFBbUI7SUFFOUIsWUFBWSxDQUFvQixFQUFFLENBQWEsRUFBWSxDQUFTO1FBQVQsTUFBQyxHQUFELENBQUMsQ0FBUTtRQUNsRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDWCxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUM7SUFDNUIsQ0FBQztDQUNGLENBQUE7Z0hBTlksbUJBQW1CO29HQUFuQixtQkFBbUIscU9BSHBCLDJCQUEyQjtBQUcxQixtQkFBbUI7SUFWL0IsUUFBUSxDQUFDO1FBQ1IscUJBQXFCLEVBQUUseUJBQXlCO1FBQ2hELE1BQU0sRUFBRSxDQUFDLGdCQUFnQixFQUFFLGFBQWEsRUFBRSxZQUFZLEVBQUUsYUFBYSxFQUFFLG1CQUFtQixDQUFDO0tBQzVGLENBQUM7cUNBU2UsaUJBQWlCLEVBQUssVUFBVSxFQUFlLE1BQU07R0FGekQsbUJBQW1CLENBTS9CO1NBTlksbUJBQW1COzJGQUFuQixtQkFBbUI7a0JBTi9CLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLHVCQUF1QjtvQkFDakMsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07b0JBQy9DLFFBQVEsRUFBRSwyQkFBMkI7b0JBQ3JDLE1BQU0sRUFBRSxDQUFDLGdCQUFnQixFQUFFLGFBQWEsRUFBRSxZQUFZLEVBQUUsYUFBYSxFQUFFLG1CQUFtQixDQUFDO2lCQUM1Rjs7SUFzQlksY0FBYyxTQUFkLGNBQWM7SUFFekIsWUFBWSxDQUFvQixFQUFFLENBQWEsRUFBWSxDQUFTO1FBQVQsTUFBQyxHQUFELENBQUMsQ0FBUTtRQUNsRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDWCxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUM7SUFDNUIsQ0FBQztDQUNGLENBQUE7MkdBTlksY0FBYzsrRkFBZCxjQUFjLDRHQUhmLDJCQUEyQjtBQUcxQixjQUFjO0lBVjFCLFFBQVEsQ0FBQztRQUNSLHFCQUFxQixFQUFFLG9CQUFvQjtRQUMzQyxNQUFNLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQztLQUM5QixDQUFDO3FDQVNlLGlCQUFpQixFQUFLLFVBQVUsRUFBZSxNQUFNO0dBRnpELGNBQWMsQ0FNMUI7U0FOWSxjQUFjOzJGQUFkLGNBQWM7a0JBTjFCLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLGtCQUFrQjtvQkFDNUIsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07b0JBQy9DLFFBQVEsRUFBRSwyQkFBMkI7b0JBQ3JDLE1BQU0sRUFBRSxDQUFDLG1CQUFtQixDQUFDO2lCQUM5Qjs7SUFzQlkscUJBQXFCLFNBQXJCLHFCQUFxQjtJQUVoQyxZQUFZLENBQW9CLEVBQUUsQ0FBYSxFQUFZLENBQVM7UUFBVCxNQUFDLEdBQUQsQ0FBQyxDQUFRO1FBQ2xFLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNYLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQztJQUM1QixDQUFDO0NBQ0YsQ0FBQTtrSEFOWSxxQkFBcUI7c0dBQXJCLHFCQUFxQix3R0FIdEIsMkJBQTJCO0FBRzFCLHFCQUFxQjtJQVZqQyxRQUFRLENBQUM7UUFDUixxQkFBcUIsRUFBRSwyQkFBMkI7UUFDbEQsTUFBTSxFQUFFLENBQUMsYUFBYSxDQUFDO0tBQ3hCLENBQUM7cUNBU2UsaUJBQWlCLEVBQUssVUFBVSxFQUFlLE1BQU07R0FGekQscUJBQXFCLENBTWpDO1NBTlkscUJBQXFCOzJGQUFyQixxQkFBcUI7a0JBTmpDLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLDBCQUEwQjtvQkFDcEMsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07b0JBQy9DLFFBQVEsRUFBRSwyQkFBMkI7b0JBQ3JDLE1BQU0sRUFBRSxDQUFDLGFBQWEsQ0FBQztpQkFDeEI7O0lBb0JZLGNBQWMsU0FBZCxjQUFjO0lBRXpCLFlBQVksQ0FBb0IsRUFBRSxDQUFhLEVBQVksQ0FBUztRQUFULE1BQUMsR0FBRCxDQUFDLENBQVE7UUFDbEUsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ1gsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDO0lBQzVCLENBQUM7Q0FDRixDQUFBOzJHQU5ZLGNBQWM7K0ZBQWQsY0FBYyx3REFGZiwyQkFBMkI7QUFFMUIsY0FBYztJQVIxQixRQUFRLENBQUM7UUFDUixxQkFBcUIsRUFBRSxvQkFBb0I7S0FDNUMsQ0FBQztxQ0FRZSxpQkFBaUIsRUFBSyxVQUFVLEVBQWUsTUFBTTtHQUZ6RCxjQUFjLENBTTFCO1NBTlksY0FBYzsyRkFBZCxjQUFjO2tCQUwxQixTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSxrQkFBa0I7b0JBQzVCLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxRQUFRLEVBQUUsMkJBQTJCO2lCQUN0Qzs7SUFzQlksdUJBQXVCLFNBQXZCLHVCQUF1QjtJQUVsQyxZQUFZLENBQW9CLEVBQUUsQ0FBYSxFQUFZLENBQVM7UUFBVCxNQUFDLEdBQUQsQ0FBQyxDQUFRO1FBQ2xFLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNYLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQztJQUM1QixDQUFDO0NBQ0YsQ0FBQTtvSEFOWSx1QkFBdUI7d0dBQXZCLHVCQUF1QiwwR0FIeEIsMkJBQTJCO0FBRzFCLHVCQUF1QjtJQVZuQyxRQUFRLENBQUM7UUFDUixxQkFBcUIsRUFBRSw2QkFBNkI7UUFDcEQsTUFBTSxFQUFFLENBQUMsYUFBYSxDQUFDO0tBQ3hCLENBQUM7cUNBU2UsaUJBQWlCLEVBQUssVUFBVSxFQUFlLE1BQU07R0FGekQsdUJBQXVCLENBTW5DO1NBTlksdUJBQXVCOzJGQUF2Qix1QkFBdUI7a0JBTm5DLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLDRCQUE0QjtvQkFDdEMsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07b0JBQy9DLFFBQVEsRUFBRSwyQkFBMkI7b0JBQ3JDLE1BQU0sRUFBRSxDQUFDLGFBQWEsQ0FBQztpQkFDeEI7O0lBNkJZLFFBQVEsU0FBUixRQUFRO0lBRW5CLFlBQVksQ0FBb0IsRUFBRSxDQUFhLEVBQVksQ0FBUztRQUFULE1BQUMsR0FBRCxDQUFDLENBQVE7UUFDbEUsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ1gsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDO1FBQzFCLFlBQVksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLEVBQUUsRUFBRSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7SUFDN0MsQ0FBQztDQUNGLENBQUE7cUdBUFksUUFBUTt5RkFBUixRQUFRLG1LQUhULDJCQUEyQjtBQUcxQixRQUFRO0lBWHBCLFFBQVEsQ0FBQztRQUNSLHFCQUFxQixFQUFFLGNBQWM7UUFDckMsTUFBTSxFQUFFLENBQUMsUUFBUSxFQUFFLGdCQUFnQixFQUFFLFVBQVUsRUFBRSxhQUFhLENBQUM7UUFDL0QsT0FBTyxFQUFFLENBQUMsbUJBQW1CLENBQUM7S0FDL0IsQ0FBQztxQ0FTZSxpQkFBaUIsRUFBSyxVQUFVLEVBQWUsTUFBTTtHQUZ6RCxRQUFRLENBT3BCO1NBUFksUUFBUTsyRkFBUixRQUFRO2tCQU5wQixTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSxXQUFXO29CQUNyQixlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtvQkFDL0MsUUFBUSxFQUFFLDJCQUEyQjtvQkFDckMsTUFBTSxFQUFFLENBQUMsUUFBUSxFQUFFLGdCQUFnQixFQUFFLFVBQVUsRUFBRSxhQUFhLENBQUM7aUJBQ2hFOztJQTZCWSxnQkFBZ0IsU0FBaEIsZ0JBQWdCO0lBRTNCLFlBQVksQ0FBb0IsRUFBRSxDQUFhLEVBQVksQ0FBUztRQUFULE1BQUMsR0FBRCxDQUFDLENBQVE7UUFDbEUsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ1gsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDO1FBQzFCLFlBQVksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLEVBQUUsRUFBRSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7SUFDN0MsQ0FBQztDQUNGLENBQUE7NkdBUFksZ0JBQWdCO2lHQUFoQixnQkFBZ0IsNEtBSGpCLDJCQUEyQjtBQUcxQixnQkFBZ0I7SUFWNUIsUUFBUSxDQUFDO1FBQ1IscUJBQXFCLEVBQUUsc0JBQXNCO1FBQzdDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRSxnQkFBZ0IsRUFBRSxVQUFVLEVBQUUsYUFBYSxDQUFDO0tBQ2hFLENBQUM7cUNBU2UsaUJBQWlCLEVBQUssVUFBVSxFQUFlLE1BQU07R0FGekQsZ0JBQWdCLENBTzVCO1NBUFksZ0JBQWdCOzJGQUFoQixnQkFBZ0I7a0JBTjVCLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLG9CQUFvQjtvQkFDOUIsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07b0JBQy9DLFFBQVEsRUFBRSwyQkFBMkI7b0JBQ3JDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRSxnQkFBZ0IsRUFBRSxVQUFVLEVBQUUsYUFBYSxDQUFDO2lCQUNoRTs7SUFxQlksWUFBWSxTQUFaLFlBQVk7SUFFdkIsWUFBWSxDQUFvQixFQUFFLENBQWEsRUFBWSxDQUFTO1FBQVQsTUFBQyxHQUFELENBQUMsQ0FBUTtRQUNsRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDWCxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUM7SUFDNUIsQ0FBQztDQUNGLENBQUE7eUdBTlksWUFBWTs2RkFBWixZQUFZLHNEQUZiLDJCQUEyQjtBQUUxQixZQUFZO0lBUnhCLFFBQVEsQ0FBQztRQUNSLHFCQUFxQixFQUFFLGtCQUFrQjtLQUMxQyxDQUFDO3FDQVFlLGlCQUFpQixFQUFLLFVBQVUsRUFBZSxNQUFNO0dBRnpELFlBQVksQ0FNeEI7U0FOWSxZQUFZOzJGQUFaLFlBQVk7a0JBTHhCLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLGdCQUFnQjtvQkFDMUIsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07b0JBQy9DLFFBQVEsRUFBRSwyQkFBMkI7aUJBQ3RDOztJQTRCWSxnQkFBZ0IsU0FBaEIsZ0JBQWdCO0lBRTNCLFlBQVksQ0FBb0IsRUFBRSxDQUFhLEVBQVksQ0FBUztRQUFULE1BQUMsR0FBRCxDQUFDLENBQVE7UUFDbEUsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ1gsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDO1FBQzFCLFlBQVksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLEVBQUUsRUFBRSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7SUFDN0MsQ0FBQztDQUNGLENBQUE7NkdBUFksZ0JBQWdCO2lHQUFoQixnQkFBZ0Isc0pBSGpCLDJCQUEyQjtBQUcxQixnQkFBZ0I7SUFWNUIsUUFBUSxDQUFDO1FBQ1IscUJBQXFCLEVBQUUsc0JBQXNCO1FBQzdDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRSxnQkFBZ0IsRUFBRSxhQUFhLENBQUM7S0FDcEQsQ0FBQztxQ0FTZSxpQkFBaUIsRUFBSyxVQUFVLEVBQWUsTUFBTTtHQUZ6RCxnQkFBZ0IsQ0FPNUI7U0FQWSxnQkFBZ0I7MkZBQWhCLGdCQUFnQjtrQkFONUIsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUsb0JBQW9CO29CQUM5QixlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtvQkFDL0MsUUFBUSxFQUFFLDJCQUEyQjtvQkFDckMsTUFBTSxFQUFFLENBQUMsUUFBUSxFQUFFLGdCQUFnQixFQUFFLGFBQWEsQ0FBQztpQkFDcEQ7O0lBdUJZLFlBQVksU0FBWixZQUFZO0lBRXZCLFlBQVksQ0FBb0IsRUFBRSxDQUFhLEVBQVksQ0FBUztRQUFULE1BQUMsR0FBRCxDQUFDLENBQVE7UUFDbEUsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ1gsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDO0lBQzVCLENBQUM7Q0FDRixDQUFBO3lHQU5ZLFlBQVk7NkZBQVosWUFBWSxzSUFIYiwyQkFBMkI7QUFHMUIsWUFBWTtJQVZ4QixRQUFRLENBQUM7UUFDUixxQkFBcUIsRUFBRSxrQkFBa0I7UUFDekMsTUFBTSxFQUFFLENBQUMsVUFBVSxFQUFFLFFBQVEsRUFBRSxhQUFhLENBQUM7S0FDOUMsQ0FBQztxQ0FTZSxpQkFBaUIsRUFBSyxVQUFVLEVBQWUsTUFBTTtHQUZ6RCxZQUFZLENBTXhCO1NBTlksWUFBWTsyRkFBWixZQUFZO2tCQU54QixTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSxnQkFBZ0I7b0JBQzFCLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxRQUFRLEVBQUUsMkJBQTJCO29CQUNyQyxNQUFNLEVBQUUsQ0FBQyxVQUFVLEVBQUUsUUFBUSxFQUFFLGFBQWEsQ0FBQztpQkFDOUM7O0lBc0JZLGlCQUFpQixTQUFqQixpQkFBaUI7SUFFNUIsWUFBWSxDQUFvQixFQUFFLENBQWEsRUFBWSxDQUFTO1FBQVQsTUFBQyxHQUFELENBQUMsQ0FBUTtRQUNsRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDWCxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUM7SUFDNUIsQ0FBQztDQUNGLENBQUE7OEdBTlksaUJBQWlCO2tHQUFqQixpQkFBaUIsMkdBSGxCLDJCQUEyQjtBQUcxQixpQkFBaUI7SUFWN0IsUUFBUSxDQUFDO1FBQ1IscUJBQXFCLEVBQUUsdUJBQXVCO1FBQzlDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRSxRQUFRLENBQUM7S0FDN0IsQ0FBQztxQ0FTZSxpQkFBaUIsRUFBSyxVQUFVLEVBQWUsTUFBTTtHQUZ6RCxpQkFBaUIsQ0FNN0I7U0FOWSxpQkFBaUI7MkZBQWpCLGlCQUFpQjtrQkFON0IsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUscUJBQXFCO29CQUMvQixlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtvQkFDL0MsUUFBUSxFQUFFLDJCQUEyQjtvQkFDckMsTUFBTSxFQUFFLENBQUMsUUFBUSxFQUFFLFFBQVEsQ0FBQztpQkFDN0I7O0lBc0JZLFFBQVEsU0FBUixRQUFRO0lBRW5CLFlBQVksQ0FBb0IsRUFBRSxDQUFhLEVBQVksQ0FBUztRQUFULE1BQUMsR0FBRCxDQUFDLENBQVE7UUFDbEUsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ1gsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDO0lBQzVCLENBQUM7Q0FDRixDQUFBO3FHQU5ZLFFBQVE7eUZBQVIsUUFBUSwrSkFIVCwyQkFBMkI7QUFHMUIsUUFBUTtJQVZwQixRQUFRLENBQUM7UUFDUixxQkFBcUIsRUFBRSxjQUFjO1FBQ3JDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRSxpQkFBaUIsRUFBRSxTQUFTLEVBQUUsV0FBVyxDQUFDO0tBQzlELENBQUM7cUNBU2UsaUJBQWlCLEVBQUssVUFBVSxFQUFlLE1BQU07R0FGekQsUUFBUSxDQU1wQjtTQU5ZLFFBQVE7MkZBQVIsUUFBUTtrQkFOcEIsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUsV0FBVztvQkFDckIsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07b0JBQy9DLFFBQVEsRUFBRSwyQkFBMkI7b0JBQ3JDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRSxpQkFBaUIsRUFBRSxTQUFTLEVBQUUsV0FBVyxDQUFDO2lCQUM5RDs7SUFzQlksZUFBZSxTQUFmLGVBQWU7SUFFMUIsWUFBWSxDQUFvQixFQUFFLENBQWEsRUFBWSxDQUFTO1FBQVQsTUFBQyxHQUFELENBQUMsQ0FBUTtRQUNsRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDWCxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUM7SUFDNUIsQ0FBQztDQUNGLENBQUE7NEdBTlksZUFBZTtnR0FBZixlQUFlLDZJQUhoQiwyQkFBMkI7QUFHMUIsZUFBZTtJQVYzQixRQUFRLENBQUM7UUFDUixxQkFBcUIsRUFBRSxxQkFBcUI7UUFDNUMsTUFBTSxFQUFFLENBQUMsZUFBZSxFQUFFLFFBQVEsRUFBRSxVQUFVLENBQUM7S0FDaEQsQ0FBQztxQ0FTZSxpQkFBaUIsRUFBSyxVQUFVLEVBQWUsTUFBTTtHQUZ6RCxlQUFlLENBTTNCO1NBTlksZUFBZTsyRkFBZixlQUFlO2tCQU4zQixTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSxtQkFBbUI7b0JBQzdCLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxRQUFRLEVBQUUsMkJBQTJCO29CQUNyQyxNQUFNLEVBQUUsQ0FBQyxlQUFlLEVBQUUsUUFBUSxFQUFFLFVBQVUsQ0FBQztpQkFDaEQ7O0lBc0JZLGNBQWMsU0FBZCxjQUFjO0lBRXpCLFlBQVksQ0FBb0IsRUFBRSxDQUFhLEVBQVksQ0FBUztRQUFULE1BQUMsR0FBRCxDQUFDLENBQVE7UUFDbEUsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ1gsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDO0lBQzVCLENBQUM7Q0FDRixDQUFBOzJHQU5ZLGNBQWM7K0ZBQWQsY0FBYyw0R0FIZiwyQkFBMkI7QUFHMUIsY0FBYztJQVYxQixRQUFRLENBQUM7UUFDUixxQkFBcUIsRUFBRSxvQkFBb0I7UUFDM0MsTUFBTSxFQUFFLENBQUMsUUFBUSxFQUFFLFVBQVUsQ0FBQztLQUMvQixDQUFDO3FDQVNlLGlCQUFpQixFQUFLLFVBQVUsRUFBZSxNQUFNO0dBRnpELGNBQWMsQ0FNMUI7U0FOWSxjQUFjOzJGQUFkLGNBQWM7a0JBTjFCLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLGtCQUFrQjtvQkFDNUIsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07b0JBQy9DLFFBQVEsRUFBRSwyQkFBMkI7b0JBQ3JDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRSxVQUFVLENBQUM7aUJBQy9COztJQXNCWSxlQUFlLFNBQWYsZUFBZTtJQUUxQixZQUFZLENBQW9CLEVBQUUsQ0FBYSxFQUFZLENBQVM7UUFBVCxNQUFDLEdBQUQsQ0FBQyxDQUFRO1FBQ2xFLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNYLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQztJQUM1QixDQUFDO0NBQ0YsQ0FBQTs0R0FOWSxlQUFlO2dHQUFmLGVBQWUsK0hBSGhCLDJCQUEyQjtBQUcxQixlQUFlO0lBVjNCLFFBQVEsQ0FBQztRQUNSLHFCQUFxQixFQUFFLHFCQUFxQjtRQUM1QyxNQUFNLEVBQUUsQ0FBQyxhQUFhLEVBQUUsY0FBYyxDQUFDO0tBQ3hDLENBQUM7cUNBU2UsaUJBQWlCLEVBQUssVUFBVSxFQUFlLE1BQU07R0FGekQsZUFBZSxDQU0zQjtTQU5ZLGVBQWU7MkZBQWYsZUFBZTtrQkFOM0IsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUsbUJBQW1CO29CQUM3QixlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtvQkFDL0MsUUFBUSxFQUFFLDJCQUEyQjtvQkFDckMsTUFBTSxFQUFFLENBQUMsYUFBYSxFQUFFLGNBQWMsQ0FBQztpQkFDeEM7O0lBNEJZLGNBQWMsU0FBZCxjQUFjO0lBRXpCLFlBQVksQ0FBb0IsRUFBRSxDQUFhLEVBQVksQ0FBUztRQUFULE1BQUMsR0FBRCxDQUFDLENBQVE7UUFDbEUsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ1gsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDO1FBQzFCLFlBQVksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLEVBQUUsRUFBRSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7SUFDNUMsQ0FBQztDQUNGLENBQUE7MkdBUFksY0FBYzsrRkFBZCxjQUFjLDhTQUhmLDJCQUEyQjtBQUcxQixjQUFjO0lBVjFCLFFBQVEsQ0FBQztRQUNSLHFCQUFxQixFQUFFLG9CQUFvQjtRQUMzQyxNQUFNLEVBQUUsQ0FBQyxPQUFPLEVBQUUsYUFBYSxFQUFFLGFBQWEsRUFBRSxVQUFVLEVBQUUsbUJBQW1CLEVBQUUsT0FBTyxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLFNBQVMsQ0FBQztLQUN4SSxDQUFDO3FDQVNlLGlCQUFpQixFQUFLLFVBQVUsRUFBZSxNQUFNO0dBRnpELGNBQWMsQ0FPMUI7U0FQWSxjQUFjOzJGQUFkLGNBQWM7a0JBTjFCLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLGtCQUFrQjtvQkFDNUIsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07b0JBQy9DLFFBQVEsRUFBRSwyQkFBMkI7b0JBQ3JDLE1BQU0sRUFBRSxDQUFDLE9BQU8sRUFBRSxhQUFhLEVBQUUsYUFBYSxFQUFFLFVBQVUsRUFBRSxtQkFBbUIsRUFBRSxPQUFPLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsU0FBUyxDQUFDO2lCQUN4STs7SUF1QlkseUJBQXlCLFNBQXpCLHlCQUF5QjtJQUVwQyxZQUFZLENBQW9CLEVBQUUsQ0FBYSxFQUFZLENBQVM7UUFBVCxNQUFDLEdBQUQsQ0FBQyxDQUFRO1FBQ2xFLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNYLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQztJQUM1QixDQUFDO0NBQ0YsQ0FBQTtzSEFOWSx5QkFBeUI7MEdBQXpCLHlCQUF5Qix1S0FIMUIsMkJBQTJCO0FBRzFCLHlCQUF5QjtJQVZyQyxRQUFRLENBQUM7UUFDUixxQkFBcUIsRUFBRSwrQkFBK0I7UUFDdEQsTUFBTSxFQUFFLENBQUMsYUFBYSxFQUFFLGVBQWUsRUFBRSxhQUFhLENBQUM7S0FDeEQsQ0FBQztxQ0FTZSxpQkFBaUIsRUFBSyxVQUFVLEVBQWUsTUFBTTtHQUZ6RCx5QkFBeUIsQ0FNckM7U0FOWSx5QkFBeUI7MkZBQXpCLHlCQUF5QjtrQkFOckMsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUsNkJBQTZCO29CQUN2QyxlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtvQkFDL0MsUUFBUSxFQUFFLDJCQUEyQjtvQkFDckMsTUFBTSxFQUFFLENBQUMsYUFBYSxFQUFFLGVBQWUsRUFBRSxhQUFhLENBQUM7aUJBQ3hEOztJQWdEWSx5QkFBeUIsU0FBekIseUJBQXlCO0lBRXBDLFlBQVksQ0FBb0IsRUFBRSxDQUFhLEVBQVksQ0FBUztRQUFULE1BQUMsR0FBRCxDQUFDLENBQVE7UUFDbEUsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ1gsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDO1FBQzFCLFlBQVksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLEVBQUUsRUFBRSxDQUFDLGNBQWMsRUFBRSxZQUFZLEVBQUUsVUFBVSxFQUFFLFlBQVksRUFBRSxZQUFZLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQztJQUNwSCxDQUFDO0NBQ0YsQ0FBQTtzSEFQWSx5QkFBeUI7MEdBQXpCLHlCQUF5QixvT0FIMUIsMkJBQTJCO0FBRzFCLHlCQUF5QjtJQVZyQyxRQUFRLENBQUM7UUFDUixxQkFBcUIsRUFBRSwrQkFBK0I7UUFDdEQsTUFBTSxFQUFFLENBQUMsUUFBUSxFQUFFLE1BQU0sRUFBRSw4QkFBOEIsRUFBRSxpQkFBaUIsRUFBRSxRQUFRLENBQUM7S0FDeEYsQ0FBQztxQ0FTZSxpQkFBaUIsRUFBSyxVQUFVLEVBQWUsTUFBTTtHQUZ6RCx5QkFBeUIsQ0FPckM7U0FQWSx5QkFBeUI7MkZBQXpCLHlCQUF5QjtrQkFOckMsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUsOEJBQThCO29CQUN4QyxlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtvQkFDL0MsUUFBUSxFQUFFLDJCQUEyQjtvQkFDckMsTUFBTSxFQUFFLENBQUMsUUFBUSxFQUFFLE1BQU0sRUFBRSw4QkFBOEIsRUFBRSxpQkFBaUIsRUFBRSxRQUFRLENBQUM7aUJBQ3hGOztJQXVCWSw2QkFBNkIsU0FBN0IsNkJBQTZCO0lBRXhDLFlBQVksQ0FBb0IsRUFBRSxDQUFhLEVBQVksQ0FBUztRQUFULE1BQUMsR0FBRCxDQUFDLENBQVE7UUFDbEUsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ1gsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDO0lBQzVCLENBQUM7Q0FDRixDQUFBOzBIQU5ZLDZCQUE2Qjs4R0FBN0IsNkJBQTZCLDZLQUg5QiwyQkFBMkI7QUFHMUIsNkJBQTZCO0lBVnpDLFFBQVEsQ0FBQztRQUNSLHFCQUFxQixFQUFFLG1DQUFtQztRQUMxRCxNQUFNLEVBQUUsQ0FBQyxPQUFPLEVBQUUsUUFBUSxFQUFFLFFBQVEsRUFBRSxRQUFRLEVBQUUsUUFBUSxDQUFDO0tBQzFELENBQUM7cUNBU2UsaUJBQWlCLEVBQUssVUFBVSxFQUFlLE1BQU07R0FGekQsNkJBQTZCLENBTXpDO1NBTlksNkJBQTZCOzJGQUE3Qiw2QkFBNkI7a0JBTnpDLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLG1DQUFtQztvQkFDN0MsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07b0JBQy9DLFFBQVEsRUFBRSwyQkFBMkI7b0JBQ3JDLE1BQU0sRUFBRSxDQUFDLE9BQU8sRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFFLFFBQVEsRUFBRSxRQUFRLENBQUM7aUJBQzFEOztJQXNCWSxzQkFBc0IsU0FBdEIsc0JBQXNCO0lBRWpDLFlBQVksQ0FBb0IsRUFBRSxDQUFhLEVBQVksQ0FBUztRQUFULE1BQUMsR0FBRCxDQUFDLENBQVE7UUFDbEUsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ1gsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDO0lBQzVCLENBQUM7Q0FDRixDQUFBO21IQU5ZLHNCQUFzQjt1R0FBdEIsc0JBQXNCLDJGQUh2QiwyQkFBMkI7QUFHMUIsc0JBQXNCO0lBVmxDLFFBQVEsQ0FBQztRQUNSLHFCQUFxQixFQUFFLDRCQUE0QjtRQUNuRCxNQUFNLEVBQUUsQ0FBQyxNQUFNLENBQUM7S0FDakIsQ0FBQztxQ0FTZSxpQkFBaUIsRUFBSyxVQUFVLEVBQWUsTUFBTTtHQUZ6RCxzQkFBc0IsQ0FNbEM7U0FOWSxzQkFBc0I7MkZBQXRCLHNCQUFzQjtrQkFObEMsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUsMkJBQTJCO29CQUNyQyxlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtvQkFDL0MsUUFBUSxFQUFFLDJCQUEyQjtvQkFDckMsTUFBTSxFQUFFLENBQUMsTUFBTSxDQUFDO2lCQUNqQjs7SUE4QlksMkJBQTJCLFNBQTNCLDJCQUEyQjtJQUV0QyxZQUFZLENBQW9CLEVBQUUsQ0FBYSxFQUFZLENBQVM7UUFBVCxNQUFDLEdBQUQsQ0FBQyxDQUFRO1FBQ2xFLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNYLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQztRQUMxQixZQUFZLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxFQUFFLEVBQUUsQ0FBQyxZQUFZLEVBQUUsV0FBVyxDQUFDLENBQUMsQ0FBQztJQUMzRCxDQUFDO0NBQ0YsQ0FBQTt3SEFQWSwyQkFBMkI7NEdBQTNCLDJCQUEyQix1RUFGNUIsMkJBQTJCO0FBRTFCLDJCQUEyQjtJQVJ2QyxRQUFRLENBQUM7UUFDUixxQkFBcUIsRUFBRSxpQ0FBaUM7S0FDekQsQ0FBQztxQ0FRZSxpQkFBaUIsRUFBSyxVQUFVLEVBQWUsTUFBTTtHQUZ6RCwyQkFBMkIsQ0FPdkM7U0FQWSwyQkFBMkI7MkZBQTNCLDJCQUEyQjtrQkFMdkMsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUsaUNBQWlDO29CQUMzQyxlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtvQkFDL0MsUUFBUSxFQUFFLDJCQUEyQjtpQkFDdEM7O0lBOEJZLHdCQUF3QixTQUF4Qix3QkFBd0I7SUFFbkMsWUFBWSxDQUFvQixFQUFFLENBQWEsRUFBWSxDQUFTO1FBQVQsTUFBQyxHQUFELENBQUMsQ0FBUTtRQUNsRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDWCxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUM7UUFDMUIsWUFBWSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsRUFBRSxFQUFFLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO0lBQ2xELENBQUM7Q0FDRixDQUFBO3FIQVBZLHdCQUF3Qjt5R0FBeEIsd0JBQXdCLHdUQUh6QiwyQkFBMkI7QUFHMUIsd0JBQXdCO0lBWHBDLFFBQVEsQ0FBQztRQUNSLHFCQUFxQixFQUFFLDhCQUE4QjtRQUNyRCxNQUFNLEVBQUUsQ0FBQyxvQkFBb0IsRUFBRSxzQkFBc0IsRUFBRSxNQUFNLEVBQUUsa0JBQWtCLEVBQUUsY0FBYyxFQUFFLGNBQWMsRUFBRSxnQkFBZ0IsQ0FBQztRQUNwSSxPQUFPLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQztLQUMvQixDQUFDO3FDQVNlLGlCQUFpQixFQUFLLFVBQVUsRUFBZSxNQUFNO0dBRnpELHdCQUF3QixDQU9wQztTQVBZLHdCQUF3QjsyRkFBeEIsd0JBQXdCO2tCQU5wQyxTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSw0QkFBNEI7b0JBQ3RDLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxRQUFRLEVBQUUsMkJBQTJCO29CQUNyQyxNQUFNLEVBQUUsQ0FBQyxvQkFBb0IsRUFBRSxzQkFBc0IsRUFBRSxNQUFNLEVBQUUsa0JBQWtCLEVBQUUsY0FBYyxFQUFFLGNBQWMsRUFBRSxnQkFBZ0IsQ0FBQztpQkFDckk7O0lBeUNZLHFCQUFxQixTQUFyQixxQkFBcUI7SUFFaEMsWUFBWSxDQUFvQixFQUFFLENBQWEsRUFBWSxDQUFTO1FBQVQsTUFBQyxHQUFELENBQUMsQ0FBUTtRQUNsRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDWCxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUM7UUFDMUIsWUFBWSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsRUFBRSxFQUFFLENBQUMsV0FBVyxFQUFFLFNBQVMsRUFBRSxtQkFBbUIsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDO0lBQzNGLENBQUM7Q0FDRixDQUFBO2tIQVBZLHFCQUFxQjtzR0FBckIscUJBQXFCLCttQkFIdEIsMkJBQTJCO0FBRzFCLHFCQUFxQjtJQVZqQyxRQUFRLENBQUM7UUFDUixxQkFBcUIsRUFBRSwyQkFBMkI7UUFDbEQsTUFBTSxFQUFFLENBQUMsVUFBVSxFQUFFLFlBQVksRUFBRSx3QkFBd0IsRUFBRSx1QkFBdUIsRUFBRSxtQkFBbUIsRUFBRSx5QkFBeUIsRUFBRSxvQkFBb0IsRUFBRSxXQUFXLEVBQUUsZ0JBQWdCLEVBQUUsY0FBYyxFQUFFLE1BQU0sRUFBRSxrQkFBa0IsRUFBRSxtQkFBbUIsRUFBRSxZQUFZLEVBQUUsZ0JBQWdCLEVBQUUsYUFBYSxDQUFDO0tBQzNTLENBQUM7cUNBU2UsaUJBQWlCLEVBQUssVUFBVSxFQUFlLE1BQU07R0FGekQscUJBQXFCLENBT2pDO1NBUFkscUJBQXFCOzJGQUFyQixxQkFBcUI7a0JBTmpDLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLHlCQUF5QjtvQkFDbkMsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07b0JBQy9DLFFBQVEsRUFBRSwyQkFBMkI7b0JBQ3JDLE1BQU0sRUFBRSxDQUFDLFVBQVUsRUFBRSxZQUFZLEVBQUUsd0JBQXdCLEVBQUUsdUJBQXVCLEVBQUUsbUJBQW1CLEVBQUUseUJBQXlCLEVBQUUsb0JBQW9CLEVBQUUsV0FBVyxFQUFFLGdCQUFnQixFQUFFLGNBQWMsRUFBRSxNQUFNLEVBQUUsa0JBQWtCLEVBQUUsbUJBQW1CLEVBQUUsWUFBWSxFQUFFLGdCQUFnQixFQUFFLGFBQWEsQ0FBQztpQkFDM1M7O0lBNkJZLDJCQUEyQixTQUEzQiwyQkFBMkI7SUFFdEMsWUFBWSxDQUFvQixFQUFFLENBQWEsRUFBWSxDQUFTO1FBQVQsTUFBQyxHQUFELENBQUMsQ0FBUTtRQUNsRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDWCxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUM7UUFDMUIsWUFBWSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsRUFBRSxFQUFFLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztJQUM5QyxDQUFDO0NBQ0YsQ0FBQTt3SEFQWSwyQkFBMkI7NEdBQTNCLDJCQUEyQiw4SUFINUIsMkJBQTJCO0FBRzFCLDJCQUEyQjtJQVZ2QyxRQUFRLENBQUM7UUFDUixxQkFBcUIsRUFBRSxpQ0FBaUM7UUFDeEQsTUFBTSxFQUFFLENBQUMsYUFBYSxFQUFFLE1BQU0sRUFBRSxRQUFRLENBQUM7S0FDMUMsQ0FBQztxQ0FTZSxpQkFBaUIsRUFBSyxVQUFVLEVBQWUsTUFBTTtHQUZ6RCwyQkFBMkIsQ0FPdkM7U0FQWSwyQkFBMkI7MkZBQTNCLDJCQUEyQjtrQkFOdkMsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUsZ0NBQWdDO29CQUMxQyxlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtvQkFDL0MsUUFBUSxFQUFFLDJCQUEyQjtvQkFDckMsTUFBTSxFQUFFLENBQUMsYUFBYSxFQUFFLE1BQU0sRUFBRSxRQUFRLENBQUM7aUJBQzFDOztJQTZCWSxpQkFBaUIsU0FBakIsaUJBQWlCO0lBRTVCLFlBQVksQ0FBb0IsRUFBRSxDQUFhLEVBQVksQ0FBUztRQUFULE1BQUMsR0FBRCxDQUFDLENBQVE7UUFDbEUsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ1gsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDO1FBQzFCLFlBQVksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLEVBQUUsRUFBRSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7SUFDOUMsQ0FBQztDQUNGLENBQUE7OEdBUFksaUJBQWlCO2tHQUFqQixpQkFBaUIsdUlBSGxCLDJCQUEyQjtBQUcxQixpQkFBaUI7SUFWN0IsUUFBUSxDQUFDO1FBQ1IscUJBQXFCLEVBQUUsdUJBQXVCO1FBQzlDLE1BQU0sRUFBRSxDQUFDLFVBQVUsRUFBRSxhQUFhLEVBQUUsTUFBTSxDQUFDO0tBQzVDLENBQUM7cUNBU2UsaUJBQWlCLEVBQUssVUFBVSxFQUFlLE1BQU07R0FGekQsaUJBQWlCLENBTzdCO1NBUFksaUJBQWlCOzJGQUFqQixpQkFBaUI7a0JBTjdCLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLHFCQUFxQjtvQkFDL0IsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07b0JBQy9DLFFBQVEsRUFBRSwyQkFBMkI7b0JBQ3JDLE1BQU0sRUFBRSxDQUFDLFVBQVUsRUFBRSxhQUFhLEVBQUUsTUFBTSxDQUFDO2lCQUM1Qzs7SUE2QlksdUJBQXVCLFNBQXZCLHVCQUF1QjtJQUVsQyxZQUFZLENBQW9CLEVBQUUsQ0FBYSxFQUFZLENBQVM7UUFBVCxNQUFDLEdBQUQsQ0FBQyxDQUFRO1FBQ2xFLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNYLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQztRQUMxQixZQUFZLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxFQUFFLEVBQUUsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO0lBQzlDLENBQUM7Q0FDRixDQUFBO29IQVBZLHVCQUF1Qjt3R0FBdkIsdUJBQXVCLGdKQUh4QiwyQkFBMkI7QUFHMUIsdUJBQXVCO0lBVm5DLFFBQVEsQ0FBQztRQUNSLHFCQUFxQixFQUFFLDZCQUE2QjtRQUNwRCxNQUFNLEVBQUUsQ0FBQyxVQUFVLEVBQUUsYUFBYSxFQUFFLE1BQU0sQ0FBQztLQUM1QyxDQUFDO3FDQVNlLGlCQUFpQixFQUFLLFVBQVUsRUFBZSxNQUFNO0dBRnpELHVCQUF1QixDQU9uQztTQVBZLHVCQUF1QjsyRkFBdkIsdUJBQXVCO2tCQU5uQyxTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSw4QkFBOEI7b0JBQ3hDLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxRQUFRLEVBQUUsMkJBQTJCO29CQUNyQyxNQUFNLEVBQUUsQ0FBQyxVQUFVLEVBQUUsYUFBYSxFQUFFLE1BQU0sQ0FBQztpQkFDNUM7O0lBc0NZLGtCQUFrQixTQUFsQixrQkFBa0I7SUFFN0IsWUFBWSxDQUFvQixFQUFFLENBQWEsRUFBWSxDQUFTO1FBQVQsTUFBQyxHQUFELENBQUMsQ0FBUTtRQUNsRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDWCxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUM7UUFDMUIsWUFBWSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsRUFBRSxFQUFFLENBQUMsWUFBWSxFQUFFLFdBQVcsRUFBRSxXQUFXLENBQUMsQ0FBQyxDQUFDO0lBQ3hFLENBQUM7Q0FDRixDQUFBOytHQVBZLGtCQUFrQjttR0FBbEIsa0JBQWtCLHNKQUhuQiwyQkFBMkI7QUFHMUIsa0JBQWtCO0lBWDlCLFFBQVEsQ0FBQztRQUNSLHFCQUFxQixFQUFFLHdCQUF3QjtRQUMvQyxNQUFNLEVBQUUsQ0FBQyxhQUFhLEVBQUUsTUFBTSxFQUFFLGlCQUFpQixDQUFDO1FBQ2xELE9BQU8sRUFBRSxDQUFDLGlCQUFpQixDQUFDO0tBQzdCLENBQUM7cUNBU2UsaUJBQWlCLEVBQUssVUFBVSxFQUFlLE1BQU07R0FGekQsa0JBQWtCLENBTzlCO1NBUFksa0JBQWtCOzJGQUFsQixrQkFBa0I7a0JBTjlCLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLHNCQUFzQjtvQkFDaEMsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07b0JBQy9DLFFBQVEsRUFBRSwyQkFBMkI7b0JBQ3JDLE1BQU0sRUFBRSxDQUFDLGFBQWEsRUFBRSxNQUFNLEVBQUUsaUJBQWlCLENBQUM7aUJBQ25EOztJQXFCWSxpQkFBaUIsU0FBakIsaUJBQWlCO0lBRTVCLFlBQVksQ0FBb0IsRUFBRSxDQUFhLEVBQVksQ0FBUztRQUFULE1BQUMsR0FBRCxDQUFDLENBQVE7UUFDbEUsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ1gsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDO0lBQzVCLENBQUM7Q0FDRixDQUFBOzhHQU5ZLGlCQUFpQjtrR0FBakIsaUJBQWlCLDJEQUZsQiwyQkFBMkI7QUFFMUIsaUJBQWlCO0lBUjdCLFFBQVEsQ0FBQztRQUNSLHFCQUFxQixFQUFFLHVCQUF1QjtLQUMvQyxDQUFDO3FDQVFlLGlCQUFpQixFQUFLLFVBQVUsRUFBZSxNQUFNO0dBRnpELGlCQUFpQixDQU03QjtTQU5ZLGlCQUFpQjsyRkFBakIsaUJBQWlCO2tCQUw3QixTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSxxQkFBcUI7b0JBQy9CLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxRQUFRLEVBQUUsMkJBQTJCO2lCQUN0Qzs7SUF1QlksMEJBQTBCLFNBQTFCLDBCQUEwQjtJQUVyQyxZQUFZLENBQW9CLEVBQUUsQ0FBYSxFQUFZLENBQVM7UUFBVCxNQUFDLEdBQUQsQ0FBQyxDQUFRO1FBQ2xFLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNYLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQztJQUM1QixDQUFDO0NBQ0YsQ0FBQTt1SEFOWSwwQkFBMEI7MkdBQTFCLDBCQUEwQix1TUFIM0IsMkJBQTJCO0FBRzFCLDBCQUEwQjtJQVh0QyxRQUFRLENBQUM7UUFDUixxQkFBcUIsRUFBRSxnQ0FBZ0M7UUFDdkQsTUFBTSxFQUFFLENBQUMsdUJBQXVCLEVBQUUsYUFBYSxFQUFFLE1BQU0sRUFBRSxhQUFhLENBQUM7UUFDdkUsT0FBTyxFQUFFLENBQUMsMEJBQTBCLENBQUM7S0FDdEMsQ0FBQztxQ0FTZSxpQkFBaUIsRUFBSyxVQUFVLEVBQWUsTUFBTTtHQUZ6RCwwQkFBMEIsQ0FNdEM7U0FOWSwwQkFBMEI7MkZBQTFCLDBCQUEwQjtrQkFOdEMsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUsK0JBQStCO29CQUN6QyxlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtvQkFDL0MsUUFBUSxFQUFFLDJCQUEyQjtvQkFDckMsTUFBTSxFQUFFLENBQUMsdUJBQXVCLEVBQUUsYUFBYSxFQUFFLE1BQU0sRUFBRSxhQUFhLENBQUM7aUJBQ3hFOztJQTRCWSw4QkFBOEIsU0FBOUIsOEJBQThCO0lBRXpDLFlBQVksQ0FBb0IsRUFBRSxDQUFhLEVBQVksQ0FBUztRQUFULE1BQUMsR0FBRCxDQUFDLENBQVE7UUFDbEUsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ1gsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDO1FBQzFCLFlBQVksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLEVBQUUsRUFBRSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7SUFDN0MsQ0FBQztDQUNGLENBQUE7MkhBUFksOEJBQThCOytHQUE5Qiw4QkFBOEIsOFBBSC9CLDJCQUEyQjtBQUcxQiw4QkFBOEI7SUFWMUMsUUFBUSxDQUFDO1FBQ1IscUJBQXFCLEVBQUUsb0NBQW9DO1FBQzNELE1BQU0sRUFBRSxDQUFDLFVBQVUsRUFBRSxnQkFBZ0IsRUFBRSxhQUFhLEVBQUUsUUFBUSxFQUFFLE1BQU0sRUFBRSxnQkFBZ0IsRUFBRSxRQUFRLENBQUM7S0FDcEcsQ0FBQztxQ0FTZSxpQkFBaUIsRUFBSyxVQUFVLEVBQWUsTUFBTTtHQUZ6RCw4QkFBOEIsQ0FPMUM7U0FQWSw4QkFBOEI7MkZBQTlCLDhCQUE4QjtrQkFOMUMsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUsb0NBQW9DO29CQUM5QyxlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtvQkFDL0MsUUFBUSxFQUFFLDJCQUEyQjtvQkFDckMsTUFBTSxFQUFFLENBQUMsVUFBVSxFQUFFLGdCQUFnQixFQUFFLGFBQWEsRUFBRSxRQUFRLEVBQUUsTUFBTSxFQUFFLGdCQUFnQixFQUFFLFFBQVEsQ0FBQztpQkFDcEc7O0lBNkJZLHFCQUFxQixTQUFyQixxQkFBcUI7SUFFaEMsWUFBWSxDQUFvQixFQUFFLENBQWEsRUFBWSxDQUFTO1FBQVQsTUFBQyxHQUFELENBQUMsQ0FBUTtRQUNsRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDWCxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUM7UUFDMUIsWUFBWSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsRUFBRSxFQUFFLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztJQUM3QyxDQUFDO0NBQ0YsQ0FBQTtrSEFQWSxxQkFBcUI7c0dBQXJCLHFCQUFxQixtSUFIdEIsMkJBQTJCO0FBRzFCLHFCQUFxQjtJQVZqQyxRQUFRLENBQUM7UUFDUixxQkFBcUIsRUFBRSwyQkFBMkI7UUFDbEQsTUFBTSxFQUFFLENBQUMsYUFBYSxFQUFFLGFBQWEsQ0FBQztLQUN2QyxDQUFDO3FDQVNlLGlCQUFpQixFQUFLLFVBQVUsRUFBZSxNQUFNO0dBRnpELHFCQUFxQixDQU9qQztTQVBZLHFCQUFxQjsyRkFBckIscUJBQXFCO2tCQU5qQyxTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSx5QkFBeUI7b0JBQ25DLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxRQUFRLEVBQUUsMkJBQTJCO29CQUNyQyxNQUFNLEVBQUUsQ0FBQyxhQUFhLEVBQUUsYUFBYSxDQUFDO2lCQUN2Qzs7SUFpQ1ksc0JBQXNCLFNBQXRCLHNCQUFzQjtJQUVqQyxZQUFZLENBQW9CLEVBQUUsQ0FBYSxFQUFZLENBQVM7UUFBVCxNQUFDLEdBQUQsQ0FBQyxDQUFRO1FBQ2xFLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNYLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQztRQUMxQixZQUFZLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxFQUFFLEVBQUUsQ0FBQyxzQkFBc0IsRUFBRSxvQkFBb0IsQ0FBQyxDQUFDLENBQUM7SUFDOUUsQ0FBQztDQUNGLENBQUE7bUhBUFksc0JBQXNCO3VHQUF0QixzQkFBc0IsOFBBSHZCLDJCQUEyQjtBQUcxQixzQkFBc0I7SUFWbEMsUUFBUSxDQUFDO1FBQ1IscUJBQXFCLEVBQUUsNEJBQTRCO1FBQ25ELE1BQU0sRUFBRSxDQUFDLHFCQUFxQixFQUFFLGNBQWMsRUFBRSxNQUFNLEVBQUUsb0JBQW9CLEVBQUUsdUJBQXVCLENBQUM7S0FDdkcsQ0FBQztxQ0FTZSxpQkFBaUIsRUFBSyxVQUFVLEVBQWUsTUFBTTtHQUZ6RCxzQkFBc0IsQ0FPbEM7U0FQWSxzQkFBc0I7MkZBQXRCLHNCQUFzQjtrQkFObEMsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUsMEJBQTBCO29CQUNwQyxlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtvQkFDL0MsUUFBUSxFQUFFLDJCQUEyQjtvQkFDckMsTUFBTSxFQUFFLENBQUMscUJBQXFCLEVBQUUsY0FBYyxFQUFFLE1BQU0sRUFBRSxvQkFBb0IsRUFBRSx1QkFBdUIsQ0FBQztpQkFDdkc7O0lBdUJZLHNCQUFzQixTQUF0QixzQkFBc0I7SUFFakMsWUFBWSxDQUFvQixFQUFFLENBQWEsRUFBWSxDQUFTO1FBQVQsTUFBQyxHQUFELENBQUMsQ0FBUTtRQUNsRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDWCxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUM7SUFDNUIsQ0FBQztDQUNGLENBQUE7bUhBTlksc0JBQXNCO3VHQUF0QixzQkFBc0Isd0dBSHZCLDJCQUEyQjtBQUcxQixzQkFBc0I7SUFWbEMsUUFBUSxDQUFDO1FBQ1IscUJBQXFCLEVBQUUsNEJBQTRCO1FBQ25ELE1BQU0sRUFBRSxDQUFDLGFBQWEsQ0FBQztLQUN4QixDQUFDO3FDQVNlLGlCQUFpQixFQUFLLFVBQVUsRUFBZSxNQUFNO0dBRnpELHNCQUFzQixDQU1sQztTQU5ZLHNCQUFzQjsyRkFBdEIsc0JBQXNCO2tCQU5sQyxTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSwwQkFBMEI7b0JBQ3BDLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxRQUFRLEVBQUUsMkJBQTJCO29CQUNyQyxNQUFNLEVBQUUsQ0FBQyxhQUFhLENBQUM7aUJBQ3hCOztJQXNCWSxRQUFRLFNBQVIsUUFBUTtJQUVuQixZQUFZLENBQW9CLEVBQUUsQ0FBYSxFQUFZLENBQVM7UUFBVCxNQUFDLEdBQUQsQ0FBQyxDQUFRO1FBQ2xFLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNYLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQztJQUM1QixDQUFDO0NBQ0YsQ0FBQTtxR0FOWSxRQUFRO3lGQUFSLFFBQVEsMkZBSFQsMkJBQTJCO0FBRzFCLFFBQVE7SUFWcEIsUUFBUSxDQUFDO1FBQ1IscUJBQXFCLEVBQUUsY0FBYztRQUNyQyxNQUFNLEVBQUUsQ0FBQyxjQUFjLENBQUM7S0FDekIsQ0FBQztxQ0FTZSxpQkFBaUIsRUFBSyxVQUFVLEVBQWUsTUFBTTtHQUZ6RCxRQUFRLENBTXBCO1NBTlksUUFBUTsyRkFBUixRQUFRO2tCQU5wQixTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSxXQUFXO29CQUNyQixlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtvQkFDL0MsUUFBUSxFQUFFLDJCQUEyQjtvQkFDckMsTUFBTSxFQUFFLENBQUMsY0FBYyxDQUFDO2lCQUN6Qjs7SUFzQlksa0JBQWtCLFNBQWxCLGtCQUFrQjtJQUU3QixZQUFZLENBQW9CLEVBQUUsQ0FBYSxFQUFZLENBQVM7UUFBVCxNQUFDLEdBQUQsQ0FBQyxDQUFRO1FBQ2xFLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNYLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQztJQUM1QixDQUFDO0NBQ0YsQ0FBQTsrR0FOWSxrQkFBa0I7bUdBQWxCLGtCQUFrQixxR0FIbkIsMkJBQTJCO0FBRzFCLGtCQUFrQjtJQVY5QixRQUFRLENBQUM7UUFDUixxQkFBcUIsRUFBRSx3QkFBd0I7UUFDL0MsTUFBTSxFQUFFLENBQUMsYUFBYSxDQUFDO0tBQ3hCLENBQUM7cUNBU2UsaUJBQWlCLEVBQUssVUFBVSxFQUFlLE1BQU07R0FGekQsa0JBQWtCLENBTTlCO1NBTlksa0JBQWtCOzJGQUFsQixrQkFBa0I7a0JBTjlCLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLHVCQUF1QjtvQkFDakMsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07b0JBQy9DLFFBQVEsRUFBRSwyQkFBMkI7b0JBQ3JDLE1BQU0sRUFBRSxDQUFDLGFBQWEsQ0FBQztpQkFDeEI7O0lBc0JZLGlCQUFpQixTQUFqQixpQkFBaUI7SUFFNUIsWUFBWSxDQUFvQixFQUFFLENBQWEsRUFBWSxDQUFTO1FBQVQsTUFBQyxHQUFELENBQUMsQ0FBUTtRQUNsRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDWCxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUM7SUFDNUIsQ0FBQztDQUNGLENBQUE7OEdBTlksaUJBQWlCO2tHQUFqQixpQkFBaUIsMEtBSGxCLDJCQUEyQjtBQUcxQixpQkFBaUI7SUFWN0IsUUFBUSxDQUFDO1FBQ1IscUJBQXFCLEVBQUUsdUJBQXVCO1FBQzlDLE1BQU0sRUFBRSxDQUFDLGNBQWMsRUFBRSxtQkFBbUIsRUFBRSxhQUFhLENBQUM7S0FDN0QsQ0FBQztxQ0FTZSxpQkFBaUIsRUFBSyxVQUFVLEVBQWUsTUFBTTtHQUZ6RCxpQkFBaUIsQ0FNN0I7U0FOWSxpQkFBaUI7MkZBQWpCLGlCQUFpQjtrQkFON0IsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUsc0JBQXNCO29CQUNoQyxlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtvQkFDL0MsUUFBUSxFQUFFLDJCQUEyQjtvQkFDckMsTUFBTSxFQUFFLENBQUMsY0FBYyxFQUFFLG1CQUFtQixFQUFFLGFBQWEsQ0FBQztpQkFDN0Q7O0lBc0JZLGtCQUFrQixTQUFsQixrQkFBa0I7SUFFN0IsWUFBWSxDQUFvQixFQUFFLENBQWEsRUFBWSxDQUFTO1FBQVQsTUFBQyxHQUFELENBQUMsQ0FBUTtRQUNsRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDWCxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUM7SUFDNUIsQ0FBQztDQUNGLENBQUE7K0dBTlksa0JBQWtCO21HQUFsQixrQkFBa0IscUdBSG5CLDJCQUEyQjtBQUcxQixrQkFBa0I7SUFWOUIsUUFBUSxDQUFDO1FBQ1IscUJBQXFCLEVBQUUsd0JBQXdCO1FBQy9DLE1BQU0sRUFBRSxDQUFDLGFBQWEsQ0FBQztLQUN4QixDQUFDO3FDQVNlLGlCQUFpQixFQUFLLFVBQVUsRUFBZSxNQUFNO0dBRnpELGtCQUFrQixDQU05QjtTQU5ZLGtCQUFrQjsyRkFBbEIsa0JBQWtCO2tCQU45QixTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSx1QkFBdUI7b0JBQ2pDLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxRQUFRLEVBQUUsMkJBQTJCO29CQUNyQyxNQUFNLEVBQUUsQ0FBQyxhQUFhLENBQUM7aUJBQ3hCOztJQXNCWSxvQkFBb0IsU0FBcEIsb0JBQW9CO0lBRS9CLFlBQVksQ0FBb0IsRUFBRSxDQUFhLEVBQVksQ0FBUztRQUFULE1BQUMsR0FBRCxDQUFDLENBQVE7UUFDbEUsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ1gsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDO0lBQzVCLENBQUM7Q0FDRixDQUFBO2lIQU5ZLG9CQUFvQjtxR0FBcEIsb0JBQW9CLHVHQUhyQiwyQkFBMkI7QUFHMUIsb0JBQW9CO0lBVmhDLFFBQVEsQ0FBQztRQUNSLHFCQUFxQixFQUFFLDBCQUEwQjtRQUNqRCxNQUFNLEVBQUUsQ0FBQyxhQUFhLENBQUM7S0FDeEIsQ0FBQztxQ0FTZSxpQkFBaUIsRUFBSyxVQUFVLEVBQWUsTUFBTTtHQUZ6RCxvQkFBb0IsQ0FNaEM7U0FOWSxvQkFBb0I7MkZBQXBCLG9CQUFvQjtrQkFOaEMsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUseUJBQXlCO29CQUNuQyxlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtvQkFDL0MsUUFBUSxFQUFFLDJCQUEyQjtvQkFDckMsTUFBTSxFQUFFLENBQUMsYUFBYSxDQUFDO2lCQUN4Qjs7SUFzQlksY0FBYyxTQUFkLGNBQWM7SUFFekIsWUFBWSxDQUFvQixFQUFFLENBQWEsRUFBWSxDQUFTO1FBQVQsTUFBQyxHQUFELENBQUMsQ0FBUTtRQUNsRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDWCxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUM7SUFDNUIsQ0FBQztDQUNGLENBQUE7MkdBTlksY0FBYzsrRkFBZCxjQUFjLGdHQUhmLDJCQUEyQjtBQUcxQixjQUFjO0lBVjFCLFFBQVEsQ0FBQztRQUNSLHFCQUFxQixFQUFFLG9CQUFvQjtRQUMzQyxNQUFNLEVBQUUsQ0FBQyxhQUFhLENBQUM7S0FDeEIsQ0FBQztxQ0FTZSxpQkFBaUIsRUFBSyxVQUFVLEVBQWUsTUFBTTtHQUZ6RCxjQUFjLENBTTFCO1NBTlksY0FBYzsyRkFBZCxjQUFjO2tCQU4xQixTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSxrQkFBa0I7b0JBQzVCLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxRQUFRLEVBQUUsMkJBQTJCO29CQUNyQyxNQUFNLEVBQUUsQ0FBQyxhQUFhLENBQUM7aUJBQ3hCOztJQXNCWSxjQUFjLFNBQWQsY0FBYztJQUV6QixZQUFZLENBQW9CLEVBQUUsQ0FBYSxFQUFZLENBQVM7UUFBVCxNQUFDLEdBQUQsQ0FBQyxDQUFRO1FBQ2xFLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNYLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQztJQUM1QixDQUFDO0NBQ0YsQ0FBQTsyR0FOWSxjQUFjOytGQUFkLGNBQWMsa0ZBSGYsMkJBQTJCO0FBRzFCLGNBQWM7SUFWMUIsUUFBUSxDQUFDO1FBQ1IscUJBQXFCLEVBQUUsb0JBQW9CO1FBQzNDLE1BQU0sRUFBRSxDQUFDLE1BQU0sQ0FBQztLQUNqQixDQUFDO3FDQVNlLGlCQUFpQixFQUFLLFVBQVUsRUFBZSxNQUFNO0dBRnpELGNBQWMsQ0FNMUI7U0FOWSxjQUFjOzJGQUFkLGNBQWM7a0JBTjFCLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLGtCQUFrQjtvQkFDNUIsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07b0JBQy9DLFFBQVEsRUFBRSwyQkFBMkI7b0JBQ3JDLE1BQU0sRUFBRSxDQUFDLE1BQU0sQ0FBQztpQkFDakI7O0lBc0JZLGdCQUFnQixTQUFoQixnQkFBZ0I7SUFFM0IsWUFBWSxDQUFvQixFQUFFLENBQWEsRUFBWSxDQUFTO1FBQVQsTUFBQyxHQUFELENBQUMsQ0FBUTtRQUNsRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDWCxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUM7SUFDNUIsQ0FBQztDQUNGLENBQUE7NkdBTlksZ0JBQWdCO2lHQUFoQixnQkFBZ0IsOE5BSGpCLDJCQUEyQjtBQUcxQixnQkFBZ0I7SUFWNUIsUUFBUSxDQUFDO1FBQ1IscUJBQXFCLEVBQUUsc0JBQXNCO1FBQzdDLE1BQU0sRUFBRSxDQUFDLFdBQVcsRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLGNBQWMsRUFBRSxhQUFhLENBQUM7S0FDM0YsQ0FBQztxQ0FTZSxpQkFBaUIsRUFBSyxVQUFVLEVBQWUsTUFBTTtHQUZ6RCxnQkFBZ0IsQ0FNNUI7U0FOWSxnQkFBZ0I7MkZBQWhCLGdCQUFnQjtrQkFONUIsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUsb0JBQW9CO29CQUM5QixlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtvQkFDL0MsUUFBUSxFQUFFLDJCQUEyQjtvQkFDckMsTUFBTSxFQUFFLENBQUMsV0FBVyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sRUFBRSxVQUFVLEVBQUUsY0FBYyxFQUFFLGFBQWEsQ0FBQztpQkFDM0Y7O0lBdUJZLGlCQUFpQixTQUFqQixpQkFBaUI7SUFFNUIsWUFBWSxDQUFvQixFQUFFLENBQWEsRUFBWSxDQUFTO1FBQVQsTUFBQyxHQUFELENBQUMsQ0FBUTtRQUNsRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDWCxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUM7SUFDNUIsQ0FBQztDQUNGLENBQUE7OEdBTlksaUJBQWlCO2tHQUFqQixpQkFBaUIscUtBSGxCLDJCQUEyQjtBQUcxQixpQkFBaUI7SUFYN0IsUUFBUSxDQUFDO1FBQ1IscUJBQXFCLEVBQUUsdUJBQXVCO1FBQzlDLE1BQU0sRUFBRSxDQUFDLGVBQWUsRUFBRSxnQkFBZ0IsRUFBRSxhQUFhLENBQUM7UUFDMUQsT0FBTyxFQUFFLENBQUMsU0FBUyxFQUFFLGFBQWEsQ0FBQztLQUNwQyxDQUFDO3FDQVNlLGlCQUFpQixFQUFLLFVBQVUsRUFBZSxNQUFNO0dBRnpELGlCQUFpQixDQU03QjtTQU5ZLGlCQUFpQjsyRkFBakIsaUJBQWlCO2tCQU43QixTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSxxQkFBcUI7b0JBQy9CLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxRQUFRLEVBQUUsMkJBQTJCO29CQUNyQyxNQUFNLEVBQUUsQ0FBQyxlQUFlLEVBQUUsZ0JBQWdCLEVBQUUsYUFBYSxDQUFDO2lCQUMzRDs7SUFzQlksZUFBZSxTQUFmLGVBQWU7SUFFMUIsWUFBWSxDQUFvQixFQUFFLENBQWEsRUFBWSxDQUFTO1FBQVQsTUFBQyxHQUFELENBQUMsQ0FBUTtRQUNsRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDWCxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUM7SUFDNUIsQ0FBQztDQUNGLENBQUE7NEdBTlksZUFBZTtnR0FBZixlQUFlLHdRQUhoQiwyQkFBMkI7QUFHMUIsZUFBZTtJQVYzQixRQUFRLENBQUM7UUFDUixxQkFBcUIsRUFBRSxxQkFBcUI7UUFDNUMsTUFBTSxFQUFFLENBQUMsa0JBQWtCLEVBQUUsTUFBTSxFQUFFLFFBQVEsRUFBRSxjQUFjLEVBQUUsa0JBQWtCLEVBQUUsY0FBYyxFQUFFLGFBQWEsQ0FBQztLQUNsSCxDQUFDO3FDQVNlLGlCQUFpQixFQUFLLFVBQVUsRUFBZSxNQUFNO0dBRnpELGVBQWUsQ0FNM0I7U0FOWSxlQUFlOzJGQUFmLGVBQWU7a0JBTjNCLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLGtCQUFrQjtvQkFDNUIsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07b0JBQy9DLFFBQVEsRUFBRSwyQkFBMkI7b0JBQ3JDLE1BQU0sRUFBRSxDQUFDLGtCQUFrQixFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUUsY0FBYyxFQUFFLGtCQUFrQixFQUFFLGNBQWMsRUFBRSxhQUFhLENBQUM7aUJBQ2xIOztJQXNCWSxTQUFTLFNBQVQsU0FBUztJQUVwQixZQUFZLENBQW9CLEVBQUUsQ0FBYSxFQUFZLENBQVM7UUFBVCxNQUFDLEdBQUQsQ0FBQyxDQUFRO1FBQ2xFLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNYLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQztJQUM1QixDQUFDO0NBQ0YsQ0FBQTtzR0FOWSxTQUFTOzBGQUFULFNBQVMsMEZBSFYsMkJBQTJCO0FBRzFCLFNBQVM7SUFWckIsUUFBUSxDQUFDO1FBQ1IscUJBQXFCLEVBQUUsZUFBZTtRQUN0QyxNQUFNLEVBQUUsQ0FBQyxhQUFhLENBQUM7S0FDeEIsQ0FBQztxQ0FTZSxpQkFBaUIsRUFBSyxVQUFVLEVBQWUsTUFBTTtHQUZ6RCxTQUFTLENBTXJCO1NBTlksU0FBUzsyRkFBVCxTQUFTO2tCQU5yQixTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSxZQUFZO29CQUN0QixlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtvQkFDL0MsUUFBUSxFQUFFLDJCQUEyQjtvQkFDckMsTUFBTSxFQUFFLENBQUMsYUFBYSxDQUFDO2lCQUN4Qjs7SUE0QlksT0FBTyxTQUFQLE9BQU87SUFFbEIsWUFBWSxDQUFvQixFQUFFLENBQWEsRUFBWSxDQUFTO1FBQVQsTUFBQyxHQUFELENBQUMsQ0FBUTtRQUNsRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDWCxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUM7UUFDMUIsWUFBWSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsRUFBRSxFQUFFLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztJQUM3QyxDQUFDO0NBQ0YsQ0FBQTtvR0FQWSxPQUFPO3dGQUFQLE9BQU8sc0hBSFIsMkJBQTJCO0FBRzFCLE9BQU87SUFWbkIsUUFBUSxDQUFDO1FBQ1IscUJBQXFCLEVBQUUsYUFBYTtRQUNwQyxNQUFNLEVBQUUsQ0FBQyxVQUFVLEVBQUUsUUFBUSxFQUFFLFFBQVEsQ0FBQztLQUN6QyxDQUFDO3FDQVNlLGlCQUFpQixFQUFLLFVBQVUsRUFBZSxNQUFNO0dBRnpELE9BQU8sQ0FPbkI7U0FQWSxPQUFPOzJGQUFQLE9BQU87a0JBTm5CLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLFVBQVU7b0JBQ3BCLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxRQUFRLEVBQUUsMkJBQTJCO29CQUNyQyxNQUFNLEVBQUUsQ0FBQyxVQUFVLEVBQUUsUUFBUSxFQUFFLFFBQVEsQ0FBQztpQkFDekM7O0lBdUJZLGNBQWMsU0FBZCxjQUFjO0lBRXpCLFlBQVksQ0FBb0IsRUFBRSxDQUFhLEVBQVksQ0FBUztRQUFULE1BQUMsR0FBRCxDQUFDLENBQVE7UUFDbEUsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ1gsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDO0lBQzVCLENBQUM7Q0FDRixDQUFBOzJHQU5ZLGNBQWM7K0ZBQWQsY0FBYywrRkFIZiwyQkFBMkI7QUFHMUIsY0FBYztJQVYxQixRQUFRLENBQUM7UUFDUixxQkFBcUIsRUFBRSxvQkFBb0I7UUFDM0MsTUFBTSxFQUFFLENBQUMsYUFBYSxDQUFDO0tBQ3hCLENBQUM7cUNBU2UsaUJBQWlCLEVBQUssVUFBVSxFQUFlLE1BQU07R0FGekQsY0FBYyxDQU0xQjtTQU5ZLGNBQWM7MkZBQWQsY0FBYztrQkFOMUIsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUsaUJBQWlCO29CQUMzQixlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtvQkFDL0MsUUFBUSxFQUFFLDJCQUEyQjtvQkFDckMsTUFBTSxFQUFFLENBQUMsYUFBYSxDQUFDO2lCQUN4Qjs7SUFvQlksNEJBQTRCLFNBQTVCLDRCQUE0QjtJQUV2QyxZQUFZLENBQW9CLEVBQUUsQ0FBYSxFQUFZLENBQVM7UUFBVCxNQUFDLEdBQUQsQ0FBQyxDQUFRO1FBQ2xFLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNYLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQztJQUM1QixDQUFDO0NBQ0YsQ0FBQTt5SEFOWSw0QkFBNEI7NkdBQTVCLDRCQUE0Qix1RUFGN0IsMkJBQTJCO0FBRTFCLDRCQUE0QjtJQVJ4QyxRQUFRLENBQUM7UUFDUixxQkFBcUIsRUFBRSxrQ0FBa0M7S0FDMUQsQ0FBQztxQ0FRZSxpQkFBaUIsRUFBSyxVQUFVLEVBQWUsTUFBTTtHQUZ6RCw0QkFBNEIsQ0FNeEM7U0FOWSw0QkFBNEI7MkZBQTVCLDRCQUE0QjtrQkFMeEMsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUsaUNBQWlDO29CQUMzQyxlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtvQkFDL0MsUUFBUSxFQUFFLDJCQUEyQjtpQkFDdEM7O0lBc0JZLGtCQUFrQixTQUFsQixrQkFBa0I7SUFFN0IsWUFBWSxDQUFvQixFQUFFLENBQWEsRUFBWSxDQUFTO1FBQVQsTUFBQyxHQUFELENBQUMsQ0FBUTtRQUNsRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDWCxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUM7SUFDNUIsQ0FBQztDQUNGLENBQUE7K0dBTlksa0JBQWtCO21HQUFsQixrQkFBa0Isb0dBSG5CLDJCQUEyQjtBQUcxQixrQkFBa0I7SUFWOUIsUUFBUSxDQUFDO1FBQ1IscUJBQXFCLEVBQUUsd0JBQXdCO1FBQy9DLE1BQU0sRUFBRSxDQUFDLGFBQWEsQ0FBQztLQUN4QixDQUFDO3FDQVNlLGlCQUFpQixFQUFLLFVBQVUsRUFBZSxNQUFNO0dBRnpELGtCQUFrQixDQU05QjtTQU5ZLGtCQUFrQjsyRkFBbEIsa0JBQWtCO2tCQU45QixTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSxzQkFBc0I7b0JBQ2hDLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxRQUFRLEVBQUUsMkJBQTJCO29CQUNyQyxNQUFNLEVBQUUsQ0FBQyxhQUFhLENBQUM7aUJBQ3hCOztJQW9CWSxzQkFBc0IsU0FBdEIsc0JBQXNCO0lBRWpDLFlBQVksQ0FBb0IsRUFBRSxDQUFhLEVBQVksQ0FBUztRQUFULE1BQUMsR0FBRCxDQUFDLENBQVE7UUFDbEUsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ1gsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDO0lBQzVCLENBQUM7Q0FDRixDQUFBO21IQU5ZLHNCQUFzQjt1R0FBdEIsc0JBQXNCLGdFQUZ2QiwyQkFBMkI7QUFFMUIsc0JBQXNCO0lBUmxDLFFBQVEsQ0FBQztRQUNSLHFCQUFxQixFQUFFLDRCQUE0QjtLQUNwRCxDQUFDO3FDQVFlLGlCQUFpQixFQUFLLFVBQVUsRUFBZSxNQUFNO0dBRnpELHNCQUFzQixDQU1sQztTQU5ZLHNCQUFzQjsyRkFBdEIsc0JBQXNCO2tCQUxsQyxTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSwwQkFBMEI7b0JBQ3BDLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxRQUFRLEVBQUUsMkJBQTJCO2lCQUN0Qzs7SUFzQlksa0JBQWtCLFNBQWxCLGtCQUFrQjtJQUU3QixZQUFZLENBQW9CLEVBQUUsQ0FBYSxFQUFZLENBQVM7UUFBVCxNQUFDLEdBQUQsQ0FBQyxDQUFRO1FBQ2xFLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNYLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQztJQUM1QixDQUFDO0NBQ0YsQ0FBQTsrR0FOWSxrQkFBa0I7bUdBQWxCLGtCQUFrQiw4SEFIbkIsMkJBQTJCO0FBRzFCLGtCQUFrQjtJQVY5QixRQUFRLENBQUM7UUFDUixxQkFBcUIsRUFBRSx3QkFBd0I7UUFDL0MsTUFBTSxFQUFFLENBQUMsWUFBWSxFQUFFLGFBQWEsQ0FBQztLQUN0QyxDQUFDO3FDQVNlLGlCQUFpQixFQUFLLFVBQVUsRUFBZSxNQUFNO0dBRnpELGtCQUFrQixDQU05QjtTQU5ZLGtCQUFrQjsyRkFBbEIsa0JBQWtCO2tCQU45QixTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSxzQkFBc0I7b0JBQ2hDLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxRQUFRLEVBQUUsMkJBQTJCO29CQUNyQyxNQUFNLEVBQUUsQ0FBQyxZQUFZLEVBQUUsYUFBYSxDQUFDO2lCQUN0Qzs7SUEwQ1ksMEJBQTBCLFNBQTFCLDBCQUEwQjtJQUVyQyxZQUFZLENBQW9CLEVBQUUsQ0FBYSxFQUFZLENBQVM7UUFBVCxNQUFDLEdBQUQsQ0FBQyxDQUFRO1FBQ2xFLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNYLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQztRQUMxQixZQUFZLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxFQUFFLEVBQUUsQ0FBQyxZQUFZLEVBQUUsV0FBVyxFQUFFLFlBQVksRUFBRSxXQUFXLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQztJQUNwRyxDQUFDO0NBQ0YsQ0FBQTt1SEFQWSwwQkFBMEI7MkdBQTFCLDBCQUEwQixxRUFGM0IsMkJBQTJCO0FBRTFCLDBCQUEwQjtJQVJ0QyxRQUFRLENBQUM7UUFDUixxQkFBcUIsRUFBRSxnQ0FBZ0M7S0FDeEQsQ0FBQztxQ0FRZSxpQkFBaUIsRUFBSyxVQUFVLEVBQWUsTUFBTTtHQUZ6RCwwQkFBMEIsQ0FPdEM7U0FQWSwwQkFBMEI7MkZBQTFCLDBCQUEwQjtrQkFMdEMsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUsK0JBQStCO29CQUN6QyxlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtvQkFDL0MsUUFBUSxFQUFFLDJCQUEyQjtpQkFDdEM7O0lBaUNZLDBCQUEwQixTQUExQiwwQkFBMEI7SUFFckMsWUFBWSxDQUFvQixFQUFFLENBQWEsRUFBWSxDQUFTO1FBQVQsTUFBQyxHQUFELENBQUMsQ0FBUTtRQUNsRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDWCxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUM7UUFDMUIsWUFBWSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsRUFBRSxFQUFFLENBQUMsa0JBQWtCLEVBQUUsaUJBQWlCLENBQUMsQ0FBQyxDQUFDO0lBQ3ZFLENBQUM7Q0FDRixDQUFBO3VIQVBZLDBCQUEwQjsyR0FBMUIsMEJBQTBCLHdIQUgzQiwyQkFBMkI7QUFHMUIsMEJBQTBCO0lBVnRDLFFBQVEsQ0FBQztRQUNSLHFCQUFxQixFQUFFLGdDQUFnQztRQUN2RCxNQUFNLEVBQUUsQ0FBQyxrQkFBa0IsQ0FBQztLQUM3QixDQUFDO3FDQVNlLGlCQUFpQixFQUFLLFVBQVUsRUFBZSxNQUFNO0dBRnpELDBCQUEwQixDQU90QztTQVBZLDBCQUEwQjsyRkFBMUIsMEJBQTBCO2tCQU50QyxTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSxnQ0FBZ0M7b0JBQzFDLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxRQUFRLEVBQUUsMkJBQTJCO29CQUNyQyxNQUFNLEVBQUUsQ0FBQyxrQkFBa0IsQ0FBQztpQkFDN0I7O0lBaUNZLDBCQUEwQixTQUExQiwwQkFBMEI7SUFFckMsWUFBWSxDQUFvQixFQUFFLENBQWEsRUFBWSxDQUFTO1FBQVQsTUFBQyxHQUFELENBQUMsQ0FBUTtRQUNsRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDWCxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUM7UUFDMUIsWUFBWSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsRUFBRSxFQUFFLENBQUMsa0JBQWtCLEVBQUUsaUJBQWlCLENBQUMsQ0FBQyxDQUFDO0lBQ3ZFLENBQUM7Q0FDRixDQUFBO3VIQVBZLDBCQUEwQjsyR0FBMUIsMEJBQTBCLHdIQUgzQiwyQkFBMkI7QUFHMUIsMEJBQTBCO0lBVnRDLFFBQVEsQ0FBQztRQUNSLHFCQUFxQixFQUFFLGdDQUFnQztRQUN2RCxNQUFNLEVBQUUsQ0FBQyxrQkFBa0IsQ0FBQztLQUM3QixDQUFDO3FDQVNlLGlCQUFpQixFQUFLLFVBQVUsRUFBZSxNQUFNO0dBRnpELDBCQUEwQixDQU90QztTQVBZLDBCQUEwQjsyRkFBMUIsMEJBQTBCO2tCQU50QyxTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSxnQ0FBZ0M7b0JBQzFDLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxRQUFRLEVBQUUsMkJBQTJCO29CQUNyQyxNQUFNLEVBQUUsQ0FBQyxrQkFBa0IsQ0FBQztpQkFDN0I7O0lBc0NZLDRCQUE0QixTQUE1Qiw0QkFBNEI7SUFFdkMsWUFBWSxDQUFvQixFQUFFLENBQWEsRUFBWSxDQUFTO1FBQVQsTUFBQyxHQUFELENBQUMsQ0FBUTtRQUNsRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDWCxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUM7UUFDMUIsWUFBWSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsRUFBRSxFQUFFLENBQUMsWUFBWSxFQUFFLGVBQWUsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDLENBQUM7SUFDbEYsQ0FBQztDQUNGLENBQUE7eUhBUFksNEJBQTRCOzZHQUE1Qiw0QkFBNEIsNkhBSDdCLDJCQUEyQjtBQUcxQiw0QkFBNEI7SUFWeEMsUUFBUSxDQUFDO1FBQ1IscUJBQXFCLEVBQUUsa0NBQWtDO1FBQ3pELE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRSxXQUFXLENBQUM7S0FDaEMsQ0FBQztxQ0FTZSxpQkFBaUIsRUFBSyxVQUFVLEVBQWUsTUFBTTtHQUZ6RCw0QkFBNEIsQ0FPeEM7U0FQWSw0QkFBNEI7MkZBQTVCLDRCQUE0QjtrQkFOeEMsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUsaUNBQWlDO29CQUMzQyxlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtvQkFDL0MsUUFBUSxFQUFFLDJCQUEyQjtvQkFDckMsTUFBTSxFQUFFLENBQUMsUUFBUSxFQUFFLFdBQVcsQ0FBQztpQkFDaEM7O0lBdUVZLHNCQUFzQixTQUF0QixzQkFBc0I7SUFFakMsWUFBWSxDQUFvQixFQUFFLENBQWEsRUFBWSxDQUFTO1FBQVQsTUFBQyxHQUFELENBQUMsQ0FBUTtRQUNsRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDWCxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUM7UUFDMUIsWUFBWSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsRUFBRSxFQUFFLENBQUMsU0FBUyxFQUFFLFdBQVcsRUFBRSxTQUFTLEVBQUUsV0FBVyxFQUFFLGNBQWMsRUFBRSxRQUFRLEVBQUUsVUFBVSxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFBRSxhQUFhLENBQUMsQ0FBQyxDQUFDO0lBQ3JMLENBQUM7Q0FDRixDQUFBO21IQVBZLHNCQUFzQjt1R0FBdEIsc0JBQXNCLGlFQUZ2QiwyQkFBMkI7QUFFMUIsc0JBQXNCO0lBUmxDLFFBQVEsQ0FBQztRQUNSLHFCQUFxQixFQUFFLDRCQUE0QjtLQUNwRCxDQUFDO3FDQVFlLGlCQUFpQixFQUFLLFVBQVUsRUFBZSxNQUFNO0dBRnpELHNCQUFzQixDQU9sQztTQVBZLHNCQUFzQjsyRkFBdEIsc0JBQXNCO2tCQUxsQyxTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSwyQkFBMkI7b0JBQ3JDLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxRQUFRLEVBQUUsMkJBQTJCO2lCQUN0Qzs7SUF1RVksb0JBQW9CLFNBQXBCLG9CQUFvQjtJQUUvQixZQUFZLENBQW9CLEVBQUUsQ0FBYSxFQUFZLENBQVM7UUFBVCxNQUFDLEdBQUQsQ0FBQyxDQUFRO1FBQ2xFLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNYLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQztRQUMxQixZQUFZLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxFQUFFLEVBQUUsQ0FBQyxTQUFTLEVBQUUsV0FBVyxFQUFFLFNBQVMsRUFBRSxXQUFXLEVBQUUsY0FBYyxFQUFFLFFBQVEsRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFLFdBQVcsQ0FBQyxDQUFDLENBQUM7SUFDbkwsQ0FBQztDQUNGLENBQUE7aUhBUFksb0JBQW9CO3FHQUFwQixvQkFBb0IsK0RBRnJCLDJCQUEyQjtBQUUxQixvQkFBb0I7SUFSaEMsUUFBUSxDQUFDO1FBQ1IscUJBQXFCLEVBQUUsMEJBQTBCO0tBQ2xELENBQUM7cUNBUWUsaUJBQWlCLEVBQUssVUFBVSxFQUFlLE1BQU07R0FGekQsb0JBQW9CLENBT2hDO1NBUFksb0JBQW9COzJGQUFwQixvQkFBb0I7a0JBTGhDLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLHlCQUF5QjtvQkFDbkMsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07b0JBQy9DLFFBQVEsRUFBRSwyQkFBMkI7aUJBQ3RDOztJQTZCWSx3QkFBd0IsU0FBeEIsd0JBQXdCO0lBRW5DLFlBQVksQ0FBb0IsRUFBRSxDQUFhLEVBQVksQ0FBUztRQUFULE1BQUMsR0FBRCxDQUFDLENBQVE7UUFDbEUsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ1gsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDO1FBQzFCLFlBQVksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLEVBQUUsRUFBRSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7SUFDOUMsQ0FBQztDQUNGLENBQUE7cUhBUFksd0JBQXdCO3lHQUF4Qix3QkFBd0IscUhBSHpCLDJCQUEyQjtBQUcxQix3QkFBd0I7SUFWcEMsUUFBUSxDQUFDO1FBQ1IscUJBQXFCLEVBQUUsOEJBQThCO1FBQ3JELE1BQU0sRUFBRSxDQUFDLE1BQU0sRUFBRSxXQUFXLENBQUM7S0FDOUIsQ0FBQztxQ0FTZSxpQkFBaUIsRUFBSyxVQUFVLEVBQWUsTUFBTTtHQUZ6RCx3QkFBd0IsQ0FPcEM7U0FQWSx3QkFBd0I7MkZBQXhCLHdCQUF3QjtrQkFOcEMsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUsNkJBQTZCO29CQUN2QyxlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtvQkFDL0MsUUFBUSxFQUFFLDJCQUEyQjtvQkFDckMsTUFBTSxFQUFFLENBQUMsTUFBTSxFQUFFLFdBQVcsQ0FBQztpQkFDOUI7O0lBMkJZLHNCQUFzQixTQUF0QixzQkFBc0I7SUFFakMsWUFBWSxDQUFvQixFQUFFLENBQWEsRUFBWSxDQUFTO1FBQVQsTUFBQyxHQUFELENBQUMsQ0FBUTtRQUNsRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDWCxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUM7UUFDMUIsWUFBWSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsRUFBRSxFQUFFLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztJQUM5QyxDQUFDO0NBQ0YsQ0FBQTttSEFQWSxzQkFBc0I7dUdBQXRCLHNCQUFzQixpRUFGdkIsMkJBQTJCO0FBRTFCLHNCQUFzQjtJQVJsQyxRQUFRLENBQUM7UUFDUixxQkFBcUIsRUFBRSw0QkFBNEI7S0FDcEQsQ0FBQztxQ0FRZSxpQkFBaUIsRUFBSyxVQUFVLEVBQWUsTUFBTTtHQUZ6RCxzQkFBc0IsQ0FPbEM7U0FQWSxzQkFBc0I7MkZBQXRCLHNCQUFzQjtrQkFMbEMsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUsMkJBQTJCO29CQUNyQyxlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtvQkFDL0MsUUFBUSxFQUFFLDJCQUEyQjtpQkFDdEMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiB0c2xpbnQ6ZGlzYWJsZSAqL1xuLyogYXV0by1nZW5lcmF0ZWQgYW5ndWxhciBkaXJlY3RpdmUgcHJveGllcyAqL1xuaW1wb3J0IHsgQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3ksIENoYW5nZURldGVjdG9yUmVmLCBDb21wb25lbnQsIEVsZW1lbnRSZWYsIEV2ZW50RW1pdHRlciwgTmdab25lIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBQcm94eUNtcCwgcHJveHlPdXRwdXRzIH0gZnJvbSAnLi9hbmd1bGFyLWNvbXBvbmVudC1saWIvdXRpbHMnO1xuXG5pbXBvcnQgdHlwZSB7IENvbXBvbmVudHMgfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMnO1xuXG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lCdXR0b24gfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS1idXR0b24uanMnO1xuaW1wb3J0IHsgZGVmaW5lQ3VzdG9tRWxlbWVudCBhcyBkZWZpbmVEaWdpQ2FsZW5kYXIgfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS1jYWxlbmRhci5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lDYWxlbmRhcldlZWtWaWV3IH0gZnJvbSAnQGRpZ2kvc2tvbHZlcmtldC9jb21wb25lbnRzL2RpZ2ktY2FsZW5kYXItd2Vlay12aWV3LmpzJztcbmltcG9ydCB7IGRlZmluZUN1c3RvbUVsZW1lbnQgYXMgZGVmaW5lRGlnaUNhcmRCb3ggfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS1jYXJkLWJveC5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lDYXJkTGluayB9IGZyb20gJ0BkaWdpL3Nrb2x2ZXJrZXQvY29tcG9uZW50cy9kaWdpLWNhcmQtbGluay5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lDb2RlIH0gZnJvbSAnQGRpZ2kvc2tvbHZlcmtldC9jb21wb25lbnRzL2RpZ2ktY29kZS5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lDb2RlQmxvY2sgfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS1jb2RlLWJsb2NrLmpzJztcbmltcG9ydCB7IGRlZmluZUN1c3RvbUVsZW1lbnQgYXMgZGVmaW5lRGlnaUNvZGVFeGFtcGxlIH0gZnJvbSAnQGRpZ2kvc2tvbHZlcmtldC9jb21wb25lbnRzL2RpZ2ktY29kZS1leGFtcGxlLmpzJztcbmltcG9ydCB7IGRlZmluZUN1c3RvbUVsZW1lbnQgYXMgZGVmaW5lRGlnaURpYWxvZyB9IGZyb20gJ0BkaWdpL3Nrb2x2ZXJrZXQvY29tcG9uZW50cy9kaWdpLWRpYWxvZy5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lFeHBhbmRhYmxlQWNjb3JkaW9uIH0gZnJvbSAnQGRpZ2kvc2tvbHZlcmtldC9jb21wb25lbnRzL2RpZ2ktZXhwYW5kYWJsZS1hY2NvcmRpb24uanMnO1xuaW1wb3J0IHsgZGVmaW5lQ3VzdG9tRWxlbWVudCBhcyBkZWZpbmVEaWdpRm9ybUNoZWNrYm94IH0gZnJvbSAnQGRpZ2kvc2tvbHZlcmtldC9jb21wb25lbnRzL2RpZ2ktZm9ybS1jaGVja2JveC5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lGb3JtRmllbGRzZXQgfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS1mb3JtLWZpZWxkc2V0LmpzJztcbmltcG9ydCB7IGRlZmluZUN1c3RvbUVsZW1lbnQgYXMgZGVmaW5lRGlnaUZvcm1GaWxlVXBsb2FkIH0gZnJvbSAnQGRpZ2kvc2tvbHZlcmtldC9jb21wb25lbnRzL2RpZ2ktZm9ybS1maWxlLXVwbG9hZC5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lGb3JtRmlsdGVyIH0gZnJvbSAnQGRpZ2kvc2tvbHZlcmtldC9jb21wb25lbnRzL2RpZ2ktZm9ybS1maWx0ZXIuanMnO1xuaW1wb3J0IHsgZGVmaW5lQ3VzdG9tRWxlbWVudCBhcyBkZWZpbmVEaWdpRm9ybUlucHV0IH0gZnJvbSAnQGRpZ2kvc2tvbHZlcmtldC9jb21wb25lbnRzL2RpZ2ktZm9ybS1pbnB1dC5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lGb3JtSW5wdXRTZWFyY2ggfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS1mb3JtLWlucHV0LXNlYXJjaC5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lGb3JtTGFiZWwgfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS1mb3JtLWxhYmVsLmpzJztcbmltcG9ydCB7IGRlZmluZUN1c3RvbUVsZW1lbnQgYXMgZGVmaW5lRGlnaUZvcm1Qcm9jZXNzU3RlcCB9IGZyb20gJ0BkaWdpL3Nrb2x2ZXJrZXQvY29tcG9uZW50cy9kaWdpLWZvcm0tcHJvY2Vzcy1zdGVwLmpzJztcbmltcG9ydCB7IGRlZmluZUN1c3RvbUVsZW1lbnQgYXMgZGVmaW5lRGlnaUZvcm1Qcm9jZXNzU3RlcHMgfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS1mb3JtLXByb2Nlc3Mtc3RlcHMuanMnO1xuaW1wb3J0IHsgZGVmaW5lQ3VzdG9tRWxlbWVudCBhcyBkZWZpbmVEaWdpRm9ybVJhZGlvYnV0dG9uIH0gZnJvbSAnQGRpZ2kvc2tvbHZlcmtldC9jb21wb25lbnRzL2RpZ2ktZm9ybS1yYWRpb2J1dHRvbi5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lGb3JtUmFkaW9ncm91cCB9IGZyb20gJ0BkaWdpL3Nrb2x2ZXJrZXQvY29tcG9uZW50cy9kaWdpLWZvcm0tcmFkaW9ncm91cC5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lGb3JtU2VsZWN0IH0gZnJvbSAnQGRpZ2kvc2tvbHZlcmtldC9jb21wb25lbnRzL2RpZ2ktZm9ybS1zZWxlY3QuanMnO1xuaW1wb3J0IHsgZGVmaW5lQ3VzdG9tRWxlbWVudCBhcyBkZWZpbmVEaWdpRm9ybVRleHRhcmVhIH0gZnJvbSAnQGRpZ2kvc2tvbHZlcmtldC9jb21wb25lbnRzL2RpZ2ktZm9ybS10ZXh0YXJlYS5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lGb3JtVmFsaWRhdGlvbk1lc3NhZ2UgfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS1mb3JtLXZhbGlkYXRpb24tbWVzc2FnZS5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lJY29uIH0gZnJvbSAnQGRpZ2kvc2tvbHZlcmtldC9jb21wb25lbnRzL2RpZ2ktaWNvbi5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lJY29uQmFycyB9IGZyb20gJ0BkaWdpL3Nrb2x2ZXJrZXQvY29tcG9uZW50cy9kaWdpLWljb24tYmFycy5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lJY29uQ2hlY2sgfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS1pY29uLWNoZWNrLmpzJztcbmltcG9ydCB7IGRlZmluZUN1c3RvbUVsZW1lbnQgYXMgZGVmaW5lRGlnaUljb25DaGVja0NpcmNsZSB9IGZyb20gJ0BkaWdpL3Nrb2x2ZXJrZXQvY29tcG9uZW50cy9kaWdpLWljb24tY2hlY2stY2lyY2xlLmpzJztcbmltcG9ydCB7IGRlZmluZUN1c3RvbUVsZW1lbnQgYXMgZGVmaW5lRGlnaUljb25DaGVja0NpcmNsZVJlZyB9IGZyb20gJ0BkaWdpL3Nrb2x2ZXJrZXQvY29tcG9uZW50cy9kaWdpLWljb24tY2hlY2stY2lyY2xlLXJlZy5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lJY29uQ2hlY2tDaXJjbGVSZWdBbHQgfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS1pY29uLWNoZWNrLWNpcmNsZS1yZWctYWx0LmpzJztcbmltcG9ydCB7IGRlZmluZUN1c3RvbUVsZW1lbnQgYXMgZGVmaW5lRGlnaUljb25DaGV2cm9uRG93biB9IGZyb20gJ0BkaWdpL3Nrb2x2ZXJrZXQvY29tcG9uZW50cy9kaWdpLWljb24tY2hldnJvbi1kb3duLmpzJztcbmltcG9ydCB7IGRlZmluZUN1c3RvbUVsZW1lbnQgYXMgZGVmaW5lRGlnaUljb25DaGV2cm9uTGVmdCB9IGZyb20gJ0BkaWdpL3Nrb2x2ZXJrZXQvY29tcG9uZW50cy9kaWdpLWljb24tY2hldnJvbi1sZWZ0LmpzJztcbmltcG9ydCB7IGRlZmluZUN1c3RvbUVsZW1lbnQgYXMgZGVmaW5lRGlnaUljb25DaGV2cm9uUmlnaHQgfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS1pY29uLWNoZXZyb24tcmlnaHQuanMnO1xuaW1wb3J0IHsgZGVmaW5lQ3VzdG9tRWxlbWVudCBhcyBkZWZpbmVEaWdpSWNvbkNoZXZyb25VcCB9IGZyb20gJ0BkaWdpL3Nrb2x2ZXJrZXQvY29tcG9uZW50cy9kaWdpLWljb24tY2hldnJvbi11cC5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lJY29uQ29weSB9IGZyb20gJ0BkaWdpL3Nrb2x2ZXJrZXQvY29tcG9uZW50cy9kaWdpLWljb24tY29weS5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lJY29uRGFuZ2VyT3V0bGluZSB9IGZyb20gJ0BkaWdpL3Nrb2x2ZXJrZXQvY29tcG9uZW50cy9kaWdpLWljb24tZGFuZ2VyLW91dGxpbmUuanMnO1xuaW1wb3J0IHsgZGVmaW5lQ3VzdG9tRWxlbWVudCBhcyBkZWZpbmVEaWdpSWNvbkRvd25sb2FkIH0gZnJvbSAnQGRpZ2kvc2tvbHZlcmtldC9jb21wb25lbnRzL2RpZ2ktaWNvbi1kb3dubG9hZC5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lJY29uRXhjbGFtYXRpb25DaXJjbGUgfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS1pY29uLWV4Y2xhbWF0aW9uLWNpcmNsZS5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lJY29uRXhjbGFtYXRpb25DaXJjbGVGaWxsZWQgfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS1pY29uLWV4Y2xhbWF0aW9uLWNpcmNsZS1maWxsZWQuanMnO1xuaW1wb3J0IHsgZGVmaW5lQ3VzdG9tRWxlbWVudCBhcyBkZWZpbmVEaWdpSWNvbkV4Y2xhbWF0aW9uVHJpYW5nbGUgfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS1pY29uLWV4Y2xhbWF0aW9uLXRyaWFuZ2xlLmpzJztcbmltcG9ydCB7IGRlZmluZUN1c3RvbUVsZW1lbnQgYXMgZGVmaW5lRGlnaUljb25FeGNsYW1hdGlvblRyaWFuZ2xlV2FybmluZyB9IGZyb20gJ0BkaWdpL3Nrb2x2ZXJrZXQvY29tcG9uZW50cy9kaWdpLWljb24tZXhjbGFtYXRpb24tdHJpYW5nbGUtd2FybmluZy5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lJY29uRXh0ZXJuYWxMaW5rQWx0IH0gZnJvbSAnQGRpZ2kvc2tvbHZlcmtldC9jb21wb25lbnRzL2RpZ2ktaWNvbi1leHRlcm5hbC1saW5rLWFsdC5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lJY29uTWludXMgfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS1pY29uLW1pbnVzLmpzJztcbmltcG9ydCB7IGRlZmluZUN1c3RvbUVsZW1lbnQgYXMgZGVmaW5lRGlnaUljb25QYXBlcmNsaXAgfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS1pY29uLXBhcGVyY2xpcC5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lJY29uUGx1cyB9IGZyb20gJ0BkaWdpL3Nrb2x2ZXJrZXQvY29tcG9uZW50cy9kaWdpLWljb24tcGx1cy5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lJY29uU2VhcmNoIH0gZnJvbSAnQGRpZ2kvc2tvbHZlcmtldC9jb21wb25lbnRzL2RpZ2ktaWNvbi1zZWFyY2guanMnO1xuaW1wb3J0IHsgZGVmaW5lQ3VzdG9tRWxlbWVudCBhcyBkZWZpbmVEaWdpSWNvblNwaW5uZXIgfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS1pY29uLXNwaW5uZXIuanMnO1xuaW1wb3J0IHsgZGVmaW5lQ3VzdG9tRWxlbWVudCBhcyBkZWZpbmVEaWdpSWNvblRyYXNoIH0gZnJvbSAnQGRpZ2kvc2tvbHZlcmtldC9jb21wb25lbnRzL2RpZ2ktaWNvbi10cmFzaC5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lJY29uWCB9IGZyb20gJ0BkaWdpL3Nrb2x2ZXJrZXQvY29tcG9uZW50cy9kaWdpLWljb24teC5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lMYXlvdXRCbG9jayB9IGZyb20gJ0BkaWdpL3Nrb2x2ZXJrZXQvY29tcG9uZW50cy9kaWdpLWxheW91dC1ibG9jay5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lMYXlvdXRDb2x1bW5zIH0gZnJvbSAnQGRpZ2kvc2tvbHZlcmtldC9jb21wb25lbnRzL2RpZ2ktbGF5b3V0LWNvbHVtbnMuanMnO1xuaW1wb3J0IHsgZGVmaW5lQ3VzdG9tRWxlbWVudCBhcyBkZWZpbmVEaWdpTGF5b3V0Q29udGFpbmVyIH0gZnJvbSAnQGRpZ2kvc2tvbHZlcmtldC9jb21wb25lbnRzL2RpZ2ktbGF5b3V0LWNvbnRhaW5lci5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lMYXlvdXRHcmlkIH0gZnJvbSAnQGRpZ2kvc2tvbHZlcmtldC9jb21wb25lbnRzL2RpZ2ktbGF5b3V0LWdyaWQuanMnO1xuaW1wb3J0IHsgZGVmaW5lQ3VzdG9tRWxlbWVudCBhcyBkZWZpbmVEaWdpTGF5b3V0TWVkaWFPYmplY3QgfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS1sYXlvdXQtbWVkaWEtb2JqZWN0LmpzJztcbmltcG9ydCB7IGRlZmluZUN1c3RvbUVsZW1lbnQgYXMgZGVmaW5lRGlnaUxheW91dFJvd3MgfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS1sYXlvdXQtcm93cy5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lMYXlvdXRTdGFja2VkQmxvY2tzIH0gZnJvbSAnQGRpZ2kvc2tvbHZlcmtldC9jb21wb25lbnRzL2RpZ2ktbGF5b3V0LXN0YWNrZWQtYmxvY2tzLmpzJztcbmltcG9ydCB7IGRlZmluZUN1c3RvbUVsZW1lbnQgYXMgZGVmaW5lRGlnaUxpbmsgfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS1saW5rLmpzJztcbmltcG9ydCB7IGRlZmluZUN1c3RvbUVsZW1lbnQgYXMgZGVmaW5lRGlnaUxpbmtFeHRlcm5hbCB9IGZyb20gJ0BkaWdpL3Nrb2x2ZXJrZXQvY29tcG9uZW50cy9kaWdpLWxpbmstZXh0ZXJuYWwuanMnO1xuaW1wb3J0IHsgZGVmaW5lQ3VzdG9tRWxlbWVudCBhcyBkZWZpbmVEaWdpTGlua0ljb24gfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS1saW5rLWljb24uanMnO1xuaW1wb3J0IHsgZGVmaW5lQ3VzdG9tRWxlbWVudCBhcyBkZWZpbmVEaWdpTGlua0ludGVybmFsIH0gZnJvbSAnQGRpZ2kvc2tvbHZlcmtldC9jb21wb25lbnRzL2RpZ2ktbGluay1pbnRlcm5hbC5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lMaXN0TGluayB9IGZyb20gJ0BkaWdpL3Nrb2x2ZXJrZXQvY29tcG9uZW50cy9kaWdpLWxpc3QtbGluay5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lMb2FkZXJTcGlubmVyIH0gZnJvbSAnQGRpZ2kvc2tvbHZlcmtldC9jb21wb25lbnRzL2RpZ2ktbG9hZGVyLXNwaW5uZXIuanMnO1xuaW1wb3J0IHsgZGVmaW5lQ3VzdG9tRWxlbWVudCBhcyBkZWZpbmVEaWdpTG9nbyB9IGZyb20gJ0BkaWdpL3Nrb2x2ZXJrZXQvY29tcG9uZW50cy9kaWdpLWxvZ28uanMnO1xuaW1wb3J0IHsgZGVmaW5lQ3VzdG9tRWxlbWVudCBhcyBkZWZpbmVEaWdpTG9nb1NlcnZpY2UgfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS1sb2dvLXNlcnZpY2UuanMnO1xuaW1wb3J0IHsgZGVmaW5lQ3VzdG9tRWxlbWVudCBhcyBkZWZpbmVEaWdpTG9nb1Npc3RlciB9IGZyb20gJ0BkaWdpL3Nrb2x2ZXJrZXQvY29tcG9uZW50cy9kaWdpLWxvZ28tc2lzdGVyLmpzJztcbmltcG9ydCB7IGRlZmluZUN1c3RvbUVsZW1lbnQgYXMgZGVmaW5lRGlnaU1lZGlhRmlndXJlIH0gZnJvbSAnQGRpZ2kvc2tvbHZlcmtldC9jb21wb25lbnRzL2RpZ2ktbWVkaWEtZmlndXJlLmpzJztcbmltcG9ydCB7IGRlZmluZUN1c3RvbUVsZW1lbnQgYXMgZGVmaW5lRGlnaU1lZGlhSW1hZ2UgfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS1tZWRpYS1pbWFnZS5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lOYXZpZ2F0aW9uQnJlYWRjcnVtYnMgfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS1uYXZpZ2F0aW9uLWJyZWFkY3J1bWJzLmpzJztcbmltcG9ydCB7IGRlZmluZUN1c3RvbUVsZW1lbnQgYXMgZGVmaW5lRGlnaU5hdmlnYXRpb25Db250ZXh0TWVudSB9IGZyb20gJ0BkaWdpL3Nrb2x2ZXJrZXQvY29tcG9uZW50cy9kaWdpLW5hdmlnYXRpb24tY29udGV4dC1tZW51LmpzJztcbmltcG9ydCB7IGRlZmluZUN1c3RvbUVsZW1lbnQgYXMgZGVmaW5lRGlnaU5hdmlnYXRpb25Db250ZXh0TWVudUl0ZW0gfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS1uYXZpZ2F0aW9uLWNvbnRleHQtbWVudS1pdGVtLmpzJztcbmltcG9ydCB7IGRlZmluZUN1c3RvbUVsZW1lbnQgYXMgZGVmaW5lRGlnaU5hdmlnYXRpb25NYWluTWVudSB9IGZyb20gJ0BkaWdpL3Nrb2x2ZXJrZXQvY29tcG9uZW50cy9kaWdpLW5hdmlnYXRpb24tbWFpbi1tZW51LmpzJztcbmltcG9ydCB7IGRlZmluZUN1c3RvbUVsZW1lbnQgYXMgZGVmaW5lRGlnaU5hdmlnYXRpb25NYWluTWVudVBhbmVsIH0gZnJvbSAnQGRpZ2kvc2tvbHZlcmtldC9jb21wb25lbnRzL2RpZ2ktbmF2aWdhdGlvbi1tYWluLW1lbnUtcGFuZWwuanMnO1xuaW1wb3J0IHsgZGVmaW5lQ3VzdG9tRWxlbWVudCBhcyBkZWZpbmVEaWdpTmF2aWdhdGlvblBhZ2luYXRpb24gfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS1uYXZpZ2F0aW9uLXBhZ2luYXRpb24uanMnO1xuaW1wb3J0IHsgZGVmaW5lQ3VzdG9tRWxlbWVudCBhcyBkZWZpbmVEaWdpTmF2aWdhdGlvblNpZGViYXIgfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS1uYXZpZ2F0aW9uLXNpZGViYXIuanMnO1xuaW1wb3J0IHsgZGVmaW5lQ3VzdG9tRWxlbWVudCBhcyBkZWZpbmVEaWdpTmF2aWdhdGlvblNpZGViYXJCdXR0b24gfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS1uYXZpZ2F0aW9uLXNpZGViYXItYnV0dG9uLmpzJztcbmltcG9ydCB7IGRlZmluZUN1c3RvbUVsZW1lbnQgYXMgZGVmaW5lRGlnaU5hdmlnYXRpb25UYWIgfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS1uYXZpZ2F0aW9uLXRhYi5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lOYXZpZ2F0aW9uVGFiSW5BQm94IH0gZnJvbSAnQGRpZ2kvc2tvbHZlcmtldC9jb21wb25lbnRzL2RpZ2ktbmF2aWdhdGlvbi10YWItaW4tYS1ib3guanMnO1xuaW1wb3J0IHsgZGVmaW5lQ3VzdG9tRWxlbWVudCBhcyBkZWZpbmVEaWdpTmF2aWdhdGlvblRhYnMgfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS1uYXZpZ2F0aW9uLXRhYnMuanMnO1xuaW1wb3J0IHsgZGVmaW5lQ3VzdG9tRWxlbWVudCBhcyBkZWZpbmVEaWdpTmF2aWdhdGlvblRvYyB9IGZyb20gJ0BkaWdpL3Nrb2x2ZXJrZXQvY29tcG9uZW50cy9kaWdpLW5hdmlnYXRpb24tdG9jLmpzJztcbmltcG9ydCB7IGRlZmluZUN1c3RvbUVsZW1lbnQgYXMgZGVmaW5lRGlnaU5hdmlnYXRpb25WZXJ0aWNhbE1lbnUgfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS1uYXZpZ2F0aW9uLXZlcnRpY2FsLW1lbnUuanMnO1xuaW1wb3J0IHsgZGVmaW5lQ3VzdG9tRWxlbWVudCBhcyBkZWZpbmVEaWdpTmF2aWdhdGlvblZlcnRpY2FsTWVudUl0ZW0gfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS1uYXZpZ2F0aW9uLXZlcnRpY2FsLW1lbnUtaXRlbS5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lOb3RpZmljYXRpb25BbGVydCB9IGZyb20gJ0BkaWdpL3Nrb2x2ZXJrZXQvY29tcG9uZW50cy9kaWdpLW5vdGlmaWNhdGlvbi1hbGVydC5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lOb3RpZmljYXRpb25Db29raWUgfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS1ub3RpZmljYXRpb24tY29va2llLmpzJztcbmltcG9ydCB7IGRlZmluZUN1c3RvbUVsZW1lbnQgYXMgZGVmaW5lRGlnaU5vdGlmaWNhdGlvbkRldGFpbCB9IGZyb20gJ0BkaWdpL3Nrb2x2ZXJrZXQvY29tcG9uZW50cy9kaWdpLW5vdGlmaWNhdGlvbi1kZXRhaWwuanMnO1xuaW1wb3J0IHsgZGVmaW5lQ3VzdG9tRWxlbWVudCBhcyBkZWZpbmVEaWdpUGFnZSB9IGZyb20gJ0BkaWdpL3Nrb2x2ZXJrZXQvY29tcG9uZW50cy9kaWdpLXBhZ2UuanMnO1xuaW1wb3J0IHsgZGVmaW5lQ3VzdG9tRWxlbWVudCBhcyBkZWZpbmVEaWdpUGFnZUJsb2NrQ2FyZHMgfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS1wYWdlLWJsb2NrLWNhcmRzLmpzJztcbmltcG9ydCB7IGRlZmluZUN1c3RvbUVsZW1lbnQgYXMgZGVmaW5lRGlnaVBhZ2VCbG9ja0hlcm8gfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS1wYWdlLWJsb2NrLWhlcm8uanMnO1xuaW1wb3J0IHsgZGVmaW5lQ3VzdG9tRWxlbWVudCBhcyBkZWZpbmVEaWdpUGFnZUJsb2NrTGlzdHMgfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS1wYWdlLWJsb2NrLWxpc3RzLmpzJztcbmltcG9ydCB7IGRlZmluZUN1c3RvbUVsZW1lbnQgYXMgZGVmaW5lRGlnaVBhZ2VCbG9ja1NpZGViYXIgfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS1wYWdlLWJsb2NrLXNpZGViYXIuanMnO1xuaW1wb3J0IHsgZGVmaW5lQ3VzdG9tRWxlbWVudCBhcyBkZWZpbmVEaWdpUGFnZUZvb3RlciB9IGZyb20gJ0BkaWdpL3Nrb2x2ZXJrZXQvY29tcG9uZW50cy9kaWdpLXBhZ2UtZm9vdGVyLmpzJztcbmltcG9ydCB7IGRlZmluZUN1c3RvbUVsZW1lbnQgYXMgZGVmaW5lRGlnaVBhZ2VIZWFkZXIgfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS1wYWdlLWhlYWRlci5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lQcm9ncmVzc1N0ZXAgfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS1wcm9ncmVzcy1zdGVwLmpzJztcbmltcG9ydCB7IGRlZmluZUN1c3RvbUVsZW1lbnQgYXMgZGVmaW5lRGlnaVByb2dyZXNzU3RlcHMgfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS1wcm9ncmVzcy1zdGVwcy5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lQcm9ncmVzc2JhciB9IGZyb20gJ0BkaWdpL3Nrb2x2ZXJrZXQvY29tcG9uZW50cy9kaWdpLXByb2dyZXNzYmFyLmpzJztcbmltcG9ydCB7IGRlZmluZUN1c3RvbUVsZW1lbnQgYXMgZGVmaW5lRGlnaVRhYmxlIH0gZnJvbSAnQGRpZ2kvc2tvbHZlcmtldC9jb21wb25lbnRzL2RpZ2ktdGFibGUuanMnO1xuaW1wb3J0IHsgZGVmaW5lQ3VzdG9tRWxlbWVudCBhcyBkZWZpbmVEaWdpVGFnIH0gZnJvbSAnQGRpZ2kvc2tvbHZlcmtldC9jb21wb25lbnRzL2RpZ2ktdGFnLmpzJztcbmltcG9ydCB7IGRlZmluZUN1c3RvbUVsZW1lbnQgYXMgZGVmaW5lRGlnaVR5cG9ncmFwaHkgfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS10eXBvZ3JhcGh5LmpzJztcbmltcG9ydCB7IGRlZmluZUN1c3RvbUVsZW1lbnQgYXMgZGVmaW5lRGlnaVR5cG9ncmFwaHlIZWFkaW5nU2VjdGlvbiB9IGZyb20gJ0BkaWdpL3Nrb2x2ZXJrZXQvY29tcG9uZW50cy9kaWdpLXR5cG9ncmFwaHktaGVhZGluZy1zZWN0aW9uLmpzJztcbmltcG9ydCB7IGRlZmluZUN1c3RvbUVsZW1lbnQgYXMgZGVmaW5lRGlnaVR5cG9ncmFwaHlNZXRhIH0gZnJvbSAnQGRpZ2kvc2tvbHZlcmtldC9jb21wb25lbnRzL2RpZ2ktdHlwb2dyYXBoeS1tZXRhLmpzJztcbmltcG9ydCB7IGRlZmluZUN1c3RvbUVsZW1lbnQgYXMgZGVmaW5lRGlnaVR5cG9ncmFwaHlQcmVhbWJsZSB9IGZyb20gJ0BkaWdpL3Nrb2x2ZXJrZXQvY29tcG9uZW50cy9kaWdpLXR5cG9ncmFwaHktcHJlYW1ibGUuanMnO1xuaW1wb3J0IHsgZGVmaW5lQ3VzdG9tRWxlbWVudCBhcyBkZWZpbmVEaWdpVHlwb2dyYXBoeVRpbWUgfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS10eXBvZ3JhcGh5LXRpbWUuanMnO1xuaW1wb3J0IHsgZGVmaW5lQ3VzdG9tRWxlbWVudCBhcyBkZWZpbmVEaWdpVXRpbEJyZWFrcG9pbnRPYnNlcnZlciB9IGZyb20gJ0BkaWdpL3Nrb2x2ZXJrZXQvY29tcG9uZW50cy9kaWdpLXV0aWwtYnJlYWtwb2ludC1vYnNlcnZlci5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lVdGlsRGV0ZWN0Q2xpY2tPdXRzaWRlIH0gZnJvbSAnQGRpZ2kvc2tvbHZlcmtldC9jb21wb25lbnRzL2RpZ2ktdXRpbC1kZXRlY3QtY2xpY2stb3V0c2lkZS5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lVdGlsRGV0ZWN0Rm9jdXNPdXRzaWRlIH0gZnJvbSAnQGRpZ2kvc2tvbHZlcmtldC9jb21wb25lbnRzL2RpZ2ktdXRpbC1kZXRlY3QtZm9jdXMtb3V0c2lkZS5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lVdGlsSW50ZXJzZWN0aW9uT2JzZXJ2ZXIgfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS11dGlsLWludGVyc2VjdGlvbi1vYnNlcnZlci5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lVdGlsS2V5ZG93bkhhbmRsZXIgfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS11dGlsLWtleWRvd24taGFuZGxlci5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lVdGlsS2V5dXBIYW5kbGVyIH0gZnJvbSAnQGRpZ2kvc2tvbHZlcmtldC9jb21wb25lbnRzL2RpZ2ktdXRpbC1rZXl1cC1oYW5kbGVyLmpzJztcbmltcG9ydCB7IGRlZmluZUN1c3RvbUVsZW1lbnQgYXMgZGVmaW5lRGlnaVV0aWxNdXRhdGlvbk9ic2VydmVyIH0gZnJvbSAnQGRpZ2kvc2tvbHZlcmtldC9jb21wb25lbnRzL2RpZ2ktdXRpbC1tdXRhdGlvbi1vYnNlcnZlci5qcyc7XG5pbXBvcnQgeyBkZWZpbmVDdXN0b21FbGVtZW50IGFzIGRlZmluZURpZ2lVdGlsUmVzaXplT2JzZXJ2ZXIgfSBmcm9tICdAZGlnaS9za29sdmVya2V0L2NvbXBvbmVudHMvZGlnaS11dGlsLXJlc2l6ZS1vYnNlcnZlci5qcyc7XG5cblxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIERpZ2lCdXR0b24gZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lCdXR0b24ge1xuICAvKipcbiAgICogQnV0dG9uZWxlbWVudGV0cyAnb25jbGljayctZXZlbnQuIEBlbiBUaGUgYnV0dG9uIGVsZW1lbnQncyAnb25jbGljaycgZXZlbnQuXG4gICAqL1xuICBhZk9uQ2xpY2s6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxNb3VzZUV2ZW50Pj47XG4gIC8qKlxuICAgKiBCdXR0b25lbGVtZW50ZXRzICdvbmZvY3VzJy1ldmVudC4gQGVuIFRoZSBidXR0b24gZWxlbWVudCdzICdvbmZvY3VzJyBldmVudC5cbiAgICovXG4gIGFmT25Gb2N1czogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PEZvY3VzRXZlbnQ+PjtcbiAgLyoqXG4gICAqIEJ1dHRvbmVsZW1lbnRldHMgJ29uYmx1cictZXZlbnQuIEBlbiBUaGUgYnV0dG9uIGVsZW1lbnQncyAnb25ibHVyJyBldmVudC5cbiAgICovXG4gIGFmT25CbHVyOiBFdmVudEVtaXR0ZXI8Q3VzdG9tRXZlbnQ8YW55Pj47XG5cbn1cblxuQFByb3h5Q21wKHtcbiAgZGVmaW5lQ3VzdG9tRWxlbWVudEZuOiBkZWZpbmVEaWdpQnV0dG9uLFxuICBpbnB1dHM6IFsnYWZBcmlhQ29udHJvbHMnLCAnYWZBcmlhRXhwYW5kZWQnLCAnYWZBcmlhSGFzcG9wdXAnLCAnYWZBcmlhTGFiZWwnLCAnYWZBcmlhTGFiZWxsZWRieScsICdhZkFyaWFQcmVzc2VkJywgJ2FmQXV0b2ZvY3VzJywgJ2FmRGlyJywgJ2FmRm9ybScsICdhZkZ1bGxXaWR0aCcsICdhZklkJywgJ2FmTGFuZycsICdhZlNpemUnLCAnYWZUYWJpbmRleCcsICdhZlR5cGUnLCAnYWZWYXJpYXRpb24nXSxcbiAgbWV0aG9kczogWydhZk1HZXRCdXR0b25FbGVtZW50J11cbn0pXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdkaWdpLWJ1dHRvbicsXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxuICB0ZW1wbGF0ZTogJzxuZy1jb250ZW50PjwvbmctY29udGVudD4nLFxuICBpbnB1dHM6IFsnYWZBcmlhQ29udHJvbHMnLCAnYWZBcmlhRXhwYW5kZWQnLCAnYWZBcmlhSGFzcG9wdXAnLCAnYWZBcmlhTGFiZWwnLCAnYWZBcmlhTGFiZWxsZWRieScsICdhZkFyaWFQcmVzc2VkJywgJ2FmQXV0b2ZvY3VzJywgJ2FmRGlyJywgJ2FmRm9ybScsICdhZkZ1bGxXaWR0aCcsICdhZklkJywgJ2FmTGFuZycsICdhZlNpemUnLCAnYWZUYWJpbmRleCcsICdhZlR5cGUnLCAnYWZWYXJpYXRpb24nXVxufSlcbmV4cG9ydCBjbGFzcyBEaWdpQnV0dG9uIHtcbiAgcHJvdGVjdGVkIGVsOiBIVE1MRWxlbWVudDtcbiAgY29uc3RydWN0b3IoYzogQ2hhbmdlRGV0ZWN0b3JSZWYsIHI6IEVsZW1lbnRSZWYsIHByb3RlY3RlZCB6OiBOZ1pvbmUpIHtcbiAgICBjLmRldGFjaCgpO1xuICAgIHRoaXMuZWwgPSByLm5hdGl2ZUVsZW1lbnQ7XG4gICAgcHJveHlPdXRwdXRzKHRoaXMsIHRoaXMuZWwsIFsnYWZPbkNsaWNrJywgJ2FmT25Gb2N1cycsICdhZk9uQmx1ciddKTtcbiAgfVxufVxuXG5cbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBEaWdpQ2FsZW5kYXIgZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lDYWxlbmRhciB7XG4gIC8qKlxuICAgKiBTa2VyIG7DpHIgZXR0IGRhdHVtIGhhciB2YWx0cyBlbGxlciBhdnZhbHRzLiBSZXR1cm5lcmFyIGRhdHVtZW4gaSBlbiBhcnJheS4gQGVuIFdoZW4gYSBkYXRlIGlzIHNlbGVjdGVkLiBSZXR1cm4gdGhlIGRhdGVzIGluIGFuIGFycmF5LlxuICAgKi9cbiAgYWZPbkRhdGVTZWxlY3RlZENoYW5nZTogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PGFueT4+O1xuICAvKipcbiAgICogU2tlciBuw6RyIGZva3VzIGZseXR0YXMgdXRhbmbDtnIga2FsZW5kZXJuIEBlbiBXaGVuIGZvY3VzIG1vdmVzIG91dCBmcm9tIHRoZSBjYWxlbmRhclxuICAgKi9cbiAgYWZPbkZvY3VzT3V0c2lkZTogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PGFueT4+O1xuICAvKipcbiAgICogU2tlciB2aWQga2xpY2sgdXRhbmbDtnIga2FsZW5kZXJuIEBlbiBXaGVuIGNsaWNrIG91dHNpZGUgdGhlIGNhbGVuZGFyXG4gICAqL1xuICBhZk9uQ2xpY2tPdXRzaWRlOiBFdmVudEVtaXR0ZXI8Q3VzdG9tRXZlbnQ8YW55Pj47XG4gIC8qKlxuICAgKiBTa2VyIG7DpHIga2FsZW5kZXJkYXR1bWVuIGhhciByw7ZydHMgZsO2cnN0YSBnw6VuZ2VuLCBuw6RyIHbDpHJkZXQgcMOlIGZvY3VzZWREYXRlIGhhciDDpG5kcmF0cyBmw7Zyc3RhIGfDpW5nZW4uIEBlbiBXaGVuIHRoZSBjYWxlbmRhciBkYXRlcyBhcmUgdG91Y2hlZCB0aGUgZmlyc3QgdGltZSwgd2hlbiBmb2N1c2VkRGF0ZSBpcyBjaGFuZ2VkIHRoZSBmaXJzdCB0aW1lLlxuICAgKi9cbiAgYWZPbkRpcnR5OiBFdmVudEVtaXR0ZXI8Q3VzdG9tRXZlbnQ8YW55Pj47XG5cbn1cblxuQFByb3h5Q21wKHtcbiAgZGVmaW5lQ3VzdG9tRWxlbWVudEZuOiBkZWZpbmVEaWdpQ2FsZW5kYXIsXG4gIGlucHV0czogWydhZkFjdGl2ZScsICdhZklkJywgJ2FmSW5pdFNlbGVjdGVkTW9udGgnLCAnYWZNdWx0aXBsZURhdGVzJywgJ2FmU2VsZWN0ZWREYXRlJywgJ2FmU2hvd1dlZWtOdW1iZXInXVxufSlcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2RpZ2ktY2FsZW5kYXInLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaCxcbiAgdGVtcGxhdGU6ICc8bmctY29udGVudD48L25nLWNvbnRlbnQ+JyxcbiAgaW5wdXRzOiBbJ2FmQWN0aXZlJywgJ2FmSWQnLCAnYWZJbml0U2VsZWN0ZWRNb250aCcsICdhZk11bHRpcGxlRGF0ZXMnLCAnYWZTZWxlY3RlZERhdGUnLCAnYWZTaG93V2Vla051bWJlciddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lDYWxlbmRhciB7XG4gIHByb3RlY3RlZCBlbDogSFRNTEVsZW1lbnQ7XG4gIGNvbnN0cnVjdG9yKGM6IENoYW5nZURldGVjdG9yUmVmLCByOiBFbGVtZW50UmVmLCBwcm90ZWN0ZWQgejogTmdab25lKSB7XG4gICAgYy5kZXRhY2goKTtcbiAgICB0aGlzLmVsID0gci5uYXRpdmVFbGVtZW50O1xuICAgIHByb3h5T3V0cHV0cyh0aGlzLCB0aGlzLmVsLCBbJ2FmT25EYXRlU2VsZWN0ZWRDaGFuZ2UnLCAnYWZPbkZvY3VzT3V0c2lkZScsICdhZk9uQ2xpY2tPdXRzaWRlJywgJ2FmT25EaXJ0eSddKTtcbiAgfVxufVxuXG5cbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBEaWdpQ2FsZW5kYXJXZWVrVmlldyBleHRlbmRzIENvbXBvbmVudHMuRGlnaUNhbGVuZGFyV2Vla1ZpZXcge1xuICAvKipcbiAgICogVmlkIGJ5dGUgYXYgdmVja2EgQGVuIFdoZW4gd2VlayBjaGFuZ2VzXG4gICAqL1xuICBhZk9uV2Vla0NoYW5nZTogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PHN0cmluZz4+O1xuICAvKipcbiAgICogVmlkIGJ5dGUgYXYgZGFnIEBlbiBXaGVuIGRheSBjaGFuZ2VzXG4gICAqL1xuICBhZk9uRGF0ZUNoYW5nZTogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PHN0cmluZz4+O1xuXG59XG5cbkBQcm94eUNtcCh7XG4gIGRlZmluZUN1c3RvbUVsZW1lbnRGbjogZGVmaW5lRGlnaUNhbGVuZGFyV2Vla1ZpZXcsXG4gIGlucHV0czogWydhZkRhdGVzJywgJ2FmSGVhZGluZ0xldmVsJywgJ2FmSWQnLCAnYWZNYXhXZWVrJywgJ2FmTWluV2VlayddLFxuICBtZXRob2RzOiBbJ2FmTVNldEFjdGl2ZURhdGUnXVxufSlcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2RpZ2ktY2FsZW5kYXItd2Vlay12aWV3JyxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXG4gIHRlbXBsYXRlOiAnPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PicsXG4gIGlucHV0czogWydhZkRhdGVzJywgJ2FmSGVhZGluZ0xldmVsJywgJ2FmSWQnLCAnYWZNYXhXZWVrJywgJ2FmTWluV2VlayddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lDYWxlbmRhcldlZWtWaWV3IHtcbiAgcHJvdGVjdGVkIGVsOiBIVE1MRWxlbWVudDtcbiAgY29uc3RydWN0b3IoYzogQ2hhbmdlRGV0ZWN0b3JSZWYsIHI6IEVsZW1lbnRSZWYsIHByb3RlY3RlZCB6OiBOZ1pvbmUpIHtcbiAgICBjLmRldGFjaCgpO1xuICAgIHRoaXMuZWwgPSByLm5hdGl2ZUVsZW1lbnQ7XG4gICAgcHJveHlPdXRwdXRzKHRoaXMsIHRoaXMuZWwsIFsnYWZPbldlZWtDaGFuZ2UnLCAnYWZPbkRhdGVDaGFuZ2UnXSk7XG4gIH1cbn1cblxuXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgRGlnaUNhcmRCb3ggZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lDYXJkQm94IHt9XG5cbkBQcm94eUNtcCh7XG4gIGRlZmluZUN1c3RvbUVsZW1lbnRGbjogZGVmaW5lRGlnaUNhcmRCb3gsXG4gIGlucHV0czogWydhZkd1dHRlcicsICdhZldpZHRoJ11cbn0pXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdkaWdpLWNhcmQtYm94JyxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXG4gIHRlbXBsYXRlOiAnPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PicsXG4gIGlucHV0czogWydhZkd1dHRlcicsICdhZldpZHRoJ11cbn0pXG5leHBvcnQgY2xhc3MgRGlnaUNhcmRCb3gge1xuICBwcm90ZWN0ZWQgZWw6IEhUTUxFbGVtZW50O1xuICBjb25zdHJ1Y3RvcihjOiBDaGFuZ2VEZXRlY3RvclJlZiwgcjogRWxlbWVudFJlZiwgcHJvdGVjdGVkIHo6IE5nWm9uZSkge1xuICAgIGMuZGV0YWNoKCk7XG4gICAgdGhpcy5lbCA9IHIubmF0aXZlRWxlbWVudDtcbiAgfVxufVxuXG5cbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBEaWdpQ2FyZExpbmsgZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lDYXJkTGluayB7fVxuXG5AUHJveHlDbXAoe1xuICBkZWZpbmVDdXN0b21FbGVtZW50Rm46IGRlZmluZURpZ2lDYXJkTGlua1xufSlcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2RpZ2ktY2FyZC1saW5rJyxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXG4gIHRlbXBsYXRlOiAnPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50Pidcbn0pXG5leHBvcnQgY2xhc3MgRGlnaUNhcmRMaW5rIHtcbiAgcHJvdGVjdGVkIGVsOiBIVE1MRWxlbWVudDtcbiAgY29uc3RydWN0b3IoYzogQ2hhbmdlRGV0ZWN0b3JSZWYsIHI6IEVsZW1lbnRSZWYsIHByb3RlY3RlZCB6OiBOZ1pvbmUpIHtcbiAgICBjLmRldGFjaCgpO1xuICAgIHRoaXMuZWwgPSByLm5hdGl2ZUVsZW1lbnQ7XG4gIH1cbn1cblxuXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgRGlnaUNvZGUgZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lDb2RlIHt9XG5cbkBQcm94eUNtcCh7XG4gIGRlZmluZUN1c3RvbUVsZW1lbnRGbjogZGVmaW5lRGlnaUNvZGUsXG4gIGlucHV0czogWydhZkNvZGUnLCAnYWZMYW5nJywgJ2FmTGFuZ3VhZ2UnLCAnYWZWYXJpYXRpb24nXVxufSlcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2RpZ2ktY29kZScsXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxuICB0ZW1wbGF0ZTogJzxuZy1jb250ZW50PjwvbmctY29udGVudD4nLFxuICBpbnB1dHM6IFsnYWZDb2RlJywgJ2FmTGFuZycsICdhZkxhbmd1YWdlJywgJ2FmVmFyaWF0aW9uJ11cbn0pXG5leHBvcnQgY2xhc3MgRGlnaUNvZGUge1xuICBwcm90ZWN0ZWQgZWw6IEhUTUxFbGVtZW50O1xuICBjb25zdHJ1Y3RvcihjOiBDaGFuZ2VEZXRlY3RvclJlZiwgcjogRWxlbWVudFJlZiwgcHJvdGVjdGVkIHo6IE5nWm9uZSkge1xuICAgIGMuZGV0YWNoKCk7XG4gICAgdGhpcy5lbCA9IHIubmF0aXZlRWxlbWVudDtcbiAgfVxufVxuXG5cbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBEaWdpQ29kZUJsb2NrIGV4dGVuZHMgQ29tcG9uZW50cy5EaWdpQ29kZUJsb2NrIHt9XG5cbkBQcm94eUNtcCh7XG4gIGRlZmluZUN1c3RvbUVsZW1lbnRGbjogZGVmaW5lRGlnaUNvZGVCbG9jayxcbiAgaW5wdXRzOiBbJ2FmQ29kZScsICdhZkhpZGVUb29sYmFyJywgJ2FmTGFuZycsICdhZkxhbmd1YWdlJywgJ2FmVmFyaWF0aW9uJ11cbn0pXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdkaWdpLWNvZGUtYmxvY2snLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaCxcbiAgdGVtcGxhdGU6ICc8bmctY29udGVudD48L25nLWNvbnRlbnQ+JyxcbiAgaW5wdXRzOiBbJ2FmQ29kZScsICdhZkhpZGVUb29sYmFyJywgJ2FmTGFuZycsICdhZkxhbmd1YWdlJywgJ2FmVmFyaWF0aW9uJ11cbn0pXG5leHBvcnQgY2xhc3MgRGlnaUNvZGVCbG9jayB7XG4gIHByb3RlY3RlZCBlbDogSFRNTEVsZW1lbnQ7XG4gIGNvbnN0cnVjdG9yKGM6IENoYW5nZURldGVjdG9yUmVmLCByOiBFbGVtZW50UmVmLCBwcm90ZWN0ZWQgejogTmdab25lKSB7XG4gICAgYy5kZXRhY2goKTtcbiAgICB0aGlzLmVsID0gci5uYXRpdmVFbGVtZW50O1xuICB9XG59XG5cblxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIERpZ2lDb2RlRXhhbXBsZSBleHRlbmRzIENvbXBvbmVudHMuRGlnaUNvZGVFeGFtcGxlIHt9XG5cbkBQcm94eUNtcCh7XG4gIGRlZmluZUN1c3RvbUVsZW1lbnRGbjogZGVmaW5lRGlnaUNvZGVFeGFtcGxlLFxuICBpbnB1dHM6IFsnYWZDb2RlJywgJ2FmQ29kZUJsb2NrVmFyaWF0aW9uJywgJ2FmRXhhbXBsZVZhcmlhdGlvbicsICdhZkhpZGVDb2RlJywgJ2FmSGlkZUNvbnRyb2xzJywgJ2FmTGFuZ3VhZ2UnXVxufSlcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2RpZ2ktY29kZS1leGFtcGxlJyxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXG4gIHRlbXBsYXRlOiAnPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PicsXG4gIGlucHV0czogWydhZkNvZGUnLCAnYWZDb2RlQmxvY2tWYXJpYXRpb24nLCAnYWZFeGFtcGxlVmFyaWF0aW9uJywgJ2FmSGlkZUNvZGUnLCAnYWZIaWRlQ29udHJvbHMnLCAnYWZMYW5ndWFnZSddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lDb2RlRXhhbXBsZSB7XG4gIHByb3RlY3RlZCBlbDogSFRNTEVsZW1lbnQ7XG4gIGNvbnN0cnVjdG9yKGM6IENoYW5nZURldGVjdG9yUmVmLCByOiBFbGVtZW50UmVmLCBwcm90ZWN0ZWQgejogTmdab25lKSB7XG4gICAgYy5kZXRhY2goKTtcbiAgICB0aGlzLmVsID0gci5uYXRpdmVFbGVtZW50O1xuICB9XG59XG5cblxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIERpZ2lEaWFsb2cgZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lEaWFsb2cge1xuICAvKipcbiAgICogIFxuICAgKi9cbiAgYWZPbk9wZW46IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxhbnk+PjtcbiAgLyoqXG4gICAqICBcbiAgICovXG4gIGFmT25DbG9zZTogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PGFueT4+O1xuXG59XG5cbkBQcm94eUNtcCh7XG4gIGRlZmluZUN1c3RvbUVsZW1lbnRGbjogZGVmaW5lRGlnaURpYWxvZyxcbiAgaW5wdXRzOiBbJ2FmSGlkZUNsb3NlQnV0dG9uJywgJ2FmT3BlbiddLFxuICBtZXRob2RzOiBbJ3Nob3dNb2RhbCcsICdjbG9zZSddXG59KVxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGlnaS1kaWFsb2cnLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaCxcbiAgdGVtcGxhdGU6ICc8bmctY29udGVudD48L25nLWNvbnRlbnQ+JyxcbiAgaW5wdXRzOiBbJ2FmSGlkZUNsb3NlQnV0dG9uJywgJ2FmT3BlbiddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lEaWFsb2cge1xuICBwcm90ZWN0ZWQgZWw6IEhUTUxFbGVtZW50O1xuICBjb25zdHJ1Y3RvcihjOiBDaGFuZ2VEZXRlY3RvclJlZiwgcjogRWxlbWVudFJlZiwgcHJvdGVjdGVkIHo6IE5nWm9uZSkge1xuICAgIGMuZGV0YWNoKCk7XG4gICAgdGhpcy5lbCA9IHIubmF0aXZlRWxlbWVudDtcbiAgICBwcm94eU91dHB1dHModGhpcywgdGhpcy5lbCwgWydhZk9uT3BlbicsICdhZk9uQ2xvc2UnXSk7XG4gIH1cbn1cblxuXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgRGlnaUV4cGFuZGFibGVBY2NvcmRpb24gZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lFeHBhbmRhYmxlQWNjb3JkaW9uIHtcbiAgLyoqXG4gICAqIEJ1dHRvbmVsZW1lbnRldHMgJ29uY2xpY2snLWV2ZW50LiBAZW4gVGhlIGJ1dHRvbiBlbGVtZW50J3MgJ29uY2xpY2snIGV2ZW50LlxuICAgKi9cbiAgYWZPbkNsaWNrOiBFdmVudEVtaXR0ZXI8Q3VzdG9tRXZlbnQ8TW91c2VFdmVudD4+O1xuXG59XG5cbkBQcm94eUNtcCh7XG4gIGRlZmluZUN1c3RvbUVsZW1lbnRGbjogZGVmaW5lRGlnaUV4cGFuZGFibGVBY2NvcmRpb24sXG4gIGlucHV0czogWydhZkFuaW1hdGlvbicsICdhZkV4cGFuZGVkJywgJ2FmSGVhZGluZycsICdhZkhlYWRpbmdMZXZlbCcsICdhZklkJywgJ2FmVmFyaWF0aW9uJ11cbn0pXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdkaWdpLWV4cGFuZGFibGUtYWNjb3JkaW9uJyxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXG4gIHRlbXBsYXRlOiAnPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PicsXG4gIGlucHV0czogWydhZkFuaW1hdGlvbicsICdhZkV4cGFuZGVkJywgJ2FmSGVhZGluZycsICdhZkhlYWRpbmdMZXZlbCcsICdhZklkJywgJ2FmVmFyaWF0aW9uJ11cbn0pXG5leHBvcnQgY2xhc3MgRGlnaUV4cGFuZGFibGVBY2NvcmRpb24ge1xuICBwcm90ZWN0ZWQgZWw6IEhUTUxFbGVtZW50O1xuICBjb25zdHJ1Y3RvcihjOiBDaGFuZ2VEZXRlY3RvclJlZiwgcjogRWxlbWVudFJlZiwgcHJvdGVjdGVkIHo6IE5nWm9uZSkge1xuICAgIGMuZGV0YWNoKCk7XG4gICAgdGhpcy5lbCA9IHIubmF0aXZlRWxlbWVudDtcbiAgICBwcm94eU91dHB1dHModGhpcywgdGhpcy5lbCwgWydhZk9uQ2xpY2snXSk7XG4gIH1cbn1cblxuXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgRGlnaUZvcm1DaGVja2JveCBleHRlbmRzIENvbXBvbmVudHMuRGlnaUZvcm1DaGVja2JveCB7XG4gIC8qKlxuICAgKiBJbnB1dGVsZW1lbnRldHMgJ29uY2hhbmdlJy1ldmVudC4gQGVuIFRoZSBpbnB1dCBlbGVtZW50J3MgJ29uY2hhbmdlJyBldmVudC5cbiAgICovXG4gIGFmT25DaGFuZ2U6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxhbnk+PjtcbiAgLyoqXG4gICAqIElucHV0ZWxlbWVudGV0cyAnb25ibHVyJy1ldmVudC4gQGVuIFRoZSBpbnB1dCBlbGVtZW50J3MgJ29uYmx1cicgZXZlbnQuXG4gICAqL1xuICBhZk9uQmx1cjogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PGFueT4+O1xuICAvKipcbiAgICogSW5wdXRlbGVtZW50ZXRzICdvbmZvY3VzJy1ldmVudC4gQGVuIFRoZSBpbnB1dCBlbGVtZW50J3MgJ29uZm9jdXMnIGV2ZW50LlxuICAgKi9cbiAgYWZPbkZvY3VzOiBFdmVudEVtaXR0ZXI8Q3VzdG9tRXZlbnQ8YW55Pj47XG4gIC8qKlxuICAgKiBJbnB1dGVsZW1lbnRldHMgJ29uZm9jdXNvdXQnLWV2ZW50LiBAZW4gVGhlIGlucHV0IGVsZW1lbnQncyAnb25mb2N1c291dCcgZXZlbnQuXG4gICAqL1xuICBhZk9uRm9jdXNvdXQ6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxhbnk+PjtcbiAgLyoqXG4gICAqIElucHV0ZWxlbWVudGV0cyAnb25pbnB1dCctZXZlbnQuIEBlbiBUaGUgaW5wdXQgZWxlbWVudCdzICdvbmlucHV0JyBldmVudC5cbiAgICovXG4gIGFmT25JbnB1dDogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PGFueT4+O1xuICAvKipcbiAgICogU2tlciB2aWQgaW5wdXRmw6RsdGV0cyBmw7Zyc3RhICdvbmlucHV0JyBAZW4gRmlyc3QgdGltZSB0aGUgaW5wdXQgZWxlbWVudCByZWNlaXZlcyBhbiBpbnB1dFxuICAgKi9cbiAgYWZPbkRpcnR5OiBFdmVudEVtaXR0ZXI8Q3VzdG9tRXZlbnQ8YW55Pj47XG4gIC8qKlxuICAgKiBTa2VyIHZpZCBpbnB1dGbDpGx0ZXRzIGbDtnJzdGEgJ29uYmx1cicgQGVuIEZpcnN0IHRpbWUgdGhlIGlucHV0IGVsZW1lbnQgaXMgYmx1cnJlZFxuICAgKi9cbiAgYWZPblRvdWNoZWQ6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxhbnk+PjtcblxufVxuXG5AUHJveHlDbXAoe1xuICBkZWZpbmVDdXN0b21FbGVtZW50Rm46IGRlZmluZURpZ2lGb3JtQ2hlY2tib3gsXG4gIGlucHV0czogWydhZkFyaWFEZXNjcmliZWRieScsICdhZkFyaWFMYWJlbCcsICdhZkFyaWFMYWJlbGxlZGJ5JywgJ2FmQXV0b2ZvY3VzJywgJ2FmQ2hlY2tlZCcsICdhZkRlc2NyaXB0aW9uJywgJ2FmSWQnLCAnYWZJbmRldGVybWluYXRlJywgJ2FmTGFiZWwnLCAnYWZMYXlvdXQnLCAnYWZOYW1lJywgJ2FmUmVxdWlyZWQnLCAnYWZWYWxpZGF0aW9uJywgJ2FmVmFsdWUnLCAnYWZWYXJpYXRpb24nLCAnY2hlY2tlZCcsICd2YWx1ZSddLFxuICBtZXRob2RzOiBbJ2FmTUdldEZvcm1Db250cm9sRWxlbWVudCddXG59KVxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGlnaS1mb3JtLWNoZWNrYm94JyxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXG4gIHRlbXBsYXRlOiAnPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PicsXG4gIGlucHV0czogWydhZkFyaWFEZXNjcmliZWRieScsICdhZkFyaWFMYWJlbCcsICdhZkFyaWFMYWJlbGxlZGJ5JywgJ2FmQXV0b2ZvY3VzJywgJ2FmQ2hlY2tlZCcsICdhZkRlc2NyaXB0aW9uJywgJ2FmSWQnLCAnYWZJbmRldGVybWluYXRlJywgJ2FmTGFiZWwnLCAnYWZMYXlvdXQnLCAnYWZOYW1lJywgJ2FmUmVxdWlyZWQnLCAnYWZWYWxpZGF0aW9uJywgJ2FmVmFsdWUnLCAnYWZWYXJpYXRpb24nLCAnY2hlY2tlZCcsICd2YWx1ZSddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lGb3JtQ2hlY2tib3gge1xuICBwcm90ZWN0ZWQgZWw6IEhUTUxFbGVtZW50O1xuICBjb25zdHJ1Y3RvcihjOiBDaGFuZ2VEZXRlY3RvclJlZiwgcjogRWxlbWVudFJlZiwgcHJvdGVjdGVkIHo6IE5nWm9uZSkge1xuICAgIGMuZGV0YWNoKCk7XG4gICAgdGhpcy5lbCA9IHIubmF0aXZlRWxlbWVudDtcbiAgICBwcm94eU91dHB1dHModGhpcywgdGhpcy5lbCwgWydhZk9uQ2hhbmdlJywgJ2FmT25CbHVyJywgJ2FmT25Gb2N1cycsICdhZk9uRm9jdXNvdXQnLCAnYWZPbklucHV0JywgJ2FmT25EaXJ0eScsICdhZk9uVG91Y2hlZCddKTtcbiAgfVxufVxuXG5cbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBEaWdpRm9ybUZpZWxkc2V0IGV4dGVuZHMgQ29tcG9uZW50cy5EaWdpRm9ybUZpZWxkc2V0IHt9XG5cbkBQcm94eUNtcCh7XG4gIGRlZmluZUN1c3RvbUVsZW1lbnRGbjogZGVmaW5lRGlnaUZvcm1GaWVsZHNldCxcbiAgaW5wdXRzOiBbJ2FmRm9ybScsICdhZklkJywgJ2FmTGVnZW5kJywgJ2FmTmFtZSddXG59KVxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGlnaS1mb3JtLWZpZWxkc2V0JyxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXG4gIHRlbXBsYXRlOiAnPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PicsXG4gIGlucHV0czogWydhZkZvcm0nLCAnYWZJZCcsICdhZkxlZ2VuZCcsICdhZk5hbWUnXVxufSlcbmV4cG9ydCBjbGFzcyBEaWdpRm9ybUZpZWxkc2V0IHtcbiAgcHJvdGVjdGVkIGVsOiBIVE1MRWxlbWVudDtcbiAgY29uc3RydWN0b3IoYzogQ2hhbmdlRGV0ZWN0b3JSZWYsIHI6IEVsZW1lbnRSZWYsIHByb3RlY3RlZCB6OiBOZ1pvbmUpIHtcbiAgICBjLmRldGFjaCgpO1xuICAgIHRoaXMuZWwgPSByLm5hdGl2ZUVsZW1lbnQ7XG4gIH1cbn1cblxuXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgRGlnaUZvcm1GaWxlVXBsb2FkIGV4dGVuZHMgQ29tcG9uZW50cy5EaWdpRm9ybUZpbGVVcGxvYWQge1xuICAvKipcbiAgICogU8OkbmRlciB1dCBmaWwgdmlkIHVwcGxhZGRuaW5nIEBlbiBFbWl0cyBmaWxlIG9uIHVwbG9hZFxuICAgKi9cbiAgYWZPblVwbG9hZEZpbGU6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxhbnk+PjtcbiAgLyoqXG4gICAqIFPDpG5kZXIgdXQgdmlsa2VuIGZpbCBzb20gdGFnaXRzIGJvcnQgQGVuIEVtaXRzIHdoaWNoIGZpbGUgdGhhdCB3YXMgZGVsZXRlZC5cbiAgICovXG4gIGFmT25SZW1vdmVGaWxlOiBFdmVudEVtaXR0ZXI8Q3VzdG9tRXZlbnQ8YW55Pj47XG4gIC8qKlxuICAgKiBTw6RuZGVyIHV0IHZpbGtlbiBmaWwgc29tIGhhciBhdmJydXRpcyB1cHBsYWRkbmluZyBAZW4gRW1pdHMgd2hpY2ggZmlsZSB0aGF0IHdhcyBjYW5jZWxlZFxuICAgKi9cbiAgYWZPbkNhbmNlbEZpbGU6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxhbnk+PjtcbiAgLyoqXG4gICAqIFPDpG5kZXIgdXQgdmlsa2VuIGZpbCBzb20gZsO2cnPDtmtlciBsYWRkYXMgdXBwIGlnZW4gQGVuIEVtaXRzIHdoaWNoIGZpbGUgaXMgdHJ5aW5nIHRvIHJldHJ5IGl0cyB1cGxvYWRcbiAgICovXG4gIGFmT25SZXRyeUZpbGU6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxhbnk+PjtcblxufVxuXG5AUHJveHlDbXAoe1xuICBkZWZpbmVDdXN0b21FbGVtZW50Rm46IGRlZmluZURpZ2lGb3JtRmlsZVVwbG9hZCxcbiAgaW5wdXRzOiBbJ2FmQW5ub3VuY2VJZk9wdGlvbmFsJywgJ2FmQW5ub3VuY2VJZk9wdGlvbmFsVGV4dCcsICdhZkZpbGVNYXhTaXplJywgJ2FmRmlsZVR5cGVzJywgJ2FmSGVhZGluZ0ZpbGVzJywgJ2FmSGVhZGluZ0xldmVsJywgJ2FmSWQnLCAnYWZMYWJlbCcsICdhZkxhYmVsRGVzY3JpcHRpb24nLCAnYWZNYXhGaWxlcycsICdhZk5hbWUnLCAnYWZSZXF1aXJlZCcsICdhZlJlcXVpcmVkVGV4dCcsICdhZlVwbG9hZEJ0blRleHQnLCAnYWZWYWxpZGF0aW9uJywgJ2FmVmFyaWF0aW9uJ10sXG4gIG1ldGhvZHM6IFsnYWZNVmFsaWRhdGVGaWxlJywgJ2FmTUdldEFsbEZpbGVzJywgJ2FmTUltcG9ydEZpbGVzJywgJ2FmTUdldEZvcm1Db250cm9sRWxlbWVudCddXG59KVxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGlnaS1mb3JtLWZpbGUtdXBsb2FkJyxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXG4gIHRlbXBsYXRlOiAnPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PicsXG4gIGlucHV0czogWydhZkFubm91bmNlSWZPcHRpb25hbCcsICdhZkFubm91bmNlSWZPcHRpb25hbFRleHQnLCAnYWZGaWxlTWF4U2l6ZScsICdhZkZpbGVUeXBlcycsICdhZkhlYWRpbmdGaWxlcycsICdhZkhlYWRpbmdMZXZlbCcsICdhZklkJywgJ2FmTGFiZWwnLCAnYWZMYWJlbERlc2NyaXB0aW9uJywgJ2FmTWF4RmlsZXMnLCAnYWZOYW1lJywgJ2FmUmVxdWlyZWQnLCAnYWZSZXF1aXJlZFRleHQnLCAnYWZVcGxvYWRCdG5UZXh0JywgJ2FmVmFsaWRhdGlvbicsICdhZlZhcmlhdGlvbiddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lGb3JtRmlsZVVwbG9hZCB7XG4gIHByb3RlY3RlZCBlbDogSFRNTEVsZW1lbnQ7XG4gIGNvbnN0cnVjdG9yKGM6IENoYW5nZURldGVjdG9yUmVmLCByOiBFbGVtZW50UmVmLCBwcm90ZWN0ZWQgejogTmdab25lKSB7XG4gICAgYy5kZXRhY2goKTtcbiAgICB0aGlzLmVsID0gci5uYXRpdmVFbGVtZW50O1xuICAgIHByb3h5T3V0cHV0cyh0aGlzLCB0aGlzLmVsLCBbJ2FmT25VcGxvYWRGaWxlJywgJ2FmT25SZW1vdmVGaWxlJywgJ2FmT25DYW5jZWxGaWxlJywgJ2FmT25SZXRyeUZpbGUnXSk7XG4gIH1cbn1cblxuXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgRGlnaUZvcm1GaWx0ZXIgZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lGb3JtRmlsdGVyIHtcbiAgLyoqXG4gICAqICBcbiAgICovXG4gIGFmT25Gb2N1c291dDogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PGFueT4+O1xuICAvKipcbiAgICogVmlkIHVwcGRhdGVyaW5nIGF2IGZpbHRyZXQgQGVuIEF0IGZpbHRlciBzdWJtaXRcbiAgICovXG4gIGFmT25TdWJtaXRGaWx0ZXJzOiBFdmVudEVtaXR0ZXI8Q3VzdG9tRXZlbnQ8YW55Pj47XG4gIC8qKlxuICAgKiBOw6RyIGZpbHRyZXQgc3TDpG5ncyB1dGFuIGF0dCB2YWxkYSBhbHRlcm5hdGl2IGJla3LDpGZ0YXRzIEBlbiBXaGVuIHRoZSBmaWx0ZXIgaXMgY2xvc2VkIHdpdGhvdXQgY29uZmlybWluZyB0aGUgc2VsZWN0ZWQgb3B0aW9uc1xuICAgKi9cbiAgYWZPbkZpbHRlckNsb3NlZDogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PGFueT4+O1xuICAvKipcbiAgICogVmlkIHJlc2V0IGF2IGZpbHRyZXQgQGVuIEF0IGZpbHRlciByZXNldFxuICAgKi9cbiAgYWZPblJlc2V0RmlsdGVyczogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PGFueT4+O1xuICAvKipcbiAgICogTsOkciBldHQgZmlsdGVyIMOkbmRyYXMgQGVuIFdoZW4gYSBmaWx0ZXIgaXMgY2hhbmdlZFxuICAgKi9cbiAgYWZPbkNoYW5nZTogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PGFueT4+O1xuXG59XG5cbkBQcm94eUNtcCh7XG4gIGRlZmluZUN1c3RvbUVsZW1lbnRGbjogZGVmaW5lRGlnaUZvcm1GaWx0ZXIsXG4gIGlucHV0czogWydhZkFsaWduUmlnaHQnLCAnYWZBdXRvZm9jdXMnLCAnYWZGaWx0ZXJCdXR0b25BcmlhRGVzY3JpYmVkYnknLCAnYWZGaWx0ZXJCdXR0b25BcmlhTGFiZWwnLCAnYWZGaWx0ZXJCdXR0b25UZXh0JywgJ2FmSGlkZVJlc2V0QnV0dG9uJywgJ2FmSWQnLCAnYWZOYW1lJywgJ2FmUmVzZXRCdXR0b25UZXh0JywgJ2FmUmVzZXRCdXR0b25UZXh0QXJpYUxhYmVsJywgJ2FmU3VibWl0QnV0dG9uVGV4dCcsICdhZlN1Ym1pdEJ1dHRvblRleHRBcmlhTGFiZWwnXVxufSlcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2RpZ2ktZm9ybS1maWx0ZXInLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaCxcbiAgdGVtcGxhdGU6ICc8bmctY29udGVudD48L25nLWNvbnRlbnQ+JyxcbiAgaW5wdXRzOiBbJ2FmQWxpZ25SaWdodCcsICdhZkF1dG9mb2N1cycsICdhZkZpbHRlckJ1dHRvbkFyaWFEZXNjcmliZWRieScsICdhZkZpbHRlckJ1dHRvbkFyaWFMYWJlbCcsICdhZkZpbHRlckJ1dHRvblRleHQnLCAnYWZIaWRlUmVzZXRCdXR0b24nLCAnYWZJZCcsICdhZk5hbWUnLCAnYWZSZXNldEJ1dHRvblRleHQnLCAnYWZSZXNldEJ1dHRvblRleHRBcmlhTGFiZWwnLCAnYWZTdWJtaXRCdXR0b25UZXh0JywgJ2FmU3VibWl0QnV0dG9uVGV4dEFyaWFMYWJlbCddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lGb3JtRmlsdGVyIHtcbiAgcHJvdGVjdGVkIGVsOiBIVE1MRWxlbWVudDtcbiAgY29uc3RydWN0b3IoYzogQ2hhbmdlRGV0ZWN0b3JSZWYsIHI6IEVsZW1lbnRSZWYsIHByb3RlY3RlZCB6OiBOZ1pvbmUpIHtcbiAgICBjLmRldGFjaCgpO1xuICAgIHRoaXMuZWwgPSByLm5hdGl2ZUVsZW1lbnQ7XG4gICAgcHJveHlPdXRwdXRzKHRoaXMsIHRoaXMuZWwsIFsnYWZPbkZvY3Vzb3V0JywgJ2FmT25TdWJtaXRGaWx0ZXJzJywgJ2FmT25GaWx0ZXJDbG9zZWQnLCAnYWZPblJlc2V0RmlsdGVycycsICdhZk9uQ2hhbmdlJ10pO1xuICB9XG59XG5cblxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIERpZ2lGb3JtSW5wdXQgZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lGb3JtSW5wdXQge1xuICAvKipcbiAgICogSW5wdXRlbGVtZW50ZXRzICdvbmNoYW5nZSctZXZlbnQuIEBlbiBUaGUgaW5wdXQgZWxlbWVudCdzICdvbmNoYW5nZScgZXZlbnQuXG4gICAqL1xuICBhZk9uQ2hhbmdlOiBFdmVudEVtaXR0ZXI8Q3VzdG9tRXZlbnQ8YW55Pj47XG4gIC8qKlxuICAgKiBJbnB1dGVsZW1lbnRldHMgJ29uYmx1cictZXZlbnQuIEBlbiBUaGUgaW5wdXQgZWxlbWVudCdzICdvbmJsdXInIGV2ZW50LlxuICAgKi9cbiAgYWZPbkJsdXI6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxhbnk+PjtcbiAgLyoqXG4gICAqIElucHV0ZWxlbWVudGV0cyAnb25rZXl1cCctZXZlbnQuIEBlbiBUaGUgaW5wdXQgZWxlbWVudCdzICdvbmtleXVwJyBldmVudC5cbiAgICovXG4gIGFmT25LZXl1cDogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PGFueT4+O1xuICAvKipcbiAgICogSW5wdXRlbGVtZW50ZXRzICdvbmZvY3VzJy1ldmVudC4gQGVuIFRoZSBpbnB1dCBlbGVtZW50J3MgJ29uZm9jdXMnIGV2ZW50LlxuICAgKi9cbiAgYWZPbkZvY3VzOiBFdmVudEVtaXR0ZXI8Q3VzdG9tRXZlbnQ8YW55Pj47XG4gIC8qKlxuICAgKiBJbnB1dGVsZW1lbnRldHMgJ29uZm9jdXNvdXQnLWV2ZW50LiBAZW4gVGhlIGlucHV0IGVsZW1lbnQncyAnb25mb2N1c291dCcgZXZlbnQuXG4gICAqL1xuICBhZk9uRm9jdXNvdXQ6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxhbnk+PjtcbiAgLyoqXG4gICAqIElucHV0ZWxlbWVudGV0cyAnb25pbnB1dCctZXZlbnQuIEBlbiBUaGUgaW5wdXQgZWxlbWVudCdzICdvbmlucHV0JyBldmVudC5cbiAgICovXG4gIGFmT25JbnB1dDogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PGFueT4+O1xuICAvKipcbiAgICogU2tlciB2aWQgaW5wdXRmw6RsdGV0cyBmw7Zyc3RhICdvbmlucHV0JyBAZW4gRmlyc3QgdGltZSB0aGUgaW5wdXQgZWxlbWVudCByZWNlaXZlcyBhbiBpbnB1dFxuICAgKi9cbiAgYWZPbkRpcnR5OiBFdmVudEVtaXR0ZXI8Q3VzdG9tRXZlbnQ8YW55Pj47XG4gIC8qKlxuICAgKiBTa2VyIHZpZCBpbnB1dGbDpGx0ZXRzIGbDtnJzdGEgJ29uYmx1cicgQGVuIEZpcnN0IHRpbWUgdGhlIGlucHV0IGVsZW1lbnQgaXMgYmx1cnJlZFxuICAgKi9cbiAgYWZPblRvdWNoZWQ6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxhbnk+PjtcblxufVxuXG5AUHJveHlDbXAoe1xuICBkZWZpbmVDdXN0b21FbGVtZW50Rm46IGRlZmluZURpZ2lGb3JtSW5wdXQsXG4gIGlucHV0czogWydhZkFubm91bmNlSWZPcHRpb25hbCcsICdhZkFubm91bmNlSWZPcHRpb25hbFRleHQnLCAnYWZBcmlhQWN0aXZlZGVzY2VuZGFudCcsICdhZkFyaWFBdXRvY29tcGxldGUnLCAnYWZBcmlhRGVzY3JpYmVkYnknLCAnYWZBcmlhTGFiZWxsZWRieScsICdhZkF1dG9jb21wbGV0ZScsICdhZkF1dG9mb2N1cycsICdhZkJ1dHRvblZhcmlhdGlvbicsICdhZkRpcm5hbWUnLCAnYWZJZCcsICdhZkxhYmVsJywgJ2FmTGFiZWxEZXNjcmlwdGlvbicsICdhZkxpc3QnLCAnYWZNYXgnLCAnYWZNYXhsZW5ndGgnLCAnYWZNaW4nLCAnYWZNaW5sZW5ndGgnLCAnYWZOYW1lJywgJ2FmUmVxdWlyZWQnLCAnYWZSZXF1aXJlZFRleHQnLCAnYWZSb2xlJywgJ2FmU3RlcCcsICdhZlR5cGUnLCAnYWZWYWxpZGF0aW9uJywgJ2FmVmFsaWRhdGlvblRleHQnLCAnYWZWYWx1ZScsICdhZlZhcmlhdGlvbicsICd2YWx1ZSddLFxuICBtZXRob2RzOiBbJ2FmTUdldEZvcm1Db250cm9sRWxlbWVudCddXG59KVxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGlnaS1mb3JtLWlucHV0JyxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXG4gIHRlbXBsYXRlOiAnPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PicsXG4gIGlucHV0czogWydhZkFubm91bmNlSWZPcHRpb25hbCcsICdhZkFubm91bmNlSWZPcHRpb25hbFRleHQnLCAnYWZBcmlhQWN0aXZlZGVzY2VuZGFudCcsICdhZkFyaWFBdXRvY29tcGxldGUnLCAnYWZBcmlhRGVzY3JpYmVkYnknLCAnYWZBcmlhTGFiZWxsZWRieScsICdhZkF1dG9jb21wbGV0ZScsICdhZkF1dG9mb2N1cycsICdhZkJ1dHRvblZhcmlhdGlvbicsICdhZkRpcm5hbWUnLCAnYWZJZCcsICdhZkxhYmVsJywgJ2FmTGFiZWxEZXNjcmlwdGlvbicsICdhZkxpc3QnLCAnYWZNYXgnLCAnYWZNYXhsZW5ndGgnLCAnYWZNaW4nLCAnYWZNaW5sZW5ndGgnLCAnYWZOYW1lJywgJ2FmUmVxdWlyZWQnLCAnYWZSZXF1aXJlZFRleHQnLCAnYWZSb2xlJywgJ2FmU3RlcCcsICdhZlR5cGUnLCAnYWZWYWxpZGF0aW9uJywgJ2FmVmFsaWRhdGlvblRleHQnLCAnYWZWYWx1ZScsICdhZlZhcmlhdGlvbicsICd2YWx1ZSddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lGb3JtSW5wdXQge1xuICBwcm90ZWN0ZWQgZWw6IEhUTUxFbGVtZW50O1xuICBjb25zdHJ1Y3RvcihjOiBDaGFuZ2VEZXRlY3RvclJlZiwgcjogRWxlbWVudFJlZiwgcHJvdGVjdGVkIHo6IE5nWm9uZSkge1xuICAgIGMuZGV0YWNoKCk7XG4gICAgdGhpcy5lbCA9IHIubmF0aXZlRWxlbWVudDtcbiAgICBwcm94eU91dHB1dHModGhpcywgdGhpcy5lbCwgWydhZk9uQ2hhbmdlJywgJ2FmT25CbHVyJywgJ2FmT25LZXl1cCcsICdhZk9uRm9jdXMnLCAnYWZPbkZvY3Vzb3V0JywgJ2FmT25JbnB1dCcsICdhZk9uRGlydHknLCAnYWZPblRvdWNoZWQnXSk7XG4gIH1cbn1cblxuXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgRGlnaUZvcm1JbnB1dFNlYXJjaCBleHRlbmRzIENvbXBvbmVudHMuRGlnaUZvcm1JbnB1dFNlYXJjaCB7XG4gIC8qKlxuICAgKiBWaWQgZm9rdXMgdXRhbmbDtnIga29tcG9uZW50ZW4uIEBlbiBXaGVuIGZvY3VzIGlzIG91dHNpZGUgY29tcG9uZW50LlxuICAgKi9cbiAgYWZPbkZvY3VzT3V0c2lkZTogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PGFueT4+O1xuICAvKipcbiAgICogVmlkIGZva3VzIGludXRpIGtvbXBvbmVudGVuLiBAZW4gV2hlbiBmb2N1cyBpcyBpbnNpZGUgY29tcG9uZW50LlxuICAgKi9cbiAgYWZPbkZvY3VzSW5zaWRlOiBFdmVudEVtaXR0ZXI8Q3VzdG9tRXZlbnQ8YW55Pj47XG4gIC8qKlxuICAgKiBWaWQga2xpY2sgdXRhbmbDtnIga29tcG9uZW50ZW4uIEBlbiBXaGVuIGNsaWNrIG91dHNpZGUgY29tcG9uZW50LlxuICAgKi9cbiAgYWZPbkNsaWNrT3V0c2lkZTogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PGFueT4+O1xuICAvKipcbiAgICogVmlkIGtsaWNrIGludXRpIGtvbXBvbmVudGVuLiBAZW4gV2hlbiBjbGljayBpbnNpZGUgY29tcG9uZW50LlxuICAgKi9cbiAgYWZPbkNsaWNrSW5zaWRlOiBFdmVudEVtaXR0ZXI8Q3VzdG9tRXZlbnQ8YW55Pj47XG4gIC8qKlxuICAgKiBJbnB1dGVsZW1lbnRldHMgJ29uY2hhbmdlJy1ldmVudC4gQGVuIFRoZSBpbnB1dCBlbGVtZW50J3MgJ29uY2hhbmdlJyBldmVudC5cbiAgICovXG4gIGFmT25DaGFuZ2U6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxhbnk+PjtcbiAgLyoqXG4gICAqIElucHV0ZWxlbWVudGV0cyAnb25ibHVyJy1ldmVudC4gQGVuIFRoZSBpbnB1dCBlbGVtZW50J3MgJ29uYmx1cicgZXZlbnQuXG4gICAqL1xuICBhZk9uQmx1cjogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PGFueT4+O1xuICAvKipcbiAgICogSW5wdXRlbGVtZW50ZXRzICdvbmtleXVwJy1ldmVudC4gQGVuIFRoZSBpbnB1dCBlbGVtZW50J3MgJ29ua2V5dXAnIGV2ZW50LlxuICAgKi9cbiAgYWZPbktleXVwOiBFdmVudEVtaXR0ZXI8Q3VzdG9tRXZlbnQ8YW55Pj47XG4gIC8qKlxuICAgKiBJbnB1dGVsZW1lbnRldHMgJ29uZm9jdXMnLWV2ZW50LiBAZW4gVGhlIGlucHV0IGVsZW1lbnQncyAnb25mb2N1cycgZXZlbnQuXG4gICAqL1xuICBhZk9uRm9jdXM6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxhbnk+PjtcbiAgLyoqXG4gICAqIElucHV0ZWxlbWVudGV0cyAnb25mb2N1c291dCctZXZlbnQuIEBlbiBUaGUgaW5wdXQgZWxlbWVudCdzICdvbmZvY3Vzb3V0JyBldmVudC5cbiAgICovXG4gIGFmT25Gb2N1c291dDogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PGFueT4+O1xuICAvKipcbiAgICogSW5wdXRlbGVtZW50ZXRzICdvbmlucHV0Jy1ldmVudC4gQGVuIFRoZSBpbnB1dCBlbGVtZW50J3MgJ29uaW5wdXQnIGV2ZW50LlxuICAgKi9cbiAgYWZPbklucHV0OiBFdmVudEVtaXR0ZXI8Q3VzdG9tRXZlbnQ8YW55Pj47XG4gIC8qKlxuICAgKiBLbmFwcGVsZW1lbnRldHMgJ29uY2xpY2snLWV2ZW50LiBAZW4gVGhlIGJ1dHRvbiBlbGVtZW50J3MgJ29uY2xpY2snIGV2ZW50LlxuICAgKi9cbiAgYWZPbkNsaWNrOiBFdmVudEVtaXR0ZXI8Q3VzdG9tRXZlbnQ8YW55Pj47XG5cbn1cblxuQFByb3h5Q21wKHtcbiAgZGVmaW5lQ3VzdG9tRWxlbWVudEZuOiBkZWZpbmVEaWdpRm9ybUlucHV0U2VhcmNoLFxuICBpbnB1dHM6IFsnYWZBcmlhQWN0aXZlZGVzY2VuZGFudCcsICdhZkFyaWFBdXRvY29tcGxldGUnLCAnYWZBcmlhRGVzY3JpYmVkYnknLCAnYWZBcmlhTGFiZWxsZWRieScsICdhZkF1dG9jb21wbGV0ZScsICdhZkF1dG9mb2N1cycsICdhZkJ1dHRvbkFyaWFMYWJlbCcsICdhZkJ1dHRvbkFyaWFMYWJlbGxlZGJ5JywgJ2FmQnV0dG9uVGV4dCcsICdhZkJ1dHRvblR5cGUnLCAnYWZCdXR0b25WYXJpYXRpb24nLCAnYWZIaWRlQnV0dG9uJywgJ2FmSWQnLCAnYWZMYWJlbCcsICdhZkxhYmVsRGVzY3JpcHRpb24nLCAnYWZOYW1lJywgJ2FmVHlwZScsICdhZlZhbHVlJywgJ2FmVmFyaWF0aW9uJywgJ3ZhbHVlJ10sXG4gIG1ldGhvZHM6IFsnYWZNR2V0Rm9ybUNvbnRyb2xFbGVtZW50J11cbn0pXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdkaWdpLWZvcm0taW5wdXQtc2VhcmNoJyxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXG4gIHRlbXBsYXRlOiAnPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PicsXG4gIGlucHV0czogWydhZkFyaWFBY3RpdmVkZXNjZW5kYW50JywgJ2FmQXJpYUF1dG9jb21wbGV0ZScsICdhZkFyaWFEZXNjcmliZWRieScsICdhZkFyaWFMYWJlbGxlZGJ5JywgJ2FmQXV0b2NvbXBsZXRlJywgJ2FmQXV0b2ZvY3VzJywgJ2FmQnV0dG9uQXJpYUxhYmVsJywgJ2FmQnV0dG9uQXJpYUxhYmVsbGVkYnknLCAnYWZCdXR0b25UZXh0JywgJ2FmQnV0dG9uVHlwZScsICdhZkJ1dHRvblZhcmlhdGlvbicsICdhZkhpZGVCdXR0b24nLCAnYWZJZCcsICdhZkxhYmVsJywgJ2FmTGFiZWxEZXNjcmlwdGlvbicsICdhZk5hbWUnLCAnYWZUeXBlJywgJ2FmVmFsdWUnLCAnYWZWYXJpYXRpb24nLCAndmFsdWUnXVxufSlcbmV4cG9ydCBjbGFzcyBEaWdpRm9ybUlucHV0U2VhcmNoIHtcbiAgcHJvdGVjdGVkIGVsOiBIVE1MRWxlbWVudDtcbiAgY29uc3RydWN0b3IoYzogQ2hhbmdlRGV0ZWN0b3JSZWYsIHI6IEVsZW1lbnRSZWYsIHByb3RlY3RlZCB6OiBOZ1pvbmUpIHtcbiAgICBjLmRldGFjaCgpO1xuICAgIHRoaXMuZWwgPSByLm5hdGl2ZUVsZW1lbnQ7XG4gICAgcHJveHlPdXRwdXRzKHRoaXMsIHRoaXMuZWwsIFsnYWZPbkZvY3VzT3V0c2lkZScsICdhZk9uRm9jdXNJbnNpZGUnLCAnYWZPbkNsaWNrT3V0c2lkZScsICdhZk9uQ2xpY2tJbnNpZGUnLCAnYWZPbkNoYW5nZScsICdhZk9uQmx1cicsICdhZk9uS2V5dXAnLCAnYWZPbkZvY3VzJywgJ2FmT25Gb2N1c291dCcsICdhZk9uSW5wdXQnLCAnYWZPbkNsaWNrJ10pO1xuICB9XG59XG5cblxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIERpZ2lGb3JtTGFiZWwgZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lGb3JtTGFiZWwge31cblxuQFByb3h5Q21wKHtcbiAgZGVmaW5lQ3VzdG9tRWxlbWVudEZuOiBkZWZpbmVEaWdpRm9ybUxhYmVsLFxuICBpbnB1dHM6IFsnYWZBbm5vdW5jZUlmT3B0aW9uYWwnLCAnYWZBbm5vdW5jZUlmT3B0aW9uYWxUZXh0JywgJ2FmRGVzY3JpcHRpb24nLCAnYWZGb3InLCAnYWZJZCcsICdhZkxhYmVsJywgJ2FmUmVxdWlyZWQnLCAnYWZSZXF1aXJlZFRleHQnXVxufSlcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2RpZ2ktZm9ybS1sYWJlbCcsXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxuICB0ZW1wbGF0ZTogJzxuZy1jb250ZW50PjwvbmctY29udGVudD4nLFxuICBpbnB1dHM6IFsnYWZBbm5vdW5jZUlmT3B0aW9uYWwnLCAnYWZBbm5vdW5jZUlmT3B0aW9uYWxUZXh0JywgJ2FmRGVzY3JpcHRpb24nLCAnYWZGb3InLCAnYWZJZCcsICdhZkxhYmVsJywgJ2FmUmVxdWlyZWQnLCAnYWZSZXF1aXJlZFRleHQnXVxufSlcbmV4cG9ydCBjbGFzcyBEaWdpRm9ybUxhYmVsIHtcbiAgcHJvdGVjdGVkIGVsOiBIVE1MRWxlbWVudDtcbiAgY29uc3RydWN0b3IoYzogQ2hhbmdlRGV0ZWN0b3JSZWYsIHI6IEVsZW1lbnRSZWYsIHByb3RlY3RlZCB6OiBOZ1pvbmUpIHtcbiAgICBjLmRldGFjaCgpO1xuICAgIHRoaXMuZWwgPSByLm5hdGl2ZUVsZW1lbnQ7XG4gIH1cbn1cblxuXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgRGlnaUZvcm1Qcm9jZXNzU3RlcCBleHRlbmRzIENvbXBvbmVudHMuRGlnaUZvcm1Qcm9jZXNzU3RlcCB7XG4gIC8qKlxuICAgKiAgXG4gICAqL1xuICBhZkNsaWNrOiBFdmVudEVtaXR0ZXI8Q3VzdG9tRXZlbnQ8TW91c2VFdmVudD4+O1xuXG59XG5cbkBQcm94eUNtcCh7XG4gIGRlZmluZUN1c3RvbUVsZW1lbnRGbjogZGVmaW5lRGlnaUZvcm1Qcm9jZXNzU3RlcCxcbiAgaW5wdXRzOiBbJ2FmQ29udGV4dCcsICdhZkhyZWYnLCAnYWZMYWJlbCcsICdhZlR5cGUnXVxufSlcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2RpZ2ktZm9ybS1wcm9jZXNzLXN0ZXAnLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaCxcbiAgdGVtcGxhdGU6ICc8bmctY29udGVudD48L25nLWNvbnRlbnQ+JyxcbiAgaW5wdXRzOiBbJ2FmQ29udGV4dCcsICdhZkhyZWYnLCAnYWZMYWJlbCcsICdhZlR5cGUnXVxufSlcbmV4cG9ydCBjbGFzcyBEaWdpRm9ybVByb2Nlc3NTdGVwIHtcbiAgcHJvdGVjdGVkIGVsOiBIVE1MRWxlbWVudDtcbiAgY29uc3RydWN0b3IoYzogQ2hhbmdlRGV0ZWN0b3JSZWYsIHI6IEVsZW1lbnRSZWYsIHByb3RlY3RlZCB6OiBOZ1pvbmUpIHtcbiAgICBjLmRldGFjaCgpO1xuICAgIHRoaXMuZWwgPSByLm5hdGl2ZUVsZW1lbnQ7XG4gICAgcHJveHlPdXRwdXRzKHRoaXMsIHRoaXMuZWwsIFsnYWZDbGljayddKTtcbiAgfVxufVxuXG5cbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBEaWdpRm9ybVByb2Nlc3NTdGVwcyBleHRlbmRzIENvbXBvbmVudHMuRGlnaUZvcm1Qcm9jZXNzU3RlcHMge31cblxuQFByb3h5Q21wKHtcbiAgZGVmaW5lQ3VzdG9tRWxlbWVudEZuOiBkZWZpbmVEaWdpRm9ybVByb2Nlc3NTdGVwcyxcbiAgaW5wdXRzOiBbJ2FmQ3VycmVudFN0ZXAnLCAnYWZJZCddXG59KVxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGlnaS1mb3JtLXByb2Nlc3Mtc3RlcHMnLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaCxcbiAgdGVtcGxhdGU6ICc8bmctY29udGVudD48L25nLWNvbnRlbnQ+JyxcbiAgaW5wdXRzOiBbJ2FmQ3VycmVudFN0ZXAnLCAnYWZJZCddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lGb3JtUHJvY2Vzc1N0ZXBzIHtcbiAgcHJvdGVjdGVkIGVsOiBIVE1MRWxlbWVudDtcbiAgY29uc3RydWN0b3IoYzogQ2hhbmdlRGV0ZWN0b3JSZWYsIHI6IEVsZW1lbnRSZWYsIHByb3RlY3RlZCB6OiBOZ1pvbmUpIHtcbiAgICBjLmRldGFjaCgpO1xuICAgIHRoaXMuZWwgPSByLm5hdGl2ZUVsZW1lbnQ7XG4gIH1cbn1cblxuXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgRGlnaUZvcm1SYWRpb2J1dHRvbiBleHRlbmRzIENvbXBvbmVudHMuRGlnaUZvcm1SYWRpb2J1dHRvbiB7XG4gIC8qKlxuICAgKiBJbnB1dGVsZW1lbnRldHMgJ29uY2hhbmdlJy1ldmVudC4gQGVuIFRoZSBpbnB1dCBlbGVtZW50J3MgJ29uY2hhbmdlJyBldmVudC5cbiAgICovXG4gIGFmT25DaGFuZ2U6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxhbnk+PjtcbiAgLyoqXG4gICAqIElucHV0ZWxlbWVudGV0cyAnb25ibHVyJy1ldmVudC4gQGVuIFRoZSBpbnB1dCBlbGVtZW50J3MgJ29uYmx1cicgZXZlbnQuXG4gICAqL1xuICBhZk9uQmx1cjogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PGFueT4+O1xuICAvKipcbiAgICogSW5wdXRlbGVtZW50ZXRzICdvbmZvY3VzJy1ldmVudC4gQGVuIFRoZSBpbnB1dCBlbGVtZW50J3MgJ29uZm9jdXMnIGV2ZW50LlxuICAgKi9cbiAgYWZPbkZvY3VzOiBFdmVudEVtaXR0ZXI8Q3VzdG9tRXZlbnQ8YW55Pj47XG4gIC8qKlxuICAgKiBJbnB1dGVsZW1lbnRldHMgJ29uZm9jdXNvdXQnLWV2ZW50LiBAZW4gVGhlIGlucHV0IGVsZW1lbnQncyAnb25mb2N1c291dCcgZXZlbnQuXG4gICAqL1xuICBhZk9uRm9jdXNvdXQ6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxhbnk+PjtcbiAgLyoqXG4gICAqIElucHV0ZWxlbWVudGV0cyAnb25pbnB1dCctZXZlbnQuIEBlbiBUaGUgaW5wdXQgZWxlbWVudCdzICdvbmlucHV0JyBldmVudC5cbiAgICovXG4gIGFmT25JbnB1dDogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PGFueT4+O1xuICAvKipcbiAgICogU2tlciB2aWQgaW5wdXRmw6RsdGV0cyBmw7Zyc3RhICdvbmlucHV0JyBAZW4gRmlyc3QgdGltZSB0aGUgaW5wdXQgZWxlbWVudCByZWNlaXZlcyBhbiBpbnB1dFxuICAgKi9cbiAgYWZPbkRpcnR5OiBFdmVudEVtaXR0ZXI8Q3VzdG9tRXZlbnQ8YW55Pj47XG4gIC8qKlxuICAgKiBTa2VyIHZpZCBpbnB1dGbDpGx0ZXRzIGbDtnJzdGEgJ29uYmx1cicgQGVuIEZpcnN0IHRpbWUgdGhlIGlucHV0IGVsZW1lbnQgaXMgYmx1cnJlZFxuICAgKi9cbiAgYWZPblRvdWNoZWQ6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxhbnk+PjtcblxufVxuXG5AUHJveHlDbXAoe1xuICBkZWZpbmVDdXN0b21FbGVtZW50Rm46IGRlZmluZURpZ2lGb3JtUmFkaW9idXR0b24sXG4gIGlucHV0czogWydhZkFyaWFEZXNjcmliZWRieScsICdhZkFyaWFMYWJlbGxlZGJ5JywgJ2FmQXV0b2ZvY3VzJywgJ2FmQ2hlY2tlZCcsICdhZklkJywgJ2FmTGFiZWwnLCAnYWZMYXlvdXQnLCAnYWZOYW1lJywgJ2FmUmVxdWlyZWQnLCAnYWZWYWxpZGF0aW9uJywgJ2FmVmFsdWUnLCAnYWZWYXJpYXRpb24nLCAnY2hlY2tlZCcsICd2YWx1ZSddLFxuICBtZXRob2RzOiBbJ2FmTUdldEZvcm1Db250cm9sRWxlbWVudCddXG59KVxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGlnaS1mb3JtLXJhZGlvYnV0dG9uJyxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXG4gIHRlbXBsYXRlOiAnPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PicsXG4gIGlucHV0czogWydhZkFyaWFEZXNjcmliZWRieScsICdhZkFyaWFMYWJlbGxlZGJ5JywgJ2FmQXV0b2ZvY3VzJywgJ2FmQ2hlY2tlZCcsICdhZklkJywgJ2FmTGFiZWwnLCAnYWZMYXlvdXQnLCAnYWZOYW1lJywgJ2FmUmVxdWlyZWQnLCAnYWZWYWxpZGF0aW9uJywgJ2FmVmFsdWUnLCAnYWZWYXJpYXRpb24nLCAnY2hlY2tlZCcsICd2YWx1ZSddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lGb3JtUmFkaW9idXR0b24ge1xuICBwcm90ZWN0ZWQgZWw6IEhUTUxFbGVtZW50O1xuICBjb25zdHJ1Y3RvcihjOiBDaGFuZ2VEZXRlY3RvclJlZiwgcjogRWxlbWVudFJlZiwgcHJvdGVjdGVkIHo6IE5nWm9uZSkge1xuICAgIGMuZGV0YWNoKCk7XG4gICAgdGhpcy5lbCA9IHIubmF0aXZlRWxlbWVudDtcbiAgICBwcm94eU91dHB1dHModGhpcywgdGhpcy5lbCwgWydhZk9uQ2hhbmdlJywgJ2FmT25CbHVyJywgJ2FmT25Gb2N1cycsICdhZk9uRm9jdXNvdXQnLCAnYWZPbklucHV0JywgJ2FmT25EaXJ0eScsICdhZk9uVG91Y2hlZCddKTtcbiAgfVxufVxuXG5cbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBEaWdpRm9ybVJhZGlvZ3JvdXAgZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lGb3JtUmFkaW9ncm91cCB7XG4gIC8qKlxuICAgKiBFdmVudCBuw6RyIHbDpHJkZXQgw6RuZHJhcyBAZW4gRXZlbnQgd2hlbiB2YWx1ZSBjaGFuZ2VzXG4gICAqL1xuICBhZk9uR3JvdXBDaGFuZ2U6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxzdHJpbmc+PjtcblxufVxuXG5AUHJveHlDbXAoe1xuICBkZWZpbmVDdXN0b21FbGVtZW50Rm46IGRlZmluZURpZ2lGb3JtUmFkaW9ncm91cCxcbiAgaW5wdXRzOiBbJ2FmTmFtZScsICdhZlZhbHVlJywgJ3ZhbHVlJ11cbn0pXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdkaWdpLWZvcm0tcmFkaW9ncm91cCcsXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxuICB0ZW1wbGF0ZTogJzxuZy1jb250ZW50PjwvbmctY29udGVudD4nLFxuICBpbnB1dHM6IFsnYWZOYW1lJywgJ2FmVmFsdWUnLCAndmFsdWUnXVxufSlcbmV4cG9ydCBjbGFzcyBEaWdpRm9ybVJhZGlvZ3JvdXAge1xuICBwcm90ZWN0ZWQgZWw6IEhUTUxFbGVtZW50O1xuICBjb25zdHJ1Y3RvcihjOiBDaGFuZ2VEZXRlY3RvclJlZiwgcjogRWxlbWVudFJlZiwgcHJvdGVjdGVkIHo6IE5nWm9uZSkge1xuICAgIGMuZGV0YWNoKCk7XG4gICAgdGhpcy5lbCA9IHIubmF0aXZlRWxlbWVudDtcbiAgICBwcm94eU91dHB1dHModGhpcywgdGhpcy5lbCwgWydhZk9uR3JvdXBDaGFuZ2UnXSk7XG4gIH1cbn1cblxuXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgRGlnaUZvcm1TZWxlY3QgZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lGb3JtU2VsZWN0IHtcbiAgLyoqXG4gICAqICBcbiAgICovXG4gIGFmT25DaGFuZ2U6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxhbnk+PjtcbiAgLyoqXG4gICAqICBcbiAgICovXG4gIGFmT25Gb2N1czogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PGFueT4+O1xuICAvKipcbiAgICogIFxuICAgKi9cbiAgYWZPbkZvY3Vzb3V0OiBFdmVudEVtaXR0ZXI8Q3VzdG9tRXZlbnQ8YW55Pj47XG4gIC8qKlxuICAgKiAgXG4gICAqL1xuICBhZk9uQmx1cjogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PGFueT4+O1xuICAvKipcbiAgICogU2tlciBmw7Zyc3RhIGfDpW5nZW4gdsOkbGphcmVsZW1lbnRldCDDpG5kcmFzLiBAZW4gRmlyc3QgdGltZSB0aGUgc2VsZWN0IGVsZW1lbnQgaXMgY2hhbmdlZC5cbiAgICovXG4gIGFmT25EaXJ0eTogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PGFueT4+O1xuXG59XG5cbkBQcm94eUNtcCh7XG4gIGRlZmluZUN1c3RvbUVsZW1lbnRGbjogZGVmaW5lRGlnaUZvcm1TZWxlY3QsXG4gIGlucHV0czogWydhZkFubm91bmNlSWZPcHRpb25hbCcsICdhZkFubm91bmNlSWZPcHRpb25hbFRleHQnLCAnYWZBdXRvZm9jdXMnLCAnYWZEZXNjcmlwdGlvbicsICdhZkRpc2FibGVWYWxpZGF0aW9uJywgJ2FmSWQnLCAnYWZMYWJlbCcsICdhZk5hbWUnLCAnYWZQbGFjZWhvbGRlcicsICdhZlJlcXVpcmVkJywgJ2FmUmVxdWlyZWRUZXh0JywgJ2FmU3RhcnRTZWxlY3RlZCcsICdhZlZhbGlkYXRpb24nLCAnYWZWYWxpZGF0aW9uVGV4dCcsICdhZlZhbHVlJywgJ2FmVmFyaWF0aW9uJywgJ3ZhbHVlJ10sXG4gIG1ldGhvZHM6IFsnYWZNR2V0Rm9ybUNvbnRyb2xFbGVtZW50J11cbn0pXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdkaWdpLWZvcm0tc2VsZWN0JyxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXG4gIHRlbXBsYXRlOiAnPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PicsXG4gIGlucHV0czogWydhZkFubm91bmNlSWZPcHRpb25hbCcsICdhZkFubm91bmNlSWZPcHRpb25hbFRleHQnLCAnYWZBdXRvZm9jdXMnLCAnYWZEZXNjcmlwdGlvbicsICdhZkRpc2FibGVWYWxpZGF0aW9uJywgJ2FmSWQnLCAnYWZMYWJlbCcsICdhZk5hbWUnLCAnYWZQbGFjZWhvbGRlcicsICdhZlJlcXVpcmVkJywgJ2FmUmVxdWlyZWRUZXh0JywgJ2FmU3RhcnRTZWxlY3RlZCcsICdhZlZhbGlkYXRpb24nLCAnYWZWYWxpZGF0aW9uVGV4dCcsICdhZlZhbHVlJywgJ2FmVmFyaWF0aW9uJywgJ3ZhbHVlJ11cbn0pXG5leHBvcnQgY2xhc3MgRGlnaUZvcm1TZWxlY3Qge1xuICBwcm90ZWN0ZWQgZWw6IEhUTUxFbGVtZW50O1xuICBjb25zdHJ1Y3RvcihjOiBDaGFuZ2VEZXRlY3RvclJlZiwgcjogRWxlbWVudFJlZiwgcHJvdGVjdGVkIHo6IE5nWm9uZSkge1xuICAgIGMuZGV0YWNoKCk7XG4gICAgdGhpcy5lbCA9IHIubmF0aXZlRWxlbWVudDtcbiAgICBwcm94eU91dHB1dHModGhpcywgdGhpcy5lbCwgWydhZk9uQ2hhbmdlJywgJ2FmT25Gb2N1cycsICdhZk9uRm9jdXNvdXQnLCAnYWZPbkJsdXInLCAnYWZPbkRpcnR5J10pO1xuICB9XG59XG5cblxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIERpZ2lGb3JtVGV4dGFyZWEgZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lGb3JtVGV4dGFyZWEge1xuICAvKipcbiAgICogVGV4dGFyZWF0ZWxlbWVudGV0cyAnb25jaGFuZ2UnLWV2ZW50LiBAZW4gVGhlIHRleHRhcmVhIGVsZW1lbnQncyAnb25jaGFuZ2UnIGV2ZW50LlxuICAgKi9cbiAgYWZPbkNoYW5nZTogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PGFueT4+O1xuICAvKipcbiAgICogVGV4dGFyZWF0ZWxlbWVudGV0cyAnb25ibHVyJy1ldmVudC4gQGVuIFRoZSB0ZXh0YXJlYSBlbGVtZW50J3MgJ29uYmx1cicgZXZlbnQuXG4gICAqL1xuICBhZk9uQmx1cjogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PGFueT4+O1xuICAvKipcbiAgICogVGV4dGFyZWF0ZWxlbWVudGV0cyAnb25rZXl1cCctZXZlbnQuIEBlbiBUaGUgdGV4dGFyZWEgZWxlbWVudCdzICdvbmtleXVwJyBldmVudC5cbiAgICovXG4gIGFmT25LZXl1cDogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PGFueT4+O1xuICAvKipcbiAgICogVGV4dGFyZWF0ZWxlbWVudGV0cyAnb25mb2N1cyctZXZlbnQuIEBlbiBUaGUgdGV4dGFyZWEgZWxlbWVudCdzICdvbmZvY3VzJyBldmVudC5cbiAgICovXG4gIGFmT25Gb2N1czogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PGFueT4+O1xuICAvKipcbiAgICogVGV4dGFyZWF0ZWxlbWVudGV0cyAnb25mb2N1c291dCctZXZlbnQuIEBlbiBUaGUgdGV4dGFyZWEgZWxlbWVudCdzICdvbmZvY3Vzb3V0JyBldmVudC5cbiAgICovXG4gIGFmT25Gb2N1c291dDogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PGFueT4+O1xuICAvKipcbiAgICogVGV4dGFyZWF0ZWxlbWVudGV0cyAnb25pbnB1dCctZXZlbnQuIEBlbiBUaGUgdGV4dGFyZWEgZWxlbWVudCdzICdvbmlucHV0JyBldmVudC5cbiAgICovXG4gIGFmT25JbnB1dDogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PGFueT4+O1xuICAvKipcbiAgICogU2tlciB2aWQgdGV4dGFyZWFlbGVtZW50ZXRzIGbDtnJzdGEgJ29uaW5wdXQnIEBlbiBGaXJzdCB0aW1lIHRoZSB0ZXh0YXJlYSBlbGVtZW50IHJlY2VpdmVzIGFuIGlucHV0XG4gICAqL1xuICBhZk9uRGlydHk6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxhbnk+PjtcbiAgLyoqXG4gICAqIFNrZXIgdmlkIHRleHRhcmVhZWxlbWVudGV0cyBmw7Zyc3RhICdvbmJsdXInIEBlbiBGaXJzdCB0aW1lIHRoZSB0ZXh0ZWxlbWVudCBpcyBibHVycmVkXG4gICAqL1xuICBhZk9uVG91Y2hlZDogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PGFueT4+O1xuXG59XG5cbkBQcm94eUNtcCh7XG4gIGRlZmluZUN1c3RvbUVsZW1lbnRGbjogZGVmaW5lRGlnaUZvcm1UZXh0YXJlYSxcbiAgaW5wdXRzOiBbJ2FmQW5ub3VuY2VJZk9wdGlvbmFsJywgJ2FmQW5ub3VuY2VJZk9wdGlvbmFsVGV4dCcsICdhZkFyaWFBY3RpdmVkZXNjZW5kYW50JywgJ2FmQXJpYURlc2NyaWJlZGJ5JywgJ2FmQXJpYUxhYmVsbGVkYnknLCAnYWZBdXRvZm9jdXMnLCAnYWZJZCcsICdhZkxhYmVsJywgJ2FmTGFiZWxEZXNjcmlwdGlvbicsICdhZk1heGxlbmd0aCcsICdhZk1pbmxlbmd0aCcsICdhZk5hbWUnLCAnYWZSZXF1aXJlZCcsICdhZlJlcXVpcmVkVGV4dCcsICdhZlJvbGUnLCAnYWZWYWxpZGF0aW9uJywgJ2FmVmFsaWRhdGlvblRleHQnLCAnYWZWYWx1ZScsICdhZlZhcmlhdGlvbicsICd2YWx1ZSddLFxuICBtZXRob2RzOiBbJ2FmTUdldEZvcm1Db250cm9sRWxlbWVudCddXG59KVxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGlnaS1mb3JtLXRleHRhcmVhJyxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXG4gIHRlbXBsYXRlOiAnPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PicsXG4gIGlucHV0czogWydhZkFubm91bmNlSWZPcHRpb25hbCcsICdhZkFubm91bmNlSWZPcHRpb25hbFRleHQnLCAnYWZBcmlhQWN0aXZlZGVzY2VuZGFudCcsICdhZkFyaWFEZXNjcmliZWRieScsICdhZkFyaWFMYWJlbGxlZGJ5JywgJ2FmQXV0b2ZvY3VzJywgJ2FmSWQnLCAnYWZMYWJlbCcsICdhZkxhYmVsRGVzY3JpcHRpb24nLCAnYWZNYXhsZW5ndGgnLCAnYWZNaW5sZW5ndGgnLCAnYWZOYW1lJywgJ2FmUmVxdWlyZWQnLCAnYWZSZXF1aXJlZFRleHQnLCAnYWZSb2xlJywgJ2FmVmFsaWRhdGlvbicsICdhZlZhbGlkYXRpb25UZXh0JywgJ2FmVmFsdWUnLCAnYWZWYXJpYXRpb24nLCAndmFsdWUnXVxufSlcbmV4cG9ydCBjbGFzcyBEaWdpRm9ybVRleHRhcmVhIHtcbiAgcHJvdGVjdGVkIGVsOiBIVE1MRWxlbWVudDtcbiAgY29uc3RydWN0b3IoYzogQ2hhbmdlRGV0ZWN0b3JSZWYsIHI6IEVsZW1lbnRSZWYsIHByb3RlY3RlZCB6OiBOZ1pvbmUpIHtcbiAgICBjLmRldGFjaCgpO1xuICAgIHRoaXMuZWwgPSByLm5hdGl2ZUVsZW1lbnQ7XG4gICAgcHJveHlPdXRwdXRzKHRoaXMsIHRoaXMuZWwsIFsnYWZPbkNoYW5nZScsICdhZk9uQmx1cicsICdhZk9uS2V5dXAnLCAnYWZPbkZvY3VzJywgJ2FmT25Gb2N1c291dCcsICdhZk9uSW5wdXQnLCAnYWZPbkRpcnR5JywgJ2FmT25Ub3VjaGVkJ10pO1xuICB9XG59XG5cblxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIERpZ2lGb3JtVmFsaWRhdGlvbk1lc3NhZ2UgZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lGb3JtVmFsaWRhdGlvbk1lc3NhZ2Uge31cblxuQFByb3h5Q21wKHtcbiAgZGVmaW5lQ3VzdG9tRWxlbWVudEZuOiBkZWZpbmVEaWdpRm9ybVZhbGlkYXRpb25NZXNzYWdlLFxuICBpbnB1dHM6IFsnYWZWYXJpYXRpb24nXVxufSlcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2RpZ2ktZm9ybS12YWxpZGF0aW9uLW1lc3NhZ2UnLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaCxcbiAgdGVtcGxhdGU6ICc8bmctY29udGVudD48L25nLWNvbnRlbnQ+JyxcbiAgaW5wdXRzOiBbJ2FmVmFyaWF0aW9uJ11cbn0pXG5leHBvcnQgY2xhc3MgRGlnaUZvcm1WYWxpZGF0aW9uTWVzc2FnZSB7XG4gIHByb3RlY3RlZCBlbDogSFRNTEVsZW1lbnQ7XG4gIGNvbnN0cnVjdG9yKGM6IENoYW5nZURldGVjdG9yUmVmLCByOiBFbGVtZW50UmVmLCBwcm90ZWN0ZWQgejogTmdab25lKSB7XG4gICAgYy5kZXRhY2goKTtcbiAgICB0aGlzLmVsID0gci5uYXRpdmVFbGVtZW50O1xuICB9XG59XG5cblxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIERpZ2lJY29uIGV4dGVuZHMgQ29tcG9uZW50cy5EaWdpSWNvbiB7fVxuXG5AUHJveHlDbXAoe1xuICBkZWZpbmVDdXN0b21FbGVtZW50Rm46IGRlZmluZURpZ2lJY29uLFxuICBpbnB1dHM6IFsnYWZEZXNjJywgJ2FmTmFtZScsICdhZlN2Z0FyaWFIaWRkZW4nLCAnYWZUaXRsZSddXG59KVxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGlnaS1pY29uJyxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXG4gIHRlbXBsYXRlOiAnPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PicsXG4gIGlucHV0czogWydhZkRlc2MnLCAnYWZOYW1lJywgJ2FmU3ZnQXJpYUhpZGRlbicsICdhZlRpdGxlJ11cbn0pXG5leHBvcnQgY2xhc3MgRGlnaUljb24ge1xuICBwcm90ZWN0ZWQgZWw6IEhUTUxFbGVtZW50O1xuICBjb25zdHJ1Y3RvcihjOiBDaGFuZ2VEZXRlY3RvclJlZiwgcjogRWxlbWVudFJlZiwgcHJvdGVjdGVkIHo6IE5nWm9uZSkge1xuICAgIGMuZGV0YWNoKCk7XG4gICAgdGhpcy5lbCA9IHIubmF0aXZlRWxlbWVudDtcbiAgfVxufVxuXG5cbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBEaWdpSWNvbkJhcnMgZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lJY29uQmFycyB7fVxuXG5AUHJveHlDbXAoe1xuICBkZWZpbmVDdXN0b21FbGVtZW50Rm46IGRlZmluZURpZ2lJY29uQmFycyxcbiAgaW5wdXRzOiBbJ2FmRGVzYycsICdhZlN2Z0FyaWFIaWRkZW4nLCAnYWZUaXRsZSddXG59KVxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGlnaS1pY29uLWJhcnMnLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaCxcbiAgdGVtcGxhdGU6ICc8bmctY29udGVudD48L25nLWNvbnRlbnQ+JyxcbiAgaW5wdXRzOiBbJ2FmRGVzYycsICdhZlN2Z0FyaWFIaWRkZW4nLCAnYWZUaXRsZSddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lJY29uQmFycyB7XG4gIHByb3RlY3RlZCBlbDogSFRNTEVsZW1lbnQ7XG4gIGNvbnN0cnVjdG9yKGM6IENoYW5nZURldGVjdG9yUmVmLCByOiBFbGVtZW50UmVmLCBwcm90ZWN0ZWQgejogTmdab25lKSB7XG4gICAgYy5kZXRhY2goKTtcbiAgICB0aGlzLmVsID0gci5uYXRpdmVFbGVtZW50O1xuICB9XG59XG5cblxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIERpZ2lJY29uQ2hlY2sgZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lJY29uQ2hlY2sge31cblxuQFByb3h5Q21wKHtcbiAgZGVmaW5lQ3VzdG9tRWxlbWVudEZuOiBkZWZpbmVEaWdpSWNvbkNoZWNrLFxuICBpbnB1dHM6IFsnYWZEZXNjJywgJ2FmU3ZnQXJpYUhpZGRlbicsICdhZlRpdGxlJ11cbn0pXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdkaWdpLWljb24tY2hlY2snLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaCxcbiAgdGVtcGxhdGU6ICc8bmctY29udGVudD48L25nLWNvbnRlbnQ+JyxcbiAgaW5wdXRzOiBbJ2FmRGVzYycsICdhZlN2Z0FyaWFIaWRkZW4nLCAnYWZUaXRsZSddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lJY29uQ2hlY2sge1xuICBwcm90ZWN0ZWQgZWw6IEhUTUxFbGVtZW50O1xuICBjb25zdHJ1Y3RvcihjOiBDaGFuZ2VEZXRlY3RvclJlZiwgcjogRWxlbWVudFJlZiwgcHJvdGVjdGVkIHo6IE5nWm9uZSkge1xuICAgIGMuZGV0YWNoKCk7XG4gICAgdGhpcy5lbCA9IHIubmF0aXZlRWxlbWVudDtcbiAgfVxufVxuXG5cbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBEaWdpSWNvbkNoZWNrQ2lyY2xlIGV4dGVuZHMgQ29tcG9uZW50cy5EaWdpSWNvbkNoZWNrQ2lyY2xlIHt9XG5cbkBQcm94eUNtcCh7XG4gIGRlZmluZUN1c3RvbUVsZW1lbnRGbjogZGVmaW5lRGlnaUljb25DaGVja0NpcmNsZSxcbiAgaW5wdXRzOiBbJ2FmRGVzYycsICdhZlN2Z0FyaWFIaWRkZW4nLCAnYWZUaXRsZSddXG59KVxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGlnaS1pY29uLWNoZWNrLWNpcmNsZScsXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxuICB0ZW1wbGF0ZTogJzxuZy1jb250ZW50PjwvbmctY29udGVudD4nLFxuICBpbnB1dHM6IFsnYWZEZXNjJywgJ2FmU3ZnQXJpYUhpZGRlbicsICdhZlRpdGxlJ11cbn0pXG5leHBvcnQgY2xhc3MgRGlnaUljb25DaGVja0NpcmNsZSB7XG4gIHByb3RlY3RlZCBlbDogSFRNTEVsZW1lbnQ7XG4gIGNvbnN0cnVjdG9yKGM6IENoYW5nZURldGVjdG9yUmVmLCByOiBFbGVtZW50UmVmLCBwcm90ZWN0ZWQgejogTmdab25lKSB7XG4gICAgYy5kZXRhY2goKTtcbiAgICB0aGlzLmVsID0gci5uYXRpdmVFbGVtZW50O1xuICB9XG59XG5cblxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIERpZ2lJY29uQ2hlY2tDaXJjbGVSZWcgZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lJY29uQ2hlY2tDaXJjbGVSZWcge31cblxuQFByb3h5Q21wKHtcbiAgZGVmaW5lQ3VzdG9tRWxlbWVudEZuOiBkZWZpbmVEaWdpSWNvbkNoZWNrQ2lyY2xlUmVnLFxuICBpbnB1dHM6IFsnYWZEZXNjJywgJ2FmU3ZnQXJpYUhpZGRlbicsICdhZlRpdGxlJ11cbn0pXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdkaWdpLWljb24tY2hlY2stY2lyY2xlLXJlZycsXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxuICB0ZW1wbGF0ZTogJzxuZy1jb250ZW50PjwvbmctY29udGVudD4nLFxuICBpbnB1dHM6IFsnYWZEZXNjJywgJ2FmU3ZnQXJpYUhpZGRlbicsICdhZlRpdGxlJ11cbn0pXG5leHBvcnQgY2xhc3MgRGlnaUljb25DaGVja0NpcmNsZVJlZyB7XG4gIHByb3RlY3RlZCBlbDogSFRNTEVsZW1lbnQ7XG4gIGNvbnN0cnVjdG9yKGM6IENoYW5nZURldGVjdG9yUmVmLCByOiBFbGVtZW50UmVmLCBwcm90ZWN0ZWQgejogTmdab25lKSB7XG4gICAgYy5kZXRhY2goKTtcbiAgICB0aGlzLmVsID0gci5uYXRpdmVFbGVtZW50O1xuICB9XG59XG5cblxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIERpZ2lJY29uQ2hlY2tDaXJjbGVSZWdBbHQgZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lJY29uQ2hlY2tDaXJjbGVSZWdBbHQge31cblxuQFByb3h5Q21wKHtcbiAgZGVmaW5lQ3VzdG9tRWxlbWVudEZuOiBkZWZpbmVEaWdpSWNvbkNoZWNrQ2lyY2xlUmVnQWx0LFxuICBpbnB1dHM6IFsnYWZEZXNjJywgJ2FmU3ZnQXJpYUhpZGRlbicsICdhZlRpdGxlJ11cbn0pXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdkaWdpLWljb24tY2hlY2stY2lyY2xlLXJlZy1hbHQnLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaCxcbiAgdGVtcGxhdGU6ICc8bmctY29udGVudD48L25nLWNvbnRlbnQ+JyxcbiAgaW5wdXRzOiBbJ2FmRGVzYycsICdhZlN2Z0FyaWFIaWRkZW4nLCAnYWZUaXRsZSddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lJY29uQ2hlY2tDaXJjbGVSZWdBbHQge1xuICBwcm90ZWN0ZWQgZWw6IEhUTUxFbGVtZW50O1xuICBjb25zdHJ1Y3RvcihjOiBDaGFuZ2VEZXRlY3RvclJlZiwgcjogRWxlbWVudFJlZiwgcHJvdGVjdGVkIHo6IE5nWm9uZSkge1xuICAgIGMuZGV0YWNoKCk7XG4gICAgdGhpcy5lbCA9IHIubmF0aXZlRWxlbWVudDtcbiAgfVxufVxuXG5cbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBEaWdpSWNvbkNoZXZyb25Eb3duIGV4dGVuZHMgQ29tcG9uZW50cy5EaWdpSWNvbkNoZXZyb25Eb3duIHt9XG5cbkBQcm94eUNtcCh7XG4gIGRlZmluZUN1c3RvbUVsZW1lbnRGbjogZGVmaW5lRGlnaUljb25DaGV2cm9uRG93bixcbiAgaW5wdXRzOiBbJ2FmRGVzYycsICdhZlN2Z0FyaWFIaWRkZW4nLCAnYWZUaXRsZSddXG59KVxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGlnaS1pY29uLWNoZXZyb24tZG93bicsXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxuICB0ZW1wbGF0ZTogJzxuZy1jb250ZW50PjwvbmctY29udGVudD4nLFxuICBpbnB1dHM6IFsnYWZEZXNjJywgJ2FmU3ZnQXJpYUhpZGRlbicsICdhZlRpdGxlJ11cbn0pXG5leHBvcnQgY2xhc3MgRGlnaUljb25DaGV2cm9uRG93biB7XG4gIHByb3RlY3RlZCBlbDogSFRNTEVsZW1lbnQ7XG4gIGNvbnN0cnVjdG9yKGM6IENoYW5nZURldGVjdG9yUmVmLCByOiBFbGVtZW50UmVmLCBwcm90ZWN0ZWQgejogTmdab25lKSB7XG4gICAgYy5kZXRhY2goKTtcbiAgICB0aGlzLmVsID0gci5uYXRpdmVFbGVtZW50O1xuICB9XG59XG5cblxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIERpZ2lJY29uQ2hldnJvbkxlZnQgZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lJY29uQ2hldnJvbkxlZnQge31cblxuQFByb3h5Q21wKHtcbiAgZGVmaW5lQ3VzdG9tRWxlbWVudEZuOiBkZWZpbmVEaWdpSWNvbkNoZXZyb25MZWZ0LFxuICBpbnB1dHM6IFsnYWZEZXNjJywgJ2FmU3ZnQXJpYUhpZGRlbicsICdhZlRpdGxlJ11cbn0pXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdkaWdpLWljb24tY2hldnJvbi1sZWZ0JyxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXG4gIHRlbXBsYXRlOiAnPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PicsXG4gIGlucHV0czogWydhZkRlc2MnLCAnYWZTdmdBcmlhSGlkZGVuJywgJ2FmVGl0bGUnXVxufSlcbmV4cG9ydCBjbGFzcyBEaWdpSWNvbkNoZXZyb25MZWZ0IHtcbiAgcHJvdGVjdGVkIGVsOiBIVE1MRWxlbWVudDtcbiAgY29uc3RydWN0b3IoYzogQ2hhbmdlRGV0ZWN0b3JSZWYsIHI6IEVsZW1lbnRSZWYsIHByb3RlY3RlZCB6OiBOZ1pvbmUpIHtcbiAgICBjLmRldGFjaCgpO1xuICAgIHRoaXMuZWwgPSByLm5hdGl2ZUVsZW1lbnQ7XG4gIH1cbn1cblxuXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgRGlnaUljb25DaGV2cm9uUmlnaHQgZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lJY29uQ2hldnJvblJpZ2h0IHt9XG5cbkBQcm94eUNtcCh7XG4gIGRlZmluZUN1c3RvbUVsZW1lbnRGbjogZGVmaW5lRGlnaUljb25DaGV2cm9uUmlnaHQsXG4gIGlucHV0czogWydhZkRlc2MnLCAnYWZTdmdBcmlhSGlkZGVuJywgJ2FmVGl0bGUnXVxufSlcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2RpZ2ktaWNvbi1jaGV2cm9uLXJpZ2h0JyxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXG4gIHRlbXBsYXRlOiAnPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PicsXG4gIGlucHV0czogWydhZkRlc2MnLCAnYWZTdmdBcmlhSGlkZGVuJywgJ2FmVGl0bGUnXVxufSlcbmV4cG9ydCBjbGFzcyBEaWdpSWNvbkNoZXZyb25SaWdodCB7XG4gIHByb3RlY3RlZCBlbDogSFRNTEVsZW1lbnQ7XG4gIGNvbnN0cnVjdG9yKGM6IENoYW5nZURldGVjdG9yUmVmLCByOiBFbGVtZW50UmVmLCBwcm90ZWN0ZWQgejogTmdab25lKSB7XG4gICAgYy5kZXRhY2goKTtcbiAgICB0aGlzLmVsID0gci5uYXRpdmVFbGVtZW50O1xuICB9XG59XG5cblxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIERpZ2lJY29uQ2hldnJvblVwIGV4dGVuZHMgQ29tcG9uZW50cy5EaWdpSWNvbkNoZXZyb25VcCB7fVxuXG5AUHJveHlDbXAoe1xuICBkZWZpbmVDdXN0b21FbGVtZW50Rm46IGRlZmluZURpZ2lJY29uQ2hldnJvblVwLFxuICBpbnB1dHM6IFsnYWZEZXNjJywgJ2FmU3ZnQXJpYUhpZGRlbicsICdhZlRpdGxlJ11cbn0pXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdkaWdpLWljb24tY2hldnJvbi11cCcsXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxuICB0ZW1wbGF0ZTogJzxuZy1jb250ZW50PjwvbmctY29udGVudD4nLFxuICBpbnB1dHM6IFsnYWZEZXNjJywgJ2FmU3ZnQXJpYUhpZGRlbicsICdhZlRpdGxlJ11cbn0pXG5leHBvcnQgY2xhc3MgRGlnaUljb25DaGV2cm9uVXAge1xuICBwcm90ZWN0ZWQgZWw6IEhUTUxFbGVtZW50O1xuICBjb25zdHJ1Y3RvcihjOiBDaGFuZ2VEZXRlY3RvclJlZiwgcjogRWxlbWVudFJlZiwgcHJvdGVjdGVkIHo6IE5nWm9uZSkge1xuICAgIGMuZGV0YWNoKCk7XG4gICAgdGhpcy5lbCA9IHIubmF0aXZlRWxlbWVudDtcbiAgfVxufVxuXG5cbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBEaWdpSWNvbkNvcHkgZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lJY29uQ29weSB7fVxuXG5AUHJveHlDbXAoe1xuICBkZWZpbmVDdXN0b21FbGVtZW50Rm46IGRlZmluZURpZ2lJY29uQ29weSxcbiAgaW5wdXRzOiBbJ2FmRGVzYycsICdhZlN2Z0FyaWFIaWRkZW4nLCAnYWZUaXRsZSddXG59KVxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGlnaS1pY29uLWNvcHknLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaCxcbiAgdGVtcGxhdGU6ICc8bmctY29udGVudD48L25nLWNvbnRlbnQ+JyxcbiAgaW5wdXRzOiBbJ2FmRGVzYycsICdhZlN2Z0FyaWFIaWRkZW4nLCAnYWZUaXRsZSddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lJY29uQ29weSB7XG4gIHByb3RlY3RlZCBlbDogSFRNTEVsZW1lbnQ7XG4gIGNvbnN0cnVjdG9yKGM6IENoYW5nZURldGVjdG9yUmVmLCByOiBFbGVtZW50UmVmLCBwcm90ZWN0ZWQgejogTmdab25lKSB7XG4gICAgYy5kZXRhY2goKTtcbiAgICB0aGlzLmVsID0gci5uYXRpdmVFbGVtZW50O1xuICB9XG59XG5cblxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIERpZ2lJY29uRGFuZ2VyT3V0bGluZSBleHRlbmRzIENvbXBvbmVudHMuRGlnaUljb25EYW5nZXJPdXRsaW5lIHt9XG5cbkBQcm94eUNtcCh7XG4gIGRlZmluZUN1c3RvbUVsZW1lbnRGbjogZGVmaW5lRGlnaUljb25EYW5nZXJPdXRsaW5lLFxuICBpbnB1dHM6IFsnYWZEZXNjJywgJ2FmU3ZnQXJpYUhpZGRlbicsICdhZlRpdGxlJ11cbn0pXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdkaWdpLWljb24tZGFuZ2VyLW91dGxpbmUnLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaCxcbiAgdGVtcGxhdGU6ICc8bmctY29udGVudD48L25nLWNvbnRlbnQ+JyxcbiAgaW5wdXRzOiBbJ2FmRGVzYycsICdhZlN2Z0FyaWFIaWRkZW4nLCAnYWZUaXRsZSddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lJY29uRGFuZ2VyT3V0bGluZSB7XG4gIHByb3RlY3RlZCBlbDogSFRNTEVsZW1lbnQ7XG4gIGNvbnN0cnVjdG9yKGM6IENoYW5nZURldGVjdG9yUmVmLCByOiBFbGVtZW50UmVmLCBwcm90ZWN0ZWQgejogTmdab25lKSB7XG4gICAgYy5kZXRhY2goKTtcbiAgICB0aGlzLmVsID0gci5uYXRpdmVFbGVtZW50O1xuICB9XG59XG5cblxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIERpZ2lJY29uRG93bmxvYWQgZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lJY29uRG93bmxvYWQge31cblxuQFByb3h5Q21wKHtcbiAgZGVmaW5lQ3VzdG9tRWxlbWVudEZuOiBkZWZpbmVEaWdpSWNvbkRvd25sb2FkLFxuICBpbnB1dHM6IFsnYWZEZXNjJywgJ2FmU3ZnQXJpYUhpZGRlbicsICdhZlRpdGxlJ11cbn0pXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdkaWdpLWljb24tZG93bmxvYWQnLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaCxcbiAgdGVtcGxhdGU6ICc8bmctY29udGVudD48L25nLWNvbnRlbnQ+JyxcbiAgaW5wdXRzOiBbJ2FmRGVzYycsICdhZlN2Z0FyaWFIaWRkZW4nLCAnYWZUaXRsZSddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lJY29uRG93bmxvYWQge1xuICBwcm90ZWN0ZWQgZWw6IEhUTUxFbGVtZW50O1xuICBjb25zdHJ1Y3RvcihjOiBDaGFuZ2VEZXRlY3RvclJlZiwgcjogRWxlbWVudFJlZiwgcHJvdGVjdGVkIHo6IE5nWm9uZSkge1xuICAgIGMuZGV0YWNoKCk7XG4gICAgdGhpcy5lbCA9IHIubmF0aXZlRWxlbWVudDtcbiAgfVxufVxuXG5cbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBEaWdpSWNvbkV4Y2xhbWF0aW9uQ2lyY2xlIGV4dGVuZHMgQ29tcG9uZW50cy5EaWdpSWNvbkV4Y2xhbWF0aW9uQ2lyY2xlIHt9XG5cbkBQcm94eUNtcCh7XG4gIGRlZmluZUN1c3RvbUVsZW1lbnRGbjogZGVmaW5lRGlnaUljb25FeGNsYW1hdGlvbkNpcmNsZSxcbiAgaW5wdXRzOiBbJ2FmRGVzYycsICdhZlN2Z0FyaWFIaWRkZW4nLCAnYWZUaXRsZSddXG59KVxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGlnaS1pY29uLWV4Y2xhbWF0aW9uLWNpcmNsZScsXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxuICB0ZW1wbGF0ZTogJzxuZy1jb250ZW50PjwvbmctY29udGVudD4nLFxuICBpbnB1dHM6IFsnYWZEZXNjJywgJ2FmU3ZnQXJpYUhpZGRlbicsICdhZlRpdGxlJ11cbn0pXG5leHBvcnQgY2xhc3MgRGlnaUljb25FeGNsYW1hdGlvbkNpcmNsZSB7XG4gIHByb3RlY3RlZCBlbDogSFRNTEVsZW1lbnQ7XG4gIGNvbnN0cnVjdG9yKGM6IENoYW5nZURldGVjdG9yUmVmLCByOiBFbGVtZW50UmVmLCBwcm90ZWN0ZWQgejogTmdab25lKSB7XG4gICAgYy5kZXRhY2goKTtcbiAgICB0aGlzLmVsID0gci5uYXRpdmVFbGVtZW50O1xuICB9XG59XG5cblxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIERpZ2lJY29uRXhjbGFtYXRpb25DaXJjbGVGaWxsZWQgZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lJY29uRXhjbGFtYXRpb25DaXJjbGVGaWxsZWQge31cblxuQFByb3h5Q21wKHtcbiAgZGVmaW5lQ3VzdG9tRWxlbWVudEZuOiBkZWZpbmVEaWdpSWNvbkV4Y2xhbWF0aW9uQ2lyY2xlRmlsbGVkLFxuICBpbnB1dHM6IFsnYWZEZXNjJywgJ2FmU3ZnQXJpYUhpZGRlbicsICdhZlRpdGxlJ11cbn0pXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdkaWdpLWljb24tZXhjbGFtYXRpb24tY2lyY2xlLWZpbGxlZCcsXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxuICB0ZW1wbGF0ZTogJzxuZy1jb250ZW50PjwvbmctY29udGVudD4nLFxuICBpbnB1dHM6IFsnYWZEZXNjJywgJ2FmU3ZnQXJpYUhpZGRlbicsICdhZlRpdGxlJ11cbn0pXG5leHBvcnQgY2xhc3MgRGlnaUljb25FeGNsYW1hdGlvbkNpcmNsZUZpbGxlZCB7XG4gIHByb3RlY3RlZCBlbDogSFRNTEVsZW1lbnQ7XG4gIGNvbnN0cnVjdG9yKGM6IENoYW5nZURldGVjdG9yUmVmLCByOiBFbGVtZW50UmVmLCBwcm90ZWN0ZWQgejogTmdab25lKSB7XG4gICAgYy5kZXRhY2goKTtcbiAgICB0aGlzLmVsID0gci5uYXRpdmVFbGVtZW50O1xuICB9XG59XG5cblxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIERpZ2lJY29uRXhjbGFtYXRpb25UcmlhbmdsZSBleHRlbmRzIENvbXBvbmVudHMuRGlnaUljb25FeGNsYW1hdGlvblRyaWFuZ2xlIHt9XG5cbkBQcm94eUNtcCh7XG4gIGRlZmluZUN1c3RvbUVsZW1lbnRGbjogZGVmaW5lRGlnaUljb25FeGNsYW1hdGlvblRyaWFuZ2xlLFxuICBpbnB1dHM6IFsnYWZEZXNjJywgJ2FmU3ZnQXJpYUhpZGRlbicsICdhZlRpdGxlJ11cbn0pXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdkaWdpLWljb24tZXhjbGFtYXRpb24tdHJpYW5nbGUnLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaCxcbiAgdGVtcGxhdGU6ICc8bmctY29udGVudD48L25nLWNvbnRlbnQ+JyxcbiAgaW5wdXRzOiBbJ2FmRGVzYycsICdhZlN2Z0FyaWFIaWRkZW4nLCAnYWZUaXRsZSddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lJY29uRXhjbGFtYXRpb25UcmlhbmdsZSB7XG4gIHByb3RlY3RlZCBlbDogSFRNTEVsZW1lbnQ7XG4gIGNvbnN0cnVjdG9yKGM6IENoYW5nZURldGVjdG9yUmVmLCByOiBFbGVtZW50UmVmLCBwcm90ZWN0ZWQgejogTmdab25lKSB7XG4gICAgYy5kZXRhY2goKTtcbiAgICB0aGlzLmVsID0gci5uYXRpdmVFbGVtZW50O1xuICB9XG59XG5cblxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIERpZ2lJY29uRXhjbGFtYXRpb25UcmlhbmdsZVdhcm5pbmcgZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lJY29uRXhjbGFtYXRpb25UcmlhbmdsZVdhcm5pbmcge31cblxuQFByb3h5Q21wKHtcbiAgZGVmaW5lQ3VzdG9tRWxlbWVudEZuOiBkZWZpbmVEaWdpSWNvbkV4Y2xhbWF0aW9uVHJpYW5nbGVXYXJuaW5nLFxuICBpbnB1dHM6IFsnYWZEZXNjJywgJ2FmU3ZnQXJpYUhpZGRlbicsICdhZlRpdGxlJ11cbn0pXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdkaWdpLWljb24tZXhjbGFtYXRpb24tdHJpYW5nbGUtd2FybmluZycsXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxuICB0ZW1wbGF0ZTogJzxuZy1jb250ZW50PjwvbmctY29udGVudD4nLFxuICBpbnB1dHM6IFsnYWZEZXNjJywgJ2FmU3ZnQXJpYUhpZGRlbicsICdhZlRpdGxlJ11cbn0pXG5leHBvcnQgY2xhc3MgRGlnaUljb25FeGNsYW1hdGlvblRyaWFuZ2xlV2FybmluZyB7XG4gIHByb3RlY3RlZCBlbDogSFRNTEVsZW1lbnQ7XG4gIGNvbnN0cnVjdG9yKGM6IENoYW5nZURldGVjdG9yUmVmLCByOiBFbGVtZW50UmVmLCBwcm90ZWN0ZWQgejogTmdab25lKSB7XG4gICAgYy5kZXRhY2goKTtcbiAgICB0aGlzLmVsID0gci5uYXRpdmVFbGVtZW50O1xuICB9XG59XG5cblxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIERpZ2lJY29uRXh0ZXJuYWxMaW5rQWx0IGV4dGVuZHMgQ29tcG9uZW50cy5EaWdpSWNvbkV4dGVybmFsTGlua0FsdCB7fVxuXG5AUHJveHlDbXAoe1xuICBkZWZpbmVDdXN0b21FbGVtZW50Rm46IGRlZmluZURpZ2lJY29uRXh0ZXJuYWxMaW5rQWx0LFxuICBpbnB1dHM6IFsnYWZEZXNjJywgJ2FmU3ZnQXJpYUhpZGRlbicsICdhZlRpdGxlJ11cbn0pXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdkaWdpLWljb24tZXh0ZXJuYWwtbGluay1hbHQnLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaCxcbiAgdGVtcGxhdGU6ICc8bmctY29udGVudD48L25nLWNvbnRlbnQ+JyxcbiAgaW5wdXRzOiBbJ2FmRGVzYycsICdhZlN2Z0FyaWFIaWRkZW4nLCAnYWZUaXRsZSddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lJY29uRXh0ZXJuYWxMaW5rQWx0IHtcbiAgcHJvdGVjdGVkIGVsOiBIVE1MRWxlbWVudDtcbiAgY29uc3RydWN0b3IoYzogQ2hhbmdlRGV0ZWN0b3JSZWYsIHI6IEVsZW1lbnRSZWYsIHByb3RlY3RlZCB6OiBOZ1pvbmUpIHtcbiAgICBjLmRldGFjaCgpO1xuICAgIHRoaXMuZWwgPSByLm5hdGl2ZUVsZW1lbnQ7XG4gIH1cbn1cblxuXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgRGlnaUljb25NaW51cyBleHRlbmRzIENvbXBvbmVudHMuRGlnaUljb25NaW51cyB7fVxuXG5AUHJveHlDbXAoe1xuICBkZWZpbmVDdXN0b21FbGVtZW50Rm46IGRlZmluZURpZ2lJY29uTWludXMsXG4gIGlucHV0czogWydhZkRlc2MnLCAnYWZTdmdBcmlhSGlkZGVuJywgJ2FmVGl0bGUnXVxufSlcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2RpZ2ktaWNvbi1taW51cycsXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxuICB0ZW1wbGF0ZTogJzxuZy1jb250ZW50PjwvbmctY29udGVudD4nLFxuICBpbnB1dHM6IFsnYWZEZXNjJywgJ2FmU3ZnQXJpYUhpZGRlbicsICdhZlRpdGxlJ11cbn0pXG5leHBvcnQgY2xhc3MgRGlnaUljb25NaW51cyB7XG4gIHByb3RlY3RlZCBlbDogSFRNTEVsZW1lbnQ7XG4gIGNvbnN0cnVjdG9yKGM6IENoYW5nZURldGVjdG9yUmVmLCByOiBFbGVtZW50UmVmLCBwcm90ZWN0ZWQgejogTmdab25lKSB7XG4gICAgYy5kZXRhY2goKTtcbiAgICB0aGlzLmVsID0gci5uYXRpdmVFbGVtZW50O1xuICB9XG59XG5cblxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIERpZ2lJY29uUGFwZXJjbGlwIGV4dGVuZHMgQ29tcG9uZW50cy5EaWdpSWNvblBhcGVyY2xpcCB7fVxuXG5AUHJveHlDbXAoe1xuICBkZWZpbmVDdXN0b21FbGVtZW50Rm46IGRlZmluZURpZ2lJY29uUGFwZXJjbGlwLFxuICBpbnB1dHM6IFsnYWZEZXNjJywgJ2FmU3ZnQXJpYUhpZGRlbicsICdhZlRpdGxlJ11cbn0pXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdkaWdpLWljb24tcGFwZXJjbGlwJyxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXG4gIHRlbXBsYXRlOiAnPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PicsXG4gIGlucHV0czogWydhZkRlc2MnLCAnYWZTdmdBcmlhSGlkZGVuJywgJ2FmVGl0bGUnXVxufSlcbmV4cG9ydCBjbGFzcyBEaWdpSWNvblBhcGVyY2xpcCB7XG4gIHByb3RlY3RlZCBlbDogSFRNTEVsZW1lbnQ7XG4gIGNvbnN0cnVjdG9yKGM6IENoYW5nZURldGVjdG9yUmVmLCByOiBFbGVtZW50UmVmLCBwcm90ZWN0ZWQgejogTmdab25lKSB7XG4gICAgYy5kZXRhY2goKTtcbiAgICB0aGlzLmVsID0gci5uYXRpdmVFbGVtZW50O1xuICB9XG59XG5cblxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIERpZ2lJY29uUGx1cyBleHRlbmRzIENvbXBvbmVudHMuRGlnaUljb25QbHVzIHt9XG5cbkBQcm94eUNtcCh7XG4gIGRlZmluZUN1c3RvbUVsZW1lbnRGbjogZGVmaW5lRGlnaUljb25QbHVzLFxuICBpbnB1dHM6IFsnYWZEZXNjJywgJ2FmU3ZnQXJpYUhpZGRlbicsICdhZlRpdGxlJ11cbn0pXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdkaWdpLWljb24tcGx1cycsXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxuICB0ZW1wbGF0ZTogJzxuZy1jb250ZW50PjwvbmctY29udGVudD4nLFxuICBpbnB1dHM6IFsnYWZEZXNjJywgJ2FmU3ZnQXJpYUhpZGRlbicsICdhZlRpdGxlJ11cbn0pXG5leHBvcnQgY2xhc3MgRGlnaUljb25QbHVzIHtcbiAgcHJvdGVjdGVkIGVsOiBIVE1MRWxlbWVudDtcbiAgY29uc3RydWN0b3IoYzogQ2hhbmdlRGV0ZWN0b3JSZWYsIHI6IEVsZW1lbnRSZWYsIHByb3RlY3RlZCB6OiBOZ1pvbmUpIHtcbiAgICBjLmRldGFjaCgpO1xuICAgIHRoaXMuZWwgPSByLm5hdGl2ZUVsZW1lbnQ7XG4gIH1cbn1cblxuXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgRGlnaUljb25TZWFyY2ggZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lJY29uU2VhcmNoIHt9XG5cbkBQcm94eUNtcCh7XG4gIGRlZmluZUN1c3RvbUVsZW1lbnRGbjogZGVmaW5lRGlnaUljb25TZWFyY2gsXG4gIGlucHV0czogWydhZkRlc2MnLCAnYWZTdmdBcmlhSGlkZGVuJywgJ2FmVGl0bGUnXVxufSlcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2RpZ2ktaWNvbi1zZWFyY2gnLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaCxcbiAgdGVtcGxhdGU6ICc8bmctY29udGVudD48L25nLWNvbnRlbnQ+JyxcbiAgaW5wdXRzOiBbJ2FmRGVzYycsICdhZlN2Z0FyaWFIaWRkZW4nLCAnYWZUaXRsZSddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lJY29uU2VhcmNoIHtcbiAgcHJvdGVjdGVkIGVsOiBIVE1MRWxlbWVudDtcbiAgY29uc3RydWN0b3IoYzogQ2hhbmdlRGV0ZWN0b3JSZWYsIHI6IEVsZW1lbnRSZWYsIHByb3RlY3RlZCB6OiBOZ1pvbmUpIHtcbiAgICBjLmRldGFjaCgpO1xuICAgIHRoaXMuZWwgPSByLm5hdGl2ZUVsZW1lbnQ7XG4gIH1cbn1cblxuXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgRGlnaUljb25TcGlubmVyIGV4dGVuZHMgQ29tcG9uZW50cy5EaWdpSWNvblNwaW5uZXIge31cblxuQFByb3h5Q21wKHtcbiAgZGVmaW5lQ3VzdG9tRWxlbWVudEZuOiBkZWZpbmVEaWdpSWNvblNwaW5uZXIsXG4gIGlucHV0czogWydhZkRlc2MnLCAnYWZTdmdBcmlhSGlkZGVuJywgJ2FmVGl0bGUnXVxufSlcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2RpZ2ktaWNvbi1zcGlubmVyJyxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXG4gIHRlbXBsYXRlOiAnPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PicsXG4gIGlucHV0czogWydhZkRlc2MnLCAnYWZTdmdBcmlhSGlkZGVuJywgJ2FmVGl0bGUnXVxufSlcbmV4cG9ydCBjbGFzcyBEaWdpSWNvblNwaW5uZXIge1xuICBwcm90ZWN0ZWQgZWw6IEhUTUxFbGVtZW50O1xuICBjb25zdHJ1Y3RvcihjOiBDaGFuZ2VEZXRlY3RvclJlZiwgcjogRWxlbWVudFJlZiwgcHJvdGVjdGVkIHo6IE5nWm9uZSkge1xuICAgIGMuZGV0YWNoKCk7XG4gICAgdGhpcy5lbCA9IHIubmF0aXZlRWxlbWVudDtcbiAgfVxufVxuXG5cbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBEaWdpSWNvblRyYXNoIGV4dGVuZHMgQ29tcG9uZW50cy5EaWdpSWNvblRyYXNoIHt9XG5cbkBQcm94eUNtcCh7XG4gIGRlZmluZUN1c3RvbUVsZW1lbnRGbjogZGVmaW5lRGlnaUljb25UcmFzaCxcbiAgaW5wdXRzOiBbJ2FmRGVzYycsICdhZlN2Z0FyaWFIaWRkZW4nLCAnYWZUaXRsZSddXG59KVxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGlnaS1pY29uLXRyYXNoJyxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXG4gIHRlbXBsYXRlOiAnPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PicsXG4gIGlucHV0czogWydhZkRlc2MnLCAnYWZTdmdBcmlhSGlkZGVuJywgJ2FmVGl0bGUnXVxufSlcbmV4cG9ydCBjbGFzcyBEaWdpSWNvblRyYXNoIHtcbiAgcHJvdGVjdGVkIGVsOiBIVE1MRWxlbWVudDtcbiAgY29uc3RydWN0b3IoYzogQ2hhbmdlRGV0ZWN0b3JSZWYsIHI6IEVsZW1lbnRSZWYsIHByb3RlY3RlZCB6OiBOZ1pvbmUpIHtcbiAgICBjLmRldGFjaCgpO1xuICAgIHRoaXMuZWwgPSByLm5hdGl2ZUVsZW1lbnQ7XG4gIH1cbn1cblxuXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgRGlnaUljb25YIGV4dGVuZHMgQ29tcG9uZW50cy5EaWdpSWNvblgge31cblxuQFByb3h5Q21wKHtcbiAgZGVmaW5lQ3VzdG9tRWxlbWVudEZuOiBkZWZpbmVEaWdpSWNvblgsXG4gIGlucHV0czogWydhZkRlc2MnLCAnYWZTdmdBcmlhSGlkZGVuJywgJ2FmVGl0bGUnXVxufSlcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2RpZ2ktaWNvbi14JyxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXG4gIHRlbXBsYXRlOiAnPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PicsXG4gIGlucHV0czogWydhZkRlc2MnLCAnYWZTdmdBcmlhSGlkZGVuJywgJ2FmVGl0bGUnXVxufSlcbmV4cG9ydCBjbGFzcyBEaWdpSWNvblgge1xuICBwcm90ZWN0ZWQgZWw6IEhUTUxFbGVtZW50O1xuICBjb25zdHJ1Y3RvcihjOiBDaGFuZ2VEZXRlY3RvclJlZiwgcjogRWxlbWVudFJlZiwgcHJvdGVjdGVkIHo6IE5nWm9uZSkge1xuICAgIGMuZGV0YWNoKCk7XG4gICAgdGhpcy5lbCA9IHIubmF0aXZlRWxlbWVudDtcbiAgfVxufVxuXG5cbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBEaWdpTGF5b3V0QmxvY2sgZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lMYXlvdXRCbG9jayB7fVxuXG5AUHJveHlDbXAoe1xuICBkZWZpbmVDdXN0b21FbGVtZW50Rm46IGRlZmluZURpZ2lMYXlvdXRCbG9jayxcbiAgaW5wdXRzOiBbJ2FmQ29udGFpbmVyJywgJ2FmTWFyZ2luQm90dG9tJywgJ2FmTWFyZ2luVG9wJywgJ2FmVmFyaWF0aW9uJywgJ2FmVmVydGljYWxQYWRkaW5nJ11cbn0pXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdkaWdpLWxheW91dC1ibG9jaycsXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxuICB0ZW1wbGF0ZTogJzxuZy1jb250ZW50PjwvbmctY29udGVudD4nLFxuICBpbnB1dHM6IFsnYWZDb250YWluZXInLCAnYWZNYXJnaW5Cb3R0b20nLCAnYWZNYXJnaW5Ub3AnLCAnYWZWYXJpYXRpb24nLCAnYWZWZXJ0aWNhbFBhZGRpbmcnXVxufSlcbmV4cG9ydCBjbGFzcyBEaWdpTGF5b3V0QmxvY2sge1xuICBwcm90ZWN0ZWQgZWw6IEhUTUxFbGVtZW50O1xuICBjb25zdHJ1Y3RvcihjOiBDaGFuZ2VEZXRlY3RvclJlZiwgcjogRWxlbWVudFJlZiwgcHJvdGVjdGVkIHo6IE5nWm9uZSkge1xuICAgIGMuZGV0YWNoKCk7XG4gICAgdGhpcy5lbCA9IHIubmF0aXZlRWxlbWVudDtcbiAgfVxufVxuXG5cbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBEaWdpTGF5b3V0Q29sdW1ucyBleHRlbmRzIENvbXBvbmVudHMuRGlnaUxheW91dENvbHVtbnMge31cblxuQFByb3h5Q21wKHtcbiAgZGVmaW5lQ3VzdG9tRWxlbWVudEZuOiBkZWZpbmVEaWdpTGF5b3V0Q29sdW1ucyxcbiAgaW5wdXRzOiBbJ2FmRWxlbWVudCcsICdhZlZhcmlhdGlvbiddXG59KVxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGlnaS1sYXlvdXQtY29sdW1ucycsXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxuICB0ZW1wbGF0ZTogJzxuZy1jb250ZW50PjwvbmctY29udGVudD4nLFxuICBpbnB1dHM6IFsnYWZFbGVtZW50JywgJ2FmVmFyaWF0aW9uJ11cbn0pXG5leHBvcnQgY2xhc3MgRGlnaUxheW91dENvbHVtbnMge1xuICBwcm90ZWN0ZWQgZWw6IEhUTUxFbGVtZW50O1xuICBjb25zdHJ1Y3RvcihjOiBDaGFuZ2VEZXRlY3RvclJlZiwgcjogRWxlbWVudFJlZiwgcHJvdGVjdGVkIHo6IE5nWm9uZSkge1xuICAgIGMuZGV0YWNoKCk7XG4gICAgdGhpcy5lbCA9IHIubmF0aXZlRWxlbWVudDtcbiAgfVxufVxuXG5cbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBEaWdpTGF5b3V0Q29udGFpbmVyIGV4dGVuZHMgQ29tcG9uZW50cy5EaWdpTGF5b3V0Q29udGFpbmVyIHt9XG5cbkBQcm94eUNtcCh7XG4gIGRlZmluZUN1c3RvbUVsZW1lbnRGbjogZGVmaW5lRGlnaUxheW91dENvbnRhaW5lcixcbiAgaW5wdXRzOiBbJ2FmTWFyZ2luQm90dG9tJywgJ2FmTWFyZ2luVG9wJywgJ2FmTm9HdXR0ZXInLCAnYWZWYXJpYXRpb24nLCAnYWZWZXJ0aWNhbFBhZGRpbmcnXVxufSlcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2RpZ2ktbGF5b3V0LWNvbnRhaW5lcicsXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxuICB0ZW1wbGF0ZTogJzxuZy1jb250ZW50PjwvbmctY29udGVudD4nLFxuICBpbnB1dHM6IFsnYWZNYXJnaW5Cb3R0b20nLCAnYWZNYXJnaW5Ub3AnLCAnYWZOb0d1dHRlcicsICdhZlZhcmlhdGlvbicsICdhZlZlcnRpY2FsUGFkZGluZyddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lMYXlvdXRDb250YWluZXIge1xuICBwcm90ZWN0ZWQgZWw6IEhUTUxFbGVtZW50O1xuICBjb25zdHJ1Y3RvcihjOiBDaGFuZ2VEZXRlY3RvclJlZiwgcjogRWxlbWVudFJlZiwgcHJvdGVjdGVkIHo6IE5nWm9uZSkge1xuICAgIGMuZGV0YWNoKCk7XG4gICAgdGhpcy5lbCA9IHIubmF0aXZlRWxlbWVudDtcbiAgfVxufVxuXG5cbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBEaWdpTGF5b3V0R3JpZCBleHRlbmRzIENvbXBvbmVudHMuRGlnaUxheW91dEdyaWQge31cblxuQFByb3h5Q21wKHtcbiAgZGVmaW5lQ3VzdG9tRWxlbWVudEZuOiBkZWZpbmVEaWdpTGF5b3V0R3JpZCxcbiAgaW5wdXRzOiBbJ2FmVmVydGljYWxTcGFjaW5nJ11cbn0pXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdkaWdpLWxheW91dC1ncmlkJyxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXG4gIHRlbXBsYXRlOiAnPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PicsXG4gIGlucHV0czogWydhZlZlcnRpY2FsU3BhY2luZyddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lMYXlvdXRHcmlkIHtcbiAgcHJvdGVjdGVkIGVsOiBIVE1MRWxlbWVudDtcbiAgY29uc3RydWN0b3IoYzogQ2hhbmdlRGV0ZWN0b3JSZWYsIHI6IEVsZW1lbnRSZWYsIHByb3RlY3RlZCB6OiBOZ1pvbmUpIHtcbiAgICBjLmRldGFjaCgpO1xuICAgIHRoaXMuZWwgPSByLm5hdGl2ZUVsZW1lbnQ7XG4gIH1cbn1cblxuXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgRGlnaUxheW91dE1lZGlhT2JqZWN0IGV4dGVuZHMgQ29tcG9uZW50cy5EaWdpTGF5b3V0TWVkaWFPYmplY3Qge31cblxuQFByb3h5Q21wKHtcbiAgZGVmaW5lQ3VzdG9tRWxlbWVudEZuOiBkZWZpbmVEaWdpTGF5b3V0TWVkaWFPYmplY3QsXG4gIGlucHV0czogWydhZkFsaWdubWVudCddXG59KVxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGlnaS1sYXlvdXQtbWVkaWEtb2JqZWN0JyxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXG4gIHRlbXBsYXRlOiAnPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PicsXG4gIGlucHV0czogWydhZkFsaWdubWVudCddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lMYXlvdXRNZWRpYU9iamVjdCB7XG4gIHByb3RlY3RlZCBlbDogSFRNTEVsZW1lbnQ7XG4gIGNvbnN0cnVjdG9yKGM6IENoYW5nZURldGVjdG9yUmVmLCByOiBFbGVtZW50UmVmLCBwcm90ZWN0ZWQgejogTmdab25lKSB7XG4gICAgYy5kZXRhY2goKTtcbiAgICB0aGlzLmVsID0gci5uYXRpdmVFbGVtZW50O1xuICB9XG59XG5cblxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIERpZ2lMYXlvdXRSb3dzIGV4dGVuZHMgQ29tcG9uZW50cy5EaWdpTGF5b3V0Um93cyB7fVxuXG5AUHJveHlDbXAoe1xuICBkZWZpbmVDdXN0b21FbGVtZW50Rm46IGRlZmluZURpZ2lMYXlvdXRSb3dzXG59KVxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGlnaS1sYXlvdXQtcm93cycsXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxuICB0ZW1wbGF0ZTogJzxuZy1jb250ZW50PjwvbmctY29udGVudD4nXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lMYXlvdXRSb3dzIHtcbiAgcHJvdGVjdGVkIGVsOiBIVE1MRWxlbWVudDtcbiAgY29uc3RydWN0b3IoYzogQ2hhbmdlRGV0ZWN0b3JSZWYsIHI6IEVsZW1lbnRSZWYsIHByb3RlY3RlZCB6OiBOZ1pvbmUpIHtcbiAgICBjLmRldGFjaCgpO1xuICAgIHRoaXMuZWwgPSByLm5hdGl2ZUVsZW1lbnQ7XG4gIH1cbn1cblxuXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgRGlnaUxheW91dFN0YWNrZWRCbG9ja3MgZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lMYXlvdXRTdGFja2VkQmxvY2tzIHt9XG5cbkBQcm94eUNtcCh7XG4gIGRlZmluZUN1c3RvbUVsZW1lbnRGbjogZGVmaW5lRGlnaUxheW91dFN0YWNrZWRCbG9ja3MsXG4gIGlucHV0czogWydhZlZhcmlhdGlvbiddXG59KVxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGlnaS1sYXlvdXQtc3RhY2tlZC1ibG9ja3MnLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaCxcbiAgdGVtcGxhdGU6ICc8bmctY29udGVudD48L25nLWNvbnRlbnQ+JyxcbiAgaW5wdXRzOiBbJ2FmVmFyaWF0aW9uJ11cbn0pXG5leHBvcnQgY2xhc3MgRGlnaUxheW91dFN0YWNrZWRCbG9ja3Mge1xuICBwcm90ZWN0ZWQgZWw6IEhUTUxFbGVtZW50O1xuICBjb25zdHJ1Y3RvcihjOiBDaGFuZ2VEZXRlY3RvclJlZiwgcjogRWxlbWVudFJlZiwgcHJvdGVjdGVkIHo6IE5nWm9uZSkge1xuICAgIGMuZGV0YWNoKCk7XG4gICAgdGhpcy5lbCA9IHIubmF0aXZlRWxlbWVudDtcbiAgfVxufVxuXG5cbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBEaWdpTGluayBleHRlbmRzIENvbXBvbmVudHMuRGlnaUxpbmsge1xuICAvKipcbiAgICogTMOkbmtlbGVtZW50ZXRzICdvbmNsaWNrJy1ldmVudC4gQGVuIFRoZSBsaW5rIGVsZW1lbnQncyAnb25jbGljaycgZXZlbnQuXG4gICAqL1xuICBhZk9uQ2xpY2s6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxNb3VzZUV2ZW50Pj47XG5cbn1cblxuQFByb3h5Q21wKHtcbiAgZGVmaW5lQ3VzdG9tRWxlbWVudEZuOiBkZWZpbmVEaWdpTGluayxcbiAgaW5wdXRzOiBbJ2FmSHJlZicsICdhZk92ZXJyaWRlTGluaycsICdhZlRhcmdldCcsICdhZlZhcmlhdGlvbiddLFxuICBtZXRob2RzOiBbJ2FmTUdldExpbmtFbGVtZW50J11cbn0pXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdkaWdpLWxpbmsnLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaCxcbiAgdGVtcGxhdGU6ICc8bmctY29udGVudD48L25nLWNvbnRlbnQ+JyxcbiAgaW5wdXRzOiBbJ2FmSHJlZicsICdhZk92ZXJyaWRlTGluaycsICdhZlRhcmdldCcsICdhZlZhcmlhdGlvbiddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lMaW5rIHtcbiAgcHJvdGVjdGVkIGVsOiBIVE1MRWxlbWVudDtcbiAgY29uc3RydWN0b3IoYzogQ2hhbmdlRGV0ZWN0b3JSZWYsIHI6IEVsZW1lbnRSZWYsIHByb3RlY3RlZCB6OiBOZ1pvbmUpIHtcbiAgICBjLmRldGFjaCgpO1xuICAgIHRoaXMuZWwgPSByLm5hdGl2ZUVsZW1lbnQ7XG4gICAgcHJveHlPdXRwdXRzKHRoaXMsIHRoaXMuZWwsIFsnYWZPbkNsaWNrJ10pO1xuICB9XG59XG5cblxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIERpZ2lMaW5rRXh0ZXJuYWwgZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lMaW5rRXh0ZXJuYWwge1xuICAvKipcbiAgICogTMOkbmtlbGVtZW50ZXRzICdvbmNsaWNrJy1ldmVudC4gQGVuIFRoZSBsaW5rIGVsZW1lbnQncyAnb25jbGljaycgZXZlbnQuXG4gICAqL1xuICBhZk9uQ2xpY2s6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxNb3VzZUV2ZW50Pj47XG5cbn1cblxuQFByb3h5Q21wKHtcbiAgZGVmaW5lQ3VzdG9tRWxlbWVudEZuOiBkZWZpbmVEaWdpTGlua0V4dGVybmFsLFxuICBpbnB1dHM6IFsnYWZIcmVmJywgJ2FmT3ZlcnJpZGVMaW5rJywgJ2FmVGFyZ2V0JywgJ2FmVmFyaWF0aW9uJ11cbn0pXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdkaWdpLWxpbmstZXh0ZXJuYWwnLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaCxcbiAgdGVtcGxhdGU6ICc8bmctY29udGVudD48L25nLWNvbnRlbnQ+JyxcbiAgaW5wdXRzOiBbJ2FmSHJlZicsICdhZk92ZXJyaWRlTGluaycsICdhZlRhcmdldCcsICdhZlZhcmlhdGlvbiddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lMaW5rRXh0ZXJuYWwge1xuICBwcm90ZWN0ZWQgZWw6IEhUTUxFbGVtZW50O1xuICBjb25zdHJ1Y3RvcihjOiBDaGFuZ2VEZXRlY3RvclJlZiwgcjogRWxlbWVudFJlZiwgcHJvdGVjdGVkIHo6IE5nWm9uZSkge1xuICAgIGMuZGV0YWNoKCk7XG4gICAgdGhpcy5lbCA9IHIubmF0aXZlRWxlbWVudDtcbiAgICBwcm94eU91dHB1dHModGhpcywgdGhpcy5lbCwgWydhZk9uQ2xpY2snXSk7XG4gIH1cbn1cblxuXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgRGlnaUxpbmtJY29uIGV4dGVuZHMgQ29tcG9uZW50cy5EaWdpTGlua0ljb24ge31cblxuQFByb3h5Q21wKHtcbiAgZGVmaW5lQ3VzdG9tRWxlbWVudEZuOiBkZWZpbmVEaWdpTGlua0ljb25cbn0pXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdkaWdpLWxpbmstaWNvbicsXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxuICB0ZW1wbGF0ZTogJzxuZy1jb250ZW50PjwvbmctY29udGVudD4nXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lMaW5rSWNvbiB7XG4gIHByb3RlY3RlZCBlbDogSFRNTEVsZW1lbnQ7XG4gIGNvbnN0cnVjdG9yKGM6IENoYW5nZURldGVjdG9yUmVmLCByOiBFbGVtZW50UmVmLCBwcm90ZWN0ZWQgejogTmdab25lKSB7XG4gICAgYy5kZXRhY2goKTtcbiAgICB0aGlzLmVsID0gci5uYXRpdmVFbGVtZW50O1xuICB9XG59XG5cblxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIERpZ2lMaW5rSW50ZXJuYWwgZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lMaW5rSW50ZXJuYWwge1xuICAvKipcbiAgICogTMOkbmtlbGVtZW50ZXRzICdvbmNsaWNrJy1ldmVudC4gQGVuIFRoZSBsaW5rIGVsZW1lbnQncyAnb25jbGljaycgZXZlbnQuXG4gICAqL1xuICBhZk9uQ2xpY2s6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxNb3VzZUV2ZW50Pj47XG5cbn1cblxuQFByb3h5Q21wKHtcbiAgZGVmaW5lQ3VzdG9tRWxlbWVudEZuOiBkZWZpbmVEaWdpTGlua0ludGVybmFsLFxuICBpbnB1dHM6IFsnYWZIcmVmJywgJ2FmT3ZlcnJpZGVMaW5rJywgJ2FmVmFyaWF0aW9uJ11cbn0pXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdkaWdpLWxpbmstaW50ZXJuYWwnLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaCxcbiAgdGVtcGxhdGU6ICc8bmctY29udGVudD48L25nLWNvbnRlbnQ+JyxcbiAgaW5wdXRzOiBbJ2FmSHJlZicsICdhZk92ZXJyaWRlTGluaycsICdhZlZhcmlhdGlvbiddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lMaW5rSW50ZXJuYWwge1xuICBwcm90ZWN0ZWQgZWw6IEhUTUxFbGVtZW50O1xuICBjb25zdHJ1Y3RvcihjOiBDaGFuZ2VEZXRlY3RvclJlZiwgcjogRWxlbWVudFJlZiwgcHJvdGVjdGVkIHo6IE5nWm9uZSkge1xuICAgIGMuZGV0YWNoKCk7XG4gICAgdGhpcy5lbCA9IHIubmF0aXZlRWxlbWVudDtcbiAgICBwcm94eU91dHB1dHModGhpcywgdGhpcy5lbCwgWydhZk9uQ2xpY2snXSk7XG4gIH1cbn1cblxuXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgRGlnaUxpc3RMaW5rIGV4dGVuZHMgQ29tcG9uZW50cy5EaWdpTGlzdExpbmsge31cblxuQFByb3h5Q21wKHtcbiAgZGVmaW5lQ3VzdG9tRWxlbWVudEZuOiBkZWZpbmVEaWdpTGlzdExpbmssXG4gIGlucHV0czogWydhZkxheW91dCcsICdhZlR5cGUnLCAnYWZWYXJpYXRpb24nXVxufSlcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2RpZ2ktbGlzdC1saW5rJyxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXG4gIHRlbXBsYXRlOiAnPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PicsXG4gIGlucHV0czogWydhZkxheW91dCcsICdhZlR5cGUnLCAnYWZWYXJpYXRpb24nXVxufSlcbmV4cG9ydCBjbGFzcyBEaWdpTGlzdExpbmsge1xuICBwcm90ZWN0ZWQgZWw6IEhUTUxFbGVtZW50O1xuICBjb25zdHJ1Y3RvcihjOiBDaGFuZ2VEZXRlY3RvclJlZiwgcjogRWxlbWVudFJlZiwgcHJvdGVjdGVkIHo6IE5nWm9uZSkge1xuICAgIGMuZGV0YWNoKCk7XG4gICAgdGhpcy5lbCA9IHIubmF0aXZlRWxlbWVudDtcbiAgfVxufVxuXG5cbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBEaWdpTG9hZGVyU3Bpbm5lciBleHRlbmRzIENvbXBvbmVudHMuRGlnaUxvYWRlclNwaW5uZXIge31cblxuQFByb3h5Q21wKHtcbiAgZGVmaW5lQ3VzdG9tRWxlbWVudEZuOiBkZWZpbmVEaWdpTG9hZGVyU3Bpbm5lcixcbiAgaW5wdXRzOiBbJ2FmU2l6ZScsICdhZlRleHQnXVxufSlcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2RpZ2ktbG9hZGVyLXNwaW5uZXInLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaCxcbiAgdGVtcGxhdGU6ICc8bmctY29udGVudD48L25nLWNvbnRlbnQ+JyxcbiAgaW5wdXRzOiBbJ2FmU2l6ZScsICdhZlRleHQnXVxufSlcbmV4cG9ydCBjbGFzcyBEaWdpTG9hZGVyU3Bpbm5lciB7XG4gIHByb3RlY3RlZCBlbDogSFRNTEVsZW1lbnQ7XG4gIGNvbnN0cnVjdG9yKGM6IENoYW5nZURldGVjdG9yUmVmLCByOiBFbGVtZW50UmVmLCBwcm90ZWN0ZWQgejogTmdab25lKSB7XG4gICAgYy5kZXRhY2goKTtcbiAgICB0aGlzLmVsID0gci5uYXRpdmVFbGVtZW50O1xuICB9XG59XG5cblxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIERpZ2lMb2dvIGV4dGVuZHMgQ29tcG9uZW50cy5EaWdpTG9nbyB7fVxuXG5AUHJveHlDbXAoe1xuICBkZWZpbmVDdXN0b21FbGVtZW50Rm46IGRlZmluZURpZ2lMb2dvLFxuICBpbnB1dHM6IFsnYWZEZXNjJywgJ2FmU3ZnQXJpYUhpZGRlbicsICdhZlRpdGxlJywgJ2FmVGl0bGVJZCddXG59KVxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGlnaS1sb2dvJyxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXG4gIHRlbXBsYXRlOiAnPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PicsXG4gIGlucHV0czogWydhZkRlc2MnLCAnYWZTdmdBcmlhSGlkZGVuJywgJ2FmVGl0bGUnLCAnYWZUaXRsZUlkJ11cbn0pXG5leHBvcnQgY2xhc3MgRGlnaUxvZ28ge1xuICBwcm90ZWN0ZWQgZWw6IEhUTUxFbGVtZW50O1xuICBjb25zdHJ1Y3RvcihjOiBDaGFuZ2VEZXRlY3RvclJlZiwgcjogRWxlbWVudFJlZiwgcHJvdGVjdGVkIHo6IE5nWm9uZSkge1xuICAgIGMuZGV0YWNoKCk7XG4gICAgdGhpcy5lbCA9IHIubmF0aXZlRWxlbWVudDtcbiAgfVxufVxuXG5cbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBEaWdpTG9nb1NlcnZpY2UgZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lMb2dvU2VydmljZSB7fVxuXG5AUHJveHlDbXAoe1xuICBkZWZpbmVDdXN0b21FbGVtZW50Rm46IGRlZmluZURpZ2lMb2dvU2VydmljZSxcbiAgaW5wdXRzOiBbJ2FmRGVzY3JpcHRpb24nLCAnYWZOYW1lJywgJ2FmTmFtZUlkJ11cbn0pXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdkaWdpLWxvZ28tc2VydmljZScsXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxuICB0ZW1wbGF0ZTogJzxuZy1jb250ZW50PjwvbmctY29udGVudD4nLFxuICBpbnB1dHM6IFsnYWZEZXNjcmlwdGlvbicsICdhZk5hbWUnLCAnYWZOYW1lSWQnXVxufSlcbmV4cG9ydCBjbGFzcyBEaWdpTG9nb1NlcnZpY2Uge1xuICBwcm90ZWN0ZWQgZWw6IEhUTUxFbGVtZW50O1xuICBjb25zdHJ1Y3RvcihjOiBDaGFuZ2VEZXRlY3RvclJlZiwgcjogRWxlbWVudFJlZiwgcHJvdGVjdGVkIHo6IE5nWm9uZSkge1xuICAgIGMuZGV0YWNoKCk7XG4gICAgdGhpcy5lbCA9IHIubmF0aXZlRWxlbWVudDtcbiAgfVxufVxuXG5cbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBEaWdpTG9nb1Npc3RlciBleHRlbmRzIENvbXBvbmVudHMuRGlnaUxvZ29TaXN0ZXIge31cblxuQFByb3h5Q21wKHtcbiAgZGVmaW5lQ3VzdG9tRWxlbWVudEZuOiBkZWZpbmVEaWdpTG9nb1Npc3RlcixcbiAgaW5wdXRzOiBbJ2FmTmFtZScsICdhZk5hbWVJZCddXG59KVxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGlnaS1sb2dvLXNpc3RlcicsXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxuICB0ZW1wbGF0ZTogJzxuZy1jb250ZW50PjwvbmctY29udGVudD4nLFxuICBpbnB1dHM6IFsnYWZOYW1lJywgJ2FmTmFtZUlkJ11cbn0pXG5leHBvcnQgY2xhc3MgRGlnaUxvZ29TaXN0ZXIge1xuICBwcm90ZWN0ZWQgZWw6IEhUTUxFbGVtZW50O1xuICBjb25zdHJ1Y3RvcihjOiBDaGFuZ2VEZXRlY3RvclJlZiwgcjogRWxlbWVudFJlZiwgcHJvdGVjdGVkIHo6IE5nWm9uZSkge1xuICAgIGMuZGV0YWNoKCk7XG4gICAgdGhpcy5lbCA9IHIubmF0aXZlRWxlbWVudDtcbiAgfVxufVxuXG5cbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBEaWdpTWVkaWFGaWd1cmUgZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lNZWRpYUZpZ3VyZSB7fVxuXG5AUHJveHlDbXAoe1xuICBkZWZpbmVDdXN0b21FbGVtZW50Rm46IGRlZmluZURpZ2lNZWRpYUZpZ3VyZSxcbiAgaW5wdXRzOiBbJ2FmQWxpZ25tZW50JywgJ2FmRmlnY2FwdGlvbiddXG59KVxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGlnaS1tZWRpYS1maWd1cmUnLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaCxcbiAgdGVtcGxhdGU6ICc8bmctY29udGVudD48L25nLWNvbnRlbnQ+JyxcbiAgaW5wdXRzOiBbJ2FmQWxpZ25tZW50JywgJ2FmRmlnY2FwdGlvbiddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lNZWRpYUZpZ3VyZSB7XG4gIHByb3RlY3RlZCBlbDogSFRNTEVsZW1lbnQ7XG4gIGNvbnN0cnVjdG9yKGM6IENoYW5nZURldGVjdG9yUmVmLCByOiBFbGVtZW50UmVmLCBwcm90ZWN0ZWQgejogTmdab25lKSB7XG4gICAgYy5kZXRhY2goKTtcbiAgICB0aGlzLmVsID0gci5uYXRpdmVFbGVtZW50O1xuICB9XG59XG5cblxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIERpZ2lNZWRpYUltYWdlIGV4dGVuZHMgQ29tcG9uZW50cy5EaWdpTWVkaWFJbWFnZSB7XG4gIC8qKlxuICAgKiBCaWxkbGVtZW50ZXRzICdvbmxvYWQnLWV2ZW50LiBAZW4gVGhlIGltYWdlIGVsZW1lbnQncyAnb25sb2FkJyBldmVudC5cbiAgICovXG4gIGFmT25Mb2FkOiBFdmVudEVtaXR0ZXI8Q3VzdG9tRXZlbnQ8YW55Pj47XG5cbn1cblxuQFByb3h5Q21wKHtcbiAgZGVmaW5lQ3VzdG9tRWxlbWVudEZuOiBkZWZpbmVEaWdpTWVkaWFJbWFnZSxcbiAgaW5wdXRzOiBbJ2FmQWx0JywgJ2FmQXJpYUxhYmVsJywgJ2FmRnVsbHdpZHRoJywgJ2FmSGVpZ2h0JywgJ2FmT2JzZXJ2ZXJPcHRpb25zJywgJ2FmU3JjJywgJ2FmU3Jjc2V0JywgJ2FmVGl0bGUnLCAnYWZVbmxhenknLCAnYWZXaWR0aCddXG59KVxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGlnaS1tZWRpYS1pbWFnZScsXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxuICB0ZW1wbGF0ZTogJzxuZy1jb250ZW50PjwvbmctY29udGVudD4nLFxuICBpbnB1dHM6IFsnYWZBbHQnLCAnYWZBcmlhTGFiZWwnLCAnYWZGdWxsd2lkdGgnLCAnYWZIZWlnaHQnLCAnYWZPYnNlcnZlck9wdGlvbnMnLCAnYWZTcmMnLCAnYWZTcmNzZXQnLCAnYWZUaXRsZScsICdhZlVubGF6eScsICdhZldpZHRoJ11cbn0pXG5leHBvcnQgY2xhc3MgRGlnaU1lZGlhSW1hZ2Uge1xuICBwcm90ZWN0ZWQgZWw6IEhUTUxFbGVtZW50O1xuICBjb25zdHJ1Y3RvcihjOiBDaGFuZ2VEZXRlY3RvclJlZiwgcjogRWxlbWVudFJlZiwgcHJvdGVjdGVkIHo6IE5nWm9uZSkge1xuICAgIGMuZGV0YWNoKCk7XG4gICAgdGhpcy5lbCA9IHIubmF0aXZlRWxlbWVudDtcbiAgICBwcm94eU91dHB1dHModGhpcywgdGhpcy5lbCwgWydhZk9uTG9hZCddKTtcbiAgfVxufVxuXG5cbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBEaWdpTmF2aWdhdGlvbkJyZWFkY3J1bWJzIGV4dGVuZHMgQ29tcG9uZW50cy5EaWdpTmF2aWdhdGlvbkJyZWFkY3J1bWJzIHt9XG5cbkBQcm94eUNtcCh7XG4gIGRlZmluZUN1c3RvbUVsZW1lbnRGbjogZGVmaW5lRGlnaU5hdmlnYXRpb25CcmVhZGNydW1icyxcbiAgaW5wdXRzOiBbJ2FmQXJpYUxhYmVsJywgJ2FmQ3VycmVudFBhZ2UnLCAnYWZWYXJpYXRpb24nXVxufSlcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2RpZ2ktbmF2aWdhdGlvbi1icmVhZGNydW1icycsXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxuICB0ZW1wbGF0ZTogJzxuZy1jb250ZW50PjwvbmctY29udGVudD4nLFxuICBpbnB1dHM6IFsnYWZBcmlhTGFiZWwnLCAnYWZDdXJyZW50UGFnZScsICdhZlZhcmlhdGlvbiddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lOYXZpZ2F0aW9uQnJlYWRjcnVtYnMge1xuICBwcm90ZWN0ZWQgZWw6IEhUTUxFbGVtZW50O1xuICBjb25zdHJ1Y3RvcihjOiBDaGFuZ2VEZXRlY3RvclJlZiwgcjogRWxlbWVudFJlZiwgcHJvdGVjdGVkIHo6IE5nWm9uZSkge1xuICAgIGMuZGV0YWNoKCk7XG4gICAgdGhpcy5lbCA9IHIubmF0aXZlRWxlbWVudDtcbiAgfVxufVxuXG5cbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBEaWdpTmF2aWdhdGlvbkNvbnRleHRNZW51IGV4dGVuZHMgQ29tcG9uZW50cy5EaWdpTmF2aWdhdGlvbkNvbnRleHRNZW51IHtcbiAgLyoqXG4gICAqIE7DpHIga29tcG9uZW50ZW4gc3TDpG5ncyBAZW4gV2hlbiBjb21wb25lbnQgZ2V0cyBpbmFjdGl2ZVxuICAgKi9cbiAgYWZPbkluYWN0aXZlOiBFdmVudEVtaXR0ZXI8Q3VzdG9tRXZlbnQ8YW55Pj47XG4gIC8qKlxuICAgKiBOw6RyIGtvbXBvbmVudGVuIMO2cHBuYXMgQGVuIFdoZW4gY29tcG9uZW50IGdldHMgYWN0aXZlXG4gICAqL1xuICBhZk9uQWN0aXZlOiBFdmVudEVtaXR0ZXI8Q3VzdG9tRXZlbnQ8YW55Pj47XG4gIC8qKlxuICAgKiBOw6RyIGZva3VzIHPDpHR0cyB1dGFuZsO2ciBrb21wb25lbnRlbiBAZW4gV2hlbiBmb2N1cyBpcyBtb3ZlIG91dHNpZGUgb2YgY29tcG9uZW50XG4gICAqL1xuICBhZk9uQmx1cjogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PGFueT4+O1xuICAvKipcbiAgICogVmlkIG5hdmlnZXJpbmcgdGlsbCBueXR0IGxpc3RvYmpla3QgQGVuIFdoZW4gbmF2aWdhdGluZyB0byBhIG5ldyBsaXN0IGl0ZW1cbiAgICovXG4gIGFmT25DaGFuZ2U6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxhbnk+PjtcbiAgLyoqXG4gICAqIFRvZ2dsZWtuYXBwZW5zICdvbmNsaWNrJy1ldmVudCBAZW4gVGhlIHRvZ2dsZSBidXR0b24ncyAnb25jbGljayctZXZlbnRcbiAgICovXG4gIGFmT25Ub2dnbGU6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxhbnk+PjtcbiAgLyoqXG4gICAqICdvbmNsaWNrJy1ldmVudCBww6Uga25hcHBlbGVtZW50ZW4gaSBsaXN0YW4gQGVuIExpc3QgaXRlbSBidXR0b25zJyAnb25jbGljayctZXZlbnRcbiAgICovXG4gIGFmT25TZWxlY3Q6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxhbnk+PjtcblxufVxuXG5AUHJveHlDbXAoe1xuICBkZWZpbmVDdXN0b21FbGVtZW50Rm46IGRlZmluZURpZ2lOYXZpZ2F0aW9uQ29udGV4dE1lbnUsXG4gIGlucHV0czogWydhZkljb24nLCAnYWZJZCcsICdhZk5hdmlnYXRpb25Db250ZXh0TWVudUl0ZW1zJywgJ2FmU3RhcnRTZWxlY3RlZCcsICdhZlRleHQnXVxufSlcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2RpZ2ktbmF2aWdhdGlvbi1jb250ZXh0LW1lbnUnLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaCxcbiAgdGVtcGxhdGU6ICc8bmctY29udGVudD48L25nLWNvbnRlbnQ+JyxcbiAgaW5wdXRzOiBbJ2FmSWNvbicsICdhZklkJywgJ2FmTmF2aWdhdGlvbkNvbnRleHRNZW51SXRlbXMnLCAnYWZTdGFydFNlbGVjdGVkJywgJ2FmVGV4dCddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lOYXZpZ2F0aW9uQ29udGV4dE1lbnUge1xuICBwcm90ZWN0ZWQgZWw6IEhUTUxFbGVtZW50O1xuICBjb25zdHJ1Y3RvcihjOiBDaGFuZ2VEZXRlY3RvclJlZiwgcjogRWxlbWVudFJlZiwgcHJvdGVjdGVkIHo6IE5nWm9uZSkge1xuICAgIGMuZGV0YWNoKCk7XG4gICAgdGhpcy5lbCA9IHIubmF0aXZlRWxlbWVudDtcbiAgICBwcm94eU91dHB1dHModGhpcywgdGhpcy5lbCwgWydhZk9uSW5hY3RpdmUnLCAnYWZPbkFjdGl2ZScsICdhZk9uQmx1cicsICdhZk9uQ2hhbmdlJywgJ2FmT25Ub2dnbGUnLCAnYWZPblNlbGVjdCddKTtcbiAgfVxufVxuXG5cbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBEaWdpTmF2aWdhdGlvbkNvbnRleHRNZW51SXRlbSBleHRlbmRzIENvbXBvbmVudHMuRGlnaU5hdmlnYXRpb25Db250ZXh0TWVudUl0ZW0ge31cblxuQFByb3h5Q21wKHtcbiAgZGVmaW5lQ3VzdG9tRWxlbWVudEZuOiBkZWZpbmVEaWdpTmF2aWdhdGlvbkNvbnRleHRNZW51SXRlbSxcbiAgaW5wdXRzOiBbJ2FmRGlyJywgJ2FmSHJlZicsICdhZkxhbmcnLCAnYWZUZXh0JywgJ2FmVHlwZSddXG59KVxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGlnaS1uYXZpZ2F0aW9uLWNvbnRleHQtbWVudS1pdGVtJyxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXG4gIHRlbXBsYXRlOiAnPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PicsXG4gIGlucHV0czogWydhZkRpcicsICdhZkhyZWYnLCAnYWZMYW5nJywgJ2FmVGV4dCcsICdhZlR5cGUnXVxufSlcbmV4cG9ydCBjbGFzcyBEaWdpTmF2aWdhdGlvbkNvbnRleHRNZW51SXRlbSB7XG4gIHByb3RlY3RlZCBlbDogSFRNTEVsZW1lbnQ7XG4gIGNvbnN0cnVjdG9yKGM6IENoYW5nZURldGVjdG9yUmVmLCByOiBFbGVtZW50UmVmLCBwcm90ZWN0ZWQgejogTmdab25lKSB7XG4gICAgYy5kZXRhY2goKTtcbiAgICB0aGlzLmVsID0gci5uYXRpdmVFbGVtZW50O1xuICB9XG59XG5cblxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIERpZ2lOYXZpZ2F0aW9uTWFpbk1lbnUgZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lOYXZpZ2F0aW9uTWFpbk1lbnUge31cblxuQFByb3h5Q21wKHtcbiAgZGVmaW5lQ3VzdG9tRWxlbWVudEZuOiBkZWZpbmVEaWdpTmF2aWdhdGlvbk1haW5NZW51LFxuICBpbnB1dHM6IFsnYWZJZCddXG59KVxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGlnaS1uYXZpZ2F0aW9uLW1haW4tbWVudScsXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxuICB0ZW1wbGF0ZTogJzxuZy1jb250ZW50PjwvbmctY29udGVudD4nLFxuICBpbnB1dHM6IFsnYWZJZCddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lOYXZpZ2F0aW9uTWFpbk1lbnUge1xuICBwcm90ZWN0ZWQgZWw6IEhUTUxFbGVtZW50O1xuICBjb25zdHJ1Y3RvcihjOiBDaGFuZ2VEZXRlY3RvclJlZiwgcjogRWxlbWVudFJlZiwgcHJvdGVjdGVkIHo6IE5nWm9uZSkge1xuICAgIGMuZGV0YWNoKCk7XG4gICAgdGhpcy5lbCA9IHIubmF0aXZlRWxlbWVudDtcbiAgfVxufVxuXG5cbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBEaWdpTmF2aWdhdGlvbk1haW5NZW51UGFuZWwgZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lOYXZpZ2F0aW9uTWFpbk1lbnVQYW5lbCB7XG4gIC8qKlxuICAgKiBOw6RyIGtvbXBvbmVudGVuIMOkbmRyYXIgc3RvcmxlayBAZW4gV2hlbiB0aGUgY29tcG9uZW50IGNoYW5nZXMgc2l6ZVxuICAgKi9cbiAgYWZPblJlc2l6ZTogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PFJlc2l6ZU9ic2VydmVyRW50cnk+PjtcbiAgLyoqXG4gICAqIE7DpHIga29tcG9uZW50ZW4gc3TDpG5ncyBAZW4gV2hlbiB0aGUgY29tcG9uZW50IGNoYW5nZXMgc2l6ZVxuICAgKi9cbiAgYWZPbkNsb3NlOiBFdmVudEVtaXR0ZXI8Q3VzdG9tRXZlbnQ8YW55Pj47XG5cbn1cblxuQFByb3h5Q21wKHtcbiAgZGVmaW5lQ3VzdG9tRWxlbWVudEZuOiBkZWZpbmVEaWdpTmF2aWdhdGlvbk1haW5NZW51UGFuZWxcbn0pXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdkaWdpLW5hdmlnYXRpb24tbWFpbi1tZW51LXBhbmVsJyxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXG4gIHRlbXBsYXRlOiAnPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50Pidcbn0pXG5leHBvcnQgY2xhc3MgRGlnaU5hdmlnYXRpb25NYWluTWVudVBhbmVsIHtcbiAgcHJvdGVjdGVkIGVsOiBIVE1MRWxlbWVudDtcbiAgY29uc3RydWN0b3IoYzogQ2hhbmdlRGV0ZWN0b3JSZWYsIHI6IEVsZW1lbnRSZWYsIHByb3RlY3RlZCB6OiBOZ1pvbmUpIHtcbiAgICBjLmRldGFjaCgpO1xuICAgIHRoaXMuZWwgPSByLm5hdGl2ZUVsZW1lbnQ7XG4gICAgcHJveHlPdXRwdXRzKHRoaXMsIHRoaXMuZWwsIFsnYWZPblJlc2l6ZScsICdhZk9uQ2xvc2UnXSk7XG4gIH1cbn1cblxuXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgRGlnaU5hdmlnYXRpb25QYWdpbmF0aW9uIGV4dGVuZHMgQ29tcG9uZW50cy5EaWdpTmF2aWdhdGlvblBhZ2luYXRpb24ge1xuICAvKipcbiAgICogVmlkIGJ5dGUgYXYgc2lkYSBAZW4gV2hlbiBwYWdlIGNoYW5nZXNcbiAgICovXG4gIGFmT25QYWdlQ2hhbmdlOiBFdmVudEVtaXR0ZXI8Q3VzdG9tRXZlbnQ8bnVtYmVyPj47XG5cbn1cblxuQFByb3h5Q21wKHtcbiAgZGVmaW5lQ3VzdG9tRWxlbWVudEZuOiBkZWZpbmVEaWdpTmF2aWdhdGlvblBhZ2luYXRpb24sXG4gIGlucHV0czogWydhZkN1cnJlbnRSZXN1bHRFbmQnLCAnYWZDdXJyZW50UmVzdWx0U3RhcnQnLCAnYWZJZCcsICdhZkluaXRBY3RpdmVQYWdlJywgJ2FmUmVzdWx0TmFtZScsICdhZlRvdGFsUGFnZXMnLCAnYWZUb3RhbFJlc3VsdHMnXSxcbiAgbWV0aG9kczogWydhZk1TZXRDdXJyZW50UGFnZSddXG59KVxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGlnaS1uYXZpZ2F0aW9uLXBhZ2luYXRpb24nLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaCxcbiAgdGVtcGxhdGU6ICc8bmctY29udGVudD48L25nLWNvbnRlbnQ+JyxcbiAgaW5wdXRzOiBbJ2FmQ3VycmVudFJlc3VsdEVuZCcsICdhZkN1cnJlbnRSZXN1bHRTdGFydCcsICdhZklkJywgJ2FmSW5pdEFjdGl2ZVBhZ2UnLCAnYWZSZXN1bHROYW1lJywgJ2FmVG90YWxQYWdlcycsICdhZlRvdGFsUmVzdWx0cyddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lOYXZpZ2F0aW9uUGFnaW5hdGlvbiB7XG4gIHByb3RlY3RlZCBlbDogSFRNTEVsZW1lbnQ7XG4gIGNvbnN0cnVjdG9yKGM6IENoYW5nZURldGVjdG9yUmVmLCByOiBFbGVtZW50UmVmLCBwcm90ZWN0ZWQgejogTmdab25lKSB7XG4gICAgYy5kZXRhY2goKTtcbiAgICB0aGlzLmVsID0gci5uYXRpdmVFbGVtZW50O1xuICAgIHByb3h5T3V0cHV0cyh0aGlzLCB0aGlzLmVsLCBbJ2FmT25QYWdlQ2hhbmdlJ10pO1xuICB9XG59XG5cblxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIERpZ2lOYXZpZ2F0aW9uU2lkZWJhciBleHRlbmRzIENvbXBvbmVudHMuRGlnaU5hdmlnYXRpb25TaWRlYmFyIHtcbiAgLyoqXG4gICAqIFN0w6RuZ2tuYXBwZW5zICdvbmNsaWNrJy1ldmVudCBAZW4gQ2xvc2UgYnV0dG9uJ3MgJ29uY2xpY2snIGV2ZW50XG4gICAqL1xuICBhZk9uQ2xvc2U6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxhbnk+PjtcbiAgLyoqXG4gICAqIFN0w6RuZ25pbmcgYXYgc2lkb2bDpGx0IG1lZCBlc2MtdGFuZ2VudGVuIEBlbiBBdCBjbG9zZS9vcGVuIG9mIHNpZGViYXIgdXNpbmcgZXNjLWtleVxuICAgKi9cbiAgYWZPbkVzYzogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PGFueT4+O1xuICAvKipcbiAgICogU3TDpG5nbmluZyBhdiBzaWRvZsOkbHQgbWVkIGtsaWNrIHDDpSBza3VnZ2FuIGJha29tIG1lbnluIEBlbiBBdCBjbG9zZSBvZiBzaWRlYmFyIHdoZW4gY2xpY2tpbmcgb24gYmFja2Ryb3BcbiAgICovXG4gIGFmT25CYWNrZHJvcENsaWNrOiBFdmVudEVtaXR0ZXI8Q3VzdG9tRXZlbnQ8YW55Pj47XG4gIC8qKlxuICAgKiBWaWQgw7ZwcG5pbmcvc3TDpG5nbmluZyBhdiBzaWRvZsOkbHQgQGVuIEF0IGNsb3NlL29wZW4gb2Ygc2lkZWJhclxuICAgKi9cbiAgYWZPblRvZ2dsZTogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PGFueT4+O1xuXG59XG5cbkBQcm94eUNtcCh7XG4gIGRlZmluZUN1c3RvbUVsZW1lbnRGbjogZGVmaW5lRGlnaU5hdmlnYXRpb25TaWRlYmFyLFxuICBpbnB1dHM6IFsnYWZBY3RpdmUnLCAnYWZCYWNrZHJvcCcsICdhZkNsb3NlQnV0dG9uQXJpYUxhYmVsJywgJ2FmQ2xvc2VCdXR0b25Qb3NpdGlvbicsICdhZkNsb3NlQnV0dG9uVGV4dCcsICdhZkNsb3NlRm9jdXNhYmxlRWxlbWVudCcsICdhZkZvY3VzYWJsZUVsZW1lbnQnLCAnYWZIZWFkaW5nJywgJ2FmSGVhZGluZ0xldmVsJywgJ2FmSGlkZUhlYWRlcicsICdhZklkJywgJ2FmTW9iaWxlUG9zaXRpb24nLCAnYWZNb2JpbGVWYXJpYXRpb24nLCAnYWZQb3NpdGlvbicsICdhZlN0aWNreUhlYWRlcicsICdhZlZhcmlhdGlvbiddXG59KVxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGlnaS1uYXZpZ2F0aW9uLXNpZGViYXInLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaCxcbiAgdGVtcGxhdGU6ICc8bmctY29udGVudD48L25nLWNvbnRlbnQ+JyxcbiAgaW5wdXRzOiBbJ2FmQWN0aXZlJywgJ2FmQmFja2Ryb3AnLCAnYWZDbG9zZUJ1dHRvbkFyaWFMYWJlbCcsICdhZkNsb3NlQnV0dG9uUG9zaXRpb24nLCAnYWZDbG9zZUJ1dHRvblRleHQnLCAnYWZDbG9zZUZvY3VzYWJsZUVsZW1lbnQnLCAnYWZGb2N1c2FibGVFbGVtZW50JywgJ2FmSGVhZGluZycsICdhZkhlYWRpbmdMZXZlbCcsICdhZkhpZGVIZWFkZXInLCAnYWZJZCcsICdhZk1vYmlsZVBvc2l0aW9uJywgJ2FmTW9iaWxlVmFyaWF0aW9uJywgJ2FmUG9zaXRpb24nLCAnYWZTdGlja3lIZWFkZXInLCAnYWZWYXJpYXRpb24nXVxufSlcbmV4cG9ydCBjbGFzcyBEaWdpTmF2aWdhdGlvblNpZGViYXIge1xuICBwcm90ZWN0ZWQgZWw6IEhUTUxFbGVtZW50O1xuICBjb25zdHJ1Y3RvcihjOiBDaGFuZ2VEZXRlY3RvclJlZiwgcjogRWxlbWVudFJlZiwgcHJvdGVjdGVkIHo6IE5nWm9uZSkge1xuICAgIGMuZGV0YWNoKCk7XG4gICAgdGhpcy5lbCA9IHIubmF0aXZlRWxlbWVudDtcbiAgICBwcm94eU91dHB1dHModGhpcywgdGhpcy5lbCwgWydhZk9uQ2xvc2UnLCAnYWZPbkVzYycsICdhZk9uQmFja2Ryb3BDbGljaycsICdhZk9uVG9nZ2xlJ10pO1xuICB9XG59XG5cblxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIERpZ2lOYXZpZ2F0aW9uU2lkZWJhckJ1dHRvbiBleHRlbmRzIENvbXBvbmVudHMuRGlnaU5hdmlnYXRpb25TaWRlYmFyQnV0dG9uIHtcbiAgLyoqXG4gICAqIFRvZ2dsZWtuYXBwZW5zICdvbmNsaWNrJy1ldmVudCBAZW4gVGhlIHRvZ2dsZSBidXR0b24ncyAnb25jbGljayctZXZlbnRcbiAgICovXG4gIGFmT25Ub2dnbGU6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxhbnk+PjtcblxufVxuXG5AUHJveHlDbXAoe1xuICBkZWZpbmVDdXN0b21FbGVtZW50Rm46IGRlZmluZURpZ2lOYXZpZ2F0aW9uU2lkZWJhckJ1dHRvbixcbiAgaW5wdXRzOiBbJ2FmQXJpYUxhYmVsJywgJ2FmSWQnLCAnYWZUZXh0J11cbn0pXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdkaWdpLW5hdmlnYXRpb24tc2lkZWJhci1idXR0b24nLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaCxcbiAgdGVtcGxhdGU6ICc8bmctY29udGVudD48L25nLWNvbnRlbnQ+JyxcbiAgaW5wdXRzOiBbJ2FmQXJpYUxhYmVsJywgJ2FmSWQnLCAnYWZUZXh0J11cbn0pXG5leHBvcnQgY2xhc3MgRGlnaU5hdmlnYXRpb25TaWRlYmFyQnV0dG9uIHtcbiAgcHJvdGVjdGVkIGVsOiBIVE1MRWxlbWVudDtcbiAgY29uc3RydWN0b3IoYzogQ2hhbmdlRGV0ZWN0b3JSZWYsIHI6IEVsZW1lbnRSZWYsIHByb3RlY3RlZCB6OiBOZ1pvbmUpIHtcbiAgICBjLmRldGFjaCgpO1xuICAgIHRoaXMuZWwgPSByLm5hdGl2ZUVsZW1lbnQ7XG4gICAgcHJveHlPdXRwdXRzKHRoaXMsIHRoaXMuZWwsIFsnYWZPblRvZ2dsZSddKTtcbiAgfVxufVxuXG5cbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBEaWdpTmF2aWdhdGlvblRhYiBleHRlbmRzIENvbXBvbmVudHMuRGlnaU5hdmlnYXRpb25UYWIge1xuICAvKipcbiAgICogTsOkciB0YWJiZW4gdsOkeGxhciBtZWxsYW4gYWt0aXYgb2NoIGluYWt0aXYgQGVuIFdoZW4gdGhlIHRhYiB0b2dnbGVzIGJldHdlZW4gYWN0aXZlIGFuZCBpbmFjdGl2ZVxuICAgKi9cbiAgYWZPblRvZ2dsZTogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PGJvb2xlYW4+PjtcblxufVxuXG5AUHJveHlDbXAoe1xuICBkZWZpbmVDdXN0b21FbGVtZW50Rm46IGRlZmluZURpZ2lOYXZpZ2F0aW9uVGFiLFxuICBpbnB1dHM6IFsnYWZBY3RpdmUnLCAnYWZBcmlhTGFiZWwnLCAnYWZJZCddXG59KVxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGlnaS1uYXZpZ2F0aW9uLXRhYicsXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxuICB0ZW1wbGF0ZTogJzxuZy1jb250ZW50PjwvbmctY29udGVudD4nLFxuICBpbnB1dHM6IFsnYWZBY3RpdmUnLCAnYWZBcmlhTGFiZWwnLCAnYWZJZCddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lOYXZpZ2F0aW9uVGFiIHtcbiAgcHJvdGVjdGVkIGVsOiBIVE1MRWxlbWVudDtcbiAgY29uc3RydWN0b3IoYzogQ2hhbmdlRGV0ZWN0b3JSZWYsIHI6IEVsZW1lbnRSZWYsIHByb3RlY3RlZCB6OiBOZ1pvbmUpIHtcbiAgICBjLmRldGFjaCgpO1xuICAgIHRoaXMuZWwgPSByLm5hdGl2ZUVsZW1lbnQ7XG4gICAgcHJveHlPdXRwdXRzKHRoaXMsIHRoaXMuZWwsIFsnYWZPblRvZ2dsZSddKTtcbiAgfVxufVxuXG5cbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBEaWdpTmF2aWdhdGlvblRhYkluQUJveCBleHRlbmRzIENvbXBvbmVudHMuRGlnaU5hdmlnYXRpb25UYWJJbkFCb3gge1xuICAvKipcbiAgICogTsOkciB0YWJiZW4gdsOkeGxhciBtZWxsYW4gYWt0aXYgb2NoIGluYWt0aXYgQGVuIFdoZW4gdGhlIHRhYiB0b2dnbGVzIGJldHdlZW4gYWN0aXZlIGFuZCBpbmFjdGl2ZVxuICAgKi9cbiAgYWZPblRvZ2dsZTogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PGJvb2xlYW4+PjtcblxufVxuXG5AUHJveHlDbXAoe1xuICBkZWZpbmVDdXN0b21FbGVtZW50Rm46IGRlZmluZURpZ2lOYXZpZ2F0aW9uVGFiSW5BQm94LFxuICBpbnB1dHM6IFsnYWZBY3RpdmUnLCAnYWZBcmlhTGFiZWwnLCAnYWZJZCddXG59KVxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGlnaS1uYXZpZ2F0aW9uLXRhYi1pbi1hLWJveCcsXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxuICB0ZW1wbGF0ZTogJzxuZy1jb250ZW50PjwvbmctY29udGVudD4nLFxuICBpbnB1dHM6IFsnYWZBY3RpdmUnLCAnYWZBcmlhTGFiZWwnLCAnYWZJZCddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lOYXZpZ2F0aW9uVGFiSW5BQm94IHtcbiAgcHJvdGVjdGVkIGVsOiBIVE1MRWxlbWVudDtcbiAgY29uc3RydWN0b3IoYzogQ2hhbmdlRGV0ZWN0b3JSZWYsIHI6IEVsZW1lbnRSZWYsIHByb3RlY3RlZCB6OiBOZ1pvbmUpIHtcbiAgICBjLmRldGFjaCgpO1xuICAgIHRoaXMuZWwgPSByLm5hdGl2ZUVsZW1lbnQ7XG4gICAgcHJveHlPdXRwdXRzKHRoaXMsIHRoaXMuZWwsIFsnYWZPblRvZ2dsZSddKTtcbiAgfVxufVxuXG5cbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBEaWdpTmF2aWdhdGlvblRhYnMgZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lOYXZpZ2F0aW9uVGFicyB7XG4gIC8qKlxuICAgKiAgXG4gICAqL1xuICBhZk9uQ2hhbmdlOiBFdmVudEVtaXR0ZXI8Q3VzdG9tRXZlbnQ8YW55Pj47XG4gIC8qKlxuICAgKiAgXG4gICAqL1xuICBhZk9uQ2xpY2s6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxNb3VzZUV2ZW50Pj47XG4gIC8qKlxuICAgKiAgXG4gICAqL1xuICBhZk9uRm9jdXM6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxhbnk+PjtcblxufVxuXG5AUHJveHlDbXAoe1xuICBkZWZpbmVDdXN0b21FbGVtZW50Rm46IGRlZmluZURpZ2lOYXZpZ2F0aW9uVGFicyxcbiAgaW5wdXRzOiBbJ2FmQXJpYUxhYmVsJywgJ2FmSWQnLCAnYWZJbml0QWN0aXZlVGFiJ10sXG4gIG1ldGhvZHM6IFsnYWZNU2V0QWN0aXZlVGFiJ11cbn0pXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdkaWdpLW5hdmlnYXRpb24tdGFicycsXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxuICB0ZW1wbGF0ZTogJzxuZy1jb250ZW50PjwvbmctY29udGVudD4nLFxuICBpbnB1dHM6IFsnYWZBcmlhTGFiZWwnLCAnYWZJZCcsICdhZkluaXRBY3RpdmVUYWInXVxufSlcbmV4cG9ydCBjbGFzcyBEaWdpTmF2aWdhdGlvblRhYnMge1xuICBwcm90ZWN0ZWQgZWw6IEhUTUxFbGVtZW50O1xuICBjb25zdHJ1Y3RvcihjOiBDaGFuZ2VEZXRlY3RvclJlZiwgcjogRWxlbWVudFJlZiwgcHJvdGVjdGVkIHo6IE5nWm9uZSkge1xuICAgIGMuZGV0YWNoKCk7XG4gICAgdGhpcy5lbCA9IHIubmF0aXZlRWxlbWVudDtcbiAgICBwcm94eU91dHB1dHModGhpcywgdGhpcy5lbCwgWydhZk9uQ2hhbmdlJywgJ2FmT25DbGljaycsICdhZk9uRm9jdXMnXSk7XG4gIH1cbn1cblxuXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgRGlnaU5hdmlnYXRpb25Ub2MgZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lOYXZpZ2F0aW9uVG9jIHt9XG5cbkBQcm94eUNtcCh7XG4gIGRlZmluZUN1c3RvbUVsZW1lbnRGbjogZGVmaW5lRGlnaU5hdmlnYXRpb25Ub2Ncbn0pXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdkaWdpLW5hdmlnYXRpb24tdG9jJyxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXG4gIHRlbXBsYXRlOiAnPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50Pidcbn0pXG5leHBvcnQgY2xhc3MgRGlnaU5hdmlnYXRpb25Ub2Mge1xuICBwcm90ZWN0ZWQgZWw6IEhUTUxFbGVtZW50O1xuICBjb25zdHJ1Y3RvcihjOiBDaGFuZ2VEZXRlY3RvclJlZiwgcjogRWxlbWVudFJlZiwgcHJvdGVjdGVkIHo6IE5nWm9uZSkge1xuICAgIGMuZGV0YWNoKCk7XG4gICAgdGhpcy5lbCA9IHIubmF0aXZlRWxlbWVudDtcbiAgfVxufVxuXG5cbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBEaWdpTmF2aWdhdGlvblZlcnRpY2FsTWVudSBleHRlbmRzIENvbXBvbmVudHMuRGlnaU5hdmlnYXRpb25WZXJ0aWNhbE1lbnUge31cblxuQFByb3h5Q21wKHtcbiAgZGVmaW5lQ3VzdG9tRWxlbWVudEZuOiBkZWZpbmVEaWdpTmF2aWdhdGlvblZlcnRpY2FsTWVudSxcbiAgaW5wdXRzOiBbJ2FmQWN0aXZlSW5kaWNhdG9yU2l6ZScsICdhZkFyaWFMYWJlbCcsICdhZklkJywgJ2FmVmFyaWF0aW9uJ10sXG4gIG1ldGhvZHM6IFsnYWZNU2V0Q3VycmVudEFjdGl2ZUxldmVsJ11cbn0pXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdkaWdpLW5hdmlnYXRpb24tdmVydGljYWwtbWVudScsXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxuICB0ZW1wbGF0ZTogJzxuZy1jb250ZW50PjwvbmctY29udGVudD4nLFxuICBpbnB1dHM6IFsnYWZBY3RpdmVJbmRpY2F0b3JTaXplJywgJ2FmQXJpYUxhYmVsJywgJ2FmSWQnLCAnYWZWYXJpYXRpb24nXVxufSlcbmV4cG9ydCBjbGFzcyBEaWdpTmF2aWdhdGlvblZlcnRpY2FsTWVudSB7XG4gIHByb3RlY3RlZCBlbDogSFRNTEVsZW1lbnQ7XG4gIGNvbnN0cnVjdG9yKGM6IENoYW5nZURldGVjdG9yUmVmLCByOiBFbGVtZW50UmVmLCBwcm90ZWN0ZWQgejogTmdab25lKSB7XG4gICAgYy5kZXRhY2goKTtcbiAgICB0aGlzLmVsID0gci5uYXRpdmVFbGVtZW50O1xuICB9XG59XG5cblxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIERpZ2lOYXZpZ2F0aW9uVmVydGljYWxNZW51SXRlbSBleHRlbmRzIENvbXBvbmVudHMuRGlnaU5hdmlnYXRpb25WZXJ0aWNhbE1lbnVJdGVtIHtcbiAgLyoqXG4gICAqIEzDpG5rZW4gb2NoIHRvZ2dsZS1rbmFwcGVucyAnb25jbGljayctZXZlbnQgQGVuIExpbmsgYW5kIHRvZ2dsZSBidXR0b25zICdvbmNsaWNrJyBldmVudFxuICAgKi9cbiAgYWZPbkNsaWNrOiBFdmVudEVtaXR0ZXI8Q3VzdG9tRXZlbnQ8YW55Pj47XG5cbn1cblxuQFByb3h5Q21wKHtcbiAgZGVmaW5lQ3VzdG9tRWxlbWVudEZuOiBkZWZpbmVEaWdpTmF2aWdhdGlvblZlcnRpY2FsTWVudUl0ZW0sXG4gIGlucHV0czogWydhZkFjdGl2ZScsICdhZkFjdGl2ZVN1Ym5hdicsICdhZkhhc1N1Ym5hdicsICdhZkhyZWYnLCAnYWZJZCcsICdhZk92ZXJyaWRlTGluaycsICdhZlRleHQnXVxufSlcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2RpZ2ktbmF2aWdhdGlvbi12ZXJ0aWNhbC1tZW51LWl0ZW0nLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaCxcbiAgdGVtcGxhdGU6ICc8bmctY29udGVudD48L25nLWNvbnRlbnQ+JyxcbiAgaW5wdXRzOiBbJ2FmQWN0aXZlJywgJ2FmQWN0aXZlU3VibmF2JywgJ2FmSGFzU3VibmF2JywgJ2FmSHJlZicsICdhZklkJywgJ2FmT3ZlcnJpZGVMaW5rJywgJ2FmVGV4dCddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lOYXZpZ2F0aW9uVmVydGljYWxNZW51SXRlbSB7XG4gIHByb3RlY3RlZCBlbDogSFRNTEVsZW1lbnQ7XG4gIGNvbnN0cnVjdG9yKGM6IENoYW5nZURldGVjdG9yUmVmLCByOiBFbGVtZW50UmVmLCBwcm90ZWN0ZWQgejogTmdab25lKSB7XG4gICAgYy5kZXRhY2goKTtcbiAgICB0aGlzLmVsID0gci5uYXRpdmVFbGVtZW50O1xuICAgIHByb3h5T3V0cHV0cyh0aGlzLCB0aGlzLmVsLCBbJ2FmT25DbGljayddKTtcbiAgfVxufVxuXG5cbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBEaWdpTm90aWZpY2F0aW9uQWxlcnQgZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lOb3RpZmljYXRpb25BbGVydCB7XG4gIC8qKlxuICAgKiBTdMOkbmdrbmFwcGVucyAnb25jbGljayctZXZlbnQgQGVuIENsb3NlIGJ1dHRvbidzICdvbmNsaWNrJyBldmVudFxuICAgKi9cbiAgYWZPbkNsb3NlOiBFdmVudEVtaXR0ZXI8Q3VzdG9tRXZlbnQ8TW91c2VFdmVudD4+O1xuXG59XG5cbkBQcm94eUNtcCh7XG4gIGRlZmluZUN1c3RvbUVsZW1lbnRGbjogZGVmaW5lRGlnaU5vdGlmaWNhdGlvbkFsZXJ0LFxuICBpbnB1dHM6IFsnYWZDbG9zZWFibGUnLCAnYWZWYXJpYXRpb24nXVxufSlcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2RpZ2ktbm90aWZpY2F0aW9uLWFsZXJ0JyxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXG4gIHRlbXBsYXRlOiAnPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PicsXG4gIGlucHV0czogWydhZkNsb3NlYWJsZScsICdhZlZhcmlhdGlvbiddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lOb3RpZmljYXRpb25BbGVydCB7XG4gIHByb3RlY3RlZCBlbDogSFRNTEVsZW1lbnQ7XG4gIGNvbnN0cnVjdG9yKGM6IENoYW5nZURldGVjdG9yUmVmLCByOiBFbGVtZW50UmVmLCBwcm90ZWN0ZWQgejogTmdab25lKSB7XG4gICAgYy5kZXRhY2goKTtcbiAgICB0aGlzLmVsID0gci5uYXRpdmVFbGVtZW50O1xuICAgIHByb3h5T3V0cHV0cyh0aGlzLCB0aGlzLmVsLCBbJ2FmT25DbG9zZSddKTtcbiAgfVxufVxuXG5cbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBEaWdpTm90aWZpY2F0aW9uQ29va2llIGV4dGVuZHMgQ29tcG9uZW50cy5EaWdpTm90aWZpY2F0aW9uQ29va2llIHtcbiAgLyoqXG4gICAqIE7DpHIgYW52w6RuZGFyZW4gZ29ka8Okbm5lciBhbGxhIGtha29yIEBlbiBXaGVuIHRoZSB1c2VyIGFjY2VwdHMgYWxsIGNvb2tpZXNcbiAgICovXG4gIGFmT25BY2NlcHRBbGxDb29raWVzOiBFdmVudEVtaXR0ZXI8Q3VzdG9tRXZlbnQ8TW91c2VFdmVudD4+O1xuICAvKipcbiAgICogTsOkciBhbnbDpG5kYXJlbiBnb2Rrw6RubmVyIGFsbGEga2Frb3IgQGVuIFdoZW4gdGhlIHVzZXIgYWNjZXB0cyBhbGwgY29va2llc1xuICAgKi9cbiAgYWZPblN1Ym1pdFNldHRpbmdzOiBFdmVudEVtaXR0ZXI8Q3VzdG9tRXZlbnQ8YW55Pj47XG5cbn1cblxuQFByb3h5Q21wKHtcbiAgZGVmaW5lQ3VzdG9tRWxlbWVudEZuOiBkZWZpbmVEaWdpTm90aWZpY2F0aW9uQ29va2llLFxuICBpbnB1dHM6IFsnYWZCYW5uZXJIZWFkaW5nVGV4dCcsICdhZkJhbm5lclRleHQnLCAnYWZJZCcsICdhZk1vZGFsSGVhZGluZ1RleHQnLCAnYWZSZXF1aXJlZENvb2tpZXNUZXh0J11cbn0pXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdkaWdpLW5vdGlmaWNhdGlvbi1jb29raWUnLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaCxcbiAgdGVtcGxhdGU6ICc8bmctY29udGVudD48L25nLWNvbnRlbnQ+JyxcbiAgaW5wdXRzOiBbJ2FmQmFubmVySGVhZGluZ1RleHQnLCAnYWZCYW5uZXJUZXh0JywgJ2FmSWQnLCAnYWZNb2RhbEhlYWRpbmdUZXh0JywgJ2FmUmVxdWlyZWRDb29raWVzVGV4dCddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lOb3RpZmljYXRpb25Db29raWUge1xuICBwcm90ZWN0ZWQgZWw6IEhUTUxFbGVtZW50O1xuICBjb25zdHJ1Y3RvcihjOiBDaGFuZ2VEZXRlY3RvclJlZiwgcjogRWxlbWVudFJlZiwgcHJvdGVjdGVkIHo6IE5nWm9uZSkge1xuICAgIGMuZGV0YWNoKCk7XG4gICAgdGhpcy5lbCA9IHIubmF0aXZlRWxlbWVudDtcbiAgICBwcm94eU91dHB1dHModGhpcywgdGhpcy5lbCwgWydhZk9uQWNjZXB0QWxsQ29va2llcycsICdhZk9uU3VibWl0U2V0dGluZ3MnXSk7XG4gIH1cbn1cblxuXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgRGlnaU5vdGlmaWNhdGlvbkRldGFpbCBleHRlbmRzIENvbXBvbmVudHMuRGlnaU5vdGlmaWNhdGlvbkRldGFpbCB7fVxuXG5AUHJveHlDbXAoe1xuICBkZWZpbmVDdXN0b21FbGVtZW50Rm46IGRlZmluZURpZ2lOb3RpZmljYXRpb25EZXRhaWwsXG4gIGlucHV0czogWydhZlZhcmlhdGlvbiddXG59KVxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGlnaS1ub3RpZmljYXRpb24tZGV0YWlsJyxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXG4gIHRlbXBsYXRlOiAnPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PicsXG4gIGlucHV0czogWydhZlZhcmlhdGlvbiddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lOb3RpZmljYXRpb25EZXRhaWwge1xuICBwcm90ZWN0ZWQgZWw6IEhUTUxFbGVtZW50O1xuICBjb25zdHJ1Y3RvcihjOiBDaGFuZ2VEZXRlY3RvclJlZiwgcjogRWxlbWVudFJlZiwgcHJvdGVjdGVkIHo6IE5nWm9uZSkge1xuICAgIGMuZGV0YWNoKCk7XG4gICAgdGhpcy5lbCA9IHIubmF0aXZlRWxlbWVudDtcbiAgfVxufVxuXG5cbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBEaWdpUGFnZSBleHRlbmRzIENvbXBvbmVudHMuRGlnaVBhZ2Uge31cblxuQFByb3h5Q21wKHtcbiAgZGVmaW5lQ3VzdG9tRWxlbWVudEZuOiBkZWZpbmVEaWdpUGFnZSxcbiAgaW5wdXRzOiBbJ2FmQmFja2dyb3VuZCddXG59KVxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGlnaS1wYWdlJyxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXG4gIHRlbXBsYXRlOiAnPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PicsXG4gIGlucHV0czogWydhZkJhY2tncm91bmQnXVxufSlcbmV4cG9ydCBjbGFzcyBEaWdpUGFnZSB7XG4gIHByb3RlY3RlZCBlbDogSFRNTEVsZW1lbnQ7XG4gIGNvbnN0cnVjdG9yKGM6IENoYW5nZURldGVjdG9yUmVmLCByOiBFbGVtZW50UmVmLCBwcm90ZWN0ZWQgejogTmdab25lKSB7XG4gICAgYy5kZXRhY2goKTtcbiAgICB0aGlzLmVsID0gci5uYXRpdmVFbGVtZW50O1xuICB9XG59XG5cblxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIERpZ2lQYWdlQmxvY2tDYXJkcyBleHRlbmRzIENvbXBvbmVudHMuRGlnaVBhZ2VCbG9ja0NhcmRzIHt9XG5cbkBQcm94eUNtcCh7XG4gIGRlZmluZUN1c3RvbUVsZW1lbnRGbjogZGVmaW5lRGlnaVBhZ2VCbG9ja0NhcmRzLFxuICBpbnB1dHM6IFsnYWZWYXJpYXRpb24nXVxufSlcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2RpZ2ktcGFnZS1ibG9jay1jYXJkcycsXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxuICB0ZW1wbGF0ZTogJzxuZy1jb250ZW50PjwvbmctY29udGVudD4nLFxuICBpbnB1dHM6IFsnYWZWYXJpYXRpb24nXVxufSlcbmV4cG9ydCBjbGFzcyBEaWdpUGFnZUJsb2NrQ2FyZHMge1xuICBwcm90ZWN0ZWQgZWw6IEhUTUxFbGVtZW50O1xuICBjb25zdHJ1Y3RvcihjOiBDaGFuZ2VEZXRlY3RvclJlZiwgcjogRWxlbWVudFJlZiwgcHJvdGVjdGVkIHo6IE5nWm9uZSkge1xuICAgIGMuZGV0YWNoKCk7XG4gICAgdGhpcy5lbCA9IHIubmF0aXZlRWxlbWVudDtcbiAgfVxufVxuXG5cbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBEaWdpUGFnZUJsb2NrSGVybyBleHRlbmRzIENvbXBvbmVudHMuRGlnaVBhZ2VCbG9ja0hlcm8ge31cblxuQFByb3h5Q21wKHtcbiAgZGVmaW5lQ3VzdG9tRWxlbWVudEZuOiBkZWZpbmVEaWdpUGFnZUJsb2NrSGVybyxcbiAgaW5wdXRzOiBbJ2FmQmFja2dyb3VuZCcsICdhZkJhY2tncm91bmRJbWFnZScsICdhZlZhcmlhdGlvbiddXG59KVxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGlnaS1wYWdlLWJsb2NrLWhlcm8nLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaCxcbiAgdGVtcGxhdGU6ICc8bmctY29udGVudD48L25nLWNvbnRlbnQ+JyxcbiAgaW5wdXRzOiBbJ2FmQmFja2dyb3VuZCcsICdhZkJhY2tncm91bmRJbWFnZScsICdhZlZhcmlhdGlvbiddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lQYWdlQmxvY2tIZXJvIHtcbiAgcHJvdGVjdGVkIGVsOiBIVE1MRWxlbWVudDtcbiAgY29uc3RydWN0b3IoYzogQ2hhbmdlRGV0ZWN0b3JSZWYsIHI6IEVsZW1lbnRSZWYsIHByb3RlY3RlZCB6OiBOZ1pvbmUpIHtcbiAgICBjLmRldGFjaCgpO1xuICAgIHRoaXMuZWwgPSByLm5hdGl2ZUVsZW1lbnQ7XG4gIH1cbn1cblxuXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgRGlnaVBhZ2VCbG9ja0xpc3RzIGV4dGVuZHMgQ29tcG9uZW50cy5EaWdpUGFnZUJsb2NrTGlzdHMge31cblxuQFByb3h5Q21wKHtcbiAgZGVmaW5lQ3VzdG9tRWxlbWVudEZuOiBkZWZpbmVEaWdpUGFnZUJsb2NrTGlzdHMsXG4gIGlucHV0czogWydhZlZhcmlhdGlvbiddXG59KVxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGlnaS1wYWdlLWJsb2NrLWxpc3RzJyxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXG4gIHRlbXBsYXRlOiAnPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PicsXG4gIGlucHV0czogWydhZlZhcmlhdGlvbiddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lQYWdlQmxvY2tMaXN0cyB7XG4gIHByb3RlY3RlZCBlbDogSFRNTEVsZW1lbnQ7XG4gIGNvbnN0cnVjdG9yKGM6IENoYW5nZURldGVjdG9yUmVmLCByOiBFbGVtZW50UmVmLCBwcm90ZWN0ZWQgejogTmdab25lKSB7XG4gICAgYy5kZXRhY2goKTtcbiAgICB0aGlzLmVsID0gci5uYXRpdmVFbGVtZW50O1xuICB9XG59XG5cblxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIERpZ2lQYWdlQmxvY2tTaWRlYmFyIGV4dGVuZHMgQ29tcG9uZW50cy5EaWdpUGFnZUJsb2NrU2lkZWJhciB7fVxuXG5AUHJveHlDbXAoe1xuICBkZWZpbmVDdXN0b21FbGVtZW50Rm46IGRlZmluZURpZ2lQYWdlQmxvY2tTaWRlYmFyLFxuICBpbnB1dHM6IFsnYWZWYXJpYXRpb24nXVxufSlcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2RpZ2ktcGFnZS1ibG9jay1zaWRlYmFyJyxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXG4gIHRlbXBsYXRlOiAnPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PicsXG4gIGlucHV0czogWydhZlZhcmlhdGlvbiddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lQYWdlQmxvY2tTaWRlYmFyIHtcbiAgcHJvdGVjdGVkIGVsOiBIVE1MRWxlbWVudDtcbiAgY29uc3RydWN0b3IoYzogQ2hhbmdlRGV0ZWN0b3JSZWYsIHI6IEVsZW1lbnRSZWYsIHByb3RlY3RlZCB6OiBOZ1pvbmUpIHtcbiAgICBjLmRldGFjaCgpO1xuICAgIHRoaXMuZWwgPSByLm5hdGl2ZUVsZW1lbnQ7XG4gIH1cbn1cblxuXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgRGlnaVBhZ2VGb290ZXIgZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lQYWdlRm9vdGVyIHt9XG5cbkBQcm94eUNtcCh7XG4gIGRlZmluZUN1c3RvbUVsZW1lbnRGbjogZGVmaW5lRGlnaVBhZ2VGb290ZXIsXG4gIGlucHV0czogWydhZlZhcmlhdGlvbiddXG59KVxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGlnaS1wYWdlLWZvb3RlcicsXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxuICB0ZW1wbGF0ZTogJzxuZy1jb250ZW50PjwvbmctY29udGVudD4nLFxuICBpbnB1dHM6IFsnYWZWYXJpYXRpb24nXVxufSlcbmV4cG9ydCBjbGFzcyBEaWdpUGFnZUZvb3RlciB7XG4gIHByb3RlY3RlZCBlbDogSFRNTEVsZW1lbnQ7XG4gIGNvbnN0cnVjdG9yKGM6IENoYW5nZURldGVjdG9yUmVmLCByOiBFbGVtZW50UmVmLCBwcm90ZWN0ZWQgejogTmdab25lKSB7XG4gICAgYy5kZXRhY2goKTtcbiAgICB0aGlzLmVsID0gci5uYXRpdmVFbGVtZW50O1xuICB9XG59XG5cblxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIERpZ2lQYWdlSGVhZGVyIGV4dGVuZHMgQ29tcG9uZW50cy5EaWdpUGFnZUhlYWRlciB7fVxuXG5AUHJveHlDbXAoe1xuICBkZWZpbmVDdXN0b21FbGVtZW50Rm46IGRlZmluZURpZ2lQYWdlSGVhZGVyLFxuICBpbnB1dHM6IFsnYWZJZCddXG59KVxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGlnaS1wYWdlLWhlYWRlcicsXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxuICB0ZW1wbGF0ZTogJzxuZy1jb250ZW50PjwvbmctY29udGVudD4nLFxuICBpbnB1dHM6IFsnYWZJZCddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lQYWdlSGVhZGVyIHtcbiAgcHJvdGVjdGVkIGVsOiBIVE1MRWxlbWVudDtcbiAgY29uc3RydWN0b3IoYzogQ2hhbmdlRGV0ZWN0b3JSZWYsIHI6IEVsZW1lbnRSZWYsIHByb3RlY3RlZCB6OiBOZ1pvbmUpIHtcbiAgICBjLmRldGFjaCgpO1xuICAgIHRoaXMuZWwgPSByLm5hdGl2ZUVsZW1lbnQ7XG4gIH1cbn1cblxuXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgRGlnaVByb2dyZXNzU3RlcCBleHRlbmRzIENvbXBvbmVudHMuRGlnaVByb2dyZXNzU3RlcCB7fVxuXG5AUHJveHlDbXAoe1xuICBkZWZpbmVDdXN0b21FbGVtZW50Rm46IGRlZmluZURpZ2lQcm9ncmVzc1N0ZXAsXG4gIGlucHV0czogWydhZkhlYWRpbmcnLCAnYWZIZWFkaW5nTGV2ZWwnLCAnYWZJZCcsICdhZklzTGFzdCcsICdhZlN0ZXBTdGF0dXMnLCAnYWZWYXJpYXRpb24nXVxufSlcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2RpZ2ktcHJvZ3Jlc3Mtc3RlcCcsXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxuICB0ZW1wbGF0ZTogJzxuZy1jb250ZW50PjwvbmctY29udGVudD4nLFxuICBpbnB1dHM6IFsnYWZIZWFkaW5nJywgJ2FmSGVhZGluZ0xldmVsJywgJ2FmSWQnLCAnYWZJc0xhc3QnLCAnYWZTdGVwU3RhdHVzJywgJ2FmVmFyaWF0aW9uJ11cbn0pXG5leHBvcnQgY2xhc3MgRGlnaVByb2dyZXNzU3RlcCB7XG4gIHByb3RlY3RlZCBlbDogSFRNTEVsZW1lbnQ7XG4gIGNvbnN0cnVjdG9yKGM6IENoYW5nZURldGVjdG9yUmVmLCByOiBFbGVtZW50UmVmLCBwcm90ZWN0ZWQgejogTmdab25lKSB7XG4gICAgYy5kZXRhY2goKTtcbiAgICB0aGlzLmVsID0gci5uYXRpdmVFbGVtZW50O1xuICB9XG59XG5cblxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIERpZ2lQcm9ncmVzc1N0ZXBzIGV4dGVuZHMgQ29tcG9uZW50cy5EaWdpUHJvZ3Jlc3NTdGVwcyB7fVxuXG5AUHJveHlDbXAoe1xuICBkZWZpbmVDdXN0b21FbGVtZW50Rm46IGRlZmluZURpZ2lQcm9ncmVzc1N0ZXBzLFxuICBpbnB1dHM6IFsnYWZDdXJyZW50U3RlcCcsICdhZkhlYWRpbmdMZXZlbCcsICdhZlZhcmlhdGlvbiddLFxuICBtZXRob2RzOiBbJ2FmTU5leHQnLCAnYWZNUHJldmlvdXMnXVxufSlcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2RpZ2ktcHJvZ3Jlc3Mtc3RlcHMnLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaCxcbiAgdGVtcGxhdGU6ICc8bmctY29udGVudD48L25nLWNvbnRlbnQ+JyxcbiAgaW5wdXRzOiBbJ2FmQ3VycmVudFN0ZXAnLCAnYWZIZWFkaW5nTGV2ZWwnLCAnYWZWYXJpYXRpb24nXVxufSlcbmV4cG9ydCBjbGFzcyBEaWdpUHJvZ3Jlc3NTdGVwcyB7XG4gIHByb3RlY3RlZCBlbDogSFRNTEVsZW1lbnQ7XG4gIGNvbnN0cnVjdG9yKGM6IENoYW5nZURldGVjdG9yUmVmLCByOiBFbGVtZW50UmVmLCBwcm90ZWN0ZWQgejogTmdab25lKSB7XG4gICAgYy5kZXRhY2goKTtcbiAgICB0aGlzLmVsID0gci5uYXRpdmVFbGVtZW50O1xuICB9XG59XG5cblxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIERpZ2lQcm9ncmVzc2JhciBleHRlbmRzIENvbXBvbmVudHMuRGlnaVByb2dyZXNzYmFyIHt9XG5cbkBQcm94eUNtcCh7XG4gIGRlZmluZUN1c3RvbUVsZW1lbnRGbjogZGVmaW5lRGlnaVByb2dyZXNzYmFyLFxuICBpbnB1dHM6IFsnYWZDb21wbGV0ZWRTdGVwcycsICdhZklkJywgJ2FmUm9sZScsICdhZlN0ZXBzTGFiZWwnLCAnYWZTdGVwc1NlcGFyYXRvcicsICdhZlRvdGFsU3RlcHMnLCAnYWZWYXJpYXRpb24nXVxufSlcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2RpZ2ktcHJvZ3Jlc3NiYXInLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaCxcbiAgdGVtcGxhdGU6ICc8bmctY29udGVudD48L25nLWNvbnRlbnQ+JyxcbiAgaW5wdXRzOiBbJ2FmQ29tcGxldGVkU3RlcHMnLCAnYWZJZCcsICdhZlJvbGUnLCAnYWZTdGVwc0xhYmVsJywgJ2FmU3RlcHNTZXBhcmF0b3InLCAnYWZUb3RhbFN0ZXBzJywgJ2FmVmFyaWF0aW9uJ11cbn0pXG5leHBvcnQgY2xhc3MgRGlnaVByb2dyZXNzYmFyIHtcbiAgcHJvdGVjdGVkIGVsOiBIVE1MRWxlbWVudDtcbiAgY29uc3RydWN0b3IoYzogQ2hhbmdlRGV0ZWN0b3JSZWYsIHI6IEVsZW1lbnRSZWYsIHByb3RlY3RlZCB6OiBOZ1pvbmUpIHtcbiAgICBjLmRldGFjaCgpO1xuICAgIHRoaXMuZWwgPSByLm5hdGl2ZUVsZW1lbnQ7XG4gIH1cbn1cblxuXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgRGlnaVRhYmxlIGV4dGVuZHMgQ29tcG9uZW50cy5EaWdpVGFibGUge31cblxuQFByb3h5Q21wKHtcbiAgZGVmaW5lQ3VzdG9tRWxlbWVudEZuOiBkZWZpbmVEaWdpVGFibGUsXG4gIGlucHV0czogWydhZlZhcmlhdGlvbiddXG59KVxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGlnaS10YWJsZScsXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxuICB0ZW1wbGF0ZTogJzxuZy1jb250ZW50PjwvbmctY29udGVudD4nLFxuICBpbnB1dHM6IFsnYWZWYXJpYXRpb24nXVxufSlcbmV4cG9ydCBjbGFzcyBEaWdpVGFibGUge1xuICBwcm90ZWN0ZWQgZWw6IEhUTUxFbGVtZW50O1xuICBjb25zdHJ1Y3RvcihjOiBDaGFuZ2VEZXRlY3RvclJlZiwgcjogRWxlbWVudFJlZiwgcHJvdGVjdGVkIHo6IE5nWm9uZSkge1xuICAgIGMuZGV0YWNoKCk7XG4gICAgdGhpcy5lbCA9IHIubmF0aXZlRWxlbWVudDtcbiAgfVxufVxuXG5cbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBEaWdpVGFnIGV4dGVuZHMgQ29tcG9uZW50cy5EaWdpVGFnIHtcbiAgLyoqXG4gICAqIFRhZ2dlbGVtZW50ZXRzICdvbmNsaWNrJy1ldmVudC4gQGVuIFRoZSB0YWcgZWxlbWVudHMgJ29uY2xpY2snIGV2ZW50LlxuICAgKi9cbiAgYWZPbkNsaWNrOiBFdmVudEVtaXR0ZXI8Q3VzdG9tRXZlbnQ8YW55Pj47XG5cbn1cblxuQFByb3h5Q21wKHtcbiAgZGVmaW5lQ3VzdG9tRWxlbWVudEZuOiBkZWZpbmVEaWdpVGFnLFxuICBpbnB1dHM6IFsnYWZOb0ljb24nLCAnYWZTaXplJywgJ2FmVGV4dCddXG59KVxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGlnaS10YWcnLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaCxcbiAgdGVtcGxhdGU6ICc8bmctY29udGVudD48L25nLWNvbnRlbnQ+JyxcbiAgaW5wdXRzOiBbJ2FmTm9JY29uJywgJ2FmU2l6ZScsICdhZlRleHQnXVxufSlcbmV4cG9ydCBjbGFzcyBEaWdpVGFnIHtcbiAgcHJvdGVjdGVkIGVsOiBIVE1MRWxlbWVudDtcbiAgY29uc3RydWN0b3IoYzogQ2hhbmdlRGV0ZWN0b3JSZWYsIHI6IEVsZW1lbnRSZWYsIHByb3RlY3RlZCB6OiBOZ1pvbmUpIHtcbiAgICBjLmRldGFjaCgpO1xuICAgIHRoaXMuZWwgPSByLm5hdGl2ZUVsZW1lbnQ7XG4gICAgcHJveHlPdXRwdXRzKHRoaXMsIHRoaXMuZWwsIFsnYWZPbkNsaWNrJ10pO1xuICB9XG59XG5cblxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIERpZ2lUeXBvZ3JhcGh5IGV4dGVuZHMgQ29tcG9uZW50cy5EaWdpVHlwb2dyYXBoeSB7fVxuXG5AUHJveHlDbXAoe1xuICBkZWZpbmVDdXN0b21FbGVtZW50Rm46IGRlZmluZURpZ2lUeXBvZ3JhcGh5LFxuICBpbnB1dHM6IFsnYWZWYXJpYXRpb24nXVxufSlcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2RpZ2ktdHlwb2dyYXBoeScsXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxuICB0ZW1wbGF0ZTogJzxuZy1jb250ZW50PjwvbmctY29udGVudD4nLFxuICBpbnB1dHM6IFsnYWZWYXJpYXRpb24nXVxufSlcbmV4cG9ydCBjbGFzcyBEaWdpVHlwb2dyYXBoeSB7XG4gIHByb3RlY3RlZCBlbDogSFRNTEVsZW1lbnQ7XG4gIGNvbnN0cnVjdG9yKGM6IENoYW5nZURldGVjdG9yUmVmLCByOiBFbGVtZW50UmVmLCBwcm90ZWN0ZWQgejogTmdab25lKSB7XG4gICAgYy5kZXRhY2goKTtcbiAgICB0aGlzLmVsID0gci5uYXRpdmVFbGVtZW50O1xuICB9XG59XG5cblxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIERpZ2lUeXBvZ3JhcGh5SGVhZGluZ1NlY3Rpb24gZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lUeXBvZ3JhcGh5SGVhZGluZ1NlY3Rpb24ge31cblxuQFByb3h5Q21wKHtcbiAgZGVmaW5lQ3VzdG9tRWxlbWVudEZuOiBkZWZpbmVEaWdpVHlwb2dyYXBoeUhlYWRpbmdTZWN0aW9uXG59KVxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGlnaS10eXBvZ3JhcGh5LWhlYWRpbmctc2VjdGlvbicsXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxuICB0ZW1wbGF0ZTogJzxuZy1jb250ZW50PjwvbmctY29udGVudD4nXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lUeXBvZ3JhcGh5SGVhZGluZ1NlY3Rpb24ge1xuICBwcm90ZWN0ZWQgZWw6IEhUTUxFbGVtZW50O1xuICBjb25zdHJ1Y3RvcihjOiBDaGFuZ2VEZXRlY3RvclJlZiwgcjogRWxlbWVudFJlZiwgcHJvdGVjdGVkIHo6IE5nWm9uZSkge1xuICAgIGMuZGV0YWNoKCk7XG4gICAgdGhpcy5lbCA9IHIubmF0aXZlRWxlbWVudDtcbiAgfVxufVxuXG5cbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBEaWdpVHlwb2dyYXBoeU1ldGEgZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lUeXBvZ3JhcGh5TWV0YSB7fVxuXG5AUHJveHlDbXAoe1xuICBkZWZpbmVDdXN0b21FbGVtZW50Rm46IGRlZmluZURpZ2lUeXBvZ3JhcGh5TWV0YSxcbiAgaW5wdXRzOiBbJ2FmVmFyaWF0aW9uJ11cbn0pXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdkaWdpLXR5cG9ncmFwaHktbWV0YScsXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxuICB0ZW1wbGF0ZTogJzxuZy1jb250ZW50PjwvbmctY29udGVudD4nLFxuICBpbnB1dHM6IFsnYWZWYXJpYXRpb24nXVxufSlcbmV4cG9ydCBjbGFzcyBEaWdpVHlwb2dyYXBoeU1ldGEge1xuICBwcm90ZWN0ZWQgZWw6IEhUTUxFbGVtZW50O1xuICBjb25zdHJ1Y3RvcihjOiBDaGFuZ2VEZXRlY3RvclJlZiwgcjogRWxlbWVudFJlZiwgcHJvdGVjdGVkIHo6IE5nWm9uZSkge1xuICAgIGMuZGV0YWNoKCk7XG4gICAgdGhpcy5lbCA9IHIubmF0aXZlRWxlbWVudDtcbiAgfVxufVxuXG5cbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBEaWdpVHlwb2dyYXBoeVByZWFtYmxlIGV4dGVuZHMgQ29tcG9uZW50cy5EaWdpVHlwb2dyYXBoeVByZWFtYmxlIHt9XG5cbkBQcm94eUNtcCh7XG4gIGRlZmluZUN1c3RvbUVsZW1lbnRGbjogZGVmaW5lRGlnaVR5cG9ncmFwaHlQcmVhbWJsZVxufSlcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2RpZ2ktdHlwb2dyYXBoeS1wcmVhbWJsZScsXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxuICB0ZW1wbGF0ZTogJzxuZy1jb250ZW50PjwvbmctY29udGVudD4nXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lUeXBvZ3JhcGh5UHJlYW1ibGUge1xuICBwcm90ZWN0ZWQgZWw6IEhUTUxFbGVtZW50O1xuICBjb25zdHJ1Y3RvcihjOiBDaGFuZ2VEZXRlY3RvclJlZiwgcjogRWxlbWVudFJlZiwgcHJvdGVjdGVkIHo6IE5nWm9uZSkge1xuICAgIGMuZGV0YWNoKCk7XG4gICAgdGhpcy5lbCA9IHIubmF0aXZlRWxlbWVudDtcbiAgfVxufVxuXG5cbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBEaWdpVHlwb2dyYXBoeVRpbWUgZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lUeXBvZ3JhcGh5VGltZSB7fVxuXG5AUHJveHlDbXAoe1xuICBkZWZpbmVDdXN0b21FbGVtZW50Rm46IGRlZmluZURpZ2lUeXBvZ3JhcGh5VGltZSxcbiAgaW5wdXRzOiBbJ2FmRGF0ZVRpbWUnLCAnYWZWYXJpYXRpb24nXVxufSlcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2RpZ2ktdHlwb2dyYXBoeS10aW1lJyxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXG4gIHRlbXBsYXRlOiAnPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PicsXG4gIGlucHV0czogWydhZkRhdGVUaW1lJywgJ2FmVmFyaWF0aW9uJ11cbn0pXG5leHBvcnQgY2xhc3MgRGlnaVR5cG9ncmFwaHlUaW1lIHtcbiAgcHJvdGVjdGVkIGVsOiBIVE1MRWxlbWVudDtcbiAgY29uc3RydWN0b3IoYzogQ2hhbmdlRGV0ZWN0b3JSZWYsIHI6IEVsZW1lbnRSZWYsIHByb3RlY3RlZCB6OiBOZ1pvbmUpIHtcbiAgICBjLmRldGFjaCgpO1xuICAgIHRoaXMuZWwgPSByLm5hdGl2ZUVsZW1lbnQ7XG4gIH1cbn1cblxuXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgRGlnaVV0aWxCcmVha3BvaW50T2JzZXJ2ZXIgZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lVdGlsQnJlYWtwb2ludE9ic2VydmVyIHtcbiAgLyoqXG4gICAqIFZpZCBpbmzDpHNuaW5nIGF2IHNpZGEgc2FtdCB2aWQgdXBwZGF0ZXJpbmcgYXYgYnJ5dHB1bmt0IEBlbiBBdCBwYWdlIGxvYWQgYW5kIGF0IGNoYW5nZSBvZiBicmVha3BvaW50IG9uIHBhZ2VcbiAgICovXG4gIGFmT25DaGFuZ2U6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxhbnk+PjtcbiAgLyoqXG4gICAqIFZpZCBicnl0cHVua3QgJ3NtYWxsJyBAZW4gQXQgYnJlYWtwb2ludCAnc21hbGwnXG4gICAqL1xuICBhZk9uU21hbGw6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxhbnk+PjtcbiAgLyoqXG4gICAqIFZpZCBicnl0cHVua3QgJ21lZGl1bScgQGVuIEF0IGJyZWFrcG9pbnQgJ21lZGl1bSdcbiAgICovXG4gIGFmT25NZWRpdW06IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxhbnk+PjtcbiAgLyoqXG4gICAqIFZpZCBicnl0cHVua3QgJ2xhcmdlJyBAZW4gQXQgYnJlYWtwb2ludCAnbGFyZ2UnXG4gICAqL1xuICBhZk9uTGFyZ2U6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxhbnk+PjtcbiAgLyoqXG4gICAqIFZpZCBicnl0cHVua3QgJ3hsYXJnZScgQGVuIEF0IGJyZWFrcG9pbnQgJ3hsYXJnZSdcbiAgICovXG4gIGFmT25YTGFyZ2U6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxhbnk+PjtcblxufVxuXG5AUHJveHlDbXAoe1xuICBkZWZpbmVDdXN0b21FbGVtZW50Rm46IGRlZmluZURpZ2lVdGlsQnJlYWtwb2ludE9ic2VydmVyXG59KVxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGlnaS11dGlsLWJyZWFrcG9pbnQtb2JzZXJ2ZXInLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaCxcbiAgdGVtcGxhdGU6ICc8bmctY29udGVudD48L25nLWNvbnRlbnQ+J1xufSlcbmV4cG9ydCBjbGFzcyBEaWdpVXRpbEJyZWFrcG9pbnRPYnNlcnZlciB7XG4gIHByb3RlY3RlZCBlbDogSFRNTEVsZW1lbnQ7XG4gIGNvbnN0cnVjdG9yKGM6IENoYW5nZURldGVjdG9yUmVmLCByOiBFbGVtZW50UmVmLCBwcm90ZWN0ZWQgejogTmdab25lKSB7XG4gICAgYy5kZXRhY2goKTtcbiAgICB0aGlzLmVsID0gci5uYXRpdmVFbGVtZW50O1xuICAgIHByb3h5T3V0cHV0cyh0aGlzLCB0aGlzLmVsLCBbJ2FmT25DaGFuZ2UnLCAnYWZPblNtYWxsJywgJ2FmT25NZWRpdW0nLCAnYWZPbkxhcmdlJywgJ2FmT25YTGFyZ2UnXSk7XG4gIH1cbn1cblxuXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgRGlnaVV0aWxEZXRlY3RDbGlja091dHNpZGUgZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lVdGlsRGV0ZWN0Q2xpY2tPdXRzaWRlIHtcbiAgLyoqXG4gICAqIFZpZCBrbGljayB1dGFuZsO2ciBrb21wb25lbnRlbiBAZW4gV2hlbiBjbGljayBkZXRlY3RlZCBvdXRzaWRlIG9mIGNvbXBvbmVudFxuICAgKi9cbiAgYWZPbkNsaWNrT3V0c2lkZTogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PE1vdXNlRXZlbnQ+PjtcbiAgLyoqXG4gICAqIFZpZCBrbGljayBpbnV0aSBrb21wb25lbnRlbiBAZW4gV2hlbiBjbGljayBkZXRlY3RlZCBpbnNpZGUgb2YgY29tcG9uZW50XG4gICAqL1xuICBhZk9uQ2xpY2tJbnNpZGU6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxNb3VzZUV2ZW50Pj47XG5cbn1cblxuQFByb3h5Q21wKHtcbiAgZGVmaW5lQ3VzdG9tRWxlbWVudEZuOiBkZWZpbmVEaWdpVXRpbERldGVjdENsaWNrT3V0c2lkZSxcbiAgaW5wdXRzOiBbJ2FmRGF0YUlkZW50aWZpZXInXVxufSlcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2RpZ2ktdXRpbC1kZXRlY3QtY2xpY2stb3V0c2lkZScsXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxuICB0ZW1wbGF0ZTogJzxuZy1jb250ZW50PjwvbmctY29udGVudD4nLFxuICBpbnB1dHM6IFsnYWZEYXRhSWRlbnRpZmllciddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lVdGlsRGV0ZWN0Q2xpY2tPdXRzaWRlIHtcbiAgcHJvdGVjdGVkIGVsOiBIVE1MRWxlbWVudDtcbiAgY29uc3RydWN0b3IoYzogQ2hhbmdlRGV0ZWN0b3JSZWYsIHI6IEVsZW1lbnRSZWYsIHByb3RlY3RlZCB6OiBOZ1pvbmUpIHtcbiAgICBjLmRldGFjaCgpO1xuICAgIHRoaXMuZWwgPSByLm5hdGl2ZUVsZW1lbnQ7XG4gICAgcHJveHlPdXRwdXRzKHRoaXMsIHRoaXMuZWwsIFsnYWZPbkNsaWNrT3V0c2lkZScsICdhZk9uQ2xpY2tJbnNpZGUnXSk7XG4gIH1cbn1cblxuXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgRGlnaVV0aWxEZXRlY3RGb2N1c091dHNpZGUgZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lVdGlsRGV0ZWN0Rm9jdXNPdXRzaWRlIHtcbiAgLyoqXG4gICAqIFZpZCBmb2t1cyB1dGFuZsO2ciBrb21wb25lbnRlbiBAZW4gV2hlbiBmb2N1cyBkZXRlY3RlZCBvdXRzaWRlIG9mIGNvbXBvbmVudFxuICAgKi9cbiAgYWZPbkZvY3VzT3V0c2lkZTogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PEZvY3VzRXZlbnQ+PjtcbiAgLyoqXG4gICAqIFZpZCBmb2t1cyBpbnV0aSBrb21wb25lbnRlbiBAZW4gV2hlbiBmb2N1cyBkZXRlY3RlZCBpbnNpZGUgb2YgY29tcG9uZW50XG4gICAqL1xuICBhZk9uRm9jdXNJbnNpZGU6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxGb2N1c0V2ZW50Pj47XG5cbn1cblxuQFByb3h5Q21wKHtcbiAgZGVmaW5lQ3VzdG9tRWxlbWVudEZuOiBkZWZpbmVEaWdpVXRpbERldGVjdEZvY3VzT3V0c2lkZSxcbiAgaW5wdXRzOiBbJ2FmRGF0YUlkZW50aWZpZXInXVxufSlcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2RpZ2ktdXRpbC1kZXRlY3QtZm9jdXMtb3V0c2lkZScsXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxuICB0ZW1wbGF0ZTogJzxuZy1jb250ZW50PjwvbmctY29udGVudD4nLFxuICBpbnB1dHM6IFsnYWZEYXRhSWRlbnRpZmllciddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lVdGlsRGV0ZWN0Rm9jdXNPdXRzaWRlIHtcbiAgcHJvdGVjdGVkIGVsOiBIVE1MRWxlbWVudDtcbiAgY29uc3RydWN0b3IoYzogQ2hhbmdlRGV0ZWN0b3JSZWYsIHI6IEVsZW1lbnRSZWYsIHByb3RlY3RlZCB6OiBOZ1pvbmUpIHtcbiAgICBjLmRldGFjaCgpO1xuICAgIHRoaXMuZWwgPSByLm5hdGl2ZUVsZW1lbnQ7XG4gICAgcHJveHlPdXRwdXRzKHRoaXMsIHRoaXMuZWwsIFsnYWZPbkZvY3VzT3V0c2lkZScsICdhZk9uRm9jdXNJbnNpZGUnXSk7XG4gIH1cbn1cblxuXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgRGlnaVV0aWxJbnRlcnNlY3Rpb25PYnNlcnZlciBleHRlbmRzIENvbXBvbmVudHMuRGlnaVV0aWxJbnRlcnNlY3Rpb25PYnNlcnZlciB7XG4gIC8qKlxuICAgKiBWaWQgc3RhdHVzZsO2csOkbmRyaW5nIG1lbGxhbiAndW5pbnRlcnNlY3RlZCcgb2NoICdpbnRlcnNlY3RlZCcgQGVuIE9uIGNoYW5nZSBiZXR3ZWVuIHVuaW50ZXJzZWN0ZWQgYW5kIGludGVyc2VjdGVkXG4gICAqL1xuICBhZk9uQ2hhbmdlOiBFdmVudEVtaXR0ZXI8Q3VzdG9tRXZlbnQ8Ym9vbGVhbj4+O1xuICAvKipcbiAgICogVmlkIHN0YXR1c2bDtnLDpG5kcmluZyB0aWxsICdpbnRlcnNlY3RlZCcgQGVuIE9uIGV2ZXJ5IGNoYW5nZSB0byBpbnRlcnNlY3RlZC5cbiAgICovXG4gIGFmT25JbnRlcnNlY3Q6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxhbnk+PjtcbiAgLyoqXG4gICAqIFZpZCBzdGF0dXNmw7Zyw6RuZHJpbmcgdGlsbCAndW5pbnRlcnNlY3RlZCdcclxuT24gZXZlcnkgY2hhbmdlIHRvIHVuaW50ZXJzZWN0ZWQgXG4gICAqL1xuICBhZk9uVW5pbnRlcnNlY3Q6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxhbnk+PjtcblxufVxuXG5AUHJveHlDbXAoe1xuICBkZWZpbmVDdXN0b21FbGVtZW50Rm46IGRlZmluZURpZ2lVdGlsSW50ZXJzZWN0aW9uT2JzZXJ2ZXIsXG4gIGlucHV0czogWydhZk9uY2UnLCAnYWZPcHRpb25zJ11cbn0pXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdkaWdpLXV0aWwtaW50ZXJzZWN0aW9uLW9ic2VydmVyJyxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXG4gIHRlbXBsYXRlOiAnPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PicsXG4gIGlucHV0czogWydhZk9uY2UnLCAnYWZPcHRpb25zJ11cbn0pXG5leHBvcnQgY2xhc3MgRGlnaVV0aWxJbnRlcnNlY3Rpb25PYnNlcnZlciB7XG4gIHByb3RlY3RlZCBlbDogSFRNTEVsZW1lbnQ7XG4gIGNvbnN0cnVjdG9yKGM6IENoYW5nZURldGVjdG9yUmVmLCByOiBFbGVtZW50UmVmLCBwcm90ZWN0ZWQgejogTmdab25lKSB7XG4gICAgYy5kZXRhY2goKTtcbiAgICB0aGlzLmVsID0gci5uYXRpdmVFbGVtZW50O1xuICAgIHByb3h5T3V0cHV0cyh0aGlzLCB0aGlzLmVsLCBbJ2FmT25DaGFuZ2UnLCAnYWZPbkludGVyc2VjdCcsICdhZk9uVW5pbnRlcnNlY3QnXSk7XG4gIH1cbn1cblxuXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgRGlnaVV0aWxLZXlkb3duSGFuZGxlciBleHRlbmRzIENvbXBvbmVudHMuRGlnaVV0aWxLZXlkb3duSGFuZGxlciB7XG4gIC8qKlxuICAgKiBWaWQga2V5ZG93biBww6UgZXNjYXBlIEBlbiBBdCBrZXlkb3duIG9uIGVzY2FwZSBrZXlcbiAgICovXG4gIGFmT25Fc2M6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxLZXlib2FyZEV2ZW50Pj47XG4gIC8qKlxuICAgKiBWaWQga2V5ZG93biBww6UgZW50ZXIgQGVuIEF0IGtleWRvd24gb24gZW50ZXIga2V5XG4gICAqL1xuICBhZk9uRW50ZXI6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxLZXlib2FyZEV2ZW50Pj47XG4gIC8qKlxuICAgKiBWaWQga2V5ZG93biBww6UgdGFiIEBlbiBBdCBrZXlkb3duIG9uIHRhYiBrZXlcbiAgICovXG4gIGFmT25UYWI6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxLZXlib2FyZEV2ZW50Pj47XG4gIC8qKlxuICAgKiBWaWQga2V5ZG93biBww6Ugc3BhY2UgQGVuIEF0IGtleWRvd24gb24gc3BhY2Uga2V5XG4gICAqL1xuICBhZk9uU3BhY2U6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxLZXlib2FyZEV2ZW50Pj47XG4gIC8qKlxuICAgKiBWaWQga2V5ZG93biBww6Ugc2tpZnQgb2NoIHRhYmIgQGVuIEF0IGtleWRvd24gb24gc2hpZnQgYW5kIHRhYiBrZXlcbiAgICovXG4gIGFmT25TaGlmdFRhYjogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PEtleWJvYXJkRXZlbnQ+PjtcbiAgLyoqXG4gICAqIFZpZCBrZXlkb3duIHDDpSBwaWwgdXBwIEBlbiBBdCBrZXlkb3duIG9uIHVwIGFycm93IGtleVxuICAgKi9cbiAgYWZPblVwOiBFdmVudEVtaXR0ZXI8Q3VzdG9tRXZlbnQ8S2V5Ym9hcmRFdmVudD4+O1xuICAvKipcbiAgICogVmlkIGtleWRvd24gcMOlIHBpbCBuZXIgQGVuIEF0IGtleWRvd24gb24gZG93biBhcnJvdyBrZXlcbiAgICovXG4gIGFmT25Eb3duOiBFdmVudEVtaXR0ZXI8Q3VzdG9tRXZlbnQ8S2V5Ym9hcmRFdmVudD4+O1xuICAvKipcbiAgICogVmlkIGtleWRvd24gcMOlIHBpbCB2w6Ruc3RlciBAZW4gQXQga2V5ZG93biBvbiBsZWZ0IGFycm93IGtleVxuICAgKi9cbiAgYWZPbkxlZnQ6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxLZXlib2FyZEV2ZW50Pj47XG4gIC8qKlxuICAgKiBWaWQga2V5ZG93biBww6UgcGlsIGjDtmdlciBAZW4gQXQga2V5ZG93biBvbiByaWdodCBhcnJvdyBrZXlcbiAgICovXG4gIGFmT25SaWdodDogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PEtleWJvYXJkRXZlbnQ+PjtcbiAgLyoqXG4gICAqIFZpZCBrZXlkb3duIHDDpSBob21lIEBlbiBBdCBrZXlkb3duIG9uIGhvbWUga2V5XG4gICAqL1xuICBhZk9uSG9tZTogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PEtleWJvYXJkRXZlbnQ+PjtcbiAgLyoqXG4gICAqIFZpZCBrZXlkb3duIHDDpSBlbmQgQGVuIEF0IGtleWRvd24gb24gZW5kIGtleVxuICAgKi9cbiAgYWZPbkVuZDogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PEtleWJvYXJkRXZlbnQ+PjtcbiAgLyoqXG4gICAqIFZpZCBrZXlkb3duIHDDpSBhbGxhIHRhbmdlbnRlciBAZW4gQXQga2V5ZG93biBvbiBhbnkga2V5XG4gICAqL1xuICBhZk9uS2V5RG93bjogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PEtleWJvYXJkRXZlbnQ+PjtcblxufVxuXG5AUHJveHlDbXAoe1xuICBkZWZpbmVDdXN0b21FbGVtZW50Rm46IGRlZmluZURpZ2lVdGlsS2V5ZG93bkhhbmRsZXJcbn0pXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdkaWdpLXV0aWwta2V5ZG93bi1oYW5kbGVyJyxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXG4gIHRlbXBsYXRlOiAnPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50Pidcbn0pXG5leHBvcnQgY2xhc3MgRGlnaVV0aWxLZXlkb3duSGFuZGxlciB7XG4gIHByb3RlY3RlZCBlbDogSFRNTEVsZW1lbnQ7XG4gIGNvbnN0cnVjdG9yKGM6IENoYW5nZURldGVjdG9yUmVmLCByOiBFbGVtZW50UmVmLCBwcm90ZWN0ZWQgejogTmdab25lKSB7XG4gICAgYy5kZXRhY2goKTtcbiAgICB0aGlzLmVsID0gci5uYXRpdmVFbGVtZW50O1xuICAgIHByb3h5T3V0cHV0cyh0aGlzLCB0aGlzLmVsLCBbJ2FmT25Fc2MnLCAnYWZPbkVudGVyJywgJ2FmT25UYWInLCAnYWZPblNwYWNlJywgJ2FmT25TaGlmdFRhYicsICdhZk9uVXAnLCAnYWZPbkRvd24nLCAnYWZPbkxlZnQnLCAnYWZPblJpZ2h0JywgJ2FmT25Ib21lJywgJ2FmT25FbmQnLCAnYWZPbktleURvd24nXSk7XG4gIH1cbn1cblxuXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgRGlnaVV0aWxLZXl1cEhhbmRsZXIgZXh0ZW5kcyBDb21wb25lbnRzLkRpZ2lVdGlsS2V5dXBIYW5kbGVyIHtcbiAgLyoqXG4gICAqIFZpZCBrZXl1cCBww6UgZXNjYXBlIEBlbiBBdCBrZXl1cCBvbiBlc2NhcGUga2V5XG4gICAqL1xuICBhZk9uRXNjOiBFdmVudEVtaXR0ZXI8Q3VzdG9tRXZlbnQ8S2V5Ym9hcmRFdmVudD4+O1xuICAvKipcbiAgICogVmlkIGtleXVwIHDDpSBlbnRlciBAZW4gQXQga2V5dXAgb24gZW50ZXIga2V5XG4gICAqL1xuICBhZk9uRW50ZXI6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxLZXlib2FyZEV2ZW50Pj47XG4gIC8qKlxuICAgKiBWaWQga2V5dXAgcMOlIHRhYiBAZW4gQXQga2V5dXAgb24gdGFiIGtleVxuICAgKi9cbiAgYWZPblRhYjogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PEtleWJvYXJkRXZlbnQ+PjtcbiAgLyoqXG4gICAqIFZpZCBrZXl1cCBww6Ugc3BhY2UgQGVuIEF0IGtleXVwIG9uIHNwYWNlIGtleVxuICAgKi9cbiAgYWZPblNwYWNlOiBFdmVudEVtaXR0ZXI8Q3VzdG9tRXZlbnQ8S2V5Ym9hcmRFdmVudD4+O1xuICAvKipcbiAgICogVmlkIGtleXVwIHDDpSBza2lmdCBvY2ggdGFiYiBAZW4gQXQga2V5dXAgb24gc2hpZnQgYW5kIHRhYiBrZXlcbiAgICovXG4gIGFmT25TaGlmdFRhYjogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PEtleWJvYXJkRXZlbnQ+PjtcbiAgLyoqXG4gICAqIFZpZCBrZXl1cCBww6UgcGlsIHVwcCBAZW4gQXQga2V5dXAgb24gdXAgYXJyb3cga2V5XG4gICAqL1xuICBhZk9uVXA6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxLZXlib2FyZEV2ZW50Pj47XG4gIC8qKlxuICAgKiBWaWQga2V5dXAgcMOlIHBpbCBuZXIgQGVuIEF0IGtleXVwIG9uIGRvd24gYXJyb3cga2V5XG4gICAqL1xuICBhZk9uRG93bjogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PEtleWJvYXJkRXZlbnQ+PjtcbiAgLyoqXG4gICAqIFZpZCBrZXl1cCBww6UgcGlsIHbDpG5zdGVyIEBlbiBBdCBrZXl1cCBvbiBsZWZ0IGFycm93IGtleVxuICAgKi9cbiAgYWZPbkxlZnQ6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxLZXlib2FyZEV2ZW50Pj47XG4gIC8qKlxuICAgKiBWaWQga2V5dXAgcMOlIHBpbCBow7ZnZXIgQGVuIEF0IGtleXVwIG9uIHJpZ2h0IGFycm93IGtleVxuICAgKi9cbiAgYWZPblJpZ2h0OiBFdmVudEVtaXR0ZXI8Q3VzdG9tRXZlbnQ8S2V5Ym9hcmRFdmVudD4+O1xuICAvKipcbiAgICogVmlkIGtleXVwIHDDpSBob21lIEBlbiBBdCBrZXl1cCBvbiBob21lIGtleVxuICAgKi9cbiAgYWZPbkhvbWU6IEV2ZW50RW1pdHRlcjxDdXN0b21FdmVudDxLZXlib2FyZEV2ZW50Pj47XG4gIC8qKlxuICAgKiBWaWQga2V5dXAgcMOlIGVuZCBAZW4gQXQga2V5dXAgb24gZW5kIGtleVxuICAgKi9cbiAgYWZPbkVuZDogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PEtleWJvYXJkRXZlbnQ+PjtcbiAgLyoqXG4gICAqIFZpZCBrZXl1cCBww6UgYWxsYSB0YW5nZW50ZXIgQGVuIEF0IGtleXVwIG9uIGFueSBrZXlcbiAgICovXG4gIGFmT25LZXlVcDogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PEtleWJvYXJkRXZlbnQ+PjtcblxufVxuXG5AUHJveHlDbXAoe1xuICBkZWZpbmVDdXN0b21FbGVtZW50Rm46IGRlZmluZURpZ2lVdGlsS2V5dXBIYW5kbGVyXG59KVxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGlnaS11dGlsLWtleXVwLWhhbmRsZXInLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaCxcbiAgdGVtcGxhdGU6ICc8bmctY29udGVudD48L25nLWNvbnRlbnQ+J1xufSlcbmV4cG9ydCBjbGFzcyBEaWdpVXRpbEtleXVwSGFuZGxlciB7XG4gIHByb3RlY3RlZCBlbDogSFRNTEVsZW1lbnQ7XG4gIGNvbnN0cnVjdG9yKGM6IENoYW5nZURldGVjdG9yUmVmLCByOiBFbGVtZW50UmVmLCBwcm90ZWN0ZWQgejogTmdab25lKSB7XG4gICAgYy5kZXRhY2goKTtcbiAgICB0aGlzLmVsID0gci5uYXRpdmVFbGVtZW50O1xuICAgIHByb3h5T3V0cHV0cyh0aGlzLCB0aGlzLmVsLCBbJ2FmT25Fc2MnLCAnYWZPbkVudGVyJywgJ2FmT25UYWInLCAnYWZPblNwYWNlJywgJ2FmT25TaGlmdFRhYicsICdhZk9uVXAnLCAnYWZPbkRvd24nLCAnYWZPbkxlZnQnLCAnYWZPblJpZ2h0JywgJ2FmT25Ib21lJywgJ2FmT25FbmQnLCAnYWZPbktleVVwJ10pO1xuICB9XG59XG5cblxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIERpZ2lVdGlsTXV0YXRpb25PYnNlcnZlciBleHRlbmRzIENvbXBvbmVudHMuRGlnaVV0aWxNdXRhdGlvbk9ic2VydmVyIHtcbiAgLyoqXG4gICAqIE7DpHIgRE9NLWVsZW1lbnQgbMOkZ2dzIHRpbGwgZWxsZXIgdGFzIGJvcnQgaW51dGkgTXV0YXRpb24gT2JzZXJ2ZXI6biBAZW4gV2hlbiBET00tZWxlbWVudHMgaXMgYWRkZWQgb3IgcmVtb3ZlZCBpbnNpZGUgdGhlIE11dGF0aW9uIE9ic2VydmVyXG4gICAqL1xuICBhZk9uQ2hhbmdlOiBFdmVudEVtaXR0ZXI8Q3VzdG9tRXZlbnQ8YW55Pj47XG5cbn1cblxuQFByb3h5Q21wKHtcbiAgZGVmaW5lQ3VzdG9tRWxlbWVudEZuOiBkZWZpbmVEaWdpVXRpbE11dGF0aW9uT2JzZXJ2ZXIsXG4gIGlucHV0czogWydhZklkJywgJ2FmT3B0aW9ucyddXG59KVxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGlnaS11dGlsLW11dGF0aW9uLW9ic2VydmVyJyxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXG4gIHRlbXBsYXRlOiAnPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PicsXG4gIGlucHV0czogWydhZklkJywgJ2FmT3B0aW9ucyddXG59KVxuZXhwb3J0IGNsYXNzIERpZ2lVdGlsTXV0YXRpb25PYnNlcnZlciB7XG4gIHByb3RlY3RlZCBlbDogSFRNTEVsZW1lbnQ7XG4gIGNvbnN0cnVjdG9yKGM6IENoYW5nZURldGVjdG9yUmVmLCByOiBFbGVtZW50UmVmLCBwcm90ZWN0ZWQgejogTmdab25lKSB7XG4gICAgYy5kZXRhY2goKTtcbiAgICB0aGlzLmVsID0gci5uYXRpdmVFbGVtZW50O1xuICAgIHByb3h5T3V0cHV0cyh0aGlzLCB0aGlzLmVsLCBbJ2FmT25DaGFuZ2UnXSk7XG4gIH1cbn1cblxuXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgRGlnaVV0aWxSZXNpemVPYnNlcnZlciBleHRlbmRzIENvbXBvbmVudHMuRGlnaVV0aWxSZXNpemVPYnNlcnZlciB7XG4gIC8qKlxuICAgKiBOw6RyIGtvbXBvbmVudGVuIMOkbmRyYXIgc3RvcmxlayBAZW4gV2hlbiB0aGUgY29tcG9uZW50IGNoYW5nZXMgc2l6ZVxuICAgKi9cbiAgYWZPbkNoYW5nZTogRXZlbnRFbWl0dGVyPEN1c3RvbUV2ZW50PFJlc2l6ZU9ic2VydmVyRW50cnk+PjtcblxufVxuXG5AUHJveHlDbXAoe1xuICBkZWZpbmVDdXN0b21FbGVtZW50Rm46IGRlZmluZURpZ2lVdGlsUmVzaXplT2JzZXJ2ZXJcbn0pXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdkaWdpLXV0aWwtcmVzaXplLW9ic2VydmVyJyxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXG4gIHRlbXBsYXRlOiAnPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50Pidcbn0pXG5leHBvcnQgY2xhc3MgRGlnaVV0aWxSZXNpemVPYnNlcnZlciB7XG4gIHByb3RlY3RlZCBlbDogSFRNTEVsZW1lbnQ7XG4gIGNvbnN0cnVjdG9yKGM6IENoYW5nZURldGVjdG9yUmVmLCByOiBFbGVtZW50UmVmLCBwcm90ZWN0ZWQgejogTmdab25lKSB7XG4gICAgYy5kZXRhY2goKTtcbiAgICB0aGlzLmVsID0gci5uYXRpdmVFbGVtZW50O1xuICAgIHByb3h5T3V0cHV0cyh0aGlzLCB0aGlzLmVsLCBbJ2FmT25DaGFuZ2UnXSk7XG4gIH1cbn1cbiJdfQ==
