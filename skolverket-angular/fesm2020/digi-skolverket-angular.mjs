import { __decorate, __metadata } from "tslib";
import * as i0 from "@angular/core";
import {
  ChangeDetectorRef,
  ElementRef,
  NgZone,
  Component,
  ChangeDetectionStrategy,
  Directive,
  HostListener,
  NgModule,
} from "@angular/core";
import { fromEvent } from "rxjs";
import { defineCustomElement as defineCustomElement$1 } from "../../skolverket/components/digi-button.js";
import { defineCustomElement as defineCustomElement$2 } from "../../skolverket/components/digi-calendar.js";
import { defineCustomElement as defineCustomElement$3 } from "../../skolverket/components/digi-calendar-week-view.js";
import { defineCustomElement as defineCustomElement$4 } from "../../skolverket/components/digi-card-box.js";
import { defineCustomElement as defineCustomElement$5 } from "../../skolverket/components/digi-card-link.js";
import { defineCustomElement as defineCustomElement$6 } from "../../skolverket/components/digi-code.js";
import { defineCustomElement as defineCustomElement$7 } from "../../skolverket/components/digi-code-block.js";
import { defineCustomElement as defineCustomElement$8 } from "../../skolverket/components/digi-code-example.js";
import { defineCustomElement as defineCustomElement$9 } from "../../skolverket/components/digi-dialog.js";
import { defineCustomElement as defineCustomElement$a } from "../../skolverket/components/digi-expandable-accordion.js";
import { defineCustomElement as defineCustomElement$b } from "../../skolverket/components/digi-form-checkbox.js";
import { defineCustomElement as defineCustomElement$c } from "../../skolverket/components/digi-form-fieldset.js";
import { defineCustomElement as defineCustomElement$d } from "../../skolverket/components/digi-form-file-upload.js";
import { defineCustomElement as defineCustomElement$e } from "../../skolverket/components/digi-form-filter.js";
import { defineCustomElement as defineCustomElement$f } from "../../skolverket/components/digi-form-input.js";
import { defineCustomElement as defineCustomElement$g } from "../../skolverket/components/digi-form-input-search.js";
import { defineCustomElement as defineCustomElement$h } from "../../skolverket/components/digi-form-label.js";
import { defineCustomElement as defineCustomElement$i } from "../../skolverket/components/digi-form-process-step.js";
import { defineCustomElement as defineCustomElement$j } from "../../skolverket/components/digi-form-process-steps.js";
import { defineCustomElement as defineCustomElement$k } from "../../skolverket/components/digi-form-radiobutton.js";
import { defineCustomElement as defineCustomElement$l } from "../../skolverket/components/digi-form-radiogroup.js";
import { defineCustomElement as defineCustomElement$m } from "../../skolverket/components/digi-form-select.js";
import { defineCustomElement as defineCustomElement$n } from "../../skolverket/components/digi-form-textarea.js";
import { defineCustomElement as defineCustomElement$o } from "../../skolverket/components/digi-form-validation-message.js";
import { defineCustomElement as defineCustomElement$p } from "../../skolverket/components/digi-icon.js";
import { defineCustomElement as defineCustomElement$q } from "../../skolverket/components/digi-icon-bars.js";
import { defineCustomElement as defineCustomElement$r } from "../../skolverket/components/digi-icon-check.js";
import { defineCustomElement as defineCustomElement$s } from "../../skolverket/components/digi-icon-check-circle.js";
import { defineCustomElement as defineCustomElement$t } from "../../skolverket/components/digi-icon-check-circle-reg.js";
import { defineCustomElement as defineCustomElement$u } from "../../skolverket/components/digi-icon-check-circle-reg-alt.js";
import { defineCustomElement as defineCustomElement$v } from "../../skolverket/components/digi-icon-chevron-down.js";
import { defineCustomElement as defineCustomElement$w } from "../../skolverket/components/digi-icon-chevron-left.js";
import { defineCustomElement as defineCustomElement$x } from "../../skolverket/components/digi-icon-chevron-right.js";
import { defineCustomElement as defineCustomElement$y } from "../../skolverket/components/digi-icon-chevron-up.js";
import { defineCustomElement as defineCustomElement$z } from "../../skolverket/components/digi-icon-copy.js";
import { defineCustomElement as defineCustomElement$A } from "../../skolverket/components/digi-icon-danger-outline.js";
import { defineCustomElement as defineCustomElement$B } from "../../skolverket/components/digi-icon-download.js";
import { defineCustomElement as defineCustomElement$C } from "../../skolverket/components/digi-icon-exclamation-circle.js";
import { defineCustomElement as defineCustomElement$D } from "../../skolverket/components/digi-icon-exclamation-circle-filled.js";
import { defineCustomElement as defineCustomElement$E } from "../../skolverket/components/digi-icon-exclamation-triangle.js";
import { defineCustomElement as defineCustomElement$F } from "../../skolverket/components/digi-icon-exclamation-triangle-warning.js";
import { defineCustomElement as defineCustomElement$G } from "../../skolverket/components/digi-icon-external-link-alt.js";
import { defineCustomElement as defineCustomElement$H } from "../../skolverket/components/digi-icon-minus.js";
import { defineCustomElement as defineCustomElement$I } from "../../skolverket/components/digi-icon-paperclip.js";
import { defineCustomElement as defineCustomElement$J } from "../../skolverket/components/digi-icon-plus.js";
import { defineCustomElement as defineCustomElement$K } from "../../skolverket/components/digi-icon-search.js";
import { defineCustomElement as defineCustomElement$L } from "../../skolverket/components/digi-icon-spinner.js";
import { defineCustomElement as defineCustomElement$M } from "../../skolverket/components/digi-icon-trash.js";
import { defineCustomElement as defineCustomElement$N } from "../../skolverket/components/digi-icon-x.js";
import { defineCustomElement as defineCustomElement$O } from "../../skolverket/components/digi-layout-block.js";
import { defineCustomElement as defineCustomElement$P } from "../../skolverket/components/digi-layout-columns.js";
import { defineCustomElement as defineCustomElement$Q } from "../../skolverket/components/digi-layout-container.js";
import { defineCustomElement as defineCustomElement$R } from "../../skolverket/components/digi-layout-grid.js";
import { defineCustomElement as defineCustomElement$S } from "../../skolverket/components/digi-layout-media-object.js";
import { defineCustomElement as defineCustomElement$T } from "../../skolverket/components/digi-layout-rows.js";
import { defineCustomElement as defineCustomElement$U } from "../../skolverket/components/digi-layout-stacked-blocks.js";
import { defineCustomElement as defineCustomElement$V } from "../../skolverket/components/digi-link.js";
import { defineCustomElement as defineCustomElement$W } from "../../skolverket/components/digi-link-external.js";
import { defineCustomElement as defineCustomElement$X } from "../../skolverket/components/digi-link-icon.js";
import { defineCustomElement as defineCustomElement$Y } from "../../skolverket/components/digi-link-internal.js";
import { defineCustomElement as defineCustomElement$Z } from "../../skolverket/components/digi-list-link.js";
import { defineCustomElement as defineCustomElement$_ } from "../../skolverket/components/digi-loader-spinner.js";
import { defineCustomElement as defineCustomElement$$ } from "../../skolverket/components/digi-logo.js";
import { defineCustomElement as defineCustomElement$10 } from "../../skolverket/components/digi-logo-service.js";
import { defineCustomElement as defineCustomElement$11 } from "../../skolverket/components/digi-logo-sister.js";
import { defineCustomElement as defineCustomElement$12 } from "../../skolverket/components/digi-media-figure.js";
import { defineCustomElement as defineCustomElement$13 } from "../../skolverket/components/digi-media-image.js";
import { defineCustomElement as defineCustomElement$14 } from "../../skolverket/components/digi-navigation-breadcrumbs.js";
import { defineCustomElement as defineCustomElement$15 } from "../../skolverket/components/digi-navigation-context-menu.js";
import { defineCustomElement as defineCustomElement$16 } from "../../skolverket/components/digi-navigation-context-menu-item.js";
import { defineCustomElement as defineCustomElement$17 } from "../../skolverket/components/digi-navigation-main-menu.js";
import { defineCustomElement as defineCustomElement$18 } from "../../skolverket/components/digi-navigation-main-menu-panel.js";
import { defineCustomElement as defineCustomElement$19 } from "../../skolverket/components/digi-navigation-pagination.js";
import { defineCustomElement as defineCustomElement$1a } from "../../skolverket/components/digi-navigation-sidebar.js";
import { defineCustomElement as defineCustomElement$1b } from "../../skolverket/components/digi-navigation-sidebar-button.js";
import { defineCustomElement as defineCustomElement$1c } from "../../skolverket/components/digi-navigation-tab.js";
import { defineCustomElement as defineCustomElement$1d } from "../../skolverket/components/digi-navigation-tab-in-a-box.js";
import { defineCustomElement as defineCustomElement$1e } from "../../skolverket/components/digi-navigation-tabs.js";
import { defineCustomElement as defineCustomElement$1f } from "../../skolverket/components/digi-navigation-toc.js";
import { defineCustomElement as defineCustomElement$1g } from "../../skolverket/components/digi-navigation-vertical-menu.js";
import { defineCustomElement as defineCustomElement$1h } from "../../skolverket/components/digi-navigation-vertical-menu-item.js";
import { defineCustomElement as defineCustomElement$1i } from "../../skolverket/components/digi-notification-alert.js";
import { defineCustomElement as defineCustomElement$1j } from "../../skolverket/components/digi-notification-cookie.js";
import { defineCustomElement as defineCustomElement$1k } from "../../skolverket/components/digi-notification-detail.js";
import { defineCustomElement as defineCustomElement$1l } from "../../skolverket/components/digi-page.js";
import { defineCustomElement as defineCustomElement$1m } from "../../skolverket/components/digi-page-block-cards.js";
import { defineCustomElement as defineCustomElement$1n } from "../../skolverket/components/digi-page-block-hero.js";
import { defineCustomElement as defineCustomElement$1o } from "../../skolverket/components/digi-page-block-lists.js";
import { defineCustomElement as defineCustomElement$1p } from "../../skolverket/components/digi-page-block-sidebar.js";
import { defineCustomElement as defineCustomElement$1q } from "../../skolverket/components/digi-page-footer.js";
import { defineCustomElement as defineCustomElement$1r } from "../../skolverket/components/digi-page-header.js";
import { defineCustomElement as defineCustomElement$1s } from "../../skolverket/components/digi-progress-step.js";
import { defineCustomElement as defineCustomElement$1t } from "../../skolverket/components/digi-progress-steps.js";
import { defineCustomElement as defineCustomElement$1u } from "../../skolverket/components/digi-progressbar.js";
import { defineCustomElement as defineCustomElement$1v } from "../../skolverket/components/digi-table.js";
import { defineCustomElement as defineCustomElement$1w } from "../../skolverket/components/digi-tag.js";
import { defineCustomElement as defineCustomElement$1x } from "../../skolverket/components/digi-typography.js";
import { defineCustomElement as defineCustomElement$1y } from "../../skolverket/components/digi-typography-heading-section.js";
import { defineCustomElement as defineCustomElement$1z } from "../../skolverket/components/digi-typography-meta.js";
import { defineCustomElement as defineCustomElement$1A } from "../../skolverket/components/digi-typography-preamble.js";
import { defineCustomElement as defineCustomElement$1B } from "../../skolverket/components/digi-typography-time.js";
import { defineCustomElement as defineCustomElement$1C } from "../../skolverket/components/digi-util-breakpoint-observer.js";
import { defineCustomElement as defineCustomElement$1D } from "../../skolverket/components/digi-util-detect-click-outside.js";
import { defineCustomElement as defineCustomElement$1E } from "../../skolverket/components/digi-util-detect-focus-outside.js";
import { defineCustomElement as defineCustomElement$1F } from "../../skolverket/components/digi-util-intersection-observer.js";
import { defineCustomElement as defineCustomElement$1G } from "../../skolverket/components/digi-util-keydown-handler.js";
import { defineCustomElement as defineCustomElement$1H } from "../../skolverket/components/digi-util-keyup-handler.js";
import { defineCustomElement as defineCustomElement$1I } from "../../skolverket/components/digi-util-mutation-observer.js";
import { defineCustomElement as defineCustomElement$1J } from "../../skolverket/components/digi-util-resize-observer.js";
import { NG_VALUE_ACCESSOR } from "@angular/forms";

/* eslint-disable */
const proxyInputs = (Cmp, inputs) => {
  const Prototype = Cmp.prototype;
  inputs.forEach((item) => {
    Object.defineProperty(Prototype, item, {
      get() {
        return this.el[item];
      },
      set(val) {
        this.z.runOutsideAngular(() => (this.el[item] = val));
      },
    });
  });
};
const proxyMethods = (Cmp, methods) => {
  const Prototype = Cmp.prototype;
  methods.forEach((methodName) => {
    Prototype[methodName] = function () {
      const args = arguments;
      return this.z.runOutsideAngular(() =>
        this.el[methodName].apply(this.el, args)
      );
    };
  });
};
const proxyOutputs = (instance, el, events) => {
  events.forEach(
    (eventName) => (instance[eventName] = fromEvent(el, eventName))
  );
};
const defineCustomElement = (tagName, customElement) => {
  if (
    customElement !== undefined &&
    typeof customElements !== "undefined" &&
    !customElements.get(tagName)
  ) {
    customElements.define(tagName, customElement);
  }
};
// tslint:disable-next-line: only-arrow-functions
function ProxyCmp(opts) {
  const decorator = function (cls) {
    const { defineCustomElementFn, inputs, methods } = opts;
    if (defineCustomElementFn !== undefined) {
      defineCustomElementFn();
    }
    if (inputs) {
      proxyInputs(cls, inputs);
    }
    if (methods) {
      proxyMethods(cls, methods);
    }
    return cls;
  };
  return decorator;
}

let DigiButton = class DigiButton {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnClick", "afOnFocus", "afOnBlur"]);
  }
};
DigiButton.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiButton,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiButton.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiButton,
  selector: "digi-button",
  inputs: {
    afAriaControls: "afAriaControls",
    afAriaExpanded: "afAriaExpanded",
    afAriaHaspopup: "afAriaHaspopup",
    afAriaLabel: "afAriaLabel",
    afAriaLabelledby: "afAriaLabelledby",
    afAriaPressed: "afAriaPressed",
    afAutofocus: "afAutofocus",
    afDir: "afDir",
    afForm: "afForm",
    afFullWidth: "afFullWidth",
    afId: "afId",
    afLang: "afLang",
    afSize: "afSize",
    afTabindex: "afTabindex",
    afType: "afType",
    afVariation: "afVariation",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiButton = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$1,
      inputs: [
        "afAriaControls",
        "afAriaExpanded",
        "afAriaHaspopup",
        "afAriaLabel",
        "afAriaLabelledby",
        "afAriaPressed",
        "afAutofocus",
        "afDir",
        "afForm",
        "afFullWidth",
        "afId",
        "afLang",
        "afSize",
        "afTabindex",
        "afType",
        "afVariation",
      ],
      methods: ["afMGetButtonElement"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiButton
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiButton,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-button",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afAriaControls",
            "afAriaExpanded",
            "afAriaHaspopup",
            "afAriaLabel",
            "afAriaLabelledby",
            "afAriaPressed",
            "afAutofocus",
            "afDir",
            "afForm",
            "afFullWidth",
            "afId",
            "afLang",
            "afSize",
            "afTabindex",
            "afType",
            "afVariation",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiCalendar = class DigiCalendar {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, [
      "afOnDateSelectedChange",
      "afOnFocusOutside",
      "afOnClickOutside",
      "afOnDirty",
    ]);
  }
};
DigiCalendar.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiCalendar,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiCalendar.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiCalendar,
  selector: "digi-calendar",
  inputs: {
    afActive: "afActive",
    afId: "afId",
    afInitSelectedMonth: "afInitSelectedMonth",
    afMultipleDates: "afMultipleDates",
    afSelectedDate: "afSelectedDate",
    afShowWeekNumber: "afShowWeekNumber",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiCalendar = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$2,
      inputs: [
        "afActive",
        "afId",
        "afInitSelectedMonth",
        "afMultipleDates",
        "afSelectedDate",
        "afShowWeekNumber",
      ],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiCalendar
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiCalendar,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-calendar",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afActive",
            "afId",
            "afInitSelectedMonth",
            "afMultipleDates",
            "afSelectedDate",
            "afShowWeekNumber",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiCalendarWeekView = class DigiCalendarWeekView {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnWeekChange", "afOnDateChange"]);
  }
};
DigiCalendarWeekView.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiCalendarWeekView,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiCalendarWeekView.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiCalendarWeekView,
  selector: "digi-calendar-week-view",
  inputs: {
    afDates: "afDates",
    afHeadingLevel: "afHeadingLevel",
    afId: "afId",
    afMaxWeek: "afMaxWeek",
    afMinWeek: "afMinWeek",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiCalendarWeekView = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$3,
      inputs: ["afDates", "afHeadingLevel", "afId", "afMaxWeek", "afMinWeek"],
      methods: ["afMSetActiveDate"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiCalendarWeekView
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiCalendarWeekView,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-calendar-week-view",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afDates",
            "afHeadingLevel",
            "afId",
            "afMaxWeek",
            "afMinWeek",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiCardBox = class DigiCardBox {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiCardBox.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiCardBox,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiCardBox.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiCardBox,
  selector: "digi-card-box",
  inputs: { afGutter: "afGutter", afWidth: "afWidth" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiCardBox = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$4,
      inputs: ["afGutter", "afWidth"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiCardBox
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiCardBox,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-card-box",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afGutter", "afWidth"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiCardLink = class DigiCardLink {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiCardLink.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiCardLink,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiCardLink.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiCardLink,
  selector: "digi-card-link",
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiCardLink = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$5,
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiCardLink
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiCardLink,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-card-link",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiCode = class DigiCode {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiCode.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiCode,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiCode.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiCode,
  selector: "digi-code",
  inputs: {
    afCode: "afCode",
    afLang: "afLang",
    afLanguage: "afLanguage",
    afVariation: "afVariation",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiCode = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$6,
      inputs: ["afCode", "afLang", "afLanguage", "afVariation"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiCode
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiCode,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-code",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afCode", "afLang", "afLanguage", "afVariation"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiCodeBlock = class DigiCodeBlock {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiCodeBlock.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiCodeBlock,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiCodeBlock.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiCodeBlock,
  selector: "digi-code-block",
  inputs: {
    afCode: "afCode",
    afHideToolbar: "afHideToolbar",
    afLang: "afLang",
    afLanguage: "afLanguage",
    afVariation: "afVariation",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiCodeBlock = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$7,
      inputs: [
        "afCode",
        "afHideToolbar",
        "afLang",
        "afLanguage",
        "afVariation",
      ],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiCodeBlock
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiCodeBlock,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-code-block",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afCode",
            "afHideToolbar",
            "afLang",
            "afLanguage",
            "afVariation",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiCodeExample = class DigiCodeExample {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiCodeExample.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiCodeExample,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiCodeExample.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiCodeExample,
  selector: "digi-code-example",
  inputs: {
    afCode: "afCode",
    afCodeBlockVariation: "afCodeBlockVariation",
    afExampleVariation: "afExampleVariation",
    afHideCode: "afHideCode",
    afHideControls: "afHideControls",
    afLanguage: "afLanguage",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiCodeExample = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$8,
      inputs: [
        "afCode",
        "afCodeBlockVariation",
        "afExampleVariation",
        "afHideCode",
        "afHideControls",
        "afLanguage",
      ],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiCodeExample
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiCodeExample,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-code-example",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afCode",
            "afCodeBlockVariation",
            "afExampleVariation",
            "afHideCode",
            "afHideControls",
            "afLanguage",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiDialog = class DigiDialog {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnOpen", "afOnClose"]);
  }
};
DigiDialog.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiDialog,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiDialog.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiDialog,
  selector: "digi-dialog",
  inputs: { afHideCloseButton: "afHideCloseButton", afOpen: "afOpen" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiDialog = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$9,
      inputs: ["afHideCloseButton", "afOpen"],
      methods: ["showModal", "close"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiDialog
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiDialog,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-dialog",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afHideCloseButton", "afOpen"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiExpandableAccordion = class DigiExpandableAccordion {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnClick"]);
  }
};
DigiExpandableAccordion.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiExpandableAccordion,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiExpandableAccordion.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiExpandableAccordion,
  selector: "digi-expandable-accordion",
  inputs: {
    afAnimation: "afAnimation",
    afExpanded: "afExpanded",
    afHeading: "afHeading",
    afHeadingLevel: "afHeadingLevel",
    afId: "afId",
    afVariation: "afVariation",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiExpandableAccordion = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$a,
      inputs: [
        "afAnimation",
        "afExpanded",
        "afHeading",
        "afHeadingLevel",
        "afId",
        "afVariation",
      ],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiExpandableAccordion
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiExpandableAccordion,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-expandable-accordion",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afAnimation",
            "afExpanded",
            "afHeading",
            "afHeadingLevel",
            "afId",
            "afVariation",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiFormCheckbox = class DigiFormCheckbox {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, [
      "afOnChange",
      "afOnBlur",
      "afOnFocus",
      "afOnFocusout",
      "afOnInput",
      "afOnDirty",
      "afOnTouched",
    ]);
  }
};
DigiFormCheckbox.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormCheckbox,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiFormCheckbox.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiFormCheckbox,
  selector: "digi-form-checkbox",
  inputs: {
    afAriaDescribedby: "afAriaDescribedby",
    afAriaLabel: "afAriaLabel",
    afAriaLabelledby: "afAriaLabelledby",
    afAutofocus: "afAutofocus",
    afChecked: "afChecked",
    afDescription: "afDescription",
    afId: "afId",
    afIndeterminate: "afIndeterminate",
    afLabel: "afLabel",
    afLayout: "afLayout",
    afName: "afName",
    afRequired: "afRequired",
    afValidation: "afValidation",
    afValue: "afValue",
    afVariation: "afVariation",
    checked: "checked",
    value: "value",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiFormCheckbox = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$b,
      inputs: [
        "afAriaDescribedby",
        "afAriaLabel",
        "afAriaLabelledby",
        "afAutofocus",
        "afChecked",
        "afDescription",
        "afId",
        "afIndeterminate",
        "afLabel",
        "afLayout",
        "afName",
        "afRequired",
        "afValidation",
        "afValue",
        "afVariation",
        "checked",
        "value",
      ],
      methods: ["afMGetFormControlElement"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiFormCheckbox
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormCheckbox,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-form-checkbox",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afAriaDescribedby",
            "afAriaLabel",
            "afAriaLabelledby",
            "afAutofocus",
            "afChecked",
            "afDescription",
            "afId",
            "afIndeterminate",
            "afLabel",
            "afLayout",
            "afName",
            "afRequired",
            "afValidation",
            "afValue",
            "afVariation",
            "checked",
            "value",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiFormFieldset = class DigiFormFieldset {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiFormFieldset.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormFieldset,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiFormFieldset.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiFormFieldset,
  selector: "digi-form-fieldset",
  inputs: {
    afForm: "afForm",
    afId: "afId",
    afLegend: "afLegend",
    afName: "afName",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiFormFieldset = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$c,
      inputs: ["afForm", "afId", "afLegend", "afName"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiFormFieldset
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormFieldset,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-form-fieldset",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afForm", "afId", "afLegend", "afName"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiFormFileUpload = class DigiFormFileUpload {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, [
      "afOnUploadFile",
      "afOnRemoveFile",
      "afOnCancelFile",
      "afOnRetryFile",
    ]);
  }
};
DigiFormFileUpload.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormFileUpload,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiFormFileUpload.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiFormFileUpload,
  selector: "digi-form-file-upload",
  inputs: {
    afAnnounceIfOptional: "afAnnounceIfOptional",
    afAnnounceIfOptionalText: "afAnnounceIfOptionalText",
    afFileMaxSize: "afFileMaxSize",
    afFileTypes: "afFileTypes",
    afHeadingFiles: "afHeadingFiles",
    afHeadingLevel: "afHeadingLevel",
    afId: "afId",
    afLabel: "afLabel",
    afLabelDescription: "afLabelDescription",
    afMaxFiles: "afMaxFiles",
    afName: "afName",
    afRequired: "afRequired",
    afRequiredText: "afRequiredText",
    afUploadBtnText: "afUploadBtnText",
    afValidation: "afValidation",
    afVariation: "afVariation",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiFormFileUpload = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$d,
      inputs: [
        "afAnnounceIfOptional",
        "afAnnounceIfOptionalText",
        "afFileMaxSize",
        "afFileTypes",
        "afHeadingFiles",
        "afHeadingLevel",
        "afId",
        "afLabel",
        "afLabelDescription",
        "afMaxFiles",
        "afName",
        "afRequired",
        "afRequiredText",
        "afUploadBtnText",
        "afValidation",
        "afVariation",
      ],
      methods: [
        "afMValidateFile",
        "afMGetAllFiles",
        "afMImportFiles",
        "afMGetFormControlElement",
      ],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiFormFileUpload
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormFileUpload,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-form-file-upload",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afAnnounceIfOptional",
            "afAnnounceIfOptionalText",
            "afFileMaxSize",
            "afFileTypes",
            "afHeadingFiles",
            "afHeadingLevel",
            "afId",
            "afLabel",
            "afLabelDescription",
            "afMaxFiles",
            "afName",
            "afRequired",
            "afRequiredText",
            "afUploadBtnText",
            "afValidation",
            "afVariation",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiFormFilter = class DigiFormFilter {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, [
      "afOnFocusout",
      "afOnSubmitFilters",
      "afOnFilterClosed",
      "afOnResetFilters",
      "afOnChange",
    ]);
  }
};
DigiFormFilter.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormFilter,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiFormFilter.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiFormFilter,
  selector: "digi-form-filter",
  inputs: {
    afAlignRight: "afAlignRight",
    afAutofocus: "afAutofocus",
    afFilterButtonAriaDescribedby: "afFilterButtonAriaDescribedby",
    afFilterButtonAriaLabel: "afFilterButtonAriaLabel",
    afFilterButtonText: "afFilterButtonText",
    afHideResetButton: "afHideResetButton",
    afId: "afId",
    afName: "afName",
    afResetButtonText: "afResetButtonText",
    afResetButtonTextAriaLabel: "afResetButtonTextAriaLabel",
    afSubmitButtonText: "afSubmitButtonText",
    afSubmitButtonTextAriaLabel: "afSubmitButtonTextAriaLabel",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiFormFilter = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$e,
      inputs: [
        "afAlignRight",
        "afAutofocus",
        "afFilterButtonAriaDescribedby",
        "afFilterButtonAriaLabel",
        "afFilterButtonText",
        "afHideResetButton",
        "afId",
        "afName",
        "afResetButtonText",
        "afResetButtonTextAriaLabel",
        "afSubmitButtonText",
        "afSubmitButtonTextAriaLabel",
      ],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiFormFilter
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormFilter,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-form-filter",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afAlignRight",
            "afAutofocus",
            "afFilterButtonAriaDescribedby",
            "afFilterButtonAriaLabel",
            "afFilterButtonText",
            "afHideResetButton",
            "afId",
            "afName",
            "afResetButtonText",
            "afResetButtonTextAriaLabel",
            "afSubmitButtonText",
            "afSubmitButtonTextAriaLabel",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiFormInput = class DigiFormInput {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, [
      "afOnChange",
      "afOnBlur",
      "afOnKeyup",
      "afOnFocus",
      "afOnFocusout",
      "afOnInput",
      "afOnDirty",
      "afOnTouched",
    ]);
  }
};
DigiFormInput.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormInput,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiFormInput.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiFormInput,
  selector: "digi-form-input",
  inputs: {
    afAnnounceIfOptional: "afAnnounceIfOptional",
    afAnnounceIfOptionalText: "afAnnounceIfOptionalText",
    afAriaActivedescendant: "afAriaActivedescendant",
    afAriaAutocomplete: "afAriaAutocomplete",
    afAriaDescribedby: "afAriaDescribedby",
    afAriaLabelledby: "afAriaLabelledby",
    afAutocomplete: "afAutocomplete",
    afAutofocus: "afAutofocus",
    afButtonVariation: "afButtonVariation",
    afDirname: "afDirname",
    afId: "afId",
    afLabel: "afLabel",
    afLabelDescription: "afLabelDescription",
    afList: "afList",
    afMax: "afMax",
    afMaxlength: "afMaxlength",
    afMin: "afMin",
    afMinlength: "afMinlength",
    afName: "afName",
    afRequired: "afRequired",
    afRequiredText: "afRequiredText",
    afRole: "afRole",
    afStep: "afStep",
    afType: "afType",
    afValidation: "afValidation",
    afValidationText: "afValidationText",
    afValue: "afValue",
    afVariation: "afVariation",
    value: "value",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiFormInput = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$f,
      inputs: [
        "afAnnounceIfOptional",
        "afAnnounceIfOptionalText",
        "afAriaActivedescendant",
        "afAriaAutocomplete",
        "afAriaDescribedby",
        "afAriaLabelledby",
        "afAutocomplete",
        "afAutofocus",
        "afButtonVariation",
        "afDirname",
        "afId",
        "afLabel",
        "afLabelDescription",
        "afList",
        "afMax",
        "afMaxlength",
        "afMin",
        "afMinlength",
        "afName",
        "afRequired",
        "afRequiredText",
        "afRole",
        "afStep",
        "afType",
        "afValidation",
        "afValidationText",
        "afValue",
        "afVariation",
        "value",
      ],
      methods: ["afMGetFormControlElement"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiFormInput
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormInput,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-form-input",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afAnnounceIfOptional",
            "afAnnounceIfOptionalText",
            "afAriaActivedescendant",
            "afAriaAutocomplete",
            "afAriaDescribedby",
            "afAriaLabelledby",
            "afAutocomplete",
            "afAutofocus",
            "afButtonVariation",
            "afDirname",
            "afId",
            "afLabel",
            "afLabelDescription",
            "afList",
            "afMax",
            "afMaxlength",
            "afMin",
            "afMinlength",
            "afName",
            "afRequired",
            "afRequiredText",
            "afRole",
            "afStep",
            "afType",
            "afValidation",
            "afValidationText",
            "afValue",
            "afVariation",
            "value",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiFormInputSearch = class DigiFormInputSearch {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, [
      "afOnFocusOutside",
      "afOnFocusInside",
      "afOnClickOutside",
      "afOnClickInside",
      "afOnChange",
      "afOnBlur",
      "afOnKeyup",
      "afOnFocus",
      "afOnFocusout",
      "afOnInput",
      "afOnClick",
    ]);
  }
};
DigiFormInputSearch.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormInputSearch,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiFormInputSearch.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiFormInputSearch,
  selector: "digi-form-input-search",
  inputs: {
    afAriaActivedescendant: "afAriaActivedescendant",
    afAriaAutocomplete: "afAriaAutocomplete",
    afAriaDescribedby: "afAriaDescribedby",
    afAriaLabelledby: "afAriaLabelledby",
    afAutocomplete: "afAutocomplete",
    afAutofocus: "afAutofocus",
    afButtonAriaLabel: "afButtonAriaLabel",
    afButtonAriaLabelledby: "afButtonAriaLabelledby",
    afButtonText: "afButtonText",
    afButtonType: "afButtonType",
    afButtonVariation: "afButtonVariation",
    afHideButton: "afHideButton",
    afId: "afId",
    afLabel: "afLabel",
    afLabelDescription: "afLabelDescription",
    afName: "afName",
    afType: "afType",
    afValue: "afValue",
    afVariation: "afVariation",
    value: "value",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiFormInputSearch = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$g,
      inputs: [
        "afAriaActivedescendant",
        "afAriaAutocomplete",
        "afAriaDescribedby",
        "afAriaLabelledby",
        "afAutocomplete",
        "afAutofocus",
        "afButtonAriaLabel",
        "afButtonAriaLabelledby",
        "afButtonText",
        "afButtonType",
        "afButtonVariation",
        "afHideButton",
        "afId",
        "afLabel",
        "afLabelDescription",
        "afName",
        "afType",
        "afValue",
        "afVariation",
        "value",
      ],
      methods: ["afMGetFormControlElement"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiFormInputSearch
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormInputSearch,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-form-input-search",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afAriaActivedescendant",
            "afAriaAutocomplete",
            "afAriaDescribedby",
            "afAriaLabelledby",
            "afAutocomplete",
            "afAutofocus",
            "afButtonAriaLabel",
            "afButtonAriaLabelledby",
            "afButtonText",
            "afButtonType",
            "afButtonVariation",
            "afHideButton",
            "afId",
            "afLabel",
            "afLabelDescription",
            "afName",
            "afType",
            "afValue",
            "afVariation",
            "value",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiFormLabel = class DigiFormLabel {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiFormLabel.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormLabel,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiFormLabel.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiFormLabel,
  selector: "digi-form-label",
  inputs: {
    afAnnounceIfOptional: "afAnnounceIfOptional",
    afAnnounceIfOptionalText: "afAnnounceIfOptionalText",
    afDescription: "afDescription",
    afFor: "afFor",
    afId: "afId",
    afLabel: "afLabel",
    afRequired: "afRequired",
    afRequiredText: "afRequiredText",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiFormLabel = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$h,
      inputs: [
        "afAnnounceIfOptional",
        "afAnnounceIfOptionalText",
        "afDescription",
        "afFor",
        "afId",
        "afLabel",
        "afRequired",
        "afRequiredText",
      ],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiFormLabel
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormLabel,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-form-label",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afAnnounceIfOptional",
            "afAnnounceIfOptionalText",
            "afDescription",
            "afFor",
            "afId",
            "afLabel",
            "afRequired",
            "afRequiredText",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiFormProcessStep = class DigiFormProcessStep {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afClick"]);
  }
};
DigiFormProcessStep.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormProcessStep,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiFormProcessStep.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiFormProcessStep,
  selector: "digi-form-process-step",
  inputs: {
    afContext: "afContext",
    afHref: "afHref",
    afLabel: "afLabel",
    afType: "afType",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiFormProcessStep = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$i,
      inputs: ["afContext", "afHref", "afLabel", "afType"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiFormProcessStep
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormProcessStep,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-form-process-step",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afContext", "afHref", "afLabel", "afType"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiFormProcessSteps = class DigiFormProcessSteps {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiFormProcessSteps.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormProcessSteps,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiFormProcessSteps.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiFormProcessSteps,
  selector: "digi-form-process-steps",
  inputs: { afCurrentStep: "afCurrentStep", afId: "afId" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiFormProcessSteps = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$j,
      inputs: ["afCurrentStep", "afId"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiFormProcessSteps
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormProcessSteps,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-form-process-steps",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afCurrentStep", "afId"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiFormRadiobutton = class DigiFormRadiobutton {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, [
      "afOnChange",
      "afOnBlur",
      "afOnFocus",
      "afOnFocusout",
      "afOnInput",
      "afOnDirty",
      "afOnTouched",
    ]);
  }
};
DigiFormRadiobutton.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormRadiobutton,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiFormRadiobutton.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiFormRadiobutton,
  selector: "digi-form-radiobutton",
  inputs: {
    afAriaDescribedby: "afAriaDescribedby",
    afAriaLabelledby: "afAriaLabelledby",
    afAutofocus: "afAutofocus",
    afChecked: "afChecked",
    afId: "afId",
    afLabel: "afLabel",
    afLayout: "afLayout",
    afName: "afName",
    afRequired: "afRequired",
    afValidation: "afValidation",
    afValue: "afValue",
    afVariation: "afVariation",
    checked: "checked",
    value: "value",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiFormRadiobutton = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$k,
      inputs: [
        "afAriaDescribedby",
        "afAriaLabelledby",
        "afAutofocus",
        "afChecked",
        "afId",
        "afLabel",
        "afLayout",
        "afName",
        "afRequired",
        "afValidation",
        "afValue",
        "afVariation",
        "checked",
        "value",
      ],
      methods: ["afMGetFormControlElement"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiFormRadiobutton
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormRadiobutton,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-form-radiobutton",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afAriaDescribedby",
            "afAriaLabelledby",
            "afAutofocus",
            "afChecked",
            "afId",
            "afLabel",
            "afLayout",
            "afName",
            "afRequired",
            "afValidation",
            "afValue",
            "afVariation",
            "checked",
            "value",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiFormRadiogroup = class DigiFormRadiogroup {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnGroupChange"]);
  }
};
DigiFormRadiogroup.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormRadiogroup,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiFormRadiogroup.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiFormRadiogroup,
  selector: "digi-form-radiogroup",
  inputs: { afName: "afName", afValue: "afValue", value: "value" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiFormRadiogroup = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$l,
      inputs: ["afName", "afValue", "value"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiFormRadiogroup
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormRadiogroup,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-form-radiogroup",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afName", "afValue", "value"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiFormSelect = class DigiFormSelect {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, [
      "afOnChange",
      "afOnFocus",
      "afOnFocusout",
      "afOnBlur",
      "afOnDirty",
    ]);
  }
};
DigiFormSelect.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormSelect,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiFormSelect.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiFormSelect,
  selector: "digi-form-select",
  inputs: {
    afAnnounceIfOptional: "afAnnounceIfOptional",
    afAnnounceIfOptionalText: "afAnnounceIfOptionalText",
    afAutofocus: "afAutofocus",
    afDescription: "afDescription",
    afDisableValidation: "afDisableValidation",
    afId: "afId",
    afLabel: "afLabel",
    afName: "afName",
    afPlaceholder: "afPlaceholder",
    afRequired: "afRequired",
    afRequiredText: "afRequiredText",
    afStartSelected: "afStartSelected",
    afValidation: "afValidation",
    afValidationText: "afValidationText",
    afValue: "afValue",
    afVariation: "afVariation",
    value: "value",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiFormSelect = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$m,
      inputs: [
        "afAnnounceIfOptional",
        "afAnnounceIfOptionalText",
        "afAutofocus",
        "afDescription",
        "afDisableValidation",
        "afId",
        "afLabel",
        "afName",
        "afPlaceholder",
        "afRequired",
        "afRequiredText",
        "afStartSelected",
        "afValidation",
        "afValidationText",
        "afValue",
        "afVariation",
        "value",
      ],
      methods: ["afMGetFormControlElement"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiFormSelect
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormSelect,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-form-select",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afAnnounceIfOptional",
            "afAnnounceIfOptionalText",
            "afAutofocus",
            "afDescription",
            "afDisableValidation",
            "afId",
            "afLabel",
            "afName",
            "afPlaceholder",
            "afRequired",
            "afRequiredText",
            "afStartSelected",
            "afValidation",
            "afValidationText",
            "afValue",
            "afVariation",
            "value",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiFormTextarea = class DigiFormTextarea {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, [
      "afOnChange",
      "afOnBlur",
      "afOnKeyup",
      "afOnFocus",
      "afOnFocusout",
      "afOnInput",
      "afOnDirty",
      "afOnTouched",
    ]);
  }
};
DigiFormTextarea.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormTextarea,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiFormTextarea.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiFormTextarea,
  selector: "digi-form-textarea",
  inputs: {
    afAnnounceIfOptional: "afAnnounceIfOptional",
    afAnnounceIfOptionalText: "afAnnounceIfOptionalText",
    afAriaActivedescendant: "afAriaActivedescendant",
    afAriaDescribedby: "afAriaDescribedby",
    afAriaLabelledby: "afAriaLabelledby",
    afAutofocus: "afAutofocus",
    afId: "afId",
    afLabel: "afLabel",
    afLabelDescription: "afLabelDescription",
    afMaxlength: "afMaxlength",
    afMinlength: "afMinlength",
    afName: "afName",
    afRequired: "afRequired",
    afRequiredText: "afRequiredText",
    afRole: "afRole",
    afValidation: "afValidation",
    afValidationText: "afValidationText",
    afValue: "afValue",
    afVariation: "afVariation",
    value: "value",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiFormTextarea = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$n,
      inputs: [
        "afAnnounceIfOptional",
        "afAnnounceIfOptionalText",
        "afAriaActivedescendant",
        "afAriaDescribedby",
        "afAriaLabelledby",
        "afAutofocus",
        "afId",
        "afLabel",
        "afLabelDescription",
        "afMaxlength",
        "afMinlength",
        "afName",
        "afRequired",
        "afRequiredText",
        "afRole",
        "afValidation",
        "afValidationText",
        "afValue",
        "afVariation",
        "value",
      ],
      methods: ["afMGetFormControlElement"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiFormTextarea
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormTextarea,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-form-textarea",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afAnnounceIfOptional",
            "afAnnounceIfOptionalText",
            "afAriaActivedescendant",
            "afAriaDescribedby",
            "afAriaLabelledby",
            "afAutofocus",
            "afId",
            "afLabel",
            "afLabelDescription",
            "afMaxlength",
            "afMinlength",
            "afName",
            "afRequired",
            "afRequiredText",
            "afRole",
            "afValidation",
            "afValidationText",
            "afValue",
            "afVariation",
            "value",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiFormValidationMessage = class DigiFormValidationMessage {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiFormValidationMessage.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormValidationMessage,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiFormValidationMessage.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiFormValidationMessage,
  selector: "digi-form-validation-message",
  inputs: { afVariation: "afVariation" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiFormValidationMessage = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$o,
      inputs: ["afVariation"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiFormValidationMessage
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiFormValidationMessage,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-form-validation-message",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afVariation"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIcon = class DigiIcon {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIcon.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIcon,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIcon.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIcon,
  selector: "digi-icon",
  inputs: {
    afDesc: "afDesc",
    afName: "afName",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIcon = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$p,
      inputs: ["afDesc", "afName", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIcon
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIcon,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afName", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconBars = class DigiIconBars {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconBars.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconBars,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconBars.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconBars,
  selector: "digi-icon-bars",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconBars = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$q,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconBars
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconBars,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-bars",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconCheck = class DigiIconCheck {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconCheck.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconCheck,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconCheck.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconCheck,
  selector: "digi-icon-check",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconCheck = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$r,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconCheck
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconCheck,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-check",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconCheckCircle = class DigiIconCheckCircle {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconCheckCircle.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconCheckCircle,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconCheckCircle.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconCheckCircle,
  selector: "digi-icon-check-circle",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconCheckCircle = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$s,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconCheckCircle
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconCheckCircle,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-check-circle",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconCheckCircleReg = class DigiIconCheckCircleReg {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconCheckCircleReg.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconCheckCircleReg,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconCheckCircleReg.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconCheckCircleReg,
  selector: "digi-icon-check-circle-reg",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconCheckCircleReg = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$t,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconCheckCircleReg
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconCheckCircleReg,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-check-circle-reg",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconCheckCircleRegAlt = class DigiIconCheckCircleRegAlt {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconCheckCircleRegAlt.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconCheckCircleRegAlt,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconCheckCircleRegAlt.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconCheckCircleRegAlt,
  selector: "digi-icon-check-circle-reg-alt",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconCheckCircleRegAlt = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$u,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconCheckCircleRegAlt
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconCheckCircleRegAlt,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-check-circle-reg-alt",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconChevronDown = class DigiIconChevronDown {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconChevronDown.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconChevronDown,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconChevronDown.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconChevronDown,
  selector: "digi-icon-chevron-down",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconChevronDown = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$v,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconChevronDown
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconChevronDown,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-chevron-down",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconChevronLeft = class DigiIconChevronLeft {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconChevronLeft.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconChevronLeft,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconChevronLeft.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconChevronLeft,
  selector: "digi-icon-chevron-left",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconChevronLeft = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$w,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconChevronLeft
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconChevronLeft,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-chevron-left",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconChevronRight = class DigiIconChevronRight {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconChevronRight.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconChevronRight,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconChevronRight.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconChevronRight,
  selector: "digi-icon-chevron-right",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconChevronRight = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$x,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconChevronRight
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconChevronRight,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-chevron-right",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconChevronUp = class DigiIconChevronUp {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconChevronUp.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconChevronUp,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconChevronUp.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconChevronUp,
  selector: "digi-icon-chevron-up",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconChevronUp = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$y,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconChevronUp
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconChevronUp,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-chevron-up",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconCopy = class DigiIconCopy {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconCopy.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconCopy,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconCopy.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconCopy,
  selector: "digi-icon-copy",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconCopy = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$z,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconCopy
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconCopy,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-copy",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconDangerOutline = class DigiIconDangerOutline {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconDangerOutline.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconDangerOutline,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconDangerOutline.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconDangerOutline,
  selector: "digi-icon-danger-outline",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconDangerOutline = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$A,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconDangerOutline
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconDangerOutline,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-danger-outline",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconDownload = class DigiIconDownload {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconDownload.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconDownload,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconDownload.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconDownload,
  selector: "digi-icon-download",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconDownload = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$B,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconDownload
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconDownload,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-download",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconExclamationCircle = class DigiIconExclamationCircle {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconExclamationCircle.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconExclamationCircle,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconExclamationCircle.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconExclamationCircle,
  selector: "digi-icon-exclamation-circle",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconExclamationCircle = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$C,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconExclamationCircle
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconExclamationCircle,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-exclamation-circle",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconExclamationCircleFilled = class DigiIconExclamationCircleFilled {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconExclamationCircleFilled.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconExclamationCircleFilled,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconExclamationCircleFilled.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconExclamationCircleFilled,
  selector: "digi-icon-exclamation-circle-filled",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconExclamationCircleFilled = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$D,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconExclamationCircleFilled
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconExclamationCircleFilled,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-exclamation-circle-filled",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconExclamationTriangle = class DigiIconExclamationTriangle {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconExclamationTriangle.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconExclamationTriangle,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconExclamationTriangle.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconExclamationTriangle,
  selector: "digi-icon-exclamation-triangle",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconExclamationTriangle = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$E,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconExclamationTriangle
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconExclamationTriangle,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-exclamation-triangle",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconExclamationTriangleWarning = class DigiIconExclamationTriangleWarning {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconExclamationTriangleWarning.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconExclamationTriangleWarning,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconExclamationTriangleWarning.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconExclamationTriangleWarning,
  selector: "digi-icon-exclamation-triangle-warning",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconExclamationTriangleWarning = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$F,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconExclamationTriangleWarning
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconExclamationTriangleWarning,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-exclamation-triangle-warning",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconExternalLinkAlt = class DigiIconExternalLinkAlt {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconExternalLinkAlt.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconExternalLinkAlt,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconExternalLinkAlt.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconExternalLinkAlt,
  selector: "digi-icon-external-link-alt",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconExternalLinkAlt = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$G,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconExternalLinkAlt
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconExternalLinkAlt,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-external-link-alt",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconMinus = class DigiIconMinus {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconMinus.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconMinus,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconMinus.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconMinus,
  selector: "digi-icon-minus",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconMinus = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$H,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconMinus
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconMinus,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-minus",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconPaperclip = class DigiIconPaperclip {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconPaperclip.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconPaperclip,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconPaperclip.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconPaperclip,
  selector: "digi-icon-paperclip",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconPaperclip = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$I,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconPaperclip
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconPaperclip,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-paperclip",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconPlus = class DigiIconPlus {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconPlus.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconPlus,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconPlus.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconPlus,
  selector: "digi-icon-plus",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconPlus = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$J,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconPlus
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconPlus,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-plus",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconSearch = class DigiIconSearch {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconSearch.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconSearch,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconSearch.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconSearch,
  selector: "digi-icon-search",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconSearch = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$K,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconSearch
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconSearch,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-search",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconSpinner = class DigiIconSpinner {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconSpinner.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconSpinner,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconSpinner.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconSpinner,
  selector: "digi-icon-spinner",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconSpinner = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$L,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconSpinner
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconSpinner,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-spinner",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconTrash = class DigiIconTrash {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconTrash.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconTrash,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconTrash.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconTrash,
  selector: "digi-icon-trash",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconTrash = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$M,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconTrash
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconTrash,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-trash",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiIconX = class DigiIconX {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiIconX.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconX,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiIconX.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiIconX,
  selector: "digi-icon-x",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiIconX = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$N,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiIconX
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiIconX,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-icon-x",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiLayoutBlock = class DigiLayoutBlock {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiLayoutBlock.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLayoutBlock,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiLayoutBlock.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiLayoutBlock,
  selector: "digi-layout-block",
  inputs: {
    afContainer: "afContainer",
    afMarginBottom: "afMarginBottom",
    afMarginTop: "afMarginTop",
    afVariation: "afVariation",
    afVerticalPadding: "afVerticalPadding",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiLayoutBlock = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$O,
      inputs: [
        "afContainer",
        "afMarginBottom",
        "afMarginTop",
        "afVariation",
        "afVerticalPadding",
      ],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiLayoutBlock
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLayoutBlock,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-layout-block",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afContainer",
            "afMarginBottom",
            "afMarginTop",
            "afVariation",
            "afVerticalPadding",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiLayoutColumns = class DigiLayoutColumns {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiLayoutColumns.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLayoutColumns,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiLayoutColumns.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiLayoutColumns,
  selector: "digi-layout-columns",
  inputs: { afElement: "afElement", afVariation: "afVariation" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiLayoutColumns = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$P,
      inputs: ["afElement", "afVariation"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiLayoutColumns
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLayoutColumns,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-layout-columns",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afElement", "afVariation"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiLayoutContainer = class DigiLayoutContainer {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiLayoutContainer.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLayoutContainer,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiLayoutContainer.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiLayoutContainer,
  selector: "digi-layout-container",
  inputs: {
    afMarginBottom: "afMarginBottom",
    afMarginTop: "afMarginTop",
    afNoGutter: "afNoGutter",
    afVariation: "afVariation",
    afVerticalPadding: "afVerticalPadding",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiLayoutContainer = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$Q,
      inputs: [
        "afMarginBottom",
        "afMarginTop",
        "afNoGutter",
        "afVariation",
        "afVerticalPadding",
      ],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiLayoutContainer
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLayoutContainer,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-layout-container",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afMarginBottom",
            "afMarginTop",
            "afNoGutter",
            "afVariation",
            "afVerticalPadding",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiLayoutGrid = class DigiLayoutGrid {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiLayoutGrid.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLayoutGrid,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiLayoutGrid.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiLayoutGrid,
  selector: "digi-layout-grid",
  inputs: { afVerticalSpacing: "afVerticalSpacing" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiLayoutGrid = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$R,
      inputs: ["afVerticalSpacing"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiLayoutGrid
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLayoutGrid,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-layout-grid",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afVerticalSpacing"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiLayoutMediaObject = class DigiLayoutMediaObject {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiLayoutMediaObject.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLayoutMediaObject,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiLayoutMediaObject.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiLayoutMediaObject,
  selector: "digi-layout-media-object",
  inputs: { afAlignment: "afAlignment" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiLayoutMediaObject = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$S,
      inputs: ["afAlignment"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiLayoutMediaObject
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLayoutMediaObject,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-layout-media-object",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afAlignment"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiLayoutRows = class DigiLayoutRows {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiLayoutRows.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLayoutRows,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiLayoutRows.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiLayoutRows,
  selector: "digi-layout-rows",
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiLayoutRows = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$T,
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiLayoutRows
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLayoutRows,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-layout-rows",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiLayoutStackedBlocks = class DigiLayoutStackedBlocks {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiLayoutStackedBlocks.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLayoutStackedBlocks,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiLayoutStackedBlocks.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiLayoutStackedBlocks,
  selector: "digi-layout-stacked-blocks",
  inputs: { afVariation: "afVariation" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiLayoutStackedBlocks = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$U,
      inputs: ["afVariation"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiLayoutStackedBlocks
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLayoutStackedBlocks,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-layout-stacked-blocks",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afVariation"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiLink = class DigiLink {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnClick"]);
  }
};
DigiLink.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLink,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiLink.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiLink,
  selector: "digi-link",
  inputs: {
    afHref: "afHref",
    afOverrideLink: "afOverrideLink",
    afTarget: "afTarget",
    afVariation: "afVariation",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiLink = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$V,
      inputs: ["afHref", "afOverrideLink", "afTarget", "afVariation"],
      methods: ["afMGetLinkElement"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiLink
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLink,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-link",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afHref", "afOverrideLink", "afTarget", "afVariation"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiLinkExternal = class DigiLinkExternal {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnClick"]);
  }
};
DigiLinkExternal.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLinkExternal,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiLinkExternal.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiLinkExternal,
  selector: "digi-link-external",
  inputs: {
    afHref: "afHref",
    afOverrideLink: "afOverrideLink",
    afTarget: "afTarget",
    afVariation: "afVariation",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiLinkExternal = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$W,
      inputs: ["afHref", "afOverrideLink", "afTarget", "afVariation"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiLinkExternal
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLinkExternal,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-link-external",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afHref", "afOverrideLink", "afTarget", "afVariation"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiLinkIcon = class DigiLinkIcon {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiLinkIcon.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLinkIcon,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiLinkIcon.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiLinkIcon,
  selector: "digi-link-icon",
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiLinkIcon = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$X,
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiLinkIcon
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLinkIcon,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-link-icon",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiLinkInternal = class DigiLinkInternal {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnClick"]);
  }
};
DigiLinkInternal.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLinkInternal,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiLinkInternal.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiLinkInternal,
  selector: "digi-link-internal",
  inputs: {
    afHref: "afHref",
    afOverrideLink: "afOverrideLink",
    afVariation: "afVariation",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiLinkInternal = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$Y,
      inputs: ["afHref", "afOverrideLink", "afVariation"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiLinkInternal
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLinkInternal,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-link-internal",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afHref", "afOverrideLink", "afVariation"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiListLink = class DigiListLink {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiListLink.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiListLink,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiListLink.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiListLink,
  selector: "digi-list-link",
  inputs: {
    afLayout: "afLayout",
    afType: "afType",
    afVariation: "afVariation",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiListLink = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$Z,
      inputs: ["afLayout", "afType", "afVariation"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiListLink
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiListLink,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-list-link",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afLayout", "afType", "afVariation"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiLoaderSpinner = class DigiLoaderSpinner {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiLoaderSpinner.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLoaderSpinner,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiLoaderSpinner.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiLoaderSpinner,
  selector: "digi-loader-spinner",
  inputs: { afSize: "afSize", afText: "afText" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiLoaderSpinner = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$_,
      inputs: ["afSize", "afText"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiLoaderSpinner
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLoaderSpinner,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-loader-spinner",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afSize", "afText"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiLogo = class DigiLogo {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiLogo.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLogo,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiLogo.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiLogo,
  selector: "digi-logo",
  inputs: {
    afDesc: "afDesc",
    afSvgAriaHidden: "afSvgAriaHidden",
    afTitle: "afTitle",
    afTitleId: "afTitleId",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiLogo = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$$,
      inputs: ["afDesc", "afSvgAriaHidden", "afTitle", "afTitleId"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiLogo
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLogo,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-logo",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDesc", "afSvgAriaHidden", "afTitle", "afTitleId"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiLogoService = class DigiLogoService {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiLogoService.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLogoService,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiLogoService.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiLogoService,
  selector: "digi-logo-service",
  inputs: {
    afDescription: "afDescription",
    afName: "afName",
    afNameId: "afNameId",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiLogoService = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$10,
      inputs: ["afDescription", "afName", "afNameId"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiLogoService
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLogoService,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-logo-service",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDescription", "afName", "afNameId"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiLogoSister = class DigiLogoSister {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiLogoSister.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLogoSister,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiLogoSister.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiLogoSister,
  selector: "digi-logo-sister",
  inputs: { afName: "afName", afNameId: "afNameId" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiLogoSister = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$11,
      inputs: ["afName", "afNameId"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiLogoSister
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiLogoSister,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-logo-sister",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afName", "afNameId"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiMediaFigure = class DigiMediaFigure {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiMediaFigure.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiMediaFigure,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiMediaFigure.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiMediaFigure,
  selector: "digi-media-figure",
  inputs: { afAlignment: "afAlignment", afFigcaption: "afFigcaption" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiMediaFigure = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$12,
      inputs: ["afAlignment", "afFigcaption"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiMediaFigure
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiMediaFigure,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-media-figure",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afAlignment", "afFigcaption"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiMediaImage = class DigiMediaImage {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnLoad"]);
  }
};
DigiMediaImage.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiMediaImage,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiMediaImage.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiMediaImage,
  selector: "digi-media-image",
  inputs: {
    afAlt: "afAlt",
    afAriaLabel: "afAriaLabel",
    afFullwidth: "afFullwidth",
    afHeight: "afHeight",
    afObserverOptions: "afObserverOptions",
    afSrc: "afSrc",
    afSrcset: "afSrcset",
    afTitle: "afTitle",
    afUnlazy: "afUnlazy",
    afWidth: "afWidth",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiMediaImage = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$13,
      inputs: [
        "afAlt",
        "afAriaLabel",
        "afFullwidth",
        "afHeight",
        "afObserverOptions",
        "afSrc",
        "afSrcset",
        "afTitle",
        "afUnlazy",
        "afWidth",
      ],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiMediaImage
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiMediaImage,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-media-image",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afAlt",
            "afAriaLabel",
            "afFullwidth",
            "afHeight",
            "afObserverOptions",
            "afSrc",
            "afSrcset",
            "afTitle",
            "afUnlazy",
            "afWidth",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiNavigationBreadcrumbs = class DigiNavigationBreadcrumbs {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiNavigationBreadcrumbs.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationBreadcrumbs,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiNavigationBreadcrumbs.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiNavigationBreadcrumbs,
  selector: "digi-navigation-breadcrumbs",
  inputs: {
    afAriaLabel: "afAriaLabel",
    afCurrentPage: "afCurrentPage",
    afVariation: "afVariation",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiNavigationBreadcrumbs = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$14,
      inputs: ["afAriaLabel", "afCurrentPage", "afVariation"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiNavigationBreadcrumbs
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationBreadcrumbs,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-navigation-breadcrumbs",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afAriaLabel", "afCurrentPage", "afVariation"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiNavigationContextMenu = class DigiNavigationContextMenu {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, [
      "afOnInactive",
      "afOnActive",
      "afOnBlur",
      "afOnChange",
      "afOnToggle",
      "afOnSelect",
    ]);
  }
};
DigiNavigationContextMenu.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationContextMenu,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiNavigationContextMenu.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiNavigationContextMenu,
  selector: "digi-navigation-context-menu",
  inputs: {
    afIcon: "afIcon",
    afId: "afId",
    afNavigationContextMenuItems: "afNavigationContextMenuItems",
    afStartSelected: "afStartSelected",
    afText: "afText",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiNavigationContextMenu = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$15,
      inputs: [
        "afIcon",
        "afId",
        "afNavigationContextMenuItems",
        "afStartSelected",
        "afText",
      ],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiNavigationContextMenu
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationContextMenu,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-navigation-context-menu",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afIcon",
            "afId",
            "afNavigationContextMenuItems",
            "afStartSelected",
            "afText",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiNavigationContextMenuItem = class DigiNavigationContextMenuItem {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiNavigationContextMenuItem.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationContextMenuItem,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiNavigationContextMenuItem.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiNavigationContextMenuItem,
  selector: "digi-navigation-context-menu-item",
  inputs: {
    afDir: "afDir",
    afHref: "afHref",
    afLang: "afLang",
    afText: "afText",
    afType: "afType",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiNavigationContextMenuItem = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$16,
      inputs: ["afDir", "afHref", "afLang", "afText", "afType"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiNavigationContextMenuItem
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationContextMenuItem,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-navigation-context-menu-item",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDir", "afHref", "afLang", "afText", "afType"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiNavigationMainMenu = class DigiNavigationMainMenu {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiNavigationMainMenu.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationMainMenu,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiNavigationMainMenu.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiNavigationMainMenu,
  selector: "digi-navigation-main-menu",
  inputs: { afId: "afId" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiNavigationMainMenu = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$17,
      inputs: ["afId"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiNavigationMainMenu
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationMainMenu,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-navigation-main-menu",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afId"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiNavigationMainMenuPanel = class DigiNavigationMainMenuPanel {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnResize", "afOnClose"]);
  }
};
DigiNavigationMainMenuPanel.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationMainMenuPanel,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiNavigationMainMenuPanel.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiNavigationMainMenuPanel,
  selector: "digi-navigation-main-menu-panel",
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiNavigationMainMenuPanel = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$18,
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiNavigationMainMenuPanel
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationMainMenuPanel,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-navigation-main-menu-panel",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiNavigationPagination = class DigiNavigationPagination {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnPageChange"]);
  }
};
DigiNavigationPagination.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationPagination,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiNavigationPagination.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiNavigationPagination,
  selector: "digi-navigation-pagination",
  inputs: {
    afCurrentResultEnd: "afCurrentResultEnd",
    afCurrentResultStart: "afCurrentResultStart",
    afId: "afId",
    afInitActivePage: "afInitActivePage",
    afResultName: "afResultName",
    afTotalPages: "afTotalPages",
    afTotalResults: "afTotalResults",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiNavigationPagination = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$19,
      inputs: [
        "afCurrentResultEnd",
        "afCurrentResultStart",
        "afId",
        "afInitActivePage",
        "afResultName",
        "afTotalPages",
        "afTotalResults",
      ],
      methods: ["afMSetCurrentPage"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiNavigationPagination
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationPagination,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-navigation-pagination",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afCurrentResultEnd",
            "afCurrentResultStart",
            "afId",
            "afInitActivePage",
            "afResultName",
            "afTotalPages",
            "afTotalResults",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiNavigationSidebar = class DigiNavigationSidebar {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, [
      "afOnClose",
      "afOnEsc",
      "afOnBackdropClick",
      "afOnToggle",
    ]);
  }
};
DigiNavigationSidebar.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationSidebar,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiNavigationSidebar.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiNavigationSidebar,
  selector: "digi-navigation-sidebar",
  inputs: {
    afActive: "afActive",
    afBackdrop: "afBackdrop",
    afCloseButtonAriaLabel: "afCloseButtonAriaLabel",
    afCloseButtonPosition: "afCloseButtonPosition",
    afCloseButtonText: "afCloseButtonText",
    afCloseFocusableElement: "afCloseFocusableElement",
    afFocusableElement: "afFocusableElement",
    afHeading: "afHeading",
    afHeadingLevel: "afHeadingLevel",
    afHideHeader: "afHideHeader",
    afId: "afId",
    afMobilePosition: "afMobilePosition",
    afMobileVariation: "afMobileVariation",
    afPosition: "afPosition",
    afStickyHeader: "afStickyHeader",
    afVariation: "afVariation",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiNavigationSidebar = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$1a,
      inputs: [
        "afActive",
        "afBackdrop",
        "afCloseButtonAriaLabel",
        "afCloseButtonPosition",
        "afCloseButtonText",
        "afCloseFocusableElement",
        "afFocusableElement",
        "afHeading",
        "afHeadingLevel",
        "afHideHeader",
        "afId",
        "afMobilePosition",
        "afMobileVariation",
        "afPosition",
        "afStickyHeader",
        "afVariation",
      ],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiNavigationSidebar
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationSidebar,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-navigation-sidebar",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afActive",
            "afBackdrop",
            "afCloseButtonAriaLabel",
            "afCloseButtonPosition",
            "afCloseButtonText",
            "afCloseFocusableElement",
            "afFocusableElement",
            "afHeading",
            "afHeadingLevel",
            "afHideHeader",
            "afId",
            "afMobilePosition",
            "afMobileVariation",
            "afPosition",
            "afStickyHeader",
            "afVariation",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiNavigationSidebarButton = class DigiNavigationSidebarButton {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnToggle"]);
  }
};
DigiNavigationSidebarButton.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationSidebarButton,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiNavigationSidebarButton.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiNavigationSidebarButton,
  selector: "digi-navigation-sidebar-button",
  inputs: { afAriaLabel: "afAriaLabel", afId: "afId", afText: "afText" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiNavigationSidebarButton = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$1b,
      inputs: ["afAriaLabel", "afId", "afText"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiNavigationSidebarButton
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationSidebarButton,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-navigation-sidebar-button",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afAriaLabel", "afId", "afText"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiNavigationTab = class DigiNavigationTab {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnToggle"]);
  }
};
DigiNavigationTab.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationTab,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiNavigationTab.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiNavigationTab,
  selector: "digi-navigation-tab",
  inputs: { afActive: "afActive", afAriaLabel: "afAriaLabel", afId: "afId" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiNavigationTab = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$1c,
      inputs: ["afActive", "afAriaLabel", "afId"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiNavigationTab
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationTab,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-navigation-tab",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afActive", "afAriaLabel", "afId"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiNavigationTabInABox = class DigiNavigationTabInABox {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnToggle"]);
  }
};
DigiNavigationTabInABox.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationTabInABox,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiNavigationTabInABox.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiNavigationTabInABox,
  selector: "digi-navigation-tab-in-a-box",
  inputs: { afActive: "afActive", afAriaLabel: "afAriaLabel", afId: "afId" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiNavigationTabInABox = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$1d,
      inputs: ["afActive", "afAriaLabel", "afId"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiNavigationTabInABox
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationTabInABox,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-navigation-tab-in-a-box",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afActive", "afAriaLabel", "afId"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiNavigationTabs = class DigiNavigationTabs {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnChange", "afOnClick", "afOnFocus"]);
  }
};
DigiNavigationTabs.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationTabs,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiNavigationTabs.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiNavigationTabs,
  selector: "digi-navigation-tabs",
  inputs: {
    afAriaLabel: "afAriaLabel",
    afId: "afId",
    afInitActiveTab: "afInitActiveTab",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiNavigationTabs = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$1e,
      inputs: ["afAriaLabel", "afId", "afInitActiveTab"],
      methods: ["afMSetActiveTab"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiNavigationTabs
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationTabs,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-navigation-tabs",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afAriaLabel", "afId", "afInitActiveTab"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiNavigationToc = class DigiNavigationToc {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiNavigationToc.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationToc,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiNavigationToc.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiNavigationToc,
  selector: "digi-navigation-toc",
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiNavigationToc = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$1f,
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiNavigationToc
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationToc,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-navigation-toc",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiNavigationVerticalMenu = class DigiNavigationVerticalMenu {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiNavigationVerticalMenu.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationVerticalMenu,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiNavigationVerticalMenu.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiNavigationVerticalMenu,
  selector: "digi-navigation-vertical-menu",
  inputs: {
    afActiveIndicatorSize: "afActiveIndicatorSize",
    afAriaLabel: "afAriaLabel",
    afId: "afId",
    afVariation: "afVariation",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiNavigationVerticalMenu = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$1g,
      inputs: ["afActiveIndicatorSize", "afAriaLabel", "afId", "afVariation"],
      methods: ["afMSetCurrentActiveLevel"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiNavigationVerticalMenu
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationVerticalMenu,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-navigation-vertical-menu",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afActiveIndicatorSize",
            "afAriaLabel",
            "afId",
            "afVariation",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiNavigationVerticalMenuItem = class DigiNavigationVerticalMenuItem {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnClick"]);
  }
};
DigiNavigationVerticalMenuItem.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationVerticalMenuItem,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiNavigationVerticalMenuItem.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiNavigationVerticalMenuItem,
  selector: "digi-navigation-vertical-menu-item",
  inputs: {
    afActive: "afActive",
    afActiveSubnav: "afActiveSubnav",
    afHasSubnav: "afHasSubnav",
    afHref: "afHref",
    afId: "afId",
    afOverrideLink: "afOverrideLink",
    afText: "afText",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiNavigationVerticalMenuItem = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$1h,
      inputs: [
        "afActive",
        "afActiveSubnav",
        "afHasSubnav",
        "afHref",
        "afId",
        "afOverrideLink",
        "afText",
      ],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiNavigationVerticalMenuItem
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNavigationVerticalMenuItem,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-navigation-vertical-menu-item",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afActive",
            "afActiveSubnav",
            "afHasSubnav",
            "afHref",
            "afId",
            "afOverrideLink",
            "afText",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiNotificationAlert = class DigiNotificationAlert {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnClose"]);
  }
};
DigiNotificationAlert.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNotificationAlert,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiNotificationAlert.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiNotificationAlert,
  selector: "digi-notification-alert",
  inputs: { afCloseable: "afCloseable", afVariation: "afVariation" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiNotificationAlert = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$1i,
      inputs: ["afCloseable", "afVariation"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiNotificationAlert
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNotificationAlert,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-notification-alert",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afCloseable", "afVariation"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiNotificationCookie = class DigiNotificationCookie {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnAcceptAllCookies", "afOnSubmitSettings"]);
  }
};
DigiNotificationCookie.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNotificationCookie,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiNotificationCookie.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiNotificationCookie,
  selector: "digi-notification-cookie",
  inputs: {
    afBannerHeadingText: "afBannerHeadingText",
    afBannerText: "afBannerText",
    afId: "afId",
    afModalHeadingText: "afModalHeadingText",
    afRequiredCookiesText: "afRequiredCookiesText",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiNotificationCookie = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$1j,
      inputs: [
        "afBannerHeadingText",
        "afBannerText",
        "afId",
        "afModalHeadingText",
        "afRequiredCookiesText",
      ],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiNotificationCookie
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNotificationCookie,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-notification-cookie",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afBannerHeadingText",
            "afBannerText",
            "afId",
            "afModalHeadingText",
            "afRequiredCookiesText",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiNotificationDetail = class DigiNotificationDetail {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiNotificationDetail.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNotificationDetail,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiNotificationDetail.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiNotificationDetail,
  selector: "digi-notification-detail",
  inputs: { afVariation: "afVariation" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiNotificationDetail = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$1k,
      inputs: ["afVariation"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiNotificationDetail
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiNotificationDetail,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-notification-detail",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afVariation"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiPage = class DigiPage {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiPage.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiPage,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiPage.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiPage,
  selector: "digi-page",
  inputs: { afBackground: "afBackground" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiPage = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$1l,
      inputs: ["afBackground"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiPage
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiPage,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-page",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afBackground"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiPageBlockCards = class DigiPageBlockCards {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiPageBlockCards.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiPageBlockCards,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiPageBlockCards.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiPageBlockCards,
  selector: "digi-page-block-cards",
  inputs: { afVariation: "afVariation" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiPageBlockCards = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$1m,
      inputs: ["afVariation"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiPageBlockCards
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiPageBlockCards,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-page-block-cards",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afVariation"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiPageBlockHero = class DigiPageBlockHero {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiPageBlockHero.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiPageBlockHero,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiPageBlockHero.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiPageBlockHero,
  selector: "digi-page-block-hero",
  inputs: {
    afBackground: "afBackground",
    afBackgroundImage: "afBackgroundImage",
    afVariation: "afVariation",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiPageBlockHero = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$1n,
      inputs: ["afBackground", "afBackgroundImage", "afVariation"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiPageBlockHero
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiPageBlockHero,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-page-block-hero",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afBackground", "afBackgroundImage", "afVariation"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiPageBlockLists = class DigiPageBlockLists {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiPageBlockLists.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiPageBlockLists,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiPageBlockLists.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiPageBlockLists,
  selector: "digi-page-block-lists",
  inputs: { afVariation: "afVariation" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiPageBlockLists = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$1o,
      inputs: ["afVariation"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiPageBlockLists
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiPageBlockLists,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-page-block-lists",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afVariation"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiPageBlockSidebar = class DigiPageBlockSidebar {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiPageBlockSidebar.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiPageBlockSidebar,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiPageBlockSidebar.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiPageBlockSidebar,
  selector: "digi-page-block-sidebar",
  inputs: { afVariation: "afVariation" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiPageBlockSidebar = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$1p,
      inputs: ["afVariation"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiPageBlockSidebar
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiPageBlockSidebar,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-page-block-sidebar",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afVariation"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiPageFooter = class DigiPageFooter {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiPageFooter.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiPageFooter,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiPageFooter.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiPageFooter,
  selector: "digi-page-footer",
  inputs: { afVariation: "afVariation" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiPageFooter = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$1q,
      inputs: ["afVariation"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiPageFooter
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiPageFooter,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-page-footer",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afVariation"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiPageHeader = class DigiPageHeader {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiPageHeader.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiPageHeader,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiPageHeader.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiPageHeader,
  selector: "digi-page-header",
  inputs: { afId: "afId" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiPageHeader = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$1r,
      inputs: ["afId"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiPageHeader
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiPageHeader,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-page-header",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afId"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiProgressStep = class DigiProgressStep {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiProgressStep.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiProgressStep,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiProgressStep.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiProgressStep,
  selector: "digi-progress-step",
  inputs: {
    afHeading: "afHeading",
    afHeadingLevel: "afHeadingLevel",
    afId: "afId",
    afIsLast: "afIsLast",
    afStepStatus: "afStepStatus",
    afVariation: "afVariation",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiProgressStep = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$1s,
      inputs: [
        "afHeading",
        "afHeadingLevel",
        "afId",
        "afIsLast",
        "afStepStatus",
        "afVariation",
      ],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiProgressStep
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiProgressStep,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-progress-step",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afHeading",
            "afHeadingLevel",
            "afId",
            "afIsLast",
            "afStepStatus",
            "afVariation",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiProgressSteps = class DigiProgressSteps {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiProgressSteps.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiProgressSteps,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiProgressSteps.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiProgressSteps,
  selector: "digi-progress-steps",
  inputs: {
    afCurrentStep: "afCurrentStep",
    afHeadingLevel: "afHeadingLevel",
    afVariation: "afVariation",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiProgressSteps = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$1t,
      inputs: ["afCurrentStep", "afHeadingLevel", "afVariation"],
      methods: ["afMNext", "afMPrevious"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiProgressSteps
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiProgressSteps,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-progress-steps",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afCurrentStep", "afHeadingLevel", "afVariation"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiProgressbar = class DigiProgressbar {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiProgressbar.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiProgressbar,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiProgressbar.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiProgressbar,
  selector: "digi-progressbar",
  inputs: {
    afCompletedSteps: "afCompletedSteps",
    afId: "afId",
    afRole: "afRole",
    afStepsLabel: "afStepsLabel",
    afStepsSeparator: "afStepsSeparator",
    afTotalSteps: "afTotalSteps",
    afVariation: "afVariation",
  },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiProgressbar = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$1u,
      inputs: [
        "afCompletedSteps",
        "afId",
        "afRole",
        "afStepsLabel",
        "afStepsSeparator",
        "afTotalSteps",
        "afVariation",
      ],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiProgressbar
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiProgressbar,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-progressbar",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: [
            "afCompletedSteps",
            "afId",
            "afRole",
            "afStepsLabel",
            "afStepsSeparator",
            "afTotalSteps",
            "afVariation",
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiTable = class DigiTable {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiTable.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiTable,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiTable.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiTable,
  selector: "digi-table",
  inputs: { afVariation: "afVariation" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiTable = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$1v,
      inputs: ["afVariation"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiTable
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiTable,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-table",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afVariation"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiTag = class DigiTag {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnClick"]);
  }
};
DigiTag.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiTag,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiTag.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiTag,
  selector: "digi-tag",
  inputs: { afNoIcon: "afNoIcon", afSize: "afSize", afText: "afText" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiTag = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$1w,
      inputs: ["afNoIcon", "afSize", "afText"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiTag
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiTag,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-tag",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afNoIcon", "afSize", "afText"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiTypography = class DigiTypography {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiTypography.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiTypography,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiTypography.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiTypography,
  selector: "digi-typography",
  inputs: { afVariation: "afVariation" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiTypography = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$1x,
      inputs: ["afVariation"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiTypography
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiTypography,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-typography",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afVariation"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiTypographyHeadingSection = class DigiTypographyHeadingSection {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiTypographyHeadingSection.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiTypographyHeadingSection,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiTypographyHeadingSection.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiTypographyHeadingSection,
  selector: "digi-typography-heading-section",
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiTypographyHeadingSection = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$1y,
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiTypographyHeadingSection
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiTypographyHeadingSection,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-typography-heading-section",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiTypographyMeta = class DigiTypographyMeta {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiTypographyMeta.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiTypographyMeta,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiTypographyMeta.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiTypographyMeta,
  selector: "digi-typography-meta",
  inputs: { afVariation: "afVariation" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiTypographyMeta = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$1z,
      inputs: ["afVariation"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiTypographyMeta
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiTypographyMeta,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-typography-meta",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afVariation"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiTypographyPreamble = class DigiTypographyPreamble {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiTypographyPreamble.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiTypographyPreamble,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiTypographyPreamble.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiTypographyPreamble,
  selector: "digi-typography-preamble",
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiTypographyPreamble = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$1A,
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiTypographyPreamble
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiTypographyPreamble,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-typography-preamble",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiTypographyTime = class DigiTypographyTime {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
  }
};
DigiTypographyTime.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiTypographyTime,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiTypographyTime.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiTypographyTime,
  selector: "digi-typography-time",
  inputs: { afDateTime: "afDateTime", afVariation: "afVariation" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiTypographyTime = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$1B,
      inputs: ["afDateTime", "afVariation"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiTypographyTime
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiTypographyTime,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-typography-time",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDateTime", "afVariation"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiUtilBreakpointObserver = class DigiUtilBreakpointObserver {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, [
      "afOnChange",
      "afOnSmall",
      "afOnMedium",
      "afOnLarge",
      "afOnXLarge",
    ]);
  }
};
DigiUtilBreakpointObserver.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiUtilBreakpointObserver,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiUtilBreakpointObserver.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiUtilBreakpointObserver,
  selector: "digi-util-breakpoint-observer",
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiUtilBreakpointObserver = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$1C,
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiUtilBreakpointObserver
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiUtilBreakpointObserver,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-util-breakpoint-observer",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiUtilDetectClickOutside = class DigiUtilDetectClickOutside {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnClickOutside", "afOnClickInside"]);
  }
};
DigiUtilDetectClickOutside.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiUtilDetectClickOutside,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiUtilDetectClickOutside.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiUtilDetectClickOutside,
  selector: "digi-util-detect-click-outside",
  inputs: { afDataIdentifier: "afDataIdentifier" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiUtilDetectClickOutside = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$1D,
      inputs: ["afDataIdentifier"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiUtilDetectClickOutside
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiUtilDetectClickOutside,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-util-detect-click-outside",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDataIdentifier"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiUtilDetectFocusOutside = class DigiUtilDetectFocusOutside {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnFocusOutside", "afOnFocusInside"]);
  }
};
DigiUtilDetectFocusOutside.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiUtilDetectFocusOutside,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiUtilDetectFocusOutside.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiUtilDetectFocusOutside,
  selector: "digi-util-detect-focus-outside",
  inputs: { afDataIdentifier: "afDataIdentifier" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiUtilDetectFocusOutside = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$1E,
      inputs: ["afDataIdentifier"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiUtilDetectFocusOutside
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiUtilDetectFocusOutside,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-util-detect-focus-outside",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afDataIdentifier"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiUtilIntersectionObserver = class DigiUtilIntersectionObserver {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, [
      "afOnChange",
      "afOnIntersect",
      "afOnUnintersect",
    ]);
  }
};
DigiUtilIntersectionObserver.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiUtilIntersectionObserver,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiUtilIntersectionObserver.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiUtilIntersectionObserver,
  selector: "digi-util-intersection-observer",
  inputs: { afOnce: "afOnce", afOptions: "afOptions" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiUtilIntersectionObserver = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$1F,
      inputs: ["afOnce", "afOptions"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiUtilIntersectionObserver
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiUtilIntersectionObserver,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-util-intersection-observer",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afOnce", "afOptions"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiUtilKeydownHandler = class DigiUtilKeydownHandler {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, [
      "afOnEsc",
      "afOnEnter",
      "afOnTab",
      "afOnSpace",
      "afOnShiftTab",
      "afOnUp",
      "afOnDown",
      "afOnLeft",
      "afOnRight",
      "afOnHome",
      "afOnEnd",
      "afOnKeyDown",
    ]);
  }
};
DigiUtilKeydownHandler.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiUtilKeydownHandler,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiUtilKeydownHandler.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiUtilKeydownHandler,
  selector: "digi-util-keydown-handler",
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiUtilKeydownHandler = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$1G,
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiUtilKeydownHandler
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiUtilKeydownHandler,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-util-keydown-handler",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiUtilKeyupHandler = class DigiUtilKeyupHandler {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, [
      "afOnEsc",
      "afOnEnter",
      "afOnTab",
      "afOnSpace",
      "afOnShiftTab",
      "afOnUp",
      "afOnDown",
      "afOnLeft",
      "afOnRight",
      "afOnHome",
      "afOnEnd",
      "afOnKeyUp",
    ]);
  }
};
DigiUtilKeyupHandler.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiUtilKeyupHandler,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiUtilKeyupHandler.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiUtilKeyupHandler,
  selector: "digi-util-keyup-handler",
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiUtilKeyupHandler = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$1H,
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiUtilKeyupHandler
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiUtilKeyupHandler,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-util-keyup-handler",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiUtilMutationObserver = class DigiUtilMutationObserver {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnChange"]);
  }
};
DigiUtilMutationObserver.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiUtilMutationObserver,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiUtilMutationObserver.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiUtilMutationObserver,
  selector: "digi-util-mutation-observer",
  inputs: { afId: "afId", afOptions: "afOptions" },
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiUtilMutationObserver = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$1I,
      inputs: ["afId", "afOptions"],
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiUtilMutationObserver
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiUtilMutationObserver,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-util-mutation-observer",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
          inputs: ["afId", "afOptions"],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});
let DigiUtilResizeObserver = class DigiUtilResizeObserver {
  constructor(c, r, z) {
    this.z = z;
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ["afOnChange"]);
  }
};
DigiUtilResizeObserver.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiUtilResizeObserver,
  deps: [
    { token: i0.ChangeDetectorRef },
    { token: i0.ElementRef },
    { token: i0.NgZone },
  ],
  target: i0.ɵɵFactoryTarget.Component,
});
DigiUtilResizeObserver.ɵcmp = i0.ɵɵngDeclareComponent({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: DigiUtilResizeObserver,
  selector: "digi-util-resize-observer",
  ngImport: i0,
  template: "<ng-content></ng-content>",
  isInline: true,
  changeDetection: i0.ChangeDetectionStrategy.OnPush,
});
DigiUtilResizeObserver = __decorate(
  [
    ProxyCmp({
      defineCustomElementFn: defineCustomElement$1J,
    }),
    __metadata("design:paramtypes", [ChangeDetectorRef, ElementRef, NgZone]),
  ],
  DigiUtilResizeObserver
);
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiUtilResizeObserver,
  decorators: [
    {
      type: Component,
      args: [
        {
          selector: "digi-util-resize-observer",
          changeDetection: ChangeDetectionStrategy.OnPush,
          template: "<ng-content></ng-content>",
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [
      { type: i0.ChangeDetectorRef },
      { type: i0.ElementRef },
      { type: i0.NgZone },
    ];
  },
});

class ValueAccessor {
  constructor(el) {
    this.el = el;
    this.onChange = () => {};
    this.onTouched = () => {};
  }
  writeValue(value) {
    this.el.nativeElement.value = this.lastValue = value == null ? "" : value;
  }
  handleChangeEvent(value) {
    if (value !== this.lastValue) {
      this.lastValue = value;
      this.onChange(value);
    }
  }
  _handleBlurEvent() {
    this.onTouched();
  }
  registerOnChange(fn) {
    this.onChange = fn;
  }
  registerOnTouched(fn) {
    this.onTouched = fn;
  }
  setDisabledState(isDisabled) {
    this.el.nativeElement.disabled = isDisabled;
  }
}
ValueAccessor.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: ValueAccessor,
  deps: [{ token: i0.ElementRef }],
  target: i0.ɵɵFactoryTarget.Directive,
});
ValueAccessor.ɵdir = i0.ɵɵngDeclareDirective({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: ValueAccessor,
  host: { listeners: { focusout: "_handleBlurEvent()" } },
  ngImport: i0,
});
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: ValueAccessor,
  decorators: [
    {
      type: Directive,
      args: [{}],
    },
  ],
  ctorParameters: function () {
    return [{ type: i0.ElementRef }];
  },
  propDecorators: {
    _handleBlurEvent: [
      {
        type: HostListener,
        args: ["focusout"],
      },
    ],
  },
});

class BooleanValueAccessor extends ValueAccessor {
  constructor(el) {
    super(el);
  }
  writeValue(value) {
    this.el.nativeElement.checked = this.lastValue =
      value == null ? false : value;
  }
}
BooleanValueAccessor.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: BooleanValueAccessor,
  deps: [{ token: i0.ElementRef }],
  target: i0.ɵɵFactoryTarget.Directive,
});
BooleanValueAccessor.ɵdir = i0.ɵɵngDeclareDirective({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: BooleanValueAccessor,
  selector: "digi-form-checkbox",
  host: {
    listeners: { afOnChange: "handleChangeEvent($event.target.checked)" },
  },
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: BooleanValueAccessor,
      multi: true,
    },
  ],
  usesInheritance: true,
  ngImport: i0,
});
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: BooleanValueAccessor,
  decorators: [
    {
      type: Directive,
      args: [
        {
          /* tslint:disable-next-line:directive-selector */
          selector: "digi-form-checkbox",
          host: {
            "(afOnChange)": "handleChangeEvent($event.target.checked)",
          },
          providers: [
            {
              provide: NG_VALUE_ACCESSOR,
              useExisting: BooleanValueAccessor,
              multi: true,
            },
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [{ type: i0.ElementRef }];
  },
});

class NumericValueAccessor extends ValueAccessor {
  constructor(el) {
    super(el);
  }
  registerOnChange(fn) {
    super.registerOnChange((value) => {
      fn(value === "" ? null : parseFloat(value));
    });
  }
}
NumericValueAccessor.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: NumericValueAccessor,
  deps: [{ token: i0.ElementRef }],
  target: i0.ɵɵFactoryTarget.Directive,
});
NumericValueAccessor.ɵdir = i0.ɵɵngDeclareDirective({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: NumericValueAccessor,
  selector: "digi-form-input[afType=number]",
  host: { listeners: { afOnInput: "handleChangeEvent($event.target.value)" } },
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: NumericValueAccessor,
      multi: true,
    },
  ],
  usesInheritance: true,
  ngImport: i0,
});
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: NumericValueAccessor,
  decorators: [
    {
      type: Directive,
      args: [
        {
          /* tslint:disable-next-line:directive-selector */
          selector: "digi-form-input[afType=number]",
          host: {
            "(afOnInput)": "handleChangeEvent($event.target.value)",
          },
          providers: [
            {
              provide: NG_VALUE_ACCESSOR,
              useExisting: NumericValueAccessor,
              multi: true,
            },
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [{ type: i0.ElementRef }];
  },
});

class RadioValueAccessor extends ValueAccessor {
  constructor(el) {
    super(el);
  }
}
RadioValueAccessor.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: RadioValueAccessor,
  deps: [{ token: i0.ElementRef }],
  target: i0.ɵɵFactoryTarget.Directive,
});
RadioValueAccessor.ɵdir = i0.ɵɵngDeclareDirective({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: RadioValueAccessor,
  selector: "digi-form-radiobutton",
  host: { listeners: { afOnChange: "handleChangeEvent($event.target.value)" } },
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: RadioValueAccessor,
      multi: true,
    },
  ],
  usesInheritance: true,
  ngImport: i0,
});
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: RadioValueAccessor,
  decorators: [
    {
      type: Directive,
      args: [
        {
          /* tslint:disable-next-line:directive-selector */
          selector: "digi-form-radiobutton",
          host: {
            "(afOnChange)": "handleChangeEvent($event.target.value)",
          },
          providers: [
            {
              provide: NG_VALUE_ACCESSOR,
              useExisting: RadioValueAccessor,
              multi: true,
            },
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [{ type: i0.ElementRef }];
  },
});

class SelectValueAccessor extends ValueAccessor {
  constructor(el) {
    super(el);
  }
}
SelectValueAccessor.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: SelectValueAccessor,
  deps: [{ token: i0.ElementRef }],
  target: i0.ɵɵFactoryTarget.Directive,
});
SelectValueAccessor.ɵdir = i0.ɵɵngDeclareDirective({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: SelectValueAccessor,
  selector: "digi-form-select",
  host: { listeners: { afOnChange: "handleChangeEvent($event.target.value)" } },
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: SelectValueAccessor,
      multi: true,
    },
  ],
  usesInheritance: true,
  ngImport: i0,
});
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: SelectValueAccessor,
  decorators: [
    {
      type: Directive,
      args: [
        {
          /* tslint:disable-next-line:directive-selector */
          selector: "digi-form-select",
          host: {
            "(afOnChange)": "handleChangeEvent($event.target.value)",
          },
          providers: [
            {
              provide: NG_VALUE_ACCESSOR,
              useExisting: SelectValueAccessor,
              multi: true,
            },
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [{ type: i0.ElementRef }];
  },
});

class TextValueAccessor extends ValueAccessor {
  constructor(el) {
    super(el);
  }
}
TextValueAccessor.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: TextValueAccessor,
  deps: [{ token: i0.ElementRef }],
  target: i0.ɵɵFactoryTarget.Directive,
});
TextValueAccessor.ɵdir = i0.ɵɵngDeclareDirective({
  minVersion: "14.0.0",
  version: "14.1.3",
  type: TextValueAccessor,
  selector:
    "digi-form-input, digi-form-input[afType=text], digi-form-input[afType=email], digi-form-input[afType=color], digi-form-input[afType=date], digi-form-input[afType=datetime-local], digi-form-input[afType=month], digi-form-input[afType=password], digi-form-input[afType=search], digi-form-input[afType=tel], digi-form-input[afType=time], digi-form-input[afType=url], digi-form-input[afType=week], digi-form-textarea, digi-form-input-search, digi-calendar",
  host: {
    listeners: {
      afOnInput: "handleChangeEvent($event.target.value)",
      afOnDateSelectedChange: "handleChangeEvent($event.target.afSelectedDate)",
    },
  },
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: TextValueAccessor,
      multi: true,
    },
  ],
  usesInheritance: true,
  ngImport: i0,
});
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: TextValueAccessor,
  decorators: [
    {
      type: Directive,
      args: [
        {
          /* tslint:disable-next-line:directive-selector */
          selector:
            "digi-form-input, digi-form-input[afType=text], digi-form-input[afType=email], digi-form-input[afType=color], digi-form-input[afType=date], digi-form-input[afType=datetime-local], digi-form-input[afType=month], digi-form-input[afType=password], digi-form-input[afType=search], digi-form-input[afType=tel], digi-form-input[afType=time], digi-form-input[afType=url], digi-form-input[afType=week], digi-form-textarea, digi-form-input-search, digi-calendar",
          host: {
            "(afOnInput)": "handleChangeEvent($event.target.value)",
            "(afOnDateSelectedChange)":
              "handleChangeEvent($event.target.afSelectedDate)",
          },
          providers: [
            {
              provide: NG_VALUE_ACCESSOR,
              useExisting: TextValueAccessor,
              multi: true,
            },
          ],
        },
      ],
    },
  ],
  ctorParameters: function () {
    return [{ type: i0.ElementRef }];
  },
});

/**
 * COMPONENTS list autogenerated on prepare.
 */
const COMPONENTS = [
  DigiButton,
  DigiCalendar,
  DigiCalendarWeekView,
  DigiCardBox,
  DigiCardLink,
  DigiCode,
  DigiCodeBlock,
  DigiCodeExample,
  DigiDialog,
  DigiExpandableAccordion,
  DigiFormCheckbox,
  DigiFormFieldset,
  DigiFormFileUpload,
  DigiFormFilter,
  DigiFormInput,
  DigiFormInputSearch,
  DigiFormLabel,
  DigiFormProcessStep,
  DigiFormProcessSteps,
  DigiFormRadiobutton,
  DigiFormRadiogroup,
  DigiFormSelect,
  DigiFormTextarea,
  DigiFormValidationMessage,
  DigiIcon,
  DigiIconBars,
  DigiIconCheck,
  DigiIconCheckCircle,
  DigiIconCheckCircleReg,
  DigiIconCheckCircleRegAlt,
  DigiIconChevronDown,
  DigiIconChevronLeft,
  DigiIconChevronRight,
  DigiIconChevronUp,
  DigiIconCopy,
  DigiIconDangerOutline,
  DigiIconDownload,
  DigiIconExclamationCircle,
  DigiIconExclamationCircleFilled,
  DigiIconExclamationTriangle,
  DigiIconExclamationTriangleWarning,
  DigiIconExternalLinkAlt,
  DigiIconMinus,
  DigiIconPaperclip,
  DigiIconPlus,
  DigiIconSearch,
  DigiIconSpinner,
  DigiIconTrash,
  DigiIconX,
  DigiLayoutBlock,
  DigiLayoutColumns,
  DigiLayoutContainer,
  DigiLayoutGrid,
  DigiLayoutMediaObject,
  DigiLayoutRows,
  DigiLayoutStackedBlocks,
  DigiLink,
  DigiLinkExternal,
  DigiLinkIcon,
  DigiLinkInternal,
  DigiListLink,
  DigiLoaderSpinner,
  DigiLogo,
  DigiLogoService,
  DigiLogoSister,
  DigiMediaFigure,
  DigiMediaImage,
  DigiNavigationBreadcrumbs,
  DigiNavigationContextMenu,
  DigiNavigationContextMenuItem,
  DigiNavigationMainMenu,
  DigiNavigationMainMenuPanel,
  DigiNavigationPagination,
  DigiNavigationSidebar,
  DigiNavigationSidebarButton,
  DigiNavigationTab,
  DigiNavigationTabInABox,
  DigiNavigationTabs,
  DigiNavigationToc,
  DigiNavigationVerticalMenu,
  DigiNavigationVerticalMenuItem,
  DigiNotificationAlert,
  DigiNotificationCookie,
  DigiNotificationDetail,
  DigiPage,
  DigiPageBlockCards,
  DigiPageBlockHero,
  DigiPageBlockLists,
  DigiPageBlockSidebar,
  DigiPageFooter,
  DigiPageHeader,
  DigiProgressStep,
  DigiProgressSteps,
  DigiProgressbar,
  DigiTable,
  DigiTag,
  DigiTypography,
  DigiTypographyHeadingSection,
  DigiTypographyMeta,
  DigiTypographyPreamble,
  DigiTypographyTime,
  DigiUtilBreakpointObserver,
  DigiUtilDetectClickOutside,
  DigiUtilDetectFocusOutside,
  DigiUtilIntersectionObserver,
  DigiUtilKeydownHandler,
  DigiUtilKeyupHandler,
  DigiUtilMutationObserver,
  DigiUtilResizeObserver,
];
const VALUEACCESSORS = [
  BooleanValueAccessor,
  NumericValueAccessor,
  RadioValueAccessor,
  SelectValueAccessor,
  TextValueAccessor,
];
class DigiSkolverketAngularModule {}
DigiSkolverketAngularModule.ɵfac = i0.ɵɵngDeclareFactory({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiSkolverketAngularModule,
  deps: [],
  target: i0.ɵɵFactoryTarget.NgModule,
});
DigiSkolverketAngularModule.ɵmod = i0.ɵɵngDeclareNgModule({
  minVersion: "14.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiSkolverketAngularModule,
  declarations: [
    DigiButton,
    DigiCalendar,
    DigiCalendarWeekView,
    DigiCardBox,
    DigiCardLink,
    DigiCode,
    DigiCodeBlock,
    DigiCodeExample,
    DigiDialog,
    DigiExpandableAccordion,
    DigiFormCheckbox,
    DigiFormFieldset,
    DigiFormFileUpload,
    DigiFormFilter,
    DigiFormInput,
    DigiFormInputSearch,
    DigiFormLabel,
    DigiFormProcessStep,
    DigiFormProcessSteps,
    DigiFormRadiobutton,
    DigiFormRadiogroup,
    DigiFormSelect,
    DigiFormTextarea,
    DigiFormValidationMessage,
    DigiIcon,
    DigiIconBars,
    DigiIconCheck,
    DigiIconCheckCircle,
    DigiIconCheckCircleReg,
    DigiIconCheckCircleRegAlt,
    DigiIconChevronDown,
    DigiIconChevronLeft,
    DigiIconChevronRight,
    DigiIconChevronUp,
    DigiIconCopy,
    DigiIconDangerOutline,
    DigiIconDownload,
    DigiIconExclamationCircle,
    DigiIconExclamationCircleFilled,
    DigiIconExclamationTriangle,
    DigiIconExclamationTriangleWarning,
    DigiIconExternalLinkAlt,
    DigiIconMinus,
    DigiIconPaperclip,
    DigiIconPlus,
    DigiIconSearch,
    DigiIconSpinner,
    DigiIconTrash,
    DigiIconX,
    DigiLayoutBlock,
    DigiLayoutColumns,
    DigiLayoutContainer,
    DigiLayoutGrid,
    DigiLayoutMediaObject,
    DigiLayoutRows,
    DigiLayoutStackedBlocks,
    DigiLink,
    DigiLinkExternal,
    DigiLinkIcon,
    DigiLinkInternal,
    DigiListLink,
    DigiLoaderSpinner,
    DigiLogo,
    DigiLogoService,
    DigiLogoSister,
    DigiMediaFigure,
    DigiMediaImage,
    DigiNavigationBreadcrumbs,
    DigiNavigationContextMenu,
    DigiNavigationContextMenuItem,
    DigiNavigationMainMenu,
    DigiNavigationMainMenuPanel,
    DigiNavigationPagination,
    DigiNavigationSidebar,
    DigiNavigationSidebarButton,
    DigiNavigationTab,
    DigiNavigationTabInABox,
    DigiNavigationTabs,
    DigiNavigationToc,
    DigiNavigationVerticalMenu,
    DigiNavigationVerticalMenuItem,
    DigiNotificationAlert,
    DigiNotificationCookie,
    DigiNotificationDetail,
    DigiPage,
    DigiPageBlockCards,
    DigiPageBlockHero,
    DigiPageBlockLists,
    DigiPageBlockSidebar,
    DigiPageFooter,
    DigiPageHeader,
    DigiProgressStep,
    DigiProgressSteps,
    DigiProgressbar,
    DigiTable,
    DigiTag,
    DigiTypography,
    DigiTypographyHeadingSection,
    DigiTypographyMeta,
    DigiTypographyPreamble,
    DigiTypographyTime,
    DigiUtilBreakpointObserver,
    DigiUtilDetectClickOutside,
    DigiUtilDetectFocusOutside,
    DigiUtilIntersectionObserver,
    DigiUtilKeydownHandler,
    DigiUtilKeyupHandler,
    DigiUtilMutationObserver,
    DigiUtilResizeObserver,
    BooleanValueAccessor,
    NumericValueAccessor,
    RadioValueAccessor,
    SelectValueAccessor,
    TextValueAccessor,
  ],
  exports: [
    DigiButton,
    DigiCalendar,
    DigiCalendarWeekView,
    DigiCardBox,
    DigiCardLink,
    DigiCode,
    DigiCodeBlock,
    DigiCodeExample,
    DigiDialog,
    DigiExpandableAccordion,
    DigiFormCheckbox,
    DigiFormFieldset,
    DigiFormFileUpload,
    DigiFormFilter,
    DigiFormInput,
    DigiFormInputSearch,
    DigiFormLabel,
    DigiFormProcessStep,
    DigiFormProcessSteps,
    DigiFormRadiobutton,
    DigiFormRadiogroup,
    DigiFormSelect,
    DigiFormTextarea,
    DigiFormValidationMessage,
    DigiIcon,
    DigiIconBars,
    DigiIconCheck,
    DigiIconCheckCircle,
    DigiIconCheckCircleReg,
    DigiIconCheckCircleRegAlt,
    DigiIconChevronDown,
    DigiIconChevronLeft,
    DigiIconChevronRight,
    DigiIconChevronUp,
    DigiIconCopy,
    DigiIconDangerOutline,
    DigiIconDownload,
    DigiIconExclamationCircle,
    DigiIconExclamationCircleFilled,
    DigiIconExclamationTriangle,
    DigiIconExclamationTriangleWarning,
    DigiIconExternalLinkAlt,
    DigiIconMinus,
    DigiIconPaperclip,
    DigiIconPlus,
    DigiIconSearch,
    DigiIconSpinner,
    DigiIconTrash,
    DigiIconX,
    DigiLayoutBlock,
    DigiLayoutColumns,
    DigiLayoutContainer,
    DigiLayoutGrid,
    DigiLayoutMediaObject,
    DigiLayoutRows,
    DigiLayoutStackedBlocks,
    DigiLink,
    DigiLinkExternal,
    DigiLinkIcon,
    DigiLinkInternal,
    DigiListLink,
    DigiLoaderSpinner,
    DigiLogo,
    DigiLogoService,
    DigiLogoSister,
    DigiMediaFigure,
    DigiMediaImage,
    DigiNavigationBreadcrumbs,
    DigiNavigationContextMenu,
    DigiNavigationContextMenuItem,
    DigiNavigationMainMenu,
    DigiNavigationMainMenuPanel,
    DigiNavigationPagination,
    DigiNavigationSidebar,
    DigiNavigationSidebarButton,
    DigiNavigationTab,
    DigiNavigationTabInABox,
    DigiNavigationTabs,
    DigiNavigationToc,
    DigiNavigationVerticalMenu,
    DigiNavigationVerticalMenuItem,
    DigiNotificationAlert,
    DigiNotificationCookie,
    DigiNotificationDetail,
    DigiPage,
    DigiPageBlockCards,
    DigiPageBlockHero,
    DigiPageBlockLists,
    DigiPageBlockSidebar,
    DigiPageFooter,
    DigiPageHeader,
    DigiProgressStep,
    DigiProgressSteps,
    DigiProgressbar,
    DigiTable,
    DigiTag,
    DigiTypography,
    DigiTypographyHeadingSection,
    DigiTypographyMeta,
    DigiTypographyPreamble,
    DigiTypographyTime,
    DigiUtilBreakpointObserver,
    DigiUtilDetectClickOutside,
    DigiUtilDetectFocusOutside,
    DigiUtilIntersectionObserver,
    DigiUtilKeydownHandler,
    DigiUtilKeyupHandler,
    DigiUtilMutationObserver,
    DigiUtilResizeObserver,
    BooleanValueAccessor,
    NumericValueAccessor,
    RadioValueAccessor,
    SelectValueAccessor,
    TextValueAccessor,
  ],
});
DigiSkolverketAngularModule.ɵinj = i0.ɵɵngDeclareInjector({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiSkolverketAngularModule,
});
i0.ɵɵngDeclareClassMetadata({
  minVersion: "12.0.0",
  version: "14.1.3",
  ngImport: i0,
  type: DigiSkolverketAngularModule,
  decorators: [
    {
      type: NgModule,
      args: [
        {
          declarations: [COMPONENTS, VALUEACCESSORS],
          exports: [COMPONENTS, VALUEACCESSORS],
        },
      ],
    },
  ],
});

// DIRECTIVES

/**
 * Generated bundle index. Do not edit.
 */

export {
  BooleanValueAccessor,
  DigiButton,
  DigiCalendar,
  DigiCalendarWeekView,
  DigiCardBox,
  DigiCardLink,
  DigiCode,
  DigiCodeBlock,
  DigiCodeExample,
  DigiDialog,
  DigiExpandableAccordion,
  DigiFormCheckbox,
  DigiFormFieldset,
  DigiFormFileUpload,
  DigiFormFilter,
  DigiFormInput,
  DigiFormInputSearch,
  DigiFormLabel,
  DigiFormProcessStep,
  DigiFormProcessSteps,
  DigiFormRadiobutton,
  DigiFormRadiogroup,
  DigiFormSelect,
  DigiFormTextarea,
  DigiFormValidationMessage,
  DigiIcon,
  DigiIconBars,
  DigiIconCheck,
  DigiIconCheckCircle,
  DigiIconCheckCircleReg,
  DigiIconCheckCircleRegAlt,
  DigiIconChevronDown,
  DigiIconChevronLeft,
  DigiIconChevronRight,
  DigiIconChevronUp,
  DigiIconCopy,
  DigiIconDangerOutline,
  DigiIconDownload,
  DigiIconExclamationCircle,
  DigiIconExclamationCircleFilled,
  DigiIconExclamationTriangle,
  DigiIconExclamationTriangleWarning,
  DigiIconExternalLinkAlt,
  DigiIconMinus,
  DigiIconPaperclip,
  DigiIconPlus,
  DigiIconSearch,
  DigiIconSpinner,
  DigiIconTrash,
  DigiIconX,
  DigiLayoutBlock,
  DigiLayoutColumns,
  DigiLayoutContainer,
  DigiLayoutGrid,
  DigiLayoutMediaObject,
  DigiLayoutRows,
  DigiLayoutStackedBlocks,
  DigiLink,
  DigiLinkExternal,
  DigiLinkIcon,
  DigiLinkInternal,
  DigiListLink,
  DigiLoaderSpinner,
  DigiLogo,
  DigiLogoService,
  DigiLogoSister,
  DigiMediaFigure,
  DigiMediaImage,
  DigiNavigationBreadcrumbs,
  DigiNavigationContextMenu,
  DigiNavigationContextMenuItem,
  DigiNavigationMainMenu,
  DigiNavigationMainMenuPanel,
  DigiNavigationPagination,
  DigiNavigationSidebar,
  DigiNavigationSidebarButton,
  DigiNavigationTab,
  DigiNavigationTabInABox,
  DigiNavigationTabs,
  DigiNavigationToc,
  DigiNavigationVerticalMenu,
  DigiNavigationVerticalMenuItem,
  DigiNotificationAlert,
  DigiNotificationCookie,
  DigiNotificationDetail,
  DigiPage,
  DigiPageBlockCards,
  DigiPageBlockHero,
  DigiPageBlockLists,
  DigiPageBlockSidebar,
  DigiPageFooter,
  DigiPageHeader,
  DigiProgressStep,
  DigiProgressSteps,
  DigiProgressbar,
  DigiSkolverketAngularModule,
  DigiTable,
  DigiTag,
  DigiTypography,
  DigiTypographyHeadingSection,
  DigiTypographyMeta,
  DigiTypographyPreamble,
  DigiTypographyTime,
  DigiUtilBreakpointObserver,
  DigiUtilDetectClickOutside,
  DigiUtilDetectFocusOutside,
  DigiUtilIntersectionObserver,
  DigiUtilKeydownHandler,
  DigiUtilKeyupHandler,
  DigiUtilMutationObserver,
  DigiUtilResizeObserver,
  NumericValueAccessor,
  RadioValueAccessor,
  SelectValueAccessor,
  TextValueAccessor,
};
//# sourceMappingURL=digi-skolverket-angular.mjs.map
