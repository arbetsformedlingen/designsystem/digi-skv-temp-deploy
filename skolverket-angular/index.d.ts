export * from './lib/stencil-generated/components';
export * from './lib/stencil-generated/boolean-value-accessor';
export * from './lib/stencil-generated/number-value-accessor';
export * from './lib/stencil-generated/radio-value-accessor';
export * from './lib/stencil-generated/select-value-accessor';
export * from './lib/stencil-generated/text-value-accessor';
export { DigiSkolverketAngularModule } from './lib/digi-skolverket-angular.module';
