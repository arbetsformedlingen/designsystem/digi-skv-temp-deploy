import {
  ChangeDetectorRef,
  ElementRef,
  EventEmitter,
  NgZone,
} from '@angular/core';
import type { Components } from '../../../skolverket/components';
import * as i0 from '@angular/core';
export declare interface DigiButton extends Components.DigiButton {
  /**
   * Buttonelementets 'onclick'-event. @en The button element's 'onclick' event.
   */
  afOnClick: EventEmitter<CustomEvent<MouseEvent>>;
  /**
   * Buttonelementets 'onfocus'-event. @en The button element's 'onfocus' event.
   */
  afOnFocus: EventEmitter<CustomEvent<FocusEvent>>;
  /**
   * Buttonelementets 'onblur'-event. @en The button element's 'onblur' event.
   */
  afOnBlur: EventEmitter<CustomEvent<any>>;
}
export declare class DigiButton {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiButton, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiButton,
    'digi-button',
    never,
    {
      afAriaControls: 'afAriaControls';
      afAriaExpanded: 'afAriaExpanded';
      afAriaHaspopup: 'afAriaHaspopup';
      afAriaLabel: 'afAriaLabel';
      afAriaLabelledby: 'afAriaLabelledby';
      afAriaPressed: 'afAriaPressed';
      afAutofocus: 'afAutofocus';
      afDir: 'afDir';
      afForm: 'afForm';
      afFullWidth: 'afFullWidth';
      afId: 'afId';
      afLang: 'afLang';
      afSize: 'afSize';
      afTabindex: 'afTabindex';
      afType: 'afType';
      afVariation: 'afVariation';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiCalendar extends Components.DigiCalendar {
  /**
   * Sker när ett datum har valts eller avvalts. Returnerar datumen i en array. @en When a date is selected. Return the dates in an array.
   */
  afOnDateSelectedChange: EventEmitter<CustomEvent<any>>;
  /**
   * Sker när fokus flyttas utanför kalendern @en When focus moves out from the calendar
   */
  afOnFocusOutside: EventEmitter<CustomEvent<any>>;
  /**
   * Sker vid klick utanför kalendern @en When click outside the calendar
   */
  afOnClickOutside: EventEmitter<CustomEvent<any>>;
  /**
   * Sker när kalenderdatumen har rörts första gången, när värdet på focusedDate har ändrats första gången. @en When the calendar dates are touched the first time, when focusedDate is changed the first time.
   */
  afOnDirty: EventEmitter<CustomEvent<any>>;
}
export declare class DigiCalendar {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiCalendar, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiCalendar,
    'digi-calendar',
    never,
    {
      afActive: 'afActive';
      afId: 'afId';
      afInitSelectedMonth: 'afInitSelectedMonth';
      afMultipleDates: 'afMultipleDates';
      afSelectedDate: 'afSelectedDate';
      afShowWeekNumber: 'afShowWeekNumber';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiCalendarWeekView
  extends Components.DigiCalendarWeekView {
  /**
   * Vid byte av vecka @en When week changes
   */
  afOnWeekChange: EventEmitter<CustomEvent<string>>;
  /**
   * Vid byte av dag @en When day changes
   */
  afOnDateChange: EventEmitter<CustomEvent<string>>;
}
export declare class DigiCalendarWeekView {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiCalendarWeekView, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiCalendarWeekView,
    'digi-calendar-week-view',
    never,
    {
      afDates: 'afDates';
      afHeadingLevel: 'afHeadingLevel';
      afId: 'afId';
      afMaxWeek: 'afMaxWeek';
      afMinWeek: 'afMinWeek';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiCardBox extends Components.DigiCardBox {}
export declare class DigiCardBox {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiCardBox, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiCardBox,
    'digi-card-box',
    never,
    { afGutter: 'afGutter'; afWidth: 'afWidth' },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiCardLink extends Components.DigiCardLink {}
export declare class DigiCardLink {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiCardLink, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiCardLink,
    'digi-card-link',
    never,
    {},
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiCode extends Components.DigiCode {}
export declare class DigiCode {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiCode, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiCode,
    'digi-code',
    never,
    {
      afCode: 'afCode';
      afLang: 'afLang';
      afLanguage: 'afLanguage';
      afVariation: 'afVariation';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiCodeBlock extends Components.DigiCodeBlock {}
export declare class DigiCodeBlock {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiCodeBlock, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiCodeBlock,
    'digi-code-block',
    never,
    {
      afCode: 'afCode';
      afHideToolbar: 'afHideToolbar';
      afLang: 'afLang';
      afLanguage: 'afLanguage';
      afVariation: 'afVariation';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiCodeExample extends Components.DigiCodeExample {}
export declare class DigiCodeExample {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiCodeExample, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiCodeExample,
    'digi-code-example',
    never,
    {
      afCode: 'afCode';
      afCodeBlockVariation: 'afCodeBlockVariation';
      afExampleVariation: 'afExampleVariation';
      afHideCode: 'afHideCode';
      afHideControls: 'afHideControls';
      afLanguage: 'afLanguage';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiDialog extends Components.DigiDialog {
  /**
   *
   */
  afOnOpen: EventEmitter<CustomEvent<any>>;
  /**
   *
   */
  afOnClose: EventEmitter<CustomEvent<any>>;
}
export declare class DigiDialog {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiDialog, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiDialog,
    'digi-dialog',
    never,
    { afHideCloseButton: 'afHideCloseButton'; afOpen: 'afOpen' },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiExpandableAccordion
  extends Components.DigiExpandableAccordion {
  /**
   * Buttonelementets 'onclick'-event. @en The button element's 'onclick' event.
   */
  afOnClick: EventEmitter<CustomEvent<MouseEvent>>;
}
export declare class DigiExpandableAccordion {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiExpandableAccordion, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiExpandableAccordion,
    'digi-expandable-accordion',
    never,
    {
      afAnimation: 'afAnimation';
      afExpanded: 'afExpanded';
      afHeading: 'afHeading';
      afHeadingLevel: 'afHeadingLevel';
      afId: 'afId';
      afVariation: 'afVariation';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiFormCheckbox extends Components.DigiFormCheckbox {
  /**
   * Inputelementets 'onchange'-event. @en The input element's 'onchange' event.
   */
  afOnChange: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onblur'-event. @en The input element's 'onblur' event.
   */
  afOnBlur: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onfocus'-event. @en The input element's 'onfocus' event.
   */
  afOnFocus: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onfocusout'-event. @en The input element's 'onfocusout' event.
   */
  afOnFocusout: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'oninput'-event. @en The input element's 'oninput' event.
   */
  afOnInput: EventEmitter<CustomEvent<any>>;
  /**
   * Sker vid inputfältets första 'oninput' @en First time the input element receives an input
   */
  afOnDirty: EventEmitter<CustomEvent<any>>;
  /**
   * Sker vid inputfältets första 'onblur' @en First time the input element is blurred
   */
  afOnTouched: EventEmitter<CustomEvent<any>>;
}
export declare class DigiFormCheckbox {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiFormCheckbox, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiFormCheckbox,
    'digi-form-checkbox',
    never,
    {
      afAriaDescribedby: 'afAriaDescribedby';
      afAriaLabel: 'afAriaLabel';
      afAriaLabelledby: 'afAriaLabelledby';
      afAutofocus: 'afAutofocus';
      afChecked: 'afChecked';
      afDescription: 'afDescription';
      afId: 'afId';
      afIndeterminate: 'afIndeterminate';
      afLabel: 'afLabel';
      afLayout: 'afLayout';
      afName: 'afName';
      afRequired: 'afRequired';
      afValidation: 'afValidation';
      afValue: 'afValue';
      afVariation: 'afVariation';
      checked: 'checked';
      value: 'value';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiFormFieldset extends Components.DigiFormFieldset {}
export declare class DigiFormFieldset {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiFormFieldset, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiFormFieldset,
    'digi-form-fieldset',
    never,
    { afForm: 'afForm'; afId: 'afId'; afLegend: 'afLegend'; afName: 'afName' },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiFormFileUpload
  extends Components.DigiFormFileUpload {
  /**
   * Sänder ut fil vid uppladdning @en Emits file on upload
   */
  afOnUploadFile: EventEmitter<CustomEvent<any>>;
  /**
   * Sänder ut vilken fil som tagits bort @en Emits which file that was deleted.
   */
  afOnRemoveFile: EventEmitter<CustomEvent<any>>;
  /**
   * Sänder ut vilken fil som har avbrutis uppladdning @en Emits which file that was canceled
   */
  afOnCancelFile: EventEmitter<CustomEvent<any>>;
  /**
   * Sänder ut vilken fil som försöker laddas upp igen @en Emits which file is trying to retry its upload
   */
  afOnRetryFile: EventEmitter<CustomEvent<any>>;
}
export declare class DigiFormFileUpload {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiFormFileUpload, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiFormFileUpload,
    'digi-form-file-upload',
    never,
    {
      afAnnounceIfOptional: 'afAnnounceIfOptional';
      afAnnounceIfOptionalText: 'afAnnounceIfOptionalText';
      afFileMaxSize: 'afFileMaxSize';
      afFileTypes: 'afFileTypes';
      afHeadingFiles: 'afHeadingFiles';
      afHeadingLevel: 'afHeadingLevel';
      afId: 'afId';
      afLabel: 'afLabel';
      afLabelDescription: 'afLabelDescription';
      afMaxFiles: 'afMaxFiles';
      afName: 'afName';
      afRequired: 'afRequired';
      afRequiredText: 'afRequiredText';
      afUploadBtnText: 'afUploadBtnText';
      afValidation: 'afValidation';
      afVariation: 'afVariation';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiFormFilter extends Components.DigiFormFilter {
  /**
   *
   */
  afOnFocusout: EventEmitter<CustomEvent<any>>;
  /**
   * Vid uppdatering av filtret @en At filter submit
   */
  afOnSubmitFilters: EventEmitter<CustomEvent<any>>;
  /**
   * När filtret stängs utan att valda alternativ bekräftats @en When the filter is closed without confirming the selected options
   */
  afOnFilterClosed: EventEmitter<CustomEvent<any>>;
  /**
   * Vid reset av filtret @en At filter reset
   */
  afOnResetFilters: EventEmitter<CustomEvent<any>>;
  /**
   * När ett filter ändras @en When a filter is changed
   */
  afOnChange: EventEmitter<CustomEvent<any>>;
}
export declare class DigiFormFilter {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiFormFilter, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiFormFilter,
    'digi-form-filter',
    never,
    {
      afAlignRight: 'afAlignRight';
      afAutofocus: 'afAutofocus';
      afFilterButtonAriaDescribedby: 'afFilterButtonAriaDescribedby';
      afFilterButtonAriaLabel: 'afFilterButtonAriaLabel';
      afFilterButtonText: 'afFilterButtonText';
      afHideResetButton: 'afHideResetButton';
      afId: 'afId';
      afName: 'afName';
      afResetButtonText: 'afResetButtonText';
      afResetButtonTextAriaLabel: 'afResetButtonTextAriaLabel';
      afSubmitButtonText: 'afSubmitButtonText';
      afSubmitButtonTextAriaLabel: 'afSubmitButtonTextAriaLabel';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiFormInput extends Components.DigiFormInput {
  /**
   * Inputelementets 'onchange'-event. @en The input element's 'onchange' event.
   */
  afOnChange: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onblur'-event. @en The input element's 'onblur' event.
   */
  afOnBlur: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onkeyup'-event. @en The input element's 'onkeyup' event.
   */
  afOnKeyup: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onfocus'-event. @en The input element's 'onfocus' event.
   */
  afOnFocus: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onfocusout'-event. @en The input element's 'onfocusout' event.
   */
  afOnFocusout: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'oninput'-event. @en The input element's 'oninput' event.
   */
  afOnInput: EventEmitter<CustomEvent<any>>;
  /**
   * Sker vid inputfältets första 'oninput' @en First time the input element receives an input
   */
  afOnDirty: EventEmitter<CustomEvent<any>>;
  /**
   * Sker vid inputfältets första 'onblur' @en First time the input element is blurred
   */
  afOnTouched: EventEmitter<CustomEvent<any>>;
}
export declare class DigiFormInput {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiFormInput, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiFormInput,
    'digi-form-input',
    never,
    {
      afAnnounceIfOptional: 'afAnnounceIfOptional';
      afAnnounceIfOptionalText: 'afAnnounceIfOptionalText';
      afAriaActivedescendant: 'afAriaActivedescendant';
      afAriaAutocomplete: 'afAriaAutocomplete';
      afAriaDescribedby: 'afAriaDescribedby';
      afAriaLabelledby: 'afAriaLabelledby';
      afAutocomplete: 'afAutocomplete';
      afAutofocus: 'afAutofocus';
      afButtonVariation: 'afButtonVariation';
      afDirname: 'afDirname';
      afId: 'afId';
      afLabel: 'afLabel';
      afLabelDescription: 'afLabelDescription';
      afList: 'afList';
      afMax: 'afMax';
      afMaxlength: 'afMaxlength';
      afMin: 'afMin';
      afMinlength: 'afMinlength';
      afName: 'afName';
      afRequired: 'afRequired';
      afRequiredText: 'afRequiredText';
      afRole: 'afRole';
      afStep: 'afStep';
      afType: 'afType';
      afValidation: 'afValidation';
      afValidationText: 'afValidationText';
      afValue: 'afValue';
      afVariation: 'afVariation';
      value: 'value';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiFormInputSearch
  extends Components.DigiFormInputSearch {
  /**
   * Vid fokus utanför komponenten. @en When focus is outside component.
   */
  afOnFocusOutside: EventEmitter<CustomEvent<any>>;
  /**
   * Vid fokus inuti komponenten. @en When focus is inside component.
   */
  afOnFocusInside: EventEmitter<CustomEvent<any>>;
  /**
   * Vid klick utanför komponenten. @en When click outside component.
   */
  afOnClickOutside: EventEmitter<CustomEvent<any>>;
  /**
   * Vid klick inuti komponenten. @en When click inside component.
   */
  afOnClickInside: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onchange'-event. @en The input element's 'onchange' event.
   */
  afOnChange: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onblur'-event. @en The input element's 'onblur' event.
   */
  afOnBlur: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onkeyup'-event. @en The input element's 'onkeyup' event.
   */
  afOnKeyup: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onfocus'-event. @en The input element's 'onfocus' event.
   */
  afOnFocus: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onfocusout'-event. @en The input element's 'onfocusout' event.
   */
  afOnFocusout: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'oninput'-event. @en The input element's 'oninput' event.
   */
  afOnInput: EventEmitter<CustomEvent<any>>;
  /**
   * Knappelementets 'onclick'-event. @en The button element's 'onclick' event.
   */
  afOnClick: EventEmitter<CustomEvent<any>>;
}
export declare class DigiFormInputSearch {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiFormInputSearch, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiFormInputSearch,
    'digi-form-input-search',
    never,
    {
      afAriaActivedescendant: 'afAriaActivedescendant';
      afAriaAutocomplete: 'afAriaAutocomplete';
      afAriaDescribedby: 'afAriaDescribedby';
      afAriaLabelledby: 'afAriaLabelledby';
      afAutocomplete: 'afAutocomplete';
      afAutofocus: 'afAutofocus';
      afButtonAriaLabel: 'afButtonAriaLabel';
      afButtonAriaLabelledby: 'afButtonAriaLabelledby';
      afButtonText: 'afButtonText';
      afButtonType: 'afButtonType';
      afButtonVariation: 'afButtonVariation';
      afHideButton: 'afHideButton';
      afId: 'afId';
      afLabel: 'afLabel';
      afLabelDescription: 'afLabelDescription';
      afName: 'afName';
      afType: 'afType';
      afValue: 'afValue';
      afVariation: 'afVariation';
      value: 'value';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiFormLabel extends Components.DigiFormLabel {}
export declare class DigiFormLabel {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiFormLabel, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiFormLabel,
    'digi-form-label',
    never,
    {
      afAnnounceIfOptional: 'afAnnounceIfOptional';
      afAnnounceIfOptionalText: 'afAnnounceIfOptionalText';
      afDescription: 'afDescription';
      afFor: 'afFor';
      afId: 'afId';
      afLabel: 'afLabel';
      afRequired: 'afRequired';
      afRequiredText: 'afRequiredText';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiFormProcessStep
  extends Components.DigiFormProcessStep {
  /**
   *
   */
  afClick: EventEmitter<CustomEvent<MouseEvent>>;
}
export declare class DigiFormProcessStep {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiFormProcessStep, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiFormProcessStep,
    'digi-form-process-step',
    never,
    {
      afContext: 'afContext';
      afHref: 'afHref';
      afLabel: 'afLabel';
      afType: 'afType';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiFormProcessSteps
  extends Components.DigiFormProcessSteps {}
export declare class DigiFormProcessSteps {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiFormProcessSteps, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiFormProcessSteps,
    'digi-form-process-steps',
    never,
    { afCurrentStep: 'afCurrentStep'; afId: 'afId' },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiFormRadiobutton
  extends Components.DigiFormRadiobutton {
  /**
   * Inputelementets 'onchange'-event. @en The input element's 'onchange' event.
   */
  afOnChange: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onblur'-event. @en The input element's 'onblur' event.
   */
  afOnBlur: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onfocus'-event. @en The input element's 'onfocus' event.
   */
  afOnFocus: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'onfocusout'-event. @en The input element's 'onfocusout' event.
   */
  afOnFocusout: EventEmitter<CustomEvent<any>>;
  /**
   * Inputelementets 'oninput'-event. @en The input element's 'oninput' event.
   */
  afOnInput: EventEmitter<CustomEvent<any>>;
  /**
   * Sker vid inputfältets första 'oninput' @en First time the input element receives an input
   */
  afOnDirty: EventEmitter<CustomEvent<any>>;
  /**
   * Sker vid inputfältets första 'onblur' @en First time the input element is blurred
   */
  afOnTouched: EventEmitter<CustomEvent<any>>;
}
export declare class DigiFormRadiobutton {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiFormRadiobutton, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiFormRadiobutton,
    'digi-form-radiobutton',
    never,
    {
      afAriaDescribedby: 'afAriaDescribedby';
      afAriaLabelledby: 'afAriaLabelledby';
      afAutofocus: 'afAutofocus';
      afChecked: 'afChecked';
      afId: 'afId';
      afLabel: 'afLabel';
      afLayout: 'afLayout';
      afName: 'afName';
      afRequired: 'afRequired';
      afValidation: 'afValidation';
      afValue: 'afValue';
      afVariation: 'afVariation';
      checked: 'checked';
      value: 'value';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiFormRadiogroup
  extends Components.DigiFormRadiogroup {
  /**
   * Event när värdet ändras @en Event when value changes
   */
  afOnGroupChange: EventEmitter<CustomEvent<string>>;
}
export declare class DigiFormRadiogroup {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiFormRadiogroup, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiFormRadiogroup,
    'digi-form-radiogroup',
    never,
    { afName: 'afName'; afValue: 'afValue'; value: 'value' },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiFormSelect extends Components.DigiFormSelect {
  /**
   *
   */
  afOnChange: EventEmitter<CustomEvent<any>>;
  /**
   *
   */
  afOnFocus: EventEmitter<CustomEvent<any>>;
  /**
   *
   */
  afOnFocusout: EventEmitter<CustomEvent<any>>;
  /**
   *
   */
  afOnBlur: EventEmitter<CustomEvent<any>>;
  /**
   * Sker första gången väljarelementet ändras. @en First time the select element is changed.
   */
  afOnDirty: EventEmitter<CustomEvent<any>>;
}
export declare class DigiFormSelect {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiFormSelect, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiFormSelect,
    'digi-form-select',
    never,
    {
      afAnnounceIfOptional: 'afAnnounceIfOptional';
      afAnnounceIfOptionalText: 'afAnnounceIfOptionalText';
      afAutofocus: 'afAutofocus';
      afDescription: 'afDescription';
      afDisableValidation: 'afDisableValidation';
      afId: 'afId';
      afLabel: 'afLabel';
      afName: 'afName';
      afPlaceholder: 'afPlaceholder';
      afRequired: 'afRequired';
      afRequiredText: 'afRequiredText';
      afStartSelected: 'afStartSelected';
      afValidation: 'afValidation';
      afValidationText: 'afValidationText';
      afValue: 'afValue';
      afVariation: 'afVariation';
      value: 'value';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiFormTextarea extends Components.DigiFormTextarea {
  /**
   * Textareatelementets 'onchange'-event. @en The textarea element's 'onchange' event.
   */
  afOnChange: EventEmitter<CustomEvent<any>>;
  /**
   * Textareatelementets 'onblur'-event. @en The textarea element's 'onblur' event.
   */
  afOnBlur: EventEmitter<CustomEvent<any>>;
  /**
   * Textareatelementets 'onkeyup'-event. @en The textarea element's 'onkeyup' event.
   */
  afOnKeyup: EventEmitter<CustomEvent<any>>;
  /**
   * Textareatelementets 'onfocus'-event. @en The textarea element's 'onfocus' event.
   */
  afOnFocus: EventEmitter<CustomEvent<any>>;
  /**
   * Textareatelementets 'onfocusout'-event. @en The textarea element's 'onfocusout' event.
   */
  afOnFocusout: EventEmitter<CustomEvent<any>>;
  /**
   * Textareatelementets 'oninput'-event. @en The textarea element's 'oninput' event.
   */
  afOnInput: EventEmitter<CustomEvent<any>>;
  /**
   * Sker vid textareaelementets första 'oninput' @en First time the textarea element receives an input
   */
  afOnDirty: EventEmitter<CustomEvent<any>>;
  /**
   * Sker vid textareaelementets första 'onblur' @en First time the textelement is blurred
   */
  afOnTouched: EventEmitter<CustomEvent<any>>;
}
export declare class DigiFormTextarea {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiFormTextarea, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiFormTextarea,
    'digi-form-textarea',
    never,
    {
      afAnnounceIfOptional: 'afAnnounceIfOptional';
      afAnnounceIfOptionalText: 'afAnnounceIfOptionalText';
      afAriaActivedescendant: 'afAriaActivedescendant';
      afAriaDescribedby: 'afAriaDescribedby';
      afAriaLabelledby: 'afAriaLabelledby';
      afAutofocus: 'afAutofocus';
      afId: 'afId';
      afLabel: 'afLabel';
      afLabelDescription: 'afLabelDescription';
      afMaxlength: 'afMaxlength';
      afMinlength: 'afMinlength';
      afName: 'afName';
      afRequired: 'afRequired';
      afRequiredText: 'afRequiredText';
      afRole: 'afRole';
      afValidation: 'afValidation';
      afValidationText: 'afValidationText';
      afValue: 'afValue';
      afVariation: 'afVariation';
      value: 'value';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiFormValidationMessage
  extends Components.DigiFormValidationMessage {}
export declare class DigiFormValidationMessage {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiFormValidationMessage, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiFormValidationMessage,
    'digi-form-validation-message',
    never,
    { afVariation: 'afVariation' },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiIcon extends Components.DigiIcon {}
export declare class DigiIcon {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiIcon, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiIcon,
    'digi-icon',
    never,
    {
      afDesc: 'afDesc';
      afName: 'afName';
      afSvgAriaHidden: 'afSvgAriaHidden';
      afTitle: 'afTitle';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiIconBars extends Components.DigiIconBars {}
export declare class DigiIconBars {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiIconBars, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiIconBars,
    'digi-icon-bars',
    never,
    {
      afDesc: 'afDesc';
      afSvgAriaHidden: 'afSvgAriaHidden';
      afTitle: 'afTitle';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiIconCheck extends Components.DigiIconCheck {}
export declare class DigiIconCheck {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiIconCheck, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiIconCheck,
    'digi-icon-check',
    never,
    {
      afDesc: 'afDesc';
      afSvgAriaHidden: 'afSvgAriaHidden';
      afTitle: 'afTitle';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiIconCheckCircle
  extends Components.DigiIconCheckCircle {}
export declare class DigiIconCheckCircle {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiIconCheckCircle, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiIconCheckCircle,
    'digi-icon-check-circle',
    never,
    {
      afDesc: 'afDesc';
      afSvgAriaHidden: 'afSvgAriaHidden';
      afTitle: 'afTitle';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiIconCheckCircleReg
  extends Components.DigiIconCheckCircleReg {}
export declare class DigiIconCheckCircleReg {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiIconCheckCircleReg, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiIconCheckCircleReg,
    'digi-icon-check-circle-reg',
    never,
    {
      afDesc: 'afDesc';
      afSvgAriaHidden: 'afSvgAriaHidden';
      afTitle: 'afTitle';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiIconCheckCircleRegAlt
  extends Components.DigiIconCheckCircleRegAlt {}
export declare class DigiIconCheckCircleRegAlt {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiIconCheckCircleRegAlt, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiIconCheckCircleRegAlt,
    'digi-icon-check-circle-reg-alt',
    never,
    {
      afDesc: 'afDesc';
      afSvgAriaHidden: 'afSvgAriaHidden';
      afTitle: 'afTitle';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiIconChevronDown
  extends Components.DigiIconChevronDown {}
export declare class DigiIconChevronDown {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiIconChevronDown, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiIconChevronDown,
    'digi-icon-chevron-down',
    never,
    {
      afDesc: 'afDesc';
      afSvgAriaHidden: 'afSvgAriaHidden';
      afTitle: 'afTitle';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiIconChevronLeft
  extends Components.DigiIconChevronLeft {}
export declare class DigiIconChevronLeft {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiIconChevronLeft, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiIconChevronLeft,
    'digi-icon-chevron-left',
    never,
    {
      afDesc: 'afDesc';
      afSvgAriaHidden: 'afSvgAriaHidden';
      afTitle: 'afTitle';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiIconChevronRight
  extends Components.DigiIconChevronRight {}
export declare class DigiIconChevronRight {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiIconChevronRight, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiIconChevronRight,
    'digi-icon-chevron-right',
    never,
    {
      afDesc: 'afDesc';
      afSvgAriaHidden: 'afSvgAriaHidden';
      afTitle: 'afTitle';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiIconChevronUp
  extends Components.DigiIconChevronUp {}
export declare class DigiIconChevronUp {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiIconChevronUp, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiIconChevronUp,
    'digi-icon-chevron-up',
    never,
    {
      afDesc: 'afDesc';
      afSvgAriaHidden: 'afSvgAriaHidden';
      afTitle: 'afTitle';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiIconCopy extends Components.DigiIconCopy {}
export declare class DigiIconCopy {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiIconCopy, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiIconCopy,
    'digi-icon-copy',
    never,
    {
      afDesc: 'afDesc';
      afSvgAriaHidden: 'afSvgAriaHidden';
      afTitle: 'afTitle';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiIconDangerOutline
  extends Components.DigiIconDangerOutline {}
export declare class DigiIconDangerOutline {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiIconDangerOutline, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiIconDangerOutline,
    'digi-icon-danger-outline',
    never,
    {
      afDesc: 'afDesc';
      afSvgAriaHidden: 'afSvgAriaHidden';
      afTitle: 'afTitle';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiIconDownload extends Components.DigiIconDownload {}
export declare class DigiIconDownload {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiIconDownload, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiIconDownload,
    'digi-icon-download',
    never,
    {
      afDesc: 'afDesc';
      afSvgAriaHidden: 'afSvgAriaHidden';
      afTitle: 'afTitle';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiIconExclamationCircle
  extends Components.DigiIconExclamationCircle {}
export declare class DigiIconExclamationCircle {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiIconExclamationCircle, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiIconExclamationCircle,
    'digi-icon-exclamation-circle',
    never,
    {
      afDesc: 'afDesc';
      afSvgAriaHidden: 'afSvgAriaHidden';
      afTitle: 'afTitle';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiIconExclamationCircleFilled
  extends Components.DigiIconExclamationCircleFilled {}
export declare class DigiIconExclamationCircleFilled {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiIconExclamationCircleFilled, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiIconExclamationCircleFilled,
    'digi-icon-exclamation-circle-filled',
    never,
    {
      afDesc: 'afDesc';
      afSvgAriaHidden: 'afSvgAriaHidden';
      afTitle: 'afTitle';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiIconExclamationTriangle
  extends Components.DigiIconExclamationTriangle {}
export declare class DigiIconExclamationTriangle {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiIconExclamationTriangle, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiIconExclamationTriangle,
    'digi-icon-exclamation-triangle',
    never,
    {
      afDesc: 'afDesc';
      afSvgAriaHidden: 'afSvgAriaHidden';
      afTitle: 'afTitle';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiIconExclamationTriangleWarning
  extends Components.DigiIconExclamationTriangleWarning {}
export declare class DigiIconExclamationTriangleWarning {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<
    DigiIconExclamationTriangleWarning,
    never
  >;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiIconExclamationTriangleWarning,
    'digi-icon-exclamation-triangle-warning',
    never,
    {
      afDesc: 'afDesc';
      afSvgAriaHidden: 'afSvgAriaHidden';
      afTitle: 'afTitle';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiIconExternalLinkAlt
  extends Components.DigiIconExternalLinkAlt {}
export declare class DigiIconExternalLinkAlt {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiIconExternalLinkAlt, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiIconExternalLinkAlt,
    'digi-icon-external-link-alt',
    never,
    {
      afDesc: 'afDesc';
      afSvgAriaHidden: 'afSvgAriaHidden';
      afTitle: 'afTitle';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiIconMinus extends Components.DigiIconMinus {}
export declare class DigiIconMinus {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiIconMinus, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiIconMinus,
    'digi-icon-minus',
    never,
    {
      afDesc: 'afDesc';
      afSvgAriaHidden: 'afSvgAriaHidden';
      afTitle: 'afTitle';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiIconPaperclip
  extends Components.DigiIconPaperclip {}
export declare class DigiIconPaperclip {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiIconPaperclip, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiIconPaperclip,
    'digi-icon-paperclip',
    never,
    {
      afDesc: 'afDesc';
      afSvgAriaHidden: 'afSvgAriaHidden';
      afTitle: 'afTitle';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiIconPlus extends Components.DigiIconPlus {}
export declare class DigiIconPlus {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiIconPlus, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiIconPlus,
    'digi-icon-plus',
    never,
    {
      afDesc: 'afDesc';
      afSvgAriaHidden: 'afSvgAriaHidden';
      afTitle: 'afTitle';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiIconSearch extends Components.DigiIconSearch {}
export declare class DigiIconSearch {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiIconSearch, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiIconSearch,
    'digi-icon-search',
    never,
    {
      afDesc: 'afDesc';
      afSvgAriaHidden: 'afSvgAriaHidden';
      afTitle: 'afTitle';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiIconSpinner extends Components.DigiIconSpinner {}
export declare class DigiIconSpinner {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiIconSpinner, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiIconSpinner,
    'digi-icon-spinner',
    never,
    {
      afDesc: 'afDesc';
      afSvgAriaHidden: 'afSvgAriaHidden';
      afTitle: 'afTitle';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiIconTrash extends Components.DigiIconTrash {}
export declare class DigiIconTrash {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiIconTrash, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiIconTrash,
    'digi-icon-trash',
    never,
    {
      afDesc: 'afDesc';
      afSvgAriaHidden: 'afSvgAriaHidden';
      afTitle: 'afTitle';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiIconX extends Components.DigiIconX {}
export declare class DigiIconX {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiIconX, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiIconX,
    'digi-icon-x',
    never,
    {
      afDesc: 'afDesc';
      afSvgAriaHidden: 'afSvgAriaHidden';
      afTitle: 'afTitle';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiLayoutBlock extends Components.DigiLayoutBlock {}
export declare class DigiLayoutBlock {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiLayoutBlock, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiLayoutBlock,
    'digi-layout-block',
    never,
    {
      afContainer: 'afContainer';
      afMarginBottom: 'afMarginBottom';
      afMarginTop: 'afMarginTop';
      afVariation: 'afVariation';
      afVerticalPadding: 'afVerticalPadding';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiLayoutColumns
  extends Components.DigiLayoutColumns {}
export declare class DigiLayoutColumns {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiLayoutColumns, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiLayoutColumns,
    'digi-layout-columns',
    never,
    { afElement: 'afElement'; afVariation: 'afVariation' },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiLayoutContainer
  extends Components.DigiLayoutContainer {}
export declare class DigiLayoutContainer {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiLayoutContainer, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiLayoutContainer,
    'digi-layout-container',
    never,
    {
      afMarginBottom: 'afMarginBottom';
      afMarginTop: 'afMarginTop';
      afNoGutter: 'afNoGutter';
      afVariation: 'afVariation';
      afVerticalPadding: 'afVerticalPadding';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiLayoutGrid extends Components.DigiLayoutGrid {}
export declare class DigiLayoutGrid {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiLayoutGrid, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiLayoutGrid,
    'digi-layout-grid',
    never,
    { afVerticalSpacing: 'afVerticalSpacing' },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiLayoutMediaObject
  extends Components.DigiLayoutMediaObject {}
export declare class DigiLayoutMediaObject {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiLayoutMediaObject, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiLayoutMediaObject,
    'digi-layout-media-object',
    never,
    { afAlignment: 'afAlignment' },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiLayoutRows extends Components.DigiLayoutRows {}
export declare class DigiLayoutRows {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiLayoutRows, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiLayoutRows,
    'digi-layout-rows',
    never,
    {},
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiLayoutStackedBlocks
  extends Components.DigiLayoutStackedBlocks {}
export declare class DigiLayoutStackedBlocks {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiLayoutStackedBlocks, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiLayoutStackedBlocks,
    'digi-layout-stacked-blocks',
    never,
    { afVariation: 'afVariation' },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiLink extends Components.DigiLink {
  /**
   * Länkelementets 'onclick'-event. @en The link element's 'onclick' event.
   */
  afOnClick: EventEmitter<CustomEvent<MouseEvent>>;
}
export declare class DigiLink {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiLink, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiLink,
    'digi-link',
    never,
    {
      afHref: 'afHref';
      afOverrideLink: 'afOverrideLink';
      afTarget: 'afTarget';
      afVariation: 'afVariation';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiLinkExternal extends Components.DigiLinkExternal {
  /**
   * Länkelementets 'onclick'-event. @en The link element's 'onclick' event.
   */
  afOnClick: EventEmitter<CustomEvent<MouseEvent>>;
}
export declare class DigiLinkExternal {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiLinkExternal, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiLinkExternal,
    'digi-link-external',
    never,
    {
      afHref: 'afHref';
      afOverrideLink: 'afOverrideLink';
      afTarget: 'afTarget';
      afVariation: 'afVariation';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiLinkIcon extends Components.DigiLinkIcon {}
export declare class DigiLinkIcon {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiLinkIcon, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiLinkIcon,
    'digi-link-icon',
    never,
    {},
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiLinkInternal extends Components.DigiLinkInternal {
  /**
   * Länkelementets 'onclick'-event. @en The link element's 'onclick' event.
   */
  afOnClick: EventEmitter<CustomEvent<MouseEvent>>;
}
export declare class DigiLinkInternal {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiLinkInternal, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiLinkInternal,
    'digi-link-internal',
    never,
    {
      afHref: 'afHref';
      afOverrideLink: 'afOverrideLink';
      afVariation: 'afVariation';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiListLink extends Components.DigiListLink {}
export declare class DigiListLink {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiListLink, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiListLink,
    'digi-list-link',
    never,
    { afLayout: 'afLayout'; afType: 'afType'; afVariation: 'afVariation' },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiLoaderSpinner
  extends Components.DigiLoaderSpinner {}
export declare class DigiLoaderSpinner {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiLoaderSpinner, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiLoaderSpinner,
    'digi-loader-spinner',
    never,
    { afSize: 'afSize'; afText: 'afText' },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiLogo extends Components.DigiLogo {}
export declare class DigiLogo {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiLogo, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiLogo,
    'digi-logo',
    never,
    {
      afDesc: 'afDesc';
      afSvgAriaHidden: 'afSvgAriaHidden';
      afTitle: 'afTitle';
      afTitleId: 'afTitleId';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiLogoService extends Components.DigiLogoService {}
export declare class DigiLogoService {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiLogoService, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiLogoService,
    'digi-logo-service',
    never,
    { afDescription: 'afDescription'; afName: 'afName'; afNameId: 'afNameId' },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiLogoSister extends Components.DigiLogoSister {}
export declare class DigiLogoSister {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiLogoSister, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiLogoSister,
    'digi-logo-sister',
    never,
    { afName: 'afName'; afNameId: 'afNameId' },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiMediaFigure extends Components.DigiMediaFigure {}
export declare class DigiMediaFigure {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiMediaFigure, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiMediaFigure,
    'digi-media-figure',
    never,
    { afAlignment: 'afAlignment'; afFigcaption: 'afFigcaption' },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiMediaImage extends Components.DigiMediaImage {
  /**
   * Bildlementets 'onload'-event. @en The image element's 'onload' event.
   */
  afOnLoad: EventEmitter<CustomEvent<any>>;
}
export declare class DigiMediaImage {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiMediaImage, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiMediaImage,
    'digi-media-image',
    never,
    {
      afAlt: 'afAlt';
      afAriaLabel: 'afAriaLabel';
      afFullwidth: 'afFullwidth';
      afHeight: 'afHeight';
      afObserverOptions: 'afObserverOptions';
      afSrc: 'afSrc';
      afSrcset: 'afSrcset';
      afTitle: 'afTitle';
      afUnlazy: 'afUnlazy';
      afWidth: 'afWidth';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiNavigationBreadcrumbs
  extends Components.DigiNavigationBreadcrumbs {}
export declare class DigiNavigationBreadcrumbs {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiNavigationBreadcrumbs, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiNavigationBreadcrumbs,
    'digi-navigation-breadcrumbs',
    never,
    {
      afAriaLabel: 'afAriaLabel';
      afCurrentPage: 'afCurrentPage';
      afVariation: 'afVariation';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiNavigationContextMenu
  extends Components.DigiNavigationContextMenu {
  /**
   * När komponenten stängs @en When component gets inactive
   */
  afOnInactive: EventEmitter<CustomEvent<any>>;
  /**
   * När komponenten öppnas @en When component gets active
   */
  afOnActive: EventEmitter<CustomEvent<any>>;
  /**
   * När fokus sätts utanför komponenten @en When focus is move outside of component
   */
  afOnBlur: EventEmitter<CustomEvent<any>>;
  /**
   * Vid navigering till nytt listobjekt @en When navigating to a new list item
   */
  afOnChange: EventEmitter<CustomEvent<any>>;
  /**
   * Toggleknappens 'onclick'-event @en The toggle button's 'onclick'-event
   */
  afOnToggle: EventEmitter<CustomEvent<any>>;
  /**
   * 'onclick'-event på knappelementen i listan @en List item buttons' 'onclick'-event
   */
  afOnSelect: EventEmitter<CustomEvent<any>>;
}
export declare class DigiNavigationContextMenu {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiNavigationContextMenu, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiNavigationContextMenu,
    'digi-navigation-context-menu',
    never,
    {
      afIcon: 'afIcon';
      afId: 'afId';
      afNavigationContextMenuItems: 'afNavigationContextMenuItems';
      afStartSelected: 'afStartSelected';
      afText: 'afText';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiNavigationContextMenuItem
  extends Components.DigiNavigationContextMenuItem {}
export declare class DigiNavigationContextMenuItem {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiNavigationContextMenuItem, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiNavigationContextMenuItem,
    'digi-navigation-context-menu-item',
    never,
    {
      afDir: 'afDir';
      afHref: 'afHref';
      afLang: 'afLang';
      afText: 'afText';
      afType: 'afType';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiNavigationMainMenu
  extends Components.DigiNavigationMainMenu {}
export declare class DigiNavigationMainMenu {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiNavigationMainMenu, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiNavigationMainMenu,
    'digi-navigation-main-menu',
    never,
    { afId: 'afId' },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiNavigationMainMenuPanel
  extends Components.DigiNavigationMainMenuPanel {
  /**
   * När komponenten ändrar storlek @en When the component changes size
   */
  afOnResize: EventEmitter<CustomEvent<ResizeObserverEntry>>;
  /**
   * När komponenten stängs @en When the component changes size
   */
  afOnClose: EventEmitter<CustomEvent<any>>;
}
export declare class DigiNavigationMainMenuPanel {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiNavigationMainMenuPanel, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiNavigationMainMenuPanel,
    'digi-navigation-main-menu-panel',
    never,
    {},
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiNavigationPagination
  extends Components.DigiNavigationPagination {
  /**
   * Vid byte av sida @en When page changes
   */
  afOnPageChange: EventEmitter<CustomEvent<number>>;
}
export declare class DigiNavigationPagination {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiNavigationPagination, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiNavigationPagination,
    'digi-navigation-pagination',
    never,
    {
      afCurrentResultEnd: 'afCurrentResultEnd';
      afCurrentResultStart: 'afCurrentResultStart';
      afId: 'afId';
      afInitActivePage: 'afInitActivePage';
      afResultName: 'afResultName';
      afTotalPages: 'afTotalPages';
      afTotalResults: 'afTotalResults';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiNavigationSidebar
  extends Components.DigiNavigationSidebar {
  /**
   * Stängknappens 'onclick'-event @en Close button's 'onclick' event
   */
  afOnClose: EventEmitter<CustomEvent<any>>;
  /**
   * Stängning av sidofält med esc-tangenten @en At close/open of sidebar using esc-key
   */
  afOnEsc: EventEmitter<CustomEvent<any>>;
  /**
   * Stängning av sidofält med klick på skuggan bakom menyn @en At close of sidebar when clicking on backdrop
   */
  afOnBackdropClick: EventEmitter<CustomEvent<any>>;
  /**
   * Vid öppning/stängning av sidofält @en At close/open of sidebar
   */
  afOnToggle: EventEmitter<CustomEvent<any>>;
}
export declare class DigiNavigationSidebar {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiNavigationSidebar, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiNavigationSidebar,
    'digi-navigation-sidebar',
    never,
    {
      afActive: 'afActive';
      afBackdrop: 'afBackdrop';
      afCloseButtonAriaLabel: 'afCloseButtonAriaLabel';
      afCloseButtonPosition: 'afCloseButtonPosition';
      afCloseButtonText: 'afCloseButtonText';
      afCloseFocusableElement: 'afCloseFocusableElement';
      afFocusableElement: 'afFocusableElement';
      afHeading: 'afHeading';
      afHeadingLevel: 'afHeadingLevel';
      afHideHeader: 'afHideHeader';
      afId: 'afId';
      afMobilePosition: 'afMobilePosition';
      afMobileVariation: 'afMobileVariation';
      afPosition: 'afPosition';
      afStickyHeader: 'afStickyHeader';
      afVariation: 'afVariation';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiNavigationSidebarButton
  extends Components.DigiNavigationSidebarButton {
  /**
   * Toggleknappens 'onclick'-event @en The toggle button's 'onclick'-event
   */
  afOnToggle: EventEmitter<CustomEvent<any>>;
}
export declare class DigiNavigationSidebarButton {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiNavigationSidebarButton, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiNavigationSidebarButton,
    'digi-navigation-sidebar-button',
    never,
    { afAriaLabel: 'afAriaLabel'; afId: 'afId'; afText: 'afText' },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiNavigationTab
  extends Components.DigiNavigationTab {
  /**
   * När tabben växlar mellan aktiv och inaktiv @en When the tab toggles between active and inactive
   */
  afOnToggle: EventEmitter<CustomEvent<boolean>>;
}
export declare class DigiNavigationTab {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiNavigationTab, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiNavigationTab,
    'digi-navigation-tab',
    never,
    { afActive: 'afActive'; afAriaLabel: 'afAriaLabel'; afId: 'afId' },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiNavigationTabInABox
  extends Components.DigiNavigationTabInABox {
  /**
   * När tabben växlar mellan aktiv och inaktiv @en When the tab toggles between active and inactive
   */
  afOnToggle: EventEmitter<CustomEvent<boolean>>;
}
export declare class DigiNavigationTabInABox {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiNavigationTabInABox, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiNavigationTabInABox,
    'digi-navigation-tab-in-a-box',
    never,
    { afActive: 'afActive'; afAriaLabel: 'afAriaLabel'; afId: 'afId' },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiNavigationTabs
  extends Components.DigiNavigationTabs {
  /**
   *
   */
  afOnChange: EventEmitter<CustomEvent<any>>;
  /**
   *
   */
  afOnClick: EventEmitter<CustomEvent<MouseEvent>>;
  /**
   *
   */
  afOnFocus: EventEmitter<CustomEvent<any>>;
}
export declare class DigiNavigationTabs {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiNavigationTabs, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiNavigationTabs,
    'digi-navigation-tabs',
    never,
    {
      afAriaLabel: 'afAriaLabel';
      afId: 'afId';
      afInitActiveTab: 'afInitActiveTab';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiNavigationToc
  extends Components.DigiNavigationToc {}
export declare class DigiNavigationToc {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiNavigationToc, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiNavigationToc,
    'digi-navigation-toc',
    never,
    {},
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiNavigationVerticalMenu
  extends Components.DigiNavigationVerticalMenu {}
export declare class DigiNavigationVerticalMenu {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiNavigationVerticalMenu, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiNavigationVerticalMenu,
    'digi-navigation-vertical-menu',
    never,
    {
      afActiveIndicatorSize: 'afActiveIndicatorSize';
      afAriaLabel: 'afAriaLabel';
      afId: 'afId';
      afVariation: 'afVariation';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiNavigationVerticalMenuItem
  extends Components.DigiNavigationVerticalMenuItem {
  /**
   * Länken och toggle-knappens 'onclick'-event @en Link and toggle buttons 'onclick' event
   */
  afOnClick: EventEmitter<CustomEvent<any>>;
}
export declare class DigiNavigationVerticalMenuItem {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiNavigationVerticalMenuItem, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiNavigationVerticalMenuItem,
    'digi-navigation-vertical-menu-item',
    never,
    {
      afActive: 'afActive';
      afActiveSubnav: 'afActiveSubnav';
      afHasSubnav: 'afHasSubnav';
      afHref: 'afHref';
      afId: 'afId';
      afOverrideLink: 'afOverrideLink';
      afText: 'afText';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiNotificationAlert
  extends Components.DigiNotificationAlert {
  /**
   * Stängknappens 'onclick'-event @en Close button's 'onclick' event
   */
  afOnClose: EventEmitter<CustomEvent<MouseEvent>>;
}
export declare class DigiNotificationAlert {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiNotificationAlert, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiNotificationAlert,
    'digi-notification-alert',
    never,
    { afCloseable: 'afCloseable'; afVariation: 'afVariation' },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiNotificationCookie
  extends Components.DigiNotificationCookie {
  /**
   * När användaren godkänner alla kakor @en When the user accepts all cookies
   */
  afOnAcceptAllCookies: EventEmitter<CustomEvent<MouseEvent>>;
  /**
   * När användaren godkänner alla kakor @en When the user accepts all cookies
   */
  afOnSubmitSettings: EventEmitter<CustomEvent<any>>;
}
export declare class DigiNotificationCookie {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiNotificationCookie, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiNotificationCookie,
    'digi-notification-cookie',
    never,
    {
      afBannerHeadingText: 'afBannerHeadingText';
      afBannerText: 'afBannerText';
      afId: 'afId';
      afModalHeadingText: 'afModalHeadingText';
      afRequiredCookiesText: 'afRequiredCookiesText';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiNotificationDetail
  extends Components.DigiNotificationDetail {}
export declare class DigiNotificationDetail {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiNotificationDetail, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiNotificationDetail,
    'digi-notification-detail',
    never,
    { afVariation: 'afVariation' },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiPage extends Components.DigiPage {}
export declare class DigiPage {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiPage, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiPage,
    'digi-page',
    never,
    { afBackground: 'afBackground' },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiPageBlockCards
  extends Components.DigiPageBlockCards {}
export declare class DigiPageBlockCards {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiPageBlockCards, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiPageBlockCards,
    'digi-page-block-cards',
    never,
    { afVariation: 'afVariation' },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiPageBlockHero
  extends Components.DigiPageBlockHero {}
export declare class DigiPageBlockHero {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiPageBlockHero, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiPageBlockHero,
    'digi-page-block-hero',
    never,
    {
      afBackground: 'afBackground';
      afBackgroundImage: 'afBackgroundImage';
      afVariation: 'afVariation';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiPageBlockLists
  extends Components.DigiPageBlockLists {}
export declare class DigiPageBlockLists {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiPageBlockLists, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiPageBlockLists,
    'digi-page-block-lists',
    never,
    { afVariation: 'afVariation' },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiPageBlockSidebar
  extends Components.DigiPageBlockSidebar {}
export declare class DigiPageBlockSidebar {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiPageBlockSidebar, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiPageBlockSidebar,
    'digi-page-block-sidebar',
    never,
    { afVariation: 'afVariation' },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiPageFooter extends Components.DigiPageFooter {}
export declare class DigiPageFooter {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiPageFooter, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiPageFooter,
    'digi-page-footer',
    never,
    { afVariation: 'afVariation' },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiPageHeader extends Components.DigiPageHeader {}
export declare class DigiPageHeader {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiPageHeader, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiPageHeader,
    'digi-page-header',
    never,
    { afId: 'afId' },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiProgressStep extends Components.DigiProgressStep {}
export declare class DigiProgressStep {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiProgressStep, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiProgressStep,
    'digi-progress-step',
    never,
    {
      afHeading: 'afHeading';
      afHeadingLevel: 'afHeadingLevel';
      afId: 'afId';
      afIsLast: 'afIsLast';
      afStepStatus: 'afStepStatus';
      afVariation: 'afVariation';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiProgressSteps
  extends Components.DigiProgressSteps {}
export declare class DigiProgressSteps {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiProgressSteps, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiProgressSteps,
    'digi-progress-steps',
    never,
    {
      afCurrentStep: 'afCurrentStep';
      afHeadingLevel: 'afHeadingLevel';
      afVariation: 'afVariation';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiProgressbar extends Components.DigiProgressbar {}
export declare class DigiProgressbar {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiProgressbar, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiProgressbar,
    'digi-progressbar',
    never,
    {
      afCompletedSteps: 'afCompletedSteps';
      afId: 'afId';
      afRole: 'afRole';
      afStepsLabel: 'afStepsLabel';
      afStepsSeparator: 'afStepsSeparator';
      afTotalSteps: 'afTotalSteps';
      afVariation: 'afVariation';
    },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiTable extends Components.DigiTable {}
export declare class DigiTable {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiTable, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiTable,
    'digi-table',
    never,
    { afVariation: 'afVariation' },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiTag extends Components.DigiTag {
  /**
   * Taggelementets 'onclick'-event. @en The tag elements 'onclick' event.
   */
  afOnClick: EventEmitter<CustomEvent<any>>;
}
export declare class DigiTag {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiTag, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiTag,
    'digi-tag',
    never,
    { afNoIcon: 'afNoIcon'; afSize: 'afSize'; afText: 'afText' },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiTypography extends Components.DigiTypography {}
export declare class DigiTypography {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiTypography, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiTypography,
    'digi-typography',
    never,
    { afVariation: 'afVariation' },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiTypographyHeadingSection
  extends Components.DigiTypographyHeadingSection {}
export declare class DigiTypographyHeadingSection {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiTypographyHeadingSection, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiTypographyHeadingSection,
    'digi-typography-heading-section',
    never,
    {},
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiTypographyMeta
  extends Components.DigiTypographyMeta {}
export declare class DigiTypographyMeta {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiTypographyMeta, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiTypographyMeta,
    'digi-typography-meta',
    never,
    { afVariation: 'afVariation' },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiTypographyPreamble
  extends Components.DigiTypographyPreamble {}
export declare class DigiTypographyPreamble {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiTypographyPreamble, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiTypographyPreamble,
    'digi-typography-preamble',
    never,
    {},
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiTypographyTime
  extends Components.DigiTypographyTime {}
export declare class DigiTypographyTime {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiTypographyTime, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiTypographyTime,
    'digi-typography-time',
    never,
    { afDateTime: 'afDateTime'; afVariation: 'afVariation' },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiUtilBreakpointObserver
  extends Components.DigiUtilBreakpointObserver {
  /**
   * Vid inläsning av sida samt vid uppdatering av brytpunkt @en At page load and at change of breakpoint on page
   */
  afOnChange: EventEmitter<CustomEvent<any>>;
  /**
   * Vid brytpunkt 'small' @en At breakpoint 'small'
   */
  afOnSmall: EventEmitter<CustomEvent<any>>;
  /**
   * Vid brytpunkt 'medium' @en At breakpoint 'medium'
   */
  afOnMedium: EventEmitter<CustomEvent<any>>;
  /**
   * Vid brytpunkt 'large' @en At breakpoint 'large'
   */
  afOnLarge: EventEmitter<CustomEvent<any>>;
  /**
   * Vid brytpunkt 'xlarge' @en At breakpoint 'xlarge'
   */
  afOnXLarge: EventEmitter<CustomEvent<any>>;
}
export declare class DigiUtilBreakpointObserver {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiUtilBreakpointObserver, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiUtilBreakpointObserver,
    'digi-util-breakpoint-observer',
    never,
    {},
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiUtilDetectClickOutside
  extends Components.DigiUtilDetectClickOutside {
  /**
   * Vid klick utanför komponenten @en When click detected outside of component
   */
  afOnClickOutside: EventEmitter<CustomEvent<MouseEvent>>;
  /**
   * Vid klick inuti komponenten @en When click detected inside of component
   */
  afOnClickInside: EventEmitter<CustomEvent<MouseEvent>>;
}
export declare class DigiUtilDetectClickOutside {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiUtilDetectClickOutside, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiUtilDetectClickOutside,
    'digi-util-detect-click-outside',
    never,
    { afDataIdentifier: 'afDataIdentifier' },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiUtilDetectFocusOutside
  extends Components.DigiUtilDetectFocusOutside {
  /**
   * Vid fokus utanför komponenten @en When focus detected outside of component
   */
  afOnFocusOutside: EventEmitter<CustomEvent<FocusEvent>>;
  /**
   * Vid fokus inuti komponenten @en When focus detected inside of component
   */
  afOnFocusInside: EventEmitter<CustomEvent<FocusEvent>>;
}
export declare class DigiUtilDetectFocusOutside {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiUtilDetectFocusOutside, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiUtilDetectFocusOutside,
    'digi-util-detect-focus-outside',
    never,
    { afDataIdentifier: 'afDataIdentifier' },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiUtilIntersectionObserver
  extends Components.DigiUtilIntersectionObserver {
  /**
   * Vid statusförändring mellan 'unintersected' och 'intersected' @en On change between unintersected and intersected
   */
  afOnChange: EventEmitter<CustomEvent<boolean>>;
  /**
   * Vid statusförändring till 'intersected' @en On every change to intersected.
   */
  afOnIntersect: EventEmitter<CustomEvent<any>>;
  /**
     * Vid statusförändring till 'unintersected'
  On every change to unintersected
     */
  afOnUnintersect: EventEmitter<CustomEvent<any>>;
}
export declare class DigiUtilIntersectionObserver {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiUtilIntersectionObserver, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiUtilIntersectionObserver,
    'digi-util-intersection-observer',
    never,
    { afOnce: 'afOnce'; afOptions: 'afOptions' },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiUtilKeydownHandler
  extends Components.DigiUtilKeydownHandler {
  /**
   * Vid keydown på escape @en At keydown on escape key
   */
  afOnEsc: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på enter @en At keydown on enter key
   */
  afOnEnter: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på tab @en At keydown on tab key
   */
  afOnTab: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på space @en At keydown on space key
   */
  afOnSpace: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på skift och tabb @en At keydown on shift and tab key
   */
  afOnShiftTab: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på pil upp @en At keydown on up arrow key
   */
  afOnUp: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på pil ner @en At keydown on down arrow key
   */
  afOnDown: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på pil vänster @en At keydown on left arrow key
   */
  afOnLeft: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på pil höger @en At keydown on right arrow key
   */
  afOnRight: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på home @en At keydown on home key
   */
  afOnHome: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på end @en At keydown on end key
   */
  afOnEnd: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keydown på alla tangenter @en At keydown on any key
   */
  afOnKeyDown: EventEmitter<CustomEvent<KeyboardEvent>>;
}
export declare class DigiUtilKeydownHandler {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiUtilKeydownHandler, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiUtilKeydownHandler,
    'digi-util-keydown-handler',
    never,
    {},
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiUtilKeyupHandler
  extends Components.DigiUtilKeyupHandler {
  /**
   * Vid keyup på escape @en At keyup on escape key
   */
  afOnEsc: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på enter @en At keyup on enter key
   */
  afOnEnter: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på tab @en At keyup on tab key
   */
  afOnTab: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på space @en At keyup on space key
   */
  afOnSpace: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på skift och tabb @en At keyup on shift and tab key
   */
  afOnShiftTab: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på pil upp @en At keyup on up arrow key
   */
  afOnUp: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på pil ner @en At keyup on down arrow key
   */
  afOnDown: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på pil vänster @en At keyup on left arrow key
   */
  afOnLeft: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på pil höger @en At keyup on right arrow key
   */
  afOnRight: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på home @en At keyup on home key
   */
  afOnHome: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på end @en At keyup on end key
   */
  afOnEnd: EventEmitter<CustomEvent<KeyboardEvent>>;
  /**
   * Vid keyup på alla tangenter @en At keyup on any key
   */
  afOnKeyUp: EventEmitter<CustomEvent<KeyboardEvent>>;
}
export declare class DigiUtilKeyupHandler {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiUtilKeyupHandler, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiUtilKeyupHandler,
    'digi-util-keyup-handler',
    never,
    {},
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiUtilMutationObserver
  extends Components.DigiUtilMutationObserver {
  /**
   * När DOM-element läggs till eller tas bort inuti Mutation Observer:n @en When DOM-elements is added or removed inside the Mutation Observer
   */
  afOnChange: EventEmitter<CustomEvent<any>>;
}
export declare class DigiUtilMutationObserver {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiUtilMutationObserver, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiUtilMutationObserver,
    'digi-util-mutation-observer',
    never,
    { afId: 'afId'; afOptions: 'afOptions' },
    {},
    never,
    ['*'],
    false
  >;
}
export declare interface DigiUtilResizeObserver
  extends Components.DigiUtilResizeObserver {
  /**
   * När komponenten ändrar storlek @en When the component changes size
   */
  afOnChange: EventEmitter<CustomEvent<ResizeObserverEntry>>;
}
export declare class DigiUtilResizeObserver {
  protected z: NgZone;
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, z: NgZone);
  static ɵfac: i0.ɵɵFactoryDeclaration<DigiUtilResizeObserver, never>;
  static ɵcmp: i0.ɵɵComponentDeclaration<
    DigiUtilResizeObserver,
    'digi-util-resize-observer',
    never,
    {},
    {},
    never,
    ['*'],
    false
  >;
}
