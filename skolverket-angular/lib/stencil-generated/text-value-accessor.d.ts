import { ElementRef } from '@angular/core';
import { ValueAccessor } from './value-accessor';
import * as i0 from "@angular/core";
export declare class TextValueAccessor extends ValueAccessor {
    constructor(el: ElementRef);
    static ɵfac: i0.ɵɵFactoryDeclaration<TextValueAccessor, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<TextValueAccessor, "digi-form-input, digi-form-input[afType=text], digi-form-input[afType=email], digi-form-input[afType=color], digi-form-input[afType=date], digi-form-input[afType=datetime-local], digi-form-input[afType=month], digi-form-input[afType=password], digi-form-input[afType=search], digi-form-input[afType=tel], digi-form-input[afType=time], digi-form-input[afType=url], digi-form-input[afType=week], digi-form-textarea, digi-form-input-search, digi-calendar", never, {}, {}, never, never, false>;
}
