var ProgressbarRole;
(function (ProgressbarRole) {
  ProgressbarRole["NONE"] = "";
  ProgressbarRole["STATUS"] = "status";
})(ProgressbarRole || (ProgressbarRole = {}));

var ProgressbarVariation;
(function (ProgressbarVariation) {
  ProgressbarVariation["PRIMARY"] = "primary";
  ProgressbarVariation["SECONDARY"] = "secondary";
})(ProgressbarVariation || (ProgressbarVariation = {}));

export { ProgressbarRole as P, ProgressbarVariation as a };
