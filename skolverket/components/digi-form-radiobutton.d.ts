import type { Components, JSX } from "../dist/types/components";

interface DigiFormRadiobutton extends Components.DigiFormRadiobutton, HTMLElement {}
export const DigiFormRadiobutton: {
  prototype: DigiFormRadiobutton;
  new (): DigiFormRadiobutton;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
