import { proxyCustomElement, HTMLElement, createEvent, h } from '@stencil/core/internal/client';
import { r as randomIdGenerator } from './randomIdGenerator.util.js';
import { d as detectFocusOutside } from './detectFocusOutside.util.js';

const UtilDetectFocusOutside = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afOnFocusOutside = createEvent(this, "afOnFocusOutside", 7);
    this.afOnFocusInside = createEvent(this, "afOnFocusInside", 7);
    this.afDataIdentifier = randomIdGenerator('data-digi-util-detect-focus-outside');
    this.$el.setAttribute(this.afDataIdentifier, '');
  }
  focusinHandler(e) {
    const target = e.target;
    const selector = `[${this.afDataIdentifier}]`;
    if (detectFocusOutside(target, selector)) {
      this.afOnFocusOutside.emit(e);
    }
    else {
      this.afOnFocusInside.emit(e);
    }
  }
  render() {
    return h("slot", null);
  }
  get $el() { return this; }
}, [6, "digi-util-detect-focus-outside", {
    "afDataIdentifier": [1, "af-data-identifier"]
  }, [[4, "focusin", "focusinHandler"]]]);
function defineCustomElement$1() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-util-detect-focus-outside"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-util-detect-focus-outside":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, UtilDetectFocusOutside);
      }
      break;
  } });
}
defineCustomElement$1();

const DigiUtilDetectFocusOutside = UtilDetectFocusOutside;
const defineCustomElement = defineCustomElement$1;

export { DigiUtilDetectFocusOutside, defineCustomElement };
