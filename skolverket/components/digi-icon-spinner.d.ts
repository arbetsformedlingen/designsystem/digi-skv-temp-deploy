import type { Components, JSX } from "../dist/types/components";

interface DigiIconSpinner extends Components.DigiIconSpinner, HTMLElement {}
export const DigiIconSpinner: {
  prototype: DigiIconSpinner;
  new (): DigiIconSpinner;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
