import { I as IconPaperclip, d as defineCustomElement$1 } from './icon-paperclip.js';

const DigiIconPaperclip = IconPaperclip;
const defineCustomElement = defineCustomElement$1;

export { DigiIconPaperclip, defineCustomElement };
