import type { Components, JSX } from "../dist/types/components";

interface DigiCode extends Components.DigiCode, HTMLElement {}
export const DigiCode: {
  prototype: DigiCode;
  new (): DigiCode;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
