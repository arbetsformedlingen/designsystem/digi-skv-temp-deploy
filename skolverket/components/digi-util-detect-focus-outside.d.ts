import type { Components, JSX } from "../dist/types/components";

interface DigiUtilDetectFocusOutside extends Components.DigiUtilDetectFocusOutside, HTMLElement {}
export const DigiUtilDetectFocusOutside: {
  prototype: DigiUtilDetectFocusOutside;
  new (): DigiUtilDetectFocusOutside;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
