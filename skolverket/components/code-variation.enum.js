var CodeLanguage;
(function (CodeLanguage) {
  CodeLanguage["JSON"] = "json";
  CodeLanguage["CSS"] = "css";
  CodeLanguage["SCSS"] = "scss";
  CodeLanguage["TYPESCRIPT"] = "typescript";
  CodeLanguage["JAVASCRIPT"] = "javascript";
  CodeLanguage["BASH"] = "bash";
  CodeLanguage["HTML"] = "html";
  CodeLanguage["GIT"] = "git";
})(CodeLanguage || (CodeLanguage = {}));

var CodeVariation;
(function (CodeVariation) {
  CodeVariation["LIGHT"] = "light";
  CodeVariation["DARK"] = "dark";
})(CodeVariation || (CodeVariation = {}));

export { CodeLanguage as C, CodeVariation as a };
