var TypographyMetaVariation;
(function (TypographyMetaVariation) {
  TypographyMetaVariation["PRIMARY"] = "primary";
  TypographyMetaVariation["SECONDARY"] = "secondary";
})(TypographyMetaVariation || (TypographyMetaVariation = {}));

export { TypographyMetaVariation as T };
