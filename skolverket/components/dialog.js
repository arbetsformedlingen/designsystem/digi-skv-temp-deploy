import { proxyCustomElement, HTMLElement, createEvent, h } from '@stencil/core/internal/client';
import { C as CardBoxGutter, d as defineCustomElement$4 } from './card-box.js';
import './page-background.enum.js';
import './layout-grid-vertical-spacing.enum.js';
import './layout-stacked-blocks-variation.enum.js';
import './list-link-variation.enum.js';
import './navigation-breadcrumbs-variation.enum.js';
import './notification-alert-variation.enum.js';
import './notification-detail-variation.enum.js';
import './page-footer-variation.enum.js';
import './table-variation.enum.js';
import { d as defineCustomElement$3 } from './icon.js';
import { d as defineCustomElement$2 } from './typography.js';
import { d as defineCustomElement$1 } from './util-detect-click-outside.js';

const dialogCss = ".sc-digi-dialog-h .sc-digi-dialog-s>[slot^=heading]{--digi--heading-4--font-size:var(--digi--typography--heading-4--font-size--mobile);--digi--heading-4--line-height:var(--digi--typography--heading-4--line-height--mobile);font-family:var(--digi--global--typography--font-family--default);font-size:var(--digi--heading-4--font-size);line-height:var(--digi--heading-4--line-height);font-weight:var(--digi--typography--heading-4--font-weight--mobile);-webkit-margin-after:0;margin-block-end:0}@media (min-width: 48rem){.sc-digi-dialog-h .sc-digi-dialog-s>[slot^=heading]{--digi--heading-4--font-size:var(--digi--typography--heading-4--font-size--desktop);--digi--heading-4--line-height:var(--digi--typography--heading-4--line-height--desktop)}}@media (min-width: 62rem){.sc-digi-dialog-h .sc-digi-dialog-s>[slot^=heading]{--digi--heading-4--font-size:var(--digi--typography--heading-4--font-size--desktop-large);--digi--heading-4--line-height:var(--digi--typography--heading-4--line-height--desktop-large)}}.digi-dialog.sc-digi-dialog{border:none;padding:0;border-radius:var(--digi--border-radius--primary);box-shadow:0px 8px 16px 0px rgba(105, 40, 89, 0.2);max-width:calc(100vw - var(--digi--container-gutter--medium) * 2);max-height:calc(100vh - var(--digi--container-gutter--medium) * 2);overflow:hidden}.digi-dialog.sc-digi-dialog::backdrop{background:rgba(0, 0, 0, 0.4)}@media (max-width: 47.9375rem){.digi-dialog.sc-digi-dialog{width:100vw;height:calc(100vh - var(--digi--container-gutter--medium));margin:0;top:var(--digi--container-gutter--medium);max-width:none;max-height:none}}.digi-dialog__inner.sc-digi-dialog{display:grid;grid-template-rows:min-content 1fr min-content;grid-template-areas:\"header close\" \"content content\" \"actions actions\";gap:var(--digi--responsive-grid-gutter);max-height:calc(100vh - var(--digi--container-gutter--medium) * 2)}@media (max-width: 47.9375rem){.digi-dialog__inner.sc-digi-dialog{height:calc(100vh - var(--digi--container-gutter--medium));max-height:none}}.digi-dialog__header.sc-digi-dialog{grid-area:header;padding:var(--digi--padding--largest) 0 0 var(--digi--padding--largest)}@media (min-width: 48rem){.digi-dialog__header.sc-digi-dialog{padding:var(--digi--padding--largest-3) 0 0 var(--digi--padding--largest-3)}}.digi-dialog__close-button-wrapper.sc-digi-dialog{grid-area:close;justify-self:end;padding:var(--digi--padding--largest) var(--digi--padding--largest) 0 0}@media (min-width: 48rem){.digi-dialog__close-button-wrapper.sc-digi-dialog{padding:var(--digi--padding--largest-3) var(--digi--padding--largest-3) 0 0}}.digi-dialog__close-button.sc-digi-dialog{font-family:var(--digi--global--typography--font-family--default);text-decoration:none;color:var(--digi--color--text--secondary);font-size:var(--digi--global--typography--font-size--large);letter-spacing:calc(var(--digi--global--typography--font-size--large) / 100 * -1);font-weight:var(--digi--global--typography--font-weight--semibold);display:flex;flex-direction:row;font-weight:var(--digi--global--typography--font-weight--semibold);font-size:var(--digi--typography--button--font-size--desktop);gap:var(--digi--gutter--icon);align-items:flex-start}.digi-dialog__close-button.sc-digi-dialog:hover{color:var(--digi--global--color--profile--purple--dark);text-decoration:underline}.digi-dialog__close-button.sc-digi-dialog:focus{outline:none;color:var(--digi--color--text--secondary)}.digi-dialog__close-button.sc-digi-dialog:focus-visible{outline:var(--digi--border-width--secondary) solid var(--digi--color--border--focus)}.digi-dialog__close-button.sc-digi-dialog:visited{color:var(--digi--color--text--secondary)}.digi-dialog__content.sc-digi-dialog{grid-area:content;padding-inline:var(--digi--padding--largest);overflow:auto}@media (min-width: 48rem){.digi-dialog__content.sc-digi-dialog{padding-inline:var(--digi--padding--largest-3)}}.digi-dialog__actions.sc-digi-dialog{grid-area:actions;padding:var(--digi--padding--largest);background:var(--digi--color--background--tertiary);display:flex;flex-wrap:wrap;justify-content:flex-end;gap:var(--digi--gutter--small)}@media (min-width: 48rem){.digi-dialog__actions.sc-digi-dialog{padding-inline:var(--digi--padding--largest-3)}}.digi-dialog__actions.sc-digi-dialog:empty{display:none}";

const Dialog = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afOnOpen = createEvent(this, "afOnOpen", 7);
    this.afOnClose = createEvent(this, "afOnClose", 7);
    this.afOpen = false;
    this.afHideCloseButton = false;
  }
  afOpenChanged(newVal, oldVal) {
    if (newVal === oldVal)
      return;
    this.afOpen ? this.showModal() : this.close();
    this.toggleEmitter(this.afOpen ? 'open' : 'close');
  }
  async showModal() {
    this._dialog.showModal();
    this.afOpen = true;
    this.toggleEmitter('open');
  }
  async close() {
    this._dialog.close();
    this.afOpen = false;
    this.toggleEmitter('close');
  }
  toggleEmitter(state) {
    state === 'open' ? this.afOnOpen.emit() : this.afOnClose.emit();
  }
  clickOutsideHandler(e) {
    if (e.detail.target !== this._dialog)
      return;
    this.close();
  }
  componentDidLoad() {
    this.afOpen && this.showModal();
  }
  render() {
    return (h("dialog", { class: "digi-dialog", ref: (el) => {
        this._dialog = el;
      }, onClose: () => this.close() }, h("digi-util-detect-click-outside", { onAfOnClickOutside: (e) => this.clickOutsideHandler(e) }, h("digi-card-box", { afGutter: CardBoxGutter.NONE }, h("digi-typography", null, h("div", { class: "digi-dialog__inner" }, h("header", { class: "digi-dialog__header" }, h("slot", { name: "heading" })), h("div", { class: "digi-dialog__content" }, h("slot", null)), h("div", { class: "digi-dialog__actions" }, h("slot", { name: "actions" })), !this.afHideCloseButton && (h("div", { class: "digi-dialog__close-button-wrapper" }, h("button", { class: "digi-dialog__close-button", onClick: () => this.close(), type: "button" }, "St\u00E4ng ", h("digi-icon", { "aria-hidden": "true", afName: `x` }))))))))));
  }
  static get watchers() { return {
    "afOpen": ["afOpenChanged"]
  }; }
  static get style() { return dialogCss; }
}, [6, "digi-dialog", {
    "afOpen": [4, "af-open"],
    "afHideCloseButton": [4, "af-hide-close-button"],
    "showModal": [64],
    "close": [64]
  }]);
function defineCustomElement() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-dialog", "digi-card-box", "digi-icon", "digi-typography", "digi-util-detect-click-outside"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-dialog":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, Dialog);
      }
      break;
    case "digi-card-box":
      if (!customElements.get(tagName)) {
        defineCustomElement$4();
      }
      break;
    case "digi-icon":
      if (!customElements.get(tagName)) {
        defineCustomElement$3();
      }
      break;
    case "digi-typography":
      if (!customElements.get(tagName)) {
        defineCustomElement$2();
      }
      break;
    case "digi-util-detect-click-outside":
      if (!customElements.get(tagName)) {
        defineCustomElement$1();
      }
      break;
  } });
}
defineCustomElement();

export { Dialog as D, defineCustomElement as d };
