import type { Components, JSX } from "../dist/types/components";

interface DigiNotificationDetail extends Components.DigiNotificationDetail, HTMLElement {}
export const DigiNotificationDetail: {
  prototype: DigiNotificationDetail;
  new (): DigiNotificationDetail;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
