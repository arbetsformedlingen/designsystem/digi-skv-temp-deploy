import { proxyCustomElement, HTMLElement, h } from '@stencil/core/internal/client';
import { M as MediaFigureAlignment } from './media-figure-alignment.enum.js';

const mediaFigureCss = ".sc-digi-media-figure-h .digi-media-figure__figcaption.sc-digi-media-figure{font-family:var(--digi--global--typography--font-family--default);font-size:var(--digi--typography--body--font-size--mobile);color:var(--digi--color--text--neutral);margin-top:var(--digi--gutter--smaller)}.digi-media-figure--align-start.sc-digi-media-figure .digi-media-figure__figcaption.sc-digi-media-figure{text-align:start}.digi-media-figure--align-center.sc-digi-media-figure .digi-media-figure__figcaption.sc-digi-media-figure{text-align:center}.digi-media-figure--align-end.sc-digi-media-figure .digi-media-figure__figcaption.sc-digi-media-figure{text-align:end}";

const MediaFigure = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afFigcaption = undefined;
    this.afAlignment = MediaFigureAlignment.START;
  }
  get cssModifiers() {
    return {
      'digi-media-figure--align-start': this.afAlignment === 'start',
      'digi-media-figure--align-center': this.afAlignment === 'center',
      'digi-media-figure--align-end': this.afAlignment === 'end',
    };
  }
  render() {
    return (h("figure", { class: Object.assign({ 'digi-media-figure': true }, this.cssModifiers) }, h("div", { class: "digi-media-figure__image" }, h("slot", null)), this.afFigcaption && (h("figcaption", { class: "digi-media-figure__figcaption" }, this.afFigcaption))));
  }
  static get style() { return mediaFigureCss; }
}, [6, "digi-media-figure", {
    "afFigcaption": [1, "af-figcaption"],
    "afAlignment": [1, "af-alignment"]
  }]);
function defineCustomElement$1() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-media-figure"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-media-figure":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, MediaFigure);
      }
      break;
  } });
}
defineCustomElement$1();

const DigiMediaFigure = MediaFigure;
const defineCustomElement = defineCustomElement$1;

export { DigiMediaFigure, defineCustomElement };
