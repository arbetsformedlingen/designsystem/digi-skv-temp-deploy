import type { Components, JSX } from "../dist/types/components";

interface DigiTypographyPreamble extends Components.DigiTypographyPreamble, HTMLElement {}
export const DigiTypographyPreamble: {
  prototype: DigiTypographyPreamble;
  new (): DigiTypographyPreamble;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
