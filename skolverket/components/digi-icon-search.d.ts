import type { Components, JSX } from "../dist/types/components";

interface DigiIconSearch extends Components.DigiIconSearch, HTMLElement {}
export const DigiIconSearch: {
  prototype: DigiIconSearch;
  new (): DigiIconSearch;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
