import { F as FormValidationMessage, d as defineCustomElement$1 } from './form-validation-message.js';

const DigiFormValidationMessage = FormValidationMessage;
const defineCustomElement = defineCustomElement$1;

export { DigiFormValidationMessage, defineCustomElement };
