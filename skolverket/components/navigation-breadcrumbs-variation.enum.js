var NavigationBreadcrumbsVariation;
(function (NavigationBreadcrumbsVariation) {
  NavigationBreadcrumbsVariation["REGULAR"] = "regular";
  NavigationBreadcrumbsVariation["COMPRESSED"] = "compressed";
})(NavigationBreadcrumbsVariation || (NavigationBreadcrumbsVariation = {}));

export { NavigationBreadcrumbsVariation as N };
