import { proxyCustomElement, HTMLElement, createEvent, h } from '@stencil/core/internal/client';
import { L as LinkVariation } from './link-variation.enum.js';

const linkCss = ".sc-digi-link-h{--digi--link--gap:var(--digi--gutter--medium);--digi--link--color--default:var(--digi--color--text--link);--digi--link--color--hover:var(--digi--color--text--link-hover);--digi--link--color--visited:var(--digi--color--text--link-visited);--digi--link--font-size--large:var(--digi--typography--link--font-size--desktop-large);--digi--link--font-size--small:var(--digi--typography--link--font-size--desktop);--digi--link--font-family:var(--digi--global--typography--font-family--default);--digi--link--font-weight:var(--digi--typography--link--font-weight--desktop);--digi--link--text-decoration--default:var(--digi--typography--link--text-decoration--desktop);--digi--link--text-decoration--hover:var(--digi--global--typography--text-decoration--default);--digi--link--text-decoration--icon--default:var(--digi--global--typography--text-decoration--default);--digi--link--text-decoration--icon--hover:var(--digi--typography--link--text-decoration--desktop);--COLOR:var(--digi--link--color--default);--LINK-TEXT-DECORATION:var(--digi--link--text-decoration--default)}.sc-digi-link-h .digi-link.sc-digi-link{font-family:var(--digi--link--font-family);font-size:var(--FONT-SIZE);font-weight:var(--digi--link--font-weight);color:var(--COLOR);-webkit-text-decoration:var(--LINK-TEXT-DECORATION);text-decoration:var(--LINK-TEXT-DECORATION);display:inline-flex;gap:var(--digi--link--gap)}.sc-digi-link-h .digi-link--small.sc-digi-link{--FONT-SIZE:var(--digi--link--font-size--small)}.sc-digi-link-h .digi-link--large.sc-digi-link{--FONT-SIZE:var(--digi--link--font-size--large)}.sc-digi-link-h .digi-link--has-icon.sc-digi-link{--LINK-TEXT-DECORATION:var(--digi--link--text-decoration--icon--default)}.sc-digi-link-h .digi-link--has-icon.sc-digi-link:focus,.sc-digi-link-h .digi-link--has-icon.sc-digi-link:hover{--digi--link--text-decoration--hover:var(--digi--link--text-decoration--icon--hover)}.sc-digi-link-h .digi-link.sc-digi-link:hover{cursor:pointer}.sc-digi-link-h .digi-link.sc-digi-link:focus,.sc-digi-link-h .digi-link.sc-digi-link:hover{--COLOR:var(--digi--link--color--hover);--LINK-TEXT-DECORATION:var(--digi--link--text-decoration--hover)}.sc-digi-link-h .digi-link.sc-digi-link:visited{--COLOR:var(--digi--link--color--visited)}.sc-digi-link-h .digi-link__icon-wrapper.sc-digi-link{margin-right:var(--digi--link--icon-wrapper--margin-right)}.digi-link__icon-wrapper.sc-digi-link-s>[slot^=icon],.digi-link__icon-wrapper .sc-digi-link-s>[slot^=icon]{--digi--icon--color:var(--COLOR);--digi--icon--height:1em;--digi--icon--width:1em}";

const Link = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afOnClick = createEvent(this, "afOnClick", 7);
    this.hasIcon = undefined;
    this.relation = undefined;
    this.afHref = undefined;
    this.afVariation = LinkVariation.SMALL;
    this.afTarget = undefined;
    this.afOverrideLink = false;
  }
  /**
   * Hämtar en referens till länkelementet. Bra för att t.ex. sätta fokus programmatiskt.
   * @en Returns a reference to the link element. Handy for setting focus programmatically.
   */
  async afMGetLinkElement() {
    return this._link;
  }
  clickLinkHandler(e) {
    if (this.afOverrideLink) {
      e.preventDefault();
    }
    this.afOnClick.emit(e);
  }
  setRel() {
    var _a;
    this.relation =
      ((_a = this.afTarget) === null || _a === void 0 ? void 0 : _a.toLowerCase()) === '_blank' ? 'noopener noreferrer' : null;
  }
  setHasIcon() {
    this.hasIcon = !!this.hostElement.querySelector('[slot="icon"]');
  }
  initRouting() {
    const tabIndex = this.hostElement.getAttribute('tabIndex');
    !!tabIndex && tabIndex === '0' && this._link.setAttribute('tabIndex', '-1');
  }
  componentWillLoad() {
    this.setHasIcon();
    this.setRel();
  }
  componentWillUpdate() {
    this.setHasIcon();
    this.setRel();
  }
  componentDidLoad() {
    this.initRouting();
  }
  get cssModifiers() {
    return {
      [`digi-link--variation-${this.afVariation}`]: !!this.afVariation,
      'digi-link--has-icon': this.hasIcon
    };
  }
  render() {
    return (h("a", { class: Object.assign({ 'digi-link': true }, this.cssModifiers), href: this.afHref, onClick: (e) => this.clickLinkHandler(e), target: this.afTarget, rel: this.relation, ref: (el) => (this._link = el) }, this.hasIcon && (h("div", { class: "digi-link__icon-wrapper", "aria-hidden": "true" }, h("slot", { name: "icon" }))), h("slot", null)));
  }
  get hostElement() { return this; }
  static get watchers() { return {
    "afTarget": ["setRel"]
  }; }
  static get style() { return linkCss; }
}, [6, "digi-link", {
    "afHref": [1, "af-href"],
    "afVariation": [1, "af-variation"],
    "afTarget": [1, "af-target"],
    "afOverrideLink": [4, "af-override-link"],
    "hasIcon": [32],
    "relation": [32],
    "afMGetLinkElement": [64]
  }]);
function defineCustomElement() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-link"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-link":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, Link);
      }
      break;
  } });
}
defineCustomElement();

export { Link as L, defineCustomElement as d };
