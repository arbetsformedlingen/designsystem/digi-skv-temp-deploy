import type { Components, JSX } from "../dist/types/components";

interface DigiNavigationVerticalMenuItem extends Components.DigiNavigationVerticalMenuItem, HTMLElement {}
export const DigiNavigationVerticalMenuItem: {
  prototype: DigiNavigationVerticalMenuItem;
  new (): DigiNavigationVerticalMenuItem;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
