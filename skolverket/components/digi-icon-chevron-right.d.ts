import type { Components, JSX } from "../dist/types/components";

interface DigiIconChevronRight extends Components.DigiIconChevronRight, HTMLElement {}
export const DigiIconChevronRight: {
  prototype: DigiIconChevronRight;
  new (): DigiIconChevronRight;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
