import { proxyCustomElement, HTMLElement, h } from '@stencil/core/internal/client';
import { L as LayoutContainerVariation } from './layout-container-variation.enum.js';

const layoutContainerCss = ".digi--util--fs--xs.sc-digi-layout-container{font-size:var(--digi--global--typography--font-size--small) !important}.digi--util--fs--s.sc-digi-layout-container{font-size:calc(var(--digi--global--typography--font-size--small) + 1px) !important}.digi--util--fs--m.sc-digi-layout-container{font-size:var(--digi--global--typography--font-size--base) !important}.digi--util--fs--l.sc-digi-layout-container{font-size:var(--digi--global--typography--font-size--large) !important}.digi--util--fw--sb.sc-digi-layout-container{font-weight:var(--digi--global--typography--font-weight--semibold) !important}.digi--util--pt--1.sc-digi-layout-container{padding-top:var(--digi--global--spacing--smallest-2) !important}.digi--util--pt--10.sc-digi-layout-container{padding-top:var(--digi--global--spacing--smallest) !important}.digi--util--pt--20.sc-digi-layout-container{padding-top:var(--digi--global--spacing--base) !important}.digi--util--pt--30.sc-digi-layout-container{padding-top:var(--digi--global--spacing--largest-2) !important}.digi--util--pt--40.sc-digi-layout-container{padding-top:var(--digi--global--spacing--largest-4) !important}.digi--util--pt--50.sc-digi-layout-container{padding-top:2.5rem !important}.digi--util--pt--60.sc-digi-layout-container{padding-top:var(--digi--global--spacing--largest-5) !important}.digi--util--pt--70.sc-digi-layout-container{padding-top:var(--digi--global--spacing--largest-6) !important}.digi--util--pt--80.sc-digi-layout-container{padding-top:4.5rem !important}.digi--util--pt--90.sc-digi-layout-container{padding-top:5rem !important}.digi--util--pb--1.sc-digi-layout-container{padding-bottom:var(--digi--global--spacing--smallest-2) !important}.digi--util--pb--10.sc-digi-layout-container{padding-bottom:var(--digi--global--spacing--smallest) !important}.digi--util--pb--20.sc-digi-layout-container{padding-bottom:var(--digi--global--spacing--base) !important}.digi--util--pb--30.sc-digi-layout-container{padding-bottom:var(--digi--global--spacing--largest-2) !important}.digi--util--pb--40.sc-digi-layout-container{padding-bottom:var(--digi--global--spacing--largest-4) !important}.digi--util--pb--50.sc-digi-layout-container{padding-bottom:2.5rem !important}.digi--util--pb--60.sc-digi-layout-container{padding-bottom:var(--digi--global--spacing--largest-5) !important}.digi--util--pb--70.sc-digi-layout-container{padding-bottom:var(--digi--global--spacing--largest-6) !important}.digi--util--pb--80.sc-digi-layout-container{padding-bottom:4.5rem !important}.digi--util--pb--90.sc-digi-layout-container{padding-bottom:5rem !important}.digi--util--p--1.sc-digi-layout-container{padding:var(--digi--global--spacing--smallest-2) !important}.digi--util--p--10.sc-digi-layout-container{padding:var(--digi--global--spacing--smallest) !important}.digi--util--p--20.sc-digi-layout-container{padding:var(--digi--global--spacing--base) !important}.digi--util--p--30.sc-digi-layout-container{padding:var(--digi--global--spacing--largest-2) !important}.digi--util--p--40.sc-digi-layout-container{padding:var(--digi--global--spacing--largest-4) !important}.digi--util--p--50.sc-digi-layout-container{padding:2.5rem !important}.digi--util--p--60.sc-digi-layout-container{padding:var(--digi--global--spacing--largest-5) !important}.digi--util--p--70.sc-digi-layout-container{padding:var(--digi--global--spacing--largest-6) !important}.digi--util--p--80.sc-digi-layout-container{padding:4.5rem !important}.digi--util--p--90.sc-digi-layout-container{padding:5rem !important}.digi--util--ptb--1.sc-digi-layout-container{padding-top:var(--digi--global--spacing--smallest-2) !important;padding-bottom:var(--digi--global--spacing--smallest-2) !important}.digi--util--ptb--10.sc-digi-layout-container{padding-top:var(--digi--global--spacing--smallest) !important;padding-bottom:var(--digi--global--spacing--smallest) !important}.digi--util--ptb--20.sc-digi-layout-container{padding-top:var(--digi--global--spacing--base) !important;padding-bottom:var(--digi--global--spacing--base) !important}.digi--util--ptb--30.sc-digi-layout-container{padding-top:var(--digi--global--spacing--largest-2) !important;padding-bottom:var(--digi--global--spacing--largest-2) !important}.digi--util--ptb--40.sc-digi-layout-container{padding-top:var(--digi--global--spacing--largest-4) !important;padding-bottom:var(--digi--global--spacing--largest-4) !important}.digi--util--ptb--50.sc-digi-layout-container{padding-top:2.5rem !important;padding-bottom:2.5rem !important}.digi--util--ptb--60.sc-digi-layout-container{padding-top:var(--digi--global--spacing--largest-5) !important;padding-bottom:var(--digi--global--spacing--largest-5) !important}.digi--util--ptb--70.sc-digi-layout-container{padding-top:var(--digi--global--spacing--largest-6) !important;padding-bottom:var(--digi--global--spacing--largest-6) !important}.digi--util--ptb--80.sc-digi-layout-container{padding-top:4.5rem !important;padding-bottom:4.5rem !important}.digi--util--ptb--90.sc-digi-layout-container{padding-top:5rem !important;padding-bottom:5rem !important}.digi--util--plr--1.sc-digi-layout-container{padding-left:var(--digi--global--spacing--smallest-2) !important;padding-right:var(--digi--global--spacing--smallest-2) !important}.digi--util--plr--10.sc-digi-layout-container{padding-left:var(--digi--global--spacing--smallest) !important;padding-right:var(--digi--global--spacing--smallest) !important}.digi--util--plr--20.sc-digi-layout-container{padding-left:var(--digi--global--spacing--base) !important;padding-right:var(--digi--global--spacing--base) !important}.digi--util--plr--30.sc-digi-layout-container{padding-left:var(--digi--global--spacing--largest-2) !important;padding-right:var(--digi--global--spacing--largest-2) !important}.digi--util--plr--40.sc-digi-layout-container{padding-left:var(--digi--global--spacing--largest-4) !important;padding-right:var(--digi--global--spacing--largest-4) !important}.digi--util--plr--50.sc-digi-layout-container{padding-left:2.5rem !important;padding-right:2.5rem !important}.digi--util--plr--60.sc-digi-layout-container{padding-left:var(--digi--global--spacing--largest-5) !important;padding-right:var(--digi--global--spacing--largest-5) !important}.digi--util--plr--70.sc-digi-layout-container{padding-left:var(--digi--global--spacing--largest-6) !important;padding-right:var(--digi--global--spacing--largest-6) !important}.digi--util--plr--80.sc-digi-layout-container{padding-left:4.5rem !important;padding-right:4.5rem !important}.digi--util--plr--90.sc-digi-layout-container{padding-left:5rem !important;padding-right:5rem !important}.digi--util--sr-only.sc-digi-layout-container{border:0;clip:rect(1px, 1px, 1px, 1px);-webkit-clip-path:inset(50%);clip-path:inset(50%);height:1px;margin:-1px;overflow:hidden;padding:0;position:absolute;width:1px;white-space:nowrap}.sc-digi-layout-container-h{--digi--layout-container--width:var(--digi--container-width--smaller);--digi--layout-container--gutter:var(--digi--container-gutter--base);--digi--layout-container--vertical-padding:0;--digi--layout-container--margin-top:0;--digi--layout-container--margin-bottom:0}.sc-digi-layout-container-h .digi-layout-container.sc-digi-layout-container{box-sizing:border-box;width:100%;max-width:var(--digi--layout-container--width);padding:var(--digi--layout-container--vertical-padding) var(--digi--layout-container--gutter);margin-left:auto;margin-right:auto;margin-top:var(--digi--layout-container--margin-top);margin-bottom:var(--digi--layout-container--margin-bottom);box-sizing:border-box}@media (min-width: 23.4375rem){.sc-digi-layout-container-h .digi-layout-container.sc-digi-layout-container{--digi--layout-container--width:var(--digi--container-width--small);--digi--layout-container--gutter:var(--digi--container-gutter--small)}}@media (min-width: 48rem){.sc-digi-layout-container-h .digi-layout-container.sc-digi-layout-container{--digi--layout-container--width:var(--digi--container-width--medium);--digi--layout-container--gutter:var(--digi--container-gutter--medium)}}@media (min-width: 62rem){.sc-digi-layout-container-h .digi-layout-container.sc-digi-layout-container{--digi--layout-container--width:var(--digi--container-width--large);--digi--layout-container--gutter:var(--digi--container-gutter--large)}}@media (min-width: 80rem){.sc-digi-layout-container-h .digi-layout-container.sc-digi-layout-container{--digi--layout-container--width:var(--digi--container-width--larger);--digi--layout-container--gutter:var(--digi--container-gutter--larger)}}@media (min-width: 80rem){.sc-digi-layout-container-h .digi-layout-container.sc-digi-layout-container{--digi--layout-container--width:var(--digi--container-width--largest);--digi--layout-container--gutter:var(--digi--container-gutter--largest)}}.sc-digi-layout-container-h .digi-layout-container--fluid.sc-digi-layout-container{--digi--layout-container--width:none !important}.sc-digi-layout-container-h .digi-layout-container--no-gutter.sc-digi-layout-container{--digi--layout-container--gutter:0}.sc-digi-layout-container-h .digi-layout-container--vertical-padding.sc-digi-layout-container{--digi--layout-container--vertical-padding:var(--digi--padding--large)}.sc-digi-layout-container-h .digi-layout-container--margin-top.sc-digi-layout-container{--digi--layout-container--margin-top:var(--digi--margin--large)}.sc-digi-layout-container-h .digi-layout-container--margin-bottom.sc-digi-layout-container{--digi--layout-container--margin-bottom:var(--digi--margin--large)}";

const LayoutContainer = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this._variation = LayoutContainerVariation.STATIC;
    this.afVariation = LayoutContainerVariation.STATIC;
    this.afNoGutter = undefined;
    this.afVerticalPadding = undefined;
    this.afMarginTop = undefined;
    this.afMarginBottom = undefined;
  }
  variationChangeHandler() {
    this._variation = this.afVariation;
  }
  componentWillLoad() {
    this.variationChangeHandler();
  }
  get cssModifiers() {
    if (this._variation === LayoutContainerVariation.NONE) {
      return {};
    }
    return {
      'digi-layout-container': true,
      'digi-layout-container--static': this._variation === LayoutContainerVariation.STATIC,
      'digi-layout-container--fluid': this._variation === LayoutContainerVariation.FLUID,
      'digi-layout-container--no-gutter': this.afNoGutter,
      'digi-layout-container--vertical-padding': this.afVerticalPadding,
      'digi-layout-container--margin-top': this.afMarginTop,
      'digi-layout-container--margin-bottom': this.afMarginBottom
    };
  }
  render() {
    return (h("div", { class: Object.assign({}, this.cssModifiers) }, h("slot", null)));
  }
  static get watchers() { return {
    "afVariation": ["variationChangeHandler"]
  }; }
  static get style() { return layoutContainerCss; }
}, [6, "digi-layout-container", {
    "afVariation": [1, "af-variation"],
    "afNoGutter": [4, "af-no-gutter"],
    "afVerticalPadding": [4, "af-vertical-padding"],
    "afMarginTop": [4, "af-margin-top"],
    "afMarginBottom": [4, "af-margin-bottom"],
    "_variation": [32]
  }]);
function defineCustomElement() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-layout-container"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-layout-container":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, LayoutContainer);
      }
      break;
  } });
}
defineCustomElement();

export { LayoutContainer as L, defineCustomElement as d };
