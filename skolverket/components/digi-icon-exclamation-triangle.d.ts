import type { Components, JSX } from "../dist/types/components";

interface DigiIconExclamationTriangle extends Components.DigiIconExclamationTriangle, HTMLElement {}
export const DigiIconExclamationTriangle: {
  prototype: DigiIconExclamationTriangle;
  new (): DigiIconExclamationTriangle;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
