import type { Components, JSX } from "../dist/types/components";

interface DigiIconExclamationTriangleWarning extends Components.DigiIconExclamationTriangleWarning, HTMLElement {}
export const DigiIconExclamationTriangleWarning: {
  prototype: DigiIconExclamationTriangleWarning;
  new (): DigiIconExclamationTriangleWarning;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
