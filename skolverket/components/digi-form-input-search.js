import { proxyCustomElement, HTMLElement, createEvent, h } from '@stencil/core/internal/client';
import { r as randomIdGenerator } from './randomIdGenerator.util.js';
import { F as FormInputSearchVariation } from './form-input-search-variation.enum.js';
import { a as FormInputType, F as FormInputButtonVariation } from './form-input-variation.enum.js';
import { a as ButtonType } from './button-variation.enum.js';
import { d as detectClickOutside } from './detectClickOutside.util.js';
import { d as detectFocusOutside } from './detectFocusOutside.util.js';
import { d as defineCustomElement$6 } from './button.js';
import { d as defineCustomElement$5 } from './form-input.js';
import { d as defineCustomElement$4 } from './form-label.js';
import { d as defineCustomElement$3 } from './form-validation-message.js';
import { d as defineCustomElement$2 } from './icon.js';

const formInputSearchCss = ".sc-digi-form-input-search-h{width:100%}";

const FormInputSearch = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afOnFocusOutside = createEvent(this, "afOnFocusOutside", 7);
    this.afOnFocusInside = createEvent(this, "afOnFocusInside", 7);
    this.afOnClickOutside = createEvent(this, "afOnClickOutside", 7);
    this.afOnClickInside = createEvent(this, "afOnClickInside", 7);
    this.afOnChange = createEvent(this, "afOnChange", 7);
    this.afOnBlur = createEvent(this, "afOnBlur", 7);
    this.afOnKeyup = createEvent(this, "afOnKeyup", 7);
    this.afOnFocus = createEvent(this, "afOnFocus", 7);
    this.afOnFocusout = createEvent(this, "afOnFocusout", 7);
    this.afOnInput = createEvent(this, "afOnInput", 7);
    this.afOnClick = createEvent(this, "afOnClick", 7);
    this.afLabel = undefined;
    this.afLabelDescription = undefined;
    this.afType = FormInputType.SEARCH;
    this.afButtonVariation = FormInputButtonVariation.PRIMARY;
    this.afName = undefined;
    this.afId = randomIdGenerator('digi-form-input-search');
    this.value = undefined;
    this.afValue = undefined;
    this.afAutocomplete = undefined;
    this.afAriaAutocomplete = undefined;
    this.afAriaActivedescendant = undefined;
    this.afAriaLabelledby = undefined;
    this.afAriaDescribedby = undefined;
    this.afVariation = FormInputSearchVariation.MEDIUM;
    this.afHideButton = undefined;
    this.afButtonType = ButtonType.SUBMIT;
    this.afButtonText = undefined;
    this.afButtonAriaLabel = undefined;
    this.afButtonAriaLabelledby = undefined;
    this.afAutofocus = undefined;
  }
  onValueChanged(value) {
    this.afValue = value;
  }
  onAfValueChanged(value) {
    this.value = value;
  }
  /**
   * Hämtar en referens till inputelementet. Bra för att t.ex. sätta fokus programmatiskt.
   * @en Returns a reference to the input element. Handy for setting focus programmatically.
   */
  async afMGetFormControlElement() {
    const formControlElement = await this._input.afMGetFormControlElement();
    return formControlElement;
  }
  blurHandler(e) {
    this.afOnBlur.emit(e);
  }
  changeHandler(e) {
    this.afValue = this.value = e.target.value;
    this.afOnChange.emit(e);
  }
  focusHandler(e) {
    this.afOnFocus.emit(e);
  }
  focusoutHandler(e) {
    this.afOnFocusout.emit(e);
  }
  keyupHandler(e) {
    this.afOnKeyup.emit(e);
  }
  inputHandler(e) {
    this.afOnInput.emit(e);
  }
  clickHandler(e) {
    this.afOnClick.emit(e);
  }
  clickOutsideListener(e) {
    const target = e.target;
    const selector = `[data-af-identifier="${this.afId}"]`;
    if (detectClickOutside(target, selector)) {
      this.afOnClickOutside.emit(e);
    }
    else {
      this.afOnClickInside.emit(e);
    }
  }
  focusOutsideListener(e) {
    const target = e.target;
    const selector = `[data-af-identifier="${this.afId}"]`;
    if (detectFocusOutside(target, selector)) {
      this.afOnFocusOutside.emit(e);
    }
    else {
      this.afOnFocusInside.emit(e);
    }
  }
  componentWillLoad() {
    this.afValue ? (this.value = this.afValue) : (this.afValue = this.value);
  }
  get cssModifiers() {
    return {
      'digi-form-input-search--small': this.afVariation === FormInputSearchVariation.SMALL,
      'digi-form-input-search--medium': this.afVariation === FormInputSearchVariation.MEDIUM,
      'digi-form-input-search--large': this.afVariation === FormInputSearchVariation.LARGE
    };
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-form-input-search': true }, this.cssModifiers), "data-af-identifier": this.afId }, h("digi-form-input", { class: {
        'digi-form-input-search__input': true,
        'digi-form-input-search__input--no-button': this.afHideButton
      }, ref: (el) => (this._input = el), onAfOnBlur: (e) => this.blurHandler(e), onAfOnChange: (e) => this.changeHandler(e), onAfOnFocus: (e) => this.focusHandler(e), onAfOnFocusout: (e) => this.focusoutHandler(e), onAfOnKeyup: (e) => this.keyupHandler(e), onAfOnInput: (e) => this.inputHandler(e), afLabel: this.afLabel, afLabelDescription: this.afLabelDescription, afAriaActivedescendant: this.afAriaActivedescendant, afAriaDescribedby: this.afAriaDescribedby, afAriaLabelledby: this.afAriaLabelledby, afAriaAutocomplete: this.afAriaAutocomplete, afAutocomplete: this.afAutocomplete, afName: this.afName, afType: this.afType, afValue: this.afValue, afVariation: this.afVariation, afAutofocus: this.afAutofocus ? this.afAutofocus : null, afButtonVariation: this.afButtonVariation }, !this.afHideButton && (h("digi-button", { class: "digi-form-input-search__button", onAfOnClick: (e) => this.clickHandler(e), afType: this.afButtonType, afAriaLabel: this.afButtonAriaLabel, afAriaLabelledby: this.afButtonAriaLabelledby, afSize: this.afVariation, slot: "button" }, h("digi-icon", { slot: "icon", afName: `search` }), this.afButtonText)))));
  }
  get hostElement() { return this; }
  static get watchers() { return {
    "value": ["onValueChanged"],
    "afValue": ["onAfValueChanged"]
  }; }
  static get style() { return formInputSearchCss; }
}, [2, "digi-form-input-search", {
    "afLabel": [1, "af-label"],
    "afLabelDescription": [1, "af-label-description"],
    "afType": [1, "af-type"],
    "afButtonVariation": [1, "af-button-variation"],
    "afName": [1, "af-name"],
    "afId": [1, "af-id"],
    "value": [1],
    "afValue": [1, "af-value"],
    "afAutocomplete": [1, "af-autocomplete"],
    "afAriaAutocomplete": [1, "af-aria-autocomplete"],
    "afAriaActivedescendant": [1, "af-aria-activedescendant"],
    "afAriaLabelledby": [1, "af-aria-labelledby"],
    "afAriaDescribedby": [1, "af-aria-describedby"],
    "afVariation": [1, "af-variation"],
    "afHideButton": [4, "af-hide-button"],
    "afButtonType": [1, "af-button-type"],
    "afButtonText": [1, "af-button-text"],
    "afButtonAriaLabel": [1, "af-button-aria-label"],
    "afButtonAriaLabelledby": [1, "af-button-aria-labelledby"],
    "afAutofocus": [4, "af-autofocus"],
    "afMGetFormControlElement": [64]
  }, [[8, "click", "clickOutsideListener"], [4, "focusin", "focusOutsideListener"]]]);
function defineCustomElement$1() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-form-input-search", "digi-button", "digi-form-input", "digi-form-label", "digi-form-validation-message", "digi-icon"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-form-input-search":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, FormInputSearch);
      }
      break;
    case "digi-button":
      if (!customElements.get(tagName)) {
        defineCustomElement$6();
      }
      break;
    case "digi-form-input":
      if (!customElements.get(tagName)) {
        defineCustomElement$5();
      }
      break;
    case "digi-form-label":
      if (!customElements.get(tagName)) {
        defineCustomElement$4();
      }
      break;
    case "digi-form-validation-message":
      if (!customElements.get(tagName)) {
        defineCustomElement$3();
      }
      break;
    case "digi-icon":
      if (!customElements.get(tagName)) {
        defineCustomElement$2();
      }
      break;
  } });
}
defineCustomElement$1();

const DigiFormInputSearch = FormInputSearch;
const defineCustomElement = defineCustomElement$1;

export { DigiFormInputSearch, defineCustomElement };
