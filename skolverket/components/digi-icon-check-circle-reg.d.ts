import type { Components, JSX } from "../dist/types/components";

interface DigiIconCheckCircleReg extends Components.DigiIconCheckCircleReg, HTMLElement {}
export const DigiIconCheckCircleReg: {
  prototype: DigiIconCheckCircleReg;
  new (): DigiIconCheckCircleReg;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
