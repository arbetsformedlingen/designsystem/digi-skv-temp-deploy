import { proxyCustomElement, HTMLElement, h } from '@stencil/core/internal/client';
import { T as TypographyMetaVariation } from './typography-meta-variation.enum.js';

const typographyMetaCss = ".sc-digi-typography-meta-h{--digi--typography-meta--font-weight--semibold:var(--digi--typography--preamble--font-weight--desktop);--digi--typography-meta--font-size:var(--digi--typography--preamble--font-size--desktop);--digi--typography-meta--font-family:var(--digi--global--typography--font-family--default);--digi--typography-meta--meta--font-weight:var(--digi--typography--body--font-weight--desktop);--digi--typography-meta--meta-secondary--font-weight:var(--digi--typography--body--font-weight--desktop)}.sc-digi-typography-meta-h .digi-typography-meta__meta.sc-digi-typography-meta,.sc-digi-typography-meta-h .digi-typography-meta__meta.sc-digi-typography-meta-s>*{font-weight:var(--digi--typography-meta--meta--font-weight)}.sc-digi-typography-meta-h .digi-typography-meta__meta-secondary.sc-digi-typography-meta,.sc-digi-typography-meta-h .digi-typography-meta__meta-secondary.sc-digi-typography-meta-s>*{font-weight:var(--digi--typography-meta--meta-secondary--font-weight)}.sc-digi-typography-meta-h .digi-typography-meta.sc-digi-typography-meta{font-size:var(--digi--typography-meta--font-size);font-family:var(--digi--typography-meta--font-family)}.sc-digi-typography-meta-h .digi-typography-meta--variation-primary.sc-digi-typography-meta .digi-typography-meta__meta.sc-digi-typography-meta{--digi--typography-meta--meta--font-weight:var(--digi--typography-meta--font-weight--semibold)}.sc-digi-typography-meta-h .digi-typography-meta--variation-secondary.sc-digi-typography-meta .digi-typography-meta__meta-secondary.sc-digi-typography-meta{--digi--typography-meta--meta-secondary--font-weight:var(--digi--typography-meta--font-weight--semibold)}";

const TypographyMeta = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afVariation = TypographyMetaVariation.PRIMARY;
  }
  get cssModifiers() {
    return {
      [`digi-typography-meta--variation-${this.afVariation}`]: !!this.afVariation
    };
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-typography-meta': true }, this.cssModifiers) }, h("div", { class: "digi-typography-meta__meta" }, h("slot", null)), h("div", { class: "digi-typography-meta__meta-secondary" }, h("slot", { name: "secondary" }))));
  }
  static get style() { return typographyMetaCss; }
}, [6, "digi-typography-meta", {
    "afVariation": [1, "af-variation"]
  }]);
function defineCustomElement$1() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-typography-meta"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-typography-meta":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, TypographyMeta);
      }
      break;
  } });
}
defineCustomElement$1();

const DigiTypographyMeta = TypographyMeta;
const defineCustomElement = defineCustomElement$1;

export { DigiTypographyMeta, defineCustomElement };
