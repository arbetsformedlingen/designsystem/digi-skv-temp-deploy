import type { Components, JSX } from "../dist/types/components";

interface DigiTypography extends Components.DigiTypography, HTMLElement {}
export const DigiTypography: {
  prototype: DigiTypography;
  new (): DigiTypography;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
