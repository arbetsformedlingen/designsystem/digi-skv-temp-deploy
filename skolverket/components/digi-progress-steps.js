import { proxyCustomElement, HTMLElement, h } from '@stencil/core/internal/client';
import { P as ProgressStepsHeadingLevel, b as ProgressStepsVariation, a as ProgressStepsStatus } from './progress-steps-variation.enum.js';
import { l as logger } from './logger.util.js';
import { d as defineCustomElement$2 } from './typography.js';

const progressStepsCss = ".sc-digi-progress-steps-h{display:block}";

const ProgressSteps = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afHeadingLevel = ProgressStepsHeadingLevel.H2;
    this.afVariation = ProgressStepsVariation.PRIMARY;
    this.afCurrentStep = 1;
    this.steps = [];
  }
  async afMNext() {
    this.afCurrentStep += 1;
  }
  async afMPrevious() {
    this.afCurrentStep -= 1;
  }
  componentWillLoad() {
    this.getSteps();
    this.setStepProps();
  }
  componentWillUpdate() {
    this.getSteps();
    this.setStepProps();
  }
  getSteps() {
    let steps = this.hostElement.querySelectorAll('digi-progress-step');
    if (!steps || steps.length <= 0) {
      logger.warn(`The slot contains no children elements.`, this.hostElement);
      return;
    }
    this.steps = [...Array.from(steps)].map((step) => {
      return {
        outerHTML: step.outerHTML,
        ref: step
      };
    });
  }
  setStepProps() {
    if (this.steps && this.afCurrentStep < 1) {
      logger.warn(`Current step is set to ${this.afCurrentStep} which is not allowed.`, this.hostElement);
      this.afCurrentStep = 1;
      return;
    }
    if (this.steps && this.afCurrentStep > this.steps.length + 1) {
      logger.warn(`Current step is set to ${this.afCurrentStep} which is more than the amount of available steps (${this.steps.length}).`, this.hostElement);
      this.afCurrentStep = this.steps.length + 1;
      return;
    }
    this.steps &&
      this.steps.forEach((step, index) => {
        step.ref.afVariation = this.afVariation;
        try {
          const topLevel = Number.parseInt(this.afHeadingLevel.split('h')[1]);
          step.ref.afHeadingLevel = 'h' + (topLevel);
          const isLast = index == this.steps.length - 1;
          step.ref.afIsLast = isLast;
          let status;
          if (index < this.afCurrentStep - 1) {
            status = ProgressStepsStatus.DONE;
          }
          else if (index == this.afCurrentStep - 1) {
            status = ProgressStepsStatus.CURRENT;
          }
          else {
            status = ProgressStepsStatus.UPCOMING;
          }
          step.ref.afStepStatus = status;
        }
        catch (e) {
          // Nothing
        }
      });
  }
  render() {
    return (h("div", { role: "list" }, h("digi-typography", null, h("slot", null))));
  }
  get hostElement() { return this; }
  static get style() { return progressStepsCss; }
}, [6, "digi-progress-steps", {
    "afHeadingLevel": [1, "af-heading-level"],
    "afVariation": [1, "af-variation"],
    "afCurrentStep": [2, "af-current-step"],
    "steps": [32],
    "afMNext": [64],
    "afMPrevious": [64]
  }]);
function defineCustomElement$1() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-progress-steps", "digi-typography"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-progress-steps":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, ProgressSteps);
      }
      break;
    case "digi-typography":
      if (!customElements.get(tagName)) {
        defineCustomElement$2();
      }
      break;
  } });
}
defineCustomElement$1();

const DigiProgressSteps = ProgressSteps;
const defineCustomElement = defineCustomElement$1;

export { DigiProgressSteps, defineCustomElement };
