import type { Components, JSX } from "../dist/types/components";

interface DigiIconExclamationCircle extends Components.DigiIconExclamationCircle, HTMLElement {}
export const DigiIconExclamationCircle: {
  prototype: DigiIconExclamationCircle;
  new (): DigiIconExclamationCircle;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
