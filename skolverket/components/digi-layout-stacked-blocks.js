import { L as LayoutStackedBlocks, d as defineCustomElement$1 } from './layout-stacked-blocks.js';

const DigiLayoutStackedBlocks = LayoutStackedBlocks;
const defineCustomElement = defineCustomElement$1;

export { DigiLayoutStackedBlocks, defineCustomElement };
