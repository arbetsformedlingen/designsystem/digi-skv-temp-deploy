import { F as FormSelect, d as defineCustomElement$1 } from './form-select.js';

const DigiFormSelect = FormSelect;
const defineCustomElement = defineCustomElement$1;

export { DigiFormSelect, defineCustomElement };
