import { U as UtilKeyupHandler, d as defineCustomElement$1 } from './util-keyup-handler.js';

const DigiUtilKeyupHandler = UtilKeyupHandler;
const defineCustomElement = defineCustomElement$1;

export { DigiUtilKeyupHandler, defineCustomElement };
