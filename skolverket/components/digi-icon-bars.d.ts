import type { Components, JSX } from "../dist/types/components";

interface DigiIconBars extends Components.DigiIconBars, HTMLElement {}
export const DigiIconBars: {
  prototype: DigiIconBars;
  new (): DigiIconBars;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
