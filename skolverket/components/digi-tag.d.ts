import type { Components, JSX } from "../dist/types/components";

interface DigiTag extends Components.DigiTag, HTMLElement {}
export const DigiTag: {
  prototype: DigiTag;
  new (): DigiTag;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
