import type { Components, JSX } from "../dist/types/components";

interface DigiUtilKeydownHandler extends Components.DigiUtilKeydownHandler, HTMLElement {}
export const DigiUtilKeydownHandler: {
  prototype: DigiUtilKeydownHandler;
  new (): DigiUtilKeydownHandler;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
