var KEY_CODE;
(function (KEY_CODE) {
  KEY_CODE[KEY_CODE["LEFT_ARROW"] = 37] = "LEFT_ARROW";
  KEY_CODE[KEY_CODE["UP_ARROW"] = 38] = "UP_ARROW";
  KEY_CODE[KEY_CODE["RIGHT_ARROW"] = 39] = "RIGHT_ARROW";
  KEY_CODE[KEY_CODE["DOWN_ARROW"] = 40] = "DOWN_ARROW";
  KEY_CODE[KEY_CODE["ENTER"] = 13] = "ENTER";
  KEY_CODE[KEY_CODE["SPACE"] = 32] = "SPACE";
  KEY_CODE[KEY_CODE["ESCAPE"] = 27] = "ESCAPE";
  KEY_CODE[KEY_CODE["TAB"] = 9] = "TAB";
  KEY_CODE[KEY_CODE["SHIFT"] = 16] = "SHIFT";
  KEY_CODE[KEY_CODE["END"] = 35] = "END";
  KEY_CODE[KEY_CODE["HOME"] = 36] = "HOME";
  KEY_CODE["SHIFT_TAB"] = "shift_tab";
  KEY_CODE["ANY"] = "any";
})(KEY_CODE || (KEY_CODE = {}));

function keyboardHandler(event) {
  const key = (event === null || event === void 0 ? void 0 : event.key) || (event === null || event === void 0 ? void 0 : event.keyCode);
  if (event.shiftKey && (key === 'Tab' || key === KEY_CODE.TAB)) {
    return KEY_CODE.SHIFT_TAB;
  }
  switch (key) {
    case 'Down':
    case 'ArrowDown':
    case KEY_CODE.DOWN_ARROW:
      return KEY_CODE.DOWN_ARROW;
    case 'Up':
    case 'ArrowUp':
    case KEY_CODE.UP_ARROW:
      return KEY_CODE.UP_ARROW;
    case 'Left':
    case 'ArrowLeft':
    case KEY_CODE.LEFT_ARROW:
      return KEY_CODE.LEFT_ARROW;
    case 'Right':
    case 'ArrowRight':
    case KEY_CODE.RIGHT_ARROW:
      return KEY_CODE.RIGHT_ARROW;
    case 'Enter':
    case KEY_CODE.ENTER:
      return KEY_CODE.ENTER;
    case 'Esc':
    case 'Escape':
    case KEY_CODE.ESCAPE:
      return KEY_CODE.ESCAPE;
    case 'Tab':
    case KEY_CODE.TAB:
      return KEY_CODE.TAB;
    case ' ':
    case 'Spacebar':
    case KEY_CODE.SPACE:
      return KEY_CODE.SPACE;
    case 'Home':
    case KEY_CODE.HOME:
      return KEY_CODE.HOME;
    case 'End':
    case KEY_CODE.END:
      return KEY_CODE.END;
    default:
      return KEY_CODE.ANY;
  }
}

export { KEY_CODE as K, keyboardHandler as k };
