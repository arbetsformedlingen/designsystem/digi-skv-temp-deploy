import { proxyCustomElement, HTMLElement, createEvent, h } from '@stencil/core/internal/client';
import { r as randomIdGenerator } from './randomIdGenerator.util.js';

const navigationTabCss = ".sc-digi-navigation-tab-h{--digi--navigation-tab--box-shadow--focus:solid 2px var(--digi--color--border--secondary)}.sc-digi-navigation-tab-h .digi-navigation-tab.sc-digi-navigation-tab:focus-visible{box-shadow:var(--digi--navigation-tab--box-shadow--focus);outline:none}";

const NavigationTab = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afOnToggle = createEvent(this, "afOnToggle", 7);
    this.afAriaLabel = undefined;
    this.afId = randomIdGenerator('digi-navigation-tab');
    this.afActive = undefined;
  }
  toggleHandler(activeTab) {
    if (activeTab || null) {
      this.afOnToggle.emit(activeTab);
    }
  }
  render() {
    return (h("div", { class: "digi-navigation-tab", tabindex: "0", role: "tabpanel", id: this.afId, "aria-label": this.afAriaLabel, hidden: !this.afActive }, h("slot", null)));
  }
  static get watchers() { return {
    "afActive": ["toggleHandler"]
  }; }
  static get style() { return navigationTabCss; }
}, [6, "digi-navigation-tab", {
    "afAriaLabel": [1, "af-aria-label"],
    "afId": [1, "af-id"],
    "afActive": [4, "af-active"]
  }]);
function defineCustomElement() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-navigation-tab"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-navigation-tab":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, NavigationTab);
      }
      break;
  } });
}
defineCustomElement();

export { NavigationTab as N, defineCustomElement as d };
