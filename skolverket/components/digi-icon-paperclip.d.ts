import type { Components, JSX } from "../dist/types/components";

interface DigiIconPaperclip extends Components.DigiIconPaperclip, HTMLElement {}
export const DigiIconPaperclip: {
  prototype: DigiIconPaperclip;
  new (): DigiIconPaperclip;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
