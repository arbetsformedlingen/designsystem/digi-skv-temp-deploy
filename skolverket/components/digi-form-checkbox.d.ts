import type { Components, JSX } from "../dist/types/components";

interface DigiFormCheckbox extends Components.DigiFormCheckbox, HTMLElement {}
export const DigiFormCheckbox: {
  prototype: DigiFormCheckbox;
  new (): DigiFormCheckbox;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
