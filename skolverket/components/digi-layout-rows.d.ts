import type { Components, JSX } from "../dist/types/components";

interface DigiLayoutRows extends Components.DigiLayoutRows, HTMLElement {}
export const DigiLayoutRows: {
  prototype: DigiLayoutRows;
  new (): DigiLayoutRows;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
