var LayoutBlockContainer;
(function (LayoutBlockContainer) {
  LayoutBlockContainer["STATIC"] = "static";
  LayoutBlockContainer["FLUID"] = "fluid";
  LayoutBlockContainer["NONE"] = "none";
})(LayoutBlockContainer || (LayoutBlockContainer = {}));

var LayoutBlockVariation;
(function (LayoutBlockVariation) {
  LayoutBlockVariation["TRANSPARENT"] = "transparent";
  LayoutBlockVariation["PRIMARY"] = "primary";
  LayoutBlockVariation["SECONDARY"] = "secondary";
  LayoutBlockVariation["TERTIARY"] = "tertiary";
  LayoutBlockVariation["SYMBOL"] = "symbol";
  LayoutBlockVariation["PROFILE"] = "profile";
})(LayoutBlockVariation || (LayoutBlockVariation = {}));

export { LayoutBlockContainer as L, LayoutBlockVariation as a };
