import type { Components, JSX } from "../dist/types/components";

interface DigiPageHeader extends Components.DigiPageHeader, HTMLElement {}
export const DigiPageHeader: {
  prototype: DigiPageHeader;
  new (): DigiPageHeader;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
