import { proxyCustomElement, HTMLElement, h } from '@stencil/core/internal/client';
import { C as CardBoxGutter, d as defineCustomElement$2 } from './card-box.js';
import { T as TableVariation } from './table-variation.enum.js';

const tableCss = ".sc-digi-table-h{--digi--table--font-family:var(--digi--global--typography--font-family--default);--digi--table--font-size:var(--digi--typography--body--font-size--desktop);--digi--table--color:var(--digi--color--text--primary);--digi--table--font-weight:var(--digi--typography--body--font-weight--desktop);--digi--table--cell--margin:var(--digi--gutter--small);--digi--table--cell--padding--y:var(--digi--gutter--larger);--digi--table--cell--padding--x:var(--digi--gutter--larger);--digi--table--cell--padding--y--s:var(--digi--gutter--small);--digi--table--cell--padding--x--s:var(--digi--gutter--small);--digi--table--th--border-color:var(--digi--color--border--primary);--digi--table--td--border-color:var(--digi--color--border--neutral-2);--digi--table--background:var(--digi--color--background--neutral-6);--digi--table--background--even:var(--digi--color--background--secondary);--digi--table--color--active:var(--digi--color--text--inverted);--digi--table--background--active:var(--digi--color--background--complementary-1)}.sc-digi-table-h .sc-digi-table-s>table{font-family:var(--digi--table--font-family);font-size:var(--digi--table--font-size);color:var(--digi--table--color);font-weight:var(--digi--table--font-weight);text-align:left;width:100%;border-collapse:collapse}.sc-digi-table-h .sc-digi-table-s>table th,.sc-digi-table-h .sc-digi-table-s>table td{-webkit-border-after:var(--digi--border-width--tertiary) var(--digi--border-style--primary) var(--digi--global--color--profile--purple--opacity15);border-block-end:var(--digi--border-width--tertiary) var(--digi--border-style--primary) var(--digi--global--color--profile--purple--opacity15)}.sc-digi-table-h .sc-digi-table-s>table th,.sc-digi-table-h .sc-digi-table-s>table td{padding:calc((59px - 1em) / 2) var(--digi--table--cell--padding--x)}.sc-digi-table-h .sc-digi-table-s>table tr[data-af-table-row~=active],.sc-digi-table-h .sc-digi-table-s>table th[data-af-table-cell~=active],.sc-digi-table-h .sc-digi-table-s>table td[data-af-table-cell~=active]{color:var(--digi--table--color--active);background-color:var(--digi--table--background--active)}.sc-digi-table-h .sc-digi-table-s>table th[data-af-table-cell~=center],.sc-digi-table-h .sc-digi-table-s>table td[data-af-table-cell~=center]{text-align:center}.sc-digi-table-h .sc-digi-table-s>table th[data-af-table-cell~=right],.sc-digi-table-h .sc-digi-table-s>table td[data-af-table-cell~=right]{text-align:right}.sc-digi-table-h .sc-digi-table-s>table thead{--digi--heading-4--font-size:var(--digi--typography--heading-4--font-size--mobile);--digi--heading-4--line-height:var(--digi--typography--heading-4--line-height--mobile);font-family:var(--digi--global--typography--font-family--default);font-size:var(--digi--heading-4--font-size);line-height:var(--digi--heading-4--line-height);font-weight:var(--digi--typography--heading-4--font-weight--mobile);-webkit-margin-after:0;margin-block-end:0;background:var(--digi--color--background--secondary)}@media (min-width: 48rem){.sc-digi-table-h .sc-digi-table-s>table thead{--digi--heading-4--font-size:var(--digi--typography--heading-4--font-size--desktop);--digi--heading-4--line-height:var(--digi--typography--heading-4--line-height--desktop)}}@media (min-width: 62rem){.sc-digi-table-h .sc-digi-table-s>table thead{--digi--heading-4--font-size:var(--digi--typography--heading-4--font-size--desktop-large);--digi--heading-4--line-height:var(--digi--typography--heading-4--line-height--desktop-large)}}.sc-digi-table-h .sc-digi-table-s>table thead th{padding:calc((59px - var(--digi--heading-4--line-height)) / 2) var(--digi--table--cell--padding--x)}.sc-digi-table-h .digi-table--variation-secondary.sc-digi-table{-webkit-padding-after:60px;padding-block-end:60px}";

const Table = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afVariation = TableVariation.PRIMARY;
  }
  get cssModifiers() {
    return {
      [`digi-table--variation-${this.afVariation}`]: !!this.afVariation
    };
  }
  render() {
    return this.afVariation === TableVariation.SECONDARY ? (h("digi-card-box", { afGutter: CardBoxGutter.NONE }, h("div", { class: Object.assign({ 'digi-table': true }, this.cssModifiers) }, h("slot", null)))) : (h("div", { class: Object.assign({ 'digi-table': true }, this.cssModifiers) }, h("slot", null)));
  }
  static get style() { return tableCss; }
}, [6, "digi-table", {
    "afVariation": [1, "af-variation"]
  }]);
function defineCustomElement$1() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-table", "digi-card-box"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-table":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, Table);
      }
      break;
    case "digi-card-box":
      if (!customElements.get(tagName)) {
        defineCustomElement$2();
      }
      break;
  } });
}
defineCustomElement$1();

const DigiTable = Table;
const defineCustomElement = defineCustomElement$1;

export { DigiTable, defineCustomElement };
