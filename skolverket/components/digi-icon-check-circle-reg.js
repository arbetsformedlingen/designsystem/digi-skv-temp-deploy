import { I as IconCheckCircleReg, d as defineCustomElement$1 } from './icon-check-circle-reg.js';

const DigiIconCheckCircleReg = IconCheckCircleReg;
const defineCustomElement = defineCustomElement$1;

export { DigiIconCheckCircleReg, defineCustomElement };
