import { proxyCustomElement, HTMLElement, createEvent, h } from '@stencil/core/internal/client';

const utilResizeObserverCss = ".sc-digi-util-resize-observer-h{display:block}";

const UtilResizeObserver = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afOnChange = createEvent(this, "afOnChange", 7);
    this._observer = null;
  }
  componentWillLoad() {
    this.initObserver();
  }
  disconnectedCallback() {
    this.removeObserver();
  }
  initObserver() {
    this._observer = new ResizeObserver(([entry]) => {
      this.afOnChange.emit(entry);
    });
    this._observer.observe(this.hostElement);
  }
  removeObserver() {
    this._observer.disconnect();
  }
  changeHandler(e) {
    this.afOnChange.emit(e);
  }
  render() {
    return h("slot", null);
  }
  get hostElement() { return this; }
  static get style() { return utilResizeObserverCss; }
}, [6, "digi-util-resize-observer"]);
function defineCustomElement() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-util-resize-observer"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-util-resize-observer":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, UtilResizeObserver);
      }
      break;
  } });
}
defineCustomElement();

export { UtilResizeObserver as U, defineCustomElement as d };
