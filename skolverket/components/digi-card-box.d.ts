import type { Components, JSX } from "../dist/types/components";

interface DigiCardBox extends Components.DigiCardBox, HTMLElement {}
export const DigiCardBox: {
  prototype: DigiCardBox;
  new (): DigiCardBox;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
