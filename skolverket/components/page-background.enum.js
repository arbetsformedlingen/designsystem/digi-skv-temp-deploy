var DialogHeadingLevel;
(function (DialogHeadingLevel) {
  DialogHeadingLevel["H1"] = "h1";
  DialogHeadingLevel["H2"] = "h2";
  DialogHeadingLevel["H3"] = "h3";
  DialogHeadingLevel["H4"] = "h4";
  DialogHeadingLevel["H5"] = "h5";
  DialogHeadingLevel["H6"] = "h6";
})(DialogHeadingLevel || (DialogHeadingLevel = {}));

var DialogSize;
(function (DialogSize) {
  DialogSize["SMALL"] = "small";
  DialogSize["MEDIUM"] = "medium";
  DialogSize["LARGE"] = "large";
})(DialogSize || (DialogSize = {}));

var DialogVariation;
(function (DialogVariation) {
  DialogVariation["PRIMARY"] = "primary";
  DialogVariation["SECONDARY"] = "secondary";
})(DialogVariation || (DialogVariation = {}));

var NavigationMainMenuActiveIndicatorSize;
(function (NavigationMainMenuActiveIndicatorSize) {
  NavigationMainMenuActiveIndicatorSize["PRIMARY"] = "primary";
  NavigationMainMenuActiveIndicatorSize["SECONDARY"] = "secondary";
})(NavigationMainMenuActiveIndicatorSize || (NavigationMainMenuActiveIndicatorSize = {}));

var NavigationMainMenuVariation;
(function (NavigationMainMenuVariation) {
  NavigationMainMenuVariation["PRIMARY"] = "primary";
  NavigationMainMenuVariation["SECONDARY"] = "secondary";
})(NavigationMainMenuVariation || (NavigationMainMenuVariation = {}));

var PageBlockCardsVariation;
(function (PageBlockCardsVariation) {
  PageBlockCardsVariation["START"] = "start";
  PageBlockCardsVariation["SUB"] = "sub";
  PageBlockCardsVariation["SECTION"] = "section";
  PageBlockCardsVariation["PROCESS"] = "process";
  PageBlockCardsVariation["ARTICLE"] = "article";
})(PageBlockCardsVariation || (PageBlockCardsVariation = {}));

var PageBlockHeroBackground;
(function (PageBlockHeroBackground) {
  PageBlockHeroBackground["AUTO"] = "auto";
  PageBlockHeroBackground["TRANSPARENT"] = "transparent";
})(PageBlockHeroBackground || (PageBlockHeroBackground = {}));

var PageBlockHeroVariation;
(function (PageBlockHeroVariation) {
  PageBlockHeroVariation["START"] = "start";
  PageBlockHeroVariation["SUB"] = "sub";
  PageBlockHeroVariation["SECTION"] = "section";
  PageBlockHeroVariation["PROCESS"] = "process";
})(PageBlockHeroVariation || (PageBlockHeroVariation = {}));

var PageBlockListsVariation;
(function (PageBlockListsVariation) {
  PageBlockListsVariation["START"] = "start";
  PageBlockListsVariation["SUB"] = "sub";
  PageBlockListsVariation["SECTION"] = "section";
  PageBlockListsVariation["PROCESS"] = "process";
  PageBlockListsVariation["ARTICLE"] = "article";
})(PageBlockListsVariation || (PageBlockListsVariation = {}));

var PageBlockSidebarVariation;
(function (PageBlockSidebarVariation) {
  PageBlockSidebarVariation["START"] = "start";
  PageBlockSidebarVariation["SUB"] = "sub";
  PageBlockSidebarVariation["SECTION"] = "section";
  PageBlockSidebarVariation["PROCESS"] = "process";
  PageBlockSidebarVariation["ARTICLE"] = "article";
})(PageBlockSidebarVariation || (PageBlockSidebarVariation = {}));

var PageBackground;
(function (PageBackground) {
  PageBackground["STRIPED_1"] = "striped_1";
  PageBackground["STRIPED_2"] = "striped_2";
  PageBackground["STRIPED_3"] = "striped_3";
  PageBackground["SQUARE_1"] = "square_1";
  PageBackground["SQUARE_2"] = "square_2";
  PageBackground["SQUARE_3"] = "square_3";
  PageBackground["DOTTED_1"] = "dotted_1";
  PageBackground["DOTTED_2"] = "dotted_2";
  PageBackground["DOTTED_3"] = "dotted_3";
})(PageBackground || (PageBackground = {}));

export { DialogHeadingLevel as D, NavigationMainMenuActiveIndicatorSize as N, PageBlockCardsVariation as P, DialogSize as a, DialogVariation as b, NavigationMainMenuVariation as c, PageBlockHeroBackground as d, PageBlockHeroVariation as e, PageBlockListsVariation as f, PageBlockSidebarVariation as g, PageBackground as h };
