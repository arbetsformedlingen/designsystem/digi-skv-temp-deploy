import type { Components, JSX } from "../dist/types/components";

interface DigiPage extends Components.DigiPage, HTMLElement {}
export const DigiPage: {
  prototype: DigiPage;
  new (): DigiPage;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
