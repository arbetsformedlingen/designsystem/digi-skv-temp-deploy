import { L as Logo, d as defineCustomElement$1 } from './logo.js';

const DigiLogo = Logo;
const defineCustomElement = defineCustomElement$1;

export { DigiLogo, defineCustomElement };
