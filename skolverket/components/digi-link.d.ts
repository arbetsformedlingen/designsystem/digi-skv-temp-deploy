import type { Components, JSX } from "../dist/types/components";

interface DigiLink extends Components.DigiLink, HTMLElement {}
export const DigiLink: {
  prototype: DigiLink;
  new (): DigiLink;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
