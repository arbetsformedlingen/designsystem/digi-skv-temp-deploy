import type { Components, JSX } from "../dist/types/components";

interface DigiCodeBlock extends Components.DigiCodeBlock, HTMLElement {}
export const DigiCodeBlock: {
  prototype: DigiCodeBlock;
  new (): DigiCodeBlock;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
