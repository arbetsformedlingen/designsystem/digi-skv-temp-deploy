import { proxyCustomElement, HTMLElement, h } from '@stencil/core/internal/client';
import { r as randomIdGenerator } from './randomIdGenerator.util.js';
import { l as logger } from './logger.util.js';

const formLabelCss = ".sc-digi-form-label-h .digi-form-label.sc-digi-form-label{font-family:var(--digi--global--typography--font-family--default);font-size:var(--digi--typography--label--font-size--desktop);color:var(--digi--color--text--primary);display:flex;flex-direction:column}.sc-digi-form-label-h .digi-form-label__label.sc-digi-form-label{line-height:var(--digi--typography--label--line-height--desktop);font-weight:var(--digi--typography--label--font-weight--desktop);cursor:pointer}.sc-digi-form-label-h .digi-form-label__label-group.sc-digi-form-label{display:inline-flex;align-items:center;gap:var(--digi--gutter--icon)}.sc-digi-form-label-h .digi-form-label__description.sc-digi-form-label{max-width:var(--digi--paragraph-width--medium);line-height:var(--digi--typography--label-description--line-height--desktop);font-size:var(--digi--typography--label-description--font-size--desktop)}";

const FormLabel = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.labelText = undefined;
    this.hasActionSlot = undefined;
    this.afLabel = undefined;
    this.afId = randomIdGenerator('digi-form-label');
    this.afFor = undefined;
    this.afDescription = undefined;
    this.afRequired = undefined;
    this.afAnnounceIfOptional = false;
    this.afRequiredText = 'obligatoriskt';
    this.afAnnounceIfOptionalText = 'frivilligt';
  }
  componentWillLoad() {
    this.setLabelText();
    this.validateLabel();
    this.validateFor();
    this.handleSlotVisibility();
  }
  validateLabel() { }
  validateFor() {
    if (this.afFor)
      return;
    logger.warn(`digi-form-label must have a for attribute. Please add a for attribute using af-for`, this.hostElement);
  }
  setLabelText() {
    this.labelText = `${this.afLabel} ${this.requiredText}`;
    if (!this.afLabel) {
      logger.warn(`digi-form-label must have a label. Please add a label using af-label`, this.hostElement);
    }
  }
  handleSlotVisibility() {
    this.hasActionSlot = !!this.hostElement.querySelector('[slot="actions"]');
  }
  get requiredText() {
    return this.afRequired && !this.afAnnounceIfOptional
      ? ` (${this.afRequiredText})`
      : !this.afRequired && this.afAnnounceIfOptional
        ? ` (${this.afAnnounceIfOptionalText})`
        : '';
  }
  render() {
    return (h("div", { class: "digi-form-label" }, h("div", { class: "digi-form-label__label-group" }, this.afFor && this.afLabel && (h("label", { class: "digi-form-label__label", htmlFor: this.afFor, id: this.afId }, this.labelText)), this.hasActionSlot && (h("div", { class: "digi-form-label__actions" }, h("slot", { name: "actions" })))), this.afDescription && (h("p", { class: "digi-form-label__description" }, this.afDescription))));
  }
  get hostElement() { return this; }
  static get watchers() { return {
    "afLabel": ["validateLabel", "setLabelText"],
    "afFor": ["validateFor"],
    "afRequired": ["setLabelText"],
    "afAnnounceIfOptional": ["setLabelText"]
  }; }
  static get style() { return formLabelCss; }
}, [6, "digi-form-label", {
    "afLabel": [1, "af-label"],
    "afId": [1, "af-id"],
    "afFor": [1, "af-for"],
    "afDescription": [1, "af-description"],
    "afRequired": [4, "af-required"],
    "afAnnounceIfOptional": [4, "af-announce-if-optional"],
    "afRequiredText": [1, "af-required-text"],
    "afAnnounceIfOptionalText": [1, "af-announce-if-optional-text"],
    "labelText": [32],
    "hasActionSlot": [32]
  }]);
function defineCustomElement() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-form-label"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-form-label":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, FormLabel);
      }
      break;
  } });
}
defineCustomElement();

export { FormLabel as F, defineCustomElement as d };
