import { L as LayoutGrid, d as defineCustomElement$1 } from './layout-grid.js';

const DigiLayoutGrid = LayoutGrid;
const defineCustomElement = defineCustomElement$1;

export { DigiLayoutGrid, defineCustomElement };
