import { proxyCustomElement, HTMLElement, h } from '@stencil/core/internal/client';
import { N as NotificationDetailVariation } from './notification-detail-variation.enum.js';
import './button-variation.enum.js';
import './expandable-accordion-variation.enum.js';
import './calendar-week-view-heading-level.enum.js';
import './code-block-variation.enum.js';
import './code-example-variation.enum.js';
import './code-variation.enum.js';
import './form-checkbox-variation.enum.js';
import './form-file-upload-variation.enum.js';
import './form-input-search-variation.enum.js';
import './form-input-variation.enum.js';
import './form-radiobutton-variation.enum.js';
import './form-select-variation.enum.js';
import './form-textarea-variation.enum.js';
import './form-validation-message-variation.enum.js';
import './layout-block-variation.enum.js';
import './layout-columns-variation.enum.js';
import './layout-container-variation.enum.js';
import './layout-media-object-alignment.enum.js';
import './link-external-variation.enum.js';
import './link-internal-variation.enum.js';
import './link-variation.enum.js';
import './loader-spinner-size.enum.js';
import './media-figure-alignment.enum.js';
import './navigation-context-menu-item-type.enum.js';
import './navigation-sidebar-variation.enum.js';
import './navigation-vertical-menu-variation.enum.js';
import './progress-step-variation.enum.js';
import './progress-steps-variation.enum.js';
import './progressbar-variation.enum.js';
import './tag-size.enum.js';
import './typography-meta-variation.enum.js';
import './typography-time-variation.enum.js';
import { T as TypographyVariation } from './typography-variation.enum.js';
import './util-breakpoint-observer-breakpoints.enum.js';
import { d as defineCustomElement$3 } from './icon.js';
import { d as defineCustomElement$2 } from './typography.js';

const notificationDetailCss = ".sc-digi-notification-detail-h{--digi--notification-detail--border-color--info:var(--digi--color--border--informative);--digi--notification-detail--border-color--warning:var(--digi--color--border--warning);--digi--notification-detail--border-color--danger:var(--digi--color--border--danger)}.sc-digi-notification-detail-h .sc-digi-notification-detail-s>[slot^=heading]{--digi--heading-4--font-size:var(--digi--typography--heading-4--font-size--mobile);--digi--heading-4--line-height:var(--digi--typography--heading-4--line-height--mobile);font-family:var(--digi--global--typography--font-family--default);font-size:var(--digi--heading-4--font-size);line-height:var(--digi--heading-4--line-height);font-weight:var(--digi--typography--heading-4--font-weight--mobile);-webkit-margin-after:0;margin-block-end:0}@media (min-width: 48rem){.sc-digi-notification-detail-h .sc-digi-notification-detail-s>[slot^=heading]{--digi--heading-4--font-size:var(--digi--typography--heading-4--font-size--desktop);--digi--heading-4--line-height:var(--digi--typography--heading-4--line-height--desktop)}}@media (min-width: 62rem){.sc-digi-notification-detail-h .sc-digi-notification-detail-s>[slot^=heading]{--digi--heading-4--font-size:var(--digi--typography--heading-4--font-size--desktop-large);--digi--heading-4--line-height:var(--digi--typography--heading-4--line-height--desktop-large)}}.digi-notification-detail.sc-digi-notification-detail{-webkit-border-start:var(--digi--border-width--secondary) solid var(--digi--color--border--secondary);border-inline-start:var(--digi--border-width--secondary) solid var(--digi--color--border--secondary);border-inline-start-color:var(--BORDER-COLOR);background:var(--digi--color--background--notification-info)}.digi-notification-detail--variation-info.sc-digi-notification-detail{--BORDER-COLOR:var(--digi--notification-detail--border-color--info);--ICON-COLOR:var(--digi--notification-detail--border-color--info)}.digi-notification-detail--variation-danger.sc-digi-notification-detail{--BORDER-COLOR:var(--digi--notification-detail--border-color--danger);--ICON-COLOR:var(--digi--notification-detail--border-color--danger)}.digi-notification-detail--variation-warning.sc-digi-notification-detail{--BORDER-COLOR:var(--digi--notification-detail--border-color--warning);--ICON-COLOR:var(--digi--notification-detail--border-color--warning)}.digi-notification-detail__inner.sc-digi-notification-detail{display:grid;grid-template-columns:min-content 1fr;grid-template-areas:\"icon content\";gap:calc(var(--digi--gutter--icon) * 2);padding:var(--digi--gutter--medium) var(--digi--gutter--larger);-webkit-padding-end:calc(var(--digi--gutter--larger) * 2);padding-inline-end:calc(var(--digi--gutter--larger) * 2)}@media (max-width: 47.9375rem){.digi-notification-detail__inner.sc-digi-notification-detail{justify-content:space-between;align-items:center}}.digi-notification-detail__icon.sc-digi-notification-detail{grid-area:icon}.digi-notification-detail__icon.sc-digi-notification-detail digi-icon.sc-digi-notification-detail{--digi--icon--color:var(--ICON-COLOR);--digi-icon--fixed-width:32px;--digi-icon--fixed-height:32px}.digi-notification-detail__content.sc-digi-notification-detail{grid-area:content}.digi-notification-detail__text.sc-digi-notification-detail{--digi--typography--body--font-size--desktop:var(--digi--global--typography--font-size--smaller);--digi--typography--body--font-size--mobile:var(--digi--global--typography--font-size--smaller);--digi--typography--body--line-height--desktop:var(--digi--global--typography--line-height--smaller);--digi--typography--body--line-height--mobile:var(--digi--global--typography--line-height--smaller)}";

const NotificationDetail = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afVariation = NotificationDetailVariation.INFO;
  }
  get cssModifiers() {
    return {
      [`digi-notification-detail--variation-${this.afVariation}`]: !!this.afVariation
    };
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-notification-detail': true }, this.cssModifiers) }, h("div", { class: "digi-notification-detail__inner" }, h("div", { class: "digi-notification-detail__icon" }, h("digi-icon", { afName: `notification-${this.afVariation}`, "aria-hidden": "true" })), h("div", { class: "digi-notification-detail__content" }, h("slot", { name: "heading" }), h("digi-typography", { class: "digi-notification-detail__text", "af-variation": TypographyVariation.SMALL }, h("slot", null))))));
  }
  static get style() { return notificationDetailCss; }
}, [6, "digi-notification-detail", {
    "afVariation": [1, "af-variation"]
  }]);
function defineCustomElement$1() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-notification-detail", "digi-icon", "digi-typography"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-notification-detail":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, NotificationDetail);
      }
      break;
    case "digi-icon":
      if (!customElements.get(tagName)) {
        defineCustomElement$3();
      }
      break;
    case "digi-typography":
      if (!customElements.get(tagName)) {
        defineCustomElement$2();
      }
      break;
  } });
}
defineCustomElement$1();

const DigiNotificationDetail = NotificationDetail;
const defineCustomElement = defineCustomElement$1;

export { DigiNotificationDetail, defineCustomElement };
