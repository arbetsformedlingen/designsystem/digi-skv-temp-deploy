import type { Components, JSX } from "../dist/types/components";

interface DigiIconCheckCircle extends Components.DigiIconCheckCircle, HTMLElement {}
export const DigiIconCheckCircle: {
  prototype: DigiIconCheckCircle;
  new (): DigiIconCheckCircle;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
