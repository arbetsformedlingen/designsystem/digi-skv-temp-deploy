import { proxyCustomElement, HTMLElement, h } from '@stencil/core/internal/client';
import { L as LayoutStackedBlocksVariation } from './layout-stacked-blocks-variation.enum.js';

const layoutStackedBlocksCss = ".sc-digi-layout-stacked-blocks-h{--digi--layout-stacked-blocks--columns:1}.sc-digi-layout-stacked-blocks-h[af-variation=enhanced] .sc-digi-layout-stacked-blocks-s>:first-child{grid-column:1/-1}@media (min-width: 48rem){.sc-digi-layout-stacked-blocks-h{--digi--layout-stacked-blocks--columns:2}}@media (min-width: 62rem){.sc-digi-layout-stacked-blocks-h:not([af-variation=enhanced]){--digi--layout-stacked-blocks--columns:3}}.sc-digi-layout-stacked-blocks-h .digi-layout-stacked-blocks.sc-digi-layout-stacked-blocks{display:grid;grid-template-columns:repeat(auto-fit, minmax(calc((100% - var(--digi--responsive-grid-gutter) * (var(--digi--layout-stacked-blocks--columns) - 1)) / var(--digi--layout-stacked-blocks--columns)), 1fr));gap:var(--digi--responsive-grid-gutter)}";

const LayoutStackedBlocks = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afVariation = LayoutStackedBlocksVariation.REGULAR;
  }
  render() {
    return (h("div", { class: {
        'digi-layout-stacked-blocks': true,
        [`digi-layout-stacked-blocks--variation-${this.afVariation}`]: !!this.afVariation
      } }, h("slot", null)));
  }
  static get style() { return layoutStackedBlocksCss; }
}, [6, "digi-layout-stacked-blocks", {
    "afVariation": [1, "af-variation"]
  }]);
function defineCustomElement() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-layout-stacked-blocks"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-layout-stacked-blocks":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, LayoutStackedBlocks);
      }
      break;
  } });
}
defineCustomElement();

export { LayoutStackedBlocks as L, defineCustomElement as d };
