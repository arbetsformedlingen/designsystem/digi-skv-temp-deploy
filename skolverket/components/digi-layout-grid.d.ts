import type { Components, JSX } from "../dist/types/components";

interface DigiLayoutGrid extends Components.DigiLayoutGrid, HTMLElement {}
export const DigiLayoutGrid: {
  prototype: DigiLayoutGrid;
  new (): DigiLayoutGrid;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
