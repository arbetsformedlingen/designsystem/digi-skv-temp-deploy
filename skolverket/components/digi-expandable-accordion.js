import { proxyCustomElement, HTMLElement, createEvent, h } from '@stencil/core/internal/client';
import { b as ButtonVariation, a as ButtonType } from './button-variation.enum.js';
import { c as ExpandableAccordionVariation, E as ExpandableAccordionHeaderLevel } from './expandable-accordion-variation.enum.js';
import './calendar-week-view-heading-level.enum.js';
import './code-block-variation.enum.js';
import './code-example-variation.enum.js';
import './code-variation.enum.js';
import './form-checkbox-variation.enum.js';
import './form-file-upload-variation.enum.js';
import './form-input-search-variation.enum.js';
import './form-input-variation.enum.js';
import './form-radiobutton-variation.enum.js';
import './form-select-variation.enum.js';
import './form-textarea-variation.enum.js';
import './form-validation-message-variation.enum.js';
import './layout-block-variation.enum.js';
import './layout-columns-variation.enum.js';
import './layout-container-variation.enum.js';
import './layout-media-object-alignment.enum.js';
import './link-external-variation.enum.js';
import './link-internal-variation.enum.js';
import './link-variation.enum.js';
import './loader-spinner-size.enum.js';
import './media-figure-alignment.enum.js';
import './navigation-context-menu-item-type.enum.js';
import './navigation-sidebar-variation.enum.js';
import './navigation-vertical-menu-variation.enum.js';
import './progress-step-variation.enum.js';
import './progress-steps-variation.enum.js';
import './progressbar-variation.enum.js';
import './tag-size.enum.js';
import './typography-meta-variation.enum.js';
import './typography-time-variation.enum.js';
import './typography-variation.enum.js';
import './util-breakpoint-observer-breakpoints.enum.js';
import { r as randomIdGenerator } from './randomIdGenerator.util.js';
import { d as defineCustomElement$4 } from './button.js';
import { d as defineCustomElement$3 } from './icon.js';
import { d as defineCustomElement$2 } from './typography.js';

const expandableAccordionCss = ".sc-digi-expandable-accordion-h{--digi--expandable-accordion--header--toggle-icon--transition:ease-in-out var(--digi--animation--duration--base) all;--digi--expandable-accordion--header--font-size:var(--digi--typography--accordion--font-size--desktop);--digi--expandable-accordion--header--font-size--small:var(--digi--typography--accordion--font-size--mobile);--digi--expandable-accordion--header--font-weight:var(--digi--typography--accordion--font-weight--desktop);--digi--expandable-accordion--icon--size:0.875rem;--digi--expandable-accordion--icon--margin-right:var(--digi--margin--medium);--digi--expandable-accordion--border-bottom-width:var(--digi--border-width--secondary);--digi--expandable-accordion--border-bottom-color:var(--digi--color--border--informative);--digi--expandable-accordion--content--padding:var(--digi--padding--medium) 0;--digi--expandable-accordion--content--transition:ease-in-out var(--digi--animation--duration--base) height;display:block}.sc-digi-expandable-accordion-h .digi-expandable-accordion.sc-digi-expandable-accordion::after{content:\"\";display:block;max-width:var(--digi--paragraph-width--medium);border-bottom-width:var(--digi--expandable-accordion--border-bottom-width);border-bottom-color:var(--digi--expandable-accordion--border-bottom-color);border-bottom-style:solid;margin-top:calc(var(--digi--expandable-accordion--border-bottom-width) * -1)}.sc-digi-expandable-accordion-h .digi-expandable-accordion--variation-secondary.sc-digi-expandable-accordion::after{content:none}.digi-expandable-accordion__header.sc-digi-expandable-accordion{display:flex;align-items:center}.digi-expandable-accordion--variation-primary.sc-digi-expandable-accordion .digi-expandable-accordion__header.sc-digi-expandable-accordion{max-width:var(--digi--paragraph-width--medium);border-bottom-width:var(--digi--expandable-accordion--border-bottom-width);border-bottom-color:var(--digi--expandable-accordion--border-bottom-color);border-bottom-style:solid}.digi-expandable-accordion__toggle-icon.sc-digi-expandable-accordion{transition:var(--digi--expandable-accordion--header--toggle-icon--transition)}.digi-expandable-accordion--expanded-true.sc-digi-expandable-accordion .digi-expandable-accordion__toggle-icon.sc-digi-expandable-accordion{transform:rotate(-180deg)}.digi-expandable-accordion__heading.sc-digi-expandable-accordion{margin:0;flex:1}.digi-expandable-accordion__button.sc-digi-expandable-accordion{font-family:var(--digi--global--typography--font-family--default);font-size:var(--digi--expandable-accordion--header--font-size--small);font-weight:var(--digi--expandable-accordion--header--font-weight);width:100%;display:flex;gap:var(--digi--expandable-accordion--icon--margin-right);align-items:center;text-align:start;cursor:pointer}@media (min-width: 48rem){.digi-expandable-accordion__button.sc-digi-expandable-accordion{font-size:var(--digi--expandable-accordion--header--font-size)}}.digi-expandable-accordion--variation-primary.sc-digi-expandable-accordion .digi-expandable-accordion__button.sc-digi-expandable-accordion{color:var(--digi--color--text--primary);background-color:transparent;padding:var(--digi--expandable-accordion--content--padding)}.digi-expandable-accordion--variation-primary.sc-digi-expandable-accordion .digi-expandable-accordion__button.sc-digi-expandable-accordion:hover{text-decoration:underline}.digi-expandable-accordion--variation-secondary.sc-digi-expandable-accordion .digi-expandable-accordion__button.sc-digi-expandable-accordion{justify-content:space-between;min-height:var(--digi--input-height--large);padding:0 var(--digi--gutter--medium);border-radius:var(--digi--border-radius--primary);background-color:var(--digi--color--background--input-empty);border:1px solid var(--digi--color--background--tertiary)}.digi-expandable-accordion--variation-secondary.sc-digi-expandable-accordion .digi-expandable-accordion__button.sc-digi-expandable-accordion:focus-visible{outline:var(--digi--focus-outline);background-color:var(--digi--color--background--tertiary)}.digi-expandable-accordion--variation-secondary.sc-digi-expandable-accordion .digi-expandable-accordion__button.sc-digi-expandable-accordion:hover{background-color:var(--digi--color--background--tertiary)}.digi-expandable-accordion--variation-secondary.digi-expandable-accordion--expanded-true.sc-digi-expandable-accordion .digi-expandable-accordion__button.sc-digi-expandable-accordion{color:var(--digi--color--text--inverted);background-color:var(--digi--color--background--inverted-1);border-color:var(--digi--color--background--inverted-1);-webkit-border-after:none;border-block-end:none;border-end-end-radius:0;border-end-start-radius:0}.digi-expandable-accordion__toggle-label.sc-digi-expandable-accordion{display:inline-flex;align-items:center;gap:var(--digi--gutter--icon);font-size:var(--digi--global--typography--font-size--interaction-medium);color:var(--digi--color--text--secondary)}.digi-expandable-accordion--expanded-true.sc-digi-expandable-accordion .digi-expandable-accordion__toggle-label.sc-digi-expandable-accordion{color:currentColor}.digi-expandable-accordion--expanded-true.sc-digi-expandable-accordion .digi-expandable-accordion__toggle-label.sc-digi-expandable-accordion digi-icon.sc-digi-expandable-accordion{--digi--icon--color:currentColor}.digi-expandable-accordion__content.sc-digi-expandable-accordion{overflow-y:hidden}.digi-expandable-accordion--expanded-false.sc-digi-expandable-accordion .digi-expandable-accordion__content.sc-digi-expandable-accordion{height:0}.digi-expandable-accordion--variation-primary.sc-digi-expandable-accordion .digi-expandable-accordion__content.sc-digi-expandable-accordion>div.sc-digi-expandable-accordion{padding:var(--digi--expandable-accordion--content--padding)}.digi-expandable-accordion--variation-primary.digi-expandable-accordion--animation-true.sc-digi-expandable-accordion .digi-expandable-accordion__content.sc-digi-expandable-accordion{transition:var(--digi--expandable-accordion--content--transition)}.digi-expandable-accordion--variation-secondary.sc-digi-expandable-accordion .digi-expandable-accordion__content.sc-digi-expandable-accordion{background:var(--digi--color--background--input-empty)}.digi-expandable-accordion--variation-secondary.sc-digi-expandable-accordion .digi-expandable-accordion__content.sc-digi-expandable-accordion>div.sc-digi-expandable-accordion{display:flex;flex-direction:column}.digi-expandable-accordion--variation-secondary.sc-digi-expandable-accordion .digi-expandable-accordion__content.sc-digi-expandable-accordion digi-typography.sc-digi-expandable-accordion{display:block;padding:var(--digi--container-gutter--small)}.digi-expandable-accordion--variation-secondary.digi-expandable-accordion--expanded-true.sc-digi-expandable-accordion .digi-expandable-accordion__content.sc-digi-expandable-accordion{border:1px solid var(--digi--color--background--inverted-1);border-radius:var(--digi--border-radius--primary);border-start-end-radius:0;border-start-start-radius:0}.digi-expandable-accordion__toggle-inside.sc-digi-expandable-accordion{text-align:center;--digi--button--color--background--function--hover:var(--digi--color--background--tertiary)}@keyframes heightAnim{from{height:0%}to{height:100%}}";

const ExpandableAccordion = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afOnClick = createEvent(this, "afOnClick", 7);
    this.afVariation = ExpandableAccordionVariation.PRIMARY;
    this.afHeading = undefined;
    this.afHeadingLevel = ExpandableAccordionHeaderLevel.H2;
    this.afExpanded = false;
    this.afAnimation = true;
    this.afId = randomIdGenerator('digi-expandable-accordion');
  }
  get cssModifiers() {
    return {
      [`digi-expandable-accordion--expanded-${!!this.afExpanded}`]: true,
      [`digi-expandable-accordion--animation-${!!this.afAnimation}`]: true,
      [`digi-expandable-accordion--variation-${this.afVariation}`]: !!this.afVariation
    };
  }
  /**
   * Hämtar en höjden till innehållet i Utfällbar yta och animera innehållet.
   * @en Gets a height to the content for expandable and animate the content.
   */
  componentDidLoad() {
    const content = this.hostElement.children[0].querySelector(`#${this.afId}-content`);
    const sectionHeight = content.firstElementChild.offsetHeight;
    if (this.afExpanded) {
      content.setAttribute('style', 'height: ' + sectionHeight + 'px');
    }
  }
  animateContentHeight() {
    const content = this.hostElement.children[0].querySelector(`#${this.afId}-content`);
    const sectionHeight = content.firstElementChild.offsetHeight;
    if (this.afExpanded) {
      content.setAttribute('style', 'height: ' + sectionHeight + 'px');
    }
    else {
      content.setAttribute('style', 'height: ' + 0 + 'px');
    }
  }
  clickToggleHandler(e, resetFocus = false) {
    e.preventDefault();
    this.afExpanded = !this.afExpanded;
    if (resetFocus) {
      this._button.focus();
    }
    this.afOnClick.emit(e);
  }
  render() {
    return (h("article", { "aria-expanded": this.afExpanded ? 'true' : 'false', class: Object.assign({ 'digi-expandable-accordion': true }, this.cssModifiers) }, h("header", { class: "digi-expandable-accordion__header" }, h(this.afHeadingLevel, { class: "digi-expandable-accordion__heading" }, h("button", { type: "button", class: "digi-expandable-accordion__button", onClick: (e) => this.clickToggleHandler(e), "aria-pressed": this.afExpanded ? 'true' : 'false', "aria-expanded": this.afExpanded ? 'true' : 'false', "aria-controls": `${this.afId}-content`, ref: (el) => (this._button = el) }, this.afVariation !== ExpandableAccordionVariation.SECONDARY && (h("digi-icon", { afName: `chevron-down`, class: "digi-expandable-accordion__toggle-icon" })), this.afHeading, this.afVariation === ExpandableAccordionVariation.SECONDARY && (h("span", { class: "digi-expandable-accordion__toggle-label" }, this.afExpanded ? 'Dölj' : 'Visa', h("digi-icon", { afName: this.afExpanded ? 'chevron-up' : 'chevron-down', slot: "icon-secondary" })))))), h("section", { class: "digi-expandable-accordion__content", id: `${this.afId}-content` }, h("div", null, h("digi-typography", null, h("slot", null)), this.afVariation === ExpandableAccordionVariation.SECONDARY && (h("digi-button", { class: "digi-expandable-accordion__toggle-inside", afVariation: ButtonVariation.FUNCTION, afType: ButtonType.BUTTON, onAfOnClick: (e) => this.clickToggleHandler(e.detail, true), afFullWidth: true, afAriaPressed: this.afExpanded, afAriaExpanded: this.afExpanded, afAriaControls: `${this.afId}-content` }, this.afExpanded ? 'Dölj' : 'Visa', h("digi-icon", { afName: this.afExpanded ? 'chevron-up' : 'chevron-down', slot: "icon-secondary" })))))));
  }
  get hostElement() { return this; }
  static get watchers() { return {
    "afExpanded": ["animateContentHeight"]
  }; }
  static get style() { return expandableAccordionCss; }
}, [6, "digi-expandable-accordion", {
    "afVariation": [1, "af-variation"],
    "afHeading": [1, "af-heading"],
    "afHeadingLevel": [1, "af-heading-level"],
    "afExpanded": [4, "af-expanded"],
    "afAnimation": [4, "af-animation"],
    "afId": [1, "af-id"]
  }]);
function defineCustomElement$1() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-expandable-accordion", "digi-button", "digi-icon", "digi-typography"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-expandable-accordion":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, ExpandableAccordion);
      }
      break;
    case "digi-button":
      if (!customElements.get(tagName)) {
        defineCustomElement$4();
      }
      break;
    case "digi-icon":
      if (!customElements.get(tagName)) {
        defineCustomElement$3();
      }
      break;
    case "digi-typography":
      if (!customElements.get(tagName)) {
        defineCustomElement$2();
      }
      break;
  } });
}
defineCustomElement$1();

const DigiExpandableAccordion = ExpandableAccordion;
const defineCustomElement = defineCustomElement$1;

export { DigiExpandableAccordion, defineCustomElement };
