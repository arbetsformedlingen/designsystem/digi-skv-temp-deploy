import { U as UtilBreakpointObserver, d as defineCustomElement$1 } from './util-breakpoint-observer.js';

const DigiUtilBreakpointObserver = UtilBreakpointObserver;
const defineCustomElement = defineCustomElement$1;

export { DigiUtilBreakpointObserver, defineCustomElement };
