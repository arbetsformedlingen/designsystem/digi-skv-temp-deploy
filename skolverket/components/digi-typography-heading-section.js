import { T as TypographyHeadingSection, d as defineCustomElement$1 } from './typography-heading-section.js';

const DigiTypographyHeadingSection = TypographyHeadingSection;
const defineCustomElement = defineCustomElement$1;

export { DigiTypographyHeadingSection, defineCustomElement };
