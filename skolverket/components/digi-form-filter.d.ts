import type { Components, JSX } from "../dist/types/components";

interface DigiFormFilter extends Components.DigiFormFilter, HTMLElement {}
export const DigiFormFilter: {
  prototype: DigiFormFilter;
  new (): DigiFormFilter;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
