var TypographyVariation;
(function (TypographyVariation) {
  TypographyVariation["SMALL"] = "small";
  TypographyVariation["LARGE"] = "large";
})(TypographyVariation || (TypographyVariation = {}));

export { TypographyVariation as T };
