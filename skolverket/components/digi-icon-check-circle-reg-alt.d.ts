import type { Components, JSX } from "../dist/types/components";

interface DigiIconCheckCircleRegAlt extends Components.DigiIconCheckCircleRegAlt, HTMLElement {}
export const DigiIconCheckCircleRegAlt: {
  prototype: DigiIconCheckCircleRegAlt;
  new (): DigiIconCheckCircleRegAlt;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
