import type { Components, JSX } from "../dist/types/components";

interface DigiCalendarWeekView extends Components.DigiCalendarWeekView, HTMLElement {}
export const DigiCalendarWeekView: {
  prototype: DigiCalendarWeekView;
  new (): DigiCalendarWeekView;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
