import type { Components, JSX } from "../dist/types/components";

interface DigiLogoService extends Components.DigiLogoService, HTMLElement {}
export const DigiLogoService: {
  prototype: DigiLogoService;
  new (): DigiLogoService;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
