import { proxyCustomElement, HTMLElement, h } from '@stencil/core/internal/client';
import { L as LayoutColumnsElement, a as LayoutColumnsVariation } from './layout-columns-variation.enum.js';

const layoutColumnsCss = ".sc-digi-layout-columns-h{--digi--layout-columns--gap--column:var(--digi--gutter--largest);--digi--layout-columns--gap--row:var(--digi--gutter--largest);--digi--layout-columns--min-width:0}.sc-digi-layout-columns-h .digi-layout-columns.sc-digi-layout-columns{display:grid;grid-column-gap:var(--digi--layout-columns--gap--column);grid-row-gap:var(--digi--layout-columns--gap--row);grid-template-columns:repeat(auto-fit, minmax(var(--digi--layout-columns--min-width), 1fr))}.sc-digi-layout-columns-h .digi-layout-columns--two.sc-digi-layout-columns{--digi--layout-columns--min-width:calc(\n    50% - var(--digi--layout-columns--gap--column)\n  )}.sc-digi-layout-columns-h .digi-layout-columns--three.sc-digi-layout-columns{--digi--layout-columns--min-width:calc(\n    33% - var(--digi--layout-columns--gap--column)\n  )}";

const LayoutColumns = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afElement = LayoutColumnsElement.DIV;
    this.afVariation = undefined;
  }
  get cssModifiers() {
    return {
      'digi-layout-columns--two': this.afVariation === LayoutColumnsVariation.TWO,
      'digi-layout-columns--three': this.afVariation === LayoutColumnsVariation.THREE,
    };
  }
  render() {
    return (h(this.afElement, { class: Object.assign({ 'digi-layout-columns': true }, this.cssModifiers) }, h("slot", null)));
  }
  static get style() { return layoutColumnsCss; }
}, [6, "digi-layout-columns", {
    "afElement": [1, "af-element"],
    "afVariation": [1, "af-variation"]
  }]);
function defineCustomElement$1() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-layout-columns"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-layout-columns":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, LayoutColumns);
      }
      break;
  } });
}
defineCustomElement$1();

const DigiLayoutColumns = LayoutColumns;
const defineCustomElement = defineCustomElement$1;

export { DigiLayoutColumns, defineCustomElement };
