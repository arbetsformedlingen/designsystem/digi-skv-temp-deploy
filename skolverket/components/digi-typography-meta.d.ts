import type { Components, JSX } from "../dist/types/components";

interface DigiTypographyMeta extends Components.DigiTypographyMeta, HTMLElement {}
export const DigiTypographyMeta: {
  prototype: DigiTypographyMeta;
  new (): DigiTypographyMeta;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
