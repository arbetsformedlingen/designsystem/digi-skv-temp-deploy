import { proxyCustomElement, HTMLElement, h } from '@stencil/core/internal/client';
import { L as LayoutGridVerticalSpacing } from './layout-grid-vertical-spacing.enum.js';
import { d as defineCustomElement$1 } from './layout-container.js';

const layoutGridCss = ".sc-digi-layout-grid-h{--digi--layout-grid--columns:4;width:100%}@media (min-width: 48rem){.sc-digi-layout-grid-h{--digi--layout-grid--columns:8}}@media (min-width: 62rem){.sc-digi-layout-grid-h{--digi--layout-grid--columns:12}}.sc-digi-layout-grid-h .digi-layout-grid.sc-digi-layout-grid{display:grid;grid-template-columns:repeat(var(--digi--layout-grid--columns), minmax(0, 1fr));grid-column-gap:var(--digi--responsive-grid-gutter);grid-row-gap:var(--digi--responsive-grid-gutter)}.sc-digi-layout-grid-h .digi-layout-grid--vertical-spacing-none.sc-digi-layout-grid{grid-row-gap:0}";

const LayoutGrid = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afVerticalSpacing = LayoutGridVerticalSpacing.REGULAR;
  }
  get cssModifiers() {
    return {
      [`digi-layout-grid--vertical-spacing-${this.afVerticalSpacing}`]: !!this.afVerticalSpacing
    };
  }
  render() {
    return (h("digi-layout-container", null, h("div", { class: Object.assign({ 'digi-layout-grid': true }, this.cssModifiers) }, h("slot", null))));
  }
  static get style() { return layoutGridCss; }
}, [6, "digi-layout-grid", {
    "afVerticalSpacing": [1, "af-vertical-spacing"]
  }]);
function defineCustomElement() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-layout-grid", "digi-layout-container"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-layout-grid":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, LayoutGrid);
      }
      break;
    case "digi-layout-container":
      if (!customElements.get(tagName)) {
        defineCustomElement$1();
      }
      break;
  } });
}
defineCustomElement();

export { LayoutGrid as L, defineCustomElement as d };
