var FormCheckboxLayout;
(function (FormCheckboxLayout) {
  FormCheckboxLayout["INLINE"] = "inline";
  FormCheckboxLayout["BLOCK"] = "block";
})(FormCheckboxLayout || (FormCheckboxLayout = {}));

var FormCheckboxValidation;
(function (FormCheckboxValidation) {
  FormCheckboxValidation["ERROR"] = "error";
})(FormCheckboxValidation || (FormCheckboxValidation = {}));

var FormCheckboxVariation;
(function (FormCheckboxVariation) {
  FormCheckboxVariation["PRIMARY"] = "primary";
  FormCheckboxVariation["SECONDARY"] = "secondary";
})(FormCheckboxVariation || (FormCheckboxVariation = {}));

export { FormCheckboxLayout as F, FormCheckboxValidation as a, FormCheckboxVariation as b };
