import { proxyCustomElement, HTMLElement, createEvent, h } from '@stencil/core/internal/client';
import { L as LinkExternalVariation } from './link-external-variation.enum.js';
import { d as defineCustomElement$3 } from './icon.js';
import { d as defineCustomElement$2 } from './link.js';

const linkExternalCss = ".sc-digi-link-external-h{--digi--link-external--gap:var(--digi--gutter--medium);--digi--link-external--color--default:var(--digi--color--text--link);--digi--link-external--color--hover:var(--digi--color--text--link-hover);--digi--link-external--color--visited:var(--digi--color--text--link-visited);--digi--link-external--font-size--large:var(--digi--typography--link--font-size--desktop-large);--digi--link-external--font-size--small:var(--digi--typography--link--font-size--desktop);--digi--link-external--font-family:var(--digi--global--typography--font-family--default);--digi--link-external--font-weight:var(--digi--typography--link--font-weight--desktop);--digi--link-external--text-decoration--default:var(--digi--global--typography--text-decoration--default);--digi--link-external--text-decoration--hover:var(--digi--typography--link--text-decoration--desktop);--digi--link-external--text-decoration--icon--default:var(--digi--global--typography--text-decoration--default);--digi--link-external--text-decoration--icon--hover:var(--digi--typography--link--text-decoration--desktop)}.sc-digi-link-external-h .digi-link-external.sc-digi-link-external{--digi--link--gap:var(--digi--link-external--gap);--digi--link--color--default:var(--digi--link-external--color--default);--digi--link--color--hover:var(--digi--link-external--color--hover);--digi--link--color--visited:var(--digi--link-external--color--visited);--digi--link--font-size--large:var(--digi--link-external--font-size--large);--digi--link--font-size--small:var(--digi--link-external--font-size--small);--digi--link--font-family:var(--digi--link-external--font-family);--digi--link--font-weight:var(--digi--link-external--font-weight);--digi--link--text-decoration--default:var(--digi--link-external--text-decoration--default);--digi--link--text-decoration--hover:var(--digi--link-external--text-decoration--hover);--digi--link--text-decoration--icon--default:var(--digi--link-external--text-decoration--icon--default);--digi--link--text-decoration--icon--hover:var(--digi--link-external--text-decoration--icon--hover)}";

const LinkExternal = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afOnClick = createEvent(this, "afOnClick", 7);
    this.afHref = undefined;
    this.afVariation = LinkExternalVariation.SMALL;
    this.afTarget = undefined;
    this.afOverrideLink = false;
  }
  clickLinkHandler(e) {
    e.stopImmediatePropagation();
    this.afOnClick.emit(e.detail);
  }
  initRouting() {
    const tabIndex = this.hostElement.getAttribute('tabIndex');
    if (!!tabIndex) {
      setTimeout(async () => {
        const linkElement = await this.hostElement
          .querySelector('digi-link')
          .afMGetLinkElement();
        tabIndex === '0' && linkElement.setAttribute('tabIndex', '-1');
      }, 0);
    }
  }
  componentDidLoad() {
    this.initRouting();
  }
  get cssModifiers() {
    return {
      [`digi-link-external--variation-${this.afVariation}`]: !!this.afVariation
    };
  }
  render() {
    return (h("digi-link", { class: Object.assign({ 'digi-link-external': true }, this.cssModifiers), afVariation: this.afVariation, afHref: this.afHref, afOverrideLink: this.afOverrideLink, onAfOnClick: (e) => this.clickLinkHandler(e), afTarget: this.afTarget }, h("digi-icon", { class: "digi-link-external__icon", "aria-hidden": "true", slot: "icon", afName: `external-link-alt` }), h("slot", null)));
  }
  get hostElement() { return this; }
  static get style() { return linkExternalCss; }
}, [6, "digi-link-external", {
    "afHref": [1, "af-href"],
    "afVariation": [1, "af-variation"],
    "afTarget": [1, "af-target"],
    "afOverrideLink": [4, "af-override-link"]
  }]);
function defineCustomElement$1() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-link-external", "digi-icon", "digi-link"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-link-external":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, LinkExternal);
      }
      break;
    case "digi-icon":
      if (!customElements.get(tagName)) {
        defineCustomElement$3();
      }
      break;
    case "digi-link":
      if (!customElements.get(tagName)) {
        defineCustomElement$2();
      }
      break;
  } });
}
defineCustomElement$1();

const DigiLinkExternal = LinkExternal;
const defineCustomElement = defineCustomElement$1;

export { DigiLinkExternal, defineCustomElement };
