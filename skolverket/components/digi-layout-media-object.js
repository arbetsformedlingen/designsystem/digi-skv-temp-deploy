import { proxyCustomElement, HTMLElement, h } from '@stencil/core/internal/client';
import { L as LayoutMediaObjectAlignment } from './layout-media-object-alignment.enum.js';

const layoutMediaObjectCss = ".sc-digi-layout-media-object-h{--digi--layout-media-object--gutter:var(--digi--gutter--largest);--digi--layout-media-object--alignment:flex-start;--digi--layout-media-object--flex-wrap:wrap}.sc-digi-layout-media-object-h .digi-layout-media-object.sc-digi-layout-media-object{display:flex;align-items:var(--digi--layout-media-object--alignment);gap:var(--digi--layout-media-object--gutter);flex-wrap:var(--digi--layout-media-object--flex-wrap)}.sc-digi-layout-media-object-h .digi-layout-media-object--alignment-center.sc-digi-layout-media-object{--digi--layout-media-object--alignment:center}.sc-digi-layout-media-object-h .digi-layout-media-object--alignment-end.sc-digi-layout-media-object{--digi--layout-media-object--alignment:flex-end}.sc-digi-layout-media-object-h .digi-layout-media-object--alignment-stretch.sc-digi-layout-media-object{--digi--layout-media-object--alignment:stretch}.digi-layout-media-object__last.sc-digi-layout-media-object{flex:1}";

const LayoutMediaObject = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afAlignment = LayoutMediaObjectAlignment.START;
  }
  get cssModifiers() {
    return {
      [`digi-layout-media-object--alignment-${this.afAlignment}`]: !!this.afAlignment
    };
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-layout-media-object': true }, this.cssModifiers) }, h("div", { class: "digi-layout-media-object__first" }, h("slot", { name: "media" })), h("div", { class: "digi-layout-media-object__last" }, h("slot", null))));
  }
  static get style() { return layoutMediaObjectCss; }
}, [6, "digi-layout-media-object", {
    "afAlignment": [1, "af-alignment"]
  }]);
function defineCustomElement$1() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-layout-media-object"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-layout-media-object":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, LayoutMediaObject);
      }
      break;
  } });
}
defineCustomElement$1();

const DigiLayoutMediaObject = LayoutMediaObject;
const defineCustomElement = defineCustomElement$1;

export { DigiLayoutMediaObject, defineCustomElement };
