import type { Components, JSX } from "../dist/types/components";

interface DigiFormSelect extends Components.DigiFormSelect, HTMLElement {}
export const DigiFormSelect: {
  prototype: DigiFormSelect;
  new (): DigiFormSelect;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
