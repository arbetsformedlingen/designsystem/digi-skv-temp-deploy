import { proxyCustomElement, HTMLElement, createEvent, h, Host } from '@stencil/core/internal/client';
import { r as randomIdGenerator } from './randomIdGenerator.util.js';
import { d as defineCustomElement$2 } from './util-mutation-observer.js';

const formRadiogroupCss = "";

const FormRadiogroup = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afOnGroupChange = createEvent(this, "afOnGroupChange", 7);
    this.radiobuttons = undefined;
    this.afValue = undefined;
    this.value = undefined;
    this.afName = randomIdGenerator('digi-form-radiogroup');
  }
  OnAfValueChange(value) {
    this.value = value;
  }
  OnValueChange(value) {
    this.afValue = value;
    this.setRadiobuttons(value);
  }
  radiobuttonChangeListener(e) {
    if (Array.from(this.radiobuttons).includes(e.target)) {
      e.preventDefault();
      this.afValue = e.detail.target.value;
      this.afOnGroupChange.emit(e);
    }
  }
  componentWillLoad() { }
  componentDidLoad() {
    this.setRadiobuttons(this.value || this.afValue);
    if (this.value)
      this.afValue = this.value;
  }
  componentWillUpdate() { }
  setRadiobuttons(value) {
    this.radiobuttons = this.hostElement.querySelectorAll('digi-form-radiobutton');
    this.radiobuttons.forEach(r => {
      r.afName = this.afName;
      if (value && r.afValue == value) {
        setTimeout(() => r.afMGetFormControlElement().then(e => e.checked || e.click()), 0);
      }
    });
  }
  get cssModifiers() {
    return {};
  }
  render() {
    return (h(Host, null, h("div", { class: Object.assign({ 'digi-form-radiogroup': true }, this.cssModifiers) }, h("digi-util-mutation-observer", { onAfOnChange: (e) => this.radiobuttonChangeListener(e) }, h("slot", null)))));
  }
  get hostElement() { return this; }
  static get watchers() { return {
    "afValue": ["OnAfValueChange"],
    "value": ["OnValueChange"]
  }; }
  static get style() { return formRadiogroupCss; }
}, [6, "digi-form-radiogroup", {
    "afValue": [1025, "af-value"],
    "value": [1025],
    "afName": [1, "af-name"],
    "radiobuttons": [32]
  }]);
function defineCustomElement$1() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-form-radiogroup", "digi-util-mutation-observer"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-form-radiogroup":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, FormRadiogroup);
      }
      break;
    case "digi-util-mutation-observer":
      if (!customElements.get(tagName)) {
        defineCustomElement$2();
      }
      break;
  } });
}
defineCustomElement$1();

const DigiFormRadiogroup = FormRadiogroup;
const defineCustomElement = defineCustomElement$1;

export { DigiFormRadiogroup, defineCustomElement };
