import { I as Icon, d as defineCustomElement$1 } from './icon.js';

const DigiIcon = Icon;
const defineCustomElement = defineCustomElement$1;

export { DigiIcon, defineCustomElement };
