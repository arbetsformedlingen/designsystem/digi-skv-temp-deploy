import { proxyCustomElement, HTMLElement, h } from '@stencil/core/internal/client';

const layoutRowsCss = ".sc-digi-layout-rows-h{display:grid;grid-gap:var(--digi--responsive-grid-gutter);grid-auto-flow:row;width:100%}";

const LayoutRows = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
  }
  render() {
    return h("slot", null);
  }
  static get style() { return layoutRowsCss; }
}, [6, "digi-layout-rows"]);
function defineCustomElement$1() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-layout-rows"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-layout-rows":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, LayoutRows);
      }
      break;
  } });
}
defineCustomElement$1();

const DigiLayoutRows = LayoutRows;
const defineCustomElement = defineCustomElement$1;

export { DigiLayoutRows, defineCustomElement };
