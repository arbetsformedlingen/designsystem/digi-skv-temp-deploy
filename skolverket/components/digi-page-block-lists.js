import { proxyCustomElement, HTMLElement, h } from '@stencil/core/internal/client';
import { a as CardBoxWidth, d as defineCustomElement$5 } from './card-box.js';
import './page-background.enum.js';
import './layout-grid-vertical-spacing.enum.js';
import './layout-stacked-blocks-variation.enum.js';
import './list-link-variation.enum.js';
import './navigation-breadcrumbs-variation.enum.js';
import './notification-alert-variation.enum.js';
import './notification-detail-variation.enum.js';
import './page-footer-variation.enum.js';
import './table-variation.enum.js';
import { d as defineCustomElement$4 } from './layout-container.js';
import { d as defineCustomElement$3 } from './layout-stacked-blocks.js';
import { d as defineCustomElement$2 } from './typography-heading-section.js';

const pageBlockListsCss = ".digi-page-block-lists.sc-digi-page-block-lists{display:block;background:var(--digi--layout-page-block-lists--background, transparent);padding:var(--digi--responsive-grid-gutter) 0}.digi-page-block-lists--variation-start.sc-digi-page-block-lists,.digi-page-block-lists--variation-sub.sc-digi-page-block-lists{--digi--layout-page-block-lists--background:var(--digi--global--color--profile--apricot--opacity50)}.digi-page-block-lists--variation-section.sc-digi-page-block-lists{--digi--layout-page-block-lists--background:var(--digi--color--background--primary)}.digi-page-block-lists__inner.sc-digi-page-block-lists{display:grid;grid-template-columns:1fr 2fr;gap:var(--digi--responsive-grid-gutter)}@media (max-width: 47.9375rem){.digi-page-block-lists__inner.sc-digi-page-block-lists{grid-template-columns:1fr}}.digi-page-block-lists__inner.sc-digi-page-block-lists digi-layout-stacked-blocks.sc-digi-page-block-lists{width:100%}";

const PageBlockLists = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afVariation = undefined;
  }
  get cssModifiers() {
    return {
      [`digi-page-block-lists--variation-${this.afVariation}`]: !!this.afVariation
    };
  }
  render() {
    return (h("digi-layout-container", { class: Object.assign({ 'digi-page-block-lists': true }, this.cssModifiers) }, h("digi-card-box", { afWidth: CardBoxWidth.FULL }, h("div", { class: "digi-page-block-lists__inner" }, h("digi-typography-heading-section", null, h("slot", { name: "heading" })), h("digi-layout-stacked-blocks", null, h("slot", null))))));
  }
  static get assetsDirs() { return ["public"]; }
  static get style() { return pageBlockListsCss; }
}, [6, "digi-page-block-lists", {
    "afVariation": [1, "af-variation"]
  }]);
function defineCustomElement$1() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-page-block-lists", "digi-card-box", "digi-layout-container", "digi-layout-stacked-blocks", "digi-typography-heading-section"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-page-block-lists":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, PageBlockLists);
      }
      break;
    case "digi-card-box":
      if (!customElements.get(tagName)) {
        defineCustomElement$5();
      }
      break;
    case "digi-layout-container":
      if (!customElements.get(tagName)) {
        defineCustomElement$4();
      }
      break;
    case "digi-layout-stacked-blocks":
      if (!customElements.get(tagName)) {
        defineCustomElement$3();
      }
      break;
    case "digi-typography-heading-section":
      if (!customElements.get(tagName)) {
        defineCustomElement$2();
      }
      break;
  } });
}
defineCustomElement$1();

const DigiPageBlockLists = PageBlockLists;
const defineCustomElement = defineCustomElement$1;

export { DigiPageBlockLists, defineCustomElement };
