import { F as FormLabel, d as defineCustomElement$1 } from './form-label.js';

const DigiFormLabel = FormLabel;
const defineCustomElement = defineCustomElement$1;

export { DigiFormLabel, defineCustomElement };
