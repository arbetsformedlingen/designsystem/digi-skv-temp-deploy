import type { Components, JSX } from "../dist/types/components";

interface DigiUtilMutationObserver extends Components.DigiUtilMutationObserver, HTMLElement {}
export const DigiUtilMutationObserver: {
  prototype: DigiUtilMutationObserver;
  new (): DigiUtilMutationObserver;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
