import type { Components, JSX } from "../dist/types/components";

interface DigiUtilBreakpointObserver extends Components.DigiUtilBreakpointObserver, HTMLElement {}
export const DigiUtilBreakpointObserver: {
  prototype: DigiUtilBreakpointObserver;
  new (): DigiUtilBreakpointObserver;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
