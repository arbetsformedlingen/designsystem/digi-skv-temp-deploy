import { N as NotificationAlert, d as defineCustomElement$1 } from './notification-alert.js';

const DigiNotificationAlert = NotificationAlert;
const defineCustomElement = defineCustomElement$1;

export { DigiNotificationAlert, defineCustomElement };
