import type { Components, JSX } from "../dist/types/components";

interface DigiIconDangerOutline extends Components.DigiIconDangerOutline, HTMLElement {}
export const DigiIconDangerOutline: {
  prototype: DigiIconDangerOutline;
  new (): DigiIconDangerOutline;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
