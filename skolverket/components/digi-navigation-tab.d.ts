import type { Components, JSX } from "../dist/types/components";

interface DigiNavigationTab extends Components.DigiNavigationTab, HTMLElement {}
export const DigiNavigationTab: {
  prototype: DigiNavigationTab;
  new (): DigiNavigationTab;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
