import type { Components, JSX } from "../dist/types/components";

interface DigiFormProcessSteps extends Components.DigiFormProcessSteps, HTMLElement {}
export const DigiFormProcessSteps: {
  prototype: DigiFormProcessSteps;
  new (): DigiFormProcessSteps;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
