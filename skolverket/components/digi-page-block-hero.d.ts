import type { Components, JSX } from "../dist/types/components";

interface DigiPageBlockHero extends Components.DigiPageBlockHero, HTMLElement {}
export const DigiPageBlockHero: {
  prototype: DigiPageBlockHero;
  new (): DigiPageBlockHero;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
