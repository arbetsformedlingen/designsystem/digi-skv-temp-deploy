import { C as CodeBlock, d as defineCustomElement$1 } from './code-block.js';

const DigiCodeBlock = CodeBlock;
const defineCustomElement = defineCustomElement$1;

export { DigiCodeBlock, defineCustomElement };
