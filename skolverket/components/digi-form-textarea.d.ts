import type { Components, JSX } from "../dist/types/components";

interface DigiFormTextarea extends Components.DigiFormTextarea, HTMLElement {}
export const DigiFormTextarea: {
  prototype: DigiFormTextarea;
  new (): DigiFormTextarea;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
