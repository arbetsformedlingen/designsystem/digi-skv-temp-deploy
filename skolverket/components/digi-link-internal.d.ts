import type { Components, JSX } from "../dist/types/components";

interface DigiLinkInternal extends Components.DigiLinkInternal, HTMLElement {}
export const DigiLinkInternal: {
  prototype: DigiLinkInternal;
  new (): DigiLinkInternal;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
