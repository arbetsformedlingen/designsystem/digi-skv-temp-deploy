import { B as Button, d as defineCustomElement$1 } from './button.js';

const DigiButton = Button;
const defineCustomElement = defineCustomElement$1;

export { DigiButton, defineCustomElement };
