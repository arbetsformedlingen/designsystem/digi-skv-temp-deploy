import type { Components, JSX } from "../dist/types/components";

interface DigiLayoutMediaObject extends Components.DigiLayoutMediaObject, HTMLElement {}
export const DigiLayoutMediaObject: {
  prototype: DigiLayoutMediaObject;
  new (): DigiLayoutMediaObject;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
