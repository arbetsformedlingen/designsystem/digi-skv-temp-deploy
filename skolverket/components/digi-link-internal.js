import { proxyCustomElement, HTMLElement, createEvent, h } from '@stencil/core/internal/client';
import { L as LinkInternalVariation } from './link-internal-variation.enum.js';
import { d as defineCustomElement$3 } from './icon.js';
import { d as defineCustomElement$2 } from './link.js';

const linkInternalCss = ".sc-digi-link-internal-h{--digi--link-internal--gap:var(--digi--gutter--medium);--digi--link-internal--color--default:var(--digi--color--text--link);--digi--link-internal--color--hover:var(--digi--color--text--link-hover);--digi--link-internal--color--visited:var(--digi--color--text--link-visited);--digi--link-internal--font-size--large:var(--digi--typography--link--font-size--desktop-large);--digi--link-internal--font-size--small:var(--digi--typography--link--font-size--desktop);--digi--link-internal--font-family:var(--digi--global--typography--font-family--default);--digi--link-internal--font-weight:var(--digi--typography--link--font-weight--desktop);--digi--link-internal--text-decoration--default:var(--digi--global--typography--text-decoration--default);--digi--link-internal--text-decoration--hover:var(--digi--typography--link--text-decoration--desktop);--digi--link-internal--text-decoration--icon--default:var(--digi--global--typography--text-decoration--default);--digi--link-internal--text-decoration--icon--hover:var(--digi--typography--link--text-decoration--desktop)}.sc-digi-link-internal-h .digi-link-internal.sc-digi-link-internal{--digi--link--gap:var(--digi--link-internal--gap);--digi--link--color--default:var(--digi--link-internal--color--default);--digi--link--color--hover:var(--digi--link-internal--color--hover);--digi--link--color--visited:var(--digi--link-internal--color--visited);--digi--link--font-size--large:var(--digi--link-internal--font-size--large);--digi--link--font-size--small:var(--digi--link-internal--font-size--small);--digi--link--font-family:var(--digi--link-internal--font-family);--digi--link--font-weight:var(--digi--link-internal--font-weight);--digi--link--text-decoration--default:var(--digi--link-internal--text-decoration--default);--digi--link--text-decoration--hover:var(--digi--link-internal--text-decoration--hover);--digi--link--text-decoration--icon--default:var(--digi--link-internal--text-decoration--icon--default);--digi--link--text-decoration--icon--hover:var(--digi--link-internal--text-decoration--icon--hover)}";

const LinkInternal = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afOnClick = createEvent(this, "afOnClick", 7);
    this.afHref = undefined;
    this.afVariation = LinkInternalVariation.SMALL;
    this.afOverrideLink = false;
  }
  clickLinkHandler(e) {
    e.stopImmediatePropagation();
    this.afOnClick.emit(e.detail);
  }
  initRouting() {
    const tabIndex = this.hostElement.getAttribute('tabIndex');
    if (!!tabIndex) {
      setTimeout(async () => {
        const linkElement = await this.hostElement
          .querySelector('digi-link')
          .afMGetLinkElement();
        tabIndex === '0' && linkElement.setAttribute('tabIndex', '-1');
      }, 0);
    }
  }
  componentDidLoad() {
    this.initRouting();
  }
  get cssModifiers() {
    return {
      [`digi-link-internal--variation-${this.afVariation}`]: !!this.afVariation
    };
  }
  render() {
    return (h("digi-link", { class: Object.assign({ 'digi-link-internal': true }, this.cssModifiers), afVariation: this.afVariation, afHref: this.afHref, afOverrideLink: this.afOverrideLink, onAfOnClick: (e) => this.clickLinkHandler(e) }, h("digi-icon", { class: "digi-link-internal__icon", "aria-hidden": "true", slot: "icon", afName: `chevron-right` }), h("slot", null)));
  }
  get hostElement() { return this; }
  static get style() { return linkInternalCss; }
}, [6, "digi-link-internal", {
    "afHref": [1, "af-href"],
    "afVariation": [1, "af-variation"],
    "afOverrideLink": [4, "af-override-link"]
  }]);
function defineCustomElement$1() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-link-internal", "digi-icon", "digi-link"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-link-internal":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, LinkInternal);
      }
      break;
    case "digi-icon":
      if (!customElements.get(tagName)) {
        defineCustomElement$3();
      }
      break;
    case "digi-link":
      if (!customElements.get(tagName)) {
        defineCustomElement$2();
      }
      break;
  } });
}
defineCustomElement$1();

const DigiLinkInternal = LinkInternal;
const defineCustomElement = defineCustomElement$1;

export { DigiLinkInternal, defineCustomElement };
