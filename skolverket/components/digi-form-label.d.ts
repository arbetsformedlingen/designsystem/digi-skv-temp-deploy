import type { Components, JSX } from "../dist/types/components";

interface DigiFormLabel extends Components.DigiFormLabel, HTMLElement {}
export const DigiFormLabel: {
  prototype: DigiFormLabel;
  new (): DigiFormLabel;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
