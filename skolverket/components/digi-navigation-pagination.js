import { proxyCustomElement, HTMLElement, createEvent, h } from '@stencil/core/internal/client';
import { r as randomIdGenerator } from './randomIdGenerator.util.js';
import { b as ButtonVariation, B as ButtonSize } from './button-variation.enum.js';
import './expandable-accordion-variation.enum.js';
import './calendar-week-view-heading-level.enum.js';
import './code-block-variation.enum.js';
import './code-example-variation.enum.js';
import './code-variation.enum.js';
import './form-checkbox-variation.enum.js';
import './form-file-upload-variation.enum.js';
import './form-input-search-variation.enum.js';
import './form-input-variation.enum.js';
import './form-radiobutton-variation.enum.js';
import './form-select-variation.enum.js';
import './form-textarea-variation.enum.js';
import './form-validation-message-variation.enum.js';
import './layout-block-variation.enum.js';
import './layout-columns-variation.enum.js';
import './layout-container-variation.enum.js';
import './layout-media-object-alignment.enum.js';
import './link-external-variation.enum.js';
import './link-internal-variation.enum.js';
import './link-variation.enum.js';
import './loader-spinner-size.enum.js';
import './media-figure-alignment.enum.js';
import './navigation-context-menu-item-type.enum.js';
import './navigation-sidebar-variation.enum.js';
import './navigation-vertical-menu-variation.enum.js';
import './progress-step-variation.enum.js';
import './progress-steps-variation.enum.js';
import './progressbar-variation.enum.js';
import './tag-size.enum.js';
import './typography-meta-variation.enum.js';
import './typography-time-variation.enum.js';
import './typography-variation.enum.js';
import './util-breakpoint-observer-breakpoints.enum.js';
import { d as defineCustomElement$8 } from './button.js';
import { d as defineCustomElement$7 } from './form-label.js';
import { d as defineCustomElement$6 } from './form-select.js';
import { d as defineCustomElement$5 } from './form-validation-message.js';
import { d as defineCustomElement$4 } from './icon.js';
import { d as defineCustomElement$3 } from './typography.js';
import { d as defineCustomElement$2 } from './util-mutation-observer.js';

const navigationPaginationCss = ".digi-navigation-pagination.sc-digi-navigation-pagination{padding:var(--digi--gutter--largest);display:grid;grid-template-rows:min-content auto;grid-template-areas:\"result\" \"pagination\"}@media (min-width: 48rem){.digi-navigation-pagination.sc-digi-navigation-pagination{gap:var(--digi--gutter--medium)}}.digi-navigation-pagination__result.sc-digi-navigation-pagination{grid-area:result}@media (max-width: 47.9375rem){.digi-navigation-pagination__result.sc-digi-navigation-pagination{display:none}}.digi-navigation-pagination__pagination.sc-digi-navigation-pagination{grid-area:pagination;display:grid;grid-template-columns:1fr;grid-template-areas:\"select\" \"buttons\";gap:var(--digi--gutter--medium);justify-content:space-between;align-items:end}@media (min-width: 48rem){.digi-navigation-pagination__pagination.sc-digi-navigation-pagination{grid-template-columns:auto auto;grid-template-areas:\"select buttons\"}}.digi-navigation-pagination__buttons.sc-digi-navigation-pagination{grid-area:buttons;display:grid;grid-template-columns:repeat(2, 1fr);gap:var(--digi--gutter--icon);align-items:center}@media (min-width: 48rem){.digi-navigation-pagination__buttons.sc-digi-navigation-pagination{grid-template-columns:repeat(2, auto)}}.digi-navigation-pagination__button.sc-digi-navigation-pagination{grid-column:1}.digi-navigation-pagination__button--hidden.sc-digi-navigation-pagination{display:none}.digi-navigation-pagination__button.sc-digi-navigation-pagination:nth-child(2){grid-column:2}.digi-navigation-pagination__select.sc-digi-navigation-pagination{min-width:150px}.digi-navigation-pagination__aria-label.sc-digi-navigation-pagination{clip:rect(0 0 0 0);height:1px;padding:0;white-space:nowrap;width:1px;overflow:hidden;position:absolute}";

const NavigationPagination = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afOnPageChange = createEvent(this, "afOnPageChange", 7);
    this.pages = [];
    this.currentPage = undefined;
    this.afInitActivePage = 1;
    this.afTotalPages = undefined;
    this.afCurrentResultStart = undefined;
    this.afId = randomIdGenerator('digi-navigation-pagination');
    this.afCurrentResultEnd = undefined;
    this.afTotalResults = 0;
    this.afResultName = 'träffar';
  }
  afInitActivePageChanged() {
    this.setCurrentPage(this.afInitActivePage, false);
  }
  /**
   * Kan användas för att manuellt sätta om den aktiva sidan.
   * @en Can be used to set the active page.
   */
  async afMSetCurrentPage(pageNumber) {
    this.setCurrentPage(pageNumber, false);
  }
  prevPage() {
    this.currentPage--;
    this.afOnPageChange.emit(this.currentPage);
  }
  nextPage() {
    this.currentPage++;
    this.afOnPageChange.emit(this.currentPage);
  }
  setCurrentPage(pageToSet = this.currentPage, emitEvent = true) {
    if (pageToSet === this.currentPage) {
      return;
    }
    this.currentPage =
      pageToSet <= 1
        ? 1
        : pageToSet <= this.afTotalPages
          ? pageToSet
          : this.afTotalPages;
    console.log(pageToSet, this.currentPage);
    if (emitEvent) {
      this.afOnPageChange.emit(this.currentPage);
    }
  }
  componentWillLoad() {
    this.setCurrentPage(this.afInitActivePage, false);
    this.updateTotalPages();
  }
  updateTotalPages() {
    if (this.afTotalPages) {
      this.pages = [...Array(this.afTotalPages)];
    }
  }
  isCurrentPage(currentPage, page) {
    if (currentPage === page || null) {
      return 'page';
    }
  }
  labelledby() {
    if (this.afTotalResults > 0) {
      return `
        ${this.afId}-aria-label
        ${this.afId}-result-show
        ${this.afId}-result-current
        ${this.afId}-result-of
        ${this.afId}-result-total
        ${this.afId}-result-name
      `;
    }
    else {
      return `${this.afId}-aria-label`;
    }
  }
  render() {
    return (h("div", { class: "digi-navigation-pagination" }, h("span", { id: `${this.afId}-aria-label`, "aria-hidden": "true", class: "digi-navigation-pagination__aria-label" }, "Paginering"), this.afTotalResults > 0 && (h("div", { class: {
        'digi-navigation-pagination__result': true,
        'digi-navigation-pagination__result--pages': this.pages.length > 1
      }, "aria-hidden": this.pages.length > 1 ? 'true' : 'false', id: `${this.afId}-result` }, h("digi-typography", null, h("span", { id: `${this.afId}-result-show` }, "Visar "), h("strong", { id: `${this.afId}-result-current` }, this.afCurrentResultStart, "-", this.afCurrentResultEnd), h("span", { id: `${this.afId}-result-of` }, " av "), h("strong", { id: `${this.afId}-result-total` }, this.afTotalResults), this.afResultName && (h("span", { id: `${this.afId}-result-name` }, ` ${this.afResultName}`))))), this.pages.length > 1 && (h("nav", { "aria-labelledby": this.labelledby(), class: {
        'digi-navigation-pagination__pagination': true
      } }, h("div", { class: "digi-navigation-pagination__select" }, h("digi-form-select", { afLabel: `Sida`, afValue: `${this.currentPage}`, onAfOnChange: (e) => {
        this.setCurrentPage(parseInt(e.detail.target.value), true);
      } }, this.pages.map((_, index) => (h("option", { value: `${index + 1}`, selected: index + 1 === this.currentPage }, index + 1, " av ", this.afTotalPages))))), h("div", { class: "digi-navigation-pagination__buttons" }, h("digi-button", { onClick: () => this.prevPage(), afVariation: ButtonVariation.SECONDARY, afSize: ButtonSize.LARGE, afFullWidth: true, class: {
        'digi-navigation-pagination__button digi-navigation-pagination__button--previous': true,
        'digi-navigation-pagination__button--keep-left': this.currentPage === this.pages.length,
        'digi-navigation-pagination__button--hidden': this.currentPage === 1
      } }, h("digi-icon", { slot: "icon", afName: `chevron-left` }), h("span", null, "F\u00F6reg\u00E5ende")), h("digi-button", { onClick: () => this.nextPage(), afVariation: ButtonVariation.SECONDARY, afSize: ButtonSize.LARGE, afFullWidth: true, class: {
        'digi-navigation-pagination__button digi-navigation-pagination__button--next': true,
        'digi-navigation-pagination__button--keep-right': this.currentPage === 1,
        'digi-navigation-pagination__button--hidden': this.currentPage === this.afTotalPages
      }, "af-variation": "secondary" }, h("digi-icon", { slot: "icon-secondary", afName: `chevron-right` }), h("span", null, "N\u00E4sta")))))));
  }
  static get watchers() { return {
    "afInitActivePage": ["afInitActivePageChanged"],
    "afTotalPages": ["updateTotalPages"]
  }; }
  static get style() { return navigationPaginationCss; }
}, [2, "digi-navigation-pagination", {
    "afInitActivePage": [2, "af-init-active-page"],
    "afTotalPages": [2, "af-total-pages"],
    "afCurrentResultStart": [2, "af-current-result-start"],
    "afId": [1, "af-id"],
    "afCurrentResultEnd": [2, "af-current-result-end"],
    "afTotalResults": [2, "af-total-results"],
    "afResultName": [1, "af-result-name"],
    "pages": [32],
    "currentPage": [32],
    "afMSetCurrentPage": [64]
  }]);
function defineCustomElement$1() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-navigation-pagination", "digi-button", "digi-form-label", "digi-form-select", "digi-form-validation-message", "digi-icon", "digi-typography", "digi-util-mutation-observer"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-navigation-pagination":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, NavigationPagination);
      }
      break;
    case "digi-button":
      if (!customElements.get(tagName)) {
        defineCustomElement$8();
      }
      break;
    case "digi-form-label":
      if (!customElements.get(tagName)) {
        defineCustomElement$7();
      }
      break;
    case "digi-form-select":
      if (!customElements.get(tagName)) {
        defineCustomElement$6();
      }
      break;
    case "digi-form-validation-message":
      if (!customElements.get(tagName)) {
        defineCustomElement$5();
      }
      break;
    case "digi-icon":
      if (!customElements.get(tagName)) {
        defineCustomElement$4();
      }
      break;
    case "digi-typography":
      if (!customElements.get(tagName)) {
        defineCustomElement$3();
      }
      break;
    case "digi-util-mutation-observer":
      if (!customElements.get(tagName)) {
        defineCustomElement$2();
      }
      break;
  } });
}
defineCustomElement$1();

const DigiNavigationPagination = NavigationPagination;
const defineCustomElement = defineCustomElement$1;

export { DigiNavigationPagination, defineCustomElement };
