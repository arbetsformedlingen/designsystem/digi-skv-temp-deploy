import type { Components, JSX } from "../dist/types/components";

interface DigiNavigationBreadcrumbs extends Components.DigiNavigationBreadcrumbs, HTMLElement {}
export const DigiNavigationBreadcrumbs: {
  prototype: DigiNavigationBreadcrumbs;
  new (): DigiNavigationBreadcrumbs;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
