import type { Components, JSX } from "../dist/types/components";

interface DigiLayoutContainer extends Components.DigiLayoutContainer, HTMLElement {}
export const DigiLayoutContainer: {
  prototype: DigiLayoutContainer;
  new (): DigiLayoutContainer;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
