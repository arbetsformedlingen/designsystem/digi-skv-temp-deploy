import type { Components, JSX } from "../dist/types/components";

interface DigiLayoutStackedBlocks extends Components.DigiLayoutStackedBlocks, HTMLElement {}
export const DigiLayoutStackedBlocks: {
  prototype: DigiLayoutStackedBlocks;
  new (): DigiLayoutStackedBlocks;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
