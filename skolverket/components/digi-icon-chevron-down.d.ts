import type { Components, JSX } from "../dist/types/components";

interface DigiIconChevronDown extends Components.DigiIconChevronDown, HTMLElement {}
export const DigiIconChevronDown: {
  prototype: DigiIconChevronDown;
  new (): DigiIconChevronDown;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
