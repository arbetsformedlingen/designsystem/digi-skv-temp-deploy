import type { Components, JSX } from "../dist/types/components";

interface DigiFormRadiogroup extends Components.DigiFormRadiogroup, HTMLElement {}
export const DigiFormRadiogroup: {
  prototype: DigiFormRadiogroup;
  new (): DigiFormRadiogroup;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
