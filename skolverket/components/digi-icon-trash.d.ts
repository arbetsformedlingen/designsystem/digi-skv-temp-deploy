import type { Components, JSX } from "../dist/types/components";

interface DigiIconTrash extends Components.DigiIconTrash, HTMLElement {}
export const DigiIconTrash: {
  prototype: DigiIconTrash;
  new (): DigiIconTrash;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
