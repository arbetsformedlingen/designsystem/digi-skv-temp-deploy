import { proxyCustomElement, HTMLElement, createEvent, h } from '@stencil/core/internal/client';
import { N as NotificationAlertVariation } from './notification-alert-variation.enum.js';
import { b as ButtonVariation } from './button-variation.enum.js';
import './expandable-accordion-variation.enum.js';
import './calendar-week-view-heading-level.enum.js';
import './code-block-variation.enum.js';
import './code-example-variation.enum.js';
import './code-variation.enum.js';
import './form-checkbox-variation.enum.js';
import './form-file-upload-variation.enum.js';
import './form-input-search-variation.enum.js';
import './form-input-variation.enum.js';
import './form-radiobutton-variation.enum.js';
import './form-select-variation.enum.js';
import './form-textarea-variation.enum.js';
import './form-validation-message-variation.enum.js';
import './layout-block-variation.enum.js';
import './layout-columns-variation.enum.js';
import './layout-container-variation.enum.js';
import './layout-media-object-alignment.enum.js';
import './link-external-variation.enum.js';
import './link-internal-variation.enum.js';
import './link-variation.enum.js';
import './loader-spinner-size.enum.js';
import './media-figure-alignment.enum.js';
import './navigation-context-menu-item-type.enum.js';
import './navigation-sidebar-variation.enum.js';
import './navigation-vertical-menu-variation.enum.js';
import './progress-step-variation.enum.js';
import './progress-steps-variation.enum.js';
import './progressbar-variation.enum.js';
import './tag-size.enum.js';
import './typography-meta-variation.enum.js';
import './typography-time-variation.enum.js';
import { T as TypographyVariation } from './typography-variation.enum.js';
import './util-breakpoint-observer-breakpoints.enum.js';
import { d as defineCustomElement$3 } from './button.js';
import { d as defineCustomElement$2 } from './icon.js';
import { d as defineCustomElement$1 } from './typography.js';

const notificationAlertCss = ".sc-digi-notification-alert-h{--digi--notification-alert--border-color--info:var(--digi--color--border--informative);--digi--notification-alert--border-color--warning:var(--digi--color--border--warning);--digi--notification-alert--border-color--danger:var(--digi--color--border--danger);--digi--notification-alert--background-color--info:var(--digi--color--background--notification-info);--digi--notification-alert--background-color--warning:var(--digi--color--background--notification-warning);--digi--notification-alert--background-color--danger:var(--digi--color--background--notification-danger)}.sc-digi-notification-alert-h .sc-digi-notification-alert-s>[slot^=heading]{--digi--heading-4--font-size:var(--digi--typography--heading-4--font-size--mobile);--digi--heading-4--line-height:var(--digi--typography--heading-4--line-height--mobile);font-family:var(--digi--global--typography--font-family--default);font-size:var(--digi--heading-4--font-size);line-height:var(--digi--heading-4--line-height);font-weight:var(--digi--typography--heading-4--font-weight--mobile);-webkit-margin-after:0;margin-block-end:0}@media (min-width: 48rem){.sc-digi-notification-alert-h .sc-digi-notification-alert-s>[slot^=heading]{--digi--heading-4--font-size:var(--digi--typography--heading-4--font-size--desktop);--digi--heading-4--line-height:var(--digi--typography--heading-4--line-height--desktop)}}@media (min-width: 62rem){.sc-digi-notification-alert-h .sc-digi-notification-alert-s>[slot^=heading]{--digi--heading-4--font-size:var(--digi--typography--heading-4--font-size--desktop-large);--digi--heading-4--line-height:var(--digi--typography--heading-4--line-height--desktop-large)}}.sc-digi-notification-alert-h .sc-digi-notification-alert-s>a[slot^=link]{font-family:var(--digi--global--typography--font-family--default);text-decoration:none;color:var(--digi--color--text--secondary);font-size:var(--digi--global--typography--font-size--large);letter-spacing:calc(var(--digi--global--typography--font-size--large) / 100 * -1);font-weight:var(--digi--global--typography--font-weight--semibold);display:block;border-radius:var(--digi--border-radius--primary);padding:calc(var(--digi--grid-gutter--smaller) * 1.5) var(--digi--grid-gutter--smaller);-webkit-padding-start:0;padding-inline-start:0;-webkit-padding-after:0;padding-block-end:0}.sc-digi-notification-alert-h .sc-digi-notification-alert-s>a[slot^=link]:hover{color:var(--digi--global--color--profile--purple--dark);text-decoration:underline}.sc-digi-notification-alert-h .sc-digi-notification-alert-s>a[slot^=link]:focus{outline:none;color:var(--digi--color--text--secondary)}.sc-digi-notification-alert-h .sc-digi-notification-alert-s>a[slot^=link]:focus-visible{outline:var(--digi--border-width--secondary) solid var(--digi--color--border--focus)}.sc-digi-notification-alert-h .sc-digi-notification-alert-s>a[slot^=link]:visited{color:var(--digi--color--text--secondary)}.sc-digi-notification-alert-h .sc-digi-notification-alert-s>a[slot^=link]::after{background-color:currentColor;padding:0 0.5em;-webkit-mask-image:url(\"data:image/svg+xml;charset=utf-8, %3Csvg%0A%09width%3D%2224%22%0A%09height%3D%2224%22%0A%09viewBox%3D%220%200%2024%2024%22%0A%09xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%0A%3E%0A%09%3Cpath%0A%09%09d%3D%22M288.917%2C415.517%2C287.5%2C414.1l3.96-3.96-3.96-3.96%2C1.414-1.414%2C5.373%2C5.374Z%22%0A%09%09transform%3D%22translate(-278.17%20-398.103)%22%0A%09%2F%3E%0A%3C%2Fsvg%3E\");mask-image:url(\"data:image/svg+xml;charset=utf-8, %3Csvg%0A%09width%3D%2224%22%0A%09height%3D%2224%22%0A%09viewBox%3D%220%200%2024%2024%22%0A%09xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%0A%3E%0A%09%3Cpath%0A%09%09d%3D%22M288.917%2C415.517%2C287.5%2C414.1l3.96-3.96-3.96-3.96%2C1.414-1.414%2C5.373%2C5.374Z%22%0A%09%09transform%3D%22translate(-278.17%20-398.103)%22%0A%09%2F%3E%0A%3C%2Fsvg%3E\");-webkit-mask-repeat:no-repeat;mask-repeat:no-repeat;-webkit-mask-position:center;mask-position:center;-webkit-clip-path:padding-box inset(0.25em 0);clip-path:padding-box inset(0.25em 0);content:\"\"}.sc-digi-notification-alert-h .sc-digi-notification-alert-s>[slot^=link]{display:block}.sc-digi-notification-alert-h .sc-digi-notification-alert-s>[slot^=link] a{font-family:var(--digi--global--typography--font-family--default);text-decoration:none;color:var(--digi--color--text--secondary);font-size:var(--digi--global--typography--font-size--large);letter-spacing:calc(var(--digi--global--typography--font-size--large) / 100 * -1);font-weight:var(--digi--global--typography--font-weight--semibold);display:block;border-radius:var(--digi--border-radius--primary);padding:calc(var(--digi--grid-gutter--smaller) * 1.5) var(--digi--grid-gutter--smaller);-webkit-padding-start:0;padding-inline-start:0;-webkit-padding-after:0;padding-block-end:0}.sc-digi-notification-alert-h .sc-digi-notification-alert-s>[slot^=link] a:hover{color:var(--digi--global--color--profile--purple--dark);text-decoration:underline}.sc-digi-notification-alert-h .sc-digi-notification-alert-s>[slot^=link] a:focus{outline:none;color:var(--digi--color--text--secondary)}.sc-digi-notification-alert-h .sc-digi-notification-alert-s>[slot^=link] a:focus-visible{outline:var(--digi--border-width--secondary) solid var(--digi--color--border--focus)}.sc-digi-notification-alert-h .sc-digi-notification-alert-s>[slot^=link] a:visited{color:var(--digi--color--text--secondary)}.sc-digi-notification-alert-h .sc-digi-notification-alert-s>[slot^=link] a::after{background-color:currentColor;padding:0 0.5em;-webkit-mask-image:url(\"data:image/svg+xml;charset=utf-8, %3Csvg%0A%09width%3D%2224%22%0A%09height%3D%2224%22%0A%09viewBox%3D%220%200%2024%2024%22%0A%09xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%0A%3E%0A%09%3Cpath%0A%09%09d%3D%22M288.917%2C415.517%2C287.5%2C414.1l3.96-3.96-3.96-3.96%2C1.414-1.414%2C5.373%2C5.374Z%22%0A%09%09transform%3D%22translate(-278.17%20-398.103)%22%0A%09%2F%3E%0A%3C%2Fsvg%3E\");mask-image:url(\"data:image/svg+xml;charset=utf-8, %3Csvg%0A%09width%3D%2224%22%0A%09height%3D%2224%22%0A%09viewBox%3D%220%200%2024%2024%22%0A%09xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%0A%3E%0A%09%3Cpath%0A%09%09d%3D%22M288.917%2C415.517%2C287.5%2C414.1l3.96-3.96-3.96-3.96%2C1.414-1.414%2C5.373%2C5.374Z%22%0A%09%09transform%3D%22translate(-278.17%20-398.103)%22%0A%09%2F%3E%0A%3C%2Fsvg%3E\");-webkit-mask-repeat:no-repeat;mask-repeat:no-repeat;-webkit-mask-position:center;mask-position:center;-webkit-clip-path:padding-box inset(0.25em 0);clip-path:padding-box inset(0.25em 0);content:\"\"}.digi-notification-alert.sc-digi-notification-alert{border:var(--digi--border-width--tertiary) var(--digi--border-style--primary) var(--digi--global--color--profile--purple--opacity15);border-radius:var(--digi--border-radius--primary);-webkit-border-before:var(--digi--border-width--secondary) solid var(--digi--color--border--secondary);border-block-start:var(--digi--border-width--secondary) solid var(--digi--color--border--secondary);border-block-start-color:var(--BORDER-COLOR);background:var(--BACKGROUND-COLOR)}.digi-notification-alert--variation-info.sc-digi-notification-alert{--BORDER-COLOR:var(--digi--notification-alert--border-color--info);--ICON-COLOR:var(--digi--notification-alert--border-color--info);--BACKGROUND-COLOR:var(--digi--notification-alert--background-color--info)}.digi-notification-alert--variation-danger.sc-digi-notification-alert{--BORDER-COLOR:var(--digi--notification-alert--border-color--danger);--ICON-COLOR:var(--digi--notification-alert--border-color--danger);--BACKGROUND-COLOR:var(--digi--notification-alert--background-color--danger)}.digi-notification-alert--variation-warning.sc-digi-notification-alert{--BORDER-COLOR:var(--digi--notification-alert--border-color--warning);--ICON-COLOR:var(--digi--notification-alert--border-color--warning);--BACKGROUND-COLOR:var(--digi--notification-alert--background-color--warning)}.digi-notification-alert__inner.sc-digi-notification-alert{display:grid;grid-template-columns:min-content min-content;grid-template-rows:min-content auto min-content;grid-template-areas:\"icon close\" \"content content\" \"actions actions\";gap:calc(var(--digi--gutter--icon) * 2);max-width:var(--digi--container-width--largest);margin:0 auto;padding:var(--digi--gutter--medium) var(--digi--gutter--larger)}@media (max-width: 47.9375rem){.digi-notification-alert__inner.sc-digi-notification-alert{justify-content:space-between;align-items:center}}@media (min-width: 48rem){.digi-notification-alert__inner.sc-digi-notification-alert{grid-template-columns:min-content 1fr auto;grid-template-rows:min-content auto;grid-template-areas:\"icon content actions\" \"icon content actions\"}.digi-notification-alert--closeable.sc-digi-notification-alert .digi-notification-alert__inner.sc-digi-notification-alert{grid-template-areas:\"icon content close\" \"icon content actions\"}}.digi-notification-alert__icon.sc-digi-notification-alert{grid-area:icon}.digi-notification-alert__icon.sc-digi-notification-alert digi-icon.sc-digi-notification-alert{--digi--icon--color:var(--ICON-COLOR);--digi-icon--fixed-width:32px;--digi-icon--fixed-height:32px}.digi-notification-alert__content.sc-digi-notification-alert{grid-area:content}.digi-notification-alert__close-button.sc-digi-notification-alert{grid-area:close;justify-self:end}.digi-notification-alert__actions.sc-digi-notification-alert{grid-area:actions}.digi-notification-alert__text.sc-digi-notification-alert{--digi--typography--body--font-size--desktop:var(--digi--global--typography--font-size--smaller);--digi--typography--body--font-size--mobile:var(--digi--global--typography--font-size--smaller);--digi--typography--body--line-height--desktop:var(--digi--global--typography--line-height--smaller);--digi--typography--body--line-height--mobile:var(--digi--global--typography--line-height--smaller)}";

const NotificationAlert = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afOnClose = createEvent(this, "afOnClose", 7);
    this.afVariation = NotificationAlertVariation.INFO;
    this.afCloseable = false;
  }
  clickHandler(e) {
    this.afOnClose.emit(e);
  }
  get cssModifiers() {
    return {
      [`digi-notification-alert--variation-${this.afVariation}`]: !!this.afVariation,
      'digi-notification-alert--closeable': this.afCloseable
    };
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-notification-alert': true }, this.cssModifiers) }, h("div", { class: "digi-notification-alert__inner" }, h("div", { class: "digi-notification-alert__icon" }, h("digi-icon", { afName: `notification-${this.afVariation}`, "aria-hidden": "true" })), this.afCloseable && (h("digi-button", { "af-variation": ButtonVariation.FUNCTION, onAfOnClick: (e) => this.clickHandler(e), class: "digi-notification-alert__close-button" }, h("span", { class: "digi-notification-alert__close-button__text" }, "St\u00E4ng"), h("digi-icon", { afName: `close`, slot: "icon-secondary", "aria-hidden": "true" }))), h("div", { class: "digi-notification-alert__content" }, h("slot", { name: "heading" }), h("digi-typography", { class: "digi-notification-alert__text", "af-variation": TypographyVariation.SMALL }, h("slot", null)), h("slot", { name: "link" })), h("div", { class: "digi-notification-alert__actions" }, h("slot", { name: "actions" })))));
  }
  static get style() { return notificationAlertCss; }
}, [6, "digi-notification-alert", {
    "afVariation": [1, "af-variation"],
    "afCloseable": [4, "af-closeable"]
  }]);
function defineCustomElement() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-notification-alert", "digi-button", "digi-icon", "digi-typography"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-notification-alert":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, NotificationAlert);
      }
      break;
    case "digi-button":
      if (!customElements.get(tagName)) {
        defineCustomElement$3();
      }
      break;
    case "digi-icon":
      if (!customElements.get(tagName)) {
        defineCustomElement$2();
      }
      break;
    case "digi-typography":
      if (!customElements.get(tagName)) {
        defineCustomElement$1();
      }
      break;
  } });
}
defineCustomElement();

export { NotificationAlert as N, defineCustomElement as d };
