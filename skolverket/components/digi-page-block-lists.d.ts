import type { Components, JSX } from "../dist/types/components";

interface DigiPageBlockLists extends Components.DigiPageBlockLists, HTMLElement {}
export const DigiPageBlockLists: {
  prototype: DigiPageBlockLists;
  new (): DigiPageBlockLists;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
