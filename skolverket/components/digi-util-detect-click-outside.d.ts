import type { Components, JSX } from "../dist/types/components";

interface DigiUtilDetectClickOutside extends Components.DigiUtilDetectClickOutside, HTMLElement {}
export const DigiUtilDetectClickOutside: {
  prototype: DigiUtilDetectClickOutside;
  new (): DigiUtilDetectClickOutside;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
