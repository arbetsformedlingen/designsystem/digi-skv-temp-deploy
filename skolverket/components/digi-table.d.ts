import type { Components, JSX } from "../dist/types/components";

interface DigiTable extends Components.DigiTable, HTMLElement {}
export const DigiTable: {
  prototype: DigiTable;
  new (): DigiTable;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
