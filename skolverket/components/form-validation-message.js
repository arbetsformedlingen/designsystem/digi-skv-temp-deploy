import { proxyCustomElement, HTMLElement, h } from '@stencil/core/internal/client';
import { F as FormValidationMessageVariation } from './form-validation-message-variation.enum.js';
import { d as defineCustomElement$1 } from './icon.js';

const formValidationMessageCss = ".sc-digi-form-validation-message-h{--digi--form-validation-message--icon-color--success:var(--digi--color--icons--success);--digi--form-validation-message--icon-color--warning:var(--digi--color--icons--warning);--digi--form-validation-message--icon-color--error:var(--digi--color--icons--danger)}.sc-digi-form-validation-message-h .digi-form-validation-message.sc-digi-form-validation-message{display:flex;flex-wrap:wrap;align-items:center}.sc-digi-form-validation-message-h .digi-form-validation-message--success.sc-digi-form-validation-message{--ICON-COLOR:var(--digi--form-validation-message--icon-color--success)}.sc-digi-form-validation-message-h .digi-form-validation-message--warning.sc-digi-form-validation-message{--ICON-COLOR:var(--digi--form-validation-message--icon-color--warning)}.sc-digi-form-validation-message-h .digi-form-validation-message--error.sc-digi-form-validation-message{--ICON-COLOR:var(--digi--form-validation-message--icon-color--error)}.sc-digi-form-validation-message-h .digi-form-validation-message__icon.sc-digi-form-validation-message{color:var(--ICON-COLOR);-webkit-margin-end:var(--digi--gutter--icon);margin-inline-end:var(--digi--gutter--icon)}.sc-digi-form-validation-message-h .digi-form-validation-message__icon.sc-digi-form-validation-message>*.sc-digi-form-validation-message{--digi--icon--width:1.25em;--digi--icon--height:1.25em;--digi--icon--color:var(--ICON-COLOR);display:flex}.sc-digi-form-validation-message-h .digi-form-validation-message__text.sc-digi-form-validation-message{flex:1;font-family:var(--digi--global--typography--font-family--default);font-size:var(--digi--typography--tag--font-size--desktop);color:var(--digi--color--text--primary)}";

const FormValidationMessage = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afVariation = FormValidationMessageVariation.SUCCESS;
  }
  get cssModifiers() {
    return {
      'digi-form-validation-message--success': this.afVariation === FormValidationMessageVariation.SUCCESS,
      'digi-form-validation-message--error': this.afVariation === FormValidationMessageVariation.ERROR,
      'digi-form-validation-message--warning': this.afVariation === FormValidationMessageVariation.WARNING
    };
  }
  get icon() {
    switch (this.afVariation) {
      case FormValidationMessageVariation.SUCCESS:
        return 'check-circle';
      case FormValidationMessageVariation.ERROR:
        return 'exclamation-circle-filled';
      case FormValidationMessageVariation.WARNING:
        return 'exclamation-triangle-warning';
    }
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-form-validation-message': true }, this.cssModifiers) }, h("div", { class: "digi-form-validation-message__icon", "aria-hidden": "true" }, h("digi-icon", { afName: this.icon })), h("span", { class: "digi-form-validation-message__text" }, h("slot", null))));
  }
  get hostElement() { return this; }
  static get style() { return formValidationMessageCss; }
}, [6, "digi-form-validation-message", {
    "afVariation": [1, "af-variation"]
  }]);
function defineCustomElement() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-form-validation-message", "digi-icon"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-form-validation-message":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, FormValidationMessage);
      }
      break;
    case "digi-icon":
      if (!customElements.get(tagName)) {
        defineCustomElement$1();
      }
      break;
  } });
}
defineCustomElement();

export { FormValidationMessage as F, defineCustomElement as d };
