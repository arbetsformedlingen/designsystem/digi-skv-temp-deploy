import type { Components, JSX } from "../dist/types/components";

interface DigiFormInput extends Components.DigiFormInput, HTMLElement {}
export const DigiFormInput: {
  prototype: DigiFormInput;
  new (): DigiFormInput;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
