import type { Components, JSX } from "../dist/types/components";

interface DigiPageBlockSidebar extends Components.DigiPageBlockSidebar, HTMLElement {}
export const DigiPageBlockSidebar: {
  prototype: DigiPageBlockSidebar;
  new (): DigiPageBlockSidebar;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
