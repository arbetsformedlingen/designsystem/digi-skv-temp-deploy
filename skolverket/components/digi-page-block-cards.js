import { proxyCustomElement, HTMLElement, h } from '@stencil/core/internal/client';
import { a as CardBoxWidth, d as defineCustomElement$5 } from './card-box.js';
import './page-background.enum.js';
import './layout-grid-vertical-spacing.enum.js';
import './layout-stacked-blocks-variation.enum.js';
import './list-link-variation.enum.js';
import './navigation-breadcrumbs-variation.enum.js';
import './notification-alert-variation.enum.js';
import './notification-detail-variation.enum.js';
import './page-footer-variation.enum.js';
import './table-variation.enum.js';
import { d as defineCustomElement$4 } from './layout-container.js';
import { d as defineCustomElement$3 } from './layout-stacked-blocks.js';
import { d as defineCustomElement$2 } from './typography-heading-section.js';

const pageBlockCardsCss = ".digi-page-block-cards.sc-digi-page-block-cards{display:block;background:var(--digi--layout-page-block-cards--background, transparent);padding:var(--digi--responsive-grid-gutter) 0}.digi-page-block-cards--variation-start.sc-digi-page-block-cards,.digi-page-block-cards--variation-sub.sc-digi-page-block-cards{--digi--layout-page-block-cards--background:var(--digi--global--color--profile--apricot--opacity50)}.digi-page-block-cards--variation-section.sc-digi-page-block-cards{--digi--layout-page-block-cards--background:var(--digi--color--background--primary)}.digi-page-block-cards__inner.sc-digi-page-block-cards{display:flex;flex-direction:column;gap:var(--digi--responsive-grid-gutter)}";

const PageBlockCards = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afVariation = undefined;
  }
  get cssModifiers() {
    return {
      [`digi-page-block-cards--variation-${this.afVariation}`]: !!this.afVariation
    };
  }
  render() {
    return (h("digi-layout-container", { class: Object.assign({ 'digi-page-block-cards': true }, this.cssModifiers) }, h("digi-card-box", { afWidth: CardBoxWidth.FULL }, h("div", { class: "digi-page-block-cards__inner" }, h("digi-typography-heading-section", null, h("slot", { name: "heading" })), h("digi-layout-stacked-blocks", null, h("slot", null))))));
  }
  static get assetsDirs() { return ["public"]; }
  static get style() { return pageBlockCardsCss; }
}, [6, "digi-page-block-cards", {
    "afVariation": [1, "af-variation"]
  }]);
function defineCustomElement$1() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-page-block-cards", "digi-card-box", "digi-layout-container", "digi-layout-stacked-blocks", "digi-typography-heading-section"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-page-block-cards":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, PageBlockCards);
      }
      break;
    case "digi-card-box":
      if (!customElements.get(tagName)) {
        defineCustomElement$5();
      }
      break;
    case "digi-layout-container":
      if (!customElements.get(tagName)) {
        defineCustomElement$4();
      }
      break;
    case "digi-layout-stacked-blocks":
      if (!customElements.get(tagName)) {
        defineCustomElement$3();
      }
      break;
    case "digi-typography-heading-section":
      if (!customElements.get(tagName)) {
        defineCustomElement$2();
      }
      break;
  } });
}
defineCustomElement$1();

const DigiPageBlockCards = PageBlockCards;
const defineCustomElement = defineCustomElement$1;

export { DigiPageBlockCards, defineCustomElement };
