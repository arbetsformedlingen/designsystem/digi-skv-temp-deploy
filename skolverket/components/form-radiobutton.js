import { proxyCustomElement, HTMLElement, createEvent, h } from '@stencil/core/internal/client';
import { r as randomIdGenerator } from './randomIdGenerator.util.js';
import { b as FormRadiobuttonVariation, F as FormRadiobuttonLayout, a as FormRadiobuttonValidation } from './form-radiobutton-variation.enum.js';

const formRadiobuttonCss = ".sc-digi-form-radiobutton-h{--digi--form-radiobutton--border-color:var(--digi--color--border--primary);--digi--form-radiobutton--border-color--primary:var(--digi--color--border--radio-primary);--digi--form-radiobutton--border-color--secondary:var(--digi--color--border--radio-secondary);--digi--form-radiobutton--border-color--error:var(--digi--color--border--danger);--digi--form-radiobutton--box-shadow--error:var(--digi--color--border--danger);--digi--form-radiobutton--background-color:var(--digi--color--background--primary);--digi--form-radiobutton--background-color--primary:var(--digi--color--background--radio-primary);--digi--form-radiobutton--background-color--secondary:var(--digi--color--background--radio-secondary);--digi--form-radiobutton--background-color--error:var(--digi--color--background--danger-2);--digi--form-radiobutton--label--background-color--hover:var(--digi--color--background--input-empty);--digi--form-radiobutton--shadow--color--focus:var(--digi--color--border--radio-focus);--digi--form-radiobutton--border-width:var(--digi--border-width--primary);--digi--form-radiobutton--color--marker:var(--digi--color--background--radio-marker);--digi--form-radiobutton--size:1.25rem;--digi--form-radiobutton--marker--size:calc(var(--digi--typography--input--font-size--desktop-large) * 0.4);--digi--form-radiobutton--height:var(--digi--input-height--large);--digi--form-radiobutton--border-radius:var(--digi--border-radius--primary);--digi--form-radiobutton--padding:var(--digi--gutter--icon)}.sc-digi-form-radiobutton-h .digi-form-radiobutton.sc-digi-form-radiobutton{--BACKGROUND--DEFAULT:var(--digi--form-radiobutton--background-color);--BORDER-COLOR--DEFAULT:var(--digi--form-radiobutton--border-color)}.sc-digi-form-radiobutton-h .digi-form-radiobutton--primary.sc-digi-form-radiobutton{--BACKGROUND--CHECKED:var(--digi--form-radiobutton--background-color--primary);--BORDER-COLOR--CHECKED:var(--digi--form-radiobutton--border-color--primary)}.sc-digi-form-radiobutton-h .digi-form-radiobutton--secondary.sc-digi-form-radiobutton{--BACKGROUND--CHECKED:var(--digi--form-radiobutton--background-color--secondary);--BORDER-COLOR--CHECKED:var(--digi--form-radiobutton--border-color--secondary)}.sc-digi-form-radiobutton-h .digi-form-radiobutton--error.sc-digi-form-radiobutton .digi-form-radiobutton__input.sc-digi-form-radiobutton:not(:checked)~.digi-form-radiobutton__label.sc-digi-form-radiobutton .digi-form-radiobutton__circle.sc-digi-form-radiobutton{--BACKGROUND--CHECKED:var(--digi--form-radiobutton--background-color--error);--BORDER-COLOR--CHECKED:var(--digi--form-radiobutton--border-color--error)}.sc-digi-form-radiobutton-h .digi-form-radiobutton--error.sc-digi-form-radiobutton .digi-form-radiobutton__input.sc-digi-form-radiobutton~.digi-form-radiobutton__label.sc-digi-form-radiobutton .digi-form-radiobutton__circle.sc-digi-form-radiobutton{background-color:var(--BACKGROUND--CHECKED);border-color:var(--BORDER-COLOR--CHECKED);box-shadow:0 0 0 var(--digi--border-width--primary) var(--digi--form-radiobutton--box-shadow--error)}.digi-form-radiobutton__input.sc-digi-form-radiobutton{position:absolute;opacity:0;cursor:pointer;height:0;width:0;top:0;left:0}.digi-form-radiobutton__label.sc-digi-form-radiobutton{display:inline-flex;align-items:center;gap:var(--digi--gutter--icon);font-family:var(--digi--global--typography--font-family--default);font-size:var(--digi--typography--body--font-size--desktop);color:var(--digi--color--text--primary);font-weight:var(--digi--typography--body--font-weight--desktop);cursor:pointer}.digi-form-radiobutton--layout-block.sc-digi-form-radiobutton .digi-form-radiobutton__label.sc-digi-form-radiobutton{display:flex;padding:0 var(--digi--form-radiobutton--padding);min-height:var(--digi--form-radiobutton--height);border-radius:var(--digi--border-radius--primary)}.digi-form-radiobutton--layout-block.sc-digi-form-radiobutton .digi-form-radiobutton__label.sc-digi-form-radiobutton:hover{background-color:var(--digi--form-radiobutton--label--background-color--hover)}.digi-form-radiobutton__circle.sc-digi-form-radiobutton{cursor:pointer;background-color:var(--BACKGROUND--DEFAULT);border-color:var(--BORDER-COLOR--DEFAULT);border-width:var(--digi--form-radiobutton--border-width);border-style:solid;height:var(--digi--form-radiobutton--size);width:var(--digi--form-radiobutton--size);border-radius:var(--digi--form-radiobutton--size);position:relative}.digi-form-radiobutton__input.sc-digi-form-radiobutton:focus-visible~.digi-form-radiobutton__label.sc-digi-form-radiobutton .digi-form-radiobutton__circle.sc-digi-form-radiobutton{box-shadow:0 0 var(--digi--border-width--primary) var(--digi--form-radiobutton--shadow--color--focus)}.digi-form-radiobutton--layout-block.sc-digi-form-radiobutton .digi-form-radiobutton__input.sc-digi-form-radiobutton:focus-visible~.digi-form-radiobutton__label.sc-digi-form-radiobutton{box-shadow:none;outline:var(--digi--focus-outline);background-color:var(--digi--form-radiobutton--label--background-color--hover)}.digi-form-radiobutton__input.sc-digi-form-radiobutton:checked~.digi-form-radiobutton__label.sc-digi-form-radiobutton .digi-form-radiobutton__circle.sc-digi-form-radiobutton{background-color:var(--BACKGROUND--CHECKED);border-color:var(--BORDER-COLOR--CHECKED)}.digi-form-radiobutton__input.sc-digi-form-radiobutton:checked~.digi-form-radiobutton__label.sc-digi-form-radiobutton .digi-form-radiobutton__circle.sc-digi-form-radiobutton::after{content:\"\";width:var(--digi--form-radiobutton--marker--size);height:var(--digi--form-radiobutton--marker--size);border-radius:var(--digi--form-radiobutton--marker--size);background:var(--digi--form-radiobutton--color--marker);position:absolute;left:calc(50% - var(--digi--form-radiobutton--marker--size) / 2);top:calc(50% - var(--digi--form-radiobutton--marker--size) / 2)}";

const FormRadiobutton = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afOnChange = createEvent(this, "afOnChange", 7);
    this.afOnBlur = createEvent(this, "afOnBlur", 7);
    this.afOnFocus = createEvent(this, "afOnFocus", 7);
    this.afOnFocusout = createEvent(this, "afOnFocusout", 7);
    this.afOnInput = createEvent(this, "afOnInput", 7);
    this.afOnDirty = createEvent(this, "afOnDirty", 7);
    this.afOnTouched = createEvent(this, "afOnTouched", 7);
    this.dirty = false;
    this.touched = false;
    this.afLabel = undefined;
    this.afVariation = FormRadiobuttonVariation.PRIMARY;
    this.afLayout = FormRadiobuttonLayout.INLINE;
    this.afName = undefined;
    this.afId = randomIdGenerator('digi-form-radiobutton');
    this.afRequired = undefined;
    this.value = undefined;
    this.afValue = undefined;
    this.checked = undefined;
    this.afChecked = undefined;
    this.afValidation = undefined;
    this.afAriaLabelledby = undefined;
    this.afAriaDescribedby = undefined;
    this.afAutofocus = undefined;
  }
  onValueChanged(value) {
    this.afValue = value;
  }
  onAfValueChanged(value) {
    this.value = value;
  }
  onCheckedChanged(checked) {
    this.afChecked = checked;
  }
  onAfCheckedChanged(checked) {
    this.checked = checked;
  }
  /**
   * Hämta en referens till inputelementet. Bra för att t.ex. sätta fokus programmatiskt.
   * @en Returns a reference to the input element. Handy for setting focus programmatically.
   */
  async afMGetFormControlElement() {
    return this._input;
  }
  componentDidLoad() {
    if (this.value) {
      this.afValue = this.value;
    }
  }
  get cssModifiers() {
    return {
      'digi-form-radiobutton--error': this.afValidation === FormRadiobuttonValidation.ERROR,
      'digi-form-radiobutton--primary': this.afVariation === FormRadiobuttonVariation.PRIMARY,
      'digi-form-radiobutton--secondary': this.afVariation === FormRadiobuttonVariation.SECONDARY,
      [`digi-form-radiobutton--layout-${this.afLayout}`]: !!this.afLayout
    };
  }
  blurHandler(e) {
    if (!this.touched) {
      this.afOnTouched.emit(e);
      this.touched = true;
    }
    this.afOnBlur.emit(e);
  }
  changeHandler(e) {
    this.afOnChange.emit(e);
  }
  focusHandler(e) {
    this.afOnFocus.emit(e);
  }
  focusoutHandler(e) {
    this.afOnFocusout.emit(e);
  }
  inputHandler(e) {
    if (!this.dirty) {
      this.afOnDirty.emit(e);
      this.dirty = true;
    }
    this.afOnInput.emit(e);
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-form-radiobutton': true }, this.cssModifiers) }, h("input", { class: "digi-form-radiobutton__input", ref: (el) => (this._input = el), onInput: (e) => this.inputHandler(e), onBlur: (e) => this.blurHandler(e), onChange: (e) => this.changeHandler(e), onFocus: (e) => this.focusHandler(e), onFocusout: (e) => this.focusoutHandler(e), "aria-describedby": this.afAriaDescribedby, "aria-labelledby": this.afAriaLabelledby, required: this.afRequired, id: this.afId, name: this.afName, type: "radio", checked: this.afChecked, value: this.afValue, autofocus: this.afAutofocus ? this.afAutofocus : null }), h("label", { htmlFor: this.afId, class: "digi-form-radiobutton__label" }, h("span", { class: "digi-form-radiobutton__circle" }), this.afLabel)));
  }
  get hostElement() { return this; }
  static get watchers() { return {
    "value": ["onValueChanged"],
    "afValue": ["onAfValueChanged"],
    "checked": ["onCheckedChanged"],
    "afChecked": ["onAfCheckedChanged"]
  }; }
  static get style() { return formRadiobuttonCss; }
}, [2, "digi-form-radiobutton", {
    "afLabel": [1, "af-label"],
    "afVariation": [1, "af-variation"],
    "afLayout": [1, "af-layout"],
    "afName": [1, "af-name"],
    "afId": [1, "af-id"],
    "afRequired": [4, "af-required"],
    "value": [1],
    "afValue": [1, "af-value"],
    "checked": [4],
    "afChecked": [4, "af-checked"],
    "afValidation": [1, "af-validation"],
    "afAriaLabelledby": [1, "af-aria-labelledby"],
    "afAriaDescribedby": [1, "af-aria-describedby"],
    "afAutofocus": [4, "af-autofocus"],
    "dirty": [32],
    "touched": [32],
    "afMGetFormControlElement": [64]
  }]);
function defineCustomElement() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-form-radiobutton"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-form-radiobutton":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, FormRadiobutton);
      }
      break;
  } });
}
defineCustomElement();

export { FormRadiobutton as F, defineCustomElement as d };
