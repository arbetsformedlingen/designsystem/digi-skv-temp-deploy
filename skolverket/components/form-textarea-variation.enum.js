var FormTextareaValidation;
(function (FormTextareaValidation) {
  FormTextareaValidation["SUCCESS"] = "success";
  FormTextareaValidation["ERROR"] = "error";
  FormTextareaValidation["WARNING"] = "warning";
  FormTextareaValidation["NEUTRAL"] = "neutral";
})(FormTextareaValidation || (FormTextareaValidation = {}));

var FormTextareaVariation;
(function (FormTextareaVariation) {
  FormTextareaVariation["SMALL"] = "small";
  FormTextareaVariation["MEDIUM"] = "medium";
  FormTextareaVariation["LARGE"] = "large";
  FormTextareaVariation["AUTO"] = "auto";
})(FormTextareaVariation || (FormTextareaVariation = {}));

export { FormTextareaValidation as F, FormTextareaVariation as a };
