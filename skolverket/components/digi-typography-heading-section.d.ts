import type { Components, JSX } from "../dist/types/components";

interface DigiTypographyHeadingSection extends Components.DigiTypographyHeadingSection, HTMLElement {}
export const DigiTypographyHeadingSection: {
  prototype: DigiTypographyHeadingSection;
  new (): DigiTypographyHeadingSection;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
