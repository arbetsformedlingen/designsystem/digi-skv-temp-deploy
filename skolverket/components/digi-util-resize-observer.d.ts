import type { Components, JSX } from "../dist/types/components";

interface DigiUtilResizeObserver extends Components.DigiUtilResizeObserver, HTMLElement {}
export const DigiUtilResizeObserver: {
  prototype: DigiUtilResizeObserver;
  new (): DigiUtilResizeObserver;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
