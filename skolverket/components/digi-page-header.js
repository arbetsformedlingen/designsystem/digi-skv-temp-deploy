import { proxyCustomElement, HTMLElement, h } from '@stencil/core/internal/client';
import { r as randomIdGenerator } from './randomIdGenerator.util.js';
import { B as BREAKPOINT_LARGE } from './tokens.es6.js';
import { d as defineCustomElement$4 } from './icon.js';
import { d as defineCustomElement$3 } from './layout-container.js';
import { d as defineCustomElement$2 } from './navigation-main-menu.js';

const pageHeaderCss = ".sc-digi-page-header-h{--digi-page-header--eyebrow-direction:column;--digi-page-header--eyebrow-justify:flex-end;--digi-page-header--eyebrow-padding:var(--digi--padding--largest)}@media (min-width: 62rem){.sc-digi-page-header-h{--digi-page-header--eyebrow-direction:row;--digi-page-header--eyebrow-justify:flex-start;--digi-page-header--eyebrow-padding:0}}.sc-digi-page-header-h .sc-digi-page-header-s>[slot^=top]{font-family:var(--digi--global--typography--font-family--default);font-size:var(--digi--global--typography--font-size--smallest-2)}.sc-digi-page-header-h .sc-digi-page-header-s>[slot^=top] a,.sc-digi-page-header-h .sc-digi-page-header-s>[slot^=top] a:hover,.sc-digi-page-header-h .sc-digi-page-header-s>[slot^=top] a:visited{color:var(--digi--color--text--secondary)}.sc-digi-page-header-h .sc-digi-page-header-s>[slot^=top] a:focus{outline:none;color:var(--digi--color--text--secondary)}.sc-digi-page-header-h .sc-digi-page-header-s>[slot^=top] a:focus-visible{outline:var(--digi--border-width--secondary) solid var(--digi--color--border--focus)}.digi-page-header.sc-digi-page-header{background:var(--digi--color--background--primary);-webkit-border-before:var(--digi--border-width--secondary) solid var(--digi--color--border--secondary);border-block-start:var(--digi--border-width--secondary) solid var(--digi--color--border--secondary);display:grid;align-items:center}@media (max-width: 61.9375rem){.digi-page-header--expanded-true.sc-digi-page-header{grid-template-rows:72px auto auto calc(var(--digi--global--spacing--smaller-3) * 3);grid-template-columns:2fr 1fr;grid-template-areas:\"logo toggle\" \"navigation navigation\" \"eyebrow eyebrow\" \"top top\";-webkit-border-after:80px solid var(--digi--color--border--secondary);border-block-end:80px solid var(--digi--color--border--secondary)}.digi-page-header--expanded-false.sc-digi-page-header{grid-template-columns:2fr 1fr;grid-template-rows:72px;grid-template-areas:\"logo toggle\";-webkit-border-after:var(--digi--border-width--tertiary) var(--digi--border-style--primary) var(--digi--global--color--profile--purple--opacity15);border-block-end:var(--digi--border-width--tertiary) var(--digi--border-style--primary) var(--digi--global--color--profile--purple--opacity15)}}@media (min-width: 62rem){.digi-page-header.sc-digi-page-header{grid-template-rows:calc(var(--digi--global--spacing--smaller-3) * 3) calc(var(--digi--global--spacing--largest) - var(--digi--global--spacing--smallest-4)) auto;grid-template-areas:\"top\" \"main\" \"navigation\"}}.digi-page-header__top.sc-digi-page-header{grid-area:top;background:var(--digi--color--background--secondary);height:100%;display:flex;align-items:center}@media (max-width: 61.9375rem){.digi-page-header__top.sc-digi-page-header{-webkit-border-before:var(--digi--border-width--secondary) solid var(--digi--color--border--secondary);border-block-start:var(--digi--border-width--secondary) solid var(--digi--color--border--secondary)}.digi-page-header--expanded-false.sc-digi-page-header .digi-page-header__top.sc-digi-page-header{display:none}}@media (min-width: 62rem){.digi-page-header__main.sc-digi-page-header{width:100%;max-width:var(--digi--responsive-container-width);margin-inline:auto;padding-inline:var(--digi--responsive-grid-gutter--outer);grid-area:main;display:flex;justify-content:space-between;align-items:center}}@media (max-width: 61.9375rem){.digi-page-header__main.sc-digi-page-header{display:contents}}.digi-page-header__logo.sc-digi-page-header{grid-area:logo}@media (max-width: 61.9375rem){.digi-page-header__logo.sc-digi-page-header{-webkit-padding-start:var(--digi--padding--largest);padding-inline-start:var(--digi--padding--largest)}}.digi-page-header__nav.sc-digi-page-header{grid-area:navigation}@media (max-width: 61.9375rem){.digi-page-header--expanded-false.sc-digi-page-header .digi-page-header__nav.sc-digi-page-header{display:none}}.digi-page-header__eyebrow.sc-digi-page-header{grid-area:eyebrow}.digi-page-header__eyebrow.sc-digi-page-header-s>ul,.digi-page-header__eyebrow .sc-digi-page-header-s>ul{display:flex;gap:var(--digi--global--spacing--smaller);justify-content:var(--digi-page-header--eyebrow-justify);flex-direction:var(--digi-page-header--eyebrow-direction);padding:var(--digi-page-header--eyebrow-padding);list-style:none}.digi-page-header__eyebrow.sc-digi-page-header-s>ul li,.digi-page-header__eyebrow .sc-digi-page-header-s>ul li{list-style:none}@media (max-width: 61.9375rem){.digi-page-header--expanded-false.sc-digi-page-header .digi-page-header__eyebrow.sc-digi-page-header{display:none}}.digi-page-header__toggle-button.sc-digi-page-header{display:none}@media (max-width: 61.9375rem){.digi-page-header__toggle-button.sc-digi-page-header{grid-area:toggle;display:flex;-webkit-padding-end:var(--digi--padding--small);padding-inline-end:var(--digi--padding--small);justify-self:end}.digi-page-header__toggle-button.sc-digi-page-header button.sc-digi-page-header{display:inline-flex;flex-direction:column;justify-content:center;align-items:center;color:var(--digi--color--text--secondary)}.digi-page-header__toggle-button.sc-digi-page-header button.sc-digi-page-header svg.sc-digi-page-header{--digi--icon--color:currentColor}.digi-page-header__toggle-button.sc-digi-page-header button.sc-digi-page-header span.sc-digi-page-header{font-family:var(--digi--typography--font-family);font-weight:var(--digi--global--typography--font-weight--semibold);font-size:var(--digi--global--typography--font-size--smallest);text-transform:uppercase}.digi-page-header__toggle-button.sc-digi-page-header button.sc-digi-page-header:focus-visible{outline:var(--digi--focus-outline)}}";

const PageHeader = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.isExpanded = false;
    this.afId = randomIdGenerator('digi-page-header');
  }
  get cssModifiers() {
    return {
      [`digi-page-header--expanded-${this.isExpanded}`]: true
    };
  }
  keyUpHandler(e) {
    var _a;
    if (e.key !== 'Escape' ||
      window.matchMedia(`(min-width: ${BREAKPOINT_LARGE})`).matches ||
      !!this.hostElement.querySelector('.digi-navigation-main-menu--active-true'))
      return;
    this.isExpanded = false;
    (_a = this.toggleButton) === null || _a === void 0 ? void 0 : _a.focus();
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-page-header': true }, this.cssModifiers) }, h("digi-layout-container", { class: "digi-page-header__top", id: `${this.afId}-top` }, h("div", null, h("slot", { name: "top" }))), h("div", { class: "digi-page-header__main" }, h("div", { class: "digi-page-header__logo" }, h("slot", { name: "logo" })), h("div", { class: "digi-page-header__toggle-button" }, h("button", { "aria-controls": `${this.afId}-top ${this.afId}-eyebrow ${this.afId}-nav`, "aria-expanded": `${this.isExpanded}`, onClick: () => (this.isExpanded = !this.isExpanded), ref: (el) => (this.toggleButton = el) }, h("digi-icon", { afName: this.isExpanded ? 'x' : 'bars' }), h("span", null, this.isExpanded ? 'Stäng' : 'Meny'))), h("div", { class: "digi-page-header__eyebrow", id: `${this.afId}-eyebrow` }, h("slot", { name: "eyebrow" }))), h("div", { class: "digi-page-header__nav", id: `${this.afId}-nav` }, h("digi-navigation-main-menu", null, h("slot", { name: "nav" })))));
  }
  get hostElement() { return this; }
  static get style() { return pageHeaderCss; }
}, [6, "digi-page-header", {
    "afId": [1, "af-id"],
    "isExpanded": [32]
  }, [[0, "keyup", "keyUpHandler"]]]);
function defineCustomElement$1() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-page-header", "digi-icon", "digi-layout-container", "digi-navigation-main-menu"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-page-header":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, PageHeader);
      }
      break;
    case "digi-icon":
      if (!customElements.get(tagName)) {
        defineCustomElement$4();
      }
      break;
    case "digi-layout-container":
      if (!customElements.get(tagName)) {
        defineCustomElement$3();
      }
      break;
    case "digi-navigation-main-menu":
      if (!customElements.get(tagName)) {
        defineCustomElement$2();
      }
      break;
  } });
}
defineCustomElement$1();

const DigiPageHeader = PageHeader;
const defineCustomElement = defineCustomElement$1;

export { DigiPageHeader, defineCustomElement };
