import { proxyCustomElement, HTMLElement, h } from '@stencil/core/internal/client';
import { d as defineCustomElement$3 } from './layout-container.js';
import { d as defineCustomElement$2 } from './layout-grid.js';

const pageBlockSidebarCss = ".sc-digi-page-block-sidebar-h{--digi--layout-page-block-sidebar--sidebar-columns:4}@media (min-width: 48rem){.sc-digi-page-block-sidebar-h{--digi--layout-page-block-sidebar--sidebar-columns:4}}@media (min-width: 62rem){.sc-digi-page-block-sidebar-h{--digi--layout-page-block-sidebar--sidebar-columns:5}}.digi-page-block-sidebar.sc-digi-page-block-sidebar{display:block;background:var(--digi--layout-page-block-sidebar--background, transparent);padding:var(--digi--responsive-grid-gutter) 0}.digi-page-block-sidebar--variation-start.sc-digi-page-block-sidebar,.digi-page-block-sidebar--variation-sub.sc-digi-page-block-sidebar{--digi--layout-page-block-sidebar--background:var(--digi--global--color--profile--apricot--opacity50)}.digi-page-block-sidebar--variation-section.sc-digi-page-block-sidebar{--digi--layout-page-block-sidebar--background:var(--digi--color--background--primary)}.digi-page-block-sidebar__sidebar.sc-digi-page-block-sidebar{grid-column:1/-1}@media (min-width: 48rem){.digi-page-block-sidebar__sidebar.sc-digi-page-block-sidebar{grid-column:1/var(--digi--layout-page-block-sidebar--sidebar-columns)}}.digi-page-block-sidebar__content.sc-digi-page-block-sidebar{grid-column:1/-1;grid-row:2}@media (min-width: 48rem){.digi-page-block-sidebar__content.sc-digi-page-block-sidebar{grid-row:1;grid-column:var(--digi--layout-page-block-sidebar--sidebar-columns)/calc(var(--digi--layout-grid--columns) + 1)}}";

const PageBlockSidebar = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afVariation = undefined;
  }
  get cssModifiers() {
    return {
      [`digi-page-block-sidebar--variation-${this.afVariation}`]: !!this.afVariation
    };
  }
  render() {
    return (h("digi-layout-grid", { class: Object.assign({ 'digi-page-block-sidebar': true }, this.cssModifiers) }, h("div", { class: "digi-page-block-sidebar__sidebar" }, h("slot", { name: "sidebar" })), h("div", { class: "digi-page-block-sidebar__content" }, h("slot", null))));
  }
  static get assetsDirs() { return ["public"]; }
  static get style() { return pageBlockSidebarCss; }
}, [6, "digi-page-block-sidebar", {
    "afVariation": [1, "af-variation"]
  }]);
function defineCustomElement$1() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-page-block-sidebar", "digi-layout-container", "digi-layout-grid"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-page-block-sidebar":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, PageBlockSidebar);
      }
      break;
    case "digi-layout-container":
      if (!customElements.get(tagName)) {
        defineCustomElement$3();
      }
      break;
    case "digi-layout-grid":
      if (!customElements.get(tagName)) {
        defineCustomElement$2();
      }
      break;
  } });
}
defineCustomElement$1();

const DigiPageBlockSidebar = PageBlockSidebar;
const defineCustomElement = defineCustomElement$1;

export { DigiPageBlockSidebar, defineCustomElement };
