import type { Components, JSX } from "../dist/types/components";

interface DigiIconX extends Components.DigiIconX, HTMLElement {}
export const DigiIconX: {
  prototype: DigiIconX;
  new (): DigiIconX;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
