import type { Components, JSX } from "../dist/types/components";

interface DigiNavigationTabs extends Components.DigiNavigationTabs, HTMLElement {}
export const DigiNavigationTabs: {
  prototype: DigiNavigationTabs;
  new (): DigiNavigationTabs;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
