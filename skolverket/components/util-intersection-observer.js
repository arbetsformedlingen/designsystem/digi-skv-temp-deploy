import { proxyCustomElement, HTMLElement, createEvent, h } from '@stencil/core/internal/client';

const UtilIntersectionObserver = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afOnChange = createEvent(this, "afOnChange", 7);
    this.afOnIntersect = createEvent(this, "afOnIntersect", 7);
    this.afOnUnintersect = createEvent(this, "afOnUnintersect", 7);
    this._observer = null;
    this._hasIntersected = false;
    this._isObserving = false;
    this.afOnce = false;
    this.afOptions = {
      rootMargin: '0px',
      threshold: 0
    };
  }
  afOptionsWatcher(newValue) {
    this._afOptions =
      typeof newValue === 'string' ? JSON.parse(newValue) : newValue;
  }
  componentWillLoad() {
    this.afOptionsWatcher(this.afOptions);
    this.initObserver();
  }
  disconnectedCallback() {
    if (this._isObserving) {
      this.removeObserver();
    }
  }
  initObserver() {
    this._observer = new IntersectionObserver(([entry]) => {
      if (entry && entry.isIntersecting) {
        this.intersectionHandler();
        if (this.afOnce) {
          this.removeObserver();
        }
      }
      else if (entry && this._hasIntersected) {
        this.unintersectionHandler();
      }
    }, this._afOptions);
    this._observer.observe(this.$el);
    this._isObserving = true;
  }
  removeObserver() {
    this._observer.disconnect();
    this._isObserving = false;
  }
  intersectionHandler() {
    this.afOnChange.emit(true);
    this.afOnIntersect.emit();
    this._hasIntersected = true;
  }
  unintersectionHandler() {
    this.afOnChange.emit(false);
    this.afOnUnintersect.emit();
    this._hasIntersected = false;
  }
  render() {
    return h("slot", null);
  }
  get $el() { return this; }
  static get watchers() { return {
    "afOptions": ["afOptionsWatcher"]
  }; }
}, [6, "digi-util-intersection-observer", {
    "afOnce": [4, "af-once"],
    "afOptions": [1, "af-options"]
  }]);
function defineCustomElement() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-util-intersection-observer"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-util-intersection-observer":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, UtilIntersectionObserver);
      }
      break;
  } });
}
defineCustomElement();

export { UtilIntersectionObserver as U, defineCustomElement as d };
