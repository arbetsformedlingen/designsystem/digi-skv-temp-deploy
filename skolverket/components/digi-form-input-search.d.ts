import type { Components, JSX } from "../dist/types/components";

interface DigiFormInputSearch extends Components.DigiFormInputSearch, HTMLElement {}
export const DigiFormInputSearch: {
  prototype: DigiFormInputSearch;
  new (): DigiFormInputSearch;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
