import type { Components, JSX } from "../dist/types/components";

interface DigiCodeExample extends Components.DigiCodeExample, HTMLElement {}
export const DigiCodeExample: {
  prototype: DigiCodeExample;
  new (): DigiCodeExample;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
