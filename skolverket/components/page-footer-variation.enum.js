var PageFooterVariation;
(function (PageFooterVariation) {
  PageFooterVariation["PRIMARY"] = "primary";
  PageFooterVariation["SECONDARY"] = "secondary";
})(PageFooterVariation || (PageFooterVariation = {}));

export { PageFooterVariation as P };
