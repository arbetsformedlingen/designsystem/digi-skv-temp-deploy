import { T as Typography, d as defineCustomElement$1 } from './typography.js';

const DigiTypography = Typography;
const defineCustomElement = defineCustomElement$1;

export { DigiTypography, defineCustomElement };
