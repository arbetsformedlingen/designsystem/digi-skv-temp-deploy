import type { Components, JSX } from "../dist/types/components";

interface DigiNavigationSidebar extends Components.DigiNavigationSidebar, HTMLElement {}
export const DigiNavigationSidebar: {
  prototype: DigiNavigationSidebar;
  new (): DigiNavigationSidebar;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
