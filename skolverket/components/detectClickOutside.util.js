import { d as detectClosest } from './detectClosest.util.js';

function detectClickOutside(target, selector) {
  return !detectClosest(target, selector);
}

export { detectClickOutside as d };
