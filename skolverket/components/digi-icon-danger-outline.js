import { I as IconDangerOutline, d as defineCustomElement$1 } from './icon-danger-outline.js';

const DigiIconDangerOutline = IconDangerOutline;
const defineCustomElement = defineCustomElement$1;

export { DigiIconDangerOutline, defineCustomElement };
