import type { Components, JSX } from "../dist/types/components";

interface DigiButton extends Components.DigiButton, HTMLElement {}
export const DigiButton: {
  prototype: DigiButton;
  new (): DigiButton;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
