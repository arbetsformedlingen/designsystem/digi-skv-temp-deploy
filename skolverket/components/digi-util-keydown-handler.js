import { U as UtilKeydownHandler, d as defineCustomElement$1 } from './util-keydown-handler.js';

const DigiUtilKeydownHandler = UtilKeydownHandler;
const defineCustomElement = defineCustomElement$1;

export { DigiUtilKeydownHandler, defineCustomElement };
