import type { Components, JSX } from "../dist/types/components";

interface DigiFormFieldset extends Components.DigiFormFieldset, HTMLElement {}
export const DigiFormFieldset: {
  prototype: DigiFormFieldset;
  new (): DigiFormFieldset;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
