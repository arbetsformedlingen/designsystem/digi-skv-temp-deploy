/* DigiSkolverket custom elements */
export { Button as DigiButton } from '../dist/types/__core/_button/button/button';
export { Calendar as DigiCalendar } from '../dist/types/__core/_calendar/calendar/calendar';
export { CalendarWeekView as DigiCalendarWeekView } from '../dist/types/__core/_calendar/week-view/calendar-week-view';
export { CardBox as DigiCardBox } from '../dist/types/components/_card/card-box/card-box';
export { CardLink as DigiCardLink } from '../dist/types/components/_card/card-link/card-link';
export { Code as DigiCode } from '../dist/types/__core/_code/code/code';
export { CodeBlock as DigiCodeBlock } from '../dist/types/__core/_code/code-block/code-block';
export { CodeExample as DigiCodeExample } from '../dist/types/__core/_code/code-example/code-example';
export { Dialog as DigiDialog } from '../dist/types/components/_dialog/dialog/dialog';
export { ExpandableAccordion as DigiExpandableAccordion } from '../dist/types/__core/_expandable/expandable-accordion/expandable-accordion';
export { FormCheckbox as DigiFormCheckbox } from '../dist/types/__core/_form/form-checkbox/form-checkbox';
export { FormFieldset as DigiFormFieldset } from '../dist/types/__core/_form/form-fieldset/form-fieldset';
export { FormFileUpload as DigiFormFileUpload } from '../dist/types/__core/_form/form-file-upload/form-file-upload';
export { formFilter as DigiFormFilter } from '../dist/types/__core/_form/form-filter/form-filter';
export { FormInput as DigiFormInput } from '../dist/types/__core/_form/form-input/form-input';
export { FormInputSearch as DigiFormInputSearch } from '../dist/types/__core/_form/form-input-search/form-input-search';
export { FormLabel as DigiFormLabel } from '../dist/types/__core/_form/form-label/form-label';
export { FormProcessStep as DigiFormProcessStep } from '../dist/types/components/_form/form-process-step/form-process-step';
export { FormProcessSteps as DigiFormProcessSteps } from '../dist/types/components/_form/form-process-steps/form-process-steps';
export { FormRadiobutton as DigiFormRadiobutton } from '../dist/types/__core/_form/form-radiobutton/form-radiobutton';
export { FormRadiogroup as DigiFormRadiogroup } from '../dist/types/__core/_form/form-radiogroup/form-radiogroup';
export { FormSelect as DigiFormSelect } from '../dist/types/__core/_form/form-select/form-select';
export { FormTextarea as DigiFormTextarea } from '../dist/types/__core/_form/form-textarea/form-textarea';
export { FormValidationMessage as DigiFormValidationMessage } from '../dist/types/__core/_form/form-validation-message/form-validation-message';
export { Icon as DigiIcon } from '../dist/types/__core/_icon/icon/icon';
export { IconBars as DigiIconBars } from '../dist/types/__core/_icon/icon-bars/icon-bars';
export { IconCheck as DigiIconCheck } from '../dist/types/__core/_icon/icon-check/icon-check';
export { IconCheckCircle as DigiIconCheckCircle } from '../dist/types/__core/_icon/icon-check-circle/icon-check-circle';
export { IconCheckCircleReg as DigiIconCheckCircleReg } from '../dist/types/__core/_icon/icon-check-circle-reg/icon-check-circle-reg';
export { IconCheckCircleRegAlt as DigiIconCheckCircleRegAlt } from '../dist/types/__core/_icon/icon-check-circle-reg-alt/icon-check-circle-reg-alt';
export { IconChevronDown as DigiIconChevronDown } from '../dist/types/__core/_icon/icon-chevron-down/icon-chevron-down';
export { IconChevronLeft as DigiIconChevronLeft } from '../dist/types/__core/_icon/icon-chevron-left/icon-chevron-left';
export { IconChevronRight as DigiIconChevronRight } from '../dist/types/__core/_icon/icon-chevron-right/icon-chevron-right';
export { IconChevronUp as DigiIconChevronUp } from '../dist/types/__core/_icon/icon-chevron-up/icon-chevron-up';
export { IconCopy as DigiIconCopy } from '../dist/types/__core/_icon/icon-copy/icon-copy';
export { IconDangerOutline as DigiIconDangerOutline } from '../dist/types/__core/_icon/icon-danger-outline/icon-danger-outline';
export { IconDownload as DigiIconDownload } from '../dist/types/__core/_icon/icon-download/icon-download';
export { IconExclamationCircle as DigiIconExclamationCircle } from '../dist/types/__core/_icon/icon-exclamation-circle/icon-exclamation-circle';
export { IconExclamationCircleFilled as DigiIconExclamationCircleFilled } from '../dist/types/__core/_icon/icon-exclamation-circle-filled/icon-exclamation-circle-filled';
export { IconExclamationTriangle as DigiIconExclamationTriangle } from '../dist/types/__core/_icon/icon-exclamation-triangle/icon-exclamation-triangle';
export { IconExclamationTriangleWarning as DigiIconExclamationTriangleWarning } from '../dist/types/__core/_icon/icon-exclamation-triangle-warning/icon-exclamation-triangle-warning';
export { IconExternalLinkAlt as DigiIconExternalLinkAlt } from '../dist/types/__core/_icon/icon-external-link-alt/icon-external-link-alt';
export { IconMinus as DigiIconMinus } from '../dist/types/__core/_icon/icon-minus/icon-minus';
export { IconPaperclip as DigiIconPaperclip } from '../dist/types/__core/_icon/icon-paperclip/icon-paperclip';
export { IconPlus as DigiIconPlus } from '../dist/types/__core/_icon/icon-plus/icon-plus';
export { IconSearch as DigiIconSearch } from '../dist/types/__core/_icon/icon-search/icon-search';
export { IconSpinner as DigiIconSpinner } from '../dist/types/__core/_icon/icon-spinner/icon-spinner';
export { Icontrash as DigiIconTrash } from '../dist/types/__core/_icon/icon-trash/icon-trash';
export { IconX as DigiIconX } from '../dist/types/__core/_icon/icon-x/icon-x';
export { LayoutBlock as DigiLayoutBlock } from '../dist/types/__core/_layout/layout-block/layout-block';
export { LayoutColumns as DigiLayoutColumns } from '../dist/types/__core/_layout/layout-columns/layout-columns';
export { LayoutContainer as DigiLayoutContainer } from '../dist/types/__core/_layout/layout-container/layout-container';
export { LayoutGrid as DigiLayoutGrid } from '../dist/types/components/_layout/layout-grid/layout-grid';
export { LayoutMediaObject as DigiLayoutMediaObject } from '../dist/types/__core/_layout/layout-media-object/layout-media-object';
export { LayoutRows as DigiLayoutRows } from '../dist/types/components/_layout/layout-rows/layout-rows';
export { LayoutStackedBlocks as DigiLayoutStackedBlocks } from '../dist/types/components/_layout/layout-stacked-blocks/layout-stacked-blocks';
export { Link as DigiLink } from '../dist/types/__core/_link/link/link';
export { LinkExternal as DigiLinkExternal } from '../dist/types/__core/_link/link-external/link-external';
export { LinkIcon as DigiLinkIcon } from '../dist/types/components/_link/link-icon/link-icon';
export { LinkInternal as DigiLinkInternal } from '../dist/types/__core/_link/link-internal/link-internal';
export { ListLink as DigiListLink } from '../dist/types/components/_list/list-link/list-link';
export { LoaderSpinner as DigiLoaderSpinner } from '../dist/types/__core/_loader/loader-spinner/loader-spinner';
export { Logo as DigiLogo } from '../dist/types/components/_logo/logo/logo';
export { LogoService as DigiLogoService } from '../dist/types/components/_logo/logo-service/logo-service';
export { LogoSister as DigiLogoSister } from '../dist/types/components/_logo/logo-sister/logo-sister';
export { MediaFigure as DigiMediaFigure } from '../dist/types/__core/_media/media-figure/media-figure';
export { MediaImage as DigiMediaImage } from '../dist/types/__core/_media/media-image/media-image';
export { NavigationBreadcrumbs as DigiNavigationBreadcrumbs } from '../dist/types/components/_navigation/navigation-breadcrumbs/navigation-breadcrumbs';
export { NavigationContextMenu as DigiNavigationContextMenu } from '../dist/types/__core/_navigation/navigation-context-menu/navigation-context-menu';
export { NavigationContextMenuItem as DigiNavigationContextMenuItem } from '../dist/types/__core/_navigation/navigation-context-menu-item/navigation-context-menu-item';
export { NavigationMainMenu as DigiNavigationMainMenu } from '../dist/types/components/_navigation/navigation-main-menu/navigation-main-menu';
export { NavigationMainMenuPanel as DigiNavigationMainMenuPanel } from '../dist/types/components/_navigation/navigation-main-menu-panel/navigation-main-menu-panel';
export { NavigationPagination as DigiNavigationPagination } from '../dist/types/components/_navigation/navigation-pagination/navigation-pagination';
export { NavigationSidebar as DigiNavigationSidebar } from '../dist/types/__core/_navigation/navigation-sidebar/navigation-sidebar';
export { NavigationSidebarButton as DigiNavigationSidebarButton } from '../dist/types/__core/_navigation/navigation-sidebar-button/navigation-sidebar-button';
export { NavigationTab as DigiNavigationTab } from '../dist/types/components/_navigation/navigation-tab/navigation-tab';
export { NavigationTabInABox as DigiNavigationTabInABox } from '../dist/types/components/_navigation/navigation-tab-in-a-box/navigation-tab-in-a-box';
export { NavigationTabs as DigiNavigationTabs } from '../dist/types/components/_navigation/navigation-tabs/navigation-tabs';
export { NavigationToc as DigiNavigationToc } from '../dist/types/components/_navigation/navigation-toc/navigation-toc';
export { NavigationVerticalMenu as DigiNavigationVerticalMenu } from '../dist/types/__core/_navigation/navigation-vertical-menu/navigation-vertical-menu';
export { NavigationVerticalMenuItem as DigiNavigationVerticalMenuItem } from '../dist/types/__core/_navigation/navigation-vertical-menu-item/navigation-vertical-menu-item';
export { NotificationAlert as DigiNotificationAlert } from '../dist/types/components/_notification/notification-alert/notification-alert';
export { NotificationCookie as DigiNotificationCookie } from '../dist/types/components/_notification/notification-cookie/notification-cookie';
export { NotificationDetail as DigiNotificationDetail } from '../dist/types/components/_notification/notification-detail/notification-detail';
export { Page as DigiPage } from '../dist/types/components/_page/page/page';
export { PageBlockCards as DigiPageBlockCards } from '../dist/types/components/_page/page-block-cards/page-block-cards';
export { PageBlockHero as DigiPageBlockHero } from '../dist/types/components/_page/page-block-hero/page-block-hero';
export { PageBlockLists as DigiPageBlockLists } from '../dist/types/components/_page/page-block-lists/page-block-lists';
export { PageBlockSidebar as DigiPageBlockSidebar } from '../dist/types/components/_page/page-block-sidebar/page-block-sidebar';
export { PageFooter as DigiPageFooter } from '../dist/types/components/_page/page-footer/page-footer';
export { PageHeader as DigiPageHeader } from '../dist/types/components/_page/page-header/page-header';
export { ProgressStep as DigiProgressStep } from '../dist/types/__core/_progress/progress-step/progress-step';
export { ProgressSteps as DigiProgressSteps } from '../dist/types/__core/_progress/progress-steps/progress-steps';
export { Progressbar as DigiProgressbar } from '../dist/types/__core/_progress/progressbar/progressbar';
export { Table as DigiTable } from '../dist/types/components/_table/table/table';
export { Tag as DigiTag } from '../dist/types/__core/_tag/tag/tag';
export { Typography as DigiTypography } from '../dist/types/__core/_typography/typography/typography';
export { TypographyHeadingSection as DigiTypographyHeadingSection } from '../dist/types/components/_typography/typography-heading-section/typography-heading-section';
export { TypographyMeta as DigiTypographyMeta } from '../dist/types/__core/_typography/typography-meta/typography-meta';
export { TypographyPreamble as DigiTypographyPreamble } from '../dist/types/__core/_typography/typography-preamble/typography-preamble';
export { TypographyTime as DigiTypographyTime } from '../dist/types/__core/_typography/typography-time/typography-time';
export { UtilBreakpointObserver as DigiUtilBreakpointObserver } from '../dist/types/__core/_util/util-breakpoint-observer/util-breakpoint-observer';
export { UtilDetectClickOutside as DigiUtilDetectClickOutside } from '../dist/types/__core/_util/util-detect-click-outside/util-detect-click-outside';
export { UtilDetectFocusOutside as DigiUtilDetectFocusOutside } from '../dist/types/__core/_util/util-detect-focus-outside/util-detect-focus-outside';
export { UtilIntersectionObserver as DigiUtilIntersectionObserver } from '../dist/types/__core/_util/util-intersection-observer/util-intersection-observer';
export { UtilKeydownHandler as DigiUtilKeydownHandler } from '../dist/types/__core/_util/util-keydown-handler/util-keydown-handler';
export { UtilKeyupHandler as DigiUtilKeyupHandler } from '../dist/types/__core/_util/util-keyup-handler/util-keyup-handler';
export { UtilMutationObserver as DigiUtilMutationObserver } from '../dist/types/__core/_util/util-mutation-observer/util-mutation-observer';
export { UtilResizeObserver as DigiUtilResizeObserver } from '../dist/types/__core/_util/util-resize-observer/util-resize-observer';

/**
 * Used to manually set the base path where assets can be found.
 * If the script is used as "module", it's recommended to use "import.meta.url",
 * such as "setAssetPath(import.meta.url)". Other options include
 * "setAssetPath(document.currentScript.src)", or using a bundler's replace plugin to
 * dynamically set the path at build time, such as "setAssetPath(process.env.ASSET_PATH)".
 * But do note that this configuration depends on how your script is bundled, or lack of
 * bundling, and where your assets can be loaded from. Additionally custom bundling
 * will have to ensure the static assets are copied to its build directory.
 */
export declare const setAssetPath: (path: string) => void;

export interface SetPlatformOptions {
  raf?: (c: FrameRequestCallback) => number;
  ael?: (el: EventTarget, eventName: string, listener: EventListenerOrEventListenerObject, options: boolean | AddEventListenerOptions) => void;
  rel?: (el: EventTarget, eventName: string, listener: EventListenerOrEventListenerObject, options: boolean | AddEventListenerOptions) => void;
}
export declare const setPlatformOptions: (opts: SetPlatformOptions) => void;
export * from '../dist/types';
