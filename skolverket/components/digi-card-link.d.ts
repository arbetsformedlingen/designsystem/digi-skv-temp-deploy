import type { Components, JSX } from "../dist/types/components";

interface DigiCardLink extends Components.DigiCardLink, HTMLElement {}
export const DigiCardLink: {
  prototype: DigiCardLink;
  new (): DigiCardLink;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
