import type { Components, JSX } from "../dist/types/components";

interface DigiMediaFigure extends Components.DigiMediaFigure, HTMLElement {}
export const DigiMediaFigure: {
  prototype: DigiMediaFigure;
  new (): DigiMediaFigure;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
