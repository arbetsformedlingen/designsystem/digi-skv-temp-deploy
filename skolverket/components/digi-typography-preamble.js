import { proxyCustomElement, HTMLElement, h } from '@stencil/core/internal/client';

const typographyPreambleCss = ".sc-digi-typography-preamble-h .digi-typography-preamble.sc-digi-typography-preamble{font-family:var(--digi--global--typography--font-family--default);font-size:var(--digi--typography--preamble--font-size--mobile);font-weight:var(--digi--typography--preamble--font-weight--desktop);line-height:var(--digi--typography--preamble--line-height--mobile);color:var(--digi--color--text--primary);max-width:var(--digi--paragraph-width--medium)}@media (min-width: 62rem){.sc-digi-typography-preamble-h .digi-typography-preamble.sc-digi-typography-preamble{font-size:var(--digi--typography--preamble--font-size--desktop);line-height:var(--digi--typography--preamble--line-height--desktop)}}";

const TypographyPreamble = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
  }
  render() {
    return (h("p", { class: "digi-typography-preamble" }, h("slot", null)));
  }
  static get style() { return typographyPreambleCss; }
}, [6, "digi-typography-preamble"]);
function defineCustomElement$1() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-typography-preamble"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-typography-preamble":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, TypographyPreamble);
      }
      break;
  } });
}
defineCustomElement$1();

const DigiTypographyPreamble = TypographyPreamble;
const defineCustomElement = defineCustomElement$1;

export { DigiTypographyPreamble, defineCustomElement };
