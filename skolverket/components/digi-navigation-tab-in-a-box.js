import { proxyCustomElement, HTMLElement, createEvent, h } from '@stencil/core/internal/client';
import { a as CardBoxWidth, d as defineCustomElement$4 } from './card-box.js';
import './page-background.enum.js';
import './layout-grid-vertical-spacing.enum.js';
import './layout-stacked-blocks-variation.enum.js';
import './list-link-variation.enum.js';
import './navigation-breadcrumbs-variation.enum.js';
import './notification-alert-variation.enum.js';
import './notification-detail-variation.enum.js';
import './page-footer-variation.enum.js';
import './table-variation.enum.js';
import { d as defineCustomElement$3 } from './navigation-tab.js';
import { d as defineCustomElement$2 } from './typography.js';

const slugify = (str) => str
  .toLowerCase()
  .trim()
  .replace(/å/g, 'a')
  .replace(/ä/g, 'a')
  .replace(/ö/g, 'o')
  .replace(/[^\w\s-]/g, '')
  .replace(/[\s_-]+/g, '-')
  .replace(/^-+|-+$/g, '');

const navigationTabInABoxCss = ".sc-digi-navigation-tab-in-a-box-h{display:contents}";

const NavigationTabInABox = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afOnToggle = createEvent(this, "afOnToggle", 7);
    this.afAriaLabel = undefined;
    this.afId = undefined;
    this.afActive = undefined;
  }
  toggleHandler(activeTab) {
    if (activeTab || null) {
      this.afOnToggle.emit(activeTab);
    }
  }
  render() {
    return (h("digi-navigation-tab", { afAriaLabel: this.afAriaLabel, afId: this.afId || slugify(this.afAriaLabel), afActive: this.afActive, onAfOnToggle: (e) => this.afOnToggle.emit(e.detail) }, h("digi-card-box", { afWidth: CardBoxWidth.FULL }, h("digi-typography", null, h("slot", null)))));
  }
  static get watchers() { return {
    "afActive": ["toggleHandler"]
  }; }
  static get style() { return navigationTabInABoxCss; }
}, [6, "digi-navigation-tab-in-a-box", {
    "afAriaLabel": [1, "af-aria-label"],
    "afId": [1, "af-id"],
    "afActive": [4, "af-active"]
  }]);
function defineCustomElement$1() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-navigation-tab-in-a-box", "digi-card-box", "digi-navigation-tab", "digi-typography"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-navigation-tab-in-a-box":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, NavigationTabInABox);
      }
      break;
    case "digi-card-box":
      if (!customElements.get(tagName)) {
        defineCustomElement$4();
      }
      break;
    case "digi-navigation-tab":
      if (!customElements.get(tagName)) {
        defineCustomElement$3();
      }
      break;
    case "digi-typography":
      if (!customElements.get(tagName)) {
        defineCustomElement$2();
      }
      break;
  } });
}
defineCustomElement$1();

const DigiNavigationTabInABox = NavigationTabInABox;
const defineCustomElement = defineCustomElement$1;

export { DigiNavigationTabInABox, defineCustomElement };
