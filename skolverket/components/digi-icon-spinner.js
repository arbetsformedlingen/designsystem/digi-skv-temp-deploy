import { I as IconSpinner, d as defineCustomElement$1 } from './icon-spinner.js';

const DigiIconSpinner = IconSpinner;
const defineCustomElement = defineCustomElement$1;

export { DigiIconSpinner, defineCustomElement };
