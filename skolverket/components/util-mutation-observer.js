import { proxyCustomElement, HTMLElement, createEvent, h } from '@stencil/core/internal/client';
import { r as randomIdGenerator } from './randomIdGenerator.util.js';

const UtilMutationObserver = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afOnChange = createEvent(this, "afOnChange", 7);
    this._observer = null;
    this._isObserving = false;
    this.afId = randomIdGenerator('digi-util-mutation-observer');
    this.afOptions = {
      attributes: false,
      childList: true,
      subtree: false
    };
  }
  afOptionsWatcher(newValue) {
    this._afOptions =
      typeof newValue === 'string' ? JSON.parse(newValue) : newValue;
  }
  componentWillLoad() {
    this.afOptionsWatcher(this.afOptions);
    this.initObserver();
  }
  disconnectedCallback() {
    if (this._isObserving) {
      this.removeObserver();
    }
  }
  initObserver() {
    this._observer = new MutationObserver((mutationsList) => {
      for (const mutation of mutationsList) {
        if (mutation.type === 'childList' || mutation.type === 'attributes') {
          this.changeHandler(mutation);
        }
      }
    });
    this._observer.observe(this.$el, this._afOptions);
    this._isObserving = true;
  }
  removeObserver() {
    this._observer.disconnect();
    this._isObserving = false;
  }
  changeHandler(e) {
    this.afOnChange.emit(e);
  }
  render() {
    h("div", { id: this.afId, ref: (el) => (this.$el = el) }, h("slot", null));
  }
  get $el() { return this; }
  static get watchers() { return {
    "afOptions": ["afOptionsWatcher"]
  }; }
}, [6, "digi-util-mutation-observer", {
    "afId": [1, "af-id"],
    "afOptions": [1, "af-options"]
  }]);
function defineCustomElement() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-util-mutation-observer"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-util-mutation-observer":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, UtilMutationObserver);
      }
      break;
  } });
}
defineCustomElement();

export { UtilMutationObserver as U, defineCustomElement as d };
