import { proxyCustomElement, HTMLElement, h } from '@stencil/core/internal/client';
import { a as CodeBlockVariation, C as CodeBlockLanguage } from './code-block-variation.enum.js';
import { p as prism } from './prism-git.js';
import { b as ButtonVariation, B as ButtonSize } from './button-variation.enum.js';
import { d as defineCustomElement$2 } from './button.js';
import { d as defineCustomElement$1 } from './icon.js';

const codeBlockCss = "digi-code-block{--digi--code-block--background:var(--digi--color--background--primary);--digi--code-block--border-radius:0.3rem;--digi--code-block--border:1px solid var(--digi--color--border--neutral-3);display:block}digi-code-block .digi-code-block{border-radius:var(--digi--code-block--border-radius);border:var(--digi--code-block--border);overflow:hidden;position:relative}digi-code-block .digi-code-block pre{margin:0}digi-code-block .digi-code-block--light{}digi-code-block .digi-code-block--light code[class*=language-],digi-code-block .digi-code-block--light pre[class*=language-]{color:#545454;background:none;font-family:var(--digi--global--typography--font-family--mono);text-align:left;white-space:pre-wrap;word-spacing:normal;word-break:normal;word-wrap:normal;line-height:1.5;-moz-tab-size:4;-o-tab-size:4;tab-size:4;-webkit-hyphens:none;hyphens:none}digi-code-block .digi-code-block--light pre[class*=language-]{padding:1em;overflow:auto}digi-code-block .digi-code-block--light :not(pre)>code[class*=language-],digi-code-block .digi-code-block--light pre[class*=language-]{background:#fefefe}digi-code-block .digi-code-block--light :not(pre)>code[class*=language-]{padding:0.1em;white-space:normal}digi-code-block .digi-code-block--light .token.comment,digi-code-block .digi-code-block--light .token.prolog,digi-code-block .digi-code-block--light .token.doctype,digi-code-block .digi-code-block--light .token.cdata{color:#696969}digi-code-block .digi-code-block--light .token.punctuation{color:#545454}digi-code-block .digi-code-block--light .token.property,digi-code-block .digi-code-block--light .token.tag,digi-code-block .digi-code-block--light .token.constant,digi-code-block .digi-code-block--light .token.symbol,digi-code-block .digi-code-block--light .token.deleted{color:#007faa}digi-code-block .digi-code-block--light .token.boolean,digi-code-block .digi-code-block--light .token.number{color:#008000}digi-code-block .digi-code-block--light .token.selector,digi-code-block .digi-code-block--light .token.attr-name,digi-code-block .digi-code-block--light .token.string,digi-code-block .digi-code-block--light .token.char,digi-code-block .digi-code-block--light .token.builtin,digi-code-block .digi-code-block--light .token.inserted{color:#aa5d00}digi-code-block .digi-code-block--light .token.operator,digi-code-block .digi-code-block--light .token.entity,digi-code-block .digi-code-block--light .token.url,digi-code-block .digi-code-block--light .language-css .token.string,digi-code-block .digi-code-block--light .style .token.string,digi-code-block .digi-code-block--light .token.variable{color:#008000}digi-code-block .digi-code-block--light .token.atrule,digi-code-block .digi-code-block--light .token.attr-value,digi-code-block .digi-code-block--light .token.function{color:#aa5d00}digi-code-block .digi-code-block--light .token.keyword{color:#d91e18}digi-code-block .digi-code-block--light .token.regex,digi-code-block .digi-code-block--light .token.important{color:#d91e18}digi-code-block .digi-code-block--light .token.important,digi-code-block .digi-code-block--light .token.bold{font-weight:bold}digi-code-block .digi-code-block--light .token.italic{font-style:italic}digi-code-block .digi-code-block--light .token.entity{cursor:help}@media screen and (-ms-high-contrast: active){digi-code-block .digi-code-block--light code[class*=language-],digi-code-block .digi-code-block--light pre[class*=language-]{color:windowText;background:window}digi-code-block .digi-code-block--light :not(pre)>code[class*=language-],digi-code-block .digi-code-block--light pre[class*=language-]{background:window}digi-code-block .digi-code-block--light .token.important{background:highlight;color:window;font-weight:normal}digi-code-block .digi-code-block--light .token.atrule,digi-code-block .digi-code-block--light .token.attr-value,digi-code-block .digi-code-block--light .token.function,digi-code-block .digi-code-block--light .token.keyword,digi-code-block .digi-code-block--light .token.operator,digi-code-block .digi-code-block--light .token.selector{font-weight:bold}digi-code-block .digi-code-block--light .token.attr-value,digi-code-block .digi-code-block--light .token.comment,digi-code-block .digi-code-block--light .token.doctype,digi-code-block .digi-code-block--light .token.function,digi-code-block .digi-code-block--light .token.keyword,digi-code-block .digi-code-block--light .token.operator,digi-code-block .digi-code-block--light .token.property,digi-code-block .digi-code-block--light .token.string{color:highlight}digi-code-block .digi-code-block--light .token.attr-value,digi-code-block .digi-code-block--light .token.url{font-weight:normal}}digi-code-block .digi-code-block--dark{}digi-code-block .digi-code-block--dark .digi-code-block__button{--digi--button--color--text--function--default:var(--digi--color--text--inverted);--digi--button--color--text--function--hover:var(--digi--color--text--inverted)}digi-code-block .digi-code-block--dark code[class*=language-],digi-code-block .digi-code-block--dark pre[class*=language-]{color:#f8f8f2;background:none;font-family:var(--digi--global--typography--font-family--mono);text-align:left;white-space:pre-wrap;word-spacing:normal;word-break:normal;word-wrap:normal;line-height:1.5;-moz-tab-size:4;-o-tab-size:4;tab-size:4;-webkit-hyphens:none;hyphens:none}digi-code-block .digi-code-block--dark pre[class*=language-]{padding:1em;overflow:auto}digi-code-block .digi-code-block--dark :not(pre)>code[class*=language-],digi-code-block .digi-code-block--dark pre[class*=language-]{--digi--color--background--inverted-3:#272727;background:var(--digi--color--background--inverted-3)}digi-code-block .digi-code-block--dark :not(pre)>code[class*=language-]{padding:0.1em;white-space:normal}digi-code-block .digi-code-block--dark .token.comment,digi-code-block .digi-code-block--dark .token.prolog,digi-code-block .digi-code-block--dark .token.doctype,digi-code-block .digi-code-block--dark .token.cdata{color:#d4d0ab}digi-code-block .digi-code-block--dark .token.punctuation{color:#fefefe}digi-code-block .digi-code-block--dark .token.property,digi-code-block .digi-code-block--dark .token.tag,digi-code-block .digi-code-block--dark .token.constant,digi-code-block .digi-code-block--dark .token.symbol,digi-code-block .digi-code-block--dark .token.deleted{color:#ffa07a}digi-code-block .digi-code-block--dark .token.boolean,digi-code-block .digi-code-block--dark .token.number{color:#00e0e0}digi-code-block .digi-code-block--dark .token.selector,digi-code-block .digi-code-block--dark .token.attr-name,digi-code-block .digi-code-block--dark .token.string,digi-code-block .digi-code-block--dark .token.char,digi-code-block .digi-code-block--dark .token.builtin,digi-code-block .digi-code-block--dark .token.inserted{color:#abe338}digi-code-block .digi-code-block--dark .token.operator,digi-code-block .digi-code-block--dark .token.entity,digi-code-block .digi-code-block--dark .token.url,digi-code-block .digi-code-block--dark .language-css .token.string,digi-code-block .digi-code-block--dark .style .token.string,digi-code-block .digi-code-block--dark .token.variable{color:#00e0e0}digi-code-block .digi-code-block--dark .token.atrule,digi-code-block .digi-code-block--dark .token.attr-value,digi-code-block .digi-code-block--dark .token.function{color:#ffd700}digi-code-block .digi-code-block--dark .token.keyword{color:#00e0e0}digi-code-block .digi-code-block--dark .token.regex,digi-code-block .digi-code-block--dark .token.important{color:#ffd700}digi-code-block .digi-code-block--dark .token.important,digi-code-block .digi-code-block--dark .token.bold{font-weight:bold}digi-code-block .digi-code-block--dark .token.italic{font-style:italic}digi-code-block .digi-code-block--dark .token.entity{cursor:help}@media screen and (-ms-high-contrast: active){digi-code-block .digi-code-block--dark code[class*=language-],digi-code-block .digi-code-block--dark pre[class*=language-]{color:windowText;background:window}digi-code-block .digi-code-block--dark :not(pre)>code[class*=language-],digi-code-block .digi-code-block--dark pre[class*=language-]{background:window}digi-code-block .digi-code-block--dark .token.important{background:highlight;color:window;font-weight:normal}digi-code-block .digi-code-block--dark .token.atrule,digi-code-block .digi-code-block--dark .token.attr-value,digi-code-block .digi-code-block--dark .token.function,digi-code-block .digi-code-block--dark .token.keyword,digi-code-block .digi-code-block--dark .token.operator,digi-code-block .digi-code-block--dark .token.selector{font-weight:bold}digi-code-block .digi-code-block--dark .token.attr-value,digi-code-block .digi-code-block--dark .token.comment,digi-code-block .digi-code-block--dark .token.doctype,digi-code-block .digi-code-block--dark .token.function,digi-code-block .digi-code-block--dark .token.keyword,digi-code-block .digi-code-block--dark .token.operator,digi-code-block .digi-code-block--dark .token.property,digi-code-block .digi-code-block--dark .token.string{color:highlight}digi-code-block .digi-code-block--dark .token.attr-value,digi-code-block .digi-code-block--dark .token.url{font-weight:normal}}digi-code-block .digi-code-block__toolbar{padding-bottom:0.5rem;display:flex;justify-content:flex-end;position:absolute;bottom:0;width:100%}digi-code-block .digi-code-block__button{margin-right:var(--digi--margin--smaller)}";

const CodeBlock = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.highlightedCode = undefined;
    this.afCode = undefined;
    this.afVariation = CodeBlockVariation.DARK;
    this.afLanguage = CodeBlockLanguage.HTML;
    this.afLang = 'en';
    this.afHideToolbar = false;
  }
  componentWillLoad() {
    prism.manual = true;
    this.formatCode();
  }
  formatCode() {
    this.highlightedCode = prism.highlight(this.afCode, prism.languages[this.afLanguage], this.afLanguage);
  }
  copyButtonClickHandler() {
    navigator.clipboard.writeText(this.afCode).then(() => {
      // console.log('hurra!');
    }, (err) => {
      // console.log('åh nej!', err);
      console.error(err);
    });
  }
  get cssModifiers() {
    return {
      'digi-code-block--light': this.afVariation === CodeBlockVariation.LIGHT,
      'digi-code-block--dark': this.afVariation === CodeBlockVariation.DARK
    };
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-code-block': true }, this.cssModifiers) }, h("pre", { class: `digi-code-block__pre language-${this.afLanguage}` }, h("code", { class: "digi-code-block__code", lang: this.afLang, innerHTML: this.highlightedCode })), !this.afHideToolbar && (h("div", { class: "digi-code-block__toolbar" }, h("digi-button", { class: "digi-code-block__button", onAfOnClick: () => this.copyButtonClickHandler(), afVariation: ButtonVariation.FUNCTION, afSize: ButtonSize.SMALL }, h("digi-icon", { class: "digi-code-block__icon", "aria-hidden": "true", afName: `copy`, slot: "icon" }), "Kopiera kod")))));
  }
  static get watchers() { return {
    "afCode": ["formatCode"]
  }; }
  static get style() { return codeBlockCss; }
}, [0, "digi-code-block", {
    "afCode": [1, "af-code"],
    "afVariation": [1, "af-variation"],
    "afLanguage": [1, "af-language"],
    "afLang": [1, "af-lang"],
    "afHideToolbar": [4, "af-hide-toolbar"],
    "highlightedCode": [32]
  }]);
function defineCustomElement() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-code-block", "digi-button", "digi-icon"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-code-block":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, CodeBlock);
      }
      break;
    case "digi-button":
      if (!customElements.get(tagName)) {
        defineCustomElement$2();
      }
      break;
    case "digi-icon":
      if (!customElements.get(tagName)) {
        defineCustomElement$1();
      }
      break;
  } });
}
defineCustomElement();

export { CodeBlock as C, defineCustomElement as d };
