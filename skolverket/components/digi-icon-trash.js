import { I as Icontrash, d as defineCustomElement$1 } from './icon-trash.js';

const DigiIconTrash = Icontrash;
const defineCustomElement = defineCustomElement$1;

export { DigiIconTrash, defineCustomElement };
