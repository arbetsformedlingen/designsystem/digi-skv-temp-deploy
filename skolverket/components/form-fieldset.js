import { proxyCustomElement, HTMLElement, h } from '@stencil/core/internal/client';
import { r as randomIdGenerator } from './randomIdGenerator.util.js';

const formFieldsetCss = ".sc-digi-form-fieldset-h{--digi--form-fieldset--padding:0;--digi--form-fieldset--border:none;--digi--form-fieldset--legend--font-weight:var(--digi--typography--description--font-weight--desktop);--digi--form-fieldset--legend--font-family:var(--digi--global--typography--font-family--default);--digi--form-fieldset--legend--font-size:var(--digi--typography--description--font-size--desktop);--digi--form-fieldset--legend--color:var(--digi--color--text--primary);--digi--form-fieldset--legend--margin:var(--digi--margin--h2-large)}.sc-digi-form-fieldset-h .digi-form-fieldset.sc-digi-form-fieldset{padding:var(--digi--form-fieldset--padding);border:var(--digi--form-fieldset--border)}.sc-digi-form-fieldset-h .digi-form-fieldset__legend.sc-digi-form-fieldset{font-weight:var(--digi--form-fieldset--legend--font-weight);font-family:var(--digi--form-fieldset--legend--font-family);font-size:var(--digi--form-fieldset--legend--font-size);color:var(--digi--form-fieldset--legend--color);-webkit-margin-after:var(--digi--form-fieldset--legend--margin);margin-block-end:var(--digi--form-fieldset--legend--margin)}";

const FormFieldset = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afLegend = undefined;
    this.afName = undefined;
    this.afForm = undefined;
    this.afId = randomIdGenerator('digi-form-fieldset');
  }
  render() {
    return (h("fieldset", { class: "digi-form-fieldset", name: this.afName, form: this.afForm, id: this.afId }, this.afLegend && (h("legend", { class: "digi-form-fieldset__legend" }, this.afLegend)), h("slot", null)));
  }
  static get style() { return formFieldsetCss; }
}, [6, "digi-form-fieldset", {
    "afLegend": [1, "af-legend"],
    "afName": [1, "af-name"],
    "afForm": [1, "af-form"],
    "afId": [1, "af-id"]
  }]);
function defineCustomElement() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-form-fieldset"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-form-fieldset":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, FormFieldset);
      }
      break;
  } });
}
defineCustomElement();

export { FormFieldset as F, defineCustomElement as d };
