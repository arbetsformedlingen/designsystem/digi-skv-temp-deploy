import type { Components, JSX } from "../dist/types/components";

interface DigiPageFooter extends Components.DigiPageFooter, HTMLElement {}
export const DigiPageFooter: {
  prototype: DigiPageFooter;
  new (): DigiPageFooter;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
