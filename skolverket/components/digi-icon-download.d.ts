import type { Components, JSX } from "../dist/types/components";

interface DigiIconDownload extends Components.DigiIconDownload, HTMLElement {}
export const DigiIconDownload: {
  prototype: DigiIconDownload;
  new (): DigiIconDownload;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
