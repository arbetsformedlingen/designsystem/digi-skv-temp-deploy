import { proxyCustomElement, HTMLElement, createEvent, h } from '@stencil/core/internal/client';

const formProcessStepCss = ".sc-digi-form-process-step-h{--digi-form-process-step--indicator--background:var(--digi--color--background--primary);--digi-form-process-step--indicator--border-color:var(--digi--global--color--profile--purple--opacity15);--digi-form-process-step--indicator--color:var(--digi--global--color--profile--purple--opacity60);--digi-form-process-step--label--color:var(--digi--global--color--neutral--grayscale--darker-2);--digi-form-process-step--indicator--size:var(--digi--global--spacing--larger)}.digi-form-process-step.sc-digi-form-process-step{counter-increment:steps}.digi-form-process-step--type-completed.sc-digi-form-process-step{--digi-form-process-step--indicator--background:var(--digi--color--background--secondary);--digi-form-process-step--indicator--border-color:var(--digi--color--border--primary);--digi-form-process-step--indicator--color:var(--digi--color--text--secondary);--digi-form-process-step--label--color:var(--digi--global--color--neutral--grayscale--darker-3);--digi-form-process-step--hover-background:var(--digi--color--background--secondary)}.digi-form-process-step--type-current.sc-digi-form-process-step{--digi-form-process-step--indicator--background:var(--digi--color--background--inverted-1);--digi-form-process-step--indicator--border-color:var(--digi--color--border--primary);--digi-form-process-step--indicator--color:var(--digi--color--text--inverted);--digi-form-process-step--label--color:var(--digi--global--color--neutral--grayscale--darker-3)}.digi-form-process-step--context-fallback.sc-digi-form-process-step{--digi-form-process-step--indicator--size:var(--digi--global--spacing--small-2);-webkit-border-after:var(--digi--border-width--tertiary) var(--digi--border-style--primary) var(--digi--global--color--profile--purple--opacity15);border-block-end:var(--digi--border-width--tertiary) var(--digi--border-style--primary) var(--digi--global--color--profile--purple--opacity15);padding:var(--digi--gutter--smallest) 0}.digi-form-process-step--context-fallback.sc-digi-form-process-step:hover{background:var(--digi-form-process-step--hover-background)}.digi-form-process-step__control.sc-digi-form-process-step{font-family:var(--digi--global--typography--font-family--default);text-decoration:none;color:var(--digi--color--text--secondary);font-size:var(--digi--global--typography--font-size--large);letter-spacing:calc(var(--digi--global--typography--font-size--large) / 100 * -1);font-weight:var(--digi--global--typography--font-weight--semibold);width:100%;font-size:var(--digi--global--typography--font-size--small);display:grid;grid-template-columns:auto 1fr;grid-auto-flow:columns;gap:var(--digi--gutter--smallest-4);align-items:center;text-align:start}.digi-form-process-step__control.sc-digi-form-process-step:hover{color:var(--digi--global--color--profile--purple--dark);text-decoration:underline}.digi-form-process-step__control.sc-digi-form-process-step:focus{outline:none;color:var(--digi--color--text--secondary)}.digi-form-process-step__control.sc-digi-form-process-step:focus-visible{outline:var(--digi--border-width--secondary) solid var(--digi--color--border--focus)}.digi-form-process-step__control.sc-digi-form-process-step:visited{color:var(--digi--color--text--secondary)}.digi-form-process-step__control.sc-digi-form-process-step:hover{text-decoration:none}.digi-form-process-step--type-current.sc-digi-form-process-step .digi-form-process-step__control.sc-digi-form-process-step,.digi-form-process-step--type-upcoming.sc-digi-form-process-step .digi-form-process-step__control.sc-digi-form-process-step{color:var(--digi-form-process-step--label--color);font-weight:var(--digi--global--typography--font-weight--regular)}.digi-form-process-step--type-current.sc-digi-form-process-step .digi-form-process-step__control.sc-digi-form-process-step:hover,.digi-form-process-step--type-upcoming.sc-digi-form-process-step .digi-form-process-step__control.sc-digi-form-process-step:hover{color:var(--digi-form-process-step--label--color)}.digi-form-process-step__control.sc-digi-form-process-step::before{content:counter(steps);width:var(--digi-form-process-step--indicator--size);height:var(--digi-form-process-step--indicator--size);border-radius:50%;display:flex;align-items:center;justify-content:center;background:var(--digi-form-process-step--indicator--background);border:1px solid var(--digi-form-process-step--indicator--border-color);color:var(--digi-form-process-step--indicator--color)}";

const FormProcessStep = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afClick = createEvent(this, "afClick", 7);
    this.afHref = undefined;
    this.afType = 'upcoming';
    this.afContext = 'regular';
    this.afLabel = undefined;
  }
  clickHandler(e) {
    this.afClick.emit(e);
  }
  get cssModifiers() {
    return {
      [`digi-form-process-step--type-${this.afType}`]: true,
      [`digi-form-process-step--context-${this.afContext}`]: true
    };
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-form-process-step': true }, this.cssModifiers) }, this.afType !== 'completed' ? (h("p", { class: "digi-form-process-step__control", "aria-current": this.afType === 'current' ? 'step' : null, "data-label": this.afLabel }, h("span", null, this.afLabel))) : this.afHref ? (h("a", { class: "digi-form-process-step__control", href: this.afHref, onClick: (e) => this.clickHandler(e) }, h("span", null, this.afLabel))) : (h("button", { class: "digi-form-process-step__control", type: "button", onClick: (e) => this.clickHandler(e) }, h("span", null, this.afLabel)))));
  }
  static get style() { return formProcessStepCss; }
}, [2, "digi-form-process-step", {
    "afHref": [1, "af-href"],
    "afType": [1, "af-type"],
    "afContext": [1, "af-context"],
    "afLabel": [1, "af-label"]
  }]);
function defineCustomElement$1() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-form-process-step"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-form-process-step":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, FormProcessStep);
      }
      break;
  } });
}
defineCustomElement$1();

const DigiFormProcessStep = FormProcessStep;
const defineCustomElement = defineCustomElement$1;

export { DigiFormProcessStep, defineCustomElement };
