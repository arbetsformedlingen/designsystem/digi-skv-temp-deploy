import type { Components, JSX } from "../dist/types/components";

interface DigiIconCheck extends Components.DigiIconCheck, HTMLElement {}
export const DigiIconCheck: {
  prototype: DigiIconCheck;
  new (): DigiIconCheck;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
