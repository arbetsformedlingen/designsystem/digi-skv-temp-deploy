import { U as UtilIntersectionObserver, d as defineCustomElement$1 } from './util-intersection-observer.js';

const DigiUtilIntersectionObserver = UtilIntersectionObserver;
const defineCustomElement = defineCustomElement$1;

export { DigiUtilIntersectionObserver, defineCustomElement };
