import type { Components, JSX } from "../dist/types/components";

interface DigiIconMinus extends Components.DigiIconMinus, HTMLElement {}
export const DigiIconMinus: {
  prototype: DigiIconMinus;
  new (): DigiIconMinus;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
