import { proxyCustomElement, HTMLElement, h, getAssetPath } from '@stencil/core/internal/client';

const pageCss = ".digi-page.sc-digi-page{display:grid;grid-template-rows:min-content 1fr min-content;min-height:100vh;background:var(--digi--page--background-image) center center;grid-template-columns:100vw}";

const Page = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afBackground = undefined;
  }
  render() {
    return (h("div", { class: "digi-page", style: {
        '--digi--page--background-image': this.afBackground
          ? `url('${getAssetPath(`./public/images/${this.afBackground}.svg`)}')`
          : null
      } }, h("div", { class: "digi-page__header" }, h("slot", { name: "header" })), h("div", { class: "digi-page__content" }, h("slot", null)), h("div", { class: "digi-page__footer" }, h("slot", { name: "footer" }))));
  }
  static get assetsDirs() { return ["public"]; }
  static get style() { return pageCss; }
}, [6, "digi-page", {
    "afBackground": [1, "af-background"]
  }]);
function defineCustomElement$1() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-page"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-page":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, Page);
      }
      break;
  } });
}
defineCustomElement$1();

const DigiPage = Page;
const defineCustomElement = defineCustomElement$1;

export { DigiPage, defineCustomElement };
