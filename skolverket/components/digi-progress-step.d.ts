import type { Components, JSX } from "../dist/types/components";

interface DigiProgressStep extends Components.DigiProgressStep, HTMLElement {}
export const DigiProgressStep: {
  prototype: DigiProgressStep;
  new (): DigiProgressStep;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
