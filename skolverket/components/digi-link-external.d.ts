import type { Components, JSX } from "../dist/types/components";

interface DigiLinkExternal extends Components.DigiLinkExternal, HTMLElement {}
export const DigiLinkExternal: {
  prototype: DigiLinkExternal;
  new (): DigiLinkExternal;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
