import type { Components, JSX } from "../dist/types/components";

interface DigiNavigationContextMenu extends Components.DigiNavigationContextMenu, HTMLElement {}
export const DigiNavigationContextMenu: {
  prototype: DigiNavigationContextMenu;
  new (): DigiNavigationContextMenu;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
