import type { Components, JSX } from "../dist/types/components";

interface DigiIconPlus extends Components.DigiIconPlus, HTMLElement {}
export const DigiIconPlus: {
  prototype: DigiIconPlus;
  new (): DigiIconPlus;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
