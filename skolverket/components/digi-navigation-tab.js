import { N as NavigationTab, d as defineCustomElement$1 } from './navigation-tab.js';

const DigiNavigationTab = NavigationTab;
const defineCustomElement = defineCustomElement$1;

export { DigiNavigationTab, defineCustomElement };
