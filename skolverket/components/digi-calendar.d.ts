import type { Components, JSX } from "../dist/types/components";

interface DigiCalendar extends Components.DigiCalendar, HTMLElement {}
export const DigiCalendar: {
  prototype: DigiCalendar;
  new (): DigiCalendar;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
