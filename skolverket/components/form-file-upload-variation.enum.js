var FormFileUploadHeadingLevel;
(function (FormFileUploadHeadingLevel) {
  FormFileUploadHeadingLevel["H1"] = "h1";
  FormFileUploadHeadingLevel["H2"] = "h2";
  FormFileUploadHeadingLevel["H3"] = "h3";
  FormFileUploadHeadingLevel["H4"] = "h4";
  FormFileUploadHeadingLevel["H5"] = "h5";
  FormFileUploadHeadingLevel["H6"] = "h6";
})(FormFileUploadHeadingLevel || (FormFileUploadHeadingLevel = {}));

var FormFileUploadValidation;
(function (FormFileUploadValidation) {
  FormFileUploadValidation["ENABLED"] = "enabled";
  FormFileUploadValidation["DISABLED"] = "disabled";
})(FormFileUploadValidation || (FormFileUploadValidation = {}));

var FormFileUploadVariation;
(function (FormFileUploadVariation) {
  FormFileUploadVariation["PRIMARY"] = "primary";
  FormFileUploadVariation["SECONDARY"] = "secondary";
  FormFileUploadVariation["TERTIARY"] = "tertiary";
})(FormFileUploadVariation || (FormFileUploadVariation = {}));

export { FormFileUploadHeadingLevel as F, FormFileUploadValidation as a, FormFileUploadVariation as b };
