import type { Components, JSX } from "../dist/types/components";

interface DigiNotificationCookie extends Components.DigiNotificationCookie, HTMLElement {}
export const DigiNotificationCookie: {
  prototype: DigiNotificationCookie;
  new (): DigiNotificationCookie;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
