import { N as NavigationMainMenu, d as defineCustomElement$1 } from './navigation-main-menu.js';

const DigiNavigationMainMenu = NavigationMainMenu;
const defineCustomElement = defineCustomElement$1;

export { DigiNavigationMainMenu, defineCustomElement };
