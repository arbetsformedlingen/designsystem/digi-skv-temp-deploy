import type { Components, JSX } from "../dist/types/components";

interface DigiIconChevronUp extends Components.DigiIconChevronUp, HTMLElement {}
export const DigiIconChevronUp: {
  prototype: DigiIconChevronUp;
  new (): DigiIconChevronUp;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
