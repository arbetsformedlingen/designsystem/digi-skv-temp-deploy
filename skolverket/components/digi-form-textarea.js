import { proxyCustomElement, HTMLElement, createEvent, h } from '@stencil/core/internal/client';
import { r as randomIdGenerator } from './randomIdGenerator.util.js';
import { a as FormTextareaVariation, F as FormTextareaValidation } from './form-textarea-variation.enum.js';
import { d as defineCustomElement$4 } from './form-label.js';
import { d as defineCustomElement$3 } from './form-validation-message.js';
import { d as defineCustomElement$2 } from './icon.js';

const formTextareaCss = ".sc-digi-form-textarea-h{--digi--form-textarea--height--small:4.6875rem;--digi--form-textarea--height--medium:8.4375rem;--digi--form-textarea--height--large:10.3125rem;--digi--form-textarea--height--auto:auto;--digi--form-textarea--border-radius:var(--digi--border-radius--input);--digi--form-textarea--padding:calc(var(--digi--gutter--medium) / 2) var(--digi--gutter--medium);--digi--form-textarea--background--empty:var(--digi--color--background--input-empty);--digi--form-textarea--background--neutral:var(--digi--color--background--input);--digi--form-textarea--background--success:var(--digi--color--background--success-2);--digi--form-textarea--background--warning:var(--digi--color--background--warning-2);--digi--form-textarea--background--error:var(--digi--color--background--danger-2);--digi--form-textarea--border--neutral:var(--digi--border-width--input-regular) solid;--digi--form-textarea--border--error:var(--digi--border-width--input-validation) solid;--digi--form-textarea--border--success:var(--digi--border-width--input-validation) solid;--digi--form-textarea--border--warning:var(--digi--border-width--input-validation) solid;--digi--form-textarea--border-color--neutral:var(--digi--color--border--neutral-3);--digi--form-textarea--border-color--success:var(--digi--color--border--success);--digi--form-textarea--border-color--warning:var(--digi--color--border--neutral-3);--digi--form-textarea--border-color--error:var(--digi--color--border--danger)}.sc-digi-form-textarea-h .digi-form-textarea.sc-digi-form-textarea{display:flex;flex-direction:column;gap:0.4em}.sc-digi-form-textarea-h .digi-form-textarea--small.sc-digi-form-textarea{--HEIGHT:var(--digi--form-textarea--height--small)}.sc-digi-form-textarea-h .digi-form-textarea--medium.sc-digi-form-textarea{--HEIGHT:var(--digi--form-textarea--height--medium)}.sc-digi-form-textarea-h .digi-form-textarea--large.sc-digi-form-textarea{--HEIGHT:var(--digi--form-textarea--height--large)}.sc-digi-form-textarea-h .digi-form-textarea--neutral.sc-digi-form-textarea{--BORDER:var(--digi--form-textarea--border--neutral);--BORDER-COLOR:var(--digi--form-textarea--border-color--neutral);--BACKGROUND:var(--digi--form-textarea--background--neutral)}.sc-digi-form-textarea-h .digi-form-textarea--empty.sc-digi-form-textarea:not(:focus-within){--BACKGROUND:var(--digi--form-textarea--background--empty)}.sc-digi-form-textarea-h .digi-form-textarea--success.sc-digi-form-textarea{--BORDER:var(--digi--form-textarea--border--success);--BORDER-COLOR:var(--digi--form-textarea--border-color--success);--BACKGROUND:var(--digi--form-textarea--background--success)}.sc-digi-form-textarea-h .digi-form-textarea--warning.sc-digi-form-textarea{--BORDER:var(--digi--form-textarea--border--warning);--BORDER-COLOR:var(--digi--form-textarea--border-color--warning);--BACKGROUND:var(--digi--form-textarea--background--warning)}.sc-digi-form-textarea-h .digi-form-textarea--error.sc-digi-form-textarea{--BORDER:var(--digi--form-textarea--border--error);--BORDER-COLOR:var(--digi--form-textarea--border-color--error);--BACKGROUND:var(--digi--form-textarea--background--error)}.sc-digi-form-textarea-h .digi-form-textarea__content.sc-digi-form-textarea{width:100%}.sc-digi-form-textarea-h .digi-form-textarea__textarea.sc-digi-form-textarea{height:var(--HEIGHT);width:inherit;font-family:var(--digi--global--typography--font-family--default);font-size:var(--digi--typography--body--font-size--desktop);padding:var(--digi--form-textarea--padding);color:var(--digi--color--text--primary);background:var(--BACKGROUND);border:var(--BORDER);border-color:var(--BORDER-COLOR);border-radius:var(--digi--form-textarea--border-radius);box-sizing:border-box}.sc-digi-form-textarea-h .digi-form-textarea__textarea.sc-digi-form-textarea:focus-visible{box-shadow:0 0 0.1rem 0.05rem var(--digi--color--border--informative);box-shadow:var(--digi--focus-shadow);outline:var(--digi--focus-outline)}";

const FormTextarea = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afOnChange = createEvent(this, "afOnChange", 7);
    this.afOnBlur = createEvent(this, "afOnBlur", 7);
    this.afOnKeyup = createEvent(this, "afOnKeyup", 7);
    this.afOnFocus = createEvent(this, "afOnFocus", 7);
    this.afOnFocusout = createEvent(this, "afOnFocusout", 7);
    this.afOnInput = createEvent(this, "afOnInput", 7);
    this.afOnDirty = createEvent(this, "afOnDirty", 7);
    this.afOnTouched = createEvent(this, "afOnTouched", 7);
    this.hasActiveValidationMessage = false;
    this.dirty = false;
    this.touched = false;
    this.afLabel = undefined;
    this.afLabelDescription = undefined;
    this.afVariation = FormTextareaVariation.MEDIUM;
    this.afName = undefined;
    this.afId = randomIdGenerator('digi-form-textarea');
    this.afMaxlength = undefined;
    this.afMinlength = undefined;
    this.afRequired = undefined;
    this.afRequiredText = undefined;
    this.afAnnounceIfOptional = false;
    this.afAnnounceIfOptionalText = undefined;
    this.value = undefined;
    this.afValue = undefined;
    this.afValidation = FormTextareaValidation.NEUTRAL;
    this.afValidationText = undefined;
    this.afRole = undefined;
    this.afAriaActivedescendant = undefined;
    this.afAriaLabelledby = undefined;
    this.afAriaDescribedby = undefined;
    this.afAutofocus = undefined;
  }
  onValueChanged(value) {
    this.afValue = value;
  }
  onAfValueChanged(value) {
    this.value = value;
  }
  afValidationTextWatch() {
    this.setActiveValidationMessage();
  }
  /**
   * Hämtar en referens till textareaelementet. Bra för att t.ex. sätta fokus programmatiskt.
   * @en Returns a reference to the textarea element. Handy for setting focus programmatically.
   */
  async afMGetFormControlElement() {
    return this._textarea;
  }
  componentWillLoad() {
    this.afValue ? (this.value = this.afValue) : (this.afValue = this.value);
    this.setActiveValidationMessage();
  }
  setActiveValidationMessage() {
    this.hasActiveValidationMessage = !!this.afValidationText;
  }
  get cssModifiers() {
    return {
      'digi-form-textarea--small': this.afVariation === FormTextareaVariation.SMALL,
      'digi-form-textarea--medium': this.afVariation === FormTextareaVariation.MEDIUM,
      'digi-form-textarea--large': this.afVariation === FormTextareaVariation.LARGE,
      'digi-form-textarea--auto': this.afVariation === FormTextareaVariation.AUTO,
      'digi-form-textarea--neutral': this.afValidation === FormTextareaValidation.NEUTRAL,
      'digi-form-textarea--success': this.afValidation === FormTextareaValidation.SUCCESS,
      'digi-form-textarea--error': this.afValidation === FormTextareaValidation.ERROR,
      'digi-form-textarea--warning': this.afValidation === FormTextareaValidation.WARNING,
      'digi-form-textarea--empty': !this.afValue &&
        (!this.afValidation || this.afValidation === FormTextareaValidation.NEUTRAL)
    };
  }
  blurHandler(e) {
    if (!this.touched) {
      this.afOnTouched.emit(e);
      this.touched = true;
    }
    this.setActiveValidationMessage();
    this.afOnBlur.emit(e);
  }
  changeHandler(e) {
    this.afOnChange.emit(e);
  }
  focusHandler(e) {
    this.afOnFocus.emit(e);
  }
  focusoutHandler(e) {
    this.afOnFocusout.emit(e);
  }
  keyupHandler(e) {
    this.afOnKeyup.emit(e);
  }
  inputHandler(e) {
    this.afValue = this.value = e.target.value;
    if (!this.dirty) {
      this.afOnDirty.emit(e);
      this.dirty = true;
    }
    this.setActiveValidationMessage();
    this.afOnInput.emit(e);
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-form-textarea': true }, this.cssModifiers) }, h("div", { class: "digi-form-textarea__label" }, h("digi-form-label", { afFor: this.afId, afLabel: this.afLabel, afDescription: this.afLabelDescription, afRequired: this.afRequired, afAnnounceIfOptional: this.afAnnounceIfOptional, afRequiredText: this.afRequiredText, afAnnounceIfOptionalText: this.afAnnounceIfOptionalText })), h("div", { class: "digi-form-textarea__content" }, h("textarea", { class: "digi-form-textarea__textarea", ref: (el) => (this._textarea = el), onBlur: (e) => this.blurHandler(e), onChange: (e) => this.changeHandler(e), onFocus: (e) => this.focusHandler(e), onFocusout: (e) => this.focusoutHandler(e), onKeyUp: (e) => this.keyupHandler(e), onInput: (e) => this.inputHandler(e), "aria-activedescendant": this.afAriaActivedescendant, "aria-describedby": this.afAriaDescribedby, "aria-labelledby": this.afAriaLabelledby, "aria-invalid": this.afValidation != FormTextareaValidation.NEUTRAL ? 'true' : 'false', maxLength: this.afMaxlength, minLength: this.afMinlength, role: this.afRole, required: this.afRequired, id: this.afId, name: this.afName, value: this.afValue, autofocus: this.afAutofocus ? this.afAutofocus : null })), h("div", { class: "digi-form-textarea__footer" }, h("div", { class: "digi-form-textarea__validation", "aria-atomic": "true", role: "alert", id: `${this.afId}--validation-message` }, this.hasActiveValidationMessage &&
      this.afValidation != FormTextareaValidation.NEUTRAL && (h("digi-form-validation-message", { class: "digi-form-textarea__validation-message", "af-variation": this.afValidation }, this.afValidationText))))));
  }
  get hostElement() { return this; }
  static get watchers() { return {
    "value": ["onValueChanged"],
    "afValue": ["onAfValueChanged"],
    "afValidationText": ["afValidationTextWatch"]
  }; }
  static get style() { return formTextareaCss; }
}, [2, "digi-form-textarea", {
    "afLabel": [1, "af-label"],
    "afLabelDescription": [1, "af-label-description"],
    "afVariation": [1, "af-variation"],
    "afName": [1, "af-name"],
    "afId": [1, "af-id"],
    "afMaxlength": [2, "af-maxlength"],
    "afMinlength": [2, "af-minlength"],
    "afRequired": [4, "af-required"],
    "afRequiredText": [1, "af-required-text"],
    "afAnnounceIfOptional": [4, "af-announce-if-optional"],
    "afAnnounceIfOptionalText": [1, "af-announce-if-optional-text"],
    "value": [1],
    "afValue": [1, "af-value"],
    "afValidation": [1, "af-validation"],
    "afValidationText": [1, "af-validation-text"],
    "afRole": [1, "af-role"],
    "afAriaActivedescendant": [1, "af-aria-activedescendant"],
    "afAriaLabelledby": [1, "af-aria-labelledby"],
    "afAriaDescribedby": [1, "af-aria-describedby"],
    "afAutofocus": [4, "af-autofocus"],
    "hasActiveValidationMessage": [32],
    "dirty": [32],
    "touched": [32],
    "afMGetFormControlElement": [64]
  }]);
function defineCustomElement$1() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-form-textarea", "digi-form-label", "digi-form-validation-message", "digi-icon"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-form-textarea":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, FormTextarea);
      }
      break;
    case "digi-form-label":
      if (!customElements.get(tagName)) {
        defineCustomElement$4();
      }
      break;
    case "digi-form-validation-message":
      if (!customElements.get(tagName)) {
        defineCustomElement$3();
      }
      break;
    case "digi-icon":
      if (!customElements.get(tagName)) {
        defineCustomElement$2();
      }
      break;
  } });
}
defineCustomElement$1();

const DigiFormTextarea = FormTextarea;
const defineCustomElement = defineCustomElement$1;

export { DigiFormTextarea, defineCustomElement };
