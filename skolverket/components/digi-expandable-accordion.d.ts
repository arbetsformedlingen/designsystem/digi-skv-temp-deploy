import type { Components, JSX } from "../dist/types/components";

interface DigiExpandableAccordion extends Components.DigiExpandableAccordion, HTMLElement {}
export const DigiExpandableAccordion: {
  prototype: DigiExpandableAccordion;
  new (): DigiExpandableAccordion;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
