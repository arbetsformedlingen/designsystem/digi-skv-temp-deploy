import { proxyCustomElement, HTMLElement, h } from '@stencil/core/internal/client';
import { d as defineCustomElement$3 } from './layout-container.js';
import { d as defineCustomElement$2 } from './typography.js';

const pageBlockHeroCss = ".digi-page-block-hero.sc-digi-page-block-hero {\n  display: block;\n  background: var(--digi--layout-page-block-hero--background, transparent);\n  padding: calc(var(--digi--responsive-grid-gutter) * 3) 0;\n}\n.digi-page-block-hero--variation-start.sc-digi-page-block-hero, .digi-page-block-hero--variation-sub.sc-digi-page-block-hero {\n  --digi--layout-page-block-hero--background: var(--digi--global--color--profile--apricot--base);\n}\n.digi-page-block-hero--variation-section.sc-digi-page-block-hero {\n  --digi--layout-page-block-hero--background: var(--digi--global--color--profile--apricot--opacity50);\n}\n.digi-page-block-hero--variation-section.sc-digi-page-block-hero {\n  --digi--layout-page-block-hero--background: var(--digi--global--color--profile--apricot--opacity50);\n}\n.digi-page-block-hero--variation-process.sc-digi-page-block-hero {\n  --digi--layout-page-block-hero--background: var(--digi--global--color--profile--blue--opacity50);\n}\n.digi-page-block-hero--background-transparent.sc-digi-page-block-hero {\n  --digi--layout-page-block-hero--background: transparent;\n}\n.digi-page-block-hero--background-image.sc-digi-page-block-hero {\n  background: var(--digi--page-block-hero--background-image) center center no-repeat;\n  background-size: cover;\n}\n\n.digi-page-block-hero__inner.sc-digi-page-block-hero {\n  display: inline-flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: flex-start;\n}\n.digi-page-block-hero--variation-start.sc-digi-page-block-hero .digi-page-block-hero__inner.sc-digi-page-block-hero {\n  gap: var(--digi--global--spacing--smaller);\n}\n.digi-page-block-hero--variation-section.sc-digi-page-block-hero .digi-page-block-hero__inner.sc-digi-page-block-hero, .digi-page-block-hero--variation-sub.sc-digi-page-block-hero .digi-page-block-hero__inner.sc-digi-page-block-hero {\n  background: var(--digi--color--background--primary);\n  box-shadow: 0px 8px 16px 0px rgba(105, 40, 89, 0.2);\n  padding: var(--digi--responsive-grid-gutter);\n  -webkit-padding-end: calc(var(--digi--responsive-grid-gutter) * 3);\n          padding-inline-end: calc(var(--digi--responsive-grid-gutter) * 3);\n}\n\n.digi-page-block-hero--variation-start.sc-digi-page-block-hero .digi-page-block-hero__heading.sc-digi-page-block-hero {\n  background: var(--digi--color--background--inverted-1);\n  color: var(--digi--color--text--inverted);\n  padding: var(--digi--padding--medium) var(--digi--padding--smaller);\n}\n.digi-page-block-hero--variation-start .digi-page-block-hero__heading.sc-digi-page-block-hero-s > [slot^=heading], \n.digi-page-block-hero--variation-start .digi-page-block-hero__heading .sc-digi-page-block-hero-s > [slot^=heading] {\n  margin-bottom: 0;\n}\n\n.digi-page-block-hero--variation-start.sc-digi-page-block-hero .digi-page-block-hero__content.sc-digi-page-block-hero {\n  background: var(--digi--color--background--primary);\n  padding: var(--digi--padding--medium) var(--digi--padding--smaller);\n  min-width: 50%;\n}\n.digi-page-block-hero--variation-start .digi-page-block-hero__content.sc-digi-page-block-hero-s > *, \n.digi-page-block-hero--variation-start .digi-page-block-hero__content .sc-digi-page-block-hero-s > * {\n  font-family: var(--digi--global--typography--font-family--default);\n  font-size: var(--digi--typography--preamble--font-size--mobile);\n  font-weight: var(--digi--typography--preamble--font-weight--desktop);\n  color: var(--digi--color--text--primary);\n  max-width: var(--digi--paragraph-width--medium);\n}\n@media (min-width: 62rem) {\n  .digi-page-block-hero--variation-start .digi-page-block-hero__content.sc-digi-page-block-hero-s > *, \n@media (min-width: 62rem) {\n  .digi-page-block-hero--variation-start .digi-page-block-hero__content .sc-digi-page-block-hero-s > * {\n    font-size: var(--digi--typography--preamble--font-size--desktop);\n  }\n}";

const PageBlockHero = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afVariation = undefined;
    this.afBackground = undefined;
    this.afBackgroundImage = undefined;
  }
  get cssModifiers() {
    return {
      [`digi-page-block-hero--variation-${this.afVariation}`]: !!this.afVariation,
      [`digi-page-block-hero--background-${this.afBackground}`]: !!this.afBackground,
      'digi-page-block-hero--background-image': !!this.afBackgroundImage
    };
  }
  render() {
    return (h("digi-layout-container", { class: Object.assign({ 'digi-page-block-hero': true }, this.cssModifiers), style: {
        '--digi--page-block-hero--background-image': this.afBackgroundImage
          ? `url('${this.afBackgroundImage}')`
          : null
      } }, h("digi-typography", null, h("div", { class: "digi-page-block-hero__inner" }, h("div", { class: "digi-page-block-hero__heading" }, h("slot", { name: "heading" })), h("div", { class: "digi-page-block-hero__content" }, h("slot", null))))));
  }
  static get assetsDirs() { return ["public"]; }
  static get style() { return pageBlockHeroCss; }
}, [6, "digi-page-block-hero", {
    "afVariation": [1, "af-variation"],
    "afBackground": [1, "af-background"],
    "afBackgroundImage": [1, "af-background-image"]
  }]);
function defineCustomElement$1() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-page-block-hero", "digi-layout-container", "digi-typography"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-page-block-hero":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, PageBlockHero);
      }
      break;
    case "digi-layout-container":
      if (!customElements.get(tagName)) {
        defineCustomElement$3();
      }
      break;
    case "digi-typography":
      if (!customElements.get(tagName)) {
        defineCustomElement$2();
      }
      break;
  } });
}
defineCustomElement$1();

const DigiPageBlockHero = PageBlockHero;
const defineCustomElement = defineCustomElement$1;

export { DigiPageBlockHero, defineCustomElement };
