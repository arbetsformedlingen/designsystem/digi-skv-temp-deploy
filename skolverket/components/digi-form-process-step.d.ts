import type { Components, JSX } from "../dist/types/components";

interface DigiFormProcessStep extends Components.DigiFormProcessStep, HTMLElement {}
export const DigiFormProcessStep: {
  prototype: DigiFormProcessStep;
  new (): DigiFormProcessStep;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
