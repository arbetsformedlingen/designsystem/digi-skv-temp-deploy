import type { Components, JSX } from "../dist/types/components";

interface DigiFormFileUpload extends Components.DigiFormFileUpload, HTMLElement {}
export const DigiFormFileUpload: {
  prototype: DigiFormFileUpload;
  new (): DigiFormFileUpload;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
