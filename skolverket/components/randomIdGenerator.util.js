function randomIdGenerator(prefix = 'digi') {
  return `${prefix}-${Math.random()
    .toString(36)
    .substring(2, 7)}`;
}

export { randomIdGenerator as r };
