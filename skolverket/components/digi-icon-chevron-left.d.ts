import type { Components, JSX } from "../dist/types/components";

interface DigiIconChevronLeft extends Components.DigiIconChevronLeft, HTMLElement {}
export const DigiIconChevronLeft: {
  prototype: DigiIconChevronLeft;
  new (): DigiIconChevronLeft;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
