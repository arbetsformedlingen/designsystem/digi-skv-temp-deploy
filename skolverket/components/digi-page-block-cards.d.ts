import type { Components, JSX } from "../dist/types/components";

interface DigiPageBlockCards extends Components.DigiPageBlockCards, HTMLElement {}
export const DigiPageBlockCards: {
  prototype: DigiPageBlockCards;
  new (): DigiPageBlockCards;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
