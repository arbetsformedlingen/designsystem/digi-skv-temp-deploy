import type { Components, JSX } from "../dist/types/components";

interface DigiIconExternalLinkAlt extends Components.DigiIconExternalLinkAlt, HTMLElement {}
export const DigiIconExternalLinkAlt: {
  prototype: DigiIconExternalLinkAlt;
  new (): DigiIconExternalLinkAlt;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
