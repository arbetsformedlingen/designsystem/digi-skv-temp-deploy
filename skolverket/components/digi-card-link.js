import { proxyCustomElement, HTMLElement, h } from '@stencil/core/internal/client';
import './button-variation.enum.js';
import './expandable-accordion-variation.enum.js';
import './calendar-week-view-heading-level.enum.js';
import './code-block-variation.enum.js';
import './code-example-variation.enum.js';
import './code-variation.enum.js';
import './form-checkbox-variation.enum.js';
import './form-file-upload-variation.enum.js';
import './form-input-search-variation.enum.js';
import './form-input-variation.enum.js';
import './form-radiobutton-variation.enum.js';
import './form-select-variation.enum.js';
import './form-textarea-variation.enum.js';
import './form-validation-message-variation.enum.js';
import './layout-block-variation.enum.js';
import './layout-columns-variation.enum.js';
import './layout-container-variation.enum.js';
import './layout-media-object-alignment.enum.js';
import './link-external-variation.enum.js';
import './link-internal-variation.enum.js';
import './link-variation.enum.js';
import './loader-spinner-size.enum.js';
import './media-figure-alignment.enum.js';
import './navigation-context-menu-item-type.enum.js';
import './navigation-sidebar-variation.enum.js';
import './navigation-vertical-menu-variation.enum.js';
import './progress-step-variation.enum.js';
import './progress-steps-variation.enum.js';
import './progressbar-variation.enum.js';
import './tag-size.enum.js';
import './typography-meta-variation.enum.js';
import './typography-time-variation.enum.js';
import { T as TypographyVariation } from './typography-variation.enum.js';
import './util-breakpoint-observer-breakpoints.enum.js';
import { d as defineCustomElement$2 } from './typography.js';

const cardLinkCss = ".sc-digi-card-link-h{--digi--card-link--padding:var(--digi--padding--largest);display:grid}.sc-digi-card-link-h .sc-digi-card-link-s>[slot^=link-heading]{display:flex;-webkit-margin-after:0.5rem;margin-block-end:0.5rem}.sc-digi-card-link-h .sc-digi-card-link-s>[slot^=link-heading] a{font-family:var(--digi--global--typography--font-family--default);text-decoration:none;color:var(--digi--color--text--secondary);font-size:var(--digi--global--typography--font-size--large);letter-spacing:calc(var(--digi--global--typography--font-size--large) / 100 * -1);font-weight:var(--digi--global--typography--font-weight--semibold)}.sc-digi-card-link-h .sc-digi-card-link-s>[slot^=link-heading] a:hover{color:var(--digi--global--color--profile--purple--dark);text-decoration:underline}.sc-digi-card-link-h .sc-digi-card-link-s>[slot^=link-heading] a:focus{outline:none;color:var(--digi--color--text--secondary)}.sc-digi-card-link-h .sc-digi-card-link-s>[slot^=link-heading] a:focus-visible{outline:var(--digi--border-width--secondary) solid var(--digi--color--border--focus)}.sc-digi-card-link-h .sc-digi-card-link-s>[slot^=link-heading] a:visited{color:var(--digi--color--text--secondary)}@media (min-width: 48rem){.sc-digi-card-link-h .sc-digi-card-link-s>[slot^=link-heading] a{font-size:var(--digi--global--typography--font-size--larger);letter-spacing:calc(var(--digi--global--typography--font-size--larger) / 100 * -1);line-height:1.44}}.sc-digi-card-link-h .sc-digi-card-link-s>[slot^=link-heading] a:hover{color:var(--digi--global--color--profile--purple--dark);text-decoration:underline}.sc-digi-card-link-h .sc-digi-card-link-s>[slot^=link-heading] a::after{background-color:currentColor;padding:0 0.5em;-webkit-mask-image:url(\"data:image/svg+xml;charset=utf-8, %3Csvg%0A%09width%3D%2224%22%0A%09height%3D%2224%22%0A%09viewBox%3D%220%200%2024%2024%22%0A%09xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%0A%3E%0A%09%3Cpath%0A%09%09d%3D%22M288.917%2C415.517%2C287.5%2C414.1l3.96-3.96-3.96-3.96%2C1.414-1.414%2C5.373%2C5.374Z%22%0A%09%09transform%3D%22translate(-278.17%20-398.103)%22%0A%09%2F%3E%0A%3C%2Fsvg%3E\");mask-image:url(\"data:image/svg+xml;charset=utf-8, %3Csvg%0A%09width%3D%2224%22%0A%09height%3D%2224%22%0A%09viewBox%3D%220%200%2024%2024%22%0A%09xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%0A%3E%0A%09%3Cpath%0A%09%09d%3D%22M288.917%2C415.517%2C287.5%2C414.1l3.96-3.96-3.96-3.96%2C1.414-1.414%2C5.373%2C5.374Z%22%0A%09%09transform%3D%22translate(-278.17%20-398.103)%22%0A%09%2F%3E%0A%3C%2Fsvg%3E\");-webkit-mask-repeat:no-repeat;mask-repeat:no-repeat;-webkit-mask-position:center;mask-position:center;-webkit-clip-path:padding-box inset(0.25em 0);clip-path:padding-box inset(0.25em 0);content:\"\";-webkit-mask-size:cover;mask-size:cover}.sc-digi-card-link-h .sc-digi-card-link-s>[slot^=link-heading] a::before{content:\"\";position:absolute;top:0;left:0;width:100%;height:100%;border-radius:var(--digi--border-radius--primary)}.sc-digi-card-link-h .sc-digi-card-link-s>[slot^=link-heading] a:focus-visible{outline:none}.sc-digi-card-link-h .sc-digi-card-link-s>[slot^=link-heading] a:focus-visible::before{content:\"\";outline:var(--digi--focus-outline)}.sc-digi-card-link-h .sc-digi-card-link-s>[slot^=link-heading] a svg{width:1.3em;height:1.3em}.sc-digi-card-link-h .sc-digi-card-link-s>img[slot^=image]{display:block;width:100%;height:auto}.sc-digi-card-link-h .digi-card-link.sc-digi-card-link{display:grid;grid-template-rows:auto 1fr;position:relative;border-radius:var(--digi--border-radius--primary);background:var(--digi--color--background--primary)}.sc-digi-card-link-h .digi-card-link__image.sc-digi-card-link{border-radius:var(--digi--border-radius--primary) var(--digi--border-radius--primary) 0 0;overflow:hidden;aspect-ratio:16/9;background-color:var(--digi--color--background--tertiary)}.sc-digi-card-link-h .digi-card-link__text.sc-digi-card-link{position:relative}.digi-card-link__content.sc-digi-card-link{display:flex;padding:var(--digi--card-link--padding);border:var(--digi--border-width--tertiary) var(--digi--border-style--primary) var(--digi--global--color--profile--purple--opacity15);border-radius:0 0 var(--digi--border-radius--primary) var(--digi--border-radius--primary)}.digi-card-link--image-false.sc-digi-card-link .digi-card-link__content.sc-digi-card-link{border-radius:var(--digi--border-radius--primary);grid-row:1/3}.digi-card-link--image-true.sc-digi-card-link .digi-card-link__content.sc-digi-card-link{-webkit-border-before:none;border-block-start:none}";

const CardLink = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.hasImage = undefined;
  }
  componentWillLoad() {
    this.setHasImage();
  }
  componentWillUpdate() {
    this.setHasImage();
  }
  setHasImage() {
    this.hasImage = !!this.hostElement.querySelector('[slot="image"]');
  }
  get cssModifiers() {
    return {
      [`digi-card-link--image-${this.hasImage}`]: true
    };
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-card-link': true }, this.cssModifiers) }, this.hasImage && (h("div", { class: "digi-card-link__image" }, h("slot", { name: "image" }))), h("digi-typography", { afVariation: TypographyVariation.LARGE, class: "digi-card-link__content" }, h("div", { class: "digi-card-link__heading" }, h("slot", { name: "link-heading" })), h("div", { class: "digi-card-link__text" }, h("slot", null)))));
  }
  get hostElement() { return this; }
  static get style() { return cardLinkCss; }
}, [6, "digi-card-link", {
    "hasImage": [32]
  }]);
function defineCustomElement$1() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-card-link", "digi-typography"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-card-link":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, CardLink);
      }
      break;
    case "digi-typography":
      if (!customElements.get(tagName)) {
        defineCustomElement$2();
      }
      break;
  } });
}
defineCustomElement$1();

const DigiCardLink = CardLink;
const defineCustomElement = defineCustomElement$1;

export { DigiCardLink, defineCustomElement };
