import type { Components, JSX } from "../dist/types/components";

interface DigiUtilIntersectionObserver extends Components.DigiUtilIntersectionObserver, HTMLElement {}
export const DigiUtilIntersectionObserver: {
  prototype: DigiUtilIntersectionObserver;
  new (): DigiUtilIntersectionObserver;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
