var FormRadiobuttonLayout;
(function (FormRadiobuttonLayout) {
  FormRadiobuttonLayout["INLINE"] = "inline";
  FormRadiobuttonLayout["BLOCK"] = "block";
})(FormRadiobuttonLayout || (FormRadiobuttonLayout = {}));

var FormRadiobuttonValidation;
(function (FormRadiobuttonValidation) {
  FormRadiobuttonValidation["ERROR"] = "error";
})(FormRadiobuttonValidation || (FormRadiobuttonValidation = {}));

var FormRadiobuttonVariation;
(function (FormRadiobuttonVariation) {
  FormRadiobuttonVariation["PRIMARY"] = "primary";
  FormRadiobuttonVariation["SECONDARY"] = "secondary";
})(FormRadiobuttonVariation || (FormRadiobuttonVariation = {}));

export { FormRadiobuttonLayout as F, FormRadiobuttonValidation as a, FormRadiobuttonVariation as b };
