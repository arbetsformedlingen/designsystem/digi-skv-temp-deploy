import type { Components, JSX } from "../dist/types/components";

interface DigiUtilKeyupHandler extends Components.DigiUtilKeyupHandler, HTMLElement {}
export const DigiUtilKeyupHandler: {
  prototype: DigiUtilKeyupHandler;
  new (): DigiUtilKeyupHandler;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
