import { L as LayoutContainer, d as defineCustomElement$1 } from './layout-container.js';

const DigiLayoutContainer = LayoutContainer;
const defineCustomElement = defineCustomElement$1;

export { DigiLayoutContainer, defineCustomElement };
