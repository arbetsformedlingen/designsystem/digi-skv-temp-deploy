import { proxyCustomElement, HTMLElement, h } from '@stencil/core/internal/client';

var CardBoxGutter;
(function (CardBoxGutter) {
  CardBoxGutter["REGULAR"] = "regular";
  CardBoxGutter["NONE"] = "none";
})(CardBoxGutter || (CardBoxGutter = {}));

var CardBoxWidth;
(function (CardBoxWidth) {
  CardBoxWidth["REGULAR"] = "regular";
  CardBoxWidth["FULL"] = "full";
})(CardBoxWidth || (CardBoxWidth = {}));

const cardBoxCss = ".sc-digi-card-box-h{--digi--card-box--padding--inline:var(--digi--responsive-grid-gutter--outer);--digi--card-box--padding--block:var(--digi--padding--largest)}@media (min-width: 48rem){.sc-digi-card-box-h{--digi--card-box--padding--block:var(--digi--card-box--padding--inline)}}@media (max-width: 47.9375rem){digi-layout-grid .sc-digi-card-box-h{grid-column:1/-1}}.sc-digi-card-box-h .digi-card-box.sc-digi-card-box{box-shadow:0px 8px 16px 0px rgba(105, 40, 89, 0.2);padding:var(--digi--card-box--padding--block) var(--digi--card-box--padding--inline);-webkit-border-before:var(--digi--border-width--secondary) solid var(--digi--color--border--secondary);border-block-start:var(--digi--border-width--secondary) solid var(--digi--color--border--secondary);border-radius:var(--digi--border-radius--primary);background:var(--digi--color--background--primary)}@media (max-width: 47.9375rem){digi-layout-container .sc-digi-card-box-h .digi-card-box.sc-digi-card-box{border-radius:0}digi-layout-container .sc-digi-card-box-h .digi-card-box--width-regular.sc-digi-card-box{width:100vw;-webkit-margin-start:calc(var(--digi--card-box--padding--inline) * -1);margin-inline-start:calc(var(--digi--card-box--padding--inline) * -1)}}.sc-digi-card-box-h .digi-card-box--gutter-none.sc-digi-card-box{--digi--card-box--padding--inline:0;--digi--card-box--padding--block:0}.sc-digi-card-box-h .digi-card-box--width-full.sc-digi-card-box{width:calc(100% + var(--digi--layout-container--gutter, 0) * 2)}digi-layout-container .sc-digi-card-box-h .digi-card-box--width-full.sc-digi-card-box{-webkit-margin-start:calc(var(--digi--layout-container--gutter, 0) * -1);margin-inline-start:calc(var(--digi--layout-container--gutter, 0) * -1)}";

const CardBox = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afWidth = CardBoxWidth.REGULAR;
    this.afGutter = CardBoxGutter.REGULAR;
  }
  get cssModifiers() {
    return {
      [`digi-card-box--width-${this.afWidth}`]: !!this.afWidth,
      [`digi-card-box--gutter-${this.afGutter}`]: !!this.afGutter
    };
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-card-box': true }, this.cssModifiers) }, h("slot", null)));
  }
  static get style() { return cardBoxCss; }
}, [6, "digi-card-box", {
    "afWidth": [1, "af-width"],
    "afGutter": [1, "af-gutter"]
  }]);
function defineCustomElement() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-card-box"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-card-box":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, CardBox);
      }
      break;
  } });
}
defineCustomElement();

export { CardBoxGutter as C, CardBoxWidth as a, CardBox as b, defineCustomElement as d };
