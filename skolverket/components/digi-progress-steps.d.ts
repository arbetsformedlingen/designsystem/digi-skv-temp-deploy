import type { Components, JSX } from "../dist/types/components";

interface DigiProgressSteps extends Components.DigiProgressSteps, HTMLElement {}
export const DigiProgressSteps: {
  prototype: DigiProgressSteps;
  new (): DigiProgressSteps;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
