import type { Components, JSX } from "../dist/types/components";

interface DigiNavigationMainMenu extends Components.DigiNavigationMainMenu, HTMLElement {}
export const DigiNavigationMainMenu: {
  prototype: DigiNavigationMainMenu;
  new (): DigiNavigationMainMenu;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
