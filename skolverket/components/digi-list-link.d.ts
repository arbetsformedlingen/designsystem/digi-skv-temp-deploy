import type { Components, JSX } from "../dist/types/components";

interface DigiListLink extends Components.DigiListLink, HTMLElement {}
export const DigiListLink: {
  prototype: DigiListLink;
  new (): DigiListLink;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
