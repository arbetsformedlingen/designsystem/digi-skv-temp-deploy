import { proxyCustomElement, HTMLElement, createEvent, h } from '@stencil/core/internal/client';
import { r as randomIdGenerator } from './randomIdGenerator.util.js';
import { d as defineCustomElement$3 } from './button.js';
import { d as defineCustomElement$2 } from './icon.js';

const navigationSidebarButtonCss = ".sc-digi-navigation-sidebar-button-h{--digi--navigation-sidebar-button--display:inline-flex}.sc-digi-navigation-sidebar-button-h .digi-navigation-sidebar-button.sc-digi-navigation-sidebar-button{display:var(--digi--navigation-sidebar-button--display)}";

const NavigationSidebarButton = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afOnToggle = createEvent(this, "afOnToggle", 7);
    this.isActive = false;
    this.afText = undefined;
    this.afAriaLabel = undefined;
    this.afId = randomIdGenerator('digi-navigation-sidebar-button');
  }
  toggleHandler() {
    this.isActive = !this.isActive;
    this.afOnToggle.emit(this.isActive);
  }
  render() {
    return (h("digi-button", { id: this.afId, class: "digi-navigation-sidebar-button", "af-variation": "function", "af-aria-label": this.afAriaLabel, onClick: () => this.toggleHandler() }, this.afText, h("digi-icon", { slot: "icon", afName: `bars` })));
  }
  static get style() { return navigationSidebarButtonCss; }
}, [2, "digi-navigation-sidebar-button", {
    "afText": [1, "af-text"],
    "afAriaLabel": [1, "af-aria-label"],
    "afId": [1, "af-id"],
    "isActive": [32]
  }]);
function defineCustomElement$1() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-navigation-sidebar-button", "digi-button", "digi-icon"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-navigation-sidebar-button":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, NavigationSidebarButton);
      }
      break;
    case "digi-button":
      if (!customElements.get(tagName)) {
        defineCustomElement$3();
      }
      break;
    case "digi-icon":
      if (!customElements.get(tagName)) {
        defineCustomElement$2();
      }
      break;
  } });
}
defineCustomElement$1();

const DigiNavigationSidebarButton = NavigationSidebarButton;
const defineCustomElement = defineCustomElement$1;

export { DigiNavigationSidebarButton, defineCustomElement };
