var ProgressStepHeadingLevel;
(function (ProgressStepHeadingLevel) {
  ProgressStepHeadingLevel["H1"] = "h1";
  ProgressStepHeadingLevel["H2"] = "h2";
  ProgressStepHeadingLevel["H3"] = "h3";
  ProgressStepHeadingLevel["H4"] = "h4";
  ProgressStepHeadingLevel["H5"] = "h5";
  ProgressStepHeadingLevel["H6"] = "h6";
})(ProgressStepHeadingLevel || (ProgressStepHeadingLevel = {}));

var ProgressStepStatus;
(function (ProgressStepStatus) {
  ProgressStepStatus["CURRENT"] = "current";
  ProgressStepStatus["UPCOMING"] = "upcoming";
  ProgressStepStatus["DONE"] = "done";
})(ProgressStepStatus || (ProgressStepStatus = {}));

var ProgressStepVariation;
(function (ProgressStepVariation) {
  ProgressStepVariation["PRIMARY"] = "primary";
  ProgressStepVariation["SECONDARY"] = "secondary";
})(ProgressStepVariation || (ProgressStepVariation = {}));

export { ProgressStepHeadingLevel as P, ProgressStepStatus as a, ProgressStepVariation as b };
