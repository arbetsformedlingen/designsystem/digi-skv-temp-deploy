import type { Components, JSX } from "../dist/types/components";

interface DigiIconCopy extends Components.DigiIconCopy, HTMLElement {}
export const DigiIconCopy: {
  prototype: DigiIconCopy;
  new (): DigiIconCopy;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
