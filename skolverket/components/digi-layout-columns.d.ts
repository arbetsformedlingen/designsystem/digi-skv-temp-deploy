import type { Components, JSX } from "../dist/types/components";

interface DigiLayoutColumns extends Components.DigiLayoutColumns, HTMLElement {}
export const DigiLayoutColumns: {
  prototype: DigiLayoutColumns;
  new (): DigiLayoutColumns;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
