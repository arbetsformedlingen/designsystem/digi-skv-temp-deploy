import type { Components, JSX } from "../dist/types/components";

interface DigiLinkIcon extends Components.DigiLinkIcon, HTMLElement {}
export const DigiLinkIcon: {
  prototype: DigiLinkIcon;
  new (): DigiLinkIcon;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
