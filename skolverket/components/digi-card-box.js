import { b as CardBox, d as defineCustomElement$1 } from './card-box.js';

const DigiCardBox = CardBox;
const defineCustomElement = defineCustomElement$1;

export { DigiCardBox, defineCustomElement };
