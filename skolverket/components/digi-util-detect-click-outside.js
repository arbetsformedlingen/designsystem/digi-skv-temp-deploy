import { U as UtilDetectClickOutside, d as defineCustomElement$1 } from './util-detect-click-outside.js';

const DigiUtilDetectClickOutside = UtilDetectClickOutside;
const defineCustomElement = defineCustomElement$1;

export { DigiUtilDetectClickOutside, defineCustomElement };
