import type { Components, JSX } from "../dist/types/components";

interface DigiIconExclamationCircleFilled extends Components.DigiIconExclamationCircleFilled, HTMLElement {}
export const DigiIconExclamationCircleFilled: {
  prototype: DigiIconExclamationCircleFilled;
  new (): DigiIconExclamationCircleFilled;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
