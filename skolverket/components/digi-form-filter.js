import { proxyCustomElement, HTMLElement, createEvent, h } from '@stencil/core/internal/client';
import { r as randomIdGenerator } from './randomIdGenerator.util.js';
import { l as logger } from './logger.util.js';
import { d as detectClickOutside } from './detectClickOutside.util.js';
import { d as detectFocusOutside } from './detectFocusOutside.util.js';
import { d as defineCustomElement$5 } from './button.js';
import { d as defineCustomElement$4 } from './icon.js';
import { d as defineCustomElement$3 } from './util-keydown-handler.js';
import { d as defineCustomElement$2 } from './util-mutation-observer.js';

const formFilterCss = ".digi--util--fs--xs.sc-digi-form-filter{font-size:var(--digi--global--typography--font-size--small) !important}.digi--util--fs--s.sc-digi-form-filter{font-size:calc(var(--digi--global--typography--font-size--small) + 1px) !important}.digi--util--fs--m.sc-digi-form-filter{font-size:var(--digi--global--typography--font-size--base) !important}.digi--util--fs--l.sc-digi-form-filter{font-size:var(--digi--global--typography--font-size--large) !important}.digi--util--fw--sb.sc-digi-form-filter{font-weight:var(--digi--global--typography--font-weight--semibold) !important}.digi--util--pt--1.sc-digi-form-filter{padding-top:var(--digi--global--spacing--smallest-2) !important}.digi--util--pt--10.sc-digi-form-filter{padding-top:var(--digi--global--spacing--smallest) !important}.digi--util--pt--20.sc-digi-form-filter{padding-top:var(--digi--global--spacing--base) !important}.digi--util--pt--30.sc-digi-form-filter{padding-top:var(--digi--global--spacing--largest-2) !important}.digi--util--pt--40.sc-digi-form-filter{padding-top:var(--digi--global--spacing--largest-4) !important}.digi--util--pt--50.sc-digi-form-filter{padding-top:2.5rem !important}.digi--util--pt--60.sc-digi-form-filter{padding-top:var(--digi--global--spacing--largest-5) !important}.digi--util--pt--70.sc-digi-form-filter{padding-top:var(--digi--global--spacing--largest-6) !important}.digi--util--pt--80.sc-digi-form-filter{padding-top:4.5rem !important}.digi--util--pt--90.sc-digi-form-filter{padding-top:5rem !important}.digi--util--pb--1.sc-digi-form-filter{padding-bottom:var(--digi--global--spacing--smallest-2) !important}.digi--util--pb--10.sc-digi-form-filter{padding-bottom:var(--digi--global--spacing--smallest) !important}.digi--util--pb--20.sc-digi-form-filter{padding-bottom:var(--digi--global--spacing--base) !important}.digi--util--pb--30.sc-digi-form-filter{padding-bottom:var(--digi--global--spacing--largest-2) !important}.digi--util--pb--40.sc-digi-form-filter{padding-bottom:var(--digi--global--spacing--largest-4) !important}.digi--util--pb--50.sc-digi-form-filter{padding-bottom:2.5rem !important}.digi--util--pb--60.sc-digi-form-filter{padding-bottom:var(--digi--global--spacing--largest-5) !important}.digi--util--pb--70.sc-digi-form-filter{padding-bottom:var(--digi--global--spacing--largest-6) !important}.digi--util--pb--80.sc-digi-form-filter{padding-bottom:4.5rem !important}.digi--util--pb--90.sc-digi-form-filter{padding-bottom:5rem !important}.digi--util--p--1.sc-digi-form-filter{padding:var(--digi--global--spacing--smallest-2) !important}.digi--util--p--10.sc-digi-form-filter{padding:var(--digi--global--spacing--smallest) !important}.digi--util--p--20.sc-digi-form-filter{padding:var(--digi--global--spacing--base) !important}.digi--util--p--30.sc-digi-form-filter{padding:var(--digi--global--spacing--largest-2) !important}.digi--util--p--40.sc-digi-form-filter{padding:var(--digi--global--spacing--largest-4) !important}.digi--util--p--50.sc-digi-form-filter{padding:2.5rem !important}.digi--util--p--60.sc-digi-form-filter{padding:var(--digi--global--spacing--largest-5) !important}.digi--util--p--70.sc-digi-form-filter{padding:var(--digi--global--spacing--largest-6) !important}.digi--util--p--80.sc-digi-form-filter{padding:4.5rem !important}.digi--util--p--90.sc-digi-form-filter{padding:5rem !important}.digi--util--ptb--1.sc-digi-form-filter{padding-top:var(--digi--global--spacing--smallest-2) !important;padding-bottom:var(--digi--global--spacing--smallest-2) !important}.digi--util--ptb--10.sc-digi-form-filter{padding-top:var(--digi--global--spacing--smallest) !important;padding-bottom:var(--digi--global--spacing--smallest) !important}.digi--util--ptb--20.sc-digi-form-filter{padding-top:var(--digi--global--spacing--base) !important;padding-bottom:var(--digi--global--spacing--base) !important}.digi--util--ptb--30.sc-digi-form-filter{padding-top:var(--digi--global--spacing--largest-2) !important;padding-bottom:var(--digi--global--spacing--largest-2) !important}.digi--util--ptb--40.sc-digi-form-filter{padding-top:var(--digi--global--spacing--largest-4) !important;padding-bottom:var(--digi--global--spacing--largest-4) !important}.digi--util--ptb--50.sc-digi-form-filter{padding-top:2.5rem !important;padding-bottom:2.5rem !important}.digi--util--ptb--60.sc-digi-form-filter{padding-top:var(--digi--global--spacing--largest-5) !important;padding-bottom:var(--digi--global--spacing--largest-5) !important}.digi--util--ptb--70.sc-digi-form-filter{padding-top:var(--digi--global--spacing--largest-6) !important;padding-bottom:var(--digi--global--spacing--largest-6) !important}.digi--util--ptb--80.sc-digi-form-filter{padding-top:4.5rem !important;padding-bottom:4.5rem !important}.digi--util--ptb--90.sc-digi-form-filter{padding-top:5rem !important;padding-bottom:5rem !important}.digi--util--plr--1.sc-digi-form-filter{padding-left:var(--digi--global--spacing--smallest-2) !important;padding-right:var(--digi--global--spacing--smallest-2) !important}.digi--util--plr--10.sc-digi-form-filter{padding-left:var(--digi--global--spacing--smallest) !important;padding-right:var(--digi--global--spacing--smallest) !important}.digi--util--plr--20.sc-digi-form-filter{padding-left:var(--digi--global--spacing--base) !important;padding-right:var(--digi--global--spacing--base) !important}.digi--util--plr--30.sc-digi-form-filter{padding-left:var(--digi--global--spacing--largest-2) !important;padding-right:var(--digi--global--spacing--largest-2) !important}.digi--util--plr--40.sc-digi-form-filter{padding-left:var(--digi--global--spacing--largest-4) !important;padding-right:var(--digi--global--spacing--largest-4) !important}.digi--util--plr--50.sc-digi-form-filter{padding-left:2.5rem !important;padding-right:2.5rem !important}.digi--util--plr--60.sc-digi-form-filter{padding-left:var(--digi--global--spacing--largest-5) !important;padding-right:var(--digi--global--spacing--largest-5) !important}.digi--util--plr--70.sc-digi-form-filter{padding-left:var(--digi--global--spacing--largest-6) !important;padding-right:var(--digi--global--spacing--largest-6) !important}.digi--util--plr--80.sc-digi-form-filter{padding-left:4.5rem !important;padding-right:4.5rem !important}.digi--util--plr--90.sc-digi-form-filter{padding-left:5rem !important;padding-right:5rem !important}.digi--util--sr-only.sc-digi-form-filter{border:0;clip:rect(1px, 1px, 1px, 1px);-webkit-clip-path:inset(50%);clip-path:inset(50%);height:1px;margin:-1px;overflow:hidden;padding:0;position:absolute;width:1px;white-space:nowrap}.sc-digi-form-filter-h{--digi--form-filter--footer--background:var(--digi--color--background--secondary);--digi--form-filter--scroll--max-height:18.75rem;--digi--form-filter--scroll--max-height--large:23.4275rem;--digi--form-filter--scroll--padding:var(--digi--padding--small);--digi--form-filter--reset-button--margin-left:var(--digi--padding--small);--digi--form-filter--legend__border-bottom:var(--digi--border-width--primary) solid var(--digi--color--border--neutral-2);--digi--form-filter--list--margin:0;--digi--form-filter--list--padding:var(--digi--gutter--small) 0 0 0;--digi--form-filter--item--margin:var(--digi--gutter--small) 0;--digi--form-filter--toggle-icon--transition:ease-in-out 0.2s all;--digi--form-filter--dropdown--box-shadow:0 0.125rem 0.375rem 0 rgba(0, 0, 0, 0.7);--digi--form-filter--dropdown--border-radius:var(--digi--border--radius--primary);--digi--form-filter--dropdown--background:var(--digi--color--background--primary);--digi--form-filter--dropdown--min-width:18.75rem;--digi--form-filter--dropdown--margin-top:var(--digi--gutter--small);--digi--form-filter--dropdown--z-index:999;--digi--form-filter--toggle-button-indicator--color--default:transparent;--digi--form-filter--toggle-button-indicator--color--active:var(--digi--color--border--success);--digi--form-filter--toggle-button-indicator--width:0.625rem;--digi--form-filter--toggle-button-indicator--height:0.625rem;--digi--form-filter--toggle-button-indicator--margin-right:var(--digi--margin--small);--digi--form-filter--toggle-button-indicator--margin-left:calc(var(--digi--margin--smaller) * -1);--digi--form-filter--toggle-button--text--margin-right:var(--digi--margin--smaller);--digi--form-filter--toggle-icon--width:0.875rem;--digi--form-filter--toggle-icon--height:0.875rem;--digi--form-filter--legend--font-weight:var(--digi--typography--preamble--font-weight--desktop);--digi--form-filter--legend--font-family:var(--digi--global--typography--font-family--default);--digi--form-filter--legend--font-size:var(--digi--typography--preamble--font-size--desktop)}.sc-digi-form-filter-h .digi-form-filter.sc-digi-form-filter{--INDICATOR--BACKGROUND:var(--digi--form-filter--toggle-button-indicator--color--default);--SCROLL--MAX-HEIGHT:var(--digi--form-filter--scroll--max-height);position:relative}.sc-digi-form-filter-h .digi-form-filter--open.sc-digi-form-filter .digi-form-filter__toggle-icon.sc-digi-form-filter{transform:rotate(-180deg)}.sc-digi-form-filter-h .digi-form-filter__toggle-icon.sc-digi-form-filter{transition:var(--digi--form-filter--toggle-icon--transition)}.sc-digi-form-filter-h .digi-form-filter__toggle-icon .sc-digi-form-filter-s>[slot^=icon]{--digi--icon--width:var(--digi--form-filter--toggle-icon--width);--digi--icon--height:var(--digi--form-filter--toggle-icon--height)}.sc-digi-form-filter-h .digi-form-filter__legend.sc-digi-form-filter{font-weight:var(--digi--form-filter--legend--font-weight);font-family:var(--digi--form-filter--legend--font-family);font-size:var(--digi--form-filter--legend--font-size)}.sc-digi-form-filter-h .digi-form-filter__scroll.sc-digi-form-filter{max-height:var(--SCROLL--MAX-HEIGHT);overflow-y:auto;padding:var(--digi--form-filter--scroll--padding)}@media (min-width: 48rem){.sc-digi-form-filter-h .digi-form-filter__scroll.sc-digi-form-filter{--SCROLL--MAX-HEIGHT:var(--digi--form-filter--toggle-icon--height--large)}}.sc-digi-form-filter-h .digi-form-filter__scroll.sc-digi-form-filter fieldset.sc-digi-form-filter{border:none}.sc-digi-form-filter-h .digi-form-filter__scroll.sc-digi-form-filter legend.sc-digi-form-filter{width:100%;border-bottom:var(--digi--form-filter--legend__border-bottom)}.sc-digi-form-filter-h .digi-form-filter__footer.sc-digi-form-filter{display:flex;align-items:center;justify-content:center;background:var(--digi--form-filter--footer--background);padding:var(--digi--form-filter--scroll--padding)}.sc-digi-form-filter-h .digi-form-filter__reset-button.sc-digi-form-filter{display:inline-block;margin-left:var(--digi--form-filter--reset-button--margin-left)}.sc-digi-form-filter-h .digi-form-filter__dropdown.sc-digi-form-filter{min-width:var(--digi--form-filter--dropdown--min-width);position:absolute;z-index:var(--digi--form-filter--dropdown--z-index);background:var(--digi--form-filter--dropdown--background);border-radius:var(--digi--form-filter--dropdown--border-radius);box-shadow:var(--digi--form-filter--dropdown--box-shadow);overflow:hidden;margin-top:var(--digi--form-filter--dropdown--margin-top)}@media (min-width: 48rem){.sc-digi-form-filter-h .digi-form-filter__dropdown--right.sc-digi-form-filter{right:0}}.sc-digi-form-filter-h .digi-form-filter__toggle-button-indicator.sc-digi-form-filter:before{content:\"\";width:var(--digi--form-filter--toggle-button-indicator--width);height:var(--digi--form-filter--toggle-button-indicator--height);background:var(--INDICATOR--BACKGROUND);display:inline-block;margin-right:var(--digi--form-filter--toggle-button-indicator--margin-right);border-radius:50% 50%;overflow:hidden;margin-left:var(--digi--form-filter--toggle-button-indicator--margin-left)}.sc-digi-form-filter-h .digi-form-filter__toggle-button-indicator--active.sc-digi-form-filter{--INDICATOR--BACKGROUND:var(--digi--form-filter--toggle-button-indicator--color--active)}.sc-digi-form-filter-h .digi-form-filter__list.sc-digi-form-filter{margin:var(--digi--form-filter--list--margin);padding:var(--digi--form-filter--list--padding)}.sc-digi-form-filter-h .digi-form-filter__item.sc-digi-form-filter{margin:var(--digi--form-filter--item--margin);display:block}.sc-digi-form-filter-h .digi-form-filter__toggle-button-text.sc-digi-form-filter{margin-right:var(--digi--form-filter--toggle-button--text--margin-right)}";

const formFilter = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afOnFocusout = createEvent(this, "afOnFocusout", 7);
    this.afOnSubmitFilters = createEvent(this, "afOnSubmitFilters", 7);
    this.afOnFilterClosed = createEvent(this, "afOnFilterClosed", 7);
    this.afOnResetFilters = createEvent(this, "afOnResetFilters", 7);
    this.afOnChange = createEvent(this, "afOnChange", 7);
    this._updateActiveItems = false;
    this.activeList = [];
    this.isActive = false;
    this.activeListItemIndex = 0;
    this.hasActiveItems = false;
    this.focusInList = false;
    this.listItems = [];
    this.afId = randomIdGenerator('digi-form-filter');
    this.afFilterButtonText = undefined;
    this.afSubmitButtonText = undefined;
    this.afSubmitButtonTextAriaLabel = undefined;
    this.afResetButtonText = 'Rensa';
    this.afResetButtonTextAriaLabel = undefined;
    this.afHideResetButton = false;
    this.afAlignRight = undefined;
    this.afName = undefined;
    this.afFilterButtonAriaLabel = undefined;
    this.afFilterButtonAriaDescribedby = undefined;
    this.afAutofocus = undefined;
  }
  clickHandler(e) {
    const target = e.target;
    const selector = `#${this.afId}-identifier`;
    if (detectClickOutside(target, selector) && this.isActive) {
      this.emitFilterClosedEvent();
    }
  }
  focusoutHandler(e) {
    const target = e.target;
    const selector = `#${this.afId}-identifier`;
    const focusOutsideFilter = detectFocusOutside(target, selector);
    if (focusOutsideFilter && this.isActive) {
      this.emitFilterClosedEvent();
    }
    else {
      this.focusInList = !detectFocusOutside(target, `#${this.afId}-filter-list`);
      //Update tabindex on focused input element
      if (this.isActive && this.focusInList && target.tagName === 'INPUT') {
        this.setTabIndex();
        this.updateTabIndex(target);
      }
      this.afOnFocusout.emit(e);
    }
  }
  changeHandler(e) {
    if (e.target.tagName === 'INPUT') {
      this.setSelectedItems(e.target);
      this.hasActiveItems = this.activeList.length > 0 ? true : false;
      const dataKey = e.target.closest('.digi-form-filter__item')
        ? e.target.closest('.digi-form-filter__item').getAttribute('data-key')
        : this.activeListItemIndex;
      this.activeListItemIndex = Number(dataKey);
      this.afOnChange.emit(e);
    }
  }
  debounce(func, timeout = 300) {
    let timer;
    return (...args) => {
      clearTimeout(timer);
      timer = setTimeout(() => {
        func.apply(this, args);
      }, timeout);
    };
  }
  setSelectedItems(item) {
    this.activeList.some((el) => el.id === item.id)
      ? (this.activeList = this.activeList.filter((f) => f.id !== item.id))
      : this.activeList.push(item);
  }
  setInactive(focusToggleButton = false) {
    this.isActive = false;
    if (focusToggleButton) {
      document.getElementById(`${this.afId}-toggle-button`).focus();
    }
    document.querySelector(`#${this.afId}-dropdown-menu .digi-form-filter__scroll`).scrollTop = 0;
  }
  setActive() {
    this.isActive = true;
    this.activeListItemIndex = 0;
    setTimeout(() => {
      this.focusActiveItem();
    }, 200);
  }
  toggleDropdown() {
    return this.isActive ? this.setInactive() : this.setActive();
  }
  inputItem(identifier, index) {
    const item = document.querySelector(`#${identifier} .digi-form-filter__item:nth-child(${index}) input`);
    return item;
  }
  getFilterData() {
    return {
      filter: this.afName,
      items: this.activeList.map((item) => {
        return {
          id: item.id,
          text: item.labels[0].htmlFor === item.id ? item.labels[0].innerText : ''
        };
      })
    };
  }
  emitFilterClosedEvent() {
    this.setInactive(true);
    this.afOnFilterClosed.emit(this.getFilterData());
  }
  submitFilters(setInactive = true) {
    if (setInactive) {
      this.setInactive(true);
    }
    this.afOnSubmitFilters.emit(this.getFilterData());
  }
  resetFilters() {
    this.listItems.map((_, i) => {
      this.inputItem(`${this.afId}-filter-list`, i + 1).checked = false;
    });
    this.activeList = [];
    this.hasActiveItems = false;
    this.afOnResetFilters.emit(this.afName);
  }
  setupListElements(collection) {
    if (!collection || collection.length <= 0) {
      logger.warn(`The slot contains no filter elements.`, this.hostElement);
      return;
    }
    this.listItems = Array.from(collection).map((element, i) => ({
      index: i,
      outerHTML: element.cloneNode().outerHTML
    }));
  }
  componentWillLoad() {
    this.setupListElements(this.hostElement.children);
  }
  setupActiveItems() {
    this.setTabIndex();
    setTimeout(() => {
      const rootElement = document.getElementById(`${this.afId}-identifier`);
      let activeInputs = Array.from(rootElement === null || rootElement === void 0 ? void 0 : rootElement.querySelectorAll('.digi-form-filter__list input:checked'));
      this.activeList = [];
      activeInputs.forEach((inputEl) => {
        this.setSelectedItems(inputEl);
      });
      this.hasActiveItems = this.activeList.length > 0 ? true : false;
    });
  }
  componentDidLoad() {
    this.setupActiveItems();
  }
  componentDidUpdate() {
    if (!this._updateActiveItems) {
      return;
    }
    this._updateActiveItems = false;
    this.setupActiveItems();
  }
  focusActiveItem() {
    const element = this.inputItem(`${this.afId}-filter-list`, this.activeListItemIndex + 1);
    element.focus();
  }
  incrementActiveItem() {
    if (this.activeListItemIndex < this.listItems.length - 1) {
      this.activeListItemIndex = this.activeListItemIndex + 1;
      this.focusActiveItem();
      return;
    }
    this.tabHandler();
  }
  setTabIndex() {
    const tabindexes = document.querySelectorAll(`#${this.afId}-dropdown-menu`);
    if (!tabindexes) {
      logger.warn(`No input elements was found to set tabindex on.`, this.hostElement);
      return;
    }
    Array.from(tabindexes).map((tabindex) => {
      tabindex.setAttribute('tabindex', '-1');
    });
  }
  updateTabIndex(element) {
    if (!element) {
      logger.warn(`No input element was found to update tabindex on.`, this.hostElement);
      return;
    }
    element.setAttribute('tabindex', '0');
  }
  decrementActiveItem() {
    if (this.activeListItemIndex === 0) {
      this._toggleButton.querySelector('button').focus();
      return;
    }
    if (this.activeListItemIndex > 0) {
      this.activeListItemIndex = this.activeListItemIndex - 1;
    }
    this.focusActiveItem();
  }
  downHandler() {
    this.incrementActiveItem();
  }
  upHandler() {
    this.decrementActiveItem();
  }
  homeHandler() {
    this.activeListItemIndex = 0;
    this.focusActiveItem();
  }
  endHandler() {
    this.activeListItemIndex = this.listItems.length - 1;
    this.focusActiveItem();
  }
  enterHandler(e) {
    e.detail.target.click();
  }
  escHandler() {
    if (this.isActive) {
      this.setInactive(true);
    }
  }
  tabHandler() {
    if (this.isActive && this.focusInList) {
      setTimeout(() => {
        this._submitButton.querySelector('button').focus();
      }, 10);
    }
  }
  shiftHandler() {
    if (this.isActive && this.focusInList) {
      this._toggleButton.querySelector('button').focus();
      this.activeListItemIndex =
        this.activeListItemIndex >= 0
          ? this.activeListItemIndex - 1
          : this.activeListItemIndex;
    }
  }
  get cssModifiers() {
    return {
      'digi-form-filter--open': this.isActive
    };
  }
  get cssModifiersToggleButtonIndicator() {
    return {
      'digi-form-filter__toggle-button-indicator--active': this.hasActiveItems
    };
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-form-filter': true }, this.cssModifiers), role: "application", id: `${this.afId}-identifier` }, h("digi-util-keydown-handler", { onAfOnEsc: () => this.escHandler() }, h("digi-button", { id: `${this.afId}-toggle-button`, class: "digi-form-filter__toggle-button", onAfOnClick: () => this.toggleDropdown(), ref: (el) => (this._toggleButton = el), "af-aria-haspopup": "true", "af-variation": "secondary", "af-aria-label": this.afFilterButtonAriaLabel, "af-aria-labelledby": this.afFilterButtonAriaDescribedby, "af-aria-controls": `${this.afId}-dropdown-menu`, "af-aria-expanded": this.isActive, "af-autofocus": this.afAutofocus ? this.afAutofocus : null }, h("i", { class: Object.assign({ 'digi-form-filter__toggle-button-indicator': true }, this.cssModifiersToggleButtonIndicator) }), h("span", { class: "digi-form-filter__toggle-button-text" }, this.afFilterButtonText), h("digi-icon", { class: "digi-form-filter__toggle-icon", slot: "icon-secondary", afName: `chevron-down` })), h("div", { hidden: !this.isActive, id: `${this.afId}-dropdown-menu`, class: {
        'digi-form-filter__dropdown': true,
        'digi-form-filter__dropdown--right': this.afAlignRight
      } }, h("div", { class: "digi-form-filter__scroll" }, h("fieldset", null, h("legend", { class: "digi-form-filter__legend" }, this.afName), h("digi-util-keydown-handler", { onAfOnDown: () => this.downHandler(), onAfOnUp: () => this.upHandler(), onAfOnHome: () => this.homeHandler(), onAfOnEnd: () => this.endHandler(), onAfOnEnter: (e) => this.enterHandler(e), onAfOnTab: () => this.tabHandler(), onAfOnShiftTab: () => this.shiftHandler() }, h("ul", { class: "digi-form-filter__list", "aria-labelledby": `${this.afId}-toggle-button`, id: `${this.afId}-filter-list` }, this.listItems.map((item) => {
      return (h("li", { class: "digi-form-filter__item", key: item.index, "data-key": item.index, innerHTML: item.outerHTML }));
    }))), h("digi-util-mutation-observer", { hidden: true, ref: (el) => (this._observer = el), onAfOnChange: this.debounce(() => {
        this.setupListElements(this._observer.children);
        this._updateActiveItems = true;
      }) }, h("slot", null)))), h("div", { class: "digi-form-filter__footer" }, h("digi-util-keydown-handler", { onAfOnUp: () => this.focusActiveItem() }, h("digi-button", { ref: (el) => (this._submitButton = el), "af-variation": "primary", "af-aria-label": this.afSubmitButtonTextAriaLabel, onClick: () => {
        this.submitFilters();
        this._toggleButton.querySelector('button').focus();
      }, class: "digi-form-filter__submit-button" }, this.afSubmitButtonText)), !this.afHideResetButton && (h("digi-button", { "af-variation": "secondary", "af-aria-label": this.afResetButtonTextAriaLabel, onClick: () => this.resetFilters(), class: "digi-form-filter__reset-button" }, this.afResetButtonText)))))));
  }
  get hostElement() { return this; }
  static get style() { return formFilterCss; }
}, [6, "digi-form-filter", {
    "afId": [1, "af-id"],
    "afFilterButtonText": [1, "af-filter-button-text"],
    "afSubmitButtonText": [1, "af-submit-button-text"],
    "afSubmitButtonTextAriaLabel": [1, "af-submit-button-text-aria-label"],
    "afResetButtonText": [1, "af-reset-button-text"],
    "afResetButtonTextAriaLabel": [1, "af-reset-button-text-aria-label"],
    "afHideResetButton": [4, "af-hide-reset-button"],
    "afAlignRight": [4, "af-align-right"],
    "afName": [1, "af-name"],
    "afFilterButtonAriaLabel": [1, "af-filter-button-aria-label"],
    "afFilterButtonAriaDescribedby": [1, "af-filter-button-aria-describedby"],
    "afAutofocus": [4, "af-autofocus"],
    "isActive": [32],
    "activeListItemIndex": [32],
    "hasActiveItems": [32],
    "focusInList": [32],
    "listItems": [32]
  }, [[8, "click", "clickHandler"], [4, "focusin", "focusoutHandler"], [0, "change", "changeHandler"]]]);
function defineCustomElement$1() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-form-filter", "digi-button", "digi-icon", "digi-util-keydown-handler", "digi-util-mutation-observer"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-form-filter":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, formFilter);
      }
      break;
    case "digi-button":
      if (!customElements.get(tagName)) {
        defineCustomElement$5();
      }
      break;
    case "digi-icon":
      if (!customElements.get(tagName)) {
        defineCustomElement$4();
      }
      break;
    case "digi-util-keydown-handler":
      if (!customElements.get(tagName)) {
        defineCustomElement$3();
      }
      break;
    case "digi-util-mutation-observer":
      if (!customElements.get(tagName)) {
        defineCustomElement$2();
      }
      break;
  } });
}
defineCustomElement$1();

const DigiFormFilter = formFilter;
const defineCustomElement = defineCustomElement$1;

export { DigiFormFilter, defineCustomElement };
