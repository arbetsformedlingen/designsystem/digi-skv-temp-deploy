import { L as Link, d as defineCustomElement$1 } from './link.js';

const DigiLink = Link;
const defineCustomElement = defineCustomElement$1;

export { DigiLink, defineCustomElement };
