import type { Components, JSX } from "../dist/types/components";

interface DigiNavigationToc extends Components.DigiNavigationToc, HTMLElement {}
export const DigiNavigationToc: {
  prototype: DigiNavigationToc;
  new (): DigiNavigationToc;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
