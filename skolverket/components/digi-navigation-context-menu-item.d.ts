import type { Components, JSX } from "../dist/types/components";

interface DigiNavigationContextMenuItem extends Components.DigiNavigationContextMenuItem, HTMLElement {}
export const DigiNavigationContextMenuItem: {
  prototype: DigiNavigationContextMenuItem;
  new (): DigiNavigationContextMenuItem;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
