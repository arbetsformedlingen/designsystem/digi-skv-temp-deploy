import { F as FormRadiobutton, d as defineCustomElement$1 } from './form-radiobutton.js';

const DigiFormRadiobutton = FormRadiobutton;
const defineCustomElement = defineCustomElement$1;

export { DigiFormRadiobutton, defineCustomElement };
