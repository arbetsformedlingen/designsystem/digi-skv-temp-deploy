import type { Components, JSX } from "../dist/types/components";

interface DigiMediaImage extends Components.DigiMediaImage, HTMLElement {}
export const DigiMediaImage: {
  prototype: DigiMediaImage;
  new (): DigiMediaImage;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
