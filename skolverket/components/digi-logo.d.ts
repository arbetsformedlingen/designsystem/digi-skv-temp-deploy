import type { Components, JSX } from "../dist/types/components";

interface DigiLogo extends Components.DigiLogo, HTMLElement {}
export const DigiLogo: {
  prototype: DigiLogo;
  new (): DigiLogo;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
