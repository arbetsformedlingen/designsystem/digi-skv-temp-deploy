import type { Components, JSX } from "../dist/types/components";

interface DigiIcon extends Components.DigiIcon, HTMLElement {}
export const DigiIcon: {
  prototype: DigiIcon;
  new (): DigiIcon;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
