import { proxyCustomElement, HTMLElement, h } from '@stencil/core/internal/client';
import './button-variation.enum.js';
import './expandable-accordion-variation.enum.js';
import './calendar-week-view-heading-level.enum.js';
import './code-block-variation.enum.js';
import './code-example-variation.enum.js';
import './code-variation.enum.js';
import './form-checkbox-variation.enum.js';
import './form-file-upload-variation.enum.js';
import './form-input-search-variation.enum.js';
import './form-input-variation.enum.js';
import './form-radiobutton-variation.enum.js';
import './form-select-variation.enum.js';
import './form-textarea-variation.enum.js';
import './form-validation-message-variation.enum.js';
import './layout-block-variation.enum.js';
import './layout-columns-variation.enum.js';
import './layout-container-variation.enum.js';
import './layout-media-object-alignment.enum.js';
import './link-external-variation.enum.js';
import './link-internal-variation.enum.js';
import './link-variation.enum.js';
import './loader-spinner-size.enum.js';
import './media-figure-alignment.enum.js';
import './navigation-context-menu-item-type.enum.js';
import './navigation-sidebar-variation.enum.js';
import './navigation-vertical-menu-variation.enum.js';
import './progress-step-variation.enum.js';
import './progress-steps-variation.enum.js';
import './progressbar-variation.enum.js';
import './tag-size.enum.js';
import './typography-meta-variation.enum.js';
import './typography-time-variation.enum.js';
import { T as TypographyVariation } from './typography-variation.enum.js';
import './util-breakpoint-observer-breakpoints.enum.js';
import { a as ListLinkType, L as ListLinkLayout, b as ListLinkVariation } from './list-link-variation.enum.js';
import { d as defineCustomElement$2 } from './typography.js';

const listLinkCss = ".sc-digi-list-link-h{--digi--list-link--direction:var(--digi--list-link--direction--override, column);--digi--list-link--border:var(--digi--border-width--tertiary) var(--digi--border-style--primary) var(--digi--global--color--profile--purple--opacity15)}[af-layout=inline].sc-digi-list-link-h{--digi--list-link--direction:var(--digi--list-link--direction--override, row);--digi--list-link--border:none}.digi-list-link__content.sc-digi-list-link{display:flex;flex-direction:var(--digi--list-link--direction)}.digi-list-link__list.sc-digi-list-link{display:flex;flex-direction:var(--digi--list-link--direction);flex-wrap:wrap;counter-reset:list}.digi-list-link__heading.sc-digi-list-link{padding:calc(var(--digi--grid-gutter--smaller) * 1.5) var(--digi--grid-gutter--smaller);--FONT-SIZE--HEADING-1:var(--digi--global--typography--font-size--larger);--FONT-SIZE--HEADING-2:var(--digi--global--typography--font-size--larger);--FONT-SIZE--HEADING-3:var(--digi--global--typography--font-size--larger);--FONT-SIZE--HEADING-4:var(--digi--global--typography--font-size--larger);--FONT-SIZE--HEADING-5:var(--digi--global--typography--font-size--larger);--FONT-SIZE--HEADING-6:var(--digi--global--typography--font-size--larger);--LINE-HEIGHT--HEADING-1:var(--LINE-HEIGHT--LINK);--LINE-HEIGHT--HEADING-2:var(--LINE-HEIGHT--LINK);--LINE-HEIGHT--HEADING-3:var(--LINE-HEIGHT--LINK);--LINE-HEIGHT--HEADING-4:var(--LINE-HEIGHT--LINK);--LINE-HEIGHT--HEADING-5:var(--LINE-HEIGHT--LINK);--LINE-HEIGHT--HEADING-6:var(--LINE-HEIGHT--LINK)}.digi-list-link--layout-block.sc-digi-list-link .digi-list-link__heading.sc-digi-list-link{-webkit-padding-before:0;padding-block-start:0}.sc-digi-list-link-s>li{list-style:none;padding:0;margin:0}.digi-list-link--variation-regular.sc-digi-list-link-s>li,.digi-list-link--variation-regular .sc-digi-list-link-s>li{-webkit-border-after:var(--digi--list-link--border);border-block-end:var(--digi--list-link--border)}.sc-digi-list-link-s>li a{font-family:var(--digi--global--typography--font-family--default);text-decoration:none;color:var(--digi--color--text--secondary);font-size:var(--digi--global--typography--font-size--large);letter-spacing:calc(var(--digi--global--typography--font-size--large) / 100 * -1);font-weight:var(--digi--global--typography--font-weight--semibold);display:block;border-radius:var(--digi--border-radius--primary);padding:calc(var(--digi--grid-gutter--smaller) * 1.5) var(--digi--grid-gutter--smaller)}.sc-digi-list-link-s>li a:hover{color:var(--digi--global--color--profile--purple--dark);text-decoration:underline}.sc-digi-list-link-s>li a:focus{outline:none;color:var(--digi--color--text--secondary)}.sc-digi-list-link-s>li a:focus-visible{outline:var(--digi--border-width--secondary) solid var(--digi--color--border--focus)}.sc-digi-list-link-s>li a:visited{color:var(--digi--color--text--secondary)}.sc-digi-list-link-s>li a::after{background-color:currentColor;padding:0 0.5em;-webkit-mask-image:url(\"data:image/svg+xml;charset=utf-8, %3Csvg%0A%09width%3D%2224%22%0A%09height%3D%2224%22%0A%09viewBox%3D%220%200%2024%2024%22%0A%09xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%0A%3E%0A%09%3Cpath%0A%09%09d%3D%22M288.917%2C415.517%2C287.5%2C414.1l3.96-3.96-3.96-3.96%2C1.414-1.414%2C5.373%2C5.374Z%22%0A%09%09transform%3D%22translate(-278.17%20-398.103)%22%0A%09%2F%3E%0A%3C%2Fsvg%3E\");mask-image:url(\"data:image/svg+xml;charset=utf-8, %3Csvg%0A%09width%3D%2224%22%0A%09height%3D%2224%22%0A%09viewBox%3D%220%200%2024%2024%22%0A%09xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%0A%3E%0A%09%3Cpath%0A%09%09d%3D%22M288.917%2C415.517%2C287.5%2C414.1l3.96-3.96-3.96-3.96%2C1.414-1.414%2C5.373%2C5.374Z%22%0A%09%09transform%3D%22translate(-278.17%20-398.103)%22%0A%09%2F%3E%0A%3C%2Fsvg%3E\");-webkit-mask-repeat:no-repeat;mask-repeat:no-repeat;-webkit-mask-position:center;mask-position:center;-webkit-clip-path:padding-box inset(0.25em 0);clip-path:padding-box inset(0.25em 0);content:\"\"}.digi-list-link--variation-compact.sc-digi-list-link-s>li a,.digi-list-link--variation-compact .sc-digi-list-link-s>li a{padding-block:0}.digi-list-link--type-ol.sc-digi-list-link-s>li a,.digi-list-link--type-ol .sc-digi-list-link-s>li a{counter-increment:list}.digi-list-link--type-ol.sc-digi-list-link-s>li a::before,.digi-list-link--type-ol .sc-digi-list-link-s>li a::before{content:counter(list, upper-alpha);-webkit-margin-end:var(--digi--global--spacing--smaller);margin-inline-end:var(--digi--global--spacing--smaller);display:inline-block}.digi-list-link__list.sc-digi-list-link{list-style:none;padding:0;margin:0}";

const ListLink = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.hasHeading = undefined;
    this.afType = ListLinkType.UNORDERED;
    this.afLayout = ListLinkLayout.BLOCK;
    this.afVariation = ListLinkVariation.REGULAR;
  }
  componentWillLoad() {
    this.setHasHeading();
  }
  componentWillUpdate() {
    this.setHasHeading();
  }
  setHasHeading() {
    this.hasHeading = !!this.hostElement.querySelector('[slot="heading"]');
  }
  get cssModifiers() {
    return {
      [`digi-list-link--type-${this.afType}`]: !!this.afType,
      [`digi-list-link--layout-${this.afLayout}`]: !!this.afLayout,
      [`digi-list-link--variation-${this.afVariation}`]: !!this.afVariation
    };
  }
  render() {
    return (h("digi-typography", { class: Object.assign({ 'digi-list-link': true }, this.cssModifiers), afVariation: TypographyVariation.SMALL }, h("div", { class: "digi-list-link__content" }, this.hasHeading && (h("div", { class: "digi-list-link__heading" }, h("slot", { name: "heading" }))), h(this.afType, { class: "digi-list-link__list" }, h("slot", null)))));
  }
  get hostElement() { return this; }
  static get style() { return listLinkCss; }
}, [6, "digi-list-link", {
    "afType": [1, "af-type"],
    "afLayout": [1, "af-layout"],
    "afVariation": [1, "af-variation"],
    "hasHeading": [32]
  }]);
function defineCustomElement$1() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-list-link", "digi-typography"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-list-link":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, ListLink);
      }
      break;
    case "digi-typography":
      if (!customElements.get(tagName)) {
        defineCustomElement$2();
      }
      break;
  } });
}
defineCustomElement$1();

const DigiListLink = ListLink;
const defineCustomElement = defineCustomElement$1;

export { DigiListLink, defineCustomElement };
