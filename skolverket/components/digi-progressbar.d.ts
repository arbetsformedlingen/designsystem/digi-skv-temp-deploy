import type { Components, JSX } from "../dist/types/components";

interface DigiProgressbar extends Components.DigiProgressbar, HTMLElement {}
export const DigiProgressbar: {
  prototype: DigiProgressbar;
  new (): DigiProgressbar;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
