import type { Components, JSX } from "../dist/types/components";

interface DigiDialog extends Components.DigiDialog, HTMLElement {}
export const DigiDialog: {
  prototype: DigiDialog;
  new (): DigiDialog;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
