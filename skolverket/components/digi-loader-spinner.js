import { proxyCustomElement, HTMLElement, h, Host } from '@stencil/core/internal/client';
import { L as LoaderSpinnerSize } from './loader-spinner-size.enum.js';
import { d as defineCustomElement$2 } from './icon.js';

const loaderSpinnerCss = ".sc-digi-loader-spinner-h{--digi--loader-spinner--icon--size--small:1rem;--digi--loader-spinner--icon--size--medium:2rem;--digi--loader-spinner--icon--size--large:5rem;--digi-loader-spinner--font--size--small:var(--digi--typography--heading-4--font-size--desktop-xsmall);--digi-loader-spinner--font--size--medium:var(--digi--typography--heading-4--font-size--desktop);--digi-loader-spinner--font--size--large:var(--digi--typography--heading-4--font-size--desktop-large)}.sc-digi-loader-spinner-h .digi-loader-spinner.sc-digi-loader-spinner{display:flex;flex-direction:column;align-items:center;width:-moz-max-content;width:max-content;padding:var(--PADDING)}.sc-digi-loader-spinner-h .digi-loader-spinner--small.sc-digi-loader-spinner{--ICON-HEIGHT:var(--digi--loader-spinner--icon--size--small);--FONT-SIZE:var(--digi-loader-spinner--font--size--small);--PADDING:0}.sc-digi-loader-spinner-h .digi-loader-spinner--medium.sc-digi-loader-spinner{--ICON-HEIGHT:var(--digi--loader-spinner--icon--size--medium);--FONT-SIZE:var(--digi-loader-spinner--font--size--medium);--PADDING:var(--digi--gutter--large)}.sc-digi-loader-spinner-h .digi-loader-spinner--large.sc-digi-loader-spinner{--ICON-HEIGHT:var(--digi--loader-spinner--icon--size--large);--FONT-SIZE:var(--digi-loader-spinner--font--size--large);--PADDING:var(--digi--gutter--large)}.sc-digi-loader-spinner-h .digi-loader-spinner__container.sc-digi-loader-spinner{height:-moz-max-content;height:max-content;width:-moz-max-content;width:max-content}.sc-digi-loader-spinner-h .digi-loader-spinner__spinner.sc-digi-loader-spinner{animation:animation__rotation 3s infinite linear;transform-origin:center;--digi--icon--height:var(--ICON-HEIGHT)}@media (prefers-reduced-motion){.sc-digi-loader-spinner-h .digi-loader-spinner__spinner.sc-digi-loader-spinner{animation:none}}@keyframes animation__rotation{from{transform:rotate(0deg)}to{transform:rotate(359deg)}}.sc-digi-loader-spinner-h .digi-loader-spinner__label.sc-digi-loader-spinner{margin-top:var(--digi--gutter--medium)}";

const LoaderSpinner = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afSize = LoaderSpinnerSize.MEDIUM;
    this.afText = undefined;
  }
  componentWillLoad() { }
  componentDidLoad() { }
  componentWillUpdate() { }
  get cssModifiers() {
    return {
      'digi-loader-spinner--small': this.afSize == 'small',
      'digi-loader-spinner--medium': this.afSize == 'medium',
      'digi-loader-spinner--large': this.afSize == 'large'
    };
  }
  render() {
    return (h(Host, null, h("div", { "aria-label": this.afText, "aria-live": "assertive", class: Object.assign({ 'digi-loader-spinner': true }, this.cssModifiers) }, h("div", { class: "digi-loader-spinner__container", "aria-hidden": "true" }, h("digi-icon", { class: "digi-loader-spinner__spinner", afName: `spinner` })), this.afText && (h("div", { class: "digi-loader-spinner__label" }, this.afText)))));
  }
  get hostElement() { return this; }
  static get style() { return loaderSpinnerCss; }
}, [2, "digi-loader-spinner", {
    "afSize": [1, "af-size"],
    "afText": [1, "af-text"]
  }]);
function defineCustomElement$1() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-loader-spinner", "digi-icon"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-loader-spinner":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, LoaderSpinner);
      }
      break;
    case "digi-icon":
      if (!customElements.get(tagName)) {
        defineCustomElement$2();
      }
      break;
  } });
}
defineCustomElement$1();

const DigiLoaderSpinner = LoaderSpinner;
const defineCustomElement = defineCustomElement$1;

export { DigiLoaderSpinner, defineCustomElement };
