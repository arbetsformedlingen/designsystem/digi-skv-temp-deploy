import { F as FormFieldset, d as defineCustomElement$1 } from './form-fieldset.js';

const DigiFormFieldset = FormFieldset;
const defineCustomElement = defineCustomElement$1;

export { DigiFormFieldset, defineCustomElement };
