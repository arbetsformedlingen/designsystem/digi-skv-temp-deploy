import { D as Dialog, d as defineCustomElement$1 } from './dialog.js';

const DigiDialog = Dialog;
const defineCustomElement = defineCustomElement$1;

export { DigiDialog, defineCustomElement };
