import type { Components, JSX } from "../dist/types/components";

interface DigiLoaderSpinner extends Components.DigiLoaderSpinner, HTMLElement {}
export const DigiLoaderSpinner: {
  prototype: DigiLoaderSpinner;
  new (): DigiLoaderSpinner;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
