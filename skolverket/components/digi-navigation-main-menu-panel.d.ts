import type { Components, JSX } from "../dist/types/components";

interface DigiNavigationMainMenuPanel extends Components.DigiNavigationMainMenuPanel, HTMLElement {}
export const DigiNavigationMainMenuPanel: {
  prototype: DigiNavigationMainMenuPanel;
  new (): DigiNavigationMainMenuPanel;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
