import { proxyCustomElement, HTMLElement, h } from '@stencil/core/internal/client';
import { N as NavigationBreadcrumbsVariation } from './navigation-breadcrumbs-variation.enum.js';

const navigationBreadcrumbsCss = ".sc-digi-navigation-breadcrumbs-h{--digi--navigation-breadcrumbs--items--display:inline-flex;--digi--navigation-breadcrumbs--items--flex-wrap:wrap;--digi--navigation-breadcrumbs--items--padding:var(--digi--gutter--icon);--digi--navigation-breadcrumbs--items--background:var(--digi--color--background--primary);--digi--navigation-breadcrumbs--items--margin:0;--digi--navigation-breadcrumbs--item--font-family:var(--digi--global--typography--font-family--default);--digi--navigation-breadcrumbs--item--font-size:var(--digi--typography--breadcrumbs--font-size--desktop);--digi--navigation-breadcrumbs--item--text-decoration:var(--digi--typography--breadcrumbs--text-decoration--desktop);--digi--navigation-breadcrumbs--item--color:var(--digi--color--text--primary);--digi--navigation-breadcrumbs--divider--padding:var(--digi--gutter--icon);--digi--navigation-breadcrumbs--link--visited--color:var(--digi--color--text--link-visited);--digi--navigation-breadcrumbs--link--hover--focus--color:var(--digi--color--text--link-hover);--digi--navigation-breadcrumbs--link--hover--focus--text-decoration:underline;--digi--navigation-breadcrumbs--link--color:var(--digi--color--text--link)}.sc-digi-navigation-breadcrumbs-h .digi-navigation-breadcrumbs__items.sc-digi-navigation-breadcrumbs{list-style:none;display:var(--digi--navigation-breadcrumbs--items--display);flex-wrap:var(--digi--navigation-breadcrumbs--items--flex-wrap);margin:var(--digi--navigation-breadcrumbs--items--margin);padding:var(--digi--navigation-breadcrumbs--items--padding);background:var(--digi--navigation-breadcrumbs--items--background)}.sc-digi-navigation-breadcrumbs-h li.sc-digi-navigation-breadcrumbs{font-family:var(--digi--navigation-breadcrumbs--item--font-family);font-size:var(--digi--navigation-breadcrumbs--item--font-size);color:var(--digi--navigation-breadcrumbs--item--color)}.sc-digi-navigation-breadcrumbs-h li.sc-digi-navigation-breadcrumbs:after{content:none}.sc-digi-navigation-breadcrumbs-s>li{font-family:var(--digi--navigation-breadcrumbs--item--font-family);font-size:var(--digi--navigation-breadcrumbs--item--font-size);color:var(--digi--navigation-breadcrumbs--item--color)}.sc-digi-navigation-breadcrumbs-s>li::after{content:\"/\";padding-inline:var(--digi--navigation-breadcrumbs--divider--padding)}.sc-digi-navigation-breadcrumbs-s>li a{text-decoration:none;color:var(--digi--navigation-breadcrumbs--link--color);font-size:var(--digi--navigation-breadcrumbs--item--font-size);-webkit-text-decoration:var(--digi--navigation-breadcrumbs--item--text-decoration);text-decoration:var(--digi--navigation-breadcrumbs--item--text-decoration)}.sc-digi-navigation-breadcrumbs-s>li a:visited{color:var(--digi--navigation-breadcrumbs--link--visited--color)}.sc-digi-navigation-breadcrumbs-s>li a:focus,.sc-digi-navigation-breadcrumbs-s>li a:hover{-webkit-text-decoration:var(--digi--navigation-breadcrumbs--link--hover--focus--text-decoration);text-decoration:var(--digi--navigation-breadcrumbs--link--hover--focus--text-decoration);--digi--navigation-breadcrumbs--link--color:var(--digi--navigation-breadcrumbs--link--hover--focus--color)}.sc-digi-navigation-breadcrumbs-s>li a:focus-visible{outline:var(--digi--focus-outline);border-radius:var(--digi--global--border-radius--small)}@media (max-width: 47.9375rem){.sc-digi-navigation-breadcrumbs-h .digi-navigation-breadcrumbs--variation-compressed .sc-digi-navigation-breadcrumbs-s>li:nth-last-child(2)::after{content:none}.sc-digi-navigation-breadcrumbs-h .digi-navigation-breadcrumbs--variation-compressed .sc-digi-navigation-breadcrumbs-s>li:nth-last-child(2) a{display:inline-flex;gap:calc(var(--digi--navigation-breadcrumbs--divider--padding) / 2);align-items:center}.sc-digi-navigation-breadcrumbs-h .digi-navigation-breadcrumbs--variation-compressed .sc-digi-navigation-breadcrumbs-s>li:nth-last-child(2) a::before{background-color:currentColor;padding:0 0.5em;-webkit-mask-image:url(\"data:image/svg+xml;charset=utf-8, %3Csvg%0A%09%09%09%09%09width%3D%229%22%0A%09%09%09%09%09height%3D%2214%22%0A%09%09%09%09%09viewBox%3D%220%200%209%2014%22%0A%09%09%09%09%09xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%0A%09%09%09%09%09%0A%09%09%09%09%3E%0A%09%09%09%09%09%0A%09%09%09%09%09%3Cpath%0A%09%09%09%09%09%09d%3D%22M8%2013L2%206.926%207.853%201%22%0A%09%09%09%09%09%09stroke%3D%22currentColor%22%0A%09%09%09%09%09%09stroke-width%3D%222%22%0A%09%09%09%09%09%09fill%3D%22none%22%0A%09%09%09%09%09%09fill-rule%3D%22evenodd%22%0A%09%09%09%09%09%2F%3E%0A%09%09%09%09%3C%2Fsvg%3E\");mask-image:url(\"data:image/svg+xml;charset=utf-8, %3Csvg%0A%09%09%09%09%09width%3D%229%22%0A%09%09%09%09%09height%3D%2214%22%0A%09%09%09%09%09viewBox%3D%220%200%209%2014%22%0A%09%09%09%09%09xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%0A%09%09%09%09%09%0A%09%09%09%09%3E%0A%09%09%09%09%09%0A%09%09%09%09%09%3Cpath%0A%09%09%09%09%09%09d%3D%22M8%2013L2%206.926%207.853%201%22%0A%09%09%09%09%09%09stroke%3D%22currentColor%22%0A%09%09%09%09%09%09stroke-width%3D%222%22%0A%09%09%09%09%09%09fill%3D%22none%22%0A%09%09%09%09%09%09fill-rule%3D%22evenodd%22%0A%09%09%09%09%09%2F%3E%0A%09%09%09%09%3C%2Fsvg%3E\");-webkit-mask-repeat:no-repeat;mask-repeat:no-repeat;-webkit-mask-position:center;mask-position:center;-webkit-clip-path:padding-box inset(0.25em 0);clip-path:padding-box inset(0.25em 0);content:\"\";height:0.5em}.sc-digi-navigation-breadcrumbs-h .digi-navigation-breadcrumbs--variation-compressed .sc-digi-navigation-breadcrumbs-s>li:not(:nth-last-child(2)){display:none}}";

const NavigationBreadcrumbs = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afAriaLabel = 'breadcrumbs';
    this.afCurrentPage = undefined;
    this.afVariation = NavigationBreadcrumbsVariation.REGULAR;
  }
  get cssModifiers() {
    return {
      [`digi-navigation-breadcrumbs--variation-${this.afVariation}`]: !!this.afVariation
    };
  }
  render() {
    return (h("nav", { class: Object.assign({ 'digi-navigation-breadcrumbs': true }, this.cssModifiers), "aria-label": this.afAriaLabel }, h("ol", { class: "digi-navigation-breadcrumbs__items" }, h("slot", null), h("li", { "aria-current": "page" }, this.afCurrentPage))));
  }
  static get style() { return navigationBreadcrumbsCss; }
}, [6, "digi-navigation-breadcrumbs", {
    "afAriaLabel": [1, "af-aria-label"],
    "afCurrentPage": [1, "af-current-page"],
    "afVariation": [1, "af-variation"]
  }]);
function defineCustomElement$1() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-navigation-breadcrumbs"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-navigation-breadcrumbs":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, NavigationBreadcrumbs);
      }
      break;
  } });
}
defineCustomElement$1();

const DigiNavigationBreadcrumbs = NavigationBreadcrumbs;
const defineCustomElement = defineCustomElement$1;

export { DigiNavigationBreadcrumbs, defineCustomElement };
