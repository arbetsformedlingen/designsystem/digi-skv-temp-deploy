var FormInputButtonVariation;
(function (FormInputButtonVariation) {
  FormInputButtonVariation["PRIMARY"] = "primary";
  FormInputButtonVariation["SECONDARY"] = "secondary";
})(FormInputButtonVariation || (FormInputButtonVariation = {}));

var FormInputType;
(function (FormInputType) {
  FormInputType["COLOR"] = "color";
  FormInputType["DATE"] = "date";
  FormInputType["DATETIME_LOCAL"] = "datetime-local";
  FormInputType["EMAIL"] = "email";
  FormInputType["HIDDEN"] = "hidden";
  FormInputType["MONTH"] = "month";
  FormInputType["NUMBER"] = "number";
  FormInputType["PASSWORD"] = "password";
  FormInputType["SEARCH"] = "search";
  FormInputType["TEL"] = "tel";
  FormInputType["TEXT"] = "text";
  FormInputType["TIME"] = "time";
  FormInputType["URL"] = "url";
  FormInputType["WEEK"] = "week";
})(FormInputType || (FormInputType = {}));

var FormInputValidation;
(function (FormInputValidation) {
  FormInputValidation["SUCCESS"] = "success";
  FormInputValidation["ERROR"] = "error";
  FormInputValidation["WARNING"] = "warning";
  FormInputValidation["NEUTRAL"] = "neutral";
})(FormInputValidation || (FormInputValidation = {}));

var FormInputVariation;
(function (FormInputVariation) {
  FormInputVariation["SMALL"] = "small";
  FormInputVariation["MEDIUM"] = "medium";
  FormInputVariation["LARGE"] = "large";
})(FormInputVariation || (FormInputVariation = {}));

export { FormInputButtonVariation as F, FormInputType as a, FormInputValidation as b, FormInputVariation as c };
