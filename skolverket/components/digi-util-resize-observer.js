import { U as UtilResizeObserver, d as defineCustomElement$1 } from './util-resize-observer.js';

const DigiUtilResizeObserver = UtilResizeObserver;
const defineCustomElement = defineCustomElement$1;

export { DigiUtilResizeObserver, defineCustomElement };
