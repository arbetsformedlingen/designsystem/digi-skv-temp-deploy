import { proxyCustomElement, HTMLElement, h } from '@stencil/core/internal/client';

const typographyHeadingSectionCss = ".sc-digi-typography-heading-section-h .sc-digi-typography-heading-section-s>*{--digi--heading-3--font-size:var(--digi--typography--heading-3--font-size--mobile);--digi--heading-3--line-height:var(--digi--typography--heading-3--line-height--mobile);font-family:var(--digi--global--typography--font-family--default);font-size:var(--digi--heading-3--font-size);line-height:var(--digi--heading-3--line-height);font-weight:var(--digi--typography--heading-3--font-weight--mobile);-webkit-margin-after:0;margin-block-end:0;font-weight:var(--digi--global--typography--font-weight--regular)}@media (min-width: 48rem){.sc-digi-typography-heading-section-h .sc-digi-typography-heading-section-s>*{--digi--heading-3--font-size:var(--digi--typography--heading-3--font-size--desktop);--digi--heading-3--line-height:var(--digi--typography--heading-3--line-height--desktop)}}@media (min-width: 62rem){.sc-digi-typography-heading-section-h .sc-digi-typography-heading-section-s>*{--digi--heading-3--font-size:var(--digi--typography--heading-3--font-size--desktop-large);--digi--heading-3--line-height:var(--digi--typography--heading-3--line-height--desktop-large)}}.sc-digi-typography-heading-section-h .sc-digi-typography-heading-section-s>*::after{content:\"\";-webkit-margin-before:var(--digi--margin--text-small);margin-block-start:var(--digi--margin--text-small);display:block;width:var(--digi--container-gutter--medium);height:var(--digi--border-width--secondary);background:var(--digi--color--border--neutral-2)}";

const TypographyHeadingSection = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
  }
  render() {
    return (h("span", null, h("slot", null)));
  }
  static get style() { return typographyHeadingSectionCss; }
}, [6, "digi-typography-heading-section"]);
function defineCustomElement() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-typography-heading-section"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-typography-heading-section":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, TypographyHeadingSection);
      }
      break;
  } });
}
defineCustomElement();

export { TypographyHeadingSection as T, defineCustomElement as d };
