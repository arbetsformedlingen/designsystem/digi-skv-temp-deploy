import type { Components, JSX } from "../dist/types/components";

interface DigiNavigationTabInABox extends Components.DigiNavigationTabInABox, HTMLElement {}
export const DigiNavigationTabInABox: {
  prototype: DigiNavigationTabInABox;
  new (): DigiNavigationTabInABox;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
