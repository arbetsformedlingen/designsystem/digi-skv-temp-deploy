import type { Components, JSX } from "../dist/types/components";

interface DigiNotificationAlert extends Components.DigiNotificationAlert, HTMLElement {}
export const DigiNotificationAlert: {
  prototype: DigiNotificationAlert;
  new (): DigiNotificationAlert;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
