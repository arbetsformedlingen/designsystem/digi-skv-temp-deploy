import { proxyCustomElement, HTMLElement, createEvent, h } from '@stencil/core/internal/client';
import { r as randomIdGenerator } from './randomIdGenerator.util.js';
import { d as detectClickOutside } from './detectClickOutside.util.js';

const UtilDetectClickOutside = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afOnClickOutside = createEvent(this, "afOnClickOutside", 7);
    this.afOnClickInside = createEvent(this, "afOnClickInside", 7);
    this.afDataIdentifier = randomIdGenerator('data-digi-util-detect-click-outside');
    this.$el.setAttribute(this.afDataIdentifier, '');
  }
  clickHandler(e) {
    const target = e.target;
    const selector = `[${this.afDataIdentifier}]`;
    if (detectClickOutside(target, selector)) {
      this.afOnClickOutside.emit(e);
    }
    else {
      this.afOnClickInside.emit(e);
    }
  }
  render() {
    return h("slot", null);
  }
  get $el() { return this; }
}, [6, "digi-util-detect-click-outside", {
    "afDataIdentifier": [1, "af-data-identifier"]
  }, [[8, "click", "clickHandler"]]]);
function defineCustomElement() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-util-detect-click-outside"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-util-detect-click-outside":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, UtilDetectClickOutside);
      }
      break;
  } });
}
defineCustomElement();

export { UtilDetectClickOutside as U, defineCustomElement as d };
