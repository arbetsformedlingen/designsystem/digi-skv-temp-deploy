import type { Components, JSX } from "../dist/types/components";

interface DigiNavigationSidebarButton extends Components.DigiNavigationSidebarButton, HTMLElement {}
export const DigiNavigationSidebarButton: {
  prototype: DigiNavigationSidebarButton;
  new (): DigiNavigationSidebarButton;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
