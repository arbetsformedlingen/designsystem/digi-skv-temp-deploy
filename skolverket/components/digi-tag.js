import { proxyCustomElement, HTMLElement, createEvent, h } from '@stencil/core/internal/client';
import { T as TagSize } from './tag-size.enum.js';
import { d as defineCustomElement$3 } from './button.js';
import { d as defineCustomElement$2 } from './icon.js';

const tagCss = ".sc-digi-tag-h{--digi--tag--display:inline-block;--digi--tag--margin-bottom:var(--digi--gutter--smaller)}.sc-digi-tag-h .digi-tag.sc-digi-tag{display:var(--digi--tag--display);margin-bottom:var(--digi--tag--margin-bottom)}";

const Tag = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afOnClick = createEvent(this, "afOnClick", 7);
    this.afText = undefined;
    this.afNoIcon = false;
    this.afSize = TagSize.SMALL;
  }
  clickHandler(e) {
    this.afOnClick.emit(e);
  }
  render() {
    return (h("digi-button", { onAfOnClick: (e) => this.clickHandler(e), "af-variation": "secondary", "af-size": this.afSize, class: "digi-tag" }, this.afText, !this.afNoIcon && h("digi-icon", { afName: `x`, slot: "icon-secondary" })));
  }
  static get style() { return tagCss; }
}, [2, "digi-tag", {
    "afText": [1, "af-text"],
    "afNoIcon": [4, "af-no-icon"],
    "afSize": [1, "af-size"]
  }]);
function defineCustomElement$1() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-tag", "digi-button", "digi-icon"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-tag":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, Tag);
      }
      break;
    case "digi-button":
      if (!customElements.get(tagName)) {
        defineCustomElement$3();
      }
      break;
    case "digi-icon":
      if (!customElements.get(tagName)) {
        defineCustomElement$2();
      }
      break;
  } });
}
defineCustomElement$1();

const DigiTag = Tag;
const defineCustomElement = defineCustomElement$1;

export { DigiTag, defineCustomElement };
