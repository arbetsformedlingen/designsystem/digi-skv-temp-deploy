import type { Components, JSX } from "../dist/types/components";

interface DigiLogoSister extends Components.DigiLogoSister, HTMLElement {}
export const DigiLogoSister: {
  prototype: DigiLogoSister;
  new (): DigiLogoSister;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
