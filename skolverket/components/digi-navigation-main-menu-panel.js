import { proxyCustomElement, HTMLElement, createEvent, h } from '@stencil/core/internal/client';
import { b as ButtonVariation } from './button-variation.enum.js';
import './expandable-accordion-variation.enum.js';
import './calendar-week-view-heading-level.enum.js';
import './code-block-variation.enum.js';
import './code-example-variation.enum.js';
import './code-variation.enum.js';
import './form-checkbox-variation.enum.js';
import './form-file-upload-variation.enum.js';
import './form-input-search-variation.enum.js';
import './form-input-variation.enum.js';
import './form-radiobutton-variation.enum.js';
import './form-select-variation.enum.js';
import './form-textarea-variation.enum.js';
import './form-validation-message-variation.enum.js';
import './layout-block-variation.enum.js';
import './layout-columns-variation.enum.js';
import './layout-container-variation.enum.js';
import './layout-media-object-alignment.enum.js';
import './link-external-variation.enum.js';
import './link-internal-variation.enum.js';
import './link-variation.enum.js';
import './loader-spinner-size.enum.js';
import './media-figure-alignment.enum.js';
import './navigation-context-menu-item-type.enum.js';
import './navigation-sidebar-variation.enum.js';
import './navigation-vertical-menu-variation.enum.js';
import './progress-step-variation.enum.js';
import './progress-steps-variation.enum.js';
import './progressbar-variation.enum.js';
import './tag-size.enum.js';
import './typography-meta-variation.enum.js';
import './typography-time-variation.enum.js';
import './typography-variation.enum.js';
import './util-breakpoint-observer-breakpoints.enum.js';
import { B as BREAKPOINT_LARGE } from './tokens.es6.js';
import { d as defineCustomElement$5 } from './button.js';
import { d as defineCustomElement$4 } from './icon.js';
import { d as defineCustomElement$3 } from './layout-container.js';
import { d as defineCustomElement$2 } from './util-resize-observer.js';

const navigationMainMenuPanelCss = ".sc-digi-navigation-main-menu-panel-h{background:var(--digi--color--background--primary)}@media (max-width: 61.9375rem){.sc-digi-navigation-main-menu-panel-h{-webkit-border-before:var(--digi--border-width--tertiary) var(--digi--border-style--primary) var(--digi--global--color--profile--purple--opacity15);border-block-start:var(--digi--border-width--tertiary) var(--digi--border-style--primary) var(--digi--global--color--profile--purple--opacity15)}}@media (min-width: 62rem){.sc-digi-navigation-main-menu-panel-h{background:linear-gradient(90deg, var(--digi--color--background--secondary) 0%, var(--digi--color--background--secondary) 50%, var(--digi--color--background--primary) 50%, var(--digi--color--background--primary) 100%);-webkit-border-after:var(--digi--border-width--secondary) solid var(--digi--color--border--secondary);border-block-end:var(--digi--border-width--secondary) solid var(--digi--color--border--secondary)}}.sc-digi-navigation-main-menu-panel-h digi-layout-container.sc-digi-navigation-main-menu-panel{display:block}.digi-navigation-main-menu-panel.sc-digi-navigation-main-menu-panel{position:relative;padding:var(--digi--responsive-grid-gutter) 0;display:grid;grid-template-columns:1fr;grid-template-areas:\"close-button\" \"main-link\" \"sub-nav\"}@media (max-width: 61.9375rem){.digi-navigation-main-menu-panel.sc-digi-navigation-main-menu-panel{width:100vw;-webkit-margin-start:calc(-1 * var(--digi--responsive-grid-gutter--outer));margin-inline-start:calc(-1 * var(--digi--responsive-grid-gutter--outer));padding-block:0}}@media (min-width: 62rem){.digi-navigation-main-menu-panel.sc-digi-navigation-main-menu-panel{grid-template-columns:2fr 1fr;grid-template-areas:\"main-link close-button\" \"sub-nav close-button\"}.digi-navigation-main-menu-panel.sc-digi-navigation-main-menu-panel::before{content:\"\";background:var(--digi--color--background--primary);position:absolute;left:33.3333333333%;top:0;bottom:0;right:0}}.digi-navigation-main-menu-panel__main-link.sc-digi-navigation-main-menu-panel{grid-area:main-link;padding:0 var(--digi--padding--largest);position:relative}@media (min-width: 62rem){.digi-navigation-main-menu-panel__main-link.sc-digi-navigation-main-menu-panel{padding:0 var(--digi--padding--largest);-webkit-margin-after:var(--digi--padding--medium);margin-block-end:var(--digi--padding--medium)}}.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>[slot^=main-link]{font-family:var(--digi--global--typography--font-family--default);text-decoration:none;color:var(--digi--color--text--secondary);font-size:var(--digi--global--typography--font-size--large);letter-spacing:calc(var(--digi--global--typography--font-size--large) / 100 * -1);font-weight:var(--digi--global--typography--font-weight--semibold);display:block;border-radius:var(--digi--border-radius--primary);padding:calc(var(--digi--grid-gutter--smaller) * 1.5) var(--digi--grid-gutter--smaller);padding:0}.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>[slot^=main-link]:hover{color:var(--digi--global--color--profile--purple--dark);text-decoration:underline}.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>[slot^=main-link]:focus{outline:none;color:var(--digi--color--text--secondary)}.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>[slot^=main-link]:focus-visible{outline:var(--digi--border-width--secondary) solid var(--digi--color--border--focus)}.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>[slot^=main-link]:visited{color:var(--digi--color--text--secondary)}.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>[slot^=main-link]::after{background-color:currentColor;padding:0 0.5em;-webkit-mask-image:url(\"data:image/svg+xml;charset=utf-8, %3Csvg%0A%09width%3D%2224%22%0A%09height%3D%2224%22%0A%09viewBox%3D%220%200%2024%2024%22%0A%09xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%0A%3E%0A%09%3Cpath%0A%09%09d%3D%22M288.917%2C415.517%2C287.5%2C414.1l3.96-3.96-3.96-3.96%2C1.414-1.414%2C5.373%2C5.374Z%22%0A%09%09transform%3D%22translate(-278.17%20-398.103)%22%0A%09%2F%3E%0A%3C%2Fsvg%3E\");mask-image:url(\"data:image/svg+xml;charset=utf-8, %3Csvg%0A%09width%3D%2224%22%0A%09height%3D%2224%22%0A%09viewBox%3D%220%200%2024%2024%22%0A%09xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%0A%3E%0A%09%3Cpath%0A%09%09d%3D%22M288.917%2C415.517%2C287.5%2C414.1l3.96-3.96-3.96-3.96%2C1.414-1.414%2C5.373%2C5.374Z%22%0A%09%09transform%3D%22translate(-278.17%20-398.103)%22%0A%09%2F%3E%0A%3C%2Fsvg%3E\");-webkit-mask-repeat:no-repeat;mask-repeat:no-repeat;-webkit-mask-position:center;mask-position:center;-webkit-clip-path:padding-box inset(0.25em 0);clip-path:padding-box inset(0.25em 0);content:\"\"}.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>[slot^=main-link]:focus-visible{position:relative;z-index:1}@media (max-width: 61.9375rem){.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>[slot^=main-link]{display:flex;align-items:center;min-height:var(--digi--global--spacing--largest-6)}.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>[slot^=main-link]::after{min-height:var(--digi--global--spacing--largest-6)}}@media (min-width: 62rem){.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>[slot^=main-link]{font-size:var(--digi--global--typography--font-size--largest)}}.digi-navigation-main-menu-panel__sub-nav.sc-digi-navigation-main-menu-panel{grid-area:sub-nav}.digi-navigation-main-menu-panel__close-button-wrapper.sc-digi-navigation-main-menu-panel{display:block;grid-area:close-button;position:relative}@media (max-width: 61.9375rem){.digi-navigation-main-menu-panel__close-button-wrapper.sc-digi-navigation-main-menu-panel{min-height:var(--digi--global--spacing--largest-6);display:flex;align-items:center;-webkit-border-after:var(--digi--border-width--tertiary) var(--digi--border-style--primary) var(--digi--global--color--profile--purple--opacity15);border-block-end:var(--digi--border-width--tertiary) var(--digi--border-style--primary) var(--digi--global--color--profile--purple--opacity15)}.digi-navigation-main-menu-panel__close-button-wrapper.sc-digi-navigation-main-menu-panel [data-viewport=large].sc-digi-navigation-main-menu-panel{display:none}}@media (min-width: 62rem){.digi-navigation-main-menu-panel__close-button-wrapper.sc-digi-navigation-main-menu-panel{justify-self:end}.digi-navigation-main-menu-panel__close-button-wrapper.sc-digi-navigation-main-menu-panel [data-viewport=small].sc-digi-navigation-main-menu-panel{display:none}}.digi-navigation-main-menu-panel__close-button.sc-digi-navigation-main-menu-panel{--digi--button--color--text--function--default:var(--digi--color--text--secondary);--digi--button--color--text--function--hover:var(--digi--color--text--secondary);--digi--button--color--text--function--focus:var(--digi--color--text--secondary);--digi--button--color--text--function--active:var(--digi--color--text--secondary);--digi--button--icon--spacing:var(--digi--gutter--smaller)}@media (max-width: 61.9375rem){.digi-navigation-main-menu-panel__close-button.sc-digi-navigation-main-menu-panel{--digi--button--font-weight:var(--digi--global--typography--font-weight--regular)}}.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul{display:grid;grid-template-columns:1fr;-moz-column-gap:var(--digi--container-gutter--medium);column-gap:var(--digi--container-gutter--medium);position:relative}@media (min-width: 62rem){.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul{grid-template-columns:1fr 1fr}}.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul a{font-family:var(--digi--global--typography--font-family--default);text-decoration:none;color:var(--digi--color--text--secondary);font-size:var(--digi--global--typography--font-size--large);letter-spacing:calc(var(--digi--global--typography--font-size--large) / 100 * -1);font-weight:var(--digi--global--typography--font-weight--semibold);font-size:var(--digi--global--typography--font-size--base);display:flex;align-items:center;flex-grow:1;height:100%;border-radius:var(--digi--border-radius--primary);-webkit-padding-start:var(--digi-navigation-main-menu--link-start-padding);padding-inline-start:var(--digi-navigation-main-menu--link-start-padding);font-weight:var(--digi--global--typography--font-weight--regular);min-height:var(--digi--global--spacing--largest-6)}.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul a:hover{color:var(--digi--global--color--profile--purple--dark);text-decoration:underline}.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul a:focus{outline:none;color:var(--digi--color--text--secondary)}.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul a:focus-visible{outline:var(--digi--border-width--secondary) solid var(--digi--color--border--focus)}.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul a:visited{color:var(--digi--color--text--secondary)}.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul a:focus-visible{z-index:1}.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul li{--digi-navigation-main-menu-panel--list-item-expanded-background:var(--digi--color--background--secondary);align-items:center;justify-content:space-between;min-height:var(--digi--global--spacing--largest-6);-webkit-border-after:var(--digi--border-width--tertiary) var(--digi--border-style--primary) var(--digi--global--color--profile--purple--opacity15);border-block-end:var(--digi--border-width--tertiary) var(--digi--border-style--primary) var(--digi--global--color--profile--purple--opacity15);padding-inline:10px}@media (max-width: 61.9375rem){.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul li{display:grid;grid-template-columns:1fr auto;-moz-column-gap:var(--digi--padding--largest);column-gap:var(--digi--padding--largest)}}@media (min-width: 62rem){.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul li{display:flex;flex-direction:row;gap:var(--digi--padding--largest)}}@media (max-width: 61.9375rem){.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul li{padding-inline:0}}.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul li:first-child{-webkit-border-before:var(--digi--border-width--tertiary) var(--digi--border-style--primary) var(--digi--global--color--profile--purple--opacity15);border-block-start:var(--digi--border-width--tertiary) var(--digi--border-style--primary) var(--digi--global--color--profile--purple--opacity15)}.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul li li:last-child{-webkit-border-after:none;border-block-end:none}@media (min-width: 62rem){.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul li:hover,.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul li[data-expanded=true]{background:var(--digi-navigation-main-menu-panel--list-item-expanded-background)}}@media (min-width: 62rem){.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul li[data-current]{background:linear-gradient(90deg, var(--digi--color--border--focus) 0%, var(--digi--color--border--focus) var(--digi--global--border-width--larger), var(--digi--color--background--tertiary) var(--digi--global--border-width--larger), var(--digi--color--background--tertiary) 100%)}.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul li[data-current]>a{font-weight:var(--digi--global--typography--font-weight--semibold)}}@media (max-width: 61.9375rem){.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul li{--digi-navigation-main-menu-panel--list-item-background:var(--digi--color--background--primary);--digi-navigation-main-menu-panel--list-item-border:var(--digi--color--background--primary);background:linear-gradient(90deg, var(--digi-navigation-main-menu-panel--list-item-border) 0%, var(--digi-navigation-main-menu-panel--list-item-border) var(--digi--global--border-width--larger), var(--digi-navigation-main-menu-panel--list-item-background) var(--digi--global--border-width--larger), var(--digi-navigation-main-menu-panel--list-item-background) 100%)}.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul li[data-current=exact]{--digi-navigation-main-menu-panel--list-item-background:var(--digi--color--background--tertiary);--digi-navigation-main-menu-panel--list-item-border:var(--digi--color--border--focus)}}.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul li .digi-navigation-main-menu-panel__toggle-button{flex-shrink:0;display:flex;align-items:center;justify-content:center;width:var(--digi--global--spacing--largest-4);height:var(--digi--global--spacing--largest-4);background:var(--digi-navigation-main-menu-panel--button-background);border-radius:50%;border:var(--digi--border-width--tertiary) var(--digi--border-style--primary) var(--digi--global--color--profile--purple--opacity15);border-color:var(--digi--color--background--inverted-1);color:var(--digi--color--text--secondary)}.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul li .digi-navigation-main-menu-panel__toggle-button[aria-expanded=true]{--digi-navigation-main-menu-panel--icon-color:var(--digi--color--text--inverted);--digi-navigation-main-menu-panel--button-background:var(--digi--color--background--inverted-1)}.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul li .digi-navigation-main-menu-panel__toggle-button[aria-expanded=true] digi-icon:not([af-name=chevron-left]){display:none}.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul li .digi-navigation-main-menu-panel__toggle-button:not([aria-expanded=true]) digi-icon:not([af-name=chevron-right]){display:none}.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul li .digi-navigation-main-menu-panel__toggle-button digi-icon{display:flex;pointer-events:none}.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul li .digi-navigation-main-menu-panel__toggle-button digi-icon svg{--digi--icon--color:var(--digi-navigation-main-menu-panel--icon-color)}.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul li .digi-navigation-main-menu-panel__toggle-button:focus-visible{outline:var(--digi--focus-outline)}@media (max-width: 61.9375rem){.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul li .digi-navigation-main-menu-panel__toggle-button{transform:rotate(90deg);position:relative;left:calc(-1 * var(--digi--padding--small))}}@media (max-width: 61.9375rem){.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul li li .digi-navigation-main-menu-panel__toggle-button{background:transparent;border-color:transparent}}@media (max-width: 61.9375rem) and (max-width: 61.9375rem){.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul li li .digi-navigation-main-menu-panel__toggle-button{transform:unset}}@media (max-width: 61.9375rem){.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul li li .digi-navigation-main-menu-panel__toggle-button digi-icon svg{--digi--icon--color:currentColor}}@media (max-width: 61.9375rem){.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul li li .digi-navigation-main-menu-panel__toggle-button[aria-expanded=true] digi-icon:not([af-name=minus]){display:none}.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul li li .digi-navigation-main-menu-panel__toggle-button[aria-expanded=true] digi-icon[af-name=minus]{display:flex}}@media (max-width: 61.9375rem){.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul li li .digi-navigation-main-menu-panel__toggle-button:not([aria-expanded=true]) digi-icon:not([af-name=plus]){display:none}.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul li li .digi-navigation-main-menu-panel__toggle-button:not([aria-expanded=true]) digi-icon[af-name=plus]{display:flex}}@media (min-width: 62rem){.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul li li .digi-navigation-main-menu-panel__toggle-button{display:none}}.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul>li{--digi-navigation-main-menu-panel--list-item-expanded-background:var(--digi--color--background--primary);--digi-navigation-main-menu-panel--icon-color:currentColor;--digi-navigation-main-menu-panel--button-background:var(--digi--color--background--primary);--digi-navigation-main-menu--link-start-padding:14px;grid-column:1}@media (max-width: 61.9375rem){.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul>li{--digi-navigation-main-menu--link-start-padding:var(--digi--padding--largest)}}.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul>li>ul{display:none}.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul>li>ul>li{--digi-navigation-main-menu-panel--icon-color:currentColor}@media (max-width: 61.9375rem){.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul>li>ul>li{--digi-navigation-main-menu-panel--list-item-border:var(--digi--color--background--tertiary)}}.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul>li>ul>li>ul li{--digi-navigation-main-menu-panel--icon-color:currentColor}@media (max-width: 61.9375rem){.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul>li>ul>li>ul li{--digi-navigation-main-menu--link-start-padding:48px;--digi-navigation-main-menu-panel--list-item-border:var(--digi--color--background--tertiary)}.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul>li>ul>li>ul li a{font-size:var(--digi--global--typography--font-size--small)}}.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul>li>ul ul{display:none}.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul>li [aria-expanded=true]+ul{display:grid}@media (max-width: 61.9375rem){.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul>li [aria-expanded=true]+ul{grid-column:1/-1}}@media (min-width: 62rem){.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul>li [aria-expanded=true]+ul ul{display:none}}@media (min-width: 62rem){.sc-digi-navigation-main-menu-panel-h .sc-digi-navigation-main-menu-panel-s>ul>li [aria-expanded=true]+ul{position:absolute;left:calc(50% + var(--digi--container-gutter--medium) / 2);top:0;width:calc(50% - var(--digi--container-gutter--medium) / 2)}}";

const NavigationMainMenuPanel = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afOnResize = createEvent(this, "afOnResize", 7);
    this.afOnClose = createEvent(this, "afOnClose", 7);
    this.nestedListItems = [];
  }
  nestedListItemsChanged() {
    this.hydrateListItems();
  }
  resizeHandler(e) {
    this.afOnResize.emit(e);
  }
  closeHandler(e) {
    this.afOnClose.emit(e);
  }
  componentWillLoad() {
    this.getAllListItems();
    this.getNestedListItems();
  }
  getAllListItems() {
    const allListItems = this.hostElement.querySelectorAll('li');
    this.setCurrentListItem(allListItems);
  }
  setCurrentListItem(listItems) {
    listItems.length &&
      Array.from(listItems).forEach((item) => {
        // If list item is exactly the current page
        if (!!item.querySelector(':scope > a[aria-current="page"]')) {
          item.setAttribute('data-current', 'exact');
          item.closest('ul').setAttribute('data-has-current-exact', 'true');
          this.hostElement.setAttribute('data-visible-current-exact', 'true');
          return;
        }
        // If list item has a nested current page
        !!item.querySelector('[aria-current="page"]') &&
          item.setAttribute('data-current', 'nested');
      });
  }
  getNestedListItems() {
    const nestedListItems = this.hostElement.querySelectorAll('digi-navigation-main-menu-panel > ul > li ul');
    if (nestedListItems.length < 0)
      return;
    this.nestedListItems = Array.from(nestedListItems);
  }
  toggleButtonClickHandler(e) {
    const target = e.target;
    if (!target.classList.contains('digi-navigation-main-menu-panel__toggle-button'))
      return;
    this.setExpansionState(target);
  }
  linkClickHandler(e) {
    const target = e.target;
    if (target.tagName !== 'A')
      return;
    this.closeHandler(e);
  }
  setExpansionState(target) {
    const isExpanded = target.getAttribute('aria-expanded') === 'true';
    const closestLi = target.closest('li');
    target.setAttribute('aria-expanded', isExpanded ? 'false' : 'true');
    closestLi.setAttribute('data-expanded', !isExpanded);
    if (isExpanded) {
      closestLi.querySelectorAll('[data-expanded]').forEach((el) => {
        el.setAttribute('data-expanded', 'false');
      });
      closestLi.querySelectorAll('[aria-expanded="true"]').forEach((el) => {
        el.setAttribute('aria-expanded', 'false');
      });
    }
    const currentPage = this.hostElement.querySelector('[aria-current="page"]');
    if (currentPage) {
      this.hostElement.setAttribute('data-visible-current-exact', window.getComputedStyle(currentPage.closest('ul')).display !== 'none'
        ? 'true'
        : 'false');
    }
    if (window.matchMedia(`(min-width: ${BREAKPOINT_LARGE})`).matches) {
      const buttons = this.hostElement.querySelectorAll('.digi-navigation-main-menu-panel__toggle-button');
      Array.from(buttons).forEach((button) => {
        if (button === target)
          return;
        button.setAttribute('aria-expanded', 'false');
        button.closest('li').removeAttribute('data-expanded');
      });
    }
  }
  hydrateListItems() {
    this.nestedListItems.forEach((nestedListItem, i) => {
      const id = `${this.hostElement.id}-subnav-${i}`;
      nestedListItem.setAttribute('id', id);
      const button = document.createElement('button');
      button.setAttribute('aria-expanded', nestedListItem.closest('[data-current="nested"]') ? 'true' : 'false');
      button.setAttribute('aria-controls', id);
      button.setAttribute('aria-label', 'Visa och dölj undermeny');
      button.classList.add('digi-navigation-main-menu-panel__toggle-button');
      button.innerHTML = `
				<digi-icon af-name="plus" aria-hidden="true"></digi-icon>
				<digi-icon af-name="minus" aria-hidden="true"></digi-icon>
				<digi-icon af-name="chevron-right" aria-hidden="true"></digi-icon>
				<digi-icon af-name="chevron-left" aria-hidden="true"></digi-icon>`;
      nestedListItem.parentNode.insertBefore(button, nestedListItem);
    });
  }
  keyUpHandler(e) {
    if (e.key !== 'Escape' ||
      window.matchMedia(`(min-width: ${BREAKPOINT_LARGE})`).matches)
      return;
    this.afOnClose.emit();
  }
  render() {
    return (h("digi-util-resize-observer", { onAfOnChange: (e) => this.resizeHandler(e.detail) }, h("digi-layout-container", null, h("div", { class: {
        'digi-navigation-main-menu-panel': true
      } }, h("div", { class: "digi-navigation-main-menu-panel__main-link", part: "main-link" }, h("slot", { name: "main-link" })), h("div", { class: "digi-navigation-main-menu-panel__sub-nav" }, h("slot", null)), h("div", { class: "digi-navigation-main-menu-panel__close-button-wrapper" }, h("digi-button", { afVariation: ButtonVariation.FUNCTION, class: "digi-navigation-main-menu-panel__close-button", onAfOnClick: (e) => this.closeHandler(e.detail), "aria-controls": this.hostElement.id, "aria-expanded": "true" }, h("digi-icon", { "data-viewport": "small", afName: 'chevron-left', "aria-hidden": "true", slot: "icon" }), h("digi-icon", { "data-viewport": "large", afName: 'x', "aria-hidden": "true", slot: "icon-secondary" }), h("span", { "data-viewport": "small" }, "Tillbaka"), h("span", { "data-viewport": "large" }, "St\u00E4ng")))))));
  }
  get hostElement() { return this; }
  static get watchers() { return {
    "nestedListItems": ["nestedListItemsChanged"]
  }; }
  static get style() { return navigationMainMenuPanelCss; }
}, [6, "digi-navigation-main-menu-panel", {
    "nestedListItems": [32]
  }, [[0, "click", "toggleButtonClickHandler"], [0, "click", "linkClickHandler"], [0, "keyup", "keyUpHandler"]]]);
function defineCustomElement$1() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-navigation-main-menu-panel", "digi-button", "digi-icon", "digi-layout-container", "digi-util-resize-observer"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-navigation-main-menu-panel":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, NavigationMainMenuPanel);
      }
      break;
    case "digi-button":
      if (!customElements.get(tagName)) {
        defineCustomElement$5();
      }
      break;
    case "digi-icon":
      if (!customElements.get(tagName)) {
        defineCustomElement$4();
      }
      break;
    case "digi-layout-container":
      if (!customElements.get(tagName)) {
        defineCustomElement$3();
      }
      break;
    case "digi-util-resize-observer":
      if (!customElements.get(tagName)) {
        defineCustomElement$2();
      }
      break;
  } });
}
defineCustomElement$1();

const DigiNavigationMainMenuPanel = NavigationMainMenuPanel;
const defineCustomElement = defineCustomElement$1;

export { DigiNavigationMainMenuPanel, defineCustomElement };
