import { U as UtilMutationObserver, d as defineCustomElement$1 } from './util-mutation-observer.js';

const DigiUtilMutationObserver = UtilMutationObserver;
const defineCustomElement = defineCustomElement$1;

export { DigiUtilMutationObserver, defineCustomElement };
