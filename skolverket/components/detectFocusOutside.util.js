import { d as detectClosest } from './detectClosest.util.js';

function detectFocusOutside(target, selector) {
  return !detectClosest(target, selector);
}

export { detectFocusOutside as d };
