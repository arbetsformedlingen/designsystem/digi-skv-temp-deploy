import type { Components, JSX } from "../dist/types/components";

interface DigiTypographyTime extends Components.DigiTypographyTime, HTMLElement {}
export const DigiTypographyTime: {
  prototype: DigiTypographyTime;
  new (): DigiTypographyTime;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
