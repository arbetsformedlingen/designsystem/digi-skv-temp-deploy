var ButtonSize;
(function (ButtonSize) {
  ButtonSize["SMALL"] = "small";
  ButtonSize["MEDIUM"] = "medium";
  ButtonSize["LARGE"] = "large";
})(ButtonSize || (ButtonSize = {}));

var ButtonType;
(function (ButtonType) {
  ButtonType["BUTTON"] = "button";
  ButtonType["SUBMIT"] = "submit";
  ButtonType["RESET"] = "reset";
})(ButtonType || (ButtonType = {}));

var ButtonVariation;
(function (ButtonVariation) {
  ButtonVariation["PRIMARY"] = "primary";
  ButtonVariation["SECONDARY"] = "secondary";
  ButtonVariation["FUNCTION"] = "function";
})(ButtonVariation || (ButtonVariation = {}));

export { ButtonSize as B, ButtonType as a, ButtonVariation as b };
