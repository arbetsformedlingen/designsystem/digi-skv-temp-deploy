var NavigationSidebarCloseButtonPosition;
(function (NavigationSidebarCloseButtonPosition) {
  NavigationSidebarCloseButtonPosition["START"] = "start";
  NavigationSidebarCloseButtonPosition["END"] = "end";
})(NavigationSidebarCloseButtonPosition || (NavigationSidebarCloseButtonPosition = {}));

var NavigationSidebarHeadingLevel;
(function (NavigationSidebarHeadingLevel) {
  NavigationSidebarHeadingLevel["H1"] = "h1";
  NavigationSidebarHeadingLevel["H2"] = "h2";
  NavigationSidebarHeadingLevel["H3"] = "h3";
  NavigationSidebarHeadingLevel["H4"] = "h4";
  NavigationSidebarHeadingLevel["H5"] = "h5";
  NavigationSidebarHeadingLevel["H6"] = "h6";
})(NavigationSidebarHeadingLevel || (NavigationSidebarHeadingLevel = {}));

var NavigationSidebarMobilePosition;
(function (NavigationSidebarMobilePosition) {
  NavigationSidebarMobilePosition["START"] = "start";
  NavigationSidebarMobilePosition["END"] = "end";
})(NavigationSidebarMobilePosition || (NavigationSidebarMobilePosition = {}));

var NavigationSidebarMobileVariation;
(function (NavigationSidebarMobileVariation) {
  NavigationSidebarMobileVariation["DEFAULT"] = "default";
  NavigationSidebarMobileVariation["FULLWIDTH"] = "fullwidth";
})(NavigationSidebarMobileVariation || (NavigationSidebarMobileVariation = {}));

var NavigationSidebarPosition;
(function (NavigationSidebarPosition) {
  NavigationSidebarPosition["START"] = "start";
  NavigationSidebarPosition["END"] = "end";
})(NavigationSidebarPosition || (NavigationSidebarPosition = {}));

var NavigationSidebarVariation;
(function (NavigationSidebarVariation) {
  NavigationSidebarVariation["OVER"] = "over";
  NavigationSidebarVariation["PUSH"] = "push";
  NavigationSidebarVariation["STATIC"] = "static";
})(NavigationSidebarVariation || (NavigationSidebarVariation = {}));

export { NavigationSidebarCloseButtonPosition as N, NavigationSidebarHeadingLevel as a, NavigationSidebarMobilePosition as b, NavigationSidebarMobileVariation as c, NavigationSidebarPosition as d, NavigationSidebarVariation as e };
