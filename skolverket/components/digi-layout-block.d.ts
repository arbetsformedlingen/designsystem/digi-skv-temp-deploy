import type { Components, JSX } from "../dist/types/components";

interface DigiLayoutBlock extends Components.DigiLayoutBlock, HTMLElement {}
export const DigiLayoutBlock: {
  prototype: DigiLayoutBlock;
  new (): DigiLayoutBlock;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
