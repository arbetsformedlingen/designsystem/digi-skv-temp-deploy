import { F as FormInput, d as defineCustomElement$1 } from './form-input.js';

const DigiFormInput = FormInput;
const defineCustomElement = defineCustomElement$1;

export { DigiFormInput, defineCustomElement };
