var ProgressStepsHeadingLevel;
(function (ProgressStepsHeadingLevel) {
  ProgressStepsHeadingLevel["H1"] = "h1";
  ProgressStepsHeadingLevel["H2"] = "h2";
  ProgressStepsHeadingLevel["H3"] = "h3";
  ProgressStepsHeadingLevel["H4"] = "h4";
  ProgressStepsHeadingLevel["H5"] = "h5";
  ProgressStepsHeadingLevel["H6"] = "h6";
})(ProgressStepsHeadingLevel || (ProgressStepsHeadingLevel = {}));

var ProgressStepsStatus;
(function (ProgressStepsStatus) {
  ProgressStepsStatus["CURRENT"] = "current";
  ProgressStepsStatus["UPCOMING"] = "upcoming";
  ProgressStepsStatus["DONE"] = "done";
})(ProgressStepsStatus || (ProgressStepsStatus = {}));

var ProgressStepsVariation;
(function (ProgressStepsVariation) {
  ProgressStepsVariation["PRIMARY"] = "primary";
  ProgressStepsVariation["SECONDARY"] = "secondary";
})(ProgressStepsVariation || (ProgressStepsVariation = {}));

export { ProgressStepsHeadingLevel as P, ProgressStepsStatus as a, ProgressStepsVariation as b };
