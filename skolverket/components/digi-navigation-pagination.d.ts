import type { Components, JSX } from "../dist/types/components";

interface DigiNavigationPagination extends Components.DigiNavigationPagination, HTMLElement {}
export const DigiNavigationPagination: {
  prototype: DigiNavigationPagination;
  new (): DigiNavigationPagination;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
