import { proxyCustomElement, HTMLElement } from '@stencil/core/internal/client';
import { l as logger } from './logger.util.js';
import { N as NavigationContextMenuItemType } from './navigation-context-menu-item-type.enum.js';

const NavigationContextMenuItem = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.afText = undefined;
    this.afHref = undefined;
    this.afType = undefined;
    this.afLang = undefined;
    this.afDir = undefined;
  }
  componentWillLoad() {
    if (this.afType !== NavigationContextMenuItemType.BUTTON &&
      this.afType !== NavigationContextMenuItemType.LINK) {
      logger.warn(`The navigation-context-menu-item is of unallowed afType. Only button and link is allowed.`, this.hostElement);
      return;
    }
  }
  get hostElement() { return this; }
}, [2, "digi-navigation-context-menu-item", {
    "afText": [1, "af-text"],
    "afHref": [1, "af-href"],
    "afType": [1, "af-type"],
    "afLang": [1, "af-lang"],
    "afDir": [1, "af-dir"]
  }]);
function defineCustomElement$1() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["digi-navigation-context-menu-item"];
  components.forEach(tagName => { switch (tagName) {
    case "digi-navigation-context-menu-item":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, NavigationContextMenuItem);
      }
      break;
  } });
}
defineCustomElement$1();

const DigiNavigationContextMenuItem = NavigationContextMenuItem;
const defineCustomElement = defineCustomElement$1;

export { DigiNavigationContextMenuItem, defineCustomElement };
