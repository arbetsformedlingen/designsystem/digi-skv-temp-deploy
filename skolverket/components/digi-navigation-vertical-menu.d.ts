import type { Components, JSX } from "../dist/types/components";

interface DigiNavigationVerticalMenu extends Components.DigiNavigationVerticalMenu, HTMLElement {}
export const DigiNavigationVerticalMenu: {
  prototype: DigiNavigationVerticalMenu;
  new (): DigiNavigationVerticalMenu;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
