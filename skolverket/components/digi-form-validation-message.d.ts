import type { Components, JSX } from "../dist/types/components";

interface DigiFormValidationMessage extends Components.DigiFormValidationMessage, HTMLElement {}
export const DigiFormValidationMessage: {
  prototype: DigiFormValidationMessage;
  new (): DigiFormValidationMessage;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
