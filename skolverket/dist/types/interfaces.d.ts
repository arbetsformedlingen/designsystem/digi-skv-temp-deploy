/**
 * Generated with extension:
 Name: Generate Index
 Id: jayfong.generate-index
 Description: Generating file indexes easily.
 Version: 1.6.0
 Publisher: Jay Fong
 VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=JayFong.generate-index
 */
export * from './__core/_navigation/navigation-context-menu/navigation-context-menu.interface';
