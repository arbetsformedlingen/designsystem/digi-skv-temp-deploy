import { KEY_CODE } from './keyCode.enum';
export declare function keyboardHandler(event: KeyboardEvent): KEY_CODE;
