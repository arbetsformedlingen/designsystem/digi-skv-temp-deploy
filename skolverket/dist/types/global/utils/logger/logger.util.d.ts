import { LoggerType } from './LoggerType.enum';
import { LoggerSource } from './LoggerSource.enum';
export declare function outputLog(message: string, host: Element, type?: LoggerType, source?: LoggerSource): void;
export declare const logger: {
  log: (message: string, host: Element, source?: LoggerSource) => void;
  info: (message: string, host: Element, source?: LoggerSource) => void;
  warn: (message: string, host: Element, source?: LoggerSource) => void;
  error: (message: string, host: Element, source?: LoggerSource) => void;
};
