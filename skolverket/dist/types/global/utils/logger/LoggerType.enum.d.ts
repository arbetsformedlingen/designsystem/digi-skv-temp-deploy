export declare enum LoggerType {
  LOG = "log",
  INFO = "info",
  WARN = "warn",
  ERROR = "error"
}
