import { HTMLStencilElement } from '../../../stencil-public-runtime';
export declare const getCSSModifiersFromProps: (el: HTMLStencilElement, propBaseName: string | string[]) => any;
