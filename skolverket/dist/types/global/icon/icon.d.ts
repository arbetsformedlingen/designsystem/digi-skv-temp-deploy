export declare const icon: {
  readonly 'accessibility-deaf': {
    readonly IconComponent: ({ afSvgAriaHidden }: {
      afSvgAriaHidden: any;
    }, children: any) => any;
  };
  readonly bars: {
    readonly IconComponent: ({ afSvgAriaHidden }: {
      afSvgAriaHidden: any;
    }, children: any) => any;
  };
  readonly 'check-circle-reg': {
    readonly IconComponent: ({ afSvgAriaHidden }: {
      afSvgAriaHidden: any;
    }, children: any) => any;
  };
  readonly check: {
    readonly IconComponent: ({ afSvgAriaHidden }: {
      afSvgAriaHidden: any;
    }, children: any) => any;
  };
  readonly 'chevron-down': {
    readonly IconComponent: ({ afSvgAriaHidden }: {
      afSvgAriaHidden: any;
    }, children: any) => any;
  };
  readonly 'chevron-left': {
    readonly IconComponent: ({ afSvgAriaHidden }: {
      afSvgAriaHidden: any;
    }, children: any) => any;
  };
  readonly 'chevron-right': {
    readonly IconComponent: ({ afSvgAriaHidden }: {
      afSvgAriaHidden: any;
    }, children: any) => any;
  };
  readonly 'chevron-up': {
    readonly IconComponent: ({ afSvgAriaHidden }: {
      afSvgAriaHidden: any;
    }, children: any) => any;
  };
  readonly dislike: {
    readonly IconComponent: ({ afSvgAriaHidden }: {
      afSvgAriaHidden: any;
    }, children: any) => any;
  };
  readonly 'exclamation-circle': {
    readonly IconComponent: ({ afSvgAriaHidden }: {
      afSvgAriaHidden: any;
    }, children: any) => any;
  };
  readonly 'exclamation-triangle-warning': {
    readonly IconComponent: ({ afSvgAriaHidden }: {
      afSvgAriaHidden: any;
    }, children: any) => any;
  };
  readonly globe: {
    readonly IconComponent: ({ afSvgAriaHidden }: {
      afSvgAriaHidden: any;
    }, children: any) => any;
  };
  readonly 'info-circle-reg': {
    readonly IconComponent: ({ afSvgAriaHidden }: {
      afSvgAriaHidden: any;
    }, children: any) => any;
  };
  readonly lattlast: {
    readonly IconComponent: ({ afSvgAriaHidden }: {
      afSvgAriaHidden: any;
    }, children: any) => any;
  };
  readonly like: {
    readonly IconComponent: ({ afSvgAriaHidden }: {
      afSvgAriaHidden: any;
    }, children: any) => any;
  };
  readonly minus: {
    readonly IconComponent: ({ afSvgAriaHidden }: {
      afSvgAriaHidden: any;
    }, children: any) => any;
  };
  readonly plus: {
    readonly IconComponent: ({ afSvgAriaHidden }: {
      afSvgAriaHidden: any;
    }, children: any) => any;
  };
  readonly search: {
    readonly IconComponent: ({ afSvgAriaHidden }: {
      afSvgAriaHidden: any;
    }, children: any) => any;
  };
  readonly sort: {
    readonly IconComponent: ({ afSvgAriaHidden }: {
      afSvgAriaHidden: any;
    }, children: any) => any;
  };
  readonly x: {
    readonly IconComponent: ({ afSvgAriaHidden }: {
      afSvgAriaHidden: any;
    }, children: any) => any;
  };
  readonly 'input-select-marker': {
    readonly IconComponent: ({ afSvgAriaHidden }: {
      afSvgAriaHidden: any;
    }, children: any) => any;
  };
  readonly 'notification-danger': {
    readonly IconComponent: ({ afSvgAriaHidden }: {
      afSvgAriaHidden: any;
    }, children: any) => any;
  };
  readonly 'notification-warning': {
    readonly IconComponent: ({ afSvgAriaHidden }: {
      afSvgAriaHidden: any;
    }, children: any) => any;
  };
  readonly 'notification-info': {
    readonly IconComponent: ({ afSvgAriaHidden }: {
      afSvgAriaHidden: any;
    }, children: any) => any;
  };
  readonly close: {
    readonly IconComponent: ({ afSvgAriaHidden }: {
      afSvgAriaHidden: any;
    }, children: any) => any;
  };
  readonly 'check-circle': {
    readonly IconComponent: ({ afSvgAriaHidden }: {
      afSvgAriaHidden: any;
    }, children: any) => any;
  };
  readonly 'check-circle-reg-alt': {
    readonly IconComponent: ({ afSvgAriaHidden }: {
      afSvgAriaHidden: any;
    }, children: any) => any;
  };
  readonly copy: {
    readonly IconComponent: ({ afSvgAriaHidden }: {
      afSvgAriaHidden: any;
    }, children: any) => any;
  };
  readonly 'danger-outline': {
    readonly IconComponent: ({ afSvgAriaHidden }: {
      afSvgAriaHidden: any;
    }, children: any) => any;
  };
  readonly download: {
    readonly IconComponent: ({ afSvgAriaHidden }: {
      afSvgAriaHidden: any;
    }, children: any) => any;
  };
  readonly 'exclamation-circle-filled': {
    readonly IconComponent: ({ afSvgAriaHidden }: {
      afSvgAriaHidden: any;
    }, children: any) => any;
  };
  readonly 'exclamation-triangle': {
    readonly IconComponent: ({ afSvgAriaHidden }: {
      afSvgAriaHidden: any;
    }, children: any) => any;
  };
  readonly 'external-link-alt': {
    readonly IconComponent: ({ afSvgAriaHidden }: {
      afSvgAriaHidden: any;
    }, children: any) => any;
  };
  readonly paperclip: {
    readonly IconComponent: ({ afSvgAriaHidden }: {
      afSvgAriaHidden: any;
    }, children: any) => any;
  };
  readonly spinner: {
    readonly IconComponent: ({ afSvgAriaHidden }: {
      afSvgAriaHidden: any;
    }, children: any) => any;
  };
  readonly trash: {
    readonly IconComponent: ({ afSvgAriaHidden }: {
      afSvgAriaHidden: any;
    }, children: any) => any;
  };
};
export declare type IconName = keyof typeof icon;
