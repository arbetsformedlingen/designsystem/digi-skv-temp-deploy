export declare enum TagSize {
  SMALL = "small",
  MEDIUM = "medium",
  LARGE = "large"
}
