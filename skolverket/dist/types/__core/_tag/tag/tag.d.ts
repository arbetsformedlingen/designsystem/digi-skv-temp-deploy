import { EventEmitter } from '../../../stencil-public-runtime';
import { TagSize } from './tag-size.enum';
/**
 * @swedishName Tagg
 */
export declare class Tag {
  /**
   * Sätter taggens text.
   * @en Set the tag text.
   */
  afText: string;
  /**
   * Tar bort taggens ikon. Falskt som förvalt.
   * @en Removes the tag icon. Defaults to false.
   */
  afNoIcon: boolean;
  /**
   * Sätter taggens storlek.
   * @en Sets tag size.
   */
  afSize: TagSize;
  /**
   * Taggelementets 'onclick'-event.
   * @en The tag elements 'onclick' event.
   */
  afOnClick: EventEmitter;
  clickHandler(e: any): void;
  render(): any;
}
