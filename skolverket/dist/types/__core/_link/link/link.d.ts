import { EventEmitter } from '../../../stencil-public-runtime';
import { HTMLStencilElement } from '../../../stencil-public-runtime';
import { LinkVariation } from './link-variation.enum';
/**
 * @slot default - Ska vara en textnod
 * @slot icon - Ska vara en ikon (eller en ikonlik) komponent
 *
 * @enums LinkVariation - link-variation.enum.ts
 * @swedishName Länk
 */
export declare class Link {
  hostElement: HTMLStencilElement;
  private _link;
  hasIcon: boolean;
  relation: string;
  /**
   * Sätter attributet 'href'.
   * @en Set `href` attribute.
   */
  afHref: string;
  /**
   * Sätter variant. Kan vara 'small' eller 'large'.
   * @en Sets the variation of the link.
   */
  afVariation: `${LinkVariation}`;
  /**
   * Sätter attributet 'target'. Om värdet är '_blank' så lägger komponenten automatikt till ett ´ref´-attribut med 'noopener noreferrer'.
   * @en Set `target` attribute. If value is '_blank', the component automatically adds a 'ref' attribute with 'noopener noreferrer'.
   */
  afTarget: string;
  /**
   * Kringgår länkens vanliga beteende.
   * Bör endast användas om det vanliga beteendet är problematiskt pga dynamisk routing eller liknande.
   * @en Override default link behavior. Should only be used if default link behaviour is a problem with e.g. routing
   */
  afOverrideLink: boolean;
  /**
   * Länkelementets 'onclick'-event.
   * @en The link element's 'onclick' event.
   */
  afOnClick: EventEmitter<MouseEvent>;
  /**
   * Hämtar en referens till länkelementet. Bra för att t.ex. sätta fokus programmatiskt.
   * @en Returns a reference to the link element. Handy for setting focus programmatically.
   */
  afMGetLinkElement(): Promise<any>;
  clickLinkHandler(e: MouseEvent): void;
  setRel(): void;
  setHasIcon(): void;
  initRouting(): void;
  componentWillLoad(): void;
  componentWillUpdate(): void;
  componentDidLoad(): void;
  get cssModifiers(): {
    [x: string]: boolean;
    'digi-link--has-icon': boolean;
  };
  render(): any;
}
