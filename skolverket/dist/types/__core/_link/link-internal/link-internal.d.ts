import { EventEmitter } from '../../../stencil-public-runtime';
import { HTMLStencilElement } from '../../../stencil-public-runtime';
import { LinkInternalVariation } from './link-internal-variation.enum';
/**
 * @slot default - Ska vara en textnod
 *
 * @enums LinkInternalVariation - link-internal-variation.enum.ts
 *@swedishName Intern länk
 */
export declare class LinkInternal {
  hostElement: HTMLStencilElement;
  /**
   * Sätter attributet 'href'.
   * @en Set `href` attribute.
   */
  afHref: string;
  /**
   * Sätter variant. Kan vara 'small' eller 'large'.
   * @en Sets the variation of the link.
   */
  afVariation: `${LinkInternalVariation}`;
  /**
   * Kringgår länkens vanliga beteende.
   * Bör endast användas om det vanliga beteendet är problematiskt pga dynamisk routing eller liknande.
   * @en Override default link behavior. Should only be used if default link behaviour is a problem with e.g. routing
   */
  afOverrideLink: boolean;
  /**
   * Länkelementets 'onclick'-event.
   * @en The link element's 'onclick' event.
   */
  afOnClick: EventEmitter<MouseEvent>;
  clickLinkHandler(e: any): void;
  initRouting(): void;
  componentDidLoad(): void;
  get cssModifiers(): {
    [x: string]: boolean;
  };
  render(): any;
}
