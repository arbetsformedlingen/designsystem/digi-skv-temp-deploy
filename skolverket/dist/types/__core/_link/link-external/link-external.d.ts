import { EventEmitter } from '../../../stencil-public-runtime';
import { LinkExternalVariation } from './link-external-variation.enum';
import { HTMLStencilElement } from '../../../stencil-public-runtime';
/**
 * @slot default - Ska vara en textnod
 *
 * @enums LinkExternalVariation - link-external-variation.enum.ts
 *@swedishName Extern länk
 */
export declare class LinkExternal {
  hostElement: HTMLStencilElement;
  /**
   * Sätter attributet 'href'.
   * @en Set `href` attribute.
   */
  afHref: string;
  /**
   * Sätter variant. Kan vara 'small' eller 'large'.
   * @en Sets the variation of the link.
   */
  afVariation: `${LinkExternalVariation}`;
  /**
   * Sätter attributet 'target'. Om värdet är '_blank' så lägger komponenten automatikt till ett ´ref´-attribut med 'noopener noreferrer'.
   * @en Set `target` attribute. If value is '_blank', the component automatically adds a 'ref' attribute with 'noopener noreferrer'.
   */
  afTarget: string;
  /**
   * Kringgår länkens vanliga beteende.
   * Bör endast användas om det vanliga beteendet är problematiskt pga dynamisk routing eller liknande.
   * @en Override default link behavior. Should only be used if default link behaviour is a problem with e.g. routing
   */
  afOverrideLink: boolean;
  /**
   * Länkelementets 'onclick'-event.
   * @en The link element's 'onclick' event.
   */
  afOnClick: EventEmitter<MouseEvent>;
  clickLinkHandler(e: any): void;
  initRouting(): void;
  componentDidLoad(): void;
  get cssModifiers(): {
    [x: string]: boolean;
  };
  render(): any;
}
