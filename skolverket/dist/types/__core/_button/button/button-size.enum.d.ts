export declare enum ButtonSize {
  SMALL = "small",
  MEDIUM = "medium",
  LARGE = "large"
}
