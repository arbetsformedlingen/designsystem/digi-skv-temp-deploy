export declare enum ButtonType {
  BUTTON = "button",
  SUBMIT = "submit",
  RESET = "reset"
}
