export declare enum ButtonVariation {
  PRIMARY = "primary",
  SECONDARY = "secondary",
  FUNCTION = "function"
}
