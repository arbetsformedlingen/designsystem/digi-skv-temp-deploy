import { EventEmitter } from '../../../stencil-public-runtime';
import { HTMLStencilElement } from '../../../stencil-public-runtime';
import { ButtonSize } from './button-size.enum';
import { ButtonType } from './button-type.enum';
import { ButtonVariation } from './button-variation.enum';
/**
 * @slot default - Ska vara en textnod
 * @slot icon - Ska vara en ikon (eller en ikonlik) komponent
 * @slot icon-secondary - Ska vara en ikon (eller en ikonlik) komponent
 *
 * @enums ButtonSize - button-size.enum.ts
 * @enums ButtonType - button-type.enum.ts
 * @enums ButtonVariation - button-variation.enum.ts
 * @swedishName Knapp
 */
export declare class Button {
  hostElement: HTMLStencilElement;
  private _button?;
  hasIcon: boolean;
  hasIconSecondary: boolean;
  isFullSize: boolean;
  /**
   * Sätter knappens storlek. 'medium' är förvalt.
   * @en Set button size. Defaults to medium size.
   */
  afSize: `${ButtonSize}`;
  /**
   * Sätter attributet 'tabindex'.
   * @en Sets a tabindex.
   */
  afTabindex: string;
  /**
   * Sätt variant. Om varianten är 'tertiary' så krävs även en ikon.
   * @en Set button variation. If set to FUNCTION, the button also needs an icon.
   */
  afVariation: `${ButtonVariation}`;
  /**
   * Sätt attributet 'type'.
   * @en Set button `type` attribute.
   */
  afType: `${ButtonType}`;
  /**
   * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Input id attribute. Defaults to random string.
   */
  afId: string;
  /**
   * Sätt attributet 'aria-label'.
   * @en Set button `aria-label` attribute.
   */
  afAriaLabel: string;
  /**
   * Sätt attributet 'aria-labelledby'.
   * @en Set button `aria-labelledby` attribute.
   */
  afAriaLabelledby: string;
  /**
   * Sätter attributet 'aria-controls'.
   * @en Set button `aria-controls` attribute.
   */
  afAriaControls: string;
  /**
   * Sätter attributet 'form'.
   * @en Set button `form` attribute.
   */
  afForm: string;
  /**
   * Sätter attributet 'aria-pressed'.
   * @en Set button `aria-pressed` attribute.
   */
  afAriaPressed: boolean;
  /**
   * Sätter attributet 'aria-expanded'.
   * @en Set button `aria-expanded` attribute.
   */
  afAriaExpanded: boolean;
  /**
   * Sätter attributet 'aria-haspopup'.
   * @en Set button `aria-haspopup` attribute.
   */
  afAriaHaspopup: boolean;
  /**
   * Sätter attributet 'lang'.
   * @en Set button `lang` attribute.
   */
  afLang: string;
  /**
   * Sätter attributet 'dir'.
   * @en Set button `dir` attribute.
   */
  afDir: string;
  /**
   * Sätter knappens bredd till 100%.
   * @en Set button width to 100%.
   */
  afFullWidth: boolean;
  fullWidthHandler(): void;
  /**
   * Sätter attributet 'autofocus'.
   * @en Input autofocus attribute
   */
  afAutofocus: boolean;
  /**
   * Buttonelementets 'onclick'-event.
   * @en The button element's 'onclick' event.
   */
  afOnClick: EventEmitter<MouseEvent>;
  /**
   * Buttonelementets 'onfocus'-event.
   * @en The button element's 'onfocus' event.
   */
  afOnFocus: EventEmitter<FocusEvent>;
  /**
   * Buttonelementets 'onblur'-event.
   * @en The button element's 'onblur' event.
   */
  afOnBlur: EventEmitter;
  /**
   * Hämta en referens till buttonelementet. Bra för att t.ex. sätta fokus programmatiskt.
   * @en Returns a reference to the button element. Handy for setting focus programmatically.
   */
  afMGetButtonElement(): Promise<HTMLButtonElement>;
  componentWillLoad(): void;
  componentWillUpdate(): void;
  setHasIcon(): void;
  get cssModifiers(): {
    [x: string]: boolean;
    'digi-button--icon': boolean;
    'digi-button--icon-secondary': boolean;
    'digi-button--full-width': boolean;
  };
  get pressedState(): "true" | "false";
  get expandedState(): "true" | "false";
  get hasPopup(): "true" | "false";
  clickHandler(e: MouseEvent): void;
  focusHandler(e: FocusEvent): void;
  blurHandler(e: any): void;
  render(): any;
}
