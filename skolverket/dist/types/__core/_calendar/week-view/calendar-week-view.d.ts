import { EventEmitter } from '../../../stencil-public-runtime';
import { CalendarWeekViewHeadingLevel } from './calendar-week-view-heading-level.enum';
/**
 * @enums CalendarWeekViewHeadingLevel - calendar-week-view-heading-level.enum.ts
 * @swedishName Veckovy
 */
export declare class CalendarWeekView {
  private minDate;
  private maxDate;
  dates: any[];
  weekDays: any[];
  todaysDate: string;
  selectedDate: string;
  selectedDateChangeHandler(): void;
  currentDate: any;
  currentDateChangeHandler(): void;
  setWeekDates(): void;
  afMinWeek: number;
  afMaxWeek: number;
  /**
   * Lista över valbara datum
   * @en List of selectable dates
   */
  afDates: string;
  afDatesUpdate(): void;
  /**
   * Sätt rubrikens vikt. 'h2' är förvalt.
   * @en Set heading level. Default is 'h2'
   */
  afHeadingLevel: CalendarWeekViewHeadingLevel;
  /**
   * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Input id attribute. Defaults to random string.
   */
  afId: string;
  /**
   * Vid byte av vecka
   * @en When week changes
   */
  afOnWeekChange: EventEmitter<string>;
  /**
   * Vid byte av dag
   * @en When day changes
   */
  afOnDateChange: EventEmitter<string>;
  /**
   * Kan användas för att manuellt sätta om det aktiva datumet. Formatet måste vara 'yyyy-MM-dd'.
   * @en Can be used to set the active date. The format needs to be 'yyy-MM-dd'.
   */
  afMSetActiveDate(activeDate: string): Promise<void>;
  prevWeek(): void;
  nextWeek(): void;
  initDates(): void;
  componentWillLoad(): void;
  get prevWeekText(): string;
  get nextWeekText(): string;
  get currentWeek(): number;
  get currentMonth(): string;
  render(): any;
}
