import { EventEmitter } from '../../../stencil-public-runtime';
/**
 * @slot calendar-footer - displayed beneath the calendar. Preferable buttons.
 *
 */
export interface YearAndMonth {
  year: number;
  month: number;
}
/**
 * @swedishName Kalender
 */
export declare class Calendar {
  activeCalendar: any[];
  months: any[];
  focusedDate: Date;
  dirty: boolean;
  private _tbody;
  private _nextButton;
  /**
   * Prefix för komponentens alla olika interna 'id'-attribut på kalendern etc. Ex. 'my-cool-id' genererar 'my-cool-id-calendar' osv. Ges inget värde genereras ett slumpmässigt.
   * @en Prefix for all id attributes inside the component. Example: 'my-cool-id' generates 'my-cool-id-calendar' and so on. Defaults to random string.
   */
  afId: string;
  /**
   * Vald månad att visa från start. Förvalt är den månad som dagens datum har. (Januari = 0, December = 11)
   * @en Selected month to display from start. Defaults to the month of todays month. (Januray = 0, December = 11)
   */
  afInitSelectedMonth: number;
  /**
   * Visa veckonummer i kalender. Förvalt är false
   * @en Show week number in the calander. Default is false
   */
  afShowWeekNumber: boolean;
  /**
   * Valt datum i kalendern
   * @en Selected date in calendar
   */
  afSelectedDate: Date;
  /**
   * Visar döljer kalendern. Visas som förvalt
   * @en Toggle calendar hide/show. Defaults to true
   */
  afActive: boolean;
  /**
   * Sätt denna till true för att kunna markera mer en ett datum i kalendern. Förvalt är false
   * @en Set this to true to be able to select multiple dates in the calander. Default is false
   */
  afMultipleDates: boolean;
  /**
   * Sker när ett datum har valts eller avvalts. Returnerar datumen i en array.
   * @en When a date is selected. Return the dates in an array.
   */
  afOnDateSelectedChange: EventEmitter;
  /**
   * Sker när fokus flyttas utanför kalendern
   * @en When focus moves out from the calendar
   */
  afOnFocusOutside: EventEmitter;
  /**
   * Sker vid klick utanför kalendern
   * @en When click outside the calendar
   */
  afOnClickOutside: EventEmitter;
  /**
   * Sker när kalenderdatumen har rörts första gången, när värdet på focusedDate har ändrats första gången.
   * @en When the calendar dates are touched the first time, when focusedDate is changed the first time.
   */
  afOnDirty: EventEmitter;
  lastWeekOfPreviousMonth: Date[];
  firstWeekOfNextMonth: Date[];
  weeksBetween: number;
  weekDaysbetween: Date[];
  selectedDates: Date[];
  focusoutHandler(e: FocusEvent): void;
  clickHandler(e: MouseEvent): void;
  drawCalendar(focusedDate: Date): void;
  appendFirstWeekOfNextMonth(): Date[];
  getLastDayOfPrevMonth(date: YearAndMonth): Date;
  getWeekDays(date?: Date): Date[];
  getFirstDayOfNextMonth(date: YearAndMonth): Date;
  getWeeksBetween(lastDayOfPrevMonth: Date, firstDayOnNextMonth: Date): number;
  getWeekdaysOfMonth(weeksBetween: number): any[];
  getUpcoming(weeks: number): Date;
  onDirty(): void;
  selectDateHandler(dateSelected: any): void;
  getMonthName(monthNumber: number): string;
  getFullMonthName(monthNumber: number): string;
  getWeekNumbers(dateInput: Date): number;
  isSameMonth(date: Date, currentDate: Date): boolean;
  isSameDate(date: Date, newDate: Date): boolean;
  isDateSelected(date: any): boolean;
  isDisabledDate(date: Date): boolean;
  subtractMonth(): void;
  addMonth(): void;
  getCurrent(date: Date): string;
  setTabIndex(date: Date): -1 | 0;
  navigateHandler(e: any, daysToAdd: number): void;
  shiftTabHandler(): void;
  resetFocus(): void;
  getFullDate(date: Date): string;
  getDateName(date: Date): string;
  setFocused(date: Date): boolean;
  ariaPressed(date: Date): "true" | "false";
  componentWillLoad(): void;
  componentWillUpdate(): void;
  cssDateModifiers(date: Date): {
    'digi-calendar__date--today': boolean;
    'digi-calendar__date--disabled': boolean;
    'digi-calendar__date--focused': boolean;
    'digi-calendar__date--selected': boolean;
  };
  render(): any;
}
