/**
 * @slot default - Should be a text node
 * @swedishName Ingress
 */
export declare class TypographyPreamble {
  render(): any;
}
