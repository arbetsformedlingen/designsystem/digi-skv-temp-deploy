export declare enum TypographyMetaVariation {
  PRIMARY = "primary",
  SECONDARY = "secondary"
}
