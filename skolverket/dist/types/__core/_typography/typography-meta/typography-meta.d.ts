import { TypographyMetaVariation } from './typography-meta-variation.enum';
/**
 * @slot default - Kan användas för olika element, ett text element är föredraget.
 * @slot secondary - Kan användas för olika element, ett text element är föredraget.
 *
 * @enums TypographyMetaVariation - typography-meta-variation.enum.ts
 * @swedishName Metatext
 */
export declare class TypographyMeta {
  /**
   * Sätter variant. Kan vara primär eller sekundär.
   * @en Set variation. Can be primary or secondary.
   */
  afVariation: `${TypographyMetaVariation}`;
  get cssModifiers(): {
    [x: string]: boolean;
  };
  render(): any;
}
