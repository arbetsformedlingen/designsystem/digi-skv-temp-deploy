import { TypographyVariation } from './typography-variation.enum';
/**
 * @slot default - Kan innehålla vad som helst
 *
 * @enums TypographyVariation - typography-variation.enum.ts
 * @swedishName Typografi
 */
export declare class Typography {
  /**
   * Sätter variant. Kan vara 'small' eller 'large'
   * @en Set variation. Can be 'small' or 'large'
   */
  afVariation: TypographyVariation;
  get cssModifiers(): {
    'digi-typography--large': boolean;
    'digi-typography--small': boolean;
  };
  render(): any;
}
