export declare enum TypographyVariation {
  SMALL = "small",
  LARGE = "large"
}
