export declare enum TypographyTimeVariation {
  PRIMARY = "primary",
  PRETTY = "pretty",
  DISTANCE = "distance"
}
