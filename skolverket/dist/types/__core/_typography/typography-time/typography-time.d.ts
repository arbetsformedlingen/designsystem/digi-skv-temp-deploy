import { TypographyTimeVariation } from './typography-time-variation.enum';
/**
 * @enums TypographyTimeVariation - typography-time-variation.enum.ts
 *@swedishName Datum
 */
export declare class TypographyTime {
  formatedDate: string;
  datetime: string;
  /**
   * Ska vara ett datumobjekt, datumsträng eller ett datumformatterat nummer
   * @en Set the date time to a Date object, date string or a valid date formatted number.
   */
  afDateTime: number | Date | string;
  /**
   * Sätter variant. Bestämmer hur det ska formatteras. Kan vara 'primary', 'pretty' eller 'distance'.
   * @en Set variation. Can be 'primary', 'pretty' or 'distance'.
   */
  afVariation: `${TypographyTimeVariation}`;
  primaryFormat(date: Date): string;
  prettyFormat(date: Date): string;
  distanceFormat(date: Date): string;
  formatDate(dateToFormat: number | Date | string): void;
  componentWillLoad(): void;
  render(): any;
}
