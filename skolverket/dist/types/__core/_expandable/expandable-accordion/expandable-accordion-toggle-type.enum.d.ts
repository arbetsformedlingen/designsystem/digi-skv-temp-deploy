export declare enum ExpandableAccordionToggleType {
  IMPLICIT = "implicit",
  EXPLICIT = "explicit"
}
