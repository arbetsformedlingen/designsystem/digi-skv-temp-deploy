export declare enum ExpandableAccordionVariation {
  PRIMARY = "primary",
  SECONDARY = "secondary"
}
