import { EventEmitter, HTMLStencilElement } from '../../../stencil-public-runtime';
import { ExpandableAccordionHeaderLevel } from './expandable-accordion-header-level.enum';
import { ExpandableAccordionVariation } from './expandable-accordion-variation.enum';
/**
 * @enums ExpandableAccordionHeaderLevel - expandable-accordion-header-level.enum.ts
 * @swedishName Utfällbar rubrik
 */
export declare class ExpandableAccordion {
  private _button;
  hostElement: HTMLStencilElement;
  /**
   * Utfällbara ytans variation
   * @en Set component variation
   */
  afVariation: ExpandableAccordionVariation;
  /**
   * Rubrikens Utfällbar yta
   * @en The heading text
   */
  afHeading: string;
  /**
   * Sätter utfällbar rubrik. 'h2' är förvalt.
   * @en Set heading level. Default is 'h2'
   */
  afHeadingLevel: ExpandableAccordionHeaderLevel;
  /**
   * Sätter läge på rubriken om den ska vara expanderad eller ej.
   * @en Sets the state of the accordion to expanded or not.
   */
  afExpanded: boolean;
  /**
   * Ställer in ifall komponenten ska ha en animation vid utfällning. Animation är påslagen som standard.
   * @en Sets the state of the animation for the accordion's expanded area. Animation is on as default.
   */
  afAnimation: boolean;
  /**
   * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Set id attribute. Defaults to random string.
   */
  afId: string;
  /**
   * Buttonelementets 'onclick'-event.
   * @en The button element's 'onclick' event.
   */
  afOnClick: EventEmitter<MouseEvent>;
  get cssModifiers(): {
    [x: string]: boolean;
  };
  /**
   * Hämtar en höjden till innehållet i Utfällbar yta och animera innehållet.
   * @en Gets a height to the content for expandable and animate the content.
   */
  componentDidLoad(): void;
  animateContentHeight(): void;
  clickToggleHandler(e: MouseEvent, resetFocus?: boolean): void;
  render(): any;
}
