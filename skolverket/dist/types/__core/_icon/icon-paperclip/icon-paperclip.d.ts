export declare class IconPaperclip {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  afTitle: string;
  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  afDesc: string;
  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  afSvgAriaHidden: boolean;
  render(): any;
}
