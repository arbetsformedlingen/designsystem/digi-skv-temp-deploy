import { EventEmitter } from '../../../stencil-public-runtime';
/**
 * @slot default - Kan innehålla vad som helst
 * @swedishName Keydown Handler
 */
export declare class UtilKeydownHandler {
  /**
   * Vid keydown på escape
   * @en At keydown on escape key
   */
  afOnEsc: EventEmitter<KeyboardEvent>;
  /**
   * Vid keydown på enter
   * @en At keydown on enter key
   */
  afOnEnter: EventEmitter<KeyboardEvent>;
  /**
   * Vid keydown på tab
   * @en At keydown on tab key
   */
  afOnTab: EventEmitter<KeyboardEvent>;
  /**
   * Vid keydown på space
   * @en At keydown on space key
   */
  afOnSpace: EventEmitter<KeyboardEvent>;
  /**
   * Vid keydown på skift och tabb
   * @en At keydown on shift and tab key
   */
  afOnShiftTab: EventEmitter<KeyboardEvent>;
  /**
   * Vid keydown på pil upp
   * @en At keydown on up arrow key
   */
  afOnUp: EventEmitter<KeyboardEvent>;
  /**
   * Vid keydown på pil ner
   * @en At keydown on down arrow key
   */
  afOnDown: EventEmitter<KeyboardEvent>;
  /**
   * Vid keydown på pil vänster
   * @en At keydown on left arrow key
   */
  afOnLeft: EventEmitter<KeyboardEvent>;
  /**
   * Vid keydown på pil höger
   * @en At keydown on right arrow key
   */
  afOnRight: EventEmitter<KeyboardEvent>;
  /**
   * Vid keydown på home
   * @en At keydown on home key
   */
  afOnHome: EventEmitter<KeyboardEvent>;
  /**
   * Vid keydown på end
   * @en At keydown on end key
   */
  afOnEnd: EventEmitter<KeyboardEvent>;
  /**
   * Vid keydown på alla tangenter
   * @en At keydown on any key
   */
  afOnKeyDown: EventEmitter<KeyboardEvent>;
  keydownHandler(e: KeyboardEvent): void;
  render(): any;
}
