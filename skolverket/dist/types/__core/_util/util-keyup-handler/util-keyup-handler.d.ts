import { EventEmitter } from '../../../stencil-public-runtime';
/**
 * @slot default - Can be anything
 * @swedishName Keyup Handler
 */
export declare class UtilKeyupHandler {
  /**
   * Vid keyup på escape
   * @en At keyup on escape key
   */
  afOnEsc: EventEmitter<KeyboardEvent>;
  /**
   * Vid keyup på enter
   * @en At keyup on enter key
   */
  afOnEnter: EventEmitter<KeyboardEvent>;
  /**
   * Vid keyup på tab
   * @en At keyup on tab key
   */
  afOnTab: EventEmitter<KeyboardEvent>;
  /**
   * Vid keyup på space
   * @en At keyup on space key
   */
  afOnSpace: EventEmitter<KeyboardEvent>;
  /**
   * Vid keyup på skift och tabb
   * @en At keyup on shift and tab key
   */
  afOnShiftTab: EventEmitter<KeyboardEvent>;
  /**
   * Vid keyup på pil upp
   * @en At keyup on up arrow key
   */
  afOnUp: EventEmitter<KeyboardEvent>;
  /**
   * Vid keyup på pil ner
   * @en At keyup on down arrow key
   */
  afOnDown: EventEmitter<KeyboardEvent>;
  /**
   * Vid keyup på pil vänster
   * @en At keyup on left arrow key
   */
  afOnLeft: EventEmitter<KeyboardEvent>;
  /**
   * Vid keyup på pil höger
   * @en At keyup on right arrow key
   */
  afOnRight: EventEmitter<KeyboardEvent>;
  /**
   * Vid keyup på home
   * @en At keyup on home key
   */
  afOnHome: EventEmitter<KeyboardEvent>;
  /**
   * Vid keyup på end
   * @en At keyup on end key
   */
  afOnEnd: EventEmitter<KeyboardEvent>;
  /**
   * Vid keyup på alla tangenter
   * @en At keyup on any key
   */
  afOnKeyUp: EventEmitter<KeyboardEvent>;
  keyupHandler(e: KeyboardEvent): void;
  render(): any;
}
