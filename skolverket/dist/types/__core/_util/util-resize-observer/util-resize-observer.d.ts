import { EventEmitter } from '../../../stencil-public-runtime';
/**
 * @slot default - Kan innehålla vad som helst
 * @swedishName Resize Observer
 */
export declare class UtilResizeObserver {
  hostElement: HTMLElement;
  private _observer;
  /**
   * När komponenten ändrar storlek
   * @en When the component changes size
   */
  afOnChange: EventEmitter<ResizeObserverEntry>;
  componentWillLoad(): void;
  disconnectedCallback(): void;
  initObserver(): void;
  removeObserver(): void;
  changeHandler(e: any): void;
  render(): any;
}
