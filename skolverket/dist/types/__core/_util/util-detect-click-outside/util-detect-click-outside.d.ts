import { EventEmitter } from '../../../stencil-public-runtime';
/**
 * @slot default - Kan innehålla vad som helst
 * @swedishName Detect Click Outside
 */
export declare class UtilDetectClickOutside {
  $el: HTMLElement;
  /**
   * Sätter en unik identifierare som behövs för att kontrollera klickeventen. Måste vara preficad med 'data-'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Set unique identifier needed for click control. Defaults to random string. Should be prefixed with 'data-'
   */
  afDataIdentifier: string;
  /**
   * Vid klick utanför komponenten
   * @en When click detected outside of component
   */
  afOnClickOutside: EventEmitter<MouseEvent>;
  /**
   * Vid klick inuti komponenten
   * @en When click detected inside of component
   */
  afOnClickInside: EventEmitter<MouseEvent>;
  constructor();
  clickHandler(e: MouseEvent): void;
  render(): any;
}
