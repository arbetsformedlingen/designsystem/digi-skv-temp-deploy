export declare enum UtilBreakpointObserverBreakpoints {
  SMALL = "small",
  MEDIUM = "medium",
  LARGE = "large",
  XLARGE = "xlarge"
}
