import { EventEmitter } from '../../../stencil-public-runtime';
import { HTMLStencilElement } from '../../../stencil-public-runtime';
/**
 * @slot default - Kan innehålla vad som helst
 * @enums UtilBreakpointObserverBreakpoints - util-breakpoint-observer-breakpoints.enum.ts
 * @swedishName Breakpoint Observer
 *
 */
export declare class UtilBreakpointObserver {
  hostElement: HTMLStencilElement;
  private _observer;
  componentDidLoad(): void;
  /**
   * Vid inläsning av sida samt vid uppdatering av brytpunkt
   * @en At page load and at change of breakpoint on page
   */
  afOnChange: EventEmitter;
  /**
   * Vid brytpunkt 'small'
   * @en At breakpoint 'small'
   */
  afOnSmall: EventEmitter;
  /**
   * Vid brytpunkt 'medium'
   * @en At breakpoint 'medium'
   */
  afOnMedium: EventEmitter;
  /**
   * Vid brytpunkt 'large'
   * @en At breakpoint 'large'
   */
  afOnLarge: EventEmitter;
  /**
   * Vid brytpunkt 'xlarge'
   * @en At breakpoint 'xlarge'
   */
  afOnXLarge: EventEmitter;
  handleBreakpoint(e?: Event): void;
  render(): any;
}
