import { EventEmitter } from '../../../stencil-public-runtime';
/**
 * @slot default - Kan innehålla vad som helst
 * @swedishName Intersection Observer
 */
export declare class UtilIntersectionObserver {
  private _observer;
  private _hasIntersected;
  private _isObserving;
  private _afOptions;
  $el: HTMLElement;
  /**
   * Stäng av observern efter första intersection
   * @en Disconnect the observer after first intersection.
   */
  afOnce: boolean;
  /**
   * Skicka options till komponentens interna Intersection Observer. T.ex. för att kontrollera när bilden lazy loadas.
   * @en Accepts an Intersection Observer options object. Controls when the image should lazy-load.
   */
  afOptions: object | string;
  afOptionsWatcher(newValue: object | string): void;
  /**
   * Vid statusförändring mellan 'unintersected' och 'intersected'
   * @en On change between unintersected and intersected
   */
  afOnChange: EventEmitter<boolean>;
  /**
   * Vid statusförändring till 'intersected'
   * @en On every change to intersected.
   */
  afOnIntersect: EventEmitter;
  /**
   * Vid statusförändring till 'unintersected'
   * On every change to unintersected
   */
  afOnUnintersect: EventEmitter;
  componentWillLoad(): void;
  disconnectedCallback(): void;
  initObserver(): void;
  removeObserver(): void;
  intersectionHandler(): void;
  unintersectionHandler(): void;
  render(): any;
}
