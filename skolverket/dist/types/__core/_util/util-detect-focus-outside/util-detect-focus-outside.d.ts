import { EventEmitter } from '../../../stencil-public-runtime';
/**
 * @slot default - Kan innehålla vad som helst
 * @swedishName Detect Focus Outside
 *
 */
export declare class UtilDetectFocusOutside {
  $el: HTMLElement;
  /**
   * Sätter en unik identifierare som behövs för att kontrollera fokuseventen. Måste vara prefixad med 'data-'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Set unique identifier needed for focus control. Defaults to random string. Should be prefixed with 'data-'
   */
  afDataIdentifier: string;
  /**
   * Vid fokus utanför komponenten
   * @en When focus detected outside of component
   */
  afOnFocusOutside: EventEmitter<FocusEvent>;
  /**
   * Vid fokus inuti komponenten
   * @en When focus detected inside of component
   */
  afOnFocusInside: EventEmitter<FocusEvent>;
  constructor();
  focusinHandler(e: FocusEvent): void;
  render(): any;
}
