import { EventEmitter } from '../../../stencil-public-runtime';
/**
 * @slot default - Kan innehålla vad som helst
 * @swedishName Mutation Observer
 */
export declare class UtilMutationObserver {
  $el: HTMLElement;
  private _observer;
  private _isObserving;
  private _afOptions;
  /**
   * Sätter attributet 'id' på det omslutande elementet. Ges inget värde så genereras ett slumpmässigt.
   * @en Label id attribute. Defaults to random string.
   */
  afId: string;
  /**
   * Skicka options till komponentens interna Mutation Observer. T.ex. för att kontrollera förändring i en slot.
   * @en Accepts an Mutation Observer options object. Controls changes .
   */
  afOptions: object | string;
  afOptionsWatcher(newValue: object | string): void;
  /**
   * När DOM-element läggs till eller tas bort inuti Mutation Observer:n
   * @en When DOM-elements is added or removed inside the Mutation Observer
   */
  afOnChange: EventEmitter;
  componentWillLoad(): void;
  disconnectedCallback(): void;
  initObserver(): void;
  removeObserver(): void;
  changeHandler(e: any): void;
  render(): void;
}
