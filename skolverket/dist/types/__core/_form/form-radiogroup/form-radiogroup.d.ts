import { EventEmitter } from '../../../stencil-public-runtime';
import { HTMLStencilElement } from '../../../stencil-public-runtime';
/**
 * @slot radiobuttons
 *
 * @swedishName Radiogrupp
 */
export declare class FormRadiogroup {
  hostElement: HTMLStencilElement;
  radiobuttons: NodeListOf<HTMLDigiFormRadiobuttonElement>;
  /**
   * Värdet på radiogruppen
   * @en Radiogroup value
   */
  afValue?: string;
  OnAfValueChange(value: any): void;
  /**
   * Värdet på radiogruppen
   * @en Radiogroup value
   * @ignore
   */
  value?: string;
  OnValueChange(value: any): void;
  /**
   * Namnet på radiogruppen
   * @en Radiogroup name
   */
  afName: string;
  /**
   * Event när värdet ändras
   * @en Event when value changes
   */
  afOnGroupChange: EventEmitter<string>;
  radiobuttonChangeListener(e: any): void;
  componentWillLoad(): void;
  componentDidLoad(): void;
  componentWillUpdate(): void;
  setRadiobuttons(value?: any): void;
  get cssModifiers(): {};
  render(): any;
}
