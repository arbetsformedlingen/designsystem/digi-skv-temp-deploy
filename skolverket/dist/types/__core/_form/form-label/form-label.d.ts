import { HTMLStencilElement } from '../../../stencil-public-runtime';
/**
 * @slot actions - Förväntar sig interaktiva och ikonstora komponenter
 * @swedishName Etikett
 */
export declare class FormLabel {
  hostElement: HTMLStencilElement;
  labelText: string;
  hasActionSlot: boolean;
  /**
   * Texten till labelelementet
   * @en The label text
   */
  afLabel: string;
  /**
   * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Set id attribute. Defaults to random string.
   */
  afId: string;
  /**
   * Sätter attributet 'for'. Ska vara identiskt med attributet 'id' på inputelementet
   * @en Set for attribute. Should be identical with the id attribute of the associated input element.
   */
  afFor: string;
  /**
   * Valfri beskrivande text
   * @en A description text
   */
  afDescription: string;
  /**
   * Aktivera om det tillhörande inputelementet är obligatoriskt
   * @en Set to true if the associated input element is required
   */
  afRequired: boolean;
  /**
   * Sätt denna till true om formuläret innehåller fler obligatoriska fält än valfria.
   * @en Set this to true if the form contains more required fields than optional fields.
   */
  afAnnounceIfOptional: boolean;
  /**
   * Sätter text för afRequired.
   * @en Set text for afRequired.
   */
  afRequiredText: string;
  /**
   * Sätter text för afAnnounceIfOptional.
   * @en Set text for afAnnounceIfOptional.
   */
  afAnnounceIfOptionalText: string;
  componentWillLoad(): void;
  validateLabel(): void;
  validateFor(): void;
  setLabelText(): void;
  handleSlotVisibility(): void;
  get requiredText(): string;
  render(): any;
}
