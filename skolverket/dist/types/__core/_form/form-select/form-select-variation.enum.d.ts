export declare enum FormSelectVariation {
  SMALL = "small",
  MEDIUM = "medium",
  LARGE = "large"
}
