export declare enum FormSelectValidation {
  SUCCESS = "success",
  ERROR = "error",
  WARNING = "warning",
  NEUTRAL = "neutral"
}
