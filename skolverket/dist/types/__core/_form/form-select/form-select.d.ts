import { EventEmitter } from '../../../stencil-public-runtime';
import { HTMLStencilElement } from '../../../stencil-public-runtime';
import { FormSelectValidation } from './form-select-validation.enum';
import { FormSelectVariation } from './form-select-variation.enum';
/**
 * @slot default - Should be an option element
 *
 * @enums FormSelectValidation - form-select-validation.enum
 * @enums FormSelectVariation - form-select-variation.enum
 * @swedishName Väljare
 */
export declare class FormSelect {
  hostElement: HTMLStencilElement;
  private _select?;
  private _observer;
  optionItems: any[];
  dirty: boolean;
  touched: boolean;
  hasPlaceholder: boolean;
  /**
   * Sätter en text för select.
   * @en Sets a label for select.
   */
  afLabel: string;
  /**
   * Är det ihopkopplade fältet tvingat?
   * @en Is the associated field required?
   */
  afRequired: boolean;
  /**
   * Sätter text för afRequired.
   * @en Set text for afRequired.
   */
  afRequiredText: string;
  /**
   * En platshållare för väljaren
   * @en A placeholder for the select
   */
  afPlaceholder: string;
  /**
   * Normalt betonas ett obligatoriskt fält visuellt och semantiskt.
   * Detta vänder på det och betonar fältet om det inte krävs.
   * Ställ in detta till sant om ditt formulär innehåller fler obligatoriska fält än inte.
   * @en Normally a required field is visually and semantically emphasized.
   * This flips that and emphasizes the field if it is not required.
   * Set this to true if your form contains more required fields than not.
   */
  afAnnounceIfOptional: boolean;
  /**
   * Sätter text för afAnnounceIfOptional.
   * @en Set text for afAnnounceIfOptional.
   */
  afAnnounceIfOptionalText: string;
  /**
   * Sätter ett id attribut.
   * @en Sets an id attribute. Standard är slumpmässig sträng.
   */
  afId: string;
  /**
   * Sätter väljarens namn.
   * @en Sets the select name.
   */
  afName: string;
  /**
   * En valfri beskrivningstext.
   * @en An optional description text.
   */
  afDescription: string;
  /**
   * Sätter attributet 'value'
   * @en Input value attribute
   * @ignore
   */
  value: string;
  onValueChanged(value: any): void;
  /**
   * Sätter attributet 'value'.
   * @en Input value attribute
   */
  afValue: string;
  onAfValueChanged(value: any): void;
  /**
   * Inaktiverar valideringen
   * @en Disables the validation.
   */
  afDisableValidation: boolean;
  /**
   * Sätter väljarens storlek.
   * @en Set select input size.
   */
  afVariation: FormSelectVariation;
  /**
   * Sätter väljarens validering
   * @en Set select validation
   */
  afValidation: FormSelectValidation;
  /**
   * Sätter valideringstexten.
   * @en Sets the validation text.
   */
  afValidationText: string;
  /**
   * Sätter ett förvalt värde.
   * @deprecated Använd afValue istället
   * @en Sets initial selected value.
   */
  afStartSelected: number;
  /**
   * Sätter attributet 'autofocus'.
   * @en Input autofocus attribute
   */
  afAutofocus: boolean;
  afOnChange: EventEmitter;
  afOnFocus: EventEmitter;
  afOnFocusout: EventEmitter;
  afOnBlur: EventEmitter;
  /**
   * Sker första gången väljarelementet ändras.
   * @en First time the select element is changed.
   */
  afOnDirty: EventEmitter;
  getOptions(): void;
  checkIfPlaceholder(): void;
  componentWillLoad(): void;
  componentDidLoad(): void;
  /**
   * Returnerar en referens till formulärkontrollelementet.
   * @en Returns a reference to the form control element.
   */
  afMGetFormControlElement(): Promise<HTMLSelectElement>;
  get cssModifiers(): {
    'digi-form-select--small': boolean;
    'digi-form-select--medium': boolean;
    'digi-form-select--large': boolean;
    'digi-form-select--neutral': boolean;
    'digi-form-select--success': boolean;
    'digi-form-select--warning': boolean;
    'digi-form-select--error': boolean;
    'digi-form-select--empty': boolean;
  };
  changeHandler(e: any): void;
  focusHandler(e: any): void;
  focusoutHandler(e: any): void;
  blurHandler(e: any): void;
  getSelectedItem(option: any): boolean;
  showValidation(): boolean;
  render(): any;
}
