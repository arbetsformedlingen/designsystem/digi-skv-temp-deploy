import { EventEmitter } from '../../../stencil-public-runtime';
import { HTMLStencilElement } from '../../../stencil-public-runtime';
import { FormInputSearchVariation } from './form-input-search-variation.enum';
import { FormInputType } from '../form-input/form-input-type.enum';
import { ButtonType } from '../../_button/button/button-type.enum';
import { FormInputButtonVariation } from '../form-input/form-input-button-variation.enum';
/**
 * @enums FormInputSearchVariation - form-input-search-variation.enum.ts
 * @swedishName Sökfält
 */
export declare class FormInputSearch {
  hostElement: HTMLStencilElement;
  private _input?;
  /**
   * Texten till labelelementet
   * @en The label text
   */
  afLabel: string;
  /**
   * Valfri beskrivande text
   * @en A description text
   */
  afLabelDescription: string;
  /**
   * Sätter attributet 'type'.
   * @en Input type attribute
   */
  afType: FormInputType;
  /**
   * Layout för hur inmatningsfältet ska visas då det finns en knapp
   * @en Input layout when there is a button
   */
  afButtonVariation: FormInputButtonVariation;
  /**
   * Sätter attributet 'name'.
   * @en Input name attribute
   */
  afName: string;
  /**
   * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Input id attribute. Defaults to random string.
   */
  afId: string;
  /**
   * Sätter attributet 'value'.
   * @en Input value attribute
   * @ignore
   */
  value: string;
  onValueChanged(value: any): void;
  /**
   * Sätter attributet 'value'.
   * @en Input value attribute
   */
  afValue: string;
  onAfValueChanged(value: any): void;
  /**
   * Sätter attributet 'autocomplete'.
   * @en Input autocomplete attribute
   */
  afAutocomplete: string;
  /**
   * Sätter attributet 'aria-autocomplete'.
   * @en Input aria-autocomplete attribute
   */
  afAriaAutocomplete: string;
  /**
   * Sätter attributet 'aria-activedescendant'.
   * @en Input aria-activedescendant attribute
   */
  afAriaActivedescendant: string;
  /**
   * Sätter attributet 'aria-labelledby'.
   * @en Input aria-labelledby attribute
   */
  afAriaLabelledby: string;
  /**
   * Sätter attributet 'aria-describedby'.
   * @en Input aria-describedby attribute
   */
  afAriaDescribedby: string;
  /**
   * Sätter variant. Kan vara 'small', 'medium' eller 'large'
   * @en Set input variation.
   */
  afVariation: FormInputSearchVariation;
  /**
   * Dölj knappen
   * @en Hide the button
   */
  afHideButton: boolean;
  /**
   * Sätter knappelementets attribut 'type'. Bör vara 'submit' eller 'button'
   * @en Set button element's type attribute.
   */
  afButtonType: ButtonType;
  /**
   * Knappens text
   * @en The button text
   */
  afButtonText: string;
  /**
   * Knappens aria label text
   * @en The buttons aria label text
   */
  afButtonAriaLabel: string;
  /**
   * Button aria-labelledby attribute
   */
  afButtonAriaLabelledby: string;
  /**
   * Sätter attributet 'autofocus'.
   * @en Input autofocus attribute
   */
  afAutofocus: boolean;
  /**
   * Vid fokus utanför komponenten.
   * @en When focus is outside component.
   */
  afOnFocusOutside: EventEmitter;
  /**
   * Vid fokus inuti komponenten.
   * @en When focus is inside component.
   */
  afOnFocusInside: EventEmitter;
  /**
   * Vid klick utanför komponenten.
   * @en When click outside component.
   */
  afOnClickOutside: EventEmitter;
  /**
   * Vid klick inuti komponenten.
   * @en When click inside component.
   */
  afOnClickInside: EventEmitter;
  /**
   * Inputelementets 'onchange'-event.
   * @en The input element's 'onchange' event.
   */
  afOnChange: EventEmitter;
  /**
   * Inputelementets 'onblur'-event.
   * @en The input element's 'onblur' event.
   */
  afOnBlur: EventEmitter;
  /**
   * Inputelementets 'onkeyup'-event.
   * @en The input element's 'onkeyup' event.
   */
  afOnKeyup: EventEmitter;
  /**
   * Inputelementets 'onfocus'-event.
   * @en The input element's 'onfocus' event.
   */
  afOnFocus: EventEmitter;
  /**
   * Inputelementets 'onfocusout'-event.
   * @en The input element's 'onfocusout' event.
   */
  afOnFocusout: EventEmitter;
  /**
   * Inputelementets 'oninput'-event.
   * @en The input element's 'oninput' event.
   */
  afOnInput: EventEmitter;
  /**
   * Knappelementets 'onclick'-event.
   * @en The button element's 'onclick' event.
   */
  afOnClick: EventEmitter;
  /**
   * Hämtar en referens till inputelementet. Bra för att t.ex. sätta fokus programmatiskt.
   * @en Returns a reference to the input element. Handy for setting focus programmatically.
   */
  afMGetFormControlElement(): Promise<any>;
  blurHandler(e: any): void;
  changeHandler(e: any): void;
  focusHandler(e: any): void;
  focusoutHandler(e: any): void;
  keyupHandler(e: any): void;
  inputHandler(e: any): void;
  clickHandler(e: any): void;
  clickOutsideListener(e: MouseEvent): void;
  focusOutsideListener(e: FocusEvent): void;
  componentWillLoad(): void;
  get cssModifiers(): {
    'digi-form-input-search--small': boolean;
    'digi-form-input-search--medium': boolean;
    'digi-form-input-search--large': boolean;
  };
  render(): any;
}
