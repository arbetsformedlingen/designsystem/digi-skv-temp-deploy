import { EventEmitter } from '../../../stencil-public-runtime';
import { HTMLStencilElement } from '../../../stencil-public-runtime';
import { FormFileUploadVariation } from './form-file-upload-variation.enum';
import { FormFileUploadValidation } from './form-file-upload-validation.enum';
import { FormFileUploadHeadingLevel } from './form-file-upload-heading-level.enum';
/**
 *	@enums FormInputFileType - form-input-file-type.enum.ts
 *  @enums FormInputFileVariation - form-input-file-variation.enum.ts
 * 	@swedishName Filuppladdare
 */
export declare class FormFileUpload {
  hostElement: HTMLStencilElement;
  _input: HTMLInputElement;
  files: File[];
  error: boolean;
  errorMessage: string;
  fileHover: boolean;
  /**
   * Texten till labelelementet
   * @en The label text
   */
  afLabel: string;
  /**
   * Valfri beskrivande text
   * @en A description text
   */
  afLabelDescription: string;
  /**
   * Sätter variant. Kan vara 'primary', 'secondary' eller 'tertiary'.
   * @en Set input variation.
   */
  afVariation: FormFileUploadVariation;
  /**
   * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Input id attribute. Defaults to random string.
   */
  afId: string;
  /** Sätter attributet 'accept'. Använd för att limitera accepterade filtyper
   * @en Set input field's accept attribute. Use this to limit accepted file types.
   */
  afFileTypes: string;
  /**
   * Sätter en maximal filstorlek i MB, 10MB är standard.
   * @en Set file max size in MB
   */
  afFileMaxSize: number;
  /**
   * Sätter maximalt antal filer man kan ladda upp
   * @en Set max number of files you can upload
   */
  afMaxFiles: number;
  /**
   * Sätter attributet 'name'.
   * @en Input name attribute
   */
  afName: string;
  /**
   * Sätter ladda upp knappens text.
   * @en Set upload buttons text
   */
  afUploadBtnText: string;
  /**
   * Sätter attributet 'required'.
   * @en Input required attribute
   */
  afRequired: boolean;
  /**
   * Sätter text för afRequired.
   * @en Set text for afRequired.
   */
  afRequiredText: string;
  /**
   * Sätt denna till true om formuläret innehåller fler obligatoriska fält än valfria.
   * @en Set this to true if the form contains more required fields than optional fields.
   */
  afAnnounceIfOptional: boolean;
  /**
   * Sätt rubrik för uppladdade filer.
   * @en Set heading for uploaded files.
   */
  afHeadingFiles: string;
  /**
   * Sätter text för afAnnounceIfOptional.
   * @en Set text for afAnnounceIfOptional.
   */
  afAnnounceIfOptionalText: string;
  /**
   * Sätter antivirus scan status. Kan vara 'enable' eller 'disabled.
   * @en Set antivirus scan status. Enable is default.
   */
  afValidation: FormFileUploadValidation;
  /**
   * Sätt rubrikens vikt. 'h2' är förvalt.
   * @en Set heading level. Default is 'h2'
   */
  afHeadingLevel: FormFileUploadHeadingLevel;
  /**
   * Sänder ut fil vid uppladdning
   * @en Emits file on upload
   */
  afOnUploadFile: EventEmitter;
  /**
   * Sänder ut vilken fil som tagits bort
   * @en Emits which file that was deleted.
   */
  afOnRemoveFile: EventEmitter;
  /**
   * Sänder ut vilken fil som har avbrutis uppladdning
   * @en Emits which file that was canceled
   */
  afOnCancelFile: EventEmitter;
  /**
   * Sänder ut vilken fil som försöker laddas upp igen
   * @en Emits which file is trying to retry its upload
   */
  afOnRetryFile: EventEmitter;
  /**
   * Validera filens status.
   * @en Validate file status
   */
  afMValidateFile(scanInfo: any): Promise<void>;
  /**
   * Få ut alla uppladdade filer
   * @en Get all uploaded files
   */
  afMGetAllFiles(): Promise<File[]>;
  /**
   * Importera en array med filer till komponenten utan att trigga uppladdningsevent. Ett fil objekt måste innehålla id, status och filen själv.
   * @en Import an array with files without triggering upload event. A file object needs to contain an id, status and the file itself.
   */
  afMImportFiles(files: File[]): Promise<void>;
  afMGetFormControlElement(): Promise<HTMLInputElement>;
  get fileMaxSize(): number;
  validateFile(scanInfo: any): {
    message: string;
  };
  emitFile(incomingFile: File): void;
  onRetryFileHandler(e: Event, id: string): void;
  onCancelFileHandler(e: Event, id: string): void;
  removeFileFromList(id: string): File;
  getIndexOfFile(id: string): number;
  onButtonUploadFileHandler(e: any): void;
  onRemoveFileHandler(e: Event, id: any): void;
  addUploadedFiles(file: File): Promise<void>;
  toBase64: (file: Blob) => Promise<unknown>;
  handleDrop(e: DragEvent): void;
  handleAllowDrop(e: DragEvent): void;
  handleOnDragEnter(e: DragEvent): void;
  handleOnDragLeave(e: DragEvent): void;
  handleInputClick(e: Event): void;
  componentWillLoad(): void;
  componentDidLoad(): void;
  componentWillUpdate(): void;
  get cssModifiers(): {
    'digi-form-file-upload--primary': boolean;
    'digi-form-file-upload--secondary': boolean;
    'digi-form-file-upload--tertiary': boolean;
  };
  render(): any;
}
