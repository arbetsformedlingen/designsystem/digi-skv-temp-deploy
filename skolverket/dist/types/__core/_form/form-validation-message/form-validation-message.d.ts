import { HTMLStencilElement } from '../../../stencil-public-runtime';
import { IconName } from '../../../global/icon/icon';
import { FormValidationMessageVariation } from './form-validation-message-variation.enum';
/**
 * @slot default - Ska vara en textnod eller liknande element. Renderas inne i ett spanelement.
 *
 * @enums FormValidationMessageVariation - form-validation-message-variation.enum.ts
 * @swedishName Valideringsmeddelande
 */
export declare class FormValidationMessage {
  hostElement: HTMLStencilElement;
  /**
   * Sätter valideringsstatus. Kan vara 'success', 'error' eller 'warning'.
   * @en Set validation status
   */
  afVariation: FormValidationMessageVariation;
  get cssModifiers(): {
    'digi-form-validation-message--success': boolean;
    'digi-form-validation-message--error': boolean;
    'digi-form-validation-message--warning': boolean;
  };
  get icon(): IconName;
  render(): any;
}
