export declare enum FormValidationMessageVariation {
  SUCCESS = "success",
  ERROR = "error",
  WARNING = "warning"
}
