import { EventEmitter } from '../../../stencil-public-runtime';
import { HTMLStencilElement } from '../../../stencil-public-runtime';
import { FormTextareaVariation } from './form-textarea-variation.enum';
import { FormTextareaValidation } from './form-textarea-validation.enum';
/**
 * @enums FormTextareaValidation - form-textarea-validation.enum.ts
 * @enums FormTextareaVariation - form-textarea-variation.enum.ts
 * @swedishName Textarea
 */
export declare class FormTextarea {
  hostElement: HTMLStencilElement;
  private _textarea?;
  hasActiveValidationMessage: boolean;
  dirty: boolean;
  touched: boolean;
  /**
   * Texten till labelelementet
   * @en The label text
   */
  afLabel: string;
  /**
   * Valfri beskrivande text
   * @en A description text
   */
  afLabelDescription: string;
  /**
   * Sätter variant. Kan vara 'small', 'medium', 'large' eller 'auto'
   * @en Set variation.
   */
  afVariation: FormTextareaVariation;
  /**
   * Sätter attributet 'name'.
   * @en set name attribute
   */
  afName: string;
  /**
   * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Set id attribute. Defaults to random string.
   */
  afId: string;
  /**
   * Sätter attributet 'maxlength'.
   * @en Set maxlength attribute
   */
  afMaxlength: number;
  /**
   * Sätter attributet 'minlength'.
   * @en Set minlength attribute
   */
  afMinlength: number;
  /**
   * Sätter attributet 'required'.
   * @en Set required attribute
   */
  afRequired: boolean;
  /**
   * Sätter text för afRequired.
   * @en Set text for afRequired.
   */
  afRequiredText: string;
  /**
   * Sätt denna till true om formuläret innehåller fler obligatoriska fält än valfria.
   * @en Set this to true if the form contains more required fields than optional fields.
   */
  afAnnounceIfOptional: boolean;
  /**
   * Sätter text för afAnnounceIfOptional.
   * @en Set text for afAnnounceIfOptional.
   */
  afAnnounceIfOptionalText: string;
  /**
   * Sätter attributet 'value'.
   * @en Set value attribute
   * @ignore
   */
  value: string;
  onValueChanged(value: any): void;
  /**
   * Sätter attributet 'value'.
   * @en Input value attribute
   */
  afValue: string;
  onAfValueChanged(value: any): void;
  /**
   * Sätter valideringsstatus. Kan vara 'success', 'error', 'warning', 'neutral' eller ingenting.
   * @en Set validation status
   */
  afValidation: FormTextareaValidation;
  /**
   * Sätter valideringsmeddelandet
   * @en Set input validation message
   */
  afValidationText: string;
  /**
   * Sätter attributet 'role'.
   * @en Set role attribute
   */
  afRole: string;
  /**
   * Sätter attributet 'aria-activedescendant'.
   * @en Set aria-activedescendant attribute
   */
  afAriaActivedescendant: string;
  /**
   * Sätter attributet 'aria-labelledby'.
   * @en Set aria-labelledby attribute
   */
  afAriaLabelledby: string;
  /**
   * Sätter attributet 'aria-describedby'.
   * @en Set aria-describedby attribute
   */
  afAriaDescribedby: string;
  /**
   * Sätter attributet 'autofocus'.
   * @en Input autofocus attribute
   */
  afAutofocus: boolean;
  /**
   * Textareatelementets 'onchange'-event.
   * @en The textarea element's 'onchange' event.
   */
  afOnChange: EventEmitter;
  /**
   * Textareatelementets 'onblur'-event.
   * @en The textarea element's 'onblur' event.
   */
  afOnBlur: EventEmitter;
  /**
   * Textareatelementets 'onkeyup'-event.
   * @en The textarea element's 'onkeyup' event.
   */
  afOnKeyup: EventEmitter;
  /**
   * Textareatelementets 'onfocus'-event.
   * @en The textarea element's 'onfocus' event.
   */
  afOnFocus: EventEmitter;
  /**
   * Textareatelementets 'onfocusout'-event.
   * @en The textarea element's 'onfocusout' event.
   */
  afOnFocusout: EventEmitter;
  /**
   * Textareatelementets 'oninput'-event.
   * @en The textarea element's 'oninput' event.
   */
  afOnInput: EventEmitter;
  /**
   * Sker vid textareaelementets första 'oninput'
   * @en First time the textarea element receives an input
   */
  afOnDirty: EventEmitter;
  /**
   * Sker vid textareaelementets första 'onblur'
   * @en First time the textelement is blurred
   */
  afOnTouched: EventEmitter;
  afValidationTextWatch(): void;
  /**
   * Hämtar en referens till textareaelementet. Bra för att t.ex. sätta fokus programmatiskt.
   * @en Returns a reference to the textarea element. Handy for setting focus programmatically.
   */
  afMGetFormControlElement(): Promise<HTMLTextAreaElement>;
  componentWillLoad(): void;
  setActiveValidationMessage(): void;
  get cssModifiers(): {
    'digi-form-textarea--small': boolean;
    'digi-form-textarea--medium': boolean;
    'digi-form-textarea--large': boolean;
    'digi-form-textarea--auto': boolean;
    'digi-form-textarea--neutral': boolean;
    'digi-form-textarea--success': boolean;
    'digi-form-textarea--error': boolean;
    'digi-form-textarea--warning': boolean;
    'digi-form-textarea--empty': boolean;
  };
  blurHandler(e: any): void;
  changeHandler(e: any): void;
  focusHandler(e: any): void;
  focusoutHandler(e: any): void;
  keyupHandler(e: any): void;
  inputHandler(e: any): void;
  render(): any;
}
