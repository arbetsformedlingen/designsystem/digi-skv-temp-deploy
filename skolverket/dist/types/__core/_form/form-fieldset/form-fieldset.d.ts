/**
 * @slot default - Ska vara formulärselement. Till exempel digi-form-input.
 * @swedishName Fältgruppering
 */
export declare class FormFieldset {
  /**
   * Legend-elementets text.
   * @en The legend text.
   */
  afLegend: string;
  /**
   * Fieldset-elementets namn attribut.
   * @en The fieldset name attribute.
   */
  afName: string;
  /**
   * Fieldset-elementets form attribut.
   * @en The fieldset form attribute.
   */
  afForm: string;
  /**
   * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Input id attribute. Defaults to random string.
   */
  afId: string;
  render(): any;
}
