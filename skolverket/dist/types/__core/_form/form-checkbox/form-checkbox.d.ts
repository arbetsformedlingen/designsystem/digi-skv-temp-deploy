import { EventEmitter } from '../../../stencil-public-runtime';
import { HTMLStencilElement } from '../../../stencil-public-runtime';
import { FormCheckboxVariation } from './form-checkbox-variation.enum';
import { FormCheckboxValidation } from './form-checkbox-validation.enum';
import { FormCheckboxLayout } from './form-checkbox-layout.enum';
/**
 * @enums FormCheckboxValidation - form-checkbox-validation.enum.ts
 * @enums FormCheckboxVariation - form-checkbox-variation.enum.ts
 * @swedishName Kryssruta
 */
export declare class FormCheckbox {
  hostElement: HTMLStencilElement;
  private _input?;
  dirty: boolean;
  touched: boolean;
  /**
   * Texten till labelelementet
   * @en The label text
   */
  afLabel: string;
  /**
   * Sätter variant. Kan vara 'primary' eller 'secondary'
   * @en Set checkbox variation.
   */
  afVariation: FormCheckboxVariation;
  /**
   * Sätter layouten.
   * @en Set checkbox layout.
   */
  afLayout: FormCheckboxLayout;
  /**
   * Sätter property 'indeterminate' på inputelementet.
   * @en Set indeterminate property.
   */
  afIndeterminate: boolean;
  /**
   * Sätter attributet 'name'.
   * @en Input name attribute
   */
  afName: string;
  /**
   * Sätter attributet 'checked'.
   * @en Input checked attribute
   * @ignore
   */
  checked: boolean;
  onCheckedChanged(checked: any): void;
  /**
   * Sätter attributet 'checked'.
   * @en Input checked attribute
   */
  afChecked: boolean;
  onAfCheckedChanged(checked: any): void;
  /**
   * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Input id attribute. Defaults to random string.
   */
  afId: string;
  /**
   * Sätter attributet 'required'
   * @en Input required attribute
   */
  afRequired: boolean;
  /**
   * Sätter attributet 'value'
   * @en Input value attribute
   * @ignore
   */
  value: string;
  onValueChanged(value: any): void;
  /**
   * Sätter attributet 'value'.
   * @en Input value attribute
   */
  afValue: string;
  onAfValueChanged(value: any): void;
  /**
   * Sätter valideringsstatus. Kan vara 'error' eller ingenting.
   * @en Set input validation
   */
  afValidation: FormCheckboxValidation;
  /**
   * Sätter attributet 'aria-labelledby'
   * @en Input aria-labelledby attribute
   */
  afAriaLabelledby: string;
  /**
   * Sätter attributet 'aria-label'
   * @en Input aria-label attribute
   */
  afAriaLabel: string;
  /**
   * Ytterligare beskrivande text
   * @en Additional description text
   */
  afDescription: string;
  /**
   * Sätter attributet 'aria-describedby'
   * @en Input aria-describedby attribute
   */
  afAriaDescribedby: string;
  /**
   * Sätter attributet 'autofocus'.
   * @en Input autofocus attribute
   */
  afAutofocus: boolean;
  /**
   * Inputelementets 'onchange'-event.
   * @en The input element's 'onchange' event.
   */
  afOnChange: EventEmitter;
  /**
   * Inputelementets 'onblur'-event.
   * @en The input element's 'onblur' event.
   */
  afOnBlur: EventEmitter;
  /**
   * Inputelementets 'onfocus'-event.
   * @en The input element's 'onfocus' event.
   */
  afOnFocus: EventEmitter;
  /**
   * Inputelementets 'onfocusout'-event.
   * @en The input element's 'onfocusout' event.
   */
  afOnFocusout: EventEmitter;
  /**
   * Inputelementets 'oninput'-event.
   * @en The input element's 'oninput' event.
   */
  afOnInput: EventEmitter;
  /**
   * Sker vid inputfältets första 'oninput'
   * @en First time the input element receives an input
   */
  afOnDirty: EventEmitter;
  /**
   * Sker vid inputfältets första 'onblur'
   * @en First time the input element is blurred
   */
  afOnTouched: EventEmitter;
  /**
   * Hämta en referens till inputelementet. Bra för att t.ex. sätta fokus programmatiskt.
   * @en Returns a reference to the input element. Handy for setting focus programmatically.
   */
  afMGetFormControlElement(): Promise<HTMLInputElement>;
  componentWillLoad(): void;
  get cssModifiers(): {
    [x: string]: boolean;
    'digi-form-checkbox--error': boolean;
    'digi-form-checkbox--primary': boolean;
    'digi-form-checkbox--secondary': boolean;
    'digi-form-checkbox--indeterminate': boolean;
  };
  blurHandler(e: any): void;
  changeHandler(e: any): void;
  focusHandler(e: any): void;
  focusoutHandler(e: any): void;
  inputHandler(e: any): void;
  render(): any;
}
