export declare enum FormCheckboxLayout {
  INLINE = "inline",
  BLOCK = "block"
}
