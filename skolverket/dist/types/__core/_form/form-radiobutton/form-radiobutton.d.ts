import { EventEmitter } from '../../../stencil-public-runtime';
import { HTMLStencilElement } from '../../../stencil-public-runtime';
import { FormRadiobuttonVariation } from './form-radiobutton-variation.enum';
import { FormRadiobuttonValidation } from './form-radiobutton-validation.enum';
import { FormRadiobuttonLayout } from './form-radiobutton-layout.enum';
/**
 * @enums FormRadiobuttonValidation - form-radiobutton-validation.enum.ts
 * @enums FormRadiobuttonVariation - form-radiobutton-variation.enum.ts
 * @swedishName Radioknapp
 */
export declare class FormRadiobutton {
  hostElement: HTMLStencilElement;
  private _input?;
  dirty: boolean;
  touched: boolean;
  /**
   * Texten till labelelementet
   * @en The label text
   */
  afLabel: string;
  /**
   * Sätter variant. Kan vara 'primary' eller 'secondary'
   * @en Set radiobutton variation.
   */
  afVariation: FormRadiobuttonVariation;
  /**
   * Sätter layouten.
   * @en Set radiobutton layout.
   */
  afLayout: FormRadiobuttonLayout;
  /**
   * Sätter attributet 'name'.
   * @en Input name attribute
   */
  afName: string;
  /**
   * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Input id attribute. Defaults to random string.
   */
  afId: string;
  /**
   * Sätter attributet 'required'
   * @en Input required attribute
   */
  afRequired: boolean;
  /**
   * Sätter attributet 'value'
   * @en Input value attribute
   * @ignore
   */
  value: string;
  onValueChanged(value: any): void;
  /**
   * Sätter initiala attributet 'value'
   * @en Input initial value attribute
   */
  afValue: string;
  onAfValueChanged(value: any): void;
  /**
   * Sätter om radioknappen ska vara ikryssad.
   * @en Sets if radiobutton should be checked.
   */
  checked: boolean;
  onCheckedChanged(checked: any): void;
  /**
   * Sätter om radioknappen ska vara ikryssad.
   * @en Sets if radiobutton should be checked.
   */
  afChecked: boolean;
  onAfCheckedChanged(checked: any): void;
  /**
   * Sätter valideringsstatus. Kan vara 'error' eller ingenting.
   * @en Set input validation
   */
  afValidation: FormRadiobuttonValidation;
  /**
   * Sätter attributet 'aria-labelledby'
   * @en Input aria-labelledby attribute
   */
  afAriaLabelledby: string;
  /**
   * Sätter attributet 'aria-describedby'
   * @en Input aria-describedby attribute
   */
  afAriaDescribedby: string;
  /**
   * Sätter attributet 'autofocus'.
   * @en Input autofocus attribute
   */
  afAutofocus: boolean;
  /**
   * Inputelementets 'onchange'-event.
   * @en The input element's 'onchange' event.
   */
  afOnChange: EventEmitter;
  /**
   * Inputelementets 'onblur'-event.
   * @en The input element's 'onblur' event.
   */
  afOnBlur: EventEmitter;
  /**
   * Inputelementets 'onfocus'-event.
   * @en The input element's 'onfocus' event.
   */
  afOnFocus: EventEmitter;
  /**
   * Inputelementets 'onfocusout'-event.
   * @en The input element's 'onfocusout' event.
   */
  afOnFocusout: EventEmitter;
  /**
   * Inputelementets 'oninput'-event.
   * @en The input element's 'oninput' event.
   */
  afOnInput: EventEmitter;
  /**
   * Sker vid inputfältets första 'oninput'
   * @en First time the input element receives an input
   */
  afOnDirty: EventEmitter;
  /**
   * Sker vid inputfältets första 'onblur'
   * @en First time the input element is blurred
   */
  afOnTouched: EventEmitter;
  /**
   * Hämta en referens till inputelementet. Bra för att t.ex. sätta fokus programmatiskt.
   * @en Returns a reference to the input element. Handy for setting focus programmatically.
   */
  afMGetFormControlElement(): Promise<HTMLInputElement>;
  componentDidLoad(): void;
  get cssModifiers(): {
    [x: string]: boolean;
    'digi-form-radiobutton--error': boolean;
    'digi-form-radiobutton--primary': boolean;
    'digi-form-radiobutton--secondary': boolean;
  };
  blurHandler(e: any): void;
  changeHandler(e: any): void;
  focusHandler(e: any): void;
  focusoutHandler(e: any): void;
  inputHandler(e: any): void;
  render(): any;
}
