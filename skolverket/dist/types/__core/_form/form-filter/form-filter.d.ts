import { EventEmitter } from '../../../stencil-public-runtime';
import { HTMLStencilElement } from '../../../stencil-public-runtime';
/**
 * @slot default - Ska vara formulärselement. Till exempel digi-form-checkbox.
 * @swedishName Multifilter
 */
export declare class formFilter {
  hostElement: HTMLStencilElement;
  isActive: boolean;
  activeListItemIndex: number;
  hasActiveItems: boolean;
  focusInList: boolean;
  private _toggleButton;
  private _submitButton;
  private _observer;
  private _updateActiveItems;
  listItems: any[];
  activeList: any[];
  /**
   * Prefixet för alla komponentens olika interna 'id'-attribut på dropdowns, filterlistor etc. Ex. 'my-cool-id' genererar 'my-cool-id-filter-list', 'my-cool-id-dropdown-menu' och så vidare. Ges inget värde så genereras ett slumpmässigt.
   * @en Prefix for all id attributes inside the component. Example: 'my-cool-id' generates 'my-cool-id-filter-list', 'my-cool-id-dropdown-menu' and so on. Defaults to random string.
   */
  afId: string;
  /**
   * Texten i filterknappen
   * @en Set the filter button text
   */
  afFilterButtonText: string;
  /**
   * Texten i submitknappen
   * @en Set the submit button text
   */
  afSubmitButtonText: string;
  /**
   * Skärmläsartext för submitknappen
   * @en Set the submit button aria-label text
   */
  afSubmitButtonTextAriaLabel: string;
  /**
   * Texten i rensaknappen
   * @en Set the reset button text
   */
  afResetButtonText: string;
  /**
  * Skärmläsartext för resetknappen
  * @en Set the reset button aria-label text
  */
  afResetButtonTextAriaLabel: string;
  /**
  * Döljer rensaknappen när värdet sätts till true
  * @en Hides the reset button when set to true
  */
  afHideResetButton: boolean;
  /**
   * Justera dropdown från höger till vänster
   * @en Align dropdown from right to left
   */
  afAlignRight: boolean;
  /**
   * Namnet på filterlistans valda filter vid submit. Används även av legendelementet inne i filterlistan.
   * @en Binding name of filterlist selected filters on submit. Also used by the legend element.
   */
  afName: string;
  /**
   * Sätter attributet 'aria-label' på filterknappen
   * @en Input aria-label attribute on filter button
   */
  afFilterButtonAriaLabel: string;
  /**
   * Sätter attributet 'aria-describedby' på filterknappen
   * @en Input aria-describedby attribute on filter button
   */
  afFilterButtonAriaDescribedby: string;
  /**
   * Sätter attributet 'autofocus'.
   * @en Input autofocus attribute
   */
  afAutofocus: boolean;
  afOnFocusout: EventEmitter;
  /**
   * Vid uppdatering av filtret
   * @en At filter submit
   */
  afOnSubmitFilters: EventEmitter;
  /**
   * När filtret stängs utan att valda alternativ bekräftats
   * @en When the filter is closed without confirming the selected options
   */
  afOnFilterClosed: EventEmitter;
  /**
   * Vid reset av filtret
   * @en At filter reset
   */
  afOnResetFilters: EventEmitter;
  /**
   * När ett filter ändras
   * @en When a filter is changed
   */
  afOnChange: EventEmitter;
  clickHandler(e: MouseEvent): void;
  focusoutHandler(e: FocusEvent): void;
  changeHandler(e: any): void;
  debounce(func: any, timeout?: number): (...args: any[]) => void;
  setSelectedItems(item: any): void;
  setInactive(focusToggleButton?: boolean): void;
  setActive(): void;
  toggleDropdown(): void;
  inputItem(identifier: any, index: any): HTMLInputElement;
  getFilterData(): {
    filter: string;
    items: {
      id: any;
      text: any;
    }[];
  };
  emitFilterClosedEvent(): void;
  submitFilters(setInactive?: boolean): void;
  resetFilters(): void;
  setupListElements(collection: HTMLCollection): void;
  componentWillLoad(): void;
  setupActiveItems(): void;
  componentDidLoad(): void;
  componentDidUpdate(): void;
  focusActiveItem(): void;
  incrementActiveItem(): void;
  setTabIndex(): void;
  updateTabIndex(element: HTMLElement): void;
  decrementActiveItem(): void;
  downHandler(): void;
  upHandler(): void;
  homeHandler(): void;
  endHandler(): void;
  enterHandler(e: any): void;
  escHandler(): void;
  tabHandler(): void;
  shiftHandler(): void;
  get cssModifiers(): {
    'digi-form-filter--open': boolean;
  };
  get cssModifiersToggleButtonIndicator(): {
    'digi-form-filter__toggle-button-indicator--active': boolean;
  };
  render(): any;
}
