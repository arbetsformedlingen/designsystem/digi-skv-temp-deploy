import { EventEmitter } from '../../../stencil-public-runtime';
import { HTMLStencilElement } from '../../../stencil-public-runtime';
import { FormInputButtonVariation } from './form-input-button-variation.enum';
import { FormInputType } from './form-input-type.enum';
import { FormInputValidation } from './form-input-validation.enum';
import { FormInputVariation } from './form-input-variation.enum';
/**
 * @enums FormInputType - form-input-type.enum.ts
 * @enums FormInputValidation - form-input-validation.enum.ts
 * @enums FormInputVariation - form-input-variation.enum.ts
 * @swedishName Inmatningsfält
 */
export declare class FormInput {
  hostElement: HTMLStencilElement;
  private _input?;
  hasActiveValidationMessage: boolean;
  hasButton: boolean;
  dirty: boolean;
  touched: boolean;
  /**
   * Texten till labelelementet
   * @en The label text
   */
  afLabel: string;
  /**
   * Valfri beskrivande text
   * @en A description text
   */
  afLabelDescription: string;
  /**
   * Sätter attributet 'type'.
   * @en Input type attribute
   */
  afType: FormInputType;
  /**
   * Layout för hur inmatningsfältet ska visas då det finns en knapp
   * @en Input layout when there is a button
   */
  afButtonVariation: FormInputButtonVariation;
  /**
   * Sätter attributet 'autofocus'.
   * @en Input autofocus attribute
   */
  afAutofocus: boolean;
  /**
   * Sätter variant. Kan vara 'small', 'medium' eller 'large'
   * @en Set input variation.
   */
  afVariation: FormInputVariation;
  /**
   * Sätter attributet 'name'.
   * @en Input name attribute
   */
  afName: string;
  /**
   * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Input id attribute. Defaults to random string.
   */
  afId: string;
  /**
   * Sätter attributet 'maxlength'.
   * @en Input maxlength attribute
   */
  afMaxlength: number;
  /**
   * Sätter attributet 'minlength'.
   * @en Input minlength attribute
   */
  afMinlength: number;
  /**
   * Sätter attributet 'required'.
   * @en Input required attribute
   */
  afRequired: boolean;
  /**
   * Sätter text för afRequired.
   * @en Set text for afRequired.
   */
  afRequiredText: string;
  /**
   * Sätt denna till true om formuläret innehåller fler obligatoriska fält än valfria.
   * @en Set this to true if the form contains more required fields than optional fields.
   */
  afAnnounceIfOptional: boolean;
  /**
   * Sätter text för afAnnounceIfOptional.
   * @en Set text for afAnnounceIfOptional.
   */
  afAnnounceIfOptionalText: string;
  /**
   * Sätter attributet 'value'.
   * @en Input value attribute
   * @ignore
   */
  value: string | number;
  onValueChanged(value: any): void;
  /**
   * Sätter attributet 'value'.
   * @en Input value attribute
   */
  afValue: string | number;
  onAfValueChanged(value: any): void;
  /**
   * Sätter valideringsstatus. Kan vara 'success', 'error', 'warning', 'neutral' eller ingenting.
   * @en Set input validation status
   */
  afValidation: FormInputValidation;
  /**
   * Sätter valideringsmeddelandet
   * @en Set input validation message
   */
  afValidationText: string;
  /**
   * Sätter attributet 'role'.
   * @en Input role attribute
   */
  afRole: string;
  /**
   * Sätter attributet 'autocomplete'.
   * @en Input autocomplete attribute
   */
  afAutocomplete: string;
  /**
   * Sätter attributet 'min'.
   * @en Input min attribute
   */
  afMin: string;
  /**
   * Sätter attributet 'max'.
   * @en Input max attribute
   */
  afMax: string;
  /**
   * Sätter attributet 'list'.
   * @en Input list attribute
   */
  afList: string;
  /**
   * Sätter attributet 'step'.
   * @en Input step attribute
   */
  afStep: string;
  /**
   * Sätter attributet 'dirname'.
   * @en Input dirname attribute
   */
  afDirname: string;
  /**
   * Sätter attributet 'aria-autocomplete'.
   * @en Input aria-autocomplete attribute
   */
  afAriaAutocomplete: string;
  /**
   * Sätter attributet 'aria-activedescendant'.
   * @en Input aria-activedescendant attribute
   */
  afAriaActivedescendant: string;
  /**
   * Sätter attributet 'aria-labelledby'.
   * @en Input aria-labelledby attribute
   */
  afAriaLabelledby: string;
  /**
   * Sätter attributet 'aria-describedby'.
   * @en Input aria-describedby attribute
   */
  afAriaDescribedby: string;
  /**
   * Inputelementets 'onchange'-event.
   * @en The input element's 'onchange' event.
   */
  afOnChange: EventEmitter;
  /**
   * Inputelementets 'onblur'-event.
   * @en The input element's 'onblur' event.
   */
  afOnBlur: EventEmitter;
  /**
   * Inputelementets 'onkeyup'-event.
   * @en The input element's 'onkeyup' event.
   */
  afOnKeyup: EventEmitter;
  /**
   * Inputelementets 'onfocus'-event.
   * @en The input element's 'onfocus' event.
   */
  afOnFocus: EventEmitter;
  /**
   * Inputelementets 'onfocusout'-event.
   * @en The input element's 'onfocusout' event.
   */
  afOnFocusout: EventEmitter;
  /**
   * Inputelementets 'oninput'-event.
   * @en The input element's 'oninput' event.
   */
  afOnInput: EventEmitter;
  /**
   * Sker vid inputfältets första 'oninput'
   * @en First time the input element receives an input
   */
  afOnDirty: EventEmitter;
  /**
   * Sker vid inputfältets första 'onblur'
   * @en First time the input element is blurred
   */
  afOnTouched: EventEmitter;
  afValidationTextWatch(): void;
  /**
   * Hämtar en referens till inputelementet. Bra för att t.ex. sätta fokus programmatiskt.
   * @en Returns a reference to the input element. Handy for setting focus programmatically.
   */
  afMGetFormControlElement(): Promise<HTMLInputElement>;
  componentWillLoad(): void;
  componentWillUpdate(): void;
  setActiveValidationMessage(): void;
  get cssModifiers(): {
    [x: string]: boolean;
    'digi-form-input--small': boolean;
    'digi-form-input--medium': boolean;
    'digi-form-input--large': boolean;
    'digi-form-input--neutral': boolean;
    'digi-form-input--success': boolean;
    'digi-form-input--error': boolean;
    'digi-form-input--warning': boolean;
    'digi-form-input--empty': boolean;
  };
  blurHandler(e: any): void;
  changeHandler(e: any): void;
  focusHandler(e: any): void;
  focusoutHandler(e: any): void;
  keyupHandler(e: any): void;
  inputHandler(e: any): void;
  setHasButton(): void;
  render(): any;
}
