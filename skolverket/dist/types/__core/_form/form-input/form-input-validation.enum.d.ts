export declare enum FormInputValidation {
  SUCCESS = "success",
  ERROR = "error",
  WARNING = "warning",
  NEUTRAL = "neutral"
}
