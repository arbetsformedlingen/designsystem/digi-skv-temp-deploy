export declare enum FormInputVariation {
  SMALL = "small",
  MEDIUM = "medium",
  LARGE = "large"
}
