import { HTMLStencilElement } from '../../../stencil-public-runtime';
import { LoaderSpinnerSize } from './loader-spinner-size.enum';
/**
 * @enums LoaderSpinnerSize - loader-spinner-size.enum.ts
 *
 * @swedishName Spinner
 */
export declare class LoaderSpinner {
  hostElement: HTMLStencilElement;
  /**
   * Sätter spinnerns storlek. 'medium' är förvalt.
   * @en Set spinner icon size. Defaults to medium size.
   */
  afSize: LoaderSpinnerSize;
  /**
   * Sätter spinnerns text.
   * @en Set spinners text.
   */
  afText: string;
  componentWillLoad(): void;
  componentDidLoad(): void;
  componentWillUpdate(): void;
  get cssModifiers(): {
    'digi-loader-spinner--small': boolean;
    'digi-loader-spinner--medium': boolean;
    'digi-loader-spinner--large': boolean;
  };
  render(): any;
}
