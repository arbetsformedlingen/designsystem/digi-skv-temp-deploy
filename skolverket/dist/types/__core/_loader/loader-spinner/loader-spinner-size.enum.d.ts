export declare enum LoaderSpinnerSize {
  SMALL = "small",
  MEDIUM = "medium",
  LARGE = "large"
}
