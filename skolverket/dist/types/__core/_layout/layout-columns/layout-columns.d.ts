import { LayoutColumnsElement } from './layout-columns-element.enum';
import { LayoutColumnsVariation } from './layout-columns-variation.enum';
/**
 * @slot default - Kan innehålla vad som helst
 *
 * @enums LayoutColumnsVariation - layout-columns-variation.enum.ts
 * @enums LayoutColumnsElement - layout-columns-element.enum.ts
 * @swedishName Kolumner
 */
export declare class LayoutColumns {
  /**
   * Sätter elementtypen på det omslutande elementet. Kan vara 'div', 'ul' eller 'ol'.
   * @en Set wrapper element type. can be 'div', 'ul' or 'ol'.
   */
  afElement: LayoutColumnsElement;
  /**
   * Sätter förvalda kolumnvarianter. Kan vara 'two' eller 'three'.
   * @en Set preset column variations. Can be 'two' or 'three'.
   */
  afVariation: LayoutColumnsVariation;
  get cssModifiers(): {
    'digi-layout-columns--two': boolean;
    'digi-layout-columns--three': boolean;
  };
  render(): any;
}
