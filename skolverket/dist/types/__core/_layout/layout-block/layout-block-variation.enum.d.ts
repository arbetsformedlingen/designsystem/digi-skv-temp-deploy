export declare enum LayoutBlockVariation {
  TRANSPARENT = "transparent",
  PRIMARY = "primary",
  SECONDARY = "secondary",
  TERTIARY = "tertiary",
  SYMBOL = "symbol",
  PROFILE = "profile"
}
