export declare enum LayoutBlockContainer {
  STATIC = "static",
  FLUID = "fluid",
  NONE = "none"
}
