import { LayoutBlockVariation } from './layout-block-variation.enum';
import { LayoutBlockContainer } from './layout-block-container.enum';
/**
 * @slot default - Kan innehålla vad som helst
 *
 * @enums LayoutBlockContainer - layout-block-container.enum.ts
 * @enums LayoutBlockVariation - layout-block-variation.enum.ts
 *@swedishName Block
 */
export declare class LayoutBlock {
  _container: LayoutBlockContainer;
  /**
   * Sätter variant. Kontrollerar bakgrundsfärgen.
   * @en Set variation. Controls background color.
   */
  afVariation: LayoutBlockVariation;
  /**
   * Sätter den inre containervarianten, eller tar bort den helt. Kan vara 'static', 'fluid' eller 'none'.
   * @en Set inner container variation, or remove it completely. Can be 'static', 'fluid' or 'none'.
   */
  afContainer: LayoutBlockContainer;
  containerChangeHandler(): void;
  componentWillLoad(): void;
  /**
   * Lägger till vertikal padding inuti containern
   * @en Adds vertical padding inside the container
   */
  afVerticalPadding: boolean;
  /**
   * Lägger till marginal i toppen på containern
   * @en Adds margin on top of the container
   */
  afMarginTop: boolean;
  /**
   * Lägger till marginal i botten på containern
   * @en Adds margin at bottom of the container
   */
  afMarginBottom: boolean;
  get cssModifiers(): {
    'digi-layout-block--transparent': boolean;
    'digi-layout-block--primary': boolean;
    'digi-layout-block--secondary': boolean;
    'digi-layout-block--tertiary': boolean;
    'digi-layout-block--symbol': boolean;
    'digi-layout-block--profile': boolean;
  };
  render(): any;
}
