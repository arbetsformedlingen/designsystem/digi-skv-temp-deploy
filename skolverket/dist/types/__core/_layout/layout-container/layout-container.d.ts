import { LayoutContainerVariation } from './layout-container-variation.enum';
/**
 * @slot default - kan innehålla vad som helst
 *
 * @enums LayoutContainerVariation - layout-container-variation.enum.ts
 * @swedishName Container
 */
export declare class LayoutContainer {
  _variation: LayoutContainerVariation;
  /**
   * Sätter containervarianten. Kan vara 'static' eller 'fluid'.
   * @en Set container variation. Can be 'static' or 'fluid'.
   */
  afVariation: LayoutContainerVariation;
  variationChangeHandler(): void;
  componentWillLoad(): void;
  /**
   * Tar bort marginalen från sidorna av containern
   * @en Remove gutters from container sides
   */
  afNoGutter: boolean;
  /**
   * Lägger till vertikal padding inuti containern
   * @en Adds vertical padding inside the container
   */
  afVerticalPadding: boolean;
  /**
   * Lägger till marginal i toppen på containern
   * @en Adds margin on top of the container
   */
  afMarginTop: boolean;
  /**
   * Lägger till marginal i botten på containern
   * @en Adds margin at bottom of the container
   */
  afMarginBottom: boolean;
  get cssModifiers(): {
    'digi-layout-container'?: undefined;
    'digi-layout-container--static'?: undefined;
    'digi-layout-container--fluid'?: undefined;
    'digi-layout-container--no-gutter'?: undefined;
    'digi-layout-container--vertical-padding'?: undefined;
    'digi-layout-container--margin-top'?: undefined;
    'digi-layout-container--margin-bottom'?: undefined;
  } | {
    'digi-layout-container': boolean;
    'digi-layout-container--static': boolean;
    'digi-layout-container--fluid': boolean;
    'digi-layout-container--no-gutter': boolean;
    'digi-layout-container--vertical-padding': boolean;
    'digi-layout-container--margin-top': boolean;
    'digi-layout-container--margin-bottom': boolean;
  };
  render(): any;
}
