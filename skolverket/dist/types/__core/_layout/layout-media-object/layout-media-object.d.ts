import { LayoutMediaObjectAlignment } from './layout-media-object-alignment.enum';
/**
 * @slot media - Bör innehålla någon sorts mediakompinent (t.ex. en bild eller en video).
 * @slot default - kan innehålla vad som helst. Vanligen är det textinnehåll.
 *
 * @enums LayoutMediaObjectAlignment - layout-media-object-alignment.enum.ts
 * @swedishName Medieobjekt
 */
export declare class LayoutMediaObject {
  /**
   * Sätter justeringen av innehållet. Kan vara 'center', 'start', 'end' eller 'stretch'.
   * @en Set alignment of the content. Can be 'center', 'start', 'end' or 'stretch'.
   */
  afAlignment: `${LayoutMediaObjectAlignment}`;
  get cssModifiers(): {
    [x: string]: boolean;
  };
  render(): any;
}
