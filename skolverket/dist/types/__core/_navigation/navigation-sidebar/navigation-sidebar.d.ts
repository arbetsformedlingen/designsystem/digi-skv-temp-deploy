import { EventEmitter } from '../../../stencil-public-runtime';
import { HTMLStencilElement } from '../../../stencil-public-runtime';
import { NavigationSidebarPosition } from './navigation-sidebar-position.enum';
import { NavigationSidebarVariation } from './navigation-sidebar-variation.enum';
import { NavigationSidebarMobilePosition } from './navigation-sidebar-mobile-position.enum';
import { NavigationSidebarMobileVariation } from './navigation-sidebar-mobile-variation.enum';
import { NavigationSidebarHeadingLevel } from './navigation-sidebar-heading-level.enum';
import { NavigationSidebarCloseButtonPosition } from './navigation-sidebar-close-button-position.enum';
/**
 * @slot default - Ska innehålla navigationskomponent
 * @slot footer - Kan innehålla vad som helst
 *
 * @enums NavigationSidebarPosition - navigation-sidebar-position.enum.ts
 * @enums NavigationSidebarVariation - navigation-sidebar-variation.enum.ts
 * @enums NavigationSidebarMobileVariation - navigation-sidebar-mobile-variation.enum.ts
 * @enums NavigationSidebarHeadingLevel - navigation-sidebar-heading-level.enum.ts
 * @enums NavigationSidebarCloseButtonPosition - navigation-sidebar-close-button-position.enum.ts
 * @swedishName Sidofält
 */
export declare class NavigationSidebar {
  hostElement: HTMLStencilElement;
  isMobile: boolean;
  showHeader: boolean;
  private _hasFooter;
  private _wrapper;
  private _closeFocusableElement;
  private _heading;
  private _focusableElementsSelectors;
  private focusableElements;
  private firstFocusableEl;
  private lastFocusableEl;
  /**
   * Positionerar menyn
   * @en Sets sidebar position
   */
  afPosition: NavigationSidebarPosition;
  /**
   * Sätt variant
   * @en Sets sidebar variation
   */
  afVariation: NavigationSidebarVariation;
  /**
   * Positionerar menyn i mobilen. Sätter du inget blir det samma som för större skärmar.
   * @en Sets sidebar position in mobile. Defaults to the same as for desktop.
   */
  afMobilePosition: NavigationSidebarMobilePosition;
  /**
   * Sätt variant för mobilen.
   * @en Sets sidebar variation for mobile.
   */
  afMobileVariation: NavigationSidebarMobileVariation;
  /**
   * Rubrikens text
   * @en The heading text
   */
  afHeading: string;
  /**
   * Sätt rubrikens vikt. 'h2' är förvalt.
   * @en Set heading level. Default is 'h2'
   */
  afHeadingLevel: NavigationSidebarHeadingLevel;
  /**
   * Justerar stäng-knappens position
   * @en Positions the close button, defaults to start
   */
  afCloseButtonPosition: NavigationSidebarCloseButtonPosition;
  /**
   * Låser rubrikdelen mot toppen
   * @en Sticks the header area to the top
   */
  afStickyHeader: boolean;
  /**
   * Stäng-knappens text
   * @en The close buttons text
   */
  afCloseButtonText: string;
  /**
   * Sätt attributet 'aria-label' på stäng-knappen
   * @en Set button `aria-label` attribute to the close button
   */
  afCloseButtonAriaLabel: string;
  /**
   * Dölj rubrikdelen i sidofältet
   * @en Hide header area in sidebar
   */
  afHideHeader: boolean;
  /**
   * Element som ska få fokus när menyn öppnas. Sätts ingen
   * så får rubriken eller stäng-knappen fokus
   * @en Element that is focused when menu is opened,
   * defaults to title or close button
   */
  afFocusableElement: string;
  /**
   * Element som ska få fokus när menyn stängs.
   * Sätts ingen så får knappen som öppnade menyn fokus
   * @en Element that is focused when menu is closed,
   * defaults to toggle button that opened the sidebar
   */
  afCloseFocusableElement: string;
  /**
   * Ändrar aktiv status
   * @en Toggles sidebar. Pass in true for default active
   */
  afActive: boolean;
  /**
   * Lägger en skugga bakom menyn som täcker sidinnehållet när menyn är aktiv
   * @en Applies a unscrollable backdrop over the page content when the sidebar is active
   */
  afBackdrop: boolean;
  /**
   * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Input id attribute. Defaults to random string.
   */
  afId: string;
  /**
   * Stängknappens 'onclick'-event
   * @en Close button's 'onclick' event
   */
  afOnClose: EventEmitter;
  /**
   * Stängning av sidofält med esc-tangenten
   * @en At close/open of sidebar using esc-key
   */
  afOnEsc: EventEmitter;
  /**
   * Stängning av sidofält med klick på skuggan bakom menyn
   * @en At close of sidebar when clicking on backdrop
   */
  afOnBackdropClick: EventEmitter;
  /**
   * Vid öppning/stängning av sidofält
   * @en At close/open of sidebar
   */
  afOnToggle: EventEmitter;
  toggleHandler(e: any): void;
  breakpointHandler(e: any): void;
  toggleTransitionEndHandler(e: any): void;
  clickHandler(e: any): void;
  setMobileView(isMobile: boolean): void;
  activeChange(active: boolean): void;
  headerChange(hide: boolean): void;
  setShowHeader(hide: boolean): void;
  setHasFooter(): void;
  setFocus(): void;
  pushPageContentToggler(): void;
  pageScrollToggler(): void;
  enablePageScroll(): void;
  disablePageScroll(): void;
  closeHandler(e: any): void;
  escHandler(e: any): void;
  tabHandler(e: any): void;
  backdropClickHandler(e: any): void;
  shiftHandler(e: any): void;
  componentWillLoad(): void;
  componentDidLoad(): void;
  componentWillUpdate(): void;
  get shouldUseFocusTrap(): boolean;
  getFocusableItems(): void;
  reducedMotion(): boolean;
  get cssModifiers(): {
    'digi-navigation-sidebar__wrapper--start': boolean;
    'digi-navigation-sidebar__wrapper--end': boolean;
    'digi-navigation-sidebar__wrapper--over': boolean;
    'digi-navigation-sidebar__wrapper--push': boolean;
    'digi-navigation-sidebar__wrapper--static': boolean;
    'digi-navigation-sidebar__wrapper--mobile--start': boolean;
    'digi-navigation-sidebar__wrapper--mobile--end': boolean;
    'digi-navigation-sidebar__wrapper--mobile--default': boolean;
    'digi-navigation-sidebar__wrapper--mobile--fullwidth': boolean;
    'digi-navigation-sidebar__wrapper--mobile--active': boolean;
    'digi-navigation-sidebar__wrapper--active': boolean;
    'digi-navigation-sidebar__wrapper--backdrop': boolean;
    'digi-navigation-sidebar__wrapper--sticky-header': boolean;
  };
  get cssModifiersBackdrop(): {
    'digi-navigation-sidebar__backdrop--active': boolean;
    'digi-navigation-sidebar__backdrop--active--mobile': boolean;
    'digi-navigation-sidebar__backdrop--hidden': boolean;
  };
  render(): any;
}
