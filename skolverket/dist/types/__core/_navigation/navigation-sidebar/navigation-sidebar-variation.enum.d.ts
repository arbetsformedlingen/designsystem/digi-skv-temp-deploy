export declare enum NavigationSidebarVariation {
  OVER = "over",
  PUSH = "push",
  STATIC = "static"
}
