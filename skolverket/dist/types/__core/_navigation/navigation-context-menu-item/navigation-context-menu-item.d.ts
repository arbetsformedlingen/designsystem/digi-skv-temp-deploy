import { HTMLStencilElement } from '../../../stencil-public-runtime';
import { NavigationContextMenuItemType } from './navigation-context-menu-item-type.enum';
/**
 * @enums NavigationContextMenuItemType - navigation-context-menu-item-type.enum.ts
 * @swedishName Rullgardinsmenyval
 */
export declare class NavigationContextMenuItem {
  hostElement: HTMLStencilElement;
  /**
   * Sätter texten i komponenten
   * @en Sets the item text
   */
  afText: string;
  /**
   * Sätter attributet 'href'.
   * @en Set href attribute
   */
  afHref: string;
  /**
   * Sätter typ. Kan vara 'link' eller 'button'.
   * @en Set type. Can be 'link' or 'button'
   */
  afType: NavigationContextMenuItemType;
  /**
   * Sätter attributet 'lang'.
   * @en Set lang attribute
   */
  afLang: string;
  /**
   * Sätter attributet 'dir'.
   * @en Set dir attribute
   */
  afDir: string;
  componentWillLoad(): void;
}
