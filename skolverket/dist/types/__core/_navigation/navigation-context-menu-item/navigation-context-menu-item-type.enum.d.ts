export declare enum NavigationContextMenuItemType {
  LINK = "link",
  BUTTON = "button"
}
