export interface INavigationContextMenuItem {
  index?: number;
  text: string;
  type?: string;
  value?: string;
  href?: string;
  lang: string;
  dir: string;
}
