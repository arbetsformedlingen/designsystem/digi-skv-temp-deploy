import { EventEmitter } from '../../../stencil-public-runtime';
import { HTMLStencilElement } from '../../../stencil-public-runtime';
import { INavigationContextMenuItem } from './navigation-context-menu.interface';
/**
 * @slot default - Ska innehålla en eller flera navigation-context-menu-item.
 * @swedishName Rullgardinsmeny
 */
export declare class NavigationContextMenu {
  hostElement: HTMLStencilElement;
  selectedListItemIndex: number;
  isActive: boolean;
  activeListItemIndex: number;
  /**
   * Sätter texten för toggleknappen.
   * @en Set text for toggle button
   */
  afText: string;
  /**
   * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Input id attribute. Defaults to random string.
   */
  afId: string;
  /**
   * Sätter det initialt valda värdet
   * @en Sets initial selected value
   */
  afStartSelected: number;
  /**
   * Sätt array istället för att använda children-slot.
   * @en Set array instead of using children slot.
   */
  afNavigationContextMenuItems: string | INavigationContextMenuItem[];
  _afNavigationContextMenuItems: string | INavigationContextMenuItem[];
  /**
   * Sätt namn på ikonen att ladda in. (<digi-icon-VÄRDE>)
   * @en Set name on icon (<digi-icon-Value)
   */
  afIcon: string;
  /**
   * När komponenten stängs
   * @en When component gets inactive
   */
  afOnInactive: EventEmitter;
  /**
   * När komponenten öppnas
   * @en When component gets active
   */
  afOnActive: EventEmitter;
  /**
   * När fokus sätts utanför komponenten
   * @en When focus is move outside of component
   */
  afOnBlur: EventEmitter;
  /**
   * Vid navigering till nytt listobjekt
   * @en When navigating to a new list item
   */
  afOnChange: EventEmitter;
  /**
   * Toggleknappens 'onclick'-event
   * @en The toggle button's 'onclick'-event
   */
  afOnToggle: EventEmitter;
  /**
   * 'onclick'-event på knappelementen i listan
   * @en List item buttons' 'onclick'-event
   */
  afOnSelect: EventEmitter;
  listItems: INavigationContextMenuItem[];
  digiIcon: string;
  private _toggleButton;
  private _observer;
  clickHandler(e: MouseEvent): void;
  focusoutHandler(e: FocusEvent): void;
  setComponentTag(): void;
  generateListItems(collection: HTMLCollection): void;
  componentWillLoad(): void;
  debounce(func: any, timeout?: number): (...args: any[]) => void;
  setInactive(focusTrigger?: boolean): void;
  setActiveListItemIndex(): void;
  setActive(): void;
  toggleMenu(e: any): void;
  focusActiveItem(): void;
  homeHandler(): void;
  endHandler(): void;
  decrementActiveItem(): void;
  incrementActiveItem(): void;
  upHandler(): void;
  downHandler(): void;
  itemClickHandler(e: any, select?: boolean): void;
  handleSelect(value: string): void;
  showCheckIcon(i: number, type: string): boolean;
  get cssModifiers(): {
    'digi-navigation-context-menu--active': boolean;
  };
  render(): any;
}
