import { EventEmitter } from '../../../stencil-public-runtime';
/**
 * @swedishName Sidofältsknapp
 */
export declare class NavigationSidebarButton {
  isActive: boolean;
  /**
   * Sätter texten för toggleknappen.
   * @en Set text for toggle button
   */
  afText: string;
  /**
   * Sätter attributet 'aria-labelledby'.
   * @en Input aria-labelledby attribute
   */
  afAriaLabel: string;
  /**
   * Sätter attributet 'id' på det omslutande elementet. Ges inget värde så genereras ett slumpmässigt.
   * @en Label id attribute. Defaults to random string.
   */
  afId: string;
  /**
   * Toggleknappens 'onclick'-event
   * @en The toggle button's 'onclick'-event
   */
  afOnToggle: EventEmitter;
  toggleHandler(): void;
  render(): any;
}
