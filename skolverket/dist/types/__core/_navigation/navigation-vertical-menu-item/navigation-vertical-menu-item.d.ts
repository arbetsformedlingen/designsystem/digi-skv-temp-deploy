import { EventEmitter } from '../../../stencil-public-runtime';
import { HTMLStencilElement } from '../../../stencil-public-runtime';
/**
 * @slot - Expects navigation-vertical-menu-item
 * @swedishName Vertikalt menyval
 */
export declare class NavigationVerticalMenuItem {
  hostElement: HTMLStencilElement;
  hasSubnav: boolean;
  hasIcon: boolean;
  isActive: boolean;
  activeSubnav: boolean;
  listItems: any[];
  private $parent;
  private $subnav;
  /**
   * Sätter texten i komponenten
   * @en Sets the item text
   */
  afText: string;
  /**
   * Sätter attributet 'href'.
   * @en Set href attribute
   */
  afHref: string;
  /**
   * Ändrar aktiv status på undermenyn. Sätt som 'true' för att ha öppen från början
   * @en Toggles subnavigation. Pass in true for default active
   */
  afActiveSubnav: boolean;
  /**
   * Ändrar aktiv status
   * @en Sets active item. Pass in true for default active
   */
  afActive: boolean;
  /**
   * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Set id attribute. Defaults to random string.
   */
  afId: string;
  /**
   * Kringgår länkens vanliga beteende.
   * Bör endast användas om det vanliga beteendet är problematiskt pga dynamisk routing eller liknande.
   * @en Override default anchor link behavior and sets focus programmatically. Should only be used if default anchor link behaviour is a problem with e.g. routing
   */
  afOverrideLink: boolean;
  /**
   *
   */
  afHasSubnav: boolean;
  /**
   * Länken och toggle-knappens 'onclick'-event
   * @en Link and toggle buttons 'onclick' event
   */
  afOnClick: EventEmitter;
  componentWillLoad(): void;
  componentDidLoad(): void;
  componentWillUpdate(): void;
  setHasSubnav(): void;
  setIsActive(): void;
  setSubnav(): void;
  setHasIcon(): void;
  clickHandler(e: MouseEvent): void;
  get toggleButtonText(): "Stäng meny" | "Öppna meny";
  get cssModifiers(): {
    'digi-navigation-vertical-menu-item': boolean;
    'digi-navigation-vertical-menu-item--active': boolean;
    'digi-navigation-vertical-menu-item--active-subnav': boolean;
    'digi-navigation-vertical-menu-item--icon': boolean;
  };
  render(): any;
}
