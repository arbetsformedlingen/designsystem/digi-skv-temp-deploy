export declare enum NavigationVerticalMenuActiveIndicatorSize {
  PRIMARY = "primary",
  SECONDARY = "secondary"
}
