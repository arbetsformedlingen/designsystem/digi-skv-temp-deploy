import { HTMLStencilElement } from '../../../stencil-public-runtime';
import { NavigationVerticalMenuVariation } from './navigation-vertical-menu-variation.enum';
import { NavigationVerticalMenuActiveIndicatorSize } from './navigation-vertical-menu-active-indicator-size.enum';
/**
 * @slot default - Måste innehålla en eller flera digi-navigation-vertical-menu-item
 *
 * @enums NavigationVerticalMenuVariation - navigation-vertical-menu-variation.enum.ts
 * @enums NavigationVerticalMenuActiveIndicatorSize = navigation-vertical-menu-active-indicator-size.enum.ts
 *@swedishName Vertikal meny
 */
export declare class NavigationVerticalMenu {
  hostElement: HTMLStencilElement;
  listItems: any[];
  /**
   * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Set id attribute. Defaults to random string.
   */
  afId: string;
  /**
   * Sätter variant. Kan vara 'primary' eller 'secondary'.
   * @en Set variation of vertical menu. Can be 'primary' or 'secondary'.
   * @ignore
   */
  afVariation: NavigationVerticalMenuVariation;
  /**
   * Sätter variant. Kan vara 'primary' eller 'secondary'.
   * @en Set variation of vertical menu. Can be 'primary' or 'secondary'.
   */
  afActiveIndicatorSize: NavigationVerticalMenuActiveIndicatorSize;
  /**
   * Sätt attributet 'aria-label'.
   * @en Set button `aria-label` attribute.
   */
  afAriaLabel: string;
  linkClickHandler(e: any): void;
  setClasses(el: any): void;
  componentWillLoad(): void;
  componentDidLoad(): void;
  componentWillUpdate(): void;
  initCurrentActiveLevel(): void;
  setCurrentActiveLevel(targetElement: any): void;
  setLevelClass(): void;
  initNav(): void;
  afMSetCurrentActiveLevel(e: any): Promise<void>;
  getListItems(): void;
  get cssModifiers(): {
    'digi-navigation-vertical-menu': boolean;
    'digi-navigation-vertical-menu--primary': boolean;
    'digi-navigation-vertical-menu--secondary': boolean;
    'digi-navigation-vertical-menu--indicator--secondary': boolean;
  };
  render(): any;
}
