export declare enum NavigationVerticalMenuVariation {
  PRIMARY = "primary",
  SECONDARY = "secondary"
}
