import { HTMLStencilElement } from '../../../stencil-public-runtime';
import { ProgressbarRole } from './progressbar-role.enum';
import { ProgressbarVariation } from './progressbar-variation.enum';
/**
 * @enums ProgressbarRole - progressbar-role.enum.ts
 * @enums ProgressbarVariation - progressbar-variation.enum.ts
 * @swedishName Förloppsmätare
 */
export declare class Progressbar {
  hostElement: HTMLStencilElement;
  private _observer;
  completedSteps: number;
  totalSteps: any[];
  listItems: any[];
  hasSlottedItems: boolean;
  /**
   * Sätter antal färdiga steg
   * @en Set finished amount of steps
   */
  afCompletedSteps: number;
  /**
   * Sätter totala antalet steg
   * @en Set amount of total steps
   */
  afTotalSteps: number;
  /**
   * Text emellan antal färdiga och totala antalet steg.
   * Som standard används "av", men kan ändras för exempelvis annat språk.
   * @en Separator text between current steps and total steps.
   * Default is "av", but can be changed e.g. for different language.
   */
  afStepsSeparator: string;
  /**
   * Beskrivande text efter siffrorna.
   * @en Descriptive text after step numbers.
   */
  afStepsLabel: string;
  /**
   * Sätter attributet 'role'. Förvalt är 'status'.
   * Använder du komponenten både före och efter ett formulär så
   * ange bara den ena som 'status' för att undvika att skärmläsare läser upp texten två gånger
   * @en Set role attribute. Defaults to 'status'.
   * If you apply the progressbar both before and after a form,
   * set only role 'status' on one of them to avoid screen readers to read the text twice
   */
  afRole: ProgressbarRole;
  /**
   * Sätter variant. Kan vara 'primary' och 'secondary'.
   * @en Set variation of progressbar. Can be 'primary' and 'secondary'.
   */
  afVariation: ProgressbarVariation;
  /**
   * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Input id attribute. Defaults to random string.
   */
  afId: string;
  componentWillLoad(): void;
  componentDidLoad(): void;
  changeTotalStepsHandler(value: number): void;
  changeCompletedStepsHandler(value: number): void;
  getItems(update?: boolean): void;
  get cssModifiers(): {
    'digi-progressbar': boolean;
    'digi-progressbar--primary': boolean;
    'digi-progressbar--secondary': boolean;
    'digi-progressbar--linear': boolean;
  };
  get slottedContent(): void;
  render(): any;
}
