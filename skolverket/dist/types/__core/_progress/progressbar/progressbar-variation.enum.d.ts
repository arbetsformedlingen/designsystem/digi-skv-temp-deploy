export declare enum ProgressbarVariation {
  PRIMARY = "primary",
  SECONDARY = "secondary"
}
