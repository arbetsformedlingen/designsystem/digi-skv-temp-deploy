export declare enum ProgressStepsStatus {
  CURRENT = "current",
  UPCOMING = "upcoming",
  DONE = "done"
}
