import { ProgressStepsVariation } from './progress-steps-variation.enum';
import { ProgressStepsHeadingLevel } from './progress-steps-heading-level.enum';
import { HTMLStencilElement } from '../../../stencil-public-runtime';
/**
 * @enums ProgressStepsVariation -progress-steps-variation.enum
 * @enums  ProgressStepsHeadingLevel - progress-steps-heading-level.enum
 *  @enums  ProgressStepsStatus - progress-steps-status.enum
 * @swedishName Förloppssteg
 */
export declare class ProgressSteps {
  hostElement: HTMLStencilElement;
  /**
   * Sätter rubriknivå på varje förloppsteg. 'h2' är förvalt.
   * @en Set heading level. Default is 'h2'
   */
  afHeadingLevel: ProgressStepsHeadingLevel;
  /**
   * Sätter variant. Kan vara 'Primary' eller 'Secondary'
   * @en Set variation. Can be 'Primary' or 'Secondary'
   */
  afVariation: ProgressStepsVariation;
  /**
   * Sätter nuvarande steg
   * @en Set current steps
   */
  afCurrentStep: number;
  steps: any[];
  afMNext(): Promise<void>;
  afMPrevious(): Promise<void>;
  componentWillLoad(): void;
  componentWillUpdate(): void;
  getSteps(): void;
  setStepProps(): void;
  render(): any;
}
