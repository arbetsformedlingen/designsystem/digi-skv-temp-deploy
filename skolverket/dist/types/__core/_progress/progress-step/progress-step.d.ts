import { HTMLStencilElement } from '../../../stencil-public-runtime';
import { ProgressStepStatus } from './progress-step-status.enum';
import { ProgressStepVariation } from './progress-step-variation.enum';
import { ProgressStepHeadingLevel } from './progress-step-heading-level.enum';
/**
 * @enums ProgressStepStatus - progress-step-status.enum.ts
 * @enums ProgressStepHeadingLevel- progress-step-heading-level.enum.ts
 * @enums ProgressbarVariation - progressbar-variation.enum.ts
 * @swedishName Förloppssteg - enskilt steg

 */
export declare class ProgressStep {
  hostElement: HTMLStencilElement;
  /**
   * Rubrikens text
   * @en The heading of text
   */
  afHeading: string;
  /**
   * Sätter rubriknivå. 'h2' är förvalt.
   * @en Set heading level. Default is 'h2'
   */
  afHeadingLevel: ProgressStepHeadingLevel;
  /**
   * Sätter attributet 'afStepStatus'. Förvalt är 'upcoming'.
   * @en Set role attribute. Defaults to 'upcoming'.
   */
  afStepStatus: ProgressStepStatus;
  /**
   * Sätter variant. Kan vara 'Primary' eller 'Secondary'
   * @en Set variation. Can be 'Primary' or 'Secondary'
   */
  afVariation: ProgressStepVariation;
  /**
   * Kollar om det är sista steget
   * @en checks if it is the last step
   */
  afIsLast: boolean;
  /**
   * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Input id attribute. Defaults to random string.
   */
  afId: string;
  render(): any;
}
