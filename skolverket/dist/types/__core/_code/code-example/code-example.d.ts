import { CodeExampleVariation } from './code-example-variation.enum';
import { CodeExampleLanguage } from './code-example-language.enum';
import { HTMLStencilElement } from '../../../stencil-public-runtime';
import { CodeBlockVariation } from '../code-block/code-block-variation.enum';
/**
 * @enums CodeExampleLanguage - code-example-language.enum.ts
 * @enums CodeExampleVariation - code-example-variation.enum.ts
 * @swedishName Kodexempel
 */
export declare class CodeExample {
  isActive: boolean;
  codeLanguage: CodeExampleLanguage;
  hasControls: boolean;
  showMoreButton: boolean;
  containerElement: HTMLDivElement;
  hostElement: HTMLStencilElement;
  private _codeExample;
  private _codeHeightObserver;
  /**
   * Försöker parsa afCode som ett objekt.
   * Returnerar antingen codeExample som sträng - antingen direkt om afCode var en sträng, annars det valda kodspråket.
   */
  get codeExample(): string;
  set codeExample(code: string);
  /**
   * Returnerar tomt om inget kodspråk har skickats med i afCode.
   * Dock returnerar en lista av kodspråk (t.ex. ts eller js) om detta är angett.
   */
  get codeExampleLanguages(): string[];
  /**
   * En sträng, eller objekt med strängar, med den kod som du vill visa upp.
   * @en A string, or object of strings, of code to use in the code block.
   */
  afCode: string | {
    [codeLang in CodeExampleLanguage]: string;
  };
  /**
   * Sätt färgtemat på kodblocket. Kan vara ljust eller mörkt.
   * @en Set code block variation. This defines the syntax highlighting and can be either light or dark.
   */
  afCodeBlockVariation: CodeBlockVariation;
  /**
   * Sätt färgtemat på exemplets bakgrund. Kan vara ljust eller mörkt.
   * @en Set example background. This defines the syntax highlighting and can be either light or dark.
   */
  afExampleVariation: CodeExampleVariation;
  /**
   * Sätt programmeringsspråk. 'html' är förvalt.
   * @en Set code language. Defaults to html.
   */
  afLanguage: CodeExampleLanguage;
  afHideControls: boolean;
  afHideCode: boolean;
  /**
   * Spara kodexemplet från afCode vid uppdatering av attributet.
   * @en Save the code example from afCode on update of the attribute.
   */
  codeExampleUpdate(): void;
  componentWillLoad(): void;
  componentWillUpdate(): void;
  componentDidLoad(): void;
  toggleButtonClickHandler(): void;
  copyButtonClickHandler(): void;
  /**
   * Ändrar kodspråk vid val av radioknapp
   * @param e Event object of the change event.
   */
  codeLanguageChangeHandler(e: any): void;
  /**
   * Visar "Visa mer" när kodblocket är högre än 180px.
   * @en Show "Visa mer" when code block is higher than 180px.
   */
  updateShowMoreButton(): void;
  get cssModifiers(): {
    [x: string]: boolean;
    'digi-code-example--active': boolean;
    'digi-code-example--inactive': boolean;
    'digi-code-example--light': boolean;
    'digi-code-example--dark': boolean;
    'digi-code-example--no-controls': boolean;
  };
  capitalize(word: any): any;
  render(): any;
}
