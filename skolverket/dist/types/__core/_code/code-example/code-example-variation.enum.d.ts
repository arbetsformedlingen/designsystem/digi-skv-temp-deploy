export declare enum CodeExampleVariation {
  LIGHT = "light",
  DARK = "dark"
}
