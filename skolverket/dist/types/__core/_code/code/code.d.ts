import { CodeVariation } from './code-variation.enum';
import { CodeLanguage } from './code-language.enum';
import 'prismjs/components/prism-typescript';
import 'prismjs/components/prism-bash';
import 'prismjs/components/prism-scss';
import 'prismjs/components/prism-json';
import 'prismjs/components/prism-git';
/**
 * @enums CodeLanguage - code-language.enum.ts
 * @enums CodeVariation - code-variation.enum.ts
 * @swedishName Kod
 */
export declare class Code {
  highlightedCode: any;
  /**
   * En sträng med den kod som du vill visa upp.
   * @en A string of code to use in the code block.
   */
  afCode: string;
  /**
   * Sätt färgtemat. Kan vara ljust eller mörkt.
   * @en Set code block variation. This defines the syntax highlighting and can be either light or dark.
   */
  afVariation: CodeVariation;
  /**
   * Sätt programmeringsspråk. 'html' är förvalt.
   * @en Set code language. Defaults to html.
   */
  afLanguage: CodeLanguage;
  /**
   * Sätt språk inuti code-elementet. 'en' (engelska) är förvalt.
   * @en Set language inside of code element. Defaults to 'en' (english).
   */
  afLang: string;
  componentWillLoad(): void;
  formatCode(): void;
  get cssModifiers(): {
    'digi-code--light': boolean;
    'digi-code--dark': boolean;
  };
  render(): any;
}
