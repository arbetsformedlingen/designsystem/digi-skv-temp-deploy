export declare enum CodeBlockVariation {
  LIGHT = "light",
  DARK = "dark"
}
