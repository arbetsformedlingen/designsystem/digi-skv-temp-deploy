import { CodeBlockVariation } from './code-block-variation.enum';
import { CodeBlockLanguage } from './code-block-language.enum';
import 'prismjs/components/prism-typescript';
import 'prismjs/components/prism-bash';
import 'prismjs/components/prism-scss';
import 'prismjs/components/prism-json';
import 'prismjs/components/prism-git';
/**
 * @enums CodeBlockLanguage - code-block-language.enum.ts
 * @enums CodeBlockVariation - code-block-variation.enum.ts
 * @swedishName Kodblock
 */
export declare class CodeBlock {
  highlightedCode: any;
  /**
   * En sträng med den kod som du vill visa upp.
   * @en A string of code to use in the code block.
   */
  afCode: string;
  /**
   * Sätt färgtemat. Kan vara ljust eller mörkt.
   * @en Set code block variation. This defines the syntax highlighting and can be either light or dark.
   */
  afVariation: CodeBlockVariation;
  /**
   * Sätt programmeringsspråk. 'html' är förvalt.
   * @en Set code language. Defaults to html.
   */
  afLanguage: CodeBlockLanguage;
  /**
   * Sätt språk inuti code-elementet. 'en' (engelska) är förvalt.
   * @en Set language inside of code element. Defaults to 'en' (english).
   */
  afLang: string;
  /**
   * Ta bort knappar för att till exempel kopiera kod
   * @en Remove toolbar for copying code etc
   */
  afHideToolbar: boolean;
  componentWillLoad(): void;
  formatCode(): void;
  copyButtonClickHandler(): void;
  get cssModifiers(): {
    'digi-code-block--light': boolean;
    'digi-code-block--dark': boolean;
  };
  render(): any;
}
