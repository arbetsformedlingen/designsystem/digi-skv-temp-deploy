import { EventEmitter } from '../../../stencil-public-runtime';
/**
 *
 * @swedishName Mediebild
 */
export declare class MediaImage {
  isPlaceholderLoaded: boolean;
  hasSetSrc: boolean;
  hasAspectRatio: boolean;
  paddedBoxPadding: string;
  isLoaded: boolean;
  src: string;
  srcset: string;
  /**
   * Skicka options till komponentens interna Intersection Observer. T.ex. för att kontrollera när bilden lazy loadas.
   * @en Accepts an Intersection Observer options object. Controls when the image should lazy-load.
   *

   */
  afObserverOptions: {
    rootMargin: string;
    threshold: number;
  };
  /**
   * Sätter attributet 'src'.
   * @en Set `src` attribute.
   */
  afSrc: string;
  /**
   * Sätter attributet 'srcset'.
   * @en Set `srcset` attribute.
   */
  afSrcset: string;
  /**
   * Sätter attributet 'alt'.
   * @en Set `alt` attribute.
   */
  afAlt: string;
  /**
   * Sätter attributet 'title'.
   * @en Set `title` attribute.
   */
  afTitle: string;
  /**
   * Sätter attributet 'aria-label'.
   * @en Set `aria-label` attribute.
   */
  afAriaLabel: string;
  /**
   * Sätter attributet 'width'. Om både 'width' och 'height' är satta så kommer komponenten att ha rätt aspektratio även innan bilden laddats in.
   * @en Set `width` attribute. if both 'width' and 'height' are set, the component will have the correct apect ratio even before the image source is loaded.
   */
  afWidth: string;
  /**
   * Sätter attributet 'height'. Om både 'width' och 'height' är satta så kommer komponenten att ha rätt aspektratio även innan bilden laddats in.
   * @en Set `height` attribute. if both 'width' and 'height' are set, the component will have the correct apect ratio even before the image source is loaded.
   */
  afHeight: string;
  /**
   * Sätter bilden till 100% i bredd och anpassar höjden automatiskt efter bredden.
   * @en Will set the image width to 100% and height to auto.
   */
  afFullwidth: boolean;
  /**
   * Tar bort lazy loading av bilden.
   * @en Removes lazy laoding of the image.
   */
  afUnlazy: boolean;
  /**
   * Bildlementets 'onload'-event.
   * @en The image element's 'onload' event.
   */
  afOnLoad: EventEmitter;
  srcChangeHandler(): void;
  connectedCallback(): void;
  setImageSrc(): void;
  setPaddedBox(): void;
  intersectHandler(): void;
  loadHandler(e: Event): void;
  get cssModifiers(): {
    'digi-media-image--unlazy': boolean;
    'digi-media-image--loaded': boolean;
    'digi-media-image--aspect-ratio': boolean;
    'digi-media-image--fullwidth': boolean;
  };
  get cssInlineStyles(): {
    '--digi--media-image--padded-box': string;
  };
  render(): any;
}
