export declare enum MediaFigureAlignment {
  START = "start",
  CENTER = "center",
  END = "end"
}
