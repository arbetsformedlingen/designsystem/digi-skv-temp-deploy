import { MediaFigureAlignment } from './media-figure-alignment.enum';
/**
 * @slot default - Ska vara en bild (eller bildlik) komponent. Helst en `digi-media-image`.
 *
 * @enums MediaFigureAlignment - media-figure-alignment.enum.ts
 * @swedishName Mediefigur
 */
export declare class MediaFigure {
  /**
   * Lägger till ett valfritt figcaptionelement
   * @en Adds an optional figcaption element
   */
  afFigcaption: string;
  /**
   * Justering av bild och text. Kan vara 'start', 'center' eller 'end'
   * @en Alignment of image and caption. Value can be 'start', 'center' or 'end'.
   */
  afAlignment: MediaFigureAlignment;
  get cssModifiers(): {
    'digi-media-figure--align-start': boolean;
    'digi-media-figure--align-center': boolean;
    'digi-media-figure--align-end': boolean;
  };
  render(): any;
}
