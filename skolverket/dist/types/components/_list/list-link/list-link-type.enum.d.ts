export declare enum ListLinkType {
  UNORDERED = "ul",
  ORDERED = "ol"
}
