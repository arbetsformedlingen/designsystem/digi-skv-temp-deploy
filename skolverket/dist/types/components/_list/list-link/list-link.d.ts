import { HTMLStencilElement } from '../../../stencil-public-runtime';
import { ListLinkLayout } from './list-link-layout.enum';
import { ListLinkType } from './list-link-type.enum';
import { ListLinkVariation } from './list-link-variation.enum';
/**
 * @slot default - ska innehålla li-element med a-element inuti.
 * @slot heading - Rubrik
 *
 * @swedishName Länklista
 */
export declare class ListLink {
  hostElement: HTMLStencilElement;
  hasHeading: boolean;
  afType: ListLinkType;
  afLayout: ListLinkLayout;
  afVariation: ListLinkVariation;
  componentWillLoad(): void;
  componentWillUpdate(): void;
  setHasHeading(): void;
  get cssModifiers(): {
    [x: string]: boolean;
  };
  render(): any;
}
