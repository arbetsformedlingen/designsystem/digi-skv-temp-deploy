export declare enum ListLinkVariation {
  REGULAR = "regular",
  COMPACT = "compact"
}
