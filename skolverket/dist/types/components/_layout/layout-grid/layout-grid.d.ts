import { LayoutGridVerticalSpacing } from './layout-grid-vertical-spacing.enum';
/**
 * @slot default - kan innehålla vad som helst
 *
 * @swedishName Basgrid
 */
export declare class LayoutGrid {
  afVerticalSpacing: `${LayoutGridVerticalSpacing}`;
  get cssModifiers(): {
    [x: string]: boolean;
  };
  render(): any;
}
