import { LayoutStackedBlocksVariation } from './layout-stacked-blocks-variation.enum';
/**
 * @slot default - kan innehålla vad som helst
 *
 * @swedishName Staplande block
 */
export declare class LayoutStackedBlocks {
  afVariation: `${LayoutStackedBlocksVariation}`;
  render(): any;
}
