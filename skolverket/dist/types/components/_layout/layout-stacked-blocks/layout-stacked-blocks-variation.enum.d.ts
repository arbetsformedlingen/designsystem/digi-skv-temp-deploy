export declare enum LayoutStackedBlocksVariation {
  REGULAR = "regular",
  ENHANCED = "enhanced"
}
