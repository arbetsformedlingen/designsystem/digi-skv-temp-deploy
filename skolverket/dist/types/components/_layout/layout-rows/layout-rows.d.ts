/**
 * @slot default - kan innehålla vad som helst
 *
 * @swedishName Rader
 */
export declare class LayoutRows {
  render(): any;
}
