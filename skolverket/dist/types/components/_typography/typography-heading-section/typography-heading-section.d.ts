/**
 * @slot default - ska innehålla ett rubrikelement
 *
 * @swedishName Avsnittsrubrik
 */
export declare class TypographyHeadingSection {
  render(): any;
}
