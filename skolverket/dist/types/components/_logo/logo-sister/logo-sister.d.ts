/**
 * @swedishName Logotyp syskonwebbplats
 */
export declare class LogoSister {
  /**
   * Syskonwebbplatsens namn
   * @en Name of the sister site.
   */
  afName: string;
  /**
   * Sätter attributet 'id' på namntextens element.
   * Om inget väljs så skapas ett slumpmässigt id.
   * @en Id attribute on the name element. Defaults to random string.
   */
  afNameId: string;
  render(): any;
}
