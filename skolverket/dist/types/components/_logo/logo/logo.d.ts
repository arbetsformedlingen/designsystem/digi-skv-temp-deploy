/**
 * @swedishName Logotyp
 */
export declare class Logo {
  relation: string;
  /**
   * Lägger till ett titleelement i svg:n. Standardtext är 'Skolverket'.
   * @en Adds a title element inside the svg. Defaults to 'Skolverket'.
   */
  afTitle: string;
  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  afDesc: string;
  /**
   * För att dölja logotypen för skärmläsare.
   * @en Hides the Logotype for screen readers.
   */
  afSvgAriaHidden: boolean;
  /**
   * Sätter attributet 'id' på title-elementet inuti svg-elementet.
   * Om inget väljs så skapas ett slumpmässigt id.
   * @en Id attribute on the title element inside of the svg. Defaults to random string.
   */
  afTitleId: string;
  get cssModifiers(): {
    'digi-logo': boolean;
  };
  render(): any;
}
