/**
 * @swedishName Logotyp tjänster & verktyg
 */
export declare class LogoService {
  /**
   * Tjänsten eller verktygets namn
   * @en Name of the service or tool.
   */
  afName: string;
  /**
   * Tjänsten eller verktygets beskrivning
   * @en Description of the service or tool.
   */
  afDescription: string;
  /**
   * Sätter attributet 'id' på namntextens element.
   * Om inget väljs så skapas ett slumpmässigt id.
   * @en Id attribute on the name element. Defaults to random string.
   */
  afNameId: string;
  render(): any;
}
