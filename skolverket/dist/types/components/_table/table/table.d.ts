import { TableVariation } from './table-variation.enum';
/**
 * @slot default - Ska innehålla ett table-element
 *
 * @swedishName Tabell
 */
export declare class Table {
  afVariation: TableVariation;
  get cssModifiers(): {
    [x: string]: boolean;
  };
  render(): any;
}
