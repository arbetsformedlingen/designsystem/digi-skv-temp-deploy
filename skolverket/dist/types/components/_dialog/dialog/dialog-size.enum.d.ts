export declare enum DialogSize {
  SMALL = "small",
  MEDIUM = "medium",
  LARGE = "large"
}
