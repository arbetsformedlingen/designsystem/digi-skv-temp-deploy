import { EventEmitter } from '../../../stencil-public-runtime';
/**
 * @slot default - Kan innehålla vad som helst
 * @slot heading - Rubrik
 * @slot actions - Knappar
 *
 * @swedishName Modal
 */
export declare class Dialog {
  private _dialog;
  afOpen: boolean;
  afOpenChanged(newVal: any, oldVal: any): void;
  afHideCloseButton: boolean;
  showModal(): Promise<void>;
  close(): Promise<void>;
  afOnOpen: EventEmitter;
  afOnClose: EventEmitter;
  toggleEmitter(state: 'open' | 'close'): void;
  clickOutsideHandler(e: any): void;
  componentDidLoad(): void;
  render(): any;
}
