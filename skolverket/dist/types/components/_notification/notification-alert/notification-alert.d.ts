import { EventEmitter } from '../../../stencil-public-runtime';
import { NotificationAlertVariation } from './notification-alert-variation.enum';
/**
 * @slot default - Kan innehålla vad som helst
 * @slot heading - Rubrik
 * @slot link - Länk
 * @slot actions - Knappar
 *
 * @swedishName Informationsmeddelande
 */
export declare class NotificationAlert {
  /**
   * Sätter variant.
   * @en Set variation of alert notification. Can be 'info', 'success', 'warning' or 'danger'.
   */
  afVariation: `${NotificationAlertVariation}`;
  /**
   * Gör det möjligt att stänga meddelandet med en stängknapp
   * @en If true, the close button is added to the component
   */
  afCloseable: boolean;
  /**
   * Stängknappens 'onclick'-event
   * @en Close button's 'onclick' event
   */
  afOnClose: EventEmitter<MouseEvent>;
  clickHandler(e: any): void;
  get cssModifiers(): {
    [x: string]: boolean;
    'digi-notification-alert--closeable': boolean;
  };
  render(): any;
}
