import { NotificationDetailVariation } from './notification-detail-variation.enum';
/**
 * @slot default - Kan innehålla vad som helst
 * @slot heading - Ska innehålla en rubrik
 *
 * @swedishName Detaljmeddelande
 */
export declare class NotificationDetail {
  /**
   * Sätter variant.
   * @en Set the variation.
   */
  afVariation: `${NotificationDetailVariation}`;
  get cssModifiers(): {
    [x: string]: boolean;
  };
  render(): any;
}
