export declare enum NotificationDetailVariation {
  INFO = "info",
  WARNING = "warning",
  DANGER = "danger"
}
