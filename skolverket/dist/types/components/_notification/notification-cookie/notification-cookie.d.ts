import { EventEmitter } from '../../../stencil-public-runtime';
/**
 * @slot default - Kan innehålla vad som helst
 * @slot link - Länk tiill exempel till läs-mer-om-cookies-sidan
 * @slot settings - Formulärselement för olika cookieinställningar
 *
 * @swedishName Cookiemeddelande
 */
export declare class NotificationCookie {
  modalIsOpen: boolean;
  afBannerText: string;
  afBannerHeadingText: string;
  afModalHeadingText: string;
  afRequiredCookiesText: string;
  /**
   * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Component id attribute. Defaults to random string.
   */
  afId: string;
  /**
   * När användaren godkänner alla kakor
   * @en When the user accepts all cookies
   */
  afOnAcceptAllCookies: EventEmitter<MouseEvent>;
  /**
   * När användaren godkänner alla kakor
   * @en When the user accepts all cookies
   */
  afOnSubmitSettings: EventEmitter;
  clickPrimaryButtonHandler(): void;
  formSubmitHandler(e: any): void;
  openModal(): void;
  closeModal(): void;
  render(): any;
}
