import { EventEmitter } from '../../../stencil-public-runtime';
/**
 * @slot default - Ska innehålla flera <form-process-step>
 *
 * @swedishName Processteg enskilt steg
 */
export declare class FormProcessStep {
  afHref: string;
  afType: 'current' | 'completed' | 'upcoming';
  afContext: 'regular' | 'fallback';
  afLabel: string;
  afClick: EventEmitter<MouseEvent>;
  clickHandler(e: MouseEvent): void;
  get cssModifiers(): {
    [x: string]: boolean;
  };
  render(): any;
}
