import { HTMLStencilElement } from '../../../stencil-public-runtime';
/**
 * @slot default - Ska innehålla flera <li><form-process-step></form-process-step></li>
 *
 * @swedishName Processteg
 */
export declare class FormProcessSteps {
  private _contentElement;
  private _button;
  hostElement: HTMLStencilElement;
  /**
   * När stegen inte får plats så ska de visas i en accordion.
   */
  isFallback: boolean;
  isExpanded: boolean;
  fallbackIsSet: boolean;
  listWidth: number;
  containerWidth: number;
  steps: NodeListOf<HTMLDigiFormProcessStepElement>;
  handleWidthChange(): void;
  afCurrentStep: number;
  /**
   * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Set id attribute. Defaults to random string.
   */
  afId: string;
  componentWillLoad(): void;
  componentDidLoad(): void;
  componentWillUpdate(): void;
  handleFocusWithin(): void;
  measureItemsList(): void;
  setTypeOnChildren(): void;
  setContextOnChildren(): void;
  resizeHandler(): void;
  clickToggleHandler(e: MouseEvent, resetFocus?: boolean): void;
  get cssModifiers(): {
    [x: string]: boolean;
    "digi-form-process-steps--fallback-is-set": boolean;
  };
  render(): any;
}
