import { PageBackground } from './page-background.enum';
/**
 * @slot default - kan innehålla vad som helst
 * @slot header - Sidhuvud
 * @slot footer - Sidfot
 *
 * @swedishName Sidmall
 */
export declare class Page {
  afBackground: `${PageBackground}`;
  render(): any;
}
