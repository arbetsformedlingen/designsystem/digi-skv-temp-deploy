import { HTMLStencilElement } from '../../../stencil-public-runtime';
/**
 * @slot default - kan innehålla vad som helst
 * @slot top - länk till mina sidor, tillbaka till skolvetket.se etc
 * @slot logo - ska innehålla en logotyp
 * @slot nav - ska innehålla en navigation
 *
 * @swedishName Sidhuvud
 */
export declare class PageHeader {
  private toggleButton?;
  hostElement: HTMLStencilElement;
  isExpanded: boolean;
  /**
   * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Input id attribute. Defaults to random string.
   */
  afId: string;
  get cssModifiers(): {
    [x: string]: boolean;
  };
  keyUpHandler(e: KeyboardEvent): void;
  render(): any;
}
