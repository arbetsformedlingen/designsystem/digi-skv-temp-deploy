import { PageBlockHeroBackground } from './page-block-hero-background.enum';
import { PageBlockHeroVariation } from './page-block-hero-variation.enum';
/**
 * @slot default
 * @slot heading - Rubrik
 *
 * @swedishName Sidblock med hero
 */
export declare class PageBlockHero {
  afVariation: `${PageBlockHeroVariation}`;
  afBackground: `${PageBlockHeroBackground}`;
  afBackgroundImage: string;
  get cssModifiers(): {
    [x: string]: boolean;
    'digi-page-block-hero--background-image': boolean;
  };
  render(): any;
}
