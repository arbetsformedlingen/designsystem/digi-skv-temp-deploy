import { PageFooterVariation } from './page-footer-variation.enum';
/**
 * @slot default - kan innehålla vad som helst
 * @slot top-first
 * @slot top
 * @slot bottom
 *
 * @swedishName Sidfot
 */
export declare class PageFooter {
  afVariation: PageFooterVariation;
  get cssModifiers(): {
    [x: string]: boolean;
  };
  render(): any;
}
