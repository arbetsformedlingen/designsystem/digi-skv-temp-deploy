import { PageBlockListsVariation } from './page-block-lists-variation.enum';
/**
 * @slot default
 * @slot heading - Rubrik
 *
 * @swedishName Sidblock med listor
 */
export declare class PageBlockLists {
  afVariation: `${PageBlockListsVariation}`;
  get cssModifiers(): {
    [x: string]: boolean;
  };
  render(): any;
}
