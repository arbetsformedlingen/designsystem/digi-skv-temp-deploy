export declare enum PageBlockCardsVariation {
  START = "start",
  SUB = "sub",
  SECTION = "section",
  PROCESS = "process",
  ARTICLE = "article"
}
