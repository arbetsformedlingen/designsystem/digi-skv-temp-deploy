import { PageBlockCardsVariation } from './page-block-cards-variation.enum';
/**
 * @slot default
 * @slot heading - Rubrik
 *
 * @swedishName Sidblock med kort
 */
export declare class PageBlockCards {
  afVariation: `${PageBlockCardsVariation}`;
  get cssModifiers(): {
    [x: string]: boolean;
  };
  render(): any;
}
