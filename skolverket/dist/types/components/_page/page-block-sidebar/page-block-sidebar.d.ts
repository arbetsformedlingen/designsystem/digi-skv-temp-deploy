import { PageBlockSidebarVariation } from './page-block-sidebar-variation.enum';
/**
 * @slot default
 * @slot sidebar
 *
 * @swedishName Sidblock med sidebar
 */
export declare class PageBlockSidebar {
  afVariation: `${PageBlockSidebarVariation}`;
  get cssModifiers(): {
    [x: string]: boolean;
  };
  render(): any;
}
