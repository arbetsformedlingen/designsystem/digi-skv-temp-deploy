import { EventEmitter } from '../../../stencil-public-runtime';
import { HTMLStencilElement } from '../../../stencil-public-runtime';
/**
 * @slot default - Se översikten för exakt innehåll
 * @slot main-link - Länk ovanför navigationsraderna
 *
 *@swedishName Huvudmenyspanel
 */
export declare class NavigationMainMenuPanel {
  hostElement: HTMLStencilElement;
  nestedListItems: HTMLUListElement[];
  nestedListItemsChanged(): void;
  /**
   * När komponenten ändrar storlek
   * @en When the component changes size
   */
  afOnResize: EventEmitter<ResizeObserverEntry>;
  /**
   * När komponenten stängs
   * @en When the component changes size
   */
  afOnClose: EventEmitter;
  resizeHandler(e: any): void;
  closeHandler(e: any): void;
  componentWillLoad(): void;
  getAllListItems(): void;
  setCurrentListItem(listItems: NodeListOf<HTMLLIElement>): void;
  getNestedListItems(): void;
  toggleButtonClickHandler(e: MouseEvent): void;
  linkClickHandler(e: MouseEvent): void;
  setExpansionState(target: any): void;
  hydrateListItems(): void;
  keyUpHandler(e: KeyboardEvent): void;
  render(): any;
}
