import { HTMLStencilElement } from '../../../stencil-public-runtime';
/**
 * @slot default - Se översikten för exakt innehåll
 *
 *@swedishName Huvudmeny
 */
export declare class NavigationMainMenu {
  hostElement: HTMLStencilElement;
  menuButtons: any[];
  isActive: boolean;
  /**
   * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Set id attribute. Defaults to random string.
   */
  afId: string;
  componentWillLoad(): void;
  hydrateButton(button: any, i: any): void;
  toggleMenu(e: any): void;
  setMainLinkFocus(target: any): void;
  setTopLevelButtonFocus(button: any): void;
  setPanelHeight(li: any, resizeObserverEntry: any): void;
  removeActive(button: any): void;
  closeSubmenuHandler(e: any): void;
  keyUpHandler(e: KeyboardEvent): void;
  closeSubmenu(e: any): void;
  get cssModifiers(): {
    [x: string]: boolean;
  };
  render(): any;
}
