export declare enum NavigationBreadcrumbsVariation {
  REGULAR = "regular",
  COMPRESSED = "compressed"
}
