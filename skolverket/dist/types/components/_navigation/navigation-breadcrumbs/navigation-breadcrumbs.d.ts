import { NavigationBreadcrumbsVariation } from './navigation-breadcrumbs-variation.enum';
/**
 * @slot default - Ska inehålla li-element med a-element inuti.
 * @swedishName Brödsmulor
 */
export declare class NavigationBreadcrumbs {
  /**
   * Sätter attributet 'aria-label' på navelementet.
   * @en Set `aria-label` attribute on the nav element.
   */
  afAriaLabel: string;
  /**
   * Sätter texten för den aktiva sidan
   * @en Set text for the active page
   */
  afCurrentPage: string;
  /**
   * Sätter texten för den aktiva sidan
   * @en Set text for the active page
   */
  afVariation: NavigationBreadcrumbsVariation;
  get cssModifiers(): {
    [x: string]: boolean;
  };
  render(): any;
}
