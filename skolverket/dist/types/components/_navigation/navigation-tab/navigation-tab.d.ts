import { EventEmitter } from '../../../stencil-public-runtime';
/**
 * @slot default - Kan innehålla vad som helst
 * @swedishName Flik
 */
export declare class NavigationTab {
  /**
   * Sätter attributet 'aria-label'
   * @en Set aria-label attribute
   */
  afAriaLabel: string;
  /**
   * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Set id attribute. Defaults to random string.
   */
  afId: string;
  /**
   * Sätter aktiv tabb. Detta sköts av digi-navigation-tabs som ska omsluta denna komponent.
   * @en Sets active tab (this is handled by digi-navigation-tabs which should wrap this component)
   */
  afActive: boolean;
  /**
   * När tabben växlar mellan aktiv och inaktiv
   * @en When the tab toggles between active and inactive
   */
  afOnToggle: EventEmitter<boolean>;
  toggleHandler(activeTab: boolean): void;
  render(): any;
}
