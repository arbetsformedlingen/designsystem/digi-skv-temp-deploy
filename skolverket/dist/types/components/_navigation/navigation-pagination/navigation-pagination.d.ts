import { EventEmitter } from '../../../stencil-public-runtime';
/**
 * @swedishName Paginering
 */
export declare class NavigationPagination {
  pages: any[];
  currentPage: number;
  /**
   * Sätter initiala aktiva sidan
   * @en Set initial start page
   */
  afInitActivePage: number;
  afInitActivePageChanged(): void;
  /**
   * Sätter totala mängden sidor
   * @en Set total pages that exists
   */
  afTotalPages: number;
  /**
   * Sätter startvärdet för nuvarande resultat
   * @en Set start value of current results
   */
  afCurrentResultStart: number;
  /**
   * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Input id attribute. Defaults to random string.
   */
  afId: string;
  /**
   * Sätter slutvärdet för nuvarande resultat
   * @en Set end value of current results
   */
  afCurrentResultEnd: number;
  /**
   * Sätter totala mängden resultat
   * @en Set total results
   */
  afTotalResults: number;
  /**
   * Sätter ett resultatnamn. Det används för att kommunicera vad som listas. T.ex. om värdet är 'användare' så kan det stå 'Visar 23-138 användare'.
   * @en Set a result name. This is used to communicate what is being listed. For example, if the value is 'users', the component might say 'Showing 23-138 users'.
   */
  afResultName: string;
  /**
   * Vid byte av sida
   * @en When page changes
   */
  afOnPageChange: EventEmitter<number>;
  /**
   * Kan användas för att manuellt sätta om den aktiva sidan.
   * @en Can be used to set the active page.
   */
  afMSetCurrentPage(pageNumber: number): Promise<void>;
  prevPage(): void;
  nextPage(): void;
  setCurrentPage(pageToSet?: number, emitEvent?: boolean): void;
  componentWillLoad(): void;
  updateTotalPages(): void;
  isCurrentPage(currentPage: number, page: number): string;
  labelledby(): string;
  render(): any;
}
