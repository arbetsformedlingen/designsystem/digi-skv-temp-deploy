import { HTMLStencilElement } from '../../../stencil-public-runtime';
/**
 * @slot default - Ska inehålla li-element med a-element inuti.
 * @slot heading - Rubrik
 * @swedishName Innehållsförteckning
 */
export declare class NavigationToc {
  hostElement: HTMLStencilElement;
  currentHash: string;
  currentHashChanged(): void;
  componentWillLoad(): void;
  setHash(): void;
  setAriaCurrent(): void;
  render(): any;
}
