import { EventEmitter } from '../../../stencil-public-runtime';
import { HTMLStencilElement } from '../../../stencil-public-runtime';
/**
 * @slot default - Ska innehålla flera digi-navigation-tab-*
 * @swedishName Flikfält
 */
export declare class NavigationTabs {
  hostElement: HTMLStencilElement;
  tabPanels: any[];
  activeTab: number;
  currentTabIndex: number;
  /**
   * Sätter attributet 'aria-label' på tablistelementet.
   * @en Set aria-label attribute on the tablist elementet.
   */
  afAriaLabel: string;
  /**
   * Sätter initial aktiv tabb
   * @en Set initial active tab index. Default to 0.
   */
  afInitActiveTab: number;
  /**
   * Input id attribute. Defaults to random string.
   */
  afId: string;
  afOnChange: EventEmitter;
  afOnClick: EventEmitter<MouseEvent>;
  afOnFocus: EventEmitter;
  /**
   * Sätter om aktiv flik.
   * @en Sets the active tab.
   */
  afMSetActiveTab(tabIndex: number): Promise<void>;
  changeHandler(activeTabIndex: number): void;
  setTabFocus(id: any): void;
  clickHandler(e: any, i: any): void;
  focusHandler(e: any): void;
  leftHandler(): void;
  rightHandler(): void;
  homeHandler(): void;
  endHandler(): void;
  tabId(prefix: string): string;
  decrementCurrentTabIndex(): void;
  incrementCurrentTabIndex(): void;
  componentDidLoad(): void;
  getTabs(e?: any): void;
  setActiveTab(newTabIndex: number): void;
  render(): any;
}
