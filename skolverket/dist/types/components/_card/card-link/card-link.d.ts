import { HTMLStencilElement } from '../../../stencil-public-runtime';
/**
 * @slot default - kan innehålla vad som helst
 * @slot image - Bild
 * @slot link-heading - Ska innehålla en rubrik med länk inuti
 *
 * @swedishName Länkat kort
 */
export declare class CardLink {
  hostElement: HTMLStencilElement;
  hasImage: boolean;
  componentWillLoad(): void;
  componentWillUpdate(): void;
  setHasImage(): void;
  get cssModifiers(): {
    [x: string]: boolean;
  };
  render(): any;
}
