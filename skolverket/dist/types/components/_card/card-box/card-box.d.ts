import { CardBoxWidth } from './card-box-width.enum';
import { CardBoxGutter } from './card-box-gutter.enum';
/**
 * @slot default - kan innehålla vad som helst
 *
 * @swedishName Skuggad box
 */
export declare class CardBox {
  afWidth: CardBoxWidth;
  afGutter: CardBoxGutter;
  get cssModifiers(): {
    [x: string]: boolean;
  };
  render(): any;
}
