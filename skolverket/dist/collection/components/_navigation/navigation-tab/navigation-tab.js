import { h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
/**
 * @slot default - Kan innehålla vad som helst
 * @swedishName Flik
 */
export class NavigationTab {
  constructor() {
    this.afAriaLabel = undefined;
    this.afId = randomIdGenerator('digi-navigation-tab');
    this.afActive = undefined;
  }
  toggleHandler(activeTab) {
    if (activeTab || null) {
      this.afOnToggle.emit(activeTab);
    }
  }
  render() {
    return (h("div", { class: "digi-navigation-tab", tabindex: "0", role: "tabpanel", id: this.afId, "aria-label": this.afAriaLabel, hidden: !this.afActive }, h("slot", null)));
  }
  static get is() { return "digi-navigation-tab"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["navigation-tab.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["navigation-tab.css"]
    };
  }
  static get properties() {
    return {
      "afAriaLabel": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": true,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set aria-label attribute"
            }],
          "text": "S\u00E4tter attributet 'aria-label'"
        },
        "attribute": "af-aria-label",
        "reflect": false
      },
      "afId": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set id attribute. Defaults to random string."
            }],
          "text": "S\u00E4tter attributet 'id'. Om inget v\u00E4ljs s\u00E5 skapas ett slumpm\u00E4ssigt id."
        },
        "attribute": "af-id",
        "reflect": false,
        "defaultValue": "randomIdGenerator('digi-navigation-tab')"
      },
      "afActive": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Sets active tab (this is handled by digi-navigation-tabs which should wrap this component)"
            }],
          "text": "S\u00E4tter aktiv tabb. Detta sk\u00F6ts av digi-navigation-tabs som ska omsluta denna komponent."
        },
        "attribute": "af-active",
        "reflect": false
      }
    };
  }
  static get events() {
    return [{
        "method": "afOnToggle",
        "name": "afOnToggle",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "When the tab toggles between active and inactive"
            }],
          "text": "N\u00E4r tabben v\u00E4xlar mellan aktiv och inaktiv"
        },
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        }
      }];
  }
  static get watchers() {
    return [{
        "propName": "afActive",
        "methodName": "toggleHandler"
      }];
  }
}
