import { h } from '@stencil/core';
import { NavigationBreadcrumbsVariation } from './navigation-breadcrumbs-variation.enum';
/**
 * @slot default - Ska inehålla li-element med a-element inuti.
 * @swedishName Brödsmulor
 */
export class NavigationBreadcrumbs {
  constructor() {
    this.afAriaLabel = 'breadcrumbs';
    this.afCurrentPage = undefined;
    this.afVariation = NavigationBreadcrumbsVariation.REGULAR;
  }
  get cssModifiers() {
    return {
      [`digi-navigation-breadcrumbs--variation-${this.afVariation}`]: !!this.afVariation
    };
  }
  render() {
    return (h("nav", { class: Object.assign({ 'digi-navigation-breadcrumbs': true }, this.cssModifiers), "aria-label": this.afAriaLabel }, h("ol", { class: "digi-navigation-breadcrumbs__items" }, h("slot", null), h("li", { "aria-current": "page" }, this.afCurrentPage))));
  }
  static get is() { return "digi-navigation-breadcrumbs"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["navigation-breadcrumbs.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["navigation-breadcrumbs.css"]
    };
  }
  static get properties() {
    return {
      "afAriaLabel": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set `aria-label` attribute on the nav element."
            }],
          "text": "S\u00E4tter attributet 'aria-label' p\u00E5 navelementet."
        },
        "attribute": "af-aria-label",
        "reflect": false,
        "defaultValue": "'breadcrumbs'"
      },
      "afCurrentPage": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": true,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set text for the active page"
            }],
          "text": "S\u00E4tter texten f\u00F6r den aktiva sidan"
        },
        "attribute": "af-current-page",
        "reflect": false
      },
      "afVariation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "NavigationBreadcrumbsVariation",
          "resolved": "NavigationBreadcrumbsVariation.COMPRESSED | NavigationBreadcrumbsVariation.REGULAR",
          "references": {
            "NavigationBreadcrumbsVariation": {
              "location": "import",
              "path": "./navigation-breadcrumbs-variation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set text for the active page"
            }],
          "text": "S\u00E4tter texten f\u00F6r den aktiva sidan"
        },
        "attribute": "af-variation",
        "reflect": false,
        "defaultValue": "NavigationBreadcrumbsVariation.REGULAR"
      }
    };
  }
}
