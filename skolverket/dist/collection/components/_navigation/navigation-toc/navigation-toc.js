import { h } from '@stencil/core';
/**
 * @slot default - Ska inehålla li-element med a-element inuti.
 * @slot heading - Rubrik
 * @swedishName Innehållsförteckning
 */
export class NavigationToc {
  constructor() {
    this.currentHash = undefined;
  }
  currentHashChanged() {
    this.setAriaCurrent();
  }
  componentWillLoad() {
    this.setHash();
  }
  setHash() {
    var _a;
    this.currentHash = ((_a = window === null || window === void 0 ? void 0 : window.location) === null || _a === void 0 ? void 0 : _a.hash) || '';
  }
  setAriaCurrent() {
    const links = this.hostElement.querySelectorAll('a');
    links.forEach((link) => {
      if (link.hash === this.currentHash) {
        link.setAttribute('aria-current', 'true');
      }
      else {
        link.removeAttribute('aria-current');
      }
    });
  }
  render() {
    return (h("div", { class: {
        'digi-navigation-toc': true
      } }, h("div", { class: "digi-navigation-toc__heading" }, h("slot", { name: "heading" })), h("digi-typography", null, h("ul", { class: "digi-navigation-toc__items" }, h("slot", null)))));
  }
  static get is() { return "digi-navigation-toc"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["navigation-toc.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["navigation-toc.css"]
    };
  }
  static get states() {
    return {
      "currentHash": {}
    };
  }
  static get elementRef() { return "hostElement"; }
  static get watchers() {
    return [{
        "propName": "currentHash",
        "methodName": "currentHashChanged"
      }];
  }
  static get listeners() {
    return [{
        "name": "hashchange",
        "method": "setHash",
        "target": "window",
        "capture": false,
        "passive": false
      }];
  }
}
