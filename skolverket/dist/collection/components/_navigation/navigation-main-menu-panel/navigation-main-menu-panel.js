import { h } from '@stencil/core';
import { ButtonVariation } from '../../../enums-core';
import { BREAKPOINT_LARGE } from '../../../design-tokens/web/tokens.es6';
/**
 * @slot default - Se översikten för exakt innehåll
 * @slot main-link - Länk ovanför navigationsraderna
 *
 *@swedishName Huvudmenyspanel
 */
export class NavigationMainMenuPanel {
  constructor() {
    this.nestedListItems = [];
  }
  nestedListItemsChanged() {
    this.hydrateListItems();
  }
  resizeHandler(e) {
    this.afOnResize.emit(e);
  }
  closeHandler(e) {
    this.afOnClose.emit(e);
  }
  componentWillLoad() {
    this.getAllListItems();
    this.getNestedListItems();
  }
  getAllListItems() {
    const allListItems = this.hostElement.querySelectorAll('li');
    this.setCurrentListItem(allListItems);
  }
  setCurrentListItem(listItems) {
    listItems.length &&
      Array.from(listItems).forEach((item) => {
        // If list item is exactly the current page
        if (!!item.querySelector(':scope > a[aria-current="page"]')) {
          item.setAttribute('data-current', 'exact');
          item.closest('ul').setAttribute('data-has-current-exact', 'true');
          this.hostElement.setAttribute('data-visible-current-exact', 'true');
          return;
        }
        // If list item has a nested current page
        !!item.querySelector('[aria-current="page"]') &&
          item.setAttribute('data-current', 'nested');
      });
  }
  getNestedListItems() {
    const nestedListItems = this.hostElement.querySelectorAll('digi-navigation-main-menu-panel > ul > li ul');
    if (nestedListItems.length < 0)
      return;
    this.nestedListItems = Array.from(nestedListItems);
  }
  toggleButtonClickHandler(e) {
    const target = e.target;
    if (!target.classList.contains('digi-navigation-main-menu-panel__toggle-button'))
      return;
    this.setExpansionState(target);
  }
  linkClickHandler(e) {
    const target = e.target;
    if (target.tagName !== 'A')
      return;
    this.closeHandler(e);
  }
  setExpansionState(target) {
    const isExpanded = target.getAttribute('aria-expanded') === 'true';
    const closestLi = target.closest('li');
    target.setAttribute('aria-expanded', isExpanded ? 'false' : 'true');
    closestLi.setAttribute('data-expanded', !isExpanded);
    if (isExpanded) {
      closestLi.querySelectorAll('[data-expanded]').forEach((el) => {
        el.setAttribute('data-expanded', 'false');
      });
      closestLi.querySelectorAll('[aria-expanded="true"]').forEach((el) => {
        el.setAttribute('aria-expanded', 'false');
      });
    }
    const currentPage = this.hostElement.querySelector('[aria-current="page"]');
    if (currentPage) {
      this.hostElement.setAttribute('data-visible-current-exact', window.getComputedStyle(currentPage.closest('ul')).display !== 'none'
        ? 'true'
        : 'false');
    }
    if (window.matchMedia(`(min-width: ${BREAKPOINT_LARGE})`).matches) {
      const buttons = this.hostElement.querySelectorAll('.digi-navigation-main-menu-panel__toggle-button');
      Array.from(buttons).forEach((button) => {
        if (button === target)
          return;
        button.setAttribute('aria-expanded', 'false');
        button.closest('li').removeAttribute('data-expanded');
      });
    }
  }
  hydrateListItems() {
    this.nestedListItems.forEach((nestedListItem, i) => {
      const id = `${this.hostElement.id}-subnav-${i}`;
      nestedListItem.setAttribute('id', id);
      const button = document.createElement('button');
      button.setAttribute('aria-expanded', nestedListItem.closest('[data-current="nested"]') ? 'true' : 'false');
      button.setAttribute('aria-controls', id);
      button.setAttribute('aria-label', 'Visa och dölj undermeny');
      button.classList.add('digi-navigation-main-menu-panel__toggle-button');
      button.innerHTML = `
				<digi-icon af-name="plus" aria-hidden="true"></digi-icon>
				<digi-icon af-name="minus" aria-hidden="true"></digi-icon>
				<digi-icon af-name="chevron-right" aria-hidden="true"></digi-icon>
				<digi-icon af-name="chevron-left" aria-hidden="true"></digi-icon>`;
      nestedListItem.parentNode.insertBefore(button, nestedListItem);
    });
  }
  keyUpHandler(e) {
    if (e.key !== 'Escape' ||
      window.matchMedia(`(min-width: ${BREAKPOINT_LARGE})`).matches)
      return;
    this.afOnClose.emit();
  }
  render() {
    return (h("digi-util-resize-observer", { onAfOnChange: (e) => this.resizeHandler(e.detail) }, h("digi-layout-container", null, h("div", { class: {
        'digi-navigation-main-menu-panel': true
      } }, h("div", { class: "digi-navigation-main-menu-panel__main-link", part: "main-link" }, h("slot", { name: "main-link" })), h("div", { class: "digi-navigation-main-menu-panel__sub-nav" }, h("slot", null)), h("div", { class: "digi-navigation-main-menu-panel__close-button-wrapper" }, h("digi-button", { afVariation: ButtonVariation.FUNCTION, class: "digi-navigation-main-menu-panel__close-button", onAfOnClick: (e) => this.closeHandler(e.detail), "aria-controls": this.hostElement.id, "aria-expanded": "true" }, h("digi-icon", { "data-viewport": "small", afName: 'chevron-left', "aria-hidden": "true", slot: "icon" }), h("digi-icon", { "data-viewport": "large", afName: 'x', "aria-hidden": "true", slot: "icon-secondary" }), h("span", { "data-viewport": "small" }, "Tillbaka"), h("span", { "data-viewport": "large" }, "St\u00E4ng")))))));
  }
  static get is() { return "digi-navigation-main-menu-panel"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["navigation-main-menu-panel.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["navigation-main-menu-panel.css"]
    };
  }
  static get states() {
    return {
      "nestedListItems": {}
    };
  }
  static get events() {
    return [{
        "method": "afOnResize",
        "name": "afOnResize",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "When the component changes size"
            }],
          "text": "N\u00E4r komponenten \u00E4ndrar storlek"
        },
        "complexType": {
          "original": "ResizeObserverEntry",
          "resolved": "ResizeObserverEntry",
          "references": {
            "ResizeObserverEntry": {
              "location": "global"
            }
          }
        }
      }, {
        "method": "afOnClose",
        "name": "afOnClose",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "When the component changes size"
            }],
          "text": "N\u00E4r komponenten st\u00E4ngs"
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }];
  }
  static get elementRef() { return "hostElement"; }
  static get watchers() {
    return [{
        "propName": "nestedListItems",
        "methodName": "nestedListItemsChanged"
      }];
  }
  static get listeners() {
    return [{
        "name": "click",
        "method": "toggleButtonClickHandler",
        "target": undefined,
        "capture": false,
        "passive": false
      }, {
        "name": "click",
        "method": "linkClickHandler",
        "target": undefined,
        "capture": false,
        "passive": false
      }, {
        "name": "keyup",
        "method": "keyUpHandler",
        "target": undefined,
        "capture": false,
        "passive": false
      }];
  }
}
