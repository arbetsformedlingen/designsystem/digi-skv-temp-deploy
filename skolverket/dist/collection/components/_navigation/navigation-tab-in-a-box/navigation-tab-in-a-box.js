import { h } from '@stencil/core';
import { CardBoxWidth } from '../../../enums-skolverket';
import { slugify } from '../../../global/utils/string';
/**
 * @slot default - Kan innehålla vad som helst
 * @swedishName Flik i en box
 */
export class NavigationTabInABox {
  constructor() {
    this.afAriaLabel = undefined;
    this.afId = undefined;
    this.afActive = undefined;
  }
  toggleHandler(activeTab) {
    if (activeTab || null) {
      this.afOnToggle.emit(activeTab);
    }
  }
  render() {
    return (h("digi-navigation-tab", { afAriaLabel: this.afAriaLabel, afId: this.afId || slugify(this.afAriaLabel), afActive: this.afActive, onAfOnToggle: (e) => this.afOnToggle.emit(e.detail) }, h("digi-card-box", { afWidth: CardBoxWidth.FULL }, h("digi-typography", null, h("slot", null)))));
  }
  static get is() { return "digi-navigation-tab-in-a-box"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["navigation-tab-in-a-box.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["navigation-tab-in-a-box.css"]
    };
  }
  static get properties() {
    return {
      "afAriaLabel": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": true,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set aria-label attribute"
            }],
          "text": "S\u00E4tter attributet 'aria-label'"
        },
        "attribute": "af-aria-label",
        "reflect": false
      },
      "afId": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set id attribute. Defaults to random string."
            }],
          "text": "S\u00E4tter attributet 'id'. Om inget v\u00E4ljs s\u00E5 skapas ett slumpm\u00E4ssigt id."
        },
        "attribute": "af-id",
        "reflect": false
      },
      "afActive": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Sets active tab (this is handled by digi-navigation-tab-in-a-boxs which should wrap this component)"
            }],
          "text": "S\u00E4tter aktiv tabb. Detta sk\u00F6ts av digi-navigation-tab-in-a-boxs som ska omsluta denna komponent."
        },
        "attribute": "af-active",
        "reflect": false
      }
    };
  }
  static get events() {
    return [{
        "method": "afOnToggle",
        "name": "afOnToggle",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "When the tab toggles between active and inactive"
            }],
          "text": "N\u00E4r tabben v\u00E4xlar mellan aktiv och inaktiv"
        },
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        }
      }];
  }
  static get watchers() {
    return [{
        "propName": "afActive",
        "methodName": "toggleHandler"
      }];
  }
}
