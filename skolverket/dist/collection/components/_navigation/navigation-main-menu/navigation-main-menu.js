import { h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { BREAKPOINT_LARGE } from '../../../design-tokens/web/tokens.es6';
/**
 * @slot default - Se översikten för exakt innehåll
 *
 *@swedishName Huvudmeny
 */
export class NavigationMainMenu {
  constructor() {
    this.menuButtons = [];
    this.isActive = false;
    this.afId = randomIdGenerator('digi-navigation-main-menu');
  }
  componentWillLoad() {
    this.menuButtons = Array.from(this.hostElement.querySelectorAll('nav > ul > li > button'));
    if (this.menuButtons.length) {
      this.menuButtons.forEach(this.hydrateButton.bind(this));
    }
  }
  hydrateButton(button, i) {
    var _a, _b;
    button.setAttribute('aria-expanded', 'false');
    button.setAttribute('aria-controls', `${this.afId}-submenu-${i}`);
    (_a = button.nextElementSibling) === null || _a === void 0 ? void 0 : _a.setAttribute('id', `${this.afId}-submenu-${i}`);
    (_b = button.nextElementSibling) === null || _b === void 0 ? void 0 : _b.addEventListener('afOnResize', (e) => {
      this.setPanelHeight(button.closest('li'), e.detail);
    });
    button.addEventListener('click', (e) => this.toggleMenu(e));
  }
  toggleMenu(e) {
    this.menuButtons.forEach((button) => {
      if (button !== e.target) {
        this.removeActive(button);
      }
    });
    e.target.setAttribute('aria-expanded', e.target.getAttribute('aria-expanded') === 'true' ? 'false' : 'true');
    e.target.closest('li').setAttribute('data-active', 'true');
    this.setMainLinkFocus(e.target);
    this.isActive = true;
  }
  setMainLinkFocus(target) {
    var _a, _b;
    if (window.matchMedia(`(min-width: ${BREAKPOINT_LARGE})`).matches)
      return;
    (_b = (_a = target.nextElementSibling) === null || _a === void 0 ? void 0 : _a.querySelector('a[slot="main-link"], [slot="main-link"] a')) === null || _b === void 0 ? void 0 : _b.focus();
  }
  setTopLevelButtonFocus(button) {
    var _a, _b;
    if (window.matchMedia(`(min-width: ${BREAKPOINT_LARGE})`).matches)
      return;
    (_b = (_a = button.nextElementSibling) === null || _a === void 0 ? void 0 : _a.querySelector('a[slot="main-link"], [slot="main-link"] a')) === null || _b === void 0 ? void 0 : _b.focus();
  }
  setPanelHeight(li, resizeObserverEntry) {
    li.style.setProperty('--panel-height', `${resizeObserverEntry.contentRect.height}px`);
  }
  removeActive(button) {
    button.setAttribute('aria-expanded', 'false');
    button.closest('li').removeAttribute('data-active');
    this.isActive = false;
  }
  closeSubmenuHandler(e) {
    this.closeSubmenu(e);
  }
  keyUpHandler(e) {
    if (e.key !== 'Escape' ||
      !window.matchMedia(`(min-width: ${BREAKPOINT_LARGE})`).matches)
      return;
    this.closeSubmenu(e);
  }
  closeSubmenu(e) {
    const button = this.hostElement.querySelector('[data-active="true"] button');
    if (!button)
      return;
    this.removeActive(button);
    if (e.detail.target.tagName !== 'A') {
      setTimeout(() => {
        button.focus();
      }, 10);
    }
  }
  get cssModifiers() {
    return {
      [`digi-navigation-main-menu--active-${this.isActive}`]: true
    };
  }
  render() {
    return (h("digi-layout-container", null, h("div", { class: Object.assign({ 'digi-navigation-main-menu': true }, this.cssModifiers) }, h("slot", null))));
  }
  static get is() { return "digi-navigation-main-menu"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["navigation-main-menu.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["navigation-main-menu.css"]
    };
  }
  static get properties() {
    return {
      "afId": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set id attribute. Defaults to random string."
            }],
          "text": "S\u00E4tter attributet 'id'. Om inget v\u00E4ljs s\u00E5 skapas ett slumpm\u00E4ssigt id."
        },
        "attribute": "af-id",
        "reflect": false,
        "defaultValue": "randomIdGenerator('digi-navigation-main-menu')"
      }
    };
  }
  static get states() {
    return {
      "menuButtons": {},
      "isActive": {}
    };
  }
  static get elementRef() { return "hostElement"; }
  static get listeners() {
    return [{
        "name": "afOnClose",
        "method": "closeSubmenuHandler",
        "target": undefined,
        "capture": false,
        "passive": false
      }, {
        "name": "keyup",
        "method": "keyUpHandler",
        "target": undefined,
        "capture": false,
        "passive": false
      }];
  }
}
