export var NavigationMainMenuVariation;
(function (NavigationMainMenuVariation) {
  NavigationMainMenuVariation["PRIMARY"] = "primary";
  NavigationMainMenuVariation["SECONDARY"] = "secondary";
})(NavigationMainMenuVariation || (NavigationMainMenuVariation = {}));
