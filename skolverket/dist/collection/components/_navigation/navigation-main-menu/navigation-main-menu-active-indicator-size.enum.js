export var NavigationMainMenuActiveIndicatorSize;
(function (NavigationMainMenuActiveIndicatorSize) {
  NavigationMainMenuActiveIndicatorSize["PRIMARY"] = "primary";
  NavigationMainMenuActiveIndicatorSize["SECONDARY"] = "secondary";
})(NavigationMainMenuActiveIndicatorSize || (NavigationMainMenuActiveIndicatorSize = {}));
