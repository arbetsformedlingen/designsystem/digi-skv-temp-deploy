import { h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { logger } from '../../../global/utils/logger';
/**
 * @slot default - Ska innehålla flera digi-navigation-tab-*
 * @swedishName Flikfält
 */
export class NavigationTabs {
  constructor() {
    this.tabPanels = [];
    this.activeTab = 0;
    this.currentTabIndex = this.activeTab;
    this.afAriaLabel = '';
    this.afInitActiveTab = 0;
    this.afId = randomIdGenerator('digi-navigation-tabs');
  }
  /**
   * Sätter om aktiv flik.
   * @en Sets the active tab.
   */
  async afMSetActiveTab(tabIndex) {
    this.setActiveTab(tabIndex);
  }
  changeHandler(activeTabIndex) {
    this.afOnChange.emit(activeTabIndex);
  }
  setTabFocus(id) {
    id = this.tabId(this.tabPanels[id].afId);
    if (document.getElementById(`${id}`)) {
      document.getElementById(`${id}`).focus();
    }
  }
  clickHandler(e, i) {
    this.setActiveTab(i);
    this.afOnClick.emit(e);
  }
  focusHandler(e) {
    const isActiveTab = this.tabPanels.findIndex((i) => this.tabId(i.afId) === document.activeElement.id) === this.activeTab;
    this.currentTabIndex = isActiveTab ? this.activeTab : this.currentTabIndex;
    this.afOnFocus.emit(e);
  }
  leftHandler() {
    this.decrementCurrentTabIndex();
  }
  rightHandler() {
    this.incrementCurrentTabIndex();
  }
  homeHandler() {
    this.setTabFocus('0');
  }
  endHandler() {
    this.setTabFocus(this.tabPanels.length - 1);
  }
  tabId(prefix) {
    return `${prefix}-tab`;
  }
  decrementCurrentTabIndex() {
    this.currentTabIndex > 0
      ? (this.currentTabIndex = this.currentTabIndex - 1)
      : (this.currentTabIndex = this.tabPanels.length - 1);
  }
  incrementCurrentTabIndex() {
    this.currentTabIndex < this.tabPanels.length - 1
      ? (this.currentTabIndex = this.currentTabIndex + 1)
      : (this.currentTabIndex = 0);
  }
  componentDidLoad() {
    this.getTabs();
  }
  getTabs(e = null) {
    let tablist;
    tablist = this.hostElement.querySelectorAll(`#${this.afId}-observer digi-navigation-tab`);
    if (!tablist) {
      logger.warn(`navigation-tabs tablist is empty, have you missed anything?`, this.hostElement);
      return;
    }
    this.tabPanels = [...tablist];
    let activeTabIndex = this.afInitActiveTab ? this.afInitActiveTab : 0;
    // If tabs are added or removed
    if (e) {
      if (!e.detail.addedNodes || !e.detail.removedNodes) {
        return;
      }
      const added = e.detail.addedNodes.item(0), removed = e.detail.removedNodes.item(0);
      activeTabIndex = this.currentTabIndex;
      if (added) {
        // If added tab is same position or before current tab, jump to the right
        Object.values(e.target.children).indexOf(added) <= this.currentTabIndex &&
          (activeTabIndex += 1);
      }
      else if (removed) {
        // If removed tab is before current tab or is last, jump to the left
        if ((removed.dataset.position == e.target.children.length &&
          removed.dataset.position == this.currentTabIndex) ||
          removed.dataset.position < this.currentTabIndex) {
          activeTabIndex -= 1;
        }
      }
    }
    this.setActiveTab(activeTabIndex);
  }
  setActiveTab(newTabIndex) {
    this.activeTab = newTabIndex;
    this.currentTabIndex = this.activeTab;
    this.tabPanels.forEach((tab, i) => {
      tab.setAttribute('af-active', i === newTabIndex);
      tab.setAttribute('data-position', i);
    });
  }
  render() {
    return (h("div", { class: "digi-navigation-tabs" }, h("digi-util-keydown-handler", { onAfOnLeft: () => this.leftHandler(), onAfOnRight: () => this.rightHandler(), onAfOnHome: () => this.homeHandler(), onAfOnEnd: () => this.endHandler() }, h("div", { class: "digi-navigation-tabs__tablist", role: "tablist", "aria-label": this.afAriaLabel }, this.tabPanels.map((tab, i) => {
      return (h("button", { class: "digi-navigation-tabs__tab", role: "tab", type: "button", "aria-selected": this.activeTab === i ? 'true' : null, "aria-controls": tab.afId, tabindex: this.activeTab !== i ? '-1' : null, id: `${this.tabId(tab.afId)}`, onClick: (e) => this.clickHandler(e, i), onFocus: (e) => this.focusHandler(e) }, h("span", { class: "digi-navigation-tabs__tab-text" }, tab.afAriaLabel)));
    }))), h("digi-util-mutation-observer", { onAfOnChange: (e) => this.getTabs(e), id: `${this.afId}-observer` }, h("slot", null))));
  }
  static get is() { return "digi-navigation-tabs"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["navigation-tabs.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["navigation-tabs.css"]
    };
  }
  static get properties() {
    return {
      "afAriaLabel": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set aria-label attribute on the tablist elementet."
            }],
          "text": "S\u00E4tter attributet 'aria-label' p\u00E5 tablistelementet."
        },
        "attribute": "af-aria-label",
        "reflect": false,
        "defaultValue": "''"
      },
      "afInitActiveTab": {
        "type": "number",
        "mutable": false,
        "complexType": {
          "original": "number",
          "resolved": "number",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set initial active tab index. Default to 0."
            }],
          "text": "S\u00E4tter initial aktiv tabb"
        },
        "attribute": "af-init-active-tab",
        "reflect": false,
        "defaultValue": "0"
      },
      "afId": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [],
          "text": "Input id attribute. Defaults to random string."
        },
        "attribute": "af-id",
        "reflect": false,
        "defaultValue": "randomIdGenerator('digi-navigation-tabs')"
      }
    };
  }
  static get states() {
    return {
      "tabPanels": {},
      "activeTab": {},
      "currentTabIndex": {}
    };
  }
  static get events() {
    return [{
        "method": "afOnChange",
        "name": "afOnChange",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [],
          "text": ""
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnClick",
        "name": "afOnClick",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [],
          "text": ""
        },
        "complexType": {
          "original": "MouseEvent",
          "resolved": "MouseEvent",
          "references": {
            "MouseEvent": {
              "location": "global"
            }
          }
        }
      }, {
        "method": "afOnFocus",
        "name": "afOnFocus",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [],
          "text": ""
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }];
  }
  static get methods() {
    return {
      "afMSetActiveTab": {
        "complexType": {
          "signature": "(tabIndex: number) => Promise<void>",
          "parameters": [{
              "tags": [],
              "text": ""
            }],
          "references": {
            "Promise": {
              "location": "global"
            }
          },
          "return": "Promise<void>"
        },
        "docs": {
          "text": "S\u00E4tter om aktiv flik.",
          "tags": [{
              "name": "en",
              "text": "Sets the active tab."
            }]
        }
      }
    };
  }
  static get elementRef() { return "hostElement"; }
  static get watchers() {
    return [{
        "propName": "activeTab",
        "methodName": "changeHandler"
      }, {
        "propName": "currentTabIndex",
        "methodName": "setTabFocus"
      }];
  }
}
