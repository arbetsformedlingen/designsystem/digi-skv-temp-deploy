import { h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { ButtonSize, ButtonVariation } from '../../../enums-core';
/**
 * @swedishName Paginering
 */
export class NavigationPagination {
  constructor() {
    this.pages = [];
    this.currentPage = undefined;
    this.afInitActivePage = 1;
    this.afTotalPages = undefined;
    this.afCurrentResultStart = undefined;
    this.afId = randomIdGenerator('digi-navigation-pagination');
    this.afCurrentResultEnd = undefined;
    this.afTotalResults = 0;
    this.afResultName = 'träffar';
  }
  afInitActivePageChanged() {
    this.setCurrentPage(this.afInitActivePage, false);
  }
  /**
   * Kan användas för att manuellt sätta om den aktiva sidan.
   * @en Can be used to set the active page.
   */
  async afMSetCurrentPage(pageNumber) {
    this.setCurrentPage(pageNumber, false);
  }
  prevPage() {
    this.currentPage--;
    this.afOnPageChange.emit(this.currentPage);
  }
  nextPage() {
    this.currentPage++;
    this.afOnPageChange.emit(this.currentPage);
  }
  setCurrentPage(pageToSet = this.currentPage, emitEvent = true) {
    if (pageToSet === this.currentPage) {
      return;
    }
    this.currentPage =
      pageToSet <= 1
        ? 1
        : pageToSet <= this.afTotalPages
          ? pageToSet
          : this.afTotalPages;
    console.log(pageToSet, this.currentPage);
    if (emitEvent) {
      this.afOnPageChange.emit(this.currentPage);
    }
  }
  componentWillLoad() {
    this.setCurrentPage(this.afInitActivePage, false);
    this.updateTotalPages();
  }
  updateTotalPages() {
    if (this.afTotalPages) {
      this.pages = [...Array(this.afTotalPages)];
    }
  }
  isCurrentPage(currentPage, page) {
    if (currentPage === page || null) {
      return 'page';
    }
  }
  labelledby() {
    if (this.afTotalResults > 0) {
      return `
        ${this.afId}-aria-label
        ${this.afId}-result-show
        ${this.afId}-result-current
        ${this.afId}-result-of
        ${this.afId}-result-total
        ${this.afId}-result-name
      `;
    }
    else {
      return `${this.afId}-aria-label`;
    }
  }
  render() {
    return (h("div", { class: "digi-navigation-pagination" }, h("span", { id: `${this.afId}-aria-label`, "aria-hidden": "true", class: "digi-navigation-pagination__aria-label" }, "Paginering"), this.afTotalResults > 0 && (h("div", { class: {
        'digi-navigation-pagination__result': true,
        'digi-navigation-pagination__result--pages': this.pages.length > 1
      }, "aria-hidden": this.pages.length > 1 ? 'true' : 'false', id: `${this.afId}-result` }, h("digi-typography", null, h("span", { id: `${this.afId}-result-show` }, "Visar "), h("strong", { id: `${this.afId}-result-current` }, this.afCurrentResultStart, "-", this.afCurrentResultEnd), h("span", { id: `${this.afId}-result-of` }, " av "), h("strong", { id: `${this.afId}-result-total` }, this.afTotalResults), this.afResultName && (h("span", { id: `${this.afId}-result-name` }, ` ${this.afResultName}`))))), this.pages.length > 1 && (h("nav", { "aria-labelledby": this.labelledby(), class: {
        'digi-navigation-pagination__pagination': true
      } }, h("div", { class: "digi-navigation-pagination__select" }, h("digi-form-select", { afLabel: `Sida`, afValue: `${this.currentPage}`, onAfOnChange: (e) => {
        this.setCurrentPage(parseInt(e.detail.target.value), true);
      } }, this.pages.map((_, index) => (h("option", { value: `${index + 1}`, selected: index + 1 === this.currentPage }, index + 1, " av ", this.afTotalPages))))), h("div", { class: "digi-navigation-pagination__buttons" }, h("digi-button", { onClick: () => this.prevPage(), afVariation: ButtonVariation.SECONDARY, afSize: ButtonSize.LARGE, afFullWidth: true, class: {
        'digi-navigation-pagination__button digi-navigation-pagination__button--previous': true,
        'digi-navigation-pagination__button--keep-left': this.currentPage === this.pages.length,
        'digi-navigation-pagination__button--hidden': this.currentPage === 1
      } }, h("digi-icon", { slot: "icon", afName: `chevron-left` }), h("span", null, "F\u00F6reg\u00E5ende")), h("digi-button", { onClick: () => this.nextPage(), afVariation: ButtonVariation.SECONDARY, afSize: ButtonSize.LARGE, afFullWidth: true, class: {
        'digi-navigation-pagination__button digi-navigation-pagination__button--next': true,
        'digi-navigation-pagination__button--keep-right': this.currentPage === 1,
        'digi-navigation-pagination__button--hidden': this.currentPage === this.afTotalPages
      }, "af-variation": "secondary" }, h("digi-icon", { slot: "icon-secondary", afName: `chevron-right` }), h("span", null, "N\u00E4sta")))))));
  }
  static get is() { return "digi-navigation-pagination"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["navigation-pagination.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["navigation-pagination.css"]
    };
  }
  static get properties() {
    return {
      "afInitActivePage": {
        "type": "number",
        "mutable": false,
        "complexType": {
          "original": "number",
          "resolved": "number",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set initial start page"
            }],
          "text": "S\u00E4tter initiala aktiva sidan"
        },
        "attribute": "af-init-active-page",
        "reflect": false,
        "defaultValue": "1"
      },
      "afTotalPages": {
        "type": "number",
        "mutable": false,
        "complexType": {
          "original": "number",
          "resolved": "number",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set total pages that exists"
            }],
          "text": "S\u00E4tter totala m\u00E4ngden sidor"
        },
        "attribute": "af-total-pages",
        "reflect": false
      },
      "afCurrentResultStart": {
        "type": "number",
        "mutable": false,
        "complexType": {
          "original": "number",
          "resolved": "number",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set start value of current results"
            }],
          "text": "S\u00E4tter startv\u00E4rdet f\u00F6r nuvarande resultat"
        },
        "attribute": "af-current-result-start",
        "reflect": false
      },
      "afId": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input id attribute. Defaults to random string."
            }],
          "text": "S\u00E4tter attributet 'id'. Om inget v\u00E4ljs s\u00E5 skapas ett slumpm\u00E4ssigt id."
        },
        "attribute": "af-id",
        "reflect": false,
        "defaultValue": "randomIdGenerator('digi-navigation-pagination')"
      },
      "afCurrentResultEnd": {
        "type": "number",
        "mutable": false,
        "complexType": {
          "original": "number",
          "resolved": "number",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set end value of current results"
            }],
          "text": "S\u00E4tter slutv\u00E4rdet f\u00F6r nuvarande resultat"
        },
        "attribute": "af-current-result-end",
        "reflect": false
      },
      "afTotalResults": {
        "type": "number",
        "mutable": false,
        "complexType": {
          "original": "number",
          "resolved": "number",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set total results"
            }],
          "text": "S\u00E4tter totala m\u00E4ngden resultat"
        },
        "attribute": "af-total-results",
        "reflect": false,
        "defaultValue": "0"
      },
      "afResultName": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set a result name. This is used to communicate what is being listed. For example, if the value is 'users', the component might say 'Showing 23-138 users'."
            }],
          "text": "S\u00E4tter ett resultatnamn. Det anv\u00E4nds f\u00F6r att kommunicera vad som listas. T.ex. om v\u00E4rdet \u00E4r 'anv\u00E4ndare' s\u00E5 kan det st\u00E5 'Visar 23-138 anv\u00E4ndare'."
        },
        "attribute": "af-result-name",
        "reflect": false,
        "defaultValue": "'tr\u00E4ffar'"
      }
    };
  }
  static get states() {
    return {
      "pages": {},
      "currentPage": {}
    };
  }
  static get events() {
    return [{
        "method": "afOnPageChange",
        "name": "afOnPageChange",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "When page changes"
            }],
          "text": "Vid byte av sida"
        },
        "complexType": {
          "original": "number",
          "resolved": "number",
          "references": {}
        }
      }];
  }
  static get methods() {
    return {
      "afMSetCurrentPage": {
        "complexType": {
          "signature": "(pageNumber: number) => Promise<void>",
          "parameters": [{
              "tags": [],
              "text": ""
            }],
          "references": {
            "Promise": {
              "location": "global"
            }
          },
          "return": "Promise<void>"
        },
        "docs": {
          "text": "Kan anv\u00E4ndas f\u00F6r att manuellt s\u00E4tta om den aktiva sidan.",
          "tags": [{
              "name": "en",
              "text": "Can be used to set the active page."
            }]
        }
      }
    };
  }
  static get watchers() {
    return [{
        "propName": "afInitActivePage",
        "methodName": "afInitActivePageChanged"
      }, {
        "propName": "afTotalPages",
        "methodName": "updateTotalPages"
      }];
  }
}
