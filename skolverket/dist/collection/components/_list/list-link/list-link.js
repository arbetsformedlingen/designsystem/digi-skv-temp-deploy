import { h } from '@stencil/core';
import { TypographyVariation } from '../../../enums-core';
import { ListLinkLayout } from './list-link-layout.enum';
import { ListLinkType } from './list-link-type.enum';
import { ListLinkVariation } from './list-link-variation.enum';
/**
 * @slot default - ska innehålla li-element med a-element inuti.
 * @slot heading - Rubrik
 *
 * @swedishName Länklista
 */
export class ListLink {
  constructor() {
    this.hasHeading = undefined;
    this.afType = ListLinkType.UNORDERED;
    this.afLayout = ListLinkLayout.BLOCK;
    this.afVariation = ListLinkVariation.REGULAR;
  }
  componentWillLoad() {
    this.setHasHeading();
  }
  componentWillUpdate() {
    this.setHasHeading();
  }
  setHasHeading() {
    this.hasHeading = !!this.hostElement.querySelector('[slot="heading"]');
  }
  get cssModifiers() {
    return {
      [`digi-list-link--type-${this.afType}`]: !!this.afType,
      [`digi-list-link--layout-${this.afLayout}`]: !!this.afLayout,
      [`digi-list-link--variation-${this.afVariation}`]: !!this.afVariation
    };
  }
  render() {
    return (h("digi-typography", { class: Object.assign({ 'digi-list-link': true }, this.cssModifiers), afVariation: TypographyVariation.SMALL }, h("div", { class: "digi-list-link__content" }, this.hasHeading && (h("div", { class: "digi-list-link__heading" }, h("slot", { name: "heading" }))), h(this.afType, { class: "digi-list-link__list" }, h("slot", null)))));
  }
  static get is() { return "digi-list-link"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["list-link.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["list-link.css"]
    };
  }
  static get properties() {
    return {
      "afType": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "ListLinkType",
          "resolved": "ListLinkType.ORDERED | ListLinkType.UNORDERED",
          "references": {
            "ListLinkType": {
              "location": "import",
              "path": "./list-link-type.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [],
          "text": ""
        },
        "attribute": "af-type",
        "reflect": false,
        "defaultValue": "ListLinkType.UNORDERED"
      },
      "afLayout": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "ListLinkLayout",
          "resolved": "ListLinkLayout.BLOCK | ListLinkLayout.INLINE",
          "references": {
            "ListLinkLayout": {
              "location": "import",
              "path": "./list-link-layout.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [],
          "text": ""
        },
        "attribute": "af-layout",
        "reflect": false,
        "defaultValue": "ListLinkLayout.BLOCK"
      },
      "afVariation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "ListLinkVariation",
          "resolved": "ListLinkVariation.COMPACT | ListLinkVariation.REGULAR",
          "references": {
            "ListLinkVariation": {
              "location": "import",
              "path": "./list-link-variation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [],
          "text": ""
        },
        "attribute": "af-variation",
        "reflect": false,
        "defaultValue": "ListLinkVariation.REGULAR"
      }
    };
  }
  static get states() {
    return {
      "hasHeading": {}
    };
  }
  static get elementRef() { return "hostElement"; }
}
