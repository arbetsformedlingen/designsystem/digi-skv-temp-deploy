export var ListLinkVariation;
(function (ListLinkVariation) {
  ListLinkVariation["REGULAR"] = "regular";
  ListLinkVariation["COMPACT"] = "compact";
})(ListLinkVariation || (ListLinkVariation = {}));
