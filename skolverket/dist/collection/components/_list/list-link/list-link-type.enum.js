export var ListLinkType;
(function (ListLinkType) {
  ListLinkType["UNORDERED"] = "ul";
  ListLinkType["ORDERED"] = "ol";
})(ListLinkType || (ListLinkType = {}));
