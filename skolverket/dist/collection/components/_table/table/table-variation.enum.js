export var TableVariation;
(function (TableVariation) {
  TableVariation["PRIMARY"] = "primary";
  TableVariation["SECONDARY"] = "secondary";
})(TableVariation || (TableVariation = {}));
