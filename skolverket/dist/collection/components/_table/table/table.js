import { h } from '@stencil/core';
import { CardBoxGutter } from '../../_card/card-box/card-box-gutter.enum';
import { TableVariation } from './table-variation.enum';
/**
 * @slot default - Ska innehålla ett table-element
 *
 * @swedishName Tabell
 */
export class Table {
  constructor() {
    this.afVariation = TableVariation.PRIMARY;
  }
  get cssModifiers() {
    return {
      [`digi-table--variation-${this.afVariation}`]: !!this.afVariation
    };
  }
  render() {
    return this.afVariation === TableVariation.SECONDARY ? (h("digi-card-box", { afGutter: CardBoxGutter.NONE }, h("div", { class: Object.assign({ 'digi-table': true }, this.cssModifiers) }, h("slot", null)))) : (h("div", { class: Object.assign({ 'digi-table': true }, this.cssModifiers) }, h("slot", null)));
  }
  static get is() { return "digi-table"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["table.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["table.css"]
    };
  }
  static get properties() {
    return {
      "afVariation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "TableVariation",
          "resolved": "TableVariation.PRIMARY | TableVariation.SECONDARY",
          "references": {
            "TableVariation": {
              "location": "import",
              "path": "./table-variation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [],
          "text": ""
        },
        "attribute": "af-variation",
        "reflect": false,
        "defaultValue": "TableVariation.PRIMARY"
      }
    };
  }
}
