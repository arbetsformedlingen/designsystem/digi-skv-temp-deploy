import { h } from '@stencil/core';
/**
 * @slot default - Ska vara en länk med en ikon och text inuti
 *
 * @swedishName Ikonlänk
 */
export class LinkIcon {
  render() {
    return (h("span", null, h("slot", null)));
  }
  static get is() { return "digi-link-icon"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["link-icon.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["link-icon.css"]
    };
  }
}
