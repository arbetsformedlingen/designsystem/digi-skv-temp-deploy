export var PageBlockCardsVariation;
(function (PageBlockCardsVariation) {
  PageBlockCardsVariation["START"] = "start";
  PageBlockCardsVariation["SUB"] = "sub";
  PageBlockCardsVariation["SECTION"] = "section";
  PageBlockCardsVariation["PROCESS"] = "process";
  PageBlockCardsVariation["ARTICLE"] = "article";
})(PageBlockCardsVariation || (PageBlockCardsVariation = {}));
