import { h } from '@stencil/core';
import { CardBoxWidth } from '../../../enums-skolverket';
/**
 * @slot default
 * @slot heading - Rubrik
 *
 * @swedishName Sidblock med kort
 */
export class PageBlockCards {
  constructor() {
    this.afVariation = undefined;
  }
  get cssModifiers() {
    return {
      [`digi-page-block-cards--variation-${this.afVariation}`]: !!this.afVariation
    };
  }
  render() {
    return (h("digi-layout-container", { class: Object.assign({ 'digi-page-block-cards': true }, this.cssModifiers) }, h("digi-card-box", { afWidth: CardBoxWidth.FULL }, h("div", { class: "digi-page-block-cards__inner" }, h("digi-typography-heading-section", null, h("slot", { name: "heading" })), h("digi-layout-stacked-blocks", null, h("slot", null))))));
  }
  static get is() { return "digi-page-block-cards"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["page-block-cards.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["page-block-cards.css"]
    };
  }
  static get assetsDirs() { return ["public"]; }
  static get properties() {
    return {
      "afVariation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "`${PageBlockCardsVariation}`",
          "resolved": "\"article\" | \"process\" | \"section\" | \"start\" | \"sub\"",
          "references": {
            "PageBlockCardsVariation": {
              "location": "import",
              "path": "./page-block-cards-variation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [],
          "text": ""
        },
        "attribute": "af-variation",
        "reflect": false
      }
    };
  }
}
