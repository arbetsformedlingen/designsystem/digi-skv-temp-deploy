import { h } from '@stencil/core';
import { CardBoxWidth } from '../../../enums-skolverket';
/**
 * @slot default
 * @slot heading - Rubrik
 *
 * @swedishName Sidblock med listor
 */
export class PageBlockLists {
  constructor() {
    this.afVariation = undefined;
  }
  get cssModifiers() {
    return {
      [`digi-page-block-lists--variation-${this.afVariation}`]: !!this.afVariation
    };
  }
  render() {
    return (h("digi-layout-container", { class: Object.assign({ 'digi-page-block-lists': true }, this.cssModifiers) }, h("digi-card-box", { afWidth: CardBoxWidth.FULL }, h("div", { class: "digi-page-block-lists__inner" }, h("digi-typography-heading-section", null, h("slot", { name: "heading" })), h("digi-layout-stacked-blocks", null, h("slot", null))))));
  }
  static get is() { return "digi-page-block-lists"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["page-block-lists.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["page-block-lists.css"]
    };
  }
  static get assetsDirs() { return ["public"]; }
  static get properties() {
    return {
      "afVariation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "`${PageBlockListsVariation}`",
          "resolved": "\"article\" | \"process\" | \"section\" | \"start\" | \"sub\"",
          "references": {
            "PageBlockListsVariation": {
              "location": "import",
              "path": "./page-block-lists-variation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [],
          "text": ""
        },
        "attribute": "af-variation",
        "reflect": false
      }
    };
  }
}
