export var PageBlockListsVariation;
(function (PageBlockListsVariation) {
  PageBlockListsVariation["START"] = "start";
  PageBlockListsVariation["SUB"] = "sub";
  PageBlockListsVariation["SECTION"] = "section";
  PageBlockListsVariation["PROCESS"] = "process";
  PageBlockListsVariation["ARTICLE"] = "article";
})(PageBlockListsVariation || (PageBlockListsVariation = {}));
