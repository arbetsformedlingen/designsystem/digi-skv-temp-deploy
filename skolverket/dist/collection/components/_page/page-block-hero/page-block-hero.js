import { h } from '@stencil/core';
/**
 * @slot default
 * @slot heading - Rubrik
 *
 * @swedishName Sidblock med hero
 */
export class PageBlockHero {
  constructor() {
    this.afVariation = undefined;
    this.afBackground = undefined;
    this.afBackgroundImage = undefined;
  }
  get cssModifiers() {
    return {
      [`digi-page-block-hero--variation-${this.afVariation}`]: !!this.afVariation,
      [`digi-page-block-hero--background-${this.afBackground}`]: !!this.afBackground,
      'digi-page-block-hero--background-image': !!this.afBackgroundImage
    };
  }
  render() {
    return (h("digi-layout-container", { class: Object.assign({ 'digi-page-block-hero': true }, this.cssModifiers), style: {
        '--digi--page-block-hero--background-image': this.afBackgroundImage
          ? `url('${this.afBackgroundImage}')`
          : null
      } }, h("digi-typography", null, h("div", { class: "digi-page-block-hero__inner" }, h("div", { class: "digi-page-block-hero__heading" }, h("slot", { name: "heading" })), h("div", { class: "digi-page-block-hero__content" }, h("slot", null))))));
  }
  static get is() { return "digi-page-block-hero"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["page-block-hero.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["page-block-hero.css"]
    };
  }
  static get assetsDirs() { return ["public"]; }
  static get properties() {
    return {
      "afVariation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "`${PageBlockHeroVariation}`",
          "resolved": "\"process\" | \"section\" | \"start\" | \"sub\"",
          "references": {
            "PageBlockHeroVariation": {
              "location": "import",
              "path": "./page-block-hero-variation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [],
          "text": ""
        },
        "attribute": "af-variation",
        "reflect": false
      },
      "afBackground": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "`${PageBlockHeroBackground}`",
          "resolved": "\"auto\" | \"transparent\"",
          "references": {
            "PageBlockHeroBackground": {
              "location": "import",
              "path": "./page-block-hero-background.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [],
          "text": ""
        },
        "attribute": "af-background",
        "reflect": false
      },
      "afBackgroundImage": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [],
          "text": ""
        },
        "attribute": "af-background-image",
        "reflect": false
      }
    };
  }
}
