export var PageBlockHeroVariation;
(function (PageBlockHeroVariation) {
  PageBlockHeroVariation["START"] = "start";
  PageBlockHeroVariation["SUB"] = "sub";
  PageBlockHeroVariation["SECTION"] = "section";
  PageBlockHeroVariation["PROCESS"] = "process";
})(PageBlockHeroVariation || (PageBlockHeroVariation = {}));
