export var PageFooterVariation;
(function (PageFooterVariation) {
  PageFooterVariation["PRIMARY"] = "primary";
  PageFooterVariation["SECONDARY"] = "secondary";
})(PageFooterVariation || (PageFooterVariation = {}));
