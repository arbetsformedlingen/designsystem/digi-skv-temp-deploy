import { h } from '@stencil/core';
import { PageFooterVariation } from './page-footer-variation.enum';
/**
 * @slot default - kan innehålla vad som helst
 * @slot top-first
 * @slot top
 * @slot bottom
 *
 * @swedishName Sidfot
 */
export class PageFooter {
  constructor() {
    this.afVariation = PageFooterVariation.PRIMARY;
  }
  get cssModifiers() {
    return {
      [`digi-page-footer--variation-${this.afVariation}`]: !!this.afVariation
    };
  }
  render() {
    return (h("digi-typography", { class: Object.assign({ 'digi-page-footer': true }, this.cssModifiers) }, h("digi-layout-grid", null, h("div", { class: "digi-page-footer__section digi-page-footer__section--top" }, h("div", { class: "digi-page-footer__top-first" }, h("slot", { name: "top-first" })), h("slot", { name: "top" })), h("div", { class: "digi-page-footer__section digi-page-footer__section--bottom" }, h("div", { class: "digi-page-footer__logo" }, h("digi-logo", { "af-title": "Skolverket", "af-desc": "Skolverket logo" })), h("div", { class: "digi-page-footer__bottom-end" }, h("slot", { name: "bottom" }))))));
  }
  static get is() { return "digi-page-footer"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["page-footer.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["page-footer.css"]
    };
  }
  static get properties() {
    return {
      "afVariation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "PageFooterVariation",
          "resolved": "PageFooterVariation.PRIMARY | PageFooterVariation.SECONDARY",
          "references": {
            "PageFooterVariation": {
              "location": "import",
              "path": "./page-footer-variation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [],
          "text": ""
        },
        "attribute": "af-variation",
        "reflect": false,
        "defaultValue": "PageFooterVariation.PRIMARY"
      }
    };
  }
}
