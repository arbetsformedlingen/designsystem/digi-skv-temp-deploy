import { h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { BREAKPOINT_LARGE } from '../../../design-tokens/web/tokens.es6';
/**
 * @slot default - kan innehålla vad som helst
 * @slot top - länk till mina sidor, tillbaka till skolvetket.se etc
 * @slot logo - ska innehålla en logotyp
 * @slot nav - ska innehålla en navigation
 *
 * @swedishName Sidhuvud
 */
export class PageHeader {
  constructor() {
    this.isExpanded = false;
    this.afId = randomIdGenerator('digi-page-header');
  }
  get cssModifiers() {
    return {
      [`digi-page-header--expanded-${this.isExpanded}`]: true
    };
  }
  keyUpHandler(e) {
    var _a;
    if (e.key !== 'Escape' ||
      window.matchMedia(`(min-width: ${BREAKPOINT_LARGE})`).matches ||
      !!this.hostElement.querySelector('.digi-navigation-main-menu--active-true'))
      return;
    this.isExpanded = false;
    (_a = this.toggleButton) === null || _a === void 0 ? void 0 : _a.focus();
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-page-header': true }, this.cssModifiers) }, h("digi-layout-container", { class: "digi-page-header__top", id: `${this.afId}-top` }, h("div", null, h("slot", { name: "top" }))), h("div", { class: "digi-page-header__main" }, h("div", { class: "digi-page-header__logo" }, h("slot", { name: "logo" })), h("div", { class: "digi-page-header__toggle-button" }, h("button", { "aria-controls": `${this.afId}-top ${this.afId}-eyebrow ${this.afId}-nav`, "aria-expanded": `${this.isExpanded}`, onClick: () => (this.isExpanded = !this.isExpanded), ref: (el) => (this.toggleButton = el) }, h("digi-icon", { afName: this.isExpanded ? 'x' : 'bars' }), h("span", null, this.isExpanded ? 'Stäng' : 'Meny'))), h("div", { class: "digi-page-header__eyebrow", id: `${this.afId}-eyebrow` }, h("slot", { name: "eyebrow" }))), h("div", { class: "digi-page-header__nav", id: `${this.afId}-nav` }, h("digi-navigation-main-menu", null, h("slot", { name: "nav" })))));
  }
  static get is() { return "digi-page-header"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["page-header.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["page-header.css"]
    };
  }
  static get properties() {
    return {
      "afId": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input id attribute. Defaults to random string."
            }],
          "text": "S\u00E4tter attributet 'id'. Om inget v\u00E4ljs s\u00E5 skapas ett slumpm\u00E4ssigt id."
        },
        "attribute": "af-id",
        "reflect": false,
        "defaultValue": "randomIdGenerator('digi-page-header')"
      }
    };
  }
  static get states() {
    return {
      "isExpanded": {}
    };
  }
  static get elementRef() { return "hostElement"; }
  static get listeners() {
    return [{
        "name": "keyup",
        "method": "keyUpHandler",
        "target": undefined,
        "capture": false,
        "passive": false
      }];
  }
}
