export var PageBackground;
(function (PageBackground) {
  PageBackground["STRIPED_1"] = "striped_1";
  PageBackground["STRIPED_2"] = "striped_2";
  PageBackground["STRIPED_3"] = "striped_3";
  PageBackground["SQUARE_1"] = "square_1";
  PageBackground["SQUARE_2"] = "square_2";
  PageBackground["SQUARE_3"] = "square_3";
  PageBackground["DOTTED_1"] = "dotted_1";
  PageBackground["DOTTED_2"] = "dotted_2";
  PageBackground["DOTTED_3"] = "dotted_3";
})(PageBackground || (PageBackground = {}));
