import { getAssetPath, h } from '@stencil/core';
/**
 * @slot default - kan innehålla vad som helst
 * @slot header - Sidhuvud
 * @slot footer - Sidfot
 *
 * @swedishName Sidmall
 */
export class Page {
  constructor() {
    this.afBackground = undefined;
  }
  render() {
    return (h("div", { class: "digi-page", style: {
        '--digi--page--background-image': this.afBackground
          ? `url('${getAssetPath(`./public/images/${this.afBackground}.svg`)}')`
          : null
      } }, h("div", { class: "digi-page__header" }, h("slot", { name: "header" })), h("div", { class: "digi-page__content" }, h("slot", null)), h("div", { class: "digi-page__footer" }, h("slot", { name: "footer" }))));
  }
  static get is() { return "digi-page"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["page.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["page.css"]
    };
  }
  static get assetsDirs() { return ["public"]; }
  static get properties() {
    return {
      "afBackground": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "`${PageBackground}`",
          "resolved": "\"dotted_1\" | \"dotted_2\" | \"dotted_3\" | \"square_1\" | \"square_2\" | \"square_3\" | \"striped_1\" | \"striped_2\" | \"striped_3\"",
          "references": {
            "PageBackground": {
              "location": "import",
              "path": "./page-background.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [],
          "text": ""
        },
        "attribute": "af-background",
        "reflect": false
      }
    };
  }
}
