import { h } from '@stencil/core';
/**
 * @slot default
 * @slot sidebar
 *
 * @swedishName Sidblock med sidebar
 */
export class PageBlockSidebar {
  constructor() {
    this.afVariation = undefined;
  }
  get cssModifiers() {
    return {
      [`digi-page-block-sidebar--variation-${this.afVariation}`]: !!this.afVariation
    };
  }
  render() {
    return (h("digi-layout-grid", { class: Object.assign({ 'digi-page-block-sidebar': true }, this.cssModifiers) }, h("div", { class: "digi-page-block-sidebar__sidebar" }, h("slot", { name: "sidebar" })), h("div", { class: "digi-page-block-sidebar__content" }, h("slot", null))));
  }
  static get is() { return "digi-page-block-sidebar"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["page-block-sidebar.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["page-block-sidebar.css"]
    };
  }
  static get assetsDirs() { return ["public"]; }
  static get properties() {
    return {
      "afVariation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "`${PageBlockSidebarVariation}`",
          "resolved": "\"article\" | \"process\" | \"section\" | \"start\" | \"sub\"",
          "references": {
            "PageBlockSidebarVariation": {
              "location": "import",
              "path": "./page-block-sidebar-variation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [],
          "text": ""
        },
        "attribute": "af-variation",
        "reflect": false
      }
    };
  }
}
