export var PageBlockSidebarVariation;
(function (PageBlockSidebarVariation) {
  PageBlockSidebarVariation["START"] = "start";
  PageBlockSidebarVariation["SUB"] = "sub";
  PageBlockSidebarVariation["SECTION"] = "section";
  PageBlockSidebarVariation["PROCESS"] = "process";
  PageBlockSidebarVariation["ARTICLE"] = "article";
})(PageBlockSidebarVariation || (PageBlockSidebarVariation = {}));
