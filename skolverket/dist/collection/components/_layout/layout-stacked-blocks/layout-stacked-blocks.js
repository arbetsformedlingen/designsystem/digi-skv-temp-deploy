import { h } from '@stencil/core';
import { LayoutStackedBlocksVariation } from './layout-stacked-blocks-variation.enum';
/**
 * @slot default - kan innehålla vad som helst
 *
 * @swedishName Staplande block
 */
export class LayoutStackedBlocks {
  constructor() {
    this.afVariation = LayoutStackedBlocksVariation.REGULAR;
  }
  render() {
    return (h("div", { class: {
        'digi-layout-stacked-blocks': true,
        [`digi-layout-stacked-blocks--variation-${this.afVariation}`]: !!this.afVariation
      } }, h("slot", null)));
  }
  static get is() { return "digi-layout-stacked-blocks"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["layout-stacked-blocks.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["layout-stacked-blocks.css"]
    };
  }
  static get properties() {
    return {
      "afVariation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "`${LayoutStackedBlocksVariation}`",
          "resolved": "\"enhanced\" | \"regular\"",
          "references": {
            "LayoutStackedBlocksVariation": {
              "location": "import",
              "path": "./layout-stacked-blocks-variation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [],
          "text": ""
        },
        "attribute": "af-variation",
        "reflect": false,
        "defaultValue": "LayoutStackedBlocksVariation.REGULAR"
      }
    };
  }
}
