export var LayoutStackedBlocksVariation;
(function (LayoutStackedBlocksVariation) {
  LayoutStackedBlocksVariation["REGULAR"] = "regular";
  LayoutStackedBlocksVariation["ENHANCED"] = "enhanced";
})(LayoutStackedBlocksVariation || (LayoutStackedBlocksVariation = {}));
