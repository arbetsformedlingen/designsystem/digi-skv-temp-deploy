import { h } from '@stencil/core';
/**
 * @slot default - kan innehålla vad som helst
 *
 * @swedishName Rader
 */
export class LayoutRows {
  render() {
    return h("slot", null);
  }
  static get is() { return "digi-layout-rows"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["layout-rows.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["layout-rows.css"]
    };
  }
}
