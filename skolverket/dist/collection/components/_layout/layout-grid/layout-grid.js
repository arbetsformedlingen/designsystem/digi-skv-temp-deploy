import { h } from '@stencil/core';
import { LayoutGridVerticalSpacing } from './layout-grid-vertical-spacing.enum';
/**
 * @slot default - kan innehålla vad som helst
 *
 * @swedishName Basgrid
 */
export class LayoutGrid {
  constructor() {
    this.afVerticalSpacing = LayoutGridVerticalSpacing.REGULAR;
  }
  get cssModifiers() {
    return {
      [`digi-layout-grid--vertical-spacing-${this.afVerticalSpacing}`]: !!this.afVerticalSpacing
    };
  }
  render() {
    return (h("digi-layout-container", null, h("div", { class: Object.assign({ 'digi-layout-grid': true }, this.cssModifiers) }, h("slot", null))));
  }
  static get is() { return "digi-layout-grid"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["layout-grid.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["layout-grid.css"]
    };
  }
  static get properties() {
    return {
      "afVerticalSpacing": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "`${LayoutGridVerticalSpacing}`",
          "resolved": "\"none\" | \"regular\"",
          "references": {
            "LayoutGridVerticalSpacing": {
              "location": "import",
              "path": "./layout-grid-vertical-spacing.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [],
          "text": ""
        },
        "attribute": "af-vertical-spacing",
        "reflect": false,
        "defaultValue": "LayoutGridVerticalSpacing.REGULAR"
      }
    };
  }
}
