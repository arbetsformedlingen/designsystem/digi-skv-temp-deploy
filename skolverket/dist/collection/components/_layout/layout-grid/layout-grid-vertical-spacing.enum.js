export var LayoutGridVerticalSpacing;
(function (LayoutGridVerticalSpacing) {
  LayoutGridVerticalSpacing["REGULAR"] = "regular";
  LayoutGridVerticalSpacing["NONE"] = "none";
})(LayoutGridVerticalSpacing || (LayoutGridVerticalSpacing = {}));
