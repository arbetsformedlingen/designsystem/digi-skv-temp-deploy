import { h } from '@stencil/core';
import { TypographyVariation } from '../../../enums-core';
/**
 * @slot default - kan innehålla vad som helst
 * @slot image - Bild
 * @slot link-heading - Ska innehålla en rubrik med länk inuti
 *
 * @swedishName Länkat kort
 */
export class CardLink {
  constructor() {
    this.hasImage = undefined;
  }
  componentWillLoad() {
    this.setHasImage();
  }
  componentWillUpdate() {
    this.setHasImage();
  }
  setHasImage() {
    this.hasImage = !!this.hostElement.querySelector('[slot="image"]');
  }
  get cssModifiers() {
    return {
      [`digi-card-link--image-${this.hasImage}`]: true
    };
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-card-link': true }, this.cssModifiers) }, this.hasImage && (h("div", { class: "digi-card-link__image" }, h("slot", { name: "image" }))), h("digi-typography", { afVariation: TypographyVariation.LARGE, class: "digi-card-link__content" }, h("div", { class: "digi-card-link__heading" }, h("slot", { name: "link-heading" })), h("div", { class: "digi-card-link__text" }, h("slot", null)))));
  }
  static get is() { return "digi-card-link"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["card-link.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["card-link.css"]
    };
  }
  static get states() {
    return {
      "hasImage": {}
    };
  }
  static get elementRef() { return "hostElement"; }
}
