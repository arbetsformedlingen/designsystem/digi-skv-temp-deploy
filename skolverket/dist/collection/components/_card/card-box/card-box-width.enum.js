export var CardBoxWidth;
(function (CardBoxWidth) {
  CardBoxWidth["REGULAR"] = "regular";
  CardBoxWidth["FULL"] = "full";
})(CardBoxWidth || (CardBoxWidth = {}));
