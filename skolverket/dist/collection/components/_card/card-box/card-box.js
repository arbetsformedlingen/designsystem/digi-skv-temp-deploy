import { h } from '@stencil/core';
import { CardBoxWidth } from './card-box-width.enum';
import { CardBoxGutter } from './card-box-gutter.enum';
/**
 * @slot default - kan innehålla vad som helst
 *
 * @swedishName Skuggad box
 */
export class CardBox {
  constructor() {
    this.afWidth = CardBoxWidth.REGULAR;
    this.afGutter = CardBoxGutter.REGULAR;
  }
  get cssModifiers() {
    return {
      [`digi-card-box--width-${this.afWidth}`]: !!this.afWidth,
      [`digi-card-box--gutter-${this.afGutter}`]: !!this.afGutter
    };
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-card-box': true }, this.cssModifiers) }, h("slot", null)));
  }
  static get is() { return "digi-card-box"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["card-box.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["card-box.css"]
    };
  }
  static get properties() {
    return {
      "afWidth": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "CardBoxWidth",
          "resolved": "CardBoxWidth.FULL | CardBoxWidth.REGULAR",
          "references": {
            "CardBoxWidth": {
              "location": "import",
              "path": "./card-box-width.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [],
          "text": ""
        },
        "attribute": "af-width",
        "reflect": false,
        "defaultValue": "CardBoxWidth.REGULAR"
      },
      "afGutter": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "CardBoxGutter",
          "resolved": "CardBoxGutter.NONE | CardBoxGutter.REGULAR",
          "references": {
            "CardBoxGutter": {
              "location": "import",
              "path": "./card-box-gutter.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [],
          "text": ""
        },
        "attribute": "af-gutter",
        "reflect": false,
        "defaultValue": "CardBoxGutter.REGULAR"
      }
    };
  }
}
