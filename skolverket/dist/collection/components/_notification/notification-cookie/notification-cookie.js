import { h, Host } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
/**
 * @slot default - Kan innehålla vad som helst
 * @slot link - Länk tiill exempel till läs-mer-om-cookies-sidan
 * @slot settings - Formulärselement för olika cookieinställningar
 *
 * @swedishName Cookiemeddelande
 */
export class NotificationCookie {
  constructor() {
    this.modalIsOpen = false;
    this.afBannerText = 'Vi använder kakor (cookies) för att webbplatsen ska fungera på ett så bra sätt som möjligt. Här kan du välja vilka cookies du vill godkänna.';
    this.afBannerHeadingText = 'Vi använder kakor';
    this.afModalHeadingText = 'Anpassa inställningar för kakor (cookies)';
    this.afRequiredCookiesText = 'Nödvändiga kakor gör att våra tjänster är säkra och fungerar som de ska. Därför går de inte att inaktivera.';
    this.afId = randomIdGenerator('digi-notification-cookie');
  }
  clickPrimaryButtonHandler() {
    this.afOnAcceptAllCookies.emit();
  }
  formSubmitHandler(e) {
    this.afOnSubmitSettings.emit(e);
  }
  openModal() {
    this.modalIsOpen = true;
  }
  closeModal() {
    this.modalIsOpen = false;
  }
  render() {
    return (h(Host, null, h("digi-notification-alert", { class: "digi-notification-cookie__alert" }, h("h2", { slot: "heading" }, this.afBannerHeadingText), h("div", { class: "digi-notification-cookie__content" }, h("p", null, this.afBannerText)), h("div", { class: "digi-notification-cookie__link", slot: "link" }, h("slot", { name: "link" })), h("div", { class: "digi-notification-cookie__actions", slot: "actions" }, h("digi-button", { afType: "button", afFullWidth: true, onAfOnClick: () => this.clickPrimaryButtonHandler() }, "Godk\u00E4nn alla kakor"), h("digi-button", { afType: "button", afFullWidth: true, afVariation: "secondary", onAfOnClick: () => this.openModal() }, "Inst\u00E4llningar"))), h("digi-dialog", { afOpen: this.modalIsOpen, onAfOnClose: () => this.closeModal() }, h("h2", { slot: "heading" }, this.afModalHeadingText), h("p", null, this.afRequiredCookiesText), h("form", { method: "dialog", id: `${this.afId}-form`, onSubmit: (e) => this.formSubmitHandler(e) }, h("digi-form-fieldset", { class: "digi-notification-cookie__form-controls", afName: "cookie-settings", afId: `${this.afId}-cookie-settings` }, h("slot", { name: "settings" }))), h("digi-button", { afType: "button", afVariation: "secondary", onAfOnClick: () => this.closeModal(), slot: "actions" }, "Avbryt"), h("digi-button", { afType: "submit", afForm: `${this.afId}-form`, slot: "actions" }, "Spara och godk\u00E4nn"))));
  }
  static get is() { return "digi-notification-cookie"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["notification-cookie.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["notification-cookie.css"]
    };
  }
  static get properties() {
    return {
      "afBannerText": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [],
          "text": ""
        },
        "attribute": "af-banner-text",
        "reflect": false,
        "defaultValue": "'Vi anv\u00E4nder kakor (cookies) f\u00F6r att webbplatsen ska fungera p\u00E5 ett s\u00E5 bra s\u00E4tt som m\u00F6jligt. H\u00E4r kan du v\u00E4lja vilka cookies du vill godk\u00E4nna.'"
      },
      "afBannerHeadingText": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [],
          "text": ""
        },
        "attribute": "af-banner-heading-text",
        "reflect": false,
        "defaultValue": "'Vi anv\u00E4nder kakor'"
      },
      "afModalHeadingText": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [],
          "text": ""
        },
        "attribute": "af-modal-heading-text",
        "reflect": false,
        "defaultValue": "'Anpassa inst\u00E4llningar f\u00F6r kakor (cookies)'"
      },
      "afRequiredCookiesText": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [],
          "text": ""
        },
        "attribute": "af-required-cookies-text",
        "reflect": false,
        "defaultValue": "'N\u00F6dv\u00E4ndiga kakor g\u00F6r att v\u00E5ra tj\u00E4nster \u00E4r s\u00E4kra och fungerar som de ska. D\u00E4rf\u00F6r g\u00E5r de inte att inaktivera.'"
      },
      "afId": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Component id attribute. Defaults to random string."
            }],
          "text": "S\u00E4tter attributet 'id'. Om inget v\u00E4ljs s\u00E5 skapas ett slumpm\u00E4ssigt id."
        },
        "attribute": "af-id",
        "reflect": false,
        "defaultValue": "randomIdGenerator('digi-notification-cookie')"
      }
    };
  }
  static get states() {
    return {
      "modalIsOpen": {}
    };
  }
  static get events() {
    return [{
        "method": "afOnAcceptAllCookies",
        "name": "afOnAcceptAllCookies",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "When the user accepts all cookies"
            }],
          "text": "N\u00E4r anv\u00E4ndaren godk\u00E4nner alla kakor"
        },
        "complexType": {
          "original": "MouseEvent",
          "resolved": "MouseEvent",
          "references": {
            "MouseEvent": {
              "location": "global"
            }
          }
        }
      }, {
        "method": "afOnSubmitSettings",
        "name": "afOnSubmitSettings",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "When the user accepts all cookies"
            }],
          "text": "N\u00E4r anv\u00E4ndaren godk\u00E4nner alla kakor"
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }];
  }
}
