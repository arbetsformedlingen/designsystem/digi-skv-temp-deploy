import { h } from '@stencil/core';
import { NotificationDetailVariation } from './notification-detail-variation.enum';
import { TypographyVariation } from '../../../enums-core';
/**
 * @slot default - Kan innehålla vad som helst
 * @slot heading - Ska innehålla en rubrik
 *
 * @swedishName Detaljmeddelande
 */
export class NotificationDetail {
  constructor() {
    this.afVariation = NotificationDetailVariation.INFO;
  }
  get cssModifiers() {
    return {
      [`digi-notification-detail--variation-${this.afVariation}`]: !!this.afVariation
    };
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-notification-detail': true }, this.cssModifiers) }, h("div", { class: "digi-notification-detail__inner" }, h("div", { class: "digi-notification-detail__icon" }, h("digi-icon", { afName: `notification-${this.afVariation}`, "aria-hidden": "true" })), h("div", { class: "digi-notification-detail__content" }, h("slot", { name: "heading" }), h("digi-typography", { class: "digi-notification-detail__text", "af-variation": TypographyVariation.SMALL }, h("slot", null))))));
  }
  static get is() { return "digi-notification-detail"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["notification-detail.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["notification-detail.css"]
    };
  }
  static get properties() {
    return {
      "afVariation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "`${NotificationDetailVariation}`",
          "resolved": "\"danger\" | \"info\" | \"warning\"",
          "references": {
            "NotificationDetailVariation": {
              "location": "import",
              "path": "./notification-detail-variation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set the variation."
            }],
          "text": "S\u00E4tter variant."
        },
        "attribute": "af-variation",
        "reflect": false,
        "defaultValue": "NotificationDetailVariation.INFO"
      }
    };
  }
}
