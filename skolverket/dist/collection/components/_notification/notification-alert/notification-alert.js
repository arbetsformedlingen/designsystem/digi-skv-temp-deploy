import { h } from '@stencil/core';
import { NotificationAlertVariation } from './notification-alert-variation.enum';
import { ButtonVariation, TypographyVariation } from '../../../enums-core';
/**
 * @slot default - Kan innehålla vad som helst
 * @slot heading - Rubrik
 * @slot link - Länk
 * @slot actions - Knappar
 *
 * @swedishName Informationsmeddelande
 */
export class NotificationAlert {
  constructor() {
    this.afVariation = NotificationAlertVariation.INFO;
    this.afCloseable = false;
  }
  clickHandler(e) {
    this.afOnClose.emit(e);
  }
  get cssModifiers() {
    return {
      [`digi-notification-alert--variation-${this.afVariation}`]: !!this.afVariation,
      'digi-notification-alert--closeable': this.afCloseable
    };
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-notification-alert': true }, this.cssModifiers) }, h("div", { class: "digi-notification-alert__inner" }, h("div", { class: "digi-notification-alert__icon" }, h("digi-icon", { afName: `notification-${this.afVariation}`, "aria-hidden": "true" })), this.afCloseable && (h("digi-button", { "af-variation": ButtonVariation.FUNCTION, onAfOnClick: (e) => this.clickHandler(e), class: "digi-notification-alert__close-button" }, h("span", { class: "digi-notification-alert__close-button__text" }, "St\u00E4ng"), h("digi-icon", { afName: `close`, slot: "icon-secondary", "aria-hidden": "true" }))), h("div", { class: "digi-notification-alert__content" }, h("slot", { name: "heading" }), h("digi-typography", { class: "digi-notification-alert__text", "af-variation": TypographyVariation.SMALL }, h("slot", null)), h("slot", { name: "link" })), h("div", { class: "digi-notification-alert__actions" }, h("slot", { name: "actions" })))));
  }
  static get is() { return "digi-notification-alert"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["notification-alert.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["notification-alert.css"]
    };
  }
  static get properties() {
    return {
      "afVariation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "`${NotificationAlertVariation}`",
          "resolved": "\"danger\" | \"info\" | \"warning\"",
          "references": {
            "NotificationAlertVariation": {
              "location": "import",
              "path": "./notification-alert-variation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set variation of alert notification. Can be 'info', 'success', 'warning' or 'danger'."
            }],
          "text": "S\u00E4tter variant."
        },
        "attribute": "af-variation",
        "reflect": false,
        "defaultValue": "NotificationAlertVariation.INFO"
      },
      "afCloseable": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "If true, the close button is added to the component"
            }],
          "text": "G\u00F6r det m\u00F6jligt att st\u00E4nga meddelandet med en st\u00E4ngknapp"
        },
        "attribute": "af-closeable",
        "reflect": false,
        "defaultValue": "false"
      }
    };
  }
  static get events() {
    return [{
        "method": "afOnClose",
        "name": "afOnClose",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Close button's 'onclick' event"
            }],
          "text": "St\u00E4ngknappens 'onclick'-event"
        },
        "complexType": {
          "original": "MouseEvent",
          "resolved": "MouseEvent",
          "references": {
            "MouseEvent": {
              "location": "global"
            }
          }
        }
      }];
  }
}
