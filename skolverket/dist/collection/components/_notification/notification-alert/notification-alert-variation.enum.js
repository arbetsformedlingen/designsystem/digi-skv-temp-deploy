export var NotificationAlertVariation;
(function (NotificationAlertVariation) {
  NotificationAlertVariation["INFO"] = "info";
  NotificationAlertVariation["WARNING"] = "warning";
  NotificationAlertVariation["DANGER"] = "danger";
})(NotificationAlertVariation || (NotificationAlertVariation = {}));
