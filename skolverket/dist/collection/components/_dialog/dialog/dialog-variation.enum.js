export var DialogVariation;
(function (DialogVariation) {
  DialogVariation["PRIMARY"] = "primary";
  DialogVariation["SECONDARY"] = "secondary";
})(DialogVariation || (DialogVariation = {}));
