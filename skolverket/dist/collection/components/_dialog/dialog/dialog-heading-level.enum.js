export var DialogHeadingLevel;
(function (DialogHeadingLevel) {
  DialogHeadingLevel["H1"] = "h1";
  DialogHeadingLevel["H2"] = "h2";
  DialogHeadingLevel["H3"] = "h3";
  DialogHeadingLevel["H4"] = "h4";
  DialogHeadingLevel["H5"] = "h5";
  DialogHeadingLevel["H6"] = "h6";
})(DialogHeadingLevel || (DialogHeadingLevel = {}));
