export var DialogSize;
(function (DialogSize) {
  DialogSize["SMALL"] = "small";
  DialogSize["MEDIUM"] = "medium";
  DialogSize["LARGE"] = "large";
})(DialogSize || (DialogSize = {}));
