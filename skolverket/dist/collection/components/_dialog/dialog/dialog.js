import { h } from '@stencil/core';
import { CardBoxGutter } from '../../../enums-skolverket';
/**
 * @slot default - Kan innehålla vad som helst
 * @slot heading - Rubrik
 * @slot actions - Knappar
 *
 * @swedishName Modal
 */
export class Dialog {
  constructor() {
    this.afOpen = false;
    this.afHideCloseButton = false;
  }
  afOpenChanged(newVal, oldVal) {
    if (newVal === oldVal)
      return;
    this.afOpen ? this.showModal() : this.close();
    this.toggleEmitter(this.afOpen ? 'open' : 'close');
  }
  async showModal() {
    this._dialog.showModal();
    this.afOpen = true;
    this.toggleEmitter('open');
  }
  async close() {
    this._dialog.close();
    this.afOpen = false;
    this.toggleEmitter('close');
  }
  toggleEmitter(state) {
    state === 'open' ? this.afOnOpen.emit() : this.afOnClose.emit();
  }
  clickOutsideHandler(e) {
    if (e.detail.target !== this._dialog)
      return;
    this.close();
  }
  componentDidLoad() {
    this.afOpen && this.showModal();
  }
  render() {
    return (h("dialog", { class: "digi-dialog", ref: (el) => {
        this._dialog = el;
      }, onClose: () => this.close() }, h("digi-util-detect-click-outside", { onAfOnClickOutside: (e) => this.clickOutsideHandler(e) }, h("digi-card-box", { afGutter: CardBoxGutter.NONE }, h("digi-typography", null, h("div", { class: "digi-dialog__inner" }, h("header", { class: "digi-dialog__header" }, h("slot", { name: "heading" })), h("div", { class: "digi-dialog__content" }, h("slot", null)), h("div", { class: "digi-dialog__actions" }, h("slot", { name: "actions" })), !this.afHideCloseButton && (h("div", { class: "digi-dialog__close-button-wrapper" }, h("button", { class: "digi-dialog__close-button", onClick: () => this.close(), type: "button" }, "St\u00E4ng ", h("digi-icon", { "aria-hidden": "true", afName: `x` }))))))))));
  }
  static get is() { return "digi-dialog"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["dialog.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["dialog.css"]
    };
  }
  static get properties() {
    return {
      "afOpen": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [],
          "text": ""
        },
        "attribute": "af-open",
        "reflect": false,
        "defaultValue": "false"
      },
      "afHideCloseButton": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [],
          "text": ""
        },
        "attribute": "af-hide-close-button",
        "reflect": false,
        "defaultValue": "false"
      }
    };
  }
  static get events() {
    return [{
        "method": "afOnOpen",
        "name": "afOnOpen",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [],
          "text": ""
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnClose",
        "name": "afOnClose",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [],
          "text": ""
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }];
  }
  static get methods() {
    return {
      "showModal": {
        "complexType": {
          "signature": "() => Promise<void>",
          "parameters": [],
          "references": {
            "Promise": {
              "location": "global"
            }
          },
          "return": "Promise<void>"
        },
        "docs": {
          "text": "",
          "tags": []
        }
      },
      "close": {
        "complexType": {
          "signature": "() => Promise<void>",
          "parameters": [],
          "references": {
            "Promise": {
              "location": "global"
            }
          },
          "return": "Promise<void>"
        },
        "docs": {
          "text": "",
          "tags": []
        }
      }
    };
  }
  static get watchers() {
    return [{
        "propName": "afOpen",
        "methodName": "afOpenChanged"
      }];
  }
}
