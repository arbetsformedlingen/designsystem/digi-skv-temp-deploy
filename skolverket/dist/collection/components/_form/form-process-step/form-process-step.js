import { h } from '@stencil/core';
/**
 * @slot default - Ska innehålla flera <form-process-step>
 *
 * @swedishName Processteg enskilt steg
 */
export class FormProcessStep {
  constructor() {
    this.afHref = undefined;
    this.afType = 'upcoming';
    this.afContext = 'regular';
    this.afLabel = undefined;
  }
  clickHandler(e) {
    this.afClick.emit(e);
  }
  get cssModifiers() {
    return {
      [`digi-form-process-step--type-${this.afType}`]: true,
      [`digi-form-process-step--context-${this.afContext}`]: true
    };
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-form-process-step': true }, this.cssModifiers) }, this.afType !== 'completed' ? (h("p", { class: "digi-form-process-step__control", "aria-current": this.afType === 'current' ? 'step' : null, "data-label": this.afLabel }, h("span", null, this.afLabel))) : this.afHref ? (h("a", { class: "digi-form-process-step__control", href: this.afHref, onClick: (e) => this.clickHandler(e) }, h("span", null, this.afLabel))) : (h("button", { class: "digi-form-process-step__control", type: "button", onClick: (e) => this.clickHandler(e) }, h("span", null, this.afLabel)))));
  }
  static get is() { return "digi-form-process-step"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["form-process-step.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["form-process-step.css"]
    };
  }
  static get properties() {
    return {
      "afHref": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [],
          "text": ""
        },
        "attribute": "af-href",
        "reflect": false
      },
      "afType": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "'current' | 'completed' | 'upcoming'",
          "resolved": "\"completed\" | \"current\" | \"upcoming\"",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [],
          "text": ""
        },
        "attribute": "af-type",
        "reflect": false,
        "defaultValue": "'upcoming'"
      },
      "afContext": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "'regular' | 'fallback'",
          "resolved": "\"fallback\" | \"regular\"",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [],
          "text": ""
        },
        "attribute": "af-context",
        "reflect": false,
        "defaultValue": "'regular'"
      },
      "afLabel": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": true,
        "optional": false,
        "docs": {
          "tags": [],
          "text": ""
        },
        "attribute": "af-label",
        "reflect": false
      }
    };
  }
  static get events() {
    return [{
        "method": "afClick",
        "name": "afClick",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [],
          "text": ""
        },
        "complexType": {
          "original": "MouseEvent",
          "resolved": "MouseEvent",
          "references": {
            "MouseEvent": {
              "location": "global"
            }
          }
        }
      }];
  }
}
