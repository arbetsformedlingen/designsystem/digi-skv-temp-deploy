import { h } from '@stencil/core';
import { ButtonType, ButtonVariation } from '../../../enums-core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
// Based on the step-by-step indicator here: https://www.w3.org/WAI/tutorials/forms/multi-page/
/**
 * @slot default - Ska innehålla flera <li><form-process-step></form-process-step></li>
 *
 * @swedishName Processteg
 */
export class FormProcessSteps {
  constructor() {
    this.isFallback = false;
    this.isExpanded = false;
    this.fallbackIsSet = false;
    this.listWidth = undefined;
    this.containerWidth = undefined;
    this.steps = undefined;
    this.afCurrentStep = undefined;
    this.afId = randomIdGenerator('digi-form-process-steps');
  }
  handleWidthChange() {
    this.isFallback = this.listWidth > this.containerWidth;
    if (!this.fallbackIsSet) {
      this.fallbackIsSet = true;
    }
  }
  componentWillLoad() {
    this.setTypeOnChildren();
  }
  componentDidLoad() {
    this.measureItemsList();
  }
  componentWillUpdate() {
    this.setTypeOnChildren();
  }
  handleFocusWithin() {
    this.isExpanded = true;
  }
  measureItemsList() {
    const itemsList = this.hostElement.querySelector('ol');
    this.listWidth = itemsList.scrollWidth;
  }
  setTypeOnChildren() {
    const steps = this.hostElement.querySelectorAll('digi-form-process-step');
    this.steps = steps;
    steps.forEach((step, i) => {
      if (i + 1 === this.afCurrentStep) {
        step.afType = 'current';
      }
      else if (i + 1 < this.afCurrentStep) {
        step.afType = 'completed';
      }
      else {
        step.afType = 'upcoming';
      }
    });
  }
  setContextOnChildren() {
    const steps = this.hostElement.querySelectorAll('digi-form-process-step');
    steps.forEach((step) => (step.afContext = this.isFallback ? 'fallback' : 'regular'));
  }
  resizeHandler() {
    this.containerWidth = this._contentElement.getBoundingClientRect().width;
  }
  clickToggleHandler(e, resetFocus = false) {
    e.preventDefault();
    this.isExpanded = !this.isExpanded;
    if (resetFocus) {
      this._button.focus();
    }
  }
  get cssModifiers() {
    return {
      [`digi-form-process-steps--expanded-${this.isExpanded}`]: true,
      [`digi-form-process-steps--fallback-${this.isFallback}`]: true,
      [`digi-form-process-steps--fallback-is-set`]: this.fallbackIsSet
    };
  }
  render() {
    var _a;
    return (h("digi-util-resize-observer", { onAfOnChange: () => this.resizeHandler() }, h("div", { class: Object.assign({ 'digi-form-process-steps': true }, this.cssModifiers) }, this.isFallback && (h("button", { class: "digi-form-process-steps__toggle", type: "button", "aria-pressed": this.isExpanded ? 'true' : 'false', "aria-expanded": this.isExpanded ? 'true' : 'false', "aria-controls": `${this.afId}-content`, onClick: (e) => this.clickToggleHandler(e, true), ref: (el) => (this._button = el) }, h("div", { class: "digi-form-process-steps__toggle-heading", "data-current-step": this.afCurrentStep }, h("div", { class: "digi-form-process-steps__toggle-text" }, h("span", null, "Steg ", this.afCurrentStep, " av ", this.steps.length), h("p", null, (_a = this.steps[this.afCurrentStep - 1]) === null || _a === void 0 ? void 0 : _a.textContent))), h("span", { class: "digi-form-process-steps__toggle-label" }, this.isExpanded ? 'Dölj steg' : 'Visa alla steg', h("digi-icon", { afName: this.isExpanded ? 'chevron-up' : 'chevron-down', "aria-hidden": true, slot: "icon-secondary" })))), h("div", { class: "digi-form-process-steps__content", ref: (el) => (this._contentElement = el) }, h("ol", { class: "digi-form-process-steps__items", id: `${this.afId}-items`, onFocusin: () => this.handleFocusWithin() }, h("slot", null)), this.isFallback && (h("digi-button", { class: "digi-form-process-steps__toggle-inside", afVariation: ButtonVariation.FUNCTION, afType: ButtonType.BUTTON, onAfOnClick: (e) => this.clickToggleHandler(e.detail, true), afFullWidth: true, afAriaPressed: this.isExpanded, afAriaExpanded: this.isExpanded, afAriaControls: `${this.afId}-content` }, this.isExpanded ? 'Dölj' : 'Visa alla steg', h("digi-icon", { afName: this.isExpanded ? 'chevron-up' : 'chevron-down', "aria-hidden": true, slot: "icon-secondary" })))))));
  }
  static get is() { return "digi-form-process-steps"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["form-process-steps.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["form-process-steps.css"]
    };
  }
  static get properties() {
    return {
      "afCurrentStep": {
        "type": "number",
        "mutable": false,
        "complexType": {
          "original": "number",
          "resolved": "number",
          "references": {}
        },
        "required": true,
        "optional": false,
        "docs": {
          "tags": [],
          "text": ""
        },
        "attribute": "af-current-step",
        "reflect": false
      },
      "afId": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set id attribute. Defaults to random string."
            }],
          "text": "S\u00E4tter attributet 'id'. Om inget v\u00E4ljs s\u00E5 skapas ett slumpm\u00E4ssigt id."
        },
        "attribute": "af-id",
        "reflect": false,
        "defaultValue": "randomIdGenerator('digi-form-process-steps')"
      }
    };
  }
  static get states() {
    return {
      "isFallback": {},
      "isExpanded": {},
      "fallbackIsSet": {},
      "listWidth": {},
      "containerWidth": {},
      "steps": {}
    };
  }
  static get elementRef() { return "hostElement"; }
  static get watchers() {
    return [{
        "propName": "listWidth",
        "methodName": "handleWidthChange"
      }, {
        "propName": "containerWidth",
        "methodName": "handleWidthChange"
      }, {
        "propName": "afCurrentStep",
        "methodName": "setTypeOnChildren"
      }, {
        "propName": "isFallback",
        "methodName": "setContextOnChildren"
      }];
  }
}
