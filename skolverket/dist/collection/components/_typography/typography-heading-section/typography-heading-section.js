import { h } from '@stencil/core';
/**
 * @slot default - ska innehålla ett rubrikelement
 *
 * @swedishName Avsnittsrubrik
 */
export class TypographyHeadingSection {
  render() {
    return (h("span", null, h("slot", null)));
  }
  static get is() { return "digi-typography-heading-section"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["typography-heading-section.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["typography-heading-section.css"]
    };
  }
}
