export const getCSSModifiersFromProps = (el, propBaseName) => {
  if (typeof propBaseName !== 'string') {
    return propBaseName.reduce((acc, curr) => {
      return Object.assign(Object.assign({}, acc), getCSSModifiersFromProps(el, curr));
    }, {});
  }
  else {
    const propMatch = Array.from(el.attributes).find((i) => {
      return i.nodeName.startsWith(`af-${propBaseName}`);
    });
    return {
      [`${el.nodeName.toLowerCase()}--${propBaseName}-${propMatch === null || propMatch === void 0 ? void 0 : propMatch.value}`]: !!propMatch
    };
  }
};
