import { detectClosest } from '../detectClosest';
export function detectFocusOutside(target, selector) {
  return !detectClosest(target, selector);
}
