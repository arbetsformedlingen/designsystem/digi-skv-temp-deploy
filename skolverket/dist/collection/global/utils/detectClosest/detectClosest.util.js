export function detectClosest(target, selector) {
  return !!target.closest(selector);
}
