import { detectClosest } from '../detectClosest';
export function detectClickOutside(target, selector) {
  return !detectClosest(target, selector);
}
