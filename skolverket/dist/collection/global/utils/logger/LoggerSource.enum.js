export var LoggerSource;
(function (LoggerSource) {
  LoggerSource["CORE"] = "@af/digi-core";
  LoggerSource["NG"] = "@af/digi-ng";
})(LoggerSource || (LoggerSource = {}));
