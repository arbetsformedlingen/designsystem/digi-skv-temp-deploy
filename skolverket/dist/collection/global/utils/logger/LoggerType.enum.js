export var LoggerType;
(function (LoggerType) {
  LoggerType["LOG"] = "log";
  LoggerType["INFO"] = "info";
  LoggerType["WARN"] = "warn";
  LoggerType["ERROR"] = "error";
})(LoggerType || (LoggerType = {}));
