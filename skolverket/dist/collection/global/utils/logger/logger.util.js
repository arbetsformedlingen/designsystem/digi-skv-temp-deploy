import { LoggerType } from './LoggerType.enum';
import { LoggerSource } from './LoggerSource.enum';
const colors = {
  log: {
    bg: '#1616b2',
    text: '#fff'
  },
  info: {
    bg: '#058470',
    text: '#fff'
  },
  warn: {
    bg: '#fddc41',
    text: '#333'
  },
  error: {
    bg: '#e21c50',
    text: '#fff'
  }
};
export function outputLog(message, host, type = LoggerType.LOG, source = LoggerSource.CORE) {
  console[type](`%c${source}: ${message || ''}`, `color: ${colors[type].text}; background-color: ${colors[type].bg};`, host || null);
}
export const logger = {
  log: (message, host, source = LoggerSource.CORE) => {
    outputLog(message, host, LoggerType.LOG, source);
  },
  info: (message, host, source = LoggerSource.CORE) => {
    outputLog(message, host, LoggerType.INFO, source);
  },
  warn: (message, host, source = LoggerSource.CORE) => {
    outputLog(message, host, LoggerType.WARN, source);
  },
  error: (message, host, source = LoggerSource.CORE) => {
    outputLog(message, host, LoggerType.ERROR, source);
  }
};
