import { namesAsArray as coreNamesAsArray } from '@digi/core/src/global/icon/types';
/**
 * TODO: Super-temporary solution. Replace with codegen from .svg files
 */
export const namesAsArray = [
  ...new Set([
    ...coreNamesAsArray,
    'accessibility-deaf',
    'bars',
    'check-circle-reg',
    'check',
    'chevron-down',
    'chevron-left',
    'chevron-right',
    'chevron-up',
    'dislike',
    'exclamation-circle',
    'exclamation-triangle-warning',
    'globe',
    'info-circle-reg',
    'lattlast',
    'like',
    'minus',
    'plus',
    'search',
    'sort',
    'x',
    'input-select-marker',
    'notification-warning',
    'notification-danger',
    'notification-info',
    'close'
  ])
];
