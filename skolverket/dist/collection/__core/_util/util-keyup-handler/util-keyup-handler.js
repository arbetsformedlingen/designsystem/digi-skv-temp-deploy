import { h } from '@stencil/core';
import { KEY_CODE, keyboardHandler } from '../../../global/utils/keyboardHandler';
/**
 * @slot default - Can be anything
 * @swedishName Keyup Handler
 */
export class UtilKeyupHandler {
  keyupHandler(e) {
    const key = keyboardHandler(e);
    switch (key) {
      case KEY_CODE.SHIFT_TAB:
        this.afOnShiftTab.emit(e);
      case KEY_CODE.DOWN_ARROW:
        this.afOnDown.emit(e);
        this.afOnKeyUp.emit(e);
        break;
      case KEY_CODE.UP_ARROW:
        this.afOnUp.emit(e);
        this.afOnKeyUp.emit(e);
        break;
      case KEY_CODE.LEFT_ARROW:
        this.afOnLeft.emit(e);
        this.afOnKeyUp.emit(e);
        break;
      case KEY_CODE.RIGHT_ARROW:
        this.afOnRight.emit(e);
        this.afOnKeyUp.emit(e);
        break;
      case KEY_CODE.ENTER:
        this.afOnEnter.emit(e);
        this.afOnKeyUp.emit(e);
        break;
      case KEY_CODE.ESCAPE:
        this.afOnEsc.emit(e);
        this.afOnKeyUp.emit(e);
        break;
      case KEY_CODE.TAB:
        this.afOnTab.emit(e);
        this.afOnKeyUp.emit(e);
        break;
      case KEY_CODE.SPACE:
        this.afOnSpace.emit(e);
        this.afOnKeyUp.emit(e);
        break;
      case KEY_CODE.HOME:
        this.afOnHome.emit(e);
        this.afOnKeyUp.emit(e);
        break;
      case KEY_CODE.END:
        this.afOnEnd.emit(e);
        this.afOnKeyUp.emit(e);
        break;
      case KEY_CODE.ANY:
        this.afOnKeyUp.emit(e);
        break;
      default:
        this.afOnKeyUp.emit(e);
        return;
    }
  }
  render() {
    return h("slot", null);
  }
  static get is() { return "digi-util-keyup-handler"; }
  static get encapsulation() { return "scoped"; }
  static get events() {
    return [{
        "method": "afOnEsc",
        "name": "afOnEsc",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "At keyup on escape key"
            }],
          "text": "Vid keyup p\u00E5 escape"
        },
        "complexType": {
          "original": "KeyboardEvent",
          "resolved": "KeyboardEvent",
          "references": {
            "KeyboardEvent": {
              "location": "global"
            }
          }
        }
      }, {
        "method": "afOnEnter",
        "name": "afOnEnter",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "At keyup on enter key"
            }],
          "text": "Vid keyup p\u00E5 enter"
        },
        "complexType": {
          "original": "KeyboardEvent",
          "resolved": "KeyboardEvent",
          "references": {
            "KeyboardEvent": {
              "location": "global"
            }
          }
        }
      }, {
        "method": "afOnTab",
        "name": "afOnTab",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "At keyup on tab key"
            }],
          "text": "Vid keyup p\u00E5 tab"
        },
        "complexType": {
          "original": "KeyboardEvent",
          "resolved": "KeyboardEvent",
          "references": {
            "KeyboardEvent": {
              "location": "global"
            }
          }
        }
      }, {
        "method": "afOnSpace",
        "name": "afOnSpace",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "At keyup on space key"
            }],
          "text": "Vid keyup p\u00E5 space"
        },
        "complexType": {
          "original": "KeyboardEvent",
          "resolved": "KeyboardEvent",
          "references": {
            "KeyboardEvent": {
              "location": "global"
            }
          }
        }
      }, {
        "method": "afOnShiftTab",
        "name": "afOnShiftTab",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "At keyup on shift and tab key"
            }],
          "text": "Vid keyup p\u00E5 skift och tabb"
        },
        "complexType": {
          "original": "KeyboardEvent",
          "resolved": "KeyboardEvent",
          "references": {
            "KeyboardEvent": {
              "location": "global"
            }
          }
        }
      }, {
        "method": "afOnUp",
        "name": "afOnUp",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "At keyup on up arrow key"
            }],
          "text": "Vid keyup p\u00E5 pil upp"
        },
        "complexType": {
          "original": "KeyboardEvent",
          "resolved": "KeyboardEvent",
          "references": {
            "KeyboardEvent": {
              "location": "global"
            }
          }
        }
      }, {
        "method": "afOnDown",
        "name": "afOnDown",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "At keyup on down arrow key"
            }],
          "text": "Vid keyup p\u00E5 pil ner"
        },
        "complexType": {
          "original": "KeyboardEvent",
          "resolved": "KeyboardEvent",
          "references": {
            "KeyboardEvent": {
              "location": "global"
            }
          }
        }
      }, {
        "method": "afOnLeft",
        "name": "afOnLeft",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "At keyup on left arrow key"
            }],
          "text": "Vid keyup p\u00E5 pil v\u00E4nster"
        },
        "complexType": {
          "original": "KeyboardEvent",
          "resolved": "KeyboardEvent",
          "references": {
            "KeyboardEvent": {
              "location": "global"
            }
          }
        }
      }, {
        "method": "afOnRight",
        "name": "afOnRight",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "At keyup on right arrow key"
            }],
          "text": "Vid keyup p\u00E5 pil h\u00F6ger"
        },
        "complexType": {
          "original": "KeyboardEvent",
          "resolved": "KeyboardEvent",
          "references": {
            "KeyboardEvent": {
              "location": "global"
            }
          }
        }
      }, {
        "method": "afOnHome",
        "name": "afOnHome",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "At keyup on home key"
            }],
          "text": "Vid keyup p\u00E5 home"
        },
        "complexType": {
          "original": "KeyboardEvent",
          "resolved": "KeyboardEvent",
          "references": {
            "KeyboardEvent": {
              "location": "global"
            }
          }
        }
      }, {
        "method": "afOnEnd",
        "name": "afOnEnd",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "At keyup on end key"
            }],
          "text": "Vid keyup p\u00E5 end"
        },
        "complexType": {
          "original": "KeyboardEvent",
          "resolved": "KeyboardEvent",
          "references": {
            "KeyboardEvent": {
              "location": "global"
            }
          }
        }
      }, {
        "method": "afOnKeyUp",
        "name": "afOnKeyUp",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "At keyup on any key"
            }],
          "text": "Vid keyup p\u00E5 alla tangenter"
        },
        "complexType": {
          "original": "KeyboardEvent",
          "resolved": "KeyboardEvent",
          "references": {
            "KeyboardEvent": {
              "location": "global"
            }
          }
        }
      }];
  }
  static get listeners() {
    return [{
        "name": "keyup",
        "method": "keyupHandler",
        "target": undefined,
        "capture": false,
        "passive": false
      }];
  }
}
