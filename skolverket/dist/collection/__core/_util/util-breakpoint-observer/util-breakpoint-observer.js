import { h } from '@stencil/core';
import { Host } from '@stencil/core/internal';
import { UtilBreakpointObserverBreakpoints } from './util-breakpoint-observer-breakpoints.enum';
/**
 * @slot default - Kan innehålla vad som helst
 * @enums UtilBreakpointObserverBreakpoints - util-breakpoint-observer-breakpoints.enum.ts
 * @swedishName Breakpoint Observer
 *
 */
export class UtilBreakpointObserver {
  componentDidLoad() {
    this.handleBreakpoint();
  }
  handleBreakpoint(e) {
    const observerCSSVariables = window.getComputedStyle(this._observer);
    const value = observerCSSVariables.getPropertyValue('--digi--util-breakpoint-observer--breakpoint');
    this.afOnChange.emit({ value, e });
    switch (value) {
      case UtilBreakpointObserverBreakpoints.SMALL:
        this.afOnSmall.emit({ value, e });
        break;
      case UtilBreakpointObserverBreakpoints.MEDIUM:
        this.afOnMedium.emit({ value, e });
        break;
      case UtilBreakpointObserverBreakpoints.LARGE:
        this.afOnLarge.emit({ value, e });
        break;
      case UtilBreakpointObserverBreakpoints.XLARGE:
        this.afOnXLarge.emit({ value, e });
    }
  }
  render() {
    return (h(Host, null, h("div", { class: "digi-util-breakpoint-observer" }, h("div", { ref: (el) => (this._observer = el), onTransitionEnd: (e) => this.handleBreakpoint(e), class: "digi-util-breakpoint-observer__observer" }), h("slot", null))));
  }
  static get is() { return "digi-util-breakpoint-observer"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["util-breakpoint-observer.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["util-breakpoint-observer.css"]
    };
  }
  static get events() {
    return [{
        "method": "afOnChange",
        "name": "afOnChange",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "At page load and at change of breakpoint on page"
            }],
          "text": "Vid inl\u00E4sning av sida samt vid uppdatering av brytpunkt"
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnSmall",
        "name": "afOnSmall",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "At breakpoint 'small'"
            }],
          "text": "Vid brytpunkt 'small'"
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnMedium",
        "name": "afOnMedium",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "At breakpoint 'medium'"
            }],
          "text": "Vid brytpunkt 'medium'"
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnLarge",
        "name": "afOnLarge",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "At breakpoint 'large'"
            }],
          "text": "Vid brytpunkt 'large'"
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnXLarge",
        "name": "afOnXLarge",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "At breakpoint 'xlarge'"
            }],
          "text": "Vid brytpunkt 'xlarge'"
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }];
  }
  static get elementRef() { return "hostElement"; }
}
