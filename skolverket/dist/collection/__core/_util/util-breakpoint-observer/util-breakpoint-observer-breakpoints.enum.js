export var UtilBreakpointObserverBreakpoints;
(function (UtilBreakpointObserverBreakpoints) {
  UtilBreakpointObserverBreakpoints["SMALL"] = "small";
  UtilBreakpointObserverBreakpoints["MEDIUM"] = "medium";
  UtilBreakpointObserverBreakpoints["LARGE"] = "large";
  UtilBreakpointObserverBreakpoints["XLARGE"] = "xlarge";
})(UtilBreakpointObserverBreakpoints || (UtilBreakpointObserverBreakpoints = {}));
