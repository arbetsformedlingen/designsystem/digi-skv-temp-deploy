import { h } from '@stencil/core';
import { KEY_CODE, keyboardHandler } from '../../../global/utils/keyboardHandler';
/**
 * @slot default - Kan innehålla vad som helst
 * @swedishName Keydown Handler
 */
export class UtilKeydownHandler {
  keydownHandler(e) {
    const key = keyboardHandler(e);
    switch (key) {
      case KEY_CODE.SHIFT_TAB:
        this.afOnShiftTab.emit(e);
      case KEY_CODE.DOWN_ARROW:
        this.afOnDown.emit(e);
        this.afOnKeyDown.emit(e);
        break;
      case KEY_CODE.UP_ARROW:
        this.afOnUp.emit(e);
        this.afOnKeyDown.emit(e);
        break;
      case KEY_CODE.LEFT_ARROW:
        this.afOnLeft.emit(e);
        this.afOnKeyDown.emit(e);
        break;
      case KEY_CODE.RIGHT_ARROW:
        this.afOnRight.emit(e);
        this.afOnKeyDown.emit(e);
        break;
      case KEY_CODE.ENTER:
        this.afOnEnter.emit(e);
        this.afOnKeyDown.emit(e);
        break;
      case KEY_CODE.ESCAPE:
        this.afOnEsc.emit(e);
        this.afOnKeyDown.emit(e);
        break;
      case KEY_CODE.TAB:
        this.afOnTab.emit(e);
        this.afOnKeyDown.emit(e);
        break;
      case KEY_CODE.SPACE:
        this.afOnSpace.emit(e);
        this.afOnKeyDown.emit(e);
        break;
      case KEY_CODE.HOME:
        this.afOnHome.emit(e);
        this.afOnKeyDown.emit(e);
        break;
      case KEY_CODE.END:
        this.afOnEnd.emit(e);
        this.afOnKeyDown.emit(e);
        break;
      case KEY_CODE.ANY:
        this.afOnKeyDown.emit(e);
        break;
      default:
        this.afOnKeyDown.emit(e);
        return;
    }
  }
  render() {
    return h("slot", null);
  }
  static get is() { return "digi-util-keydown-handler"; }
  static get encapsulation() { return "scoped"; }
  static get events() {
    return [{
        "method": "afOnEsc",
        "name": "afOnEsc",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "At keydown on escape key"
            }],
          "text": "Vid keydown p\u00E5 escape"
        },
        "complexType": {
          "original": "KeyboardEvent",
          "resolved": "KeyboardEvent",
          "references": {
            "KeyboardEvent": {
              "location": "global"
            }
          }
        }
      }, {
        "method": "afOnEnter",
        "name": "afOnEnter",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "At keydown on enter key"
            }],
          "text": "Vid keydown p\u00E5 enter"
        },
        "complexType": {
          "original": "KeyboardEvent",
          "resolved": "KeyboardEvent",
          "references": {
            "KeyboardEvent": {
              "location": "global"
            }
          }
        }
      }, {
        "method": "afOnTab",
        "name": "afOnTab",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "At keydown on tab key"
            }],
          "text": "Vid keydown p\u00E5 tab"
        },
        "complexType": {
          "original": "KeyboardEvent",
          "resolved": "KeyboardEvent",
          "references": {
            "KeyboardEvent": {
              "location": "global"
            }
          }
        }
      }, {
        "method": "afOnSpace",
        "name": "afOnSpace",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "At keydown on space key"
            }],
          "text": "Vid keydown p\u00E5 space"
        },
        "complexType": {
          "original": "KeyboardEvent",
          "resolved": "KeyboardEvent",
          "references": {
            "KeyboardEvent": {
              "location": "global"
            }
          }
        }
      }, {
        "method": "afOnShiftTab",
        "name": "afOnShiftTab",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "At keydown on shift and tab key"
            }],
          "text": "Vid keydown p\u00E5 skift och tabb"
        },
        "complexType": {
          "original": "KeyboardEvent",
          "resolved": "KeyboardEvent",
          "references": {
            "KeyboardEvent": {
              "location": "global"
            }
          }
        }
      }, {
        "method": "afOnUp",
        "name": "afOnUp",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "At keydown on up arrow key"
            }],
          "text": "Vid keydown p\u00E5 pil upp"
        },
        "complexType": {
          "original": "KeyboardEvent",
          "resolved": "KeyboardEvent",
          "references": {
            "KeyboardEvent": {
              "location": "global"
            }
          }
        }
      }, {
        "method": "afOnDown",
        "name": "afOnDown",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "At keydown on down arrow key"
            }],
          "text": "Vid keydown p\u00E5 pil ner"
        },
        "complexType": {
          "original": "KeyboardEvent",
          "resolved": "KeyboardEvent",
          "references": {
            "KeyboardEvent": {
              "location": "global"
            }
          }
        }
      }, {
        "method": "afOnLeft",
        "name": "afOnLeft",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "At keydown on left arrow key"
            }],
          "text": "Vid keydown p\u00E5 pil v\u00E4nster"
        },
        "complexType": {
          "original": "KeyboardEvent",
          "resolved": "KeyboardEvent",
          "references": {
            "KeyboardEvent": {
              "location": "global"
            }
          }
        }
      }, {
        "method": "afOnRight",
        "name": "afOnRight",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "At keydown on right arrow key"
            }],
          "text": "Vid keydown p\u00E5 pil h\u00F6ger"
        },
        "complexType": {
          "original": "KeyboardEvent",
          "resolved": "KeyboardEvent",
          "references": {
            "KeyboardEvent": {
              "location": "global"
            }
          }
        }
      }, {
        "method": "afOnHome",
        "name": "afOnHome",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "At keydown on home key"
            }],
          "text": "Vid keydown p\u00E5 home"
        },
        "complexType": {
          "original": "KeyboardEvent",
          "resolved": "KeyboardEvent",
          "references": {
            "KeyboardEvent": {
              "location": "global"
            }
          }
        }
      }, {
        "method": "afOnEnd",
        "name": "afOnEnd",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "At keydown on end key"
            }],
          "text": "Vid keydown p\u00E5 end"
        },
        "complexType": {
          "original": "KeyboardEvent",
          "resolved": "KeyboardEvent",
          "references": {
            "KeyboardEvent": {
              "location": "global"
            }
          }
        }
      }, {
        "method": "afOnKeyDown",
        "name": "afOnKeyDown",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "At keydown on any key"
            }],
          "text": "Vid keydown p\u00E5 alla tangenter"
        },
        "complexType": {
          "original": "KeyboardEvent",
          "resolved": "KeyboardEvent",
          "references": {
            "KeyboardEvent": {
              "location": "global"
            }
          }
        }
      }];
  }
  static get listeners() {
    return [{
        "name": "keydown",
        "method": "keydownHandler",
        "target": undefined,
        "capture": false,
        "passive": false
      }];
  }
}
