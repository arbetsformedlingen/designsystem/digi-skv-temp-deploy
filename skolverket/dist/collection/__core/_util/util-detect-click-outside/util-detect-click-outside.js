import { h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { detectClickOutside } from '../../../global/utils/detectClickOutside';
/**
 * @slot default - Kan innehålla vad som helst
 * @swedishName Detect Click Outside
 */
export class UtilDetectClickOutside {
  constructor() {
    this.afDataIdentifier = randomIdGenerator('data-digi-util-detect-click-outside');
    this.$el.setAttribute(this.afDataIdentifier, '');
  }
  clickHandler(e) {
    const target = e.target;
    const selector = `[${this.afDataIdentifier}]`;
    if (detectClickOutside(target, selector)) {
      this.afOnClickOutside.emit(e);
    }
    else {
      this.afOnClickInside.emit(e);
    }
  }
  render() {
    return h("slot", null);
  }
  static get is() { return "digi-util-detect-click-outside"; }
  static get encapsulation() { return "scoped"; }
  static get properties() {
    return {
      "afDataIdentifier": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set unique identifier needed for click control. Defaults to random string. Should be prefixed with 'data-'"
            }],
          "text": "S\u00E4tter en unik identifierare som beh\u00F6vs f\u00F6r att kontrollera klickeventen. M\u00E5ste vara preficad med 'data-'. Om inget v\u00E4ljs s\u00E5 skapas ett slumpm\u00E4ssigt id."
        },
        "attribute": "af-data-identifier",
        "reflect": false,
        "defaultValue": "randomIdGenerator(\r\n\t\t'data-digi-util-detect-click-outside'\r\n\t)"
      }
    };
  }
  static get events() {
    return [{
        "method": "afOnClickOutside",
        "name": "afOnClickOutside",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "When click detected outside of component"
            }],
          "text": "Vid klick utanf\u00F6r komponenten"
        },
        "complexType": {
          "original": "MouseEvent",
          "resolved": "MouseEvent",
          "references": {
            "MouseEvent": {
              "location": "global"
            }
          }
        }
      }, {
        "method": "afOnClickInside",
        "name": "afOnClickInside",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "When click detected inside of component"
            }],
          "text": "Vid klick inuti komponenten"
        },
        "complexType": {
          "original": "MouseEvent",
          "resolved": "MouseEvent",
          "references": {
            "MouseEvent": {
              "location": "global"
            }
          }
        }
      }];
  }
  static get elementRef() { return "$el"; }
  static get listeners() {
    return [{
        "name": "click",
        "method": "clickHandler",
        "target": "window",
        "capture": false,
        "passive": false
      }];
  }
}
