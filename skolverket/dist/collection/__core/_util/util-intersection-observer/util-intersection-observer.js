import { h } from '@stencil/core';
/**
 * @slot default - Kan innehålla vad som helst
 * @swedishName Intersection Observer
 */
export class UtilIntersectionObserver {
  constructor() {
    this._observer = null;
    this._hasIntersected = false;
    this._isObserving = false;
    this.afOnce = false;
    this.afOptions = {
      rootMargin: '0px',
      threshold: 0
    };
  }
  afOptionsWatcher(newValue) {
    this._afOptions =
      typeof newValue === 'string' ? JSON.parse(newValue) : newValue;
  }
  componentWillLoad() {
    this.afOptionsWatcher(this.afOptions);
    this.initObserver();
  }
  disconnectedCallback() {
    if (this._isObserving) {
      this.removeObserver();
    }
  }
  initObserver() {
    this._observer = new IntersectionObserver(([entry]) => {
      if (entry && entry.isIntersecting) {
        this.intersectionHandler();
        if (this.afOnce) {
          this.removeObserver();
        }
      }
      else if (entry && this._hasIntersected) {
        this.unintersectionHandler();
      }
    }, this._afOptions);
    this._observer.observe(this.$el);
    this._isObserving = true;
  }
  removeObserver() {
    this._observer.disconnect();
    this._isObserving = false;
  }
  intersectionHandler() {
    this.afOnChange.emit(true);
    this.afOnIntersect.emit();
    this._hasIntersected = true;
  }
  unintersectionHandler() {
    this.afOnChange.emit(false);
    this.afOnUnintersect.emit();
    this._hasIntersected = false;
  }
  render() {
    return h("slot", null);
  }
  static get is() { return "digi-util-intersection-observer"; }
  static get encapsulation() { return "scoped"; }
  static get properties() {
    return {
      "afOnce": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Disconnect the observer after first intersection."
            }],
          "text": "St\u00E4ng av observern efter f\u00F6rsta intersection"
        },
        "attribute": "af-once",
        "reflect": false,
        "defaultValue": "false"
      },
      "afOptions": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "object | string",
          "resolved": "object | string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Accepts an Intersection Observer options object. Controls when the image should lazy-load."
            }],
          "text": "Skicka options till komponentens interna Intersection Observer. T.ex. f\u00F6r att kontrollera n\u00E4r bilden lazy loadas."
        },
        "attribute": "af-options",
        "reflect": false,
        "defaultValue": "{\r\n\t\trootMargin: '0px',\r\n\t\tthreshold: 0\r\n\t}"
      }
    };
  }
  static get events() {
    return [{
        "method": "afOnChange",
        "name": "afOnChange",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "On change between unintersected and intersected"
            }],
          "text": "Vid statusf\u00F6r\u00E4ndring mellan 'unintersected' och 'intersected'"
        },
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        }
      }, {
        "method": "afOnIntersect",
        "name": "afOnIntersect",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "On every change to intersected."
            }],
          "text": "Vid statusf\u00F6r\u00E4ndring till 'intersected'"
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnUnintersect",
        "name": "afOnUnintersect",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [],
          "text": "Vid statusf\u00F6r\u00E4ndring till 'unintersected'\r\nOn every change to unintersected"
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }];
  }
  static get elementRef() { return "$el"; }
  static get watchers() {
    return [{
        "propName": "afOptions",
        "methodName": "afOptionsWatcher"
      }];
  }
}
