import { h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { detectFocusOutside } from '../../../global/utils/detectFocusOutside';
/**
 * @slot default - Kan innehålla vad som helst
 * @swedishName Detect Focus Outside
 *
 */
export class UtilDetectFocusOutside {
  constructor() {
    this.afDataIdentifier = randomIdGenerator('data-digi-util-detect-focus-outside');
    this.$el.setAttribute(this.afDataIdentifier, '');
  }
  focusinHandler(e) {
    const target = e.target;
    const selector = `[${this.afDataIdentifier}]`;
    if (detectFocusOutside(target, selector)) {
      this.afOnFocusOutside.emit(e);
    }
    else {
      this.afOnFocusInside.emit(e);
    }
  }
  render() {
    return h("slot", null);
  }
  static get is() { return "digi-util-detect-focus-outside"; }
  static get encapsulation() { return "scoped"; }
  static get properties() {
    return {
      "afDataIdentifier": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set unique identifier needed for focus control. Defaults to random string. Should be prefixed with 'data-'"
            }],
          "text": "S\u00E4tter en unik identifierare som beh\u00F6vs f\u00F6r att kontrollera fokuseventen. M\u00E5ste vara prefixad med 'data-'. Om inget v\u00E4ljs s\u00E5 skapas ett slumpm\u00E4ssigt id."
        },
        "attribute": "af-data-identifier",
        "reflect": false,
        "defaultValue": "randomIdGenerator(\r\n\t\t'data-digi-util-detect-focus-outside'\r\n\t)"
      }
    };
  }
  static get events() {
    return [{
        "method": "afOnFocusOutside",
        "name": "afOnFocusOutside",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "When focus detected outside of component"
            }],
          "text": "Vid fokus utanf\u00F6r komponenten"
        },
        "complexType": {
          "original": "FocusEvent",
          "resolved": "FocusEvent",
          "references": {
            "FocusEvent": {
              "location": "global"
            }
          }
        }
      }, {
        "method": "afOnFocusInside",
        "name": "afOnFocusInside",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "When focus detected inside of component"
            }],
          "text": "Vid fokus inuti komponenten"
        },
        "complexType": {
          "original": "FocusEvent",
          "resolved": "FocusEvent",
          "references": {
            "FocusEvent": {
              "location": "global"
            }
          }
        }
      }];
  }
  static get elementRef() { return "$el"; }
  static get listeners() {
    return [{
        "name": "focusin",
        "method": "focusinHandler",
        "target": "document",
        "capture": false,
        "passive": false
      }];
  }
}
