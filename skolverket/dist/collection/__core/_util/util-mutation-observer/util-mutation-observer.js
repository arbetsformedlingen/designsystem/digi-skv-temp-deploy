import { h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
/**
 * @slot default - Kan innehålla vad som helst
 * @swedishName Mutation Observer
 */
export class UtilMutationObserver {
  constructor() {
    this._observer = null;
    this._isObserving = false;
    this.afId = randomIdGenerator('digi-util-mutation-observer');
    this.afOptions = {
      attributes: false,
      childList: true,
      subtree: false
    };
  }
  afOptionsWatcher(newValue) {
    this._afOptions =
      typeof newValue === 'string' ? JSON.parse(newValue) : newValue;
  }
  componentWillLoad() {
    this.afOptionsWatcher(this.afOptions);
    this.initObserver();
  }
  disconnectedCallback() {
    if (this._isObserving) {
      this.removeObserver();
    }
  }
  initObserver() {
    this._observer = new MutationObserver((mutationsList) => {
      for (const mutation of mutationsList) {
        if (mutation.type === 'childList' || mutation.type === 'attributes') {
          this.changeHandler(mutation);
        }
      }
    });
    this._observer.observe(this.$el, this._afOptions);
    this._isObserving = true;
  }
  removeObserver() {
    this._observer.disconnect();
    this._isObserving = false;
  }
  changeHandler(e) {
    this.afOnChange.emit(e);
  }
  render() {
    h("div", { id: this.afId, ref: (el) => (this.$el = el) }, h("slot", null));
  }
  static get is() { return "digi-util-mutation-observer"; }
  static get encapsulation() { return "scoped"; }
  static get properties() {
    return {
      "afId": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Label id attribute. Defaults to random string."
            }],
          "text": "S\u00E4tter attributet 'id' p\u00E5 det omslutande elementet. Ges inget v\u00E4rde s\u00E5 genereras ett slumpm\u00E4ssigt."
        },
        "attribute": "af-id",
        "reflect": false,
        "defaultValue": "randomIdGenerator('digi-util-mutation-observer')"
      },
      "afOptions": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "object | string",
          "resolved": "object | string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Accepts an Mutation Observer options object. Controls changes ."
            }],
          "text": "Skicka options till komponentens interna Mutation Observer. T.ex. f\u00F6r att kontrollera f\u00F6r\u00E4ndring i en slot."
        },
        "attribute": "af-options",
        "reflect": false,
        "defaultValue": "{\r\n\t\tattributes: false,\r\n\t\tchildList: true,\r\n\t\tsubtree: false\r\n\t}"
      }
    };
  }
  static get events() {
    return [{
        "method": "afOnChange",
        "name": "afOnChange",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "When DOM-elements is added or removed inside the Mutation Observer"
            }],
          "text": "N\u00E4r DOM-element l\u00E4ggs till eller tas bort inuti Mutation Observer:n"
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }];
  }
  static get elementRef() { return "$el"; }
  static get watchers() {
    return [{
        "propName": "afOptions",
        "methodName": "afOptionsWatcher"
      }];
  }
}
