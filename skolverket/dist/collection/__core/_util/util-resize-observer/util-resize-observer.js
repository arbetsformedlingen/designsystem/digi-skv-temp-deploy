import { h } from '@stencil/core';
/**
 * @slot default - Kan innehålla vad som helst
 * @swedishName Resize Observer
 */
export class UtilResizeObserver {
  constructor() {
    this._observer = null;
  }
  componentWillLoad() {
    this.initObserver();
  }
  disconnectedCallback() {
    this.removeObserver();
  }
  initObserver() {
    this._observer = new ResizeObserver(([entry]) => {
      this.afOnChange.emit(entry);
    });
    this._observer.observe(this.hostElement);
  }
  removeObserver() {
    this._observer.disconnect();
  }
  changeHandler(e) {
    this.afOnChange.emit(e);
  }
  render() {
    return h("slot", null);
  }
  static get is() { return "digi-util-resize-observer"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["util-resize-observer.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["util-resize-observer.css"]
    };
  }
  static get events() {
    return [{
        "method": "afOnChange",
        "name": "afOnChange",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "When the component changes size"
            }],
          "text": "N\u00E4r komponenten \u00E4ndrar storlek"
        },
        "complexType": {
          "original": "ResizeObserverEntry",
          "resolved": "ResizeObserverEntry",
          "references": {
            "ResizeObserverEntry": {
              "location": "global"
            }
          }
        }
      }];
  }
  static get elementRef() { return "hostElement"; }
}
