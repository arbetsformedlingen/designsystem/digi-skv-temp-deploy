export var ProgressStepsHeadingLevel;
(function (ProgressStepsHeadingLevel) {
  ProgressStepsHeadingLevel["H1"] = "h1";
  ProgressStepsHeadingLevel["H2"] = "h2";
  ProgressStepsHeadingLevel["H3"] = "h3";
  ProgressStepsHeadingLevel["H4"] = "h4";
  ProgressStepsHeadingLevel["H5"] = "h5";
  ProgressStepsHeadingLevel["H6"] = "h6";
})(ProgressStepsHeadingLevel || (ProgressStepsHeadingLevel = {}));
