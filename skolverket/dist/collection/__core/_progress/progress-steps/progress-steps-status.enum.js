export var ProgressStepsStatus;
(function (ProgressStepsStatus) {
  ProgressStepsStatus["CURRENT"] = "current";
  ProgressStepsStatus["UPCOMING"] = "upcoming";
  ProgressStepsStatus["DONE"] = "done";
})(ProgressStepsStatus || (ProgressStepsStatus = {}));
