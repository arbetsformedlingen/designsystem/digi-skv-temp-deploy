export var ProgressStepsVariation;
(function (ProgressStepsVariation) {
  ProgressStepsVariation["PRIMARY"] = "primary";
  ProgressStepsVariation["SECONDARY"] = "secondary";
})(ProgressStepsVariation || (ProgressStepsVariation = {}));
