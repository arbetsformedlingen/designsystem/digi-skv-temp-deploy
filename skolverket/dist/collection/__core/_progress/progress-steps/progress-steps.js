import { h } from '@stencil/core';
import { ProgressStepsVariation } from './progress-steps-variation.enum';
import { ProgressStepsHeadingLevel } from './progress-steps-heading-level.enum';
import { ProgressStepsStatus } from './progress-steps-status.enum';
import { logger } from '../../../global/utils/logger';
/**
 * @enums ProgressStepsVariation -progress-steps-variation.enum
 * @enums  ProgressStepsHeadingLevel - progress-steps-heading-level.enum
 *  @enums  ProgressStepsStatus - progress-steps-status.enum
 * @swedishName Förloppssteg
 */
export class ProgressSteps {
  constructor() {
    this.afHeadingLevel = ProgressStepsHeadingLevel.H2;
    this.afVariation = ProgressStepsVariation.PRIMARY;
    this.afCurrentStep = 1;
    this.steps = [];
  }
  async afMNext() {
    this.afCurrentStep += 1;
  }
  async afMPrevious() {
    this.afCurrentStep -= 1;
  }
  componentWillLoad() {
    this.getSteps();
    this.setStepProps();
  }
  componentWillUpdate() {
    this.getSteps();
    this.setStepProps();
  }
  getSteps() {
    let steps = this.hostElement.querySelectorAll('digi-progress-step');
    if (!steps || steps.length <= 0) {
      logger.warn(`The slot contains no children elements.`, this.hostElement);
      return;
    }
    this.steps = [...Array.from(steps)].map((step) => {
      return {
        outerHTML: step.outerHTML,
        ref: step
      };
    });
  }
  setStepProps() {
    if (this.steps && this.afCurrentStep < 1) {
      logger.warn(`Current step is set to ${this.afCurrentStep} which is not allowed.`, this.hostElement);
      this.afCurrentStep = 1;
      return;
    }
    if (this.steps && this.afCurrentStep > this.steps.length + 1) {
      logger.warn(`Current step is set to ${this.afCurrentStep} which is more than the amount of available steps (${this.steps.length}).`, this.hostElement);
      this.afCurrentStep = this.steps.length + 1;
      return;
    }
    this.steps &&
      this.steps.forEach((step, index) => {
        step.ref.afVariation = this.afVariation;
        try {
          const topLevel = Number.parseInt(this.afHeadingLevel.split('h')[1]);
          step.ref.afHeadingLevel = 'h' + (topLevel);
          const isLast = index == this.steps.length - 1;
          step.ref.afIsLast = isLast;
          let status;
          if (index < this.afCurrentStep - 1) {
            status = ProgressStepsStatus.DONE;
          }
          else if (index == this.afCurrentStep - 1) {
            status = ProgressStepsStatus.CURRENT;
          }
          else {
            status = ProgressStepsStatus.UPCOMING;
          }
          step.ref.afStepStatus = status;
        }
        catch (e) {
          // Nothing
        }
      });
  }
  render() {
    return (h("div", { role: "list" }, h("digi-typography", null, h("slot", null))));
  }
  static get is() { return "digi-progress-steps"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["progress-steps.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["progress-steps.css"]
    };
  }
  static get properties() {
    return {
      "afHeadingLevel": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "ProgressStepsHeadingLevel",
          "resolved": "ProgressStepsHeadingLevel.H1 | ProgressStepsHeadingLevel.H2 | ProgressStepsHeadingLevel.H3 | ProgressStepsHeadingLevel.H4 | ProgressStepsHeadingLevel.H5 | ProgressStepsHeadingLevel.H6",
          "references": {
            "ProgressStepsHeadingLevel": {
              "location": "import",
              "path": "./progress-steps-heading-level.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set heading level. Default is 'h2'"
            }],
          "text": "S\u00E4tter rubrikniv\u00E5 p\u00E5 varje f\u00F6rloppsteg. 'h2' \u00E4r f\u00F6rvalt."
        },
        "attribute": "af-heading-level",
        "reflect": false,
        "defaultValue": "ProgressStepsHeadingLevel.H2"
      },
      "afVariation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "ProgressStepsVariation",
          "resolved": "ProgressStepsVariation.PRIMARY | ProgressStepsVariation.SECONDARY",
          "references": {
            "ProgressStepsVariation": {
              "location": "import",
              "path": "./progress-steps-variation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set variation. Can be 'Primary' or 'Secondary'"
            }],
          "text": "S\u00E4tter variant. Kan vara 'Primary' eller 'Secondary'"
        },
        "attribute": "af-variation",
        "reflect": false,
        "defaultValue": "ProgressStepsVariation.PRIMARY"
      },
      "afCurrentStep": {
        "type": "number",
        "mutable": false,
        "complexType": {
          "original": "number",
          "resolved": "number",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set current steps"
            }],
          "text": "S\u00E4tter nuvarande steg"
        },
        "attribute": "af-current-step",
        "reflect": false,
        "defaultValue": "1"
      }
    };
  }
  static get states() {
    return {
      "steps": {}
    };
  }
  static get methods() {
    return {
      "afMNext": {
        "complexType": {
          "signature": "() => Promise<void>",
          "parameters": [],
          "references": {
            "Promise": {
              "location": "global"
            }
          },
          "return": "Promise<void>"
        },
        "docs": {
          "text": "",
          "tags": []
        }
      },
      "afMPrevious": {
        "complexType": {
          "signature": "() => Promise<void>",
          "parameters": [],
          "references": {
            "Promise": {
              "location": "global"
            }
          },
          "return": "Promise<void>"
        },
        "docs": {
          "text": "",
          "tags": []
        }
      }
    };
  }
  static get elementRef() { return "hostElement"; }
}
