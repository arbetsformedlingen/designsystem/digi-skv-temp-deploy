export var ProgressbarVariation;
(function (ProgressbarVariation) {
  ProgressbarVariation["PRIMARY"] = "primary";
  ProgressbarVariation["SECONDARY"] = "secondary";
})(ProgressbarVariation || (ProgressbarVariation = {}));
