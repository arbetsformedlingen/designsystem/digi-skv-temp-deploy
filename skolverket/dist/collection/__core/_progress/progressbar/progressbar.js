import { h, } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { ProgressbarRole } from './progressbar-role.enum';
import { ProgressbarVariation } from './progressbar-variation.enum';
/**
 * @enums ProgressbarRole - progressbar-role.enum.ts
 * @enums ProgressbarVariation - progressbar-variation.enum.ts
 * @swedishName Förloppsmätare
 */
export class Progressbar {
  constructor() {
    this.completedSteps = undefined;
    this.totalSteps = [];
    this.listItems = undefined;
    this.hasSlottedItems = undefined;
    this.afCompletedSteps = undefined;
    this.afTotalSteps = undefined;
    this.afStepsSeparator = "av";
    this.afStepsLabel = "frågor besvarade";
    this.afRole = ProgressbarRole.STATUS;
    this.afVariation = ProgressbarVariation.PRIMARY;
    this.afId = randomIdGenerator('digi-progressbar');
  }
  componentWillLoad() {
    this.changeTotalStepsHandler(this.afTotalSteps);
    this.getItems();
  }
  componentDidLoad() {
    this.getItems();
  }
  changeTotalStepsHandler(value) {
    let arr = [];
    let n = 0;
    while (n < value) {
      n++;
      arr.push(n);
    }
    this.totalSteps = arr;
  }
  changeCompletedStepsHandler(value) {
    this.completedSteps = value;
  }
  getItems(update) {
    let items;
    if (update) {
      items = this._observer.children;
    }
    else {
      items = this.hostElement.querySelectorAll('.digi-progressbar__slot li');
    }
    if (items.length > 0) {
      this.hasSlottedItems = true;
      this.listItems = [...items]
        .filter((element) => element.tagName.toLowerCase() === 'li')
        .map((element) => {
        const el = element;
        const checked = el.matches('.completed');
        return {
          text: element.textContent,
          checked: checked
        };
      });
    }
  }
  get cssModifiers() {
    return {
      'digi-progressbar': true,
      'digi-progressbar--primary': this.afVariation === ProgressbarVariation.PRIMARY,
      'digi-progressbar--secondary': this.afVariation === ProgressbarVariation.SECONDARY,
      'digi-progressbar--linear': !this.hasSlottedItems
    };
  }
  get slottedContent() {
    return;
  }
  render() {
    return (h("div", { class: this.cssModifiers, role: this.afRole === ProgressbarRole.STATUS ? ProgressbarRole.STATUS : null }, h("p", { class: "digi-progressbar__label" }, this.afCompletedSteps, " ", this.afStepsSeparator, " ", this.afTotalSteps, " ", this.afStepsLabel), !this.hasSlottedItems
      ? (h("ol", { class: "digi-progressbar__list", "aria-hidden": "true" }, this.totalSteps.map((item, i) => {
        return (h("li", { class: {
            'digi-progressbar__step': true,
            'digi-progressbar__step--active': this.afCompletedSteps > i,
            'digi-progressbar__step--current': this.afCompletedSteps === i + 1
          } }, h("span", { class: "digi-progressbar__step-indicator" }, h("span", { class: "digi-progressbar__step-number" }, item))));
      })))
      : (h("ol", { class: "digi-progressbar__list", "aria-hidden": "true" }, this.listItems.map((item, index) => {
        return (h("li", { class: {
            'digi-progressbar__step': true,
            'digi-progressbar__step--active': item.checked,
          } }, h("span", { class: "digi-progressbar__step-indicator" }, h("span", { class: "digi-progressbar__step-number" }, index + 1))));
      }))), h("div", { hidden: true, class: "digi-progressbar__slot" }, h("digi-util-mutation-observer", { ref: (el) => (this._observer = el), onAfOnChange: () => this.getItems() }, h("slot", null)))));
  }
  static get is() { return "digi-progressbar"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["progressbar.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["progressbar.css"]
    };
  }
  static get properties() {
    return {
      "afCompletedSteps": {
        "type": "number",
        "mutable": false,
        "complexType": {
          "original": "number",
          "resolved": "number",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set finished amount of steps"
            }],
          "text": "S\u00E4tter antal f\u00E4rdiga steg"
        },
        "attribute": "af-completed-steps",
        "reflect": false
      },
      "afTotalSteps": {
        "type": "number",
        "mutable": false,
        "complexType": {
          "original": "number",
          "resolved": "number",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set amount of total steps"
            }],
          "text": "S\u00E4tter totala antalet steg"
        },
        "attribute": "af-total-steps",
        "reflect": false
      },
      "afStepsSeparator": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Separator text between current steps and total steps. \r\nDefault is \"av\", but can be changed e.g. for different language."
            }],
          "text": "Text emellan antal f\u00E4rdiga och totala antalet steg. \r\nSom standard anv\u00E4nds \"av\", men kan \u00E4ndras f\u00F6r exempelvis annat spr\u00E5k."
        },
        "attribute": "af-steps-separator",
        "reflect": false,
        "defaultValue": "\"av\""
      },
      "afStepsLabel": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Descriptive text after step numbers."
            }],
          "text": "Beskrivande text efter siffrorna."
        },
        "attribute": "af-steps-label",
        "reflect": false,
        "defaultValue": "\"fr\u00E5gor besvarade\""
      },
      "afRole": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "ProgressbarRole",
          "resolved": "ProgressbarRole.NONE | ProgressbarRole.STATUS",
          "references": {
            "ProgressbarRole": {
              "location": "import",
              "path": "./progressbar-role.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set role attribute. Defaults to 'status'.\r\nIf you apply the progressbar both before and after a form,\r\nset only role 'status' on one of them to avoid screen readers to read the text twice"
            }],
          "text": "S\u00E4tter attributet 'role'. F\u00F6rvalt \u00E4r 'status'.\r\nAnv\u00E4nder du komponenten b\u00E5de f\u00F6re och efter ett formul\u00E4r s\u00E5\r\nange bara den ena som 'status' f\u00F6r att undvika att sk\u00E4rml\u00E4sare l\u00E4ser upp texten tv\u00E5 g\u00E5nger"
        },
        "attribute": "af-role",
        "reflect": false,
        "defaultValue": "ProgressbarRole.STATUS"
      },
      "afVariation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "ProgressbarVariation",
          "resolved": "ProgressbarVariation.PRIMARY | ProgressbarVariation.SECONDARY",
          "references": {
            "ProgressbarVariation": {
              "location": "import",
              "path": "./progressbar-variation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set variation of progressbar. Can be 'primary' and 'secondary'."
            }],
          "text": "S\u00E4tter variant. Kan vara 'primary' och 'secondary'."
        },
        "attribute": "af-variation",
        "reflect": false,
        "defaultValue": "ProgressbarVariation.PRIMARY"
      },
      "afId": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input id attribute. Defaults to random string."
            }],
          "text": "S\u00E4tter attributet 'id'. Om inget v\u00E4ljs s\u00E5 skapas ett slumpm\u00E4ssigt id."
        },
        "attribute": "af-id",
        "reflect": false,
        "defaultValue": "randomIdGenerator('digi-progressbar')"
      }
    };
  }
  static get states() {
    return {
      "completedSteps": {},
      "totalSteps": {},
      "listItems": {},
      "hasSlottedItems": {}
    };
  }
  static get elementRef() { return "hostElement"; }
  static get watchers() {
    return [{
        "propName": "afTotalSteps",
        "methodName": "changeTotalStepsHandler"
      }, {
        "propName": "afCompletedSteps",
        "methodName": "changeCompletedStepsHandler"
      }];
  }
}
