export var ProgressStepStatus;
(function (ProgressStepStatus) {
  ProgressStepStatus["CURRENT"] = "current";
  ProgressStepStatus["UPCOMING"] = "upcoming";
  ProgressStepStatus["DONE"] = "done";
})(ProgressStepStatus || (ProgressStepStatus = {}));
