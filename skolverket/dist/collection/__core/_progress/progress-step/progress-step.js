import { h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { ProgressStepStatus } from './progress-step-status.enum';
import { ProgressStepVariation } from './progress-step-variation.enum';
import { ProgressStepHeadingLevel } from './progress-step-heading-level.enum';
/**
 * @enums ProgressStepStatus - progress-step-status.enum.ts
 * @enums ProgressStepHeadingLevel- progress-step-heading-level.enum.ts
 * @enums ProgressbarVariation - progressbar-variation.enum.ts
 * @swedishName Förloppssteg - enskilt steg

 */
export class ProgressStep {
  constructor() {
    this.afHeading = undefined;
    this.afHeadingLevel = ProgressStepHeadingLevel.H2;
    this.afStepStatus = ProgressStepStatus.UPCOMING;
    this.afVariation = ProgressStepVariation.PRIMARY;
    this.afIsLast = false;
    this.afId = randomIdGenerator('digi-progress-step');
  }
  render() {
    return (h("div", { "aria-current": this.afStepStatus == ProgressStepStatus.CURRENT && 'step', role: "listitem", class: {
        'digi-progress-step': true,
        [`digi-progress-step--${this.afStepStatus}`]: true,
        'digi-progress-step--last': this.afIsLast,
        'digi-progress-step--primary': this.afVariation == ProgressStepVariation.PRIMARY,
        'digi-progress-step--secondary': this.afVariation == ProgressStepVariation.SECONDARY
      } }, h("span", { class: "digi-progress-step__indicator" }, h("span", { class: "digi-progress-step__indicator--circle" }), h("span", { class: "digi-progress-step__indicator--line" })), h("div", { class: "digi-progress-step__content" }, h(this.afHeadingLevel, { class: "digi-progress-step__content--heading", id: `${this.afId}--heading` }, this.afHeading), h("digi-typography", null, h("slot", null)))));
  }
  static get is() { return "digi-progress-step"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["progress-step.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["progress-step.css"]
    };
  }
  static get properties() {
    return {
      "afHeading": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": true,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The heading of text"
            }],
          "text": "Rubrikens text"
        },
        "attribute": "af-heading",
        "reflect": false
      },
      "afHeadingLevel": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "ProgressStepHeadingLevel",
          "resolved": "ProgressStepHeadingLevel.H1 | ProgressStepHeadingLevel.H2 | ProgressStepHeadingLevel.H3 | ProgressStepHeadingLevel.H4 | ProgressStepHeadingLevel.H5 | ProgressStepHeadingLevel.H6",
          "references": {
            "ProgressStepHeadingLevel": {
              "location": "import",
              "path": "./progress-step-heading-level.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set heading level. Default is 'h2'"
            }],
          "text": "S\u00E4tter rubrikniv\u00E5. 'h2' \u00E4r f\u00F6rvalt."
        },
        "attribute": "af-heading-level",
        "reflect": false,
        "defaultValue": "ProgressStepHeadingLevel.H2"
      },
      "afStepStatus": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "ProgressStepStatus",
          "resolved": "ProgressStepStatus.CURRENT | ProgressStepStatus.DONE | ProgressStepStatus.UPCOMING",
          "references": {
            "ProgressStepStatus": {
              "location": "import",
              "path": "./progress-step-status.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set role attribute. Defaults to 'upcoming'."
            }],
          "text": "S\u00E4tter attributet 'afStepStatus'. F\u00F6rvalt \u00E4r 'upcoming'."
        },
        "attribute": "af-step-status",
        "reflect": false,
        "defaultValue": "ProgressStepStatus.UPCOMING"
      },
      "afVariation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "ProgressStepVariation",
          "resolved": "ProgressStepVariation.PRIMARY | ProgressStepVariation.SECONDARY",
          "references": {
            "ProgressStepVariation": {
              "location": "import",
              "path": "./progress-step-variation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set variation. Can be 'Primary' or 'Secondary'"
            }],
          "text": "S\u00E4tter variant. Kan vara 'Primary' eller 'Secondary'"
        },
        "attribute": "af-variation",
        "reflect": false,
        "defaultValue": "ProgressStepVariation.PRIMARY"
      },
      "afIsLast": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "checks if it is the last step"
            }],
          "text": "Kollar om det \u00E4r sista steget"
        },
        "attribute": "af-is-last",
        "reflect": false,
        "defaultValue": "false"
      },
      "afId": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input id attribute. Defaults to random string."
            }],
          "text": "S\u00E4tter attributet 'id'. Om inget v\u00E4ljs s\u00E5 skapas ett slumpm\u00E4ssigt id."
        },
        "attribute": "af-id",
        "reflect": false,
        "defaultValue": "randomIdGenerator('digi-progress-step')"
      }
    };
  }
  static get elementRef() { return "hostElement"; }
}
