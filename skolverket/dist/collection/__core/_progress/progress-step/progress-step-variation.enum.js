export var ProgressStepVariation;
(function (ProgressStepVariation) {
  ProgressStepVariation["PRIMARY"] = "primary";
  ProgressStepVariation["SECONDARY"] = "secondary";
})(ProgressStepVariation || (ProgressStepVariation = {}));
