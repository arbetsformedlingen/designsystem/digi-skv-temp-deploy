export var ProgressStepHeadingLevel;
(function (ProgressStepHeadingLevel) {
  ProgressStepHeadingLevel["H1"] = "h1";
  ProgressStepHeadingLevel["H2"] = "h2";
  ProgressStepHeadingLevel["H3"] = "h3";
  ProgressStepHeadingLevel["H4"] = "h4";
  ProgressStepHeadingLevel["H5"] = "h5";
  ProgressStepHeadingLevel["H6"] = "h6";
})(ProgressStepHeadingLevel || (ProgressStepHeadingLevel = {}));
