export var ExpandableAccordionHeaderLevel;
(function (ExpandableAccordionHeaderLevel) {
  ExpandableAccordionHeaderLevel["H1"] = "h1";
  ExpandableAccordionHeaderLevel["H2"] = "h2";
  ExpandableAccordionHeaderLevel["H3"] = "h3";
  ExpandableAccordionHeaderLevel["H4"] = "h4";
  ExpandableAccordionHeaderLevel["H5"] = "h5";
  ExpandableAccordionHeaderLevel["H6"] = "h6";
})(ExpandableAccordionHeaderLevel || (ExpandableAccordionHeaderLevel = {}));
