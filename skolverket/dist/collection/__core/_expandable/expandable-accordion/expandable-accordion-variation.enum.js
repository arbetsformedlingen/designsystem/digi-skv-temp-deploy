export var ExpandableAccordionVariation;
(function (ExpandableAccordionVariation) {
  ExpandableAccordionVariation["PRIMARY"] = "primary";
  ExpandableAccordionVariation["SECONDARY"] = "secondary";
})(ExpandableAccordionVariation || (ExpandableAccordionVariation = {}));
