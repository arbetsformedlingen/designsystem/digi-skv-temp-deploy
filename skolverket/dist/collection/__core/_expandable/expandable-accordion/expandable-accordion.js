import { h } from '@stencil/core';
import { ButtonType, ButtonVariation } from '../../../enums-core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { ExpandableAccordionHeaderLevel } from './expandable-accordion-header-level.enum';
import { ExpandableAccordionVariation } from './expandable-accordion-variation.enum';
/**
 * @enums ExpandableAccordionHeaderLevel - expandable-accordion-header-level.enum.ts
 * @swedishName Utfällbar rubrik
 */
export class ExpandableAccordion {
  constructor() {
    this.afVariation = ExpandableAccordionVariation.PRIMARY;
    this.afHeading = undefined;
    this.afHeadingLevel = ExpandableAccordionHeaderLevel.H2;
    this.afExpanded = false;
    this.afAnimation = true;
    this.afId = randomIdGenerator('digi-expandable-accordion');
  }
  get cssModifiers() {
    return {
      [`digi-expandable-accordion--expanded-${!!this.afExpanded}`]: true,
      [`digi-expandable-accordion--animation-${!!this.afAnimation}`]: true,
      [`digi-expandable-accordion--variation-${this.afVariation}`]: !!this.afVariation
    };
  }
  /**
   * Hämtar en höjden till innehållet i Utfällbar yta och animera innehållet.
   * @en Gets a height to the content for expandable and animate the content.
   */
  componentDidLoad() {
    const content = this.hostElement.children[0].querySelector(`#${this.afId}-content`);
    const sectionHeight = content.firstElementChild.offsetHeight;
    if (this.afExpanded) {
      content.setAttribute('style', 'height: ' + sectionHeight + 'px');
    }
  }
  animateContentHeight() {
    const content = this.hostElement.children[0].querySelector(`#${this.afId}-content`);
    const sectionHeight = content.firstElementChild.offsetHeight;
    if (this.afExpanded) {
      content.setAttribute('style', 'height: ' + sectionHeight + 'px');
    }
    else {
      content.setAttribute('style', 'height: ' + 0 + 'px');
    }
  }
  clickToggleHandler(e, resetFocus = false) {
    e.preventDefault();
    this.afExpanded = !this.afExpanded;
    if (resetFocus) {
      this._button.focus();
    }
    this.afOnClick.emit(e);
  }
  render() {
    return (h("article", { "aria-expanded": this.afExpanded ? 'true' : 'false', class: Object.assign({ 'digi-expandable-accordion': true }, this.cssModifiers) }, h("header", { class: "digi-expandable-accordion__header" }, h(this.afHeadingLevel, { class: "digi-expandable-accordion__heading" }, h("button", { type: "button", class: "digi-expandable-accordion__button", onClick: (e) => this.clickToggleHandler(e), "aria-pressed": this.afExpanded ? 'true' : 'false', "aria-expanded": this.afExpanded ? 'true' : 'false', "aria-controls": `${this.afId}-content`, ref: (el) => (this._button = el) }, this.afVariation !== ExpandableAccordionVariation.SECONDARY && (h("digi-icon", { afName: `chevron-down`, class: "digi-expandable-accordion__toggle-icon" })), this.afHeading, this.afVariation === ExpandableAccordionVariation.SECONDARY && (h("span", { class: "digi-expandable-accordion__toggle-label" }, this.afExpanded ? 'Dölj' : 'Visa', h("digi-icon", { afName: this.afExpanded ? 'chevron-up' : 'chevron-down', slot: "icon-secondary" })))))), h("section", { class: "digi-expandable-accordion__content", id: `${this.afId}-content` }, h("div", null, h("digi-typography", null, h("slot", null)), this.afVariation === ExpandableAccordionVariation.SECONDARY && (h("digi-button", { class: "digi-expandable-accordion__toggle-inside", afVariation: ButtonVariation.FUNCTION, afType: ButtonType.BUTTON, onAfOnClick: (e) => this.clickToggleHandler(e.detail, true), afFullWidth: true, afAriaPressed: this.afExpanded, afAriaExpanded: this.afExpanded, afAriaControls: `${this.afId}-content` }, this.afExpanded ? 'Dölj' : 'Visa', h("digi-icon", { afName: this.afExpanded ? 'chevron-up' : 'chevron-down', slot: "icon-secondary" })))))));
  }
  static get is() { return "digi-expandable-accordion"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["expandable-accordion.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["expandable-accordion.css"]
    };
  }
  static get properties() {
    return {
      "afVariation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "ExpandableAccordionVariation",
          "resolved": "ExpandableAccordionVariation.PRIMARY | ExpandableAccordionVariation.SECONDARY",
          "references": {
            "ExpandableAccordionVariation": {
              "location": "import",
              "path": "./expandable-accordion-variation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set component variation"
            }],
          "text": "Utf\u00E4llbara ytans variation"
        },
        "attribute": "af-variation",
        "reflect": false,
        "defaultValue": "ExpandableAccordionVariation.PRIMARY"
      },
      "afHeading": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The heading text"
            }],
          "text": "Rubrikens Utf\u00E4llbar yta"
        },
        "attribute": "af-heading",
        "reflect": false
      },
      "afHeadingLevel": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "ExpandableAccordionHeaderLevel",
          "resolved": "ExpandableAccordionHeaderLevel.H1 | ExpandableAccordionHeaderLevel.H2 | ExpandableAccordionHeaderLevel.H3 | ExpandableAccordionHeaderLevel.H4 | ExpandableAccordionHeaderLevel.H5 | ExpandableAccordionHeaderLevel.H6",
          "references": {
            "ExpandableAccordionHeaderLevel": {
              "location": "import",
              "path": "./expandable-accordion-header-level.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set heading level. Default is 'h2'"
            }],
          "text": "S\u00E4tter utf\u00E4llbar rubrik. 'h2' \u00E4r f\u00F6rvalt."
        },
        "attribute": "af-heading-level",
        "reflect": false,
        "defaultValue": "ExpandableAccordionHeaderLevel.H2"
      },
      "afExpanded": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Sets the state of the accordion to expanded or not."
            }],
          "text": "S\u00E4tter l\u00E4ge p\u00E5 rubriken om den ska vara expanderad eller ej."
        },
        "attribute": "af-expanded",
        "reflect": false,
        "defaultValue": "false"
      },
      "afAnimation": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Sets the state of the animation for the accordion's expanded area. Animation is on as default."
            }],
          "text": "St\u00E4ller in ifall komponenten ska ha en animation vid utf\u00E4llning. Animation \u00E4r p\u00E5slagen som standard."
        },
        "attribute": "af-animation",
        "reflect": false,
        "defaultValue": "true"
      },
      "afId": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set id attribute. Defaults to random string."
            }],
          "text": "S\u00E4tter attributet 'id'. Om inget v\u00E4ljs s\u00E5 skapas ett slumpm\u00E4ssigt id."
        },
        "attribute": "af-id",
        "reflect": false,
        "defaultValue": "randomIdGenerator('digi-expandable-accordion')"
      }
    };
  }
  static get events() {
    return [{
        "method": "afOnClick",
        "name": "afOnClick",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The button element's 'onclick' event."
            }],
          "text": "Buttonelementets 'onclick'-event."
        },
        "complexType": {
          "original": "MouseEvent",
          "resolved": "MouseEvent",
          "references": {
            "MouseEvent": {
              "location": "global"
            }
          }
        }
      }];
  }
  static get elementRef() { return "hostElement"; }
  static get watchers() {
    return [{
        "propName": "afExpanded",
        "methodName": "animateContentHeight"
      }];
  }
}
