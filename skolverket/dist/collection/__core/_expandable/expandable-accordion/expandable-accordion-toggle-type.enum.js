export var ExpandableAccordionToggleType;
(function (ExpandableAccordionToggleType) {
  ExpandableAccordionToggleType["IMPLICIT"] = "implicit";
  ExpandableAccordionToggleType["EXPLICIT"] = "explicit";
})(ExpandableAccordionToggleType || (ExpandableAccordionToggleType = {}));
