export var ButtonSize;
(function (ButtonSize) {
  ButtonSize["SMALL"] = "small";
  ButtonSize["MEDIUM"] = "medium";
  ButtonSize["LARGE"] = "large";
})(ButtonSize || (ButtonSize = {}));
