import { Host, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { ButtonSize } from './button-size.enum';
import { ButtonType } from './button-type.enum';
import { ButtonVariation } from './button-variation.enum';
/**
 * @slot default - Ska vara en textnod
 * @slot icon - Ska vara en ikon (eller en ikonlik) komponent
 * @slot icon-secondary - Ska vara en ikon (eller en ikonlik) komponent
 *
 * @enums ButtonSize - button-size.enum.ts
 * @enums ButtonType - button-type.enum.ts
 * @enums ButtonVariation - button-variation.enum.ts
 * @swedishName Knapp
 */
export class Button {
  constructor() {
    this.hasIcon = undefined;
    this.hasIconSecondary = undefined;
    this.isFullSize = undefined;
    this.afSize = ButtonSize.MEDIUM;
    this.afTabindex = undefined;
    this.afVariation = ButtonVariation.PRIMARY;
    this.afType = ButtonType.BUTTON;
    this.afId = randomIdGenerator('digi-button');
    this.afAriaLabel = undefined;
    this.afAriaLabelledby = undefined;
    this.afAriaControls = undefined;
    this.afForm = undefined;
    this.afAriaPressed = undefined;
    this.afAriaExpanded = undefined;
    this.afAriaHaspopup = undefined;
    this.afLang = undefined;
    this.afDir = undefined;
    this.afFullWidth = false;
    this.afAutofocus = undefined;
  }
  fullWidthHandler() {
    this.isFullSize = this.afFullWidth;
  }
  /**
   * Hämta en referens till buttonelementet. Bra för att t.ex. sätta fokus programmatiskt.
   * @en Returns a reference to the button element. Handy for setting focus programmatically.
   */
  async afMGetButtonElement() {
    return this._button;
  }
  componentWillLoad() {
    this.setHasIcon();
    this.fullWidthHandler();
  }
  componentWillUpdate() {
    this.setHasIcon();
  }
  setHasIcon() {
    const icon = !!this.hostElement.querySelector('[slot="icon"]');
    const iconSecondary = !!this.hostElement.querySelector('[slot="icon-secondary"]');
    if (icon) {
      this.hasIcon = icon;
    }
    if (iconSecondary) {
      this.hasIconSecondary = iconSecondary;
    }
  }
  get cssModifiers() {
    return {
      [`digi-button--variation-${this.afVariation}`]: !!this.afVariation,
      [`digi-button--size-${this.afSize}`]: !!this.afSize,
      'digi-button--icon': this.hasIcon,
      'digi-button--icon-secondary': this.hasIconSecondary,
      'digi-button--full-width': this.isFullSize
    };
  }
  get pressedState() {
    if (this.afAriaPressed === null || this.afAriaPressed === undefined) {
      return;
    }
    return this.afAriaPressed ? 'true' : 'false';
  }
  get expandedState() {
    if (this.afAriaExpanded === null || this.afAriaExpanded === undefined) {
      return;
    }
    return this.afAriaExpanded ? 'true' : 'false';
  }
  get hasPopup() {
    if (this.afAriaHaspopup === null || this.afAriaHaspopup === undefined) {
      return;
    }
    return this.afAriaHaspopup ? 'true' : 'false';
  }
  clickHandler(e) {
    this.afOnClick.emit(e);
  }
  focusHandler(e) {
    this.afOnFocus.emit(e);
  }
  blurHandler(e) {
    this.afOnBlur.emit(e);
  }
  render() {
    return (h(Host, null, h("button", { class: Object.assign({ 'digi-button': true }, this.cssModifiers), type: this.afType, ref: (el) => {
        var _a, _b;
        this._button = el;
        // Stencil has a bug wiith form attribute so we need to set it like this
        // https://github.com/ionic-team/stencil/issues/2703
        this.afForm
          ? (_a = this._button) === null || _a === void 0 ? void 0 : _a.setAttribute('form', this.afForm)
          : (_b = this._button) === null || _b === void 0 ? void 0 : _b.removeAttribute('form');
      }, id: this.afId, tabindex: this.afTabindex, "aria-label": this.afAriaLabel, "aria-labelledby": this.afAriaLabelledby, "aria-controls": this.afAriaControls, "aria-pressed": this.pressedState, "aria-expanded": this.expandedState, "aria-haspopup": this.hasPopup, onClick: (e) => this.clickHandler(e), onFocus: (e) => this.focusHandler(e), onBlur: (e) => this.blurHandler(e), autoFocus: this.afAutofocus ? this.afAutofocus : null, form: this.afForm, lang: this.afLang, dir: this.afDir }, this.hasIcon && (h("span", { class: "digi-button__icon" }, h("slot", { name: "icon" }))), h("span", { class: "digi-button__text" }, h("slot", null)), this.hasIconSecondary && (h("span", { class: "digi-button__icon digi-button__icon--secondary" }, h("slot", { name: "icon-secondary" }))))));
  }
  static get is() { return "digi-button"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["button.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["button.css"]
    };
  }
  static get properties() {
    return {
      "afSize": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "`${ButtonSize}`",
          "resolved": "\"large\" | \"medium\" | \"small\"",
          "references": {
            "ButtonSize": {
              "location": "import",
              "path": "./button-size.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set button size. Defaults to medium size."
            }],
          "text": "S\u00E4tter knappens storlek. 'medium' \u00E4r f\u00F6rvalt."
        },
        "attribute": "af-size",
        "reflect": false,
        "defaultValue": "ButtonSize.MEDIUM"
      },
      "afTabindex": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Sets a tabindex."
            }],
          "text": "S\u00E4tter attributet 'tabindex'."
        },
        "attribute": "af-tabindex",
        "reflect": false
      },
      "afVariation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "`${ButtonVariation}`",
          "resolved": "\"function\" | \"primary\" | \"secondary\"",
          "references": {
            "ButtonVariation": {
              "location": "import",
              "path": "./button-variation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set button variation. If set to FUNCTION, the button also needs an icon."
            }],
          "text": "S\u00E4tt variant. Om varianten \u00E4r 'tertiary' s\u00E5 kr\u00E4vs \u00E4ven en ikon."
        },
        "attribute": "af-variation",
        "reflect": false,
        "defaultValue": "ButtonVariation.PRIMARY"
      },
      "afType": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "`${ButtonType}`",
          "resolved": "\"button\" | \"reset\" | \"submit\"",
          "references": {
            "ButtonType": {
              "location": "import",
              "path": "./button-type.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set button `type` attribute."
            }],
          "text": "S\u00E4tt attributet 'type'."
        },
        "attribute": "af-type",
        "reflect": false,
        "defaultValue": "ButtonType.BUTTON"
      },
      "afId": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input id attribute. Defaults to random string."
            }],
          "text": "S\u00E4tter attributet 'id'. Om inget v\u00E4ljs s\u00E5 skapas ett slumpm\u00E4ssigt id."
        },
        "attribute": "af-id",
        "reflect": false,
        "defaultValue": "randomIdGenerator('digi-button')"
      },
      "afAriaLabel": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set button `aria-label` attribute."
            }],
          "text": "S\u00E4tt attributet 'aria-label'."
        },
        "attribute": "af-aria-label",
        "reflect": false
      },
      "afAriaLabelledby": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set button `aria-labelledby` attribute."
            }],
          "text": "S\u00E4tt attributet 'aria-labelledby'."
        },
        "attribute": "af-aria-labelledby",
        "reflect": false
      },
      "afAriaControls": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set button `aria-controls` attribute."
            }],
          "text": "S\u00E4tter attributet 'aria-controls'."
        },
        "attribute": "af-aria-controls",
        "reflect": false
      },
      "afForm": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set button `form` attribute."
            }],
          "text": "S\u00E4tter attributet 'form'."
        },
        "attribute": "af-form",
        "reflect": false
      },
      "afAriaPressed": {
        "type": "boolean",
        "mutable": true,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set button `aria-pressed` attribute."
            }],
          "text": "S\u00E4tter attributet 'aria-pressed'."
        },
        "attribute": "af-aria-pressed",
        "reflect": false
      },
      "afAriaExpanded": {
        "type": "boolean",
        "mutable": true,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set button `aria-expanded` attribute."
            }],
          "text": "S\u00E4tter attributet 'aria-expanded'."
        },
        "attribute": "af-aria-expanded",
        "reflect": false
      },
      "afAriaHaspopup": {
        "type": "boolean",
        "mutable": true,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set button `aria-haspopup` attribute."
            }],
          "text": "S\u00E4tter attributet 'aria-haspopup'."
        },
        "attribute": "af-aria-haspopup",
        "reflect": false
      },
      "afLang": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set button `lang` attribute."
            }],
          "text": "S\u00E4tter attributet 'lang'."
        },
        "attribute": "af-lang",
        "reflect": false
      },
      "afDir": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set button `dir` attribute."
            }],
          "text": "S\u00E4tter attributet 'dir'."
        },
        "attribute": "af-dir",
        "reflect": false
      },
      "afFullWidth": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set button width to 100%."
            }],
          "text": "S\u00E4tter knappens bredd till 100%."
        },
        "attribute": "af-full-width",
        "reflect": false,
        "defaultValue": "false"
      },
      "afAutofocus": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input autofocus attribute"
            }],
          "text": "S\u00E4tter attributet 'autofocus'."
        },
        "attribute": "af-autofocus",
        "reflect": false
      }
    };
  }
  static get states() {
    return {
      "hasIcon": {},
      "hasIconSecondary": {},
      "isFullSize": {}
    };
  }
  static get events() {
    return [{
        "method": "afOnClick",
        "name": "afOnClick",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The button element's 'onclick' event."
            }],
          "text": "Buttonelementets 'onclick'-event."
        },
        "complexType": {
          "original": "MouseEvent",
          "resolved": "MouseEvent",
          "references": {
            "MouseEvent": {
              "location": "global"
            }
          }
        }
      }, {
        "method": "afOnFocus",
        "name": "afOnFocus",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The button element's 'onfocus' event."
            }],
          "text": "Buttonelementets 'onfocus'-event."
        },
        "complexType": {
          "original": "FocusEvent",
          "resolved": "FocusEvent",
          "references": {
            "FocusEvent": {
              "location": "global"
            }
          }
        }
      }, {
        "method": "afOnBlur",
        "name": "afOnBlur",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The button element's 'onblur' event."
            }],
          "text": "Buttonelementets 'onblur'-event."
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }];
  }
  static get methods() {
    return {
      "afMGetButtonElement": {
        "complexType": {
          "signature": "() => Promise<HTMLButtonElement>",
          "parameters": [],
          "references": {
            "Promise": {
              "location": "global"
            },
            "HTMLButtonElement": {
              "location": "global"
            }
          },
          "return": "Promise<HTMLButtonElement>"
        },
        "docs": {
          "text": "H\u00E4mta en referens till buttonelementet. Bra f\u00F6r att t.ex. s\u00E4tta fokus programmatiskt.",
          "tags": [{
              "name": "en",
              "text": "Returns a reference to the button element. Handy for setting focus programmatically."
            }]
        }
      }
    };
  }
  static get elementRef() { return "hostElement"; }
  static get watchers() {
    return [{
        "propName": "afFullWidth",
        "methodName": "fullWidthHandler"
      }];
  }
}
