import { h, } from '@stencil/core';
/**
 *
 * @swedishName Mediebild
 */
export class MediaImage {
  constructor() {
    this.isPlaceholderLoaded = false;
    this.hasSetSrc = false;
    this.hasAspectRatio = false;
    this.paddedBoxPadding = '0%';
    this.isLoaded = false;
    this.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNk+A8AAQUBAScY42YAAAAASUVORK5CYII=';
    this.srcset = undefined;
    this.afObserverOptions = {
      rootMargin: '200px',
      threshold: 0,
    };
    this.afSrc = undefined;
    this.afSrcset = undefined;
    this.afAlt = undefined;
    this.afTitle = undefined;
    this.afAriaLabel = undefined;
    this.afWidth = undefined;
    this.afHeight = undefined;
    this.afFullwidth = undefined;
    this.afUnlazy = false;
  }
  srcChangeHandler() {
    this.setImageSrc();
  }
  connectedCallback() {
    if (this.afUnlazy) {
      this.setImageSrc();
    }
    // Add padded box to wrapper if width and height props exists
    if (this.afWidth && this.afHeight) {
      this.hasAspectRatio = true;
      this.setPaddedBox();
    }
  }
  setImageSrc() {
    this.src = this.afSrc;
    this.srcset = this.afSrcset;
    this.hasSetSrc = true;
  }
  setPaddedBox() {
    this.paddedBoxPadding =
      (parseInt(this.afHeight, 10) / parseInt(this.afWidth, 10)) * 100 + '%';
  }
  intersectHandler() {
    if (this.afUnlazy) {
      return;
    }
    this.setImageSrc();
  }
  loadHandler(e) {
    if (this.afUnlazy) {
      return;
    }
    if (this.isPlaceholderLoaded) {
      this.isLoaded = true;
      this.afOnLoad.emit(e);
      return;
    }
    this.isPlaceholderLoaded = true;
  }
  get cssModifiers() {
    return {
      'digi-media-image--unlazy': this.afUnlazy,
      'digi-media-image--loaded': this.isLoaded,
      'digi-media-image--aspect-ratio': this.hasAspectRatio,
      'digi-media-image--fullwidth': this.afFullwidth
    };
  }
  get cssInlineStyles() {
    return {
      '--digi--media-image--padded-box': this.paddedBoxPadding,
    };
  }
  render() {
    return (h("host", null, h("div", { class: Object.assign({ 'digi-media-image': true }, this.cssModifiers), style: {
        '--digi--media-image--width': this.afWidth ? `${this.afWidth}px` : null
      } }, h("digi-util-intersection-observer", { class: "digi-media-image__observer", onAfOnIntersect: () => this.intersectHandler(), afOnce: true, afOptions: this.afObserverOptions }), this.hasAspectRatio && (h("div", { class: "digi-media-image__padded-box", style: this.cssInlineStyles })), h("img", { class: "digi-media-image__image", onLoad: (e) => this.loadHandler(e), src: this.src, alt: this.afAlt, "aria-label": this.afAriaLabel, height: this.afHeight, srcset: this.srcset, title: this.afTitle, width: this.afWidth, loading: this.afUnlazy ? 'eager' : 'lazy' }))));
  }
  static get is() { return "digi-media-image"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["media-image.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["media-image.css"]
    };
  }
  static get properties() {
    return {
      "afObserverOptions": {
        "type": "unknown",
        "mutable": false,
        "complexType": {
          "original": "{ rootMargin: string; threshold: number; }",
          "resolved": "{ rootMargin: string; threshold: number; }",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Accepts an Intersection Observer options object. Controls when the image should lazy-load."
            }],
          "text": "Skicka options till komponentens interna Intersection Observer. T.ex. f\u00F6r att kontrollera n\u00E4r bilden lazy loadas."
        },
        "defaultValue": "{\r\n    rootMargin: '200px',\r\n    threshold: 0,\r\n  }"
      },
      "afSrc": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set `src` attribute."
            }],
          "text": "S\u00E4tter attributet 'src'."
        },
        "attribute": "af-src",
        "reflect": false
      },
      "afSrcset": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set `srcset` attribute."
            }],
          "text": "S\u00E4tter attributet 'srcset'."
        },
        "attribute": "af-srcset",
        "reflect": false
      },
      "afAlt": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": true,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set `alt` attribute."
            }],
          "text": "S\u00E4tter attributet 'alt'."
        },
        "attribute": "af-alt",
        "reflect": false
      },
      "afTitle": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set `title` attribute."
            }],
          "text": "S\u00E4tter attributet 'title'."
        },
        "attribute": "af-title",
        "reflect": false
      },
      "afAriaLabel": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set `aria-label` attribute."
            }],
          "text": "S\u00E4tter attributet 'aria-label'."
        },
        "attribute": "af-aria-label",
        "reflect": false
      },
      "afWidth": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set `width` attribute. if both 'width' and 'height' are set, the component will have the correct apect ratio even before the image source is loaded."
            }],
          "text": "S\u00E4tter attributet 'width'. Om b\u00E5de 'width' och 'height' \u00E4r satta s\u00E5 kommer komponenten att ha r\u00E4tt aspektratio \u00E4ven innan bilden laddats in."
        },
        "attribute": "af-width",
        "reflect": false
      },
      "afHeight": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set `height` attribute. if both 'width' and 'height' are set, the component will have the correct apect ratio even before the image source is loaded."
            }],
          "text": "S\u00E4tter attributet 'height'. Om b\u00E5de 'width' och 'height' \u00E4r satta s\u00E5 kommer komponenten att ha r\u00E4tt aspektratio \u00E4ven innan bilden laddats in."
        },
        "attribute": "af-height",
        "reflect": false
      },
      "afFullwidth": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Will set the image width to 100% and height to auto."
            }],
          "text": "S\u00E4tter bilden till 100% i bredd och anpassar h\u00F6jden automatiskt efter bredden."
        },
        "attribute": "af-fullwidth",
        "reflect": false
      },
      "afUnlazy": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Removes lazy laoding of the image."
            }],
          "text": "Tar bort lazy loading av bilden."
        },
        "attribute": "af-unlazy",
        "reflect": false,
        "defaultValue": "false"
      }
    };
  }
  static get states() {
    return {
      "isLoaded": {},
      "src": {},
      "srcset": {}
    };
  }
  static get events() {
    return [{
        "method": "afOnLoad",
        "name": "afOnLoad",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The image element's 'onload' event."
            }],
          "text": "Bildlementets 'onload'-event."
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }];
  }
  static get watchers() {
    return [{
        "propName": "afSrc",
        "methodName": "srcChangeHandler"
      }];
  }
}
