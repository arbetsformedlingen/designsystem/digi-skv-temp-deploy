export var MediaFigureAlignment;
(function (MediaFigureAlignment) {
  MediaFigureAlignment["START"] = "start";
  MediaFigureAlignment["CENTER"] = "center";
  MediaFigureAlignment["END"] = "end";
})(MediaFigureAlignment || (MediaFigureAlignment = {}));
