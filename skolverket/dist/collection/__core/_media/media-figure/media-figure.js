import { h } from '@stencil/core';
import { MediaFigureAlignment } from './media-figure-alignment.enum';
/**
 * @slot default - Ska vara en bild (eller bildlik) komponent. Helst en `digi-media-image`.
 *
 * @enums MediaFigureAlignment - media-figure-alignment.enum.ts
 * @swedishName Mediefigur
 */
export class MediaFigure {
  constructor() {
    this.afFigcaption = undefined;
    this.afAlignment = MediaFigureAlignment.START;
  }
  get cssModifiers() {
    return {
      'digi-media-figure--align-start': this.afAlignment === 'start',
      'digi-media-figure--align-center': this.afAlignment === 'center',
      'digi-media-figure--align-end': this.afAlignment === 'end',
    };
  }
  render() {
    return (h("figure", { class: Object.assign({ 'digi-media-figure': true }, this.cssModifiers) }, h("div", { class: "digi-media-figure__image" }, h("slot", null)), this.afFigcaption && (h("figcaption", { class: "digi-media-figure__figcaption" }, this.afFigcaption))));
  }
  static get is() { return "digi-media-figure"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["media-figure.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["media-figure.css"]
    };
  }
  static get properties() {
    return {
      "afFigcaption": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Adds an optional figcaption element"
            }],
          "text": "L\u00E4gger till ett valfritt figcaptionelement"
        },
        "attribute": "af-figcaption",
        "reflect": false
      },
      "afAlignment": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "MediaFigureAlignment",
          "resolved": "MediaFigureAlignment.CENTER | MediaFigureAlignment.END | MediaFigureAlignment.START",
          "references": {
            "MediaFigureAlignment": {
              "location": "import",
              "path": "./media-figure-alignment.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Alignment of image and caption. Value can be 'start', 'center' or 'end'."
            }],
          "text": "Justering av bild och text. Kan vara 'start', 'center' eller 'end'"
        },
        "attribute": "af-alignment",
        "reflect": false,
        "defaultValue": "MediaFigureAlignment.START"
      }
    };
  }
}
