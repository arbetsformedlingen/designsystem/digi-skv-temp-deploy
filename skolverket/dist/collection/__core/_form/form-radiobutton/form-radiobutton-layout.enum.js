export var FormRadiobuttonLayout;
(function (FormRadiobuttonLayout) {
  FormRadiobuttonLayout["INLINE"] = "inline";
  FormRadiobuttonLayout["BLOCK"] = "block";
})(FormRadiobuttonLayout || (FormRadiobuttonLayout = {}));
