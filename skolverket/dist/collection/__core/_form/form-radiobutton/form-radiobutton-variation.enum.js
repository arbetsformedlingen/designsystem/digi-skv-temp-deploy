export var FormRadiobuttonVariation;
(function (FormRadiobuttonVariation) {
  FormRadiobuttonVariation["PRIMARY"] = "primary";
  FormRadiobuttonVariation["SECONDARY"] = "secondary";
})(FormRadiobuttonVariation || (FormRadiobuttonVariation = {}));
