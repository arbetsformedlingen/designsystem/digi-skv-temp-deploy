import { h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { FormRadiobuttonVariation } from './form-radiobutton-variation.enum';
import { FormRadiobuttonValidation } from './form-radiobutton-validation.enum';
import { FormRadiobuttonLayout } from './form-radiobutton-layout.enum';
/**
 * @enums FormRadiobuttonValidation - form-radiobutton-validation.enum.ts
 * @enums FormRadiobuttonVariation - form-radiobutton-variation.enum.ts
 * @swedishName Radioknapp
 */
export class FormRadiobutton {
  constructor() {
    this.dirty = false;
    this.touched = false;
    this.afLabel = undefined;
    this.afVariation = FormRadiobuttonVariation.PRIMARY;
    this.afLayout = FormRadiobuttonLayout.INLINE;
    this.afName = undefined;
    this.afId = randomIdGenerator('digi-form-radiobutton');
    this.afRequired = undefined;
    this.value = undefined;
    this.afValue = undefined;
    this.checked = undefined;
    this.afChecked = undefined;
    this.afValidation = undefined;
    this.afAriaLabelledby = undefined;
    this.afAriaDescribedby = undefined;
    this.afAutofocus = undefined;
  }
  onValueChanged(value) {
    this.afValue = value;
  }
  onAfValueChanged(value) {
    this.value = value;
  }
  onCheckedChanged(checked) {
    this.afChecked = checked;
  }
  onAfCheckedChanged(checked) {
    this.checked = checked;
  }
  /**
   * Hämta en referens till inputelementet. Bra för att t.ex. sätta fokus programmatiskt.
   * @en Returns a reference to the input element. Handy for setting focus programmatically.
   */
  async afMGetFormControlElement() {
    return this._input;
  }
  componentDidLoad() {
    if (this.value) {
      this.afValue = this.value;
    }
  }
  get cssModifiers() {
    return {
      'digi-form-radiobutton--error': this.afValidation === FormRadiobuttonValidation.ERROR,
      'digi-form-radiobutton--primary': this.afVariation === FormRadiobuttonVariation.PRIMARY,
      'digi-form-radiobutton--secondary': this.afVariation === FormRadiobuttonVariation.SECONDARY,
      [`digi-form-radiobutton--layout-${this.afLayout}`]: !!this.afLayout
    };
  }
  blurHandler(e) {
    if (!this.touched) {
      this.afOnTouched.emit(e);
      this.touched = true;
    }
    this.afOnBlur.emit(e);
  }
  changeHandler(e) {
    this.afOnChange.emit(e);
  }
  focusHandler(e) {
    this.afOnFocus.emit(e);
  }
  focusoutHandler(e) {
    this.afOnFocusout.emit(e);
  }
  inputHandler(e) {
    if (!this.dirty) {
      this.afOnDirty.emit(e);
      this.dirty = true;
    }
    this.afOnInput.emit(e);
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-form-radiobutton': true }, this.cssModifiers) }, h("input", { class: "digi-form-radiobutton__input", ref: (el) => (this._input = el), onInput: (e) => this.inputHandler(e), onBlur: (e) => this.blurHandler(e), onChange: (e) => this.changeHandler(e), onFocus: (e) => this.focusHandler(e), onFocusout: (e) => this.focusoutHandler(e), "aria-describedby": this.afAriaDescribedby, "aria-labelledby": this.afAriaLabelledby, required: this.afRequired, id: this.afId, name: this.afName, type: "radio", checked: this.afChecked, value: this.afValue, autofocus: this.afAutofocus ? this.afAutofocus : null }), h("label", { htmlFor: this.afId, class: "digi-form-radiobutton__label" }, h("span", { class: "digi-form-radiobutton__circle" }), this.afLabel)));
  }
  static get is() { return "digi-form-radiobutton"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["form-radiobutton.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["form-radiobutton.css"]
    };
  }
  static get properties() {
    return {
      "afLabel": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": true,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The label text"
            }],
          "text": "Texten till labelelementet"
        },
        "attribute": "af-label",
        "reflect": false
      },
      "afVariation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "FormRadiobuttonVariation",
          "resolved": "FormRadiobuttonVariation.PRIMARY | FormRadiobuttonVariation.SECONDARY",
          "references": {
            "FormRadiobuttonVariation": {
              "location": "import",
              "path": "./form-radiobutton-variation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set radiobutton variation."
            }],
          "text": "S\u00E4tter variant. Kan vara 'primary' eller 'secondary'"
        },
        "attribute": "af-variation",
        "reflect": false,
        "defaultValue": "FormRadiobuttonVariation.PRIMARY"
      },
      "afLayout": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "FormRadiobuttonLayout",
          "resolved": "FormRadiobuttonLayout.BLOCK | FormRadiobuttonLayout.INLINE",
          "references": {
            "FormRadiobuttonLayout": {
              "location": "import",
              "path": "./form-radiobutton-layout.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set radiobutton layout."
            }],
          "text": "S\u00E4tter layouten."
        },
        "attribute": "af-layout",
        "reflect": false,
        "defaultValue": "FormRadiobuttonLayout.INLINE"
      },
      "afName": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input name attribute"
            }],
          "text": "S\u00E4tter attributet 'name'."
        },
        "attribute": "af-name",
        "reflect": false
      },
      "afId": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input id attribute. Defaults to random string."
            }],
          "text": "S\u00E4tter attributet 'id'. Om inget v\u00E4ljs s\u00E5 skapas ett slumpm\u00E4ssigt id."
        },
        "attribute": "af-id",
        "reflect": false,
        "defaultValue": "randomIdGenerator('digi-form-radiobutton')"
      },
      "afRequired": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input required attribute"
            }],
          "text": "S\u00E4tter attributet 'required'"
        },
        "attribute": "af-required",
        "reflect": false
      },
      "value": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input value attribute"
            }, {
              "name": "ignore",
              "text": undefined
            }],
          "text": "S\u00E4tter attributet 'value'"
        },
        "attribute": "value",
        "reflect": false
      },
      "afValue": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input initial value attribute"
            }],
          "text": "S\u00E4tter initiala attributet 'value'"
        },
        "attribute": "af-value",
        "reflect": false
      },
      "checked": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Sets if radiobutton should be checked."
            }],
          "text": "S\u00E4tter om radioknappen ska vara ikryssad."
        },
        "attribute": "checked",
        "reflect": false
      },
      "afChecked": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Sets if radiobutton should be checked."
            }],
          "text": "S\u00E4tter om radioknappen ska vara ikryssad."
        },
        "attribute": "af-checked",
        "reflect": false
      },
      "afValidation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "FormRadiobuttonValidation",
          "resolved": "FormRadiobuttonValidation",
          "references": {
            "FormRadiobuttonValidation": {
              "location": "import",
              "path": "./form-radiobutton-validation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set input validation"
            }],
          "text": "S\u00E4tter valideringsstatus. Kan vara 'error' eller ingenting."
        },
        "attribute": "af-validation",
        "reflect": false
      },
      "afAriaLabelledby": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input aria-labelledby attribute"
            }],
          "text": "S\u00E4tter attributet 'aria-labelledby'"
        },
        "attribute": "af-aria-labelledby",
        "reflect": false
      },
      "afAriaDescribedby": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input aria-describedby attribute"
            }],
          "text": "S\u00E4tter attributet 'aria-describedby'"
        },
        "attribute": "af-aria-describedby",
        "reflect": false
      },
      "afAutofocus": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input autofocus attribute"
            }],
          "text": "S\u00E4tter attributet 'autofocus'."
        },
        "attribute": "af-autofocus",
        "reflect": false
      }
    };
  }
  static get states() {
    return {
      "dirty": {},
      "touched": {}
    };
  }
  static get events() {
    return [{
        "method": "afOnChange",
        "name": "afOnChange",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The input element's 'onchange' event."
            }],
          "text": "Inputelementets 'onchange'-event."
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnBlur",
        "name": "afOnBlur",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The input element's 'onblur' event."
            }],
          "text": "Inputelementets 'onblur'-event."
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnFocus",
        "name": "afOnFocus",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The input element's 'onfocus' event."
            }],
          "text": "Inputelementets 'onfocus'-event."
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnFocusout",
        "name": "afOnFocusout",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The input element's 'onfocusout' event."
            }],
          "text": "Inputelementets 'onfocusout'-event."
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnInput",
        "name": "afOnInput",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The input element's 'oninput' event."
            }],
          "text": "Inputelementets 'oninput'-event."
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnDirty",
        "name": "afOnDirty",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "First time the input element receives an input"
            }],
          "text": "Sker vid inputf\u00E4ltets f\u00F6rsta 'oninput'"
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnTouched",
        "name": "afOnTouched",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "First time the input element is blurred"
            }],
          "text": "Sker vid inputf\u00E4ltets f\u00F6rsta 'onblur'"
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }];
  }
  static get methods() {
    return {
      "afMGetFormControlElement": {
        "complexType": {
          "signature": "() => Promise<HTMLInputElement>",
          "parameters": [],
          "references": {
            "Promise": {
              "location": "global"
            },
            "HTMLInputElement": {
              "location": "global"
            }
          },
          "return": "Promise<HTMLInputElement>"
        },
        "docs": {
          "text": "H\u00E4mta en referens till inputelementet. Bra f\u00F6r att t.ex. s\u00E4tta fokus programmatiskt.",
          "tags": [{
              "name": "en",
              "text": "Returns a reference to the input element. Handy for setting focus programmatically."
            }]
        }
      }
    };
  }
  static get elementRef() { return "hostElement"; }
  static get watchers() {
    return [{
        "propName": "value",
        "methodName": "onValueChanged"
      }, {
        "propName": "afValue",
        "methodName": "onAfValueChanged"
      }, {
        "propName": "checked",
        "methodName": "onCheckedChanged"
      }, {
        "propName": "afChecked",
        "methodName": "onAfCheckedChanged"
      }];
  }
}
