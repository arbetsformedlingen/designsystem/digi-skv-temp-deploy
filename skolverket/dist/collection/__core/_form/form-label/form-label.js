import { h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { logger } from '../../../global/utils/logger';
/**
 * @slot actions - Förväntar sig interaktiva och ikonstora komponenter
 * @swedishName Etikett
 */
export class FormLabel {
  constructor() {
    this.labelText = undefined;
    this.hasActionSlot = undefined;
    this.afLabel = undefined;
    this.afId = randomIdGenerator('digi-form-label');
    this.afFor = undefined;
    this.afDescription = undefined;
    this.afRequired = undefined;
    this.afAnnounceIfOptional = false;
    this.afRequiredText = 'obligatoriskt';
    this.afAnnounceIfOptionalText = 'frivilligt';
  }
  componentWillLoad() {
    this.setLabelText();
    this.validateLabel();
    this.validateFor();
    this.handleSlotVisibility();
  }
  validateLabel() { }
  validateFor() {
    if (this.afFor)
      return;
    logger.warn(`digi-form-label must have a for attribute. Please add a for attribute using af-for`, this.hostElement);
  }
  setLabelText() {
    this.labelText = `${this.afLabel} ${this.requiredText}`;
    if (!this.afLabel) {
      logger.warn(`digi-form-label must have a label. Please add a label using af-label`, this.hostElement);
    }
  }
  handleSlotVisibility() {
    this.hasActionSlot = !!this.hostElement.querySelector('[slot="actions"]');
  }
  get requiredText() {
    return this.afRequired && !this.afAnnounceIfOptional
      ? ` (${this.afRequiredText})`
      : !this.afRequired && this.afAnnounceIfOptional
        ? ` (${this.afAnnounceIfOptionalText})`
        : '';
  }
  render() {
    return (h("div", { class: "digi-form-label" }, h("div", { class: "digi-form-label__label-group" }, this.afFor && this.afLabel && (h("label", { class: "digi-form-label__label", htmlFor: this.afFor, id: this.afId }, this.labelText)), this.hasActionSlot && (h("div", { class: "digi-form-label__actions" }, h("slot", { name: "actions" })))), this.afDescription && (h("p", { class: "digi-form-label__description" }, this.afDescription))));
  }
  static get is() { return "digi-form-label"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["form-label.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["form-label.css"]
    };
  }
  static get properties() {
    return {
      "afLabel": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": true,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The label text"
            }],
          "text": "Texten till labelelementet"
        },
        "attribute": "af-label",
        "reflect": false
      },
      "afId": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set id attribute. Defaults to random string."
            }],
          "text": "S\u00E4tter attributet 'id'. Om inget v\u00E4ljs s\u00E5 skapas ett slumpm\u00E4ssigt id."
        },
        "attribute": "af-id",
        "reflect": false,
        "defaultValue": "randomIdGenerator('digi-form-label')"
      },
      "afFor": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": true,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set for attribute. Should be identical with the id attribute of the associated input element."
            }],
          "text": "S\u00E4tter attributet 'for'. Ska vara identiskt med attributet 'id' p\u00E5 inputelementet"
        },
        "attribute": "af-for",
        "reflect": false
      },
      "afDescription": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "A description text"
            }],
          "text": "Valfri beskrivande text"
        },
        "attribute": "af-description",
        "reflect": false
      },
      "afRequired": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set to true if the associated input element is required"
            }],
          "text": "Aktivera om det tillh\u00F6rande inputelementet \u00E4r obligatoriskt"
        },
        "attribute": "af-required",
        "reflect": false
      },
      "afAnnounceIfOptional": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set this to true if the form contains more required fields than optional fields."
            }],
          "text": "S\u00E4tt denna till true om formul\u00E4ret inneh\u00E5ller fler obligatoriska f\u00E4lt \u00E4n valfria."
        },
        "attribute": "af-announce-if-optional",
        "reflect": false,
        "defaultValue": "false"
      },
      "afRequiredText": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set text for afRequired."
            }],
          "text": "S\u00E4tter text f\u00F6r afRequired."
        },
        "attribute": "af-required-text",
        "reflect": false,
        "defaultValue": "'obligatoriskt'"
      },
      "afAnnounceIfOptionalText": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set text for afAnnounceIfOptional."
            }],
          "text": "S\u00E4tter text f\u00F6r afAnnounceIfOptional."
        },
        "attribute": "af-announce-if-optional-text",
        "reflect": false,
        "defaultValue": "'frivilligt'"
      }
    };
  }
  static get states() {
    return {
      "labelText": {},
      "hasActionSlot": {}
    };
  }
  static get elementRef() { return "hostElement"; }
  static get watchers() {
    return [{
        "propName": "afLabel",
        "methodName": "validateLabel"
      }, {
        "propName": "afFor",
        "methodName": "validateFor"
      }, {
        "propName": "afLabel",
        "methodName": "setLabelText"
      }, {
        "propName": "afRequired",
        "methodName": "setLabelText"
      }, {
        "propName": "afAnnounceIfOptional",
        "methodName": "setLabelText"
      }];
  }
}
