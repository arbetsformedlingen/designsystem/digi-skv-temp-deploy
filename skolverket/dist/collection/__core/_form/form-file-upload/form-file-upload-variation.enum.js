export var FormFileUploadVariation;
(function (FormFileUploadVariation) {
  FormFileUploadVariation["PRIMARY"] = "primary";
  FormFileUploadVariation["SECONDARY"] = "secondary";
  FormFileUploadVariation["TERTIARY"] = "tertiary";
})(FormFileUploadVariation || (FormFileUploadVariation = {}));
