import { h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { FormFileUploadVariation } from './form-file-upload-variation.enum';
import { FormFileUploadValidation } from './form-file-upload-validation.enum';
import { ButtonType } from '../../_button/button/button-type.enum';
import { ButtonVariation } from '../../_button/button/button-variation.enum';
import { FormFileUploadHeadingLevel } from './form-file-upload-heading-level.enum';
/**
 *	@enums FormInputFileType - form-input-file-type.enum.ts
 *  @enums FormInputFileVariation - form-input-file-variation.enum.ts
 * 	@swedishName Filuppladdare
 */
export class FormFileUpload {
  constructor() {
    this.toBase64 = (file) => new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        let encoded = reader.result.toString().replace(/^data:(.*,)?/, '');
        if (encoded.length % 4 > 0) {
          encoded += '='.repeat(4 - (encoded.length % 4));
        }
        resolve(encoded);
      };
      reader.onerror = (error) => reject(error);
    });
    this._input = undefined;
    this.files = [];
    this.error = false;
    this.errorMessage = undefined;
    this.fileHover = false;
    this.afLabel = 'Ladda upp fil';
    this.afLabelDescription = undefined;
    this.afVariation = FormFileUploadVariation.PRIMARY;
    this.afId = randomIdGenerator('digi-form-file-upload');
    this.afFileTypes = undefined;
    this.afFileMaxSize = 10;
    this.afMaxFiles = undefined;
    this.afName = undefined;
    this.afUploadBtnText = undefined;
    this.afRequired = undefined;
    this.afRequiredText = undefined;
    this.afAnnounceIfOptional = false;
    this.afHeadingFiles = 'Uppladdade filer';
    this.afAnnounceIfOptionalText = undefined;
    this.afValidation = FormFileUploadValidation.ENABLED;
    this.afHeadingLevel = FormFileUploadHeadingLevel.H2;
  }
  /**
   * Validera filens status.
   * @en Validate file status
   */
  async afMValidateFile(scanInfo) {
    this.validateFile(scanInfo);
  }
  /**
   * Få ut alla uppladdade filer
   * @en Get all uploaded files
   */
  async afMGetAllFiles() {
    if (this.afValidation === 'enabled') {
      const validatedFiles = this.files.filter((file) => file['status'] === 'OK');
      return validatedFiles;
    }
    else {
      return this.files;
    }
  }
  /**
   * Importera en array med filer till komponenten utan att trigga uppladdningsevent. Ett fil objekt måste innehålla id, status och filen själv.
   * @en Import an array with files without triggering upload event. A file object needs to contain an id, status and the file itself.
   */
  async afMImportFiles(files) {
    for (const importedFile of files) {
      let duplicate = false;
      for (const item of this.files) {
        if (item.name == importedFile.name) {
          this.error = true;
          this.errorMessage = `Det finns redan en fil med filnamnet (${importedFile.name})`;
          duplicate = true;
        }
      }
      if (!duplicate) {
        this.files = [...this.files, importedFile];
      }
    }
  }
  async afMGetFormControlElement() {
    return this._input;
  }
  get fileMaxSize() {
    // 1MB is 1048576 in Bytes
    return this.afFileMaxSize * 1048576;
  }
  validateFile(scanInfo) {
    const index = this.files.findIndex((file) => file['name'] == scanInfo['name'] || file['id'] == scanInfo['id']);
    if (index === -1) {
      return { message: 'Something went wrong' };
    }
    let file = this.files[index];
    if (scanInfo.status === 'OK') {
      file['status'] = 'OK';
      delete file['error'];
      this.removeFileFromList(file['id']);
      this.files = [...this.files, file];
    }
    else {
      file['status'] = 'error';
      file['error'] =
        scanInfo.error === undefined ? 'Fil bedömt som osäker' : scanInfo.error;
      this.removeFileFromList(file['id']);
      this.files = [...this.files, file];
    }
  }
  emitFile(incomingFile) {
    const index = this.getIndexOfFile(incomingFile['id']);
    if (this.afValidation === 'enabled') {
      this.afOnUploadFile.emit(this.files[index]);
    }
    else {
      this.files[index]['status'] = 'OK';
      this.afOnUploadFile.emit(this.files[index]);
    }
  }
  onRetryFileHandler(e, id) {
    e.preventDefault();
    const fileFromList = this.removeFileFromList(id);
    fileFromList['status'] = 'pending';
    delete fileFromList['error'];
    this.files = [...this.files, fileFromList];
    this.afOnRetryFile.emit(fileFromList);
  }
  onCancelFileHandler(e, id) {
    e.preventDefault();
    const fileFromList = this.removeFileFromList(id);
    fileFromList['status'] = 'error';
    fileFromList['error'] = 'Uppladdning avbruten';
    this.files = [...this.files, fileFromList];
    this.afOnCancelFile.emit(fileFromList);
  }
  removeFileFromList(id) {
    const index = this.getIndexOfFile(id);
    if (index === -1) {
      return;
    }
    const removedFile = this.files[index];
    this.files = [...this.files.slice(0, index), ...this.files.slice(index + 1)];
    return removedFile;
  }
  getIndexOfFile(id) {
    const index = this.files.findIndex((file) => file['id'] == id);
    return index;
  }
  onButtonUploadFileHandler(e) {
    e.preventDefault();
    if (!e.target.files)
      return;
    const file = e.target.files[0];
    this.addUploadedFiles(file);
  }
  onRemoveFileHandler(e, id) {
    e.preventDefault();
    const fileToRemove = this.removeFileFromList(id);
    this.afOnRemoveFile.emit(fileToRemove);
    this._input.value = '';
    if (this.files.length < 1)
      this.error = false;
  }
  async addUploadedFiles(file) {
    this.error = false;
    if (this.files.length >= this.afMaxFiles) {
      this.error = true;
      this.errorMessage = 'Max antal filer uppladdade!';
      return;
    }
    else if (file.size > this.fileMaxSize) {
      this.error = true;
      this.errorMessage = 'Filens storlek är för stor!';
      return;
    }
    else {
      for (const item of this.files) {
        if (item.name == file.name) {
          this.error = true;
          this.errorMessage = 'Det finns redan en fil med det filnamnet!';
          return;
        }
      }
    }
    file['id'] = randomIdGenerator('file');
    file['status'] = 'pending';
    file['base64'] = await this.toBase64(file);
    this.files = [...this.files, file];
    this.emitFile(file);
  }
  handleDrop(e) {
    e.stopPropagation();
    e.preventDefault();
    this.fileHover = false;
    if (e.dataTransfer.files) {
      const file = e.dataTransfer.files[0];
      if (file.type.includes(this.afFileTypes) || this.afFileTypes == '*') {
        this.addUploadedFiles(file);
      }
      else {
        this.error = true;
        this.errorMessage = 'Filtypen tillåts inte att ladda upp';
      }
    }
  }
  handleAllowDrop(e) {
    if (this.afVariation === 'secondary')
      return;
    e.stopPropagation();
    e.preventDefault();
  }
  handleOnDragEnter(e) {
    this.handleAllowDrop(e);
    this.fileHover = !this.fileHover;
  }
  handleOnDragLeave(e) {
    this.handleAllowDrop(e);
    this.fileHover = !this.fileHover;
  }
  handleInputClick(e) {
    e.cancelBubble = true;
    this._input.click();
  }
  componentWillLoad() { }
  componentDidLoad() { }
  componentWillUpdate() { }
  get cssModifiers() {
    return {
      'digi-form-file-upload--primary': this.afVariation == 'primary',
      'digi-form-file-upload--secondary': this.afVariation == 'secondary',
      'digi-form-file-upload--tertiary': this.afVariation == 'tertiary'
    };
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-form-file-upload': true }, this.cssModifiers) }, h("digi-form-label", { afFor: this.afId, afLabel: this.afLabel, afDescription: this.afLabelDescription, afRequired: this.afRequired, afAnnounceIfOptional: this.afAnnounceIfOptional, afRequiredText: this.afRequiredText, afAnnounceIfOptionalText: this.afAnnounceIfOptionalText }), h("div", { class: {
        'digi-form-file-upload__upload-area': true,
        'digi-form-file-upload__upload-area--hover': this.fileHover
      }, onDrop: (e) => this.handleDrop(e), onDragOver: (e) => this.handleAllowDrop(e), onDragEnter: (e) => this.handleOnDragEnter(e), onDragLeave: (e) => this.handleOnDragLeave(e), onClick: (e) => this.handleInputClick(e) }, this.fileHover && this.afVariation != 'secondary' && (h("div", { class: "digi-form-file-upload__upload-area-overlay" })), this.afVariation == 'tertiary' && (h("digi-icon-upload", { style: { '--digi--icon--height': '40px' }, class: "digi-form-file-upload__icon", "aria-hidden": "true" })), h("div", { class: "digi-form-file-upload__text-area" }, h("digi-button", { afVariation: ButtonVariation.FUNCTION, afType: ButtonType.BUTTON, onClick: (e) => this.handleInputClick(e) }, h("digi-icon-paperclip", { "aria-hidden": "true", slot: "icon" }), this.afUploadBtnText ? this.afUploadBtnText : 'Välj fil'), h("input", { ref: (el) => {
        this._input = el;
      }, onChange: (e) => this.onButtonUploadFileHandler(e), class: "digi-form-file-upload__input", multiple: true, type: "file", id: this.afId, accept: this.afFileTypes, name: this.afName ? this.afName : null, required: this.afRequired ? this.afRequired : null, onDrop: (e) => this.handleDrop(e), onDragOver: (e) => this.handleAllowDrop(e), "aria-errormessage": "digi-form-file-upload__error", "aria-describedBy": "digi-form-file-upload__error" }), this.afVariation != FormFileUploadVariation.SECONDARY &&
      'eller dra och släpp en fil inuti det streckade området')), this.error && (h("digi-form-validation-message", { id: "digi-form-file-upload__error", class: "digi-form-file-upload__error", role: "alert", "aria-label": this.errorMessage, "af-variation": "error" }, this.errorMessage)), this.files.length > 0 && (h("div", { class: "digi-form-file-upload__files" }, h(this.afHeadingLevel, { class: "digi-form-file-upload__files-heading" }, this.afHeadingFiles), h("ul", { "aria-live": "assertive" }, this.files.map((file) => {
      return (h("li", { class: "digi-form-file-upload__file-container" }, file['status'] == 'pending' && (h("div", { class: "digi-form-file-upload__file" }, h("div", { class: "digi-form-file-upload__file-header" }, h("digi-icon-spinner", { class: "digi-form-file-upload__spinner hidden-mobile" }), h("span", null, "Laddar upp... ", h("span", { class: 'hidden-mobile' }, "|")), h("p", { class: "digi-form-file-upload__file-name hidden-mobile" }, file.name), h("button", { type: "button", onClick: (e) => this.onCancelFileHandler(e, file['id']), class: "digi-form-file-upload__button hidden-mobile", "aria-label": `Avbryt uppladdningen av ${file.name}` }, "Avbryt uppladdning"), h("button", { type: "button", onClick: (e) => this.onCancelFileHandler(e, file['id']), class: "digi-form-file-upload__button digi-form-file-upload__button--mobile", "aria-label": `Avbryt uppladdningen av ${file.name}` }, h("digi-icon-x-button-outline", { style: {
          '--digi--icon--height': '20px',
          '--digi--icon--color': 'var(--digi--global--color--function--info--base)'
        }, "aria-hidden": "true" }))))), file['status'] == 'OK' && (h("div", { class: "digi-form-file-upload__file" }, h("div", { class: "digi-form-file-upload__file-header" }, h("digi-icon-check-circle-reg", { style: {
          '--digi--icon--height': '20px',
          '--digi--icon--color': 'green'
        }, class: 'hidden-mobile' }), h("p", { class: "digi-form-file-upload__file-name" }, file.name), h("button", { type: "button", onClick: (e) => this.onRemoveFileHandler(e, file['id']), class: "digi-form-file-upload__button hidden-mobile", "aria-label": `Ta bort ${file.name}` }, "Ta bort fil"), h("button", { type: "button", onClick: (e) => this.onRemoveFileHandler(e, file['id']), class: "digi-form-file-upload__button digi-form-file-upload__button--mobile", "aria-label": `Ta bort ${file.name}` }, h("digi-icon-trash", { style: {
          '--digi--icon--height': '20px',
          '--digi--icon--color': 'var(--digi--global--color--function--info--base)'
        } }))))), file['status'] == 'error' && (h("div", { class: "digi-form-file-upload__file" }, h("div", { class: "digi-form-file-upload__file-header" }, h("digi-icon-danger-outline", { style: {
          '--digi--icon--height': '20px',
          '--digi--icon--color': 'red'
        }, "aria-hidden": "true", class: 'hidden-mobile' }), h("span", { class: "digi-form-file-upload__file--error", "aria-label": `${file['name']} gick inte att ladda upp. ${file['error']}` }, "Avbruten ", h("span", null, "|")), h("p", { class: "digi-form-file-upload__file-name\n\t\t\t\t\t\t\t\t\t\t\t\t\tdigi-form-file-upload__file--error hidden-mobile\n\t\t\t\t\t\t\t\t\t\t\t\t\t" }, file.name), this.afValidation === 'disabled' ||
        (file['error'] === 'Uppladdning avbruten' && (h("div", { style: { display: 'flex' } }, h("button", { type: "button", onClick: (e) => this.onRetryFileHandler(e, file['id']), class: "digi-form-file-upload__button digi-form-file-upload__button--file hidden-mobile", "aria-label": `Misslyckad uppladdning, försök ladda upp filen igen ${file.name}` }, "F\u00F6rs\u00F6k igen", h("span", null, "|")), h("button", { type: "button", onClick: (e) => this.onRetryFileHandler(e, file['id']), class: "digi-form-file-upload__button digi-form-file-upload__button--mobile digi-form-file-upload__button--file", "aria-label": `Misslyckad uppladdning, försök ladda upp filen igen ${file.name}`, style: {
            'margin-right': '10px'
          } }, h("digi-icon-update", { style: {
            '--digi--icon--height': '20px',
            '--digi--icon--color': 'var(--digi--global--color--function--info--base)'
          } }))))), h("button", { type: "button", onClick: (e) => this.onRemoveFileHandler(e, file['id']), class: "digi-form-file-upload__button digi-form-file-upload__button--file hidden-mobile", "aria-label": `Misslyckad uppladdning, ta bort ${file.name} från fillistan` }, "Ta bort fil"), h("button", { type: "button", onClick: (e) => this.onRemoveFileHandler(e, file['id']), class: "digi-form-file-upload__button digi-form-file-upload__button--mobile digi-form-file-upload__button--file", "aria-label": `Misslyckad uppladdning, ta bort ${file.name} från fillistan` }, h("digi-icon-x-button-outline", { style: {
          '--digi--icon--height': '20px',
          '--digi--icon--color': 'var(--digi--global--color--function--info--base)'
        } }))), h("div", { class: "digi-form-file-upload__file-footer" }, h("p", { class: 'hidden-mobile', role: "status", "aria-label": `${file['name']} gick inte att ladda upp. ${file['error']}` }, `Filen gick inte att ladda upp. ${file['error']}`))))));
    }))))));
  }
  static get is() { return "digi-form-file-upload"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["form-file-upload.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["form-file-upload.css"]
    };
  }
  static get properties() {
    return {
      "afLabel": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The label text"
            }],
          "text": "Texten till labelelementet"
        },
        "attribute": "af-label",
        "reflect": false,
        "defaultValue": "'Ladda upp fil'"
      },
      "afLabelDescription": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "A description text"
            }],
          "text": "Valfri beskrivande text"
        },
        "attribute": "af-label-description",
        "reflect": false
      },
      "afVariation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "FormFileUploadVariation",
          "resolved": "FormFileUploadVariation.PRIMARY | FormFileUploadVariation.SECONDARY | FormFileUploadVariation.TERTIARY",
          "references": {
            "FormFileUploadVariation": {
              "location": "import",
              "path": "./form-file-upload-variation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set input variation."
            }],
          "text": "S\u00E4tter variant. Kan vara 'primary', 'secondary' eller 'tertiary'."
        },
        "attribute": "af-variation",
        "reflect": false,
        "defaultValue": "FormFileUploadVariation.PRIMARY"
      },
      "afId": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input id attribute. Defaults to random string."
            }],
          "text": "S\u00E4tter attributet 'id'. Om inget v\u00E4ljs s\u00E5 skapas ett slumpm\u00E4ssigt id."
        },
        "attribute": "af-id",
        "reflect": false,
        "defaultValue": "randomIdGenerator('digi-form-file-upload')"
      },
      "afFileTypes": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": true,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set input field's accept attribute. Use this to limit accepted file types."
            }],
          "text": "S\u00E4tter attributet 'accept'. Anv\u00E4nd f\u00F6r att limitera accepterade filtyper"
        },
        "attribute": "af-file-types",
        "reflect": false
      },
      "afFileMaxSize": {
        "type": "number",
        "mutable": false,
        "complexType": {
          "original": "number",
          "resolved": "number",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set file max size in MB"
            }],
          "text": "S\u00E4tter en maximal filstorlek i MB, 10MB \u00E4r standard."
        },
        "attribute": "af-file-max-size",
        "reflect": false,
        "defaultValue": "10"
      },
      "afMaxFiles": {
        "type": "number",
        "mutable": false,
        "complexType": {
          "original": "number",
          "resolved": "number",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set max number of files you can upload"
            }],
          "text": "S\u00E4tter maximalt antal filer man kan ladda upp"
        },
        "attribute": "af-max-files",
        "reflect": false
      },
      "afName": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input name attribute"
            }],
          "text": "S\u00E4tter attributet 'name'."
        },
        "attribute": "af-name",
        "reflect": false
      },
      "afUploadBtnText": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set upload buttons text"
            }],
          "text": "S\u00E4tter ladda upp knappens text."
        },
        "attribute": "af-upload-btn-text",
        "reflect": false
      },
      "afRequired": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input required attribute"
            }],
          "text": "S\u00E4tter attributet 'required'."
        },
        "attribute": "af-required",
        "reflect": false
      },
      "afRequiredText": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set text for afRequired."
            }],
          "text": "S\u00E4tter text f\u00F6r afRequired."
        },
        "attribute": "af-required-text",
        "reflect": false
      },
      "afAnnounceIfOptional": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set this to true if the form contains more required fields than optional fields."
            }],
          "text": "S\u00E4tt denna till true om formul\u00E4ret inneh\u00E5ller fler obligatoriska f\u00E4lt \u00E4n valfria."
        },
        "attribute": "af-announce-if-optional",
        "reflect": false,
        "defaultValue": "false"
      },
      "afHeadingFiles": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set heading for uploaded files."
            }],
          "text": "S\u00E4tt rubrik f\u00F6r uppladdade filer."
        },
        "attribute": "af-heading-files",
        "reflect": false,
        "defaultValue": "'Uppladdade filer'"
      },
      "afAnnounceIfOptionalText": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set text for afAnnounceIfOptional."
            }],
          "text": "S\u00E4tter text f\u00F6r afAnnounceIfOptional."
        },
        "attribute": "af-announce-if-optional-text",
        "reflect": false
      },
      "afValidation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "FormFileUploadValidation",
          "resolved": "FormFileUploadValidation.DISABLED | FormFileUploadValidation.ENABLED",
          "references": {
            "FormFileUploadValidation": {
              "location": "import",
              "path": "./form-file-upload-validation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set antivirus scan status. Enable is default."
            }],
          "text": "S\u00E4tter antivirus scan status. Kan vara 'enable' eller 'disabled."
        },
        "attribute": "af-validation",
        "reflect": false,
        "defaultValue": "FormFileUploadValidation.ENABLED"
      },
      "afHeadingLevel": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "FormFileUploadHeadingLevel",
          "resolved": "FormFileUploadHeadingLevel.H1 | FormFileUploadHeadingLevel.H2 | FormFileUploadHeadingLevel.H3 | FormFileUploadHeadingLevel.H4 | FormFileUploadHeadingLevel.H5 | FormFileUploadHeadingLevel.H6",
          "references": {
            "FormFileUploadHeadingLevel": {
              "location": "import",
              "path": "./form-file-upload-heading-level.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set heading level. Default is 'h2'"
            }],
          "text": "S\u00E4tt rubrikens vikt. 'h2' \u00E4r f\u00F6rvalt."
        },
        "attribute": "af-heading-level",
        "reflect": false,
        "defaultValue": "FormFileUploadHeadingLevel.H2"
      }
    };
  }
  static get states() {
    return {
      "_input": {},
      "files": {},
      "error": {},
      "errorMessage": {},
      "fileHover": {}
    };
  }
  static get events() {
    return [{
        "method": "afOnUploadFile",
        "name": "afOnUploadFile",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Emits file on upload"
            }],
          "text": "S\u00E4nder ut fil vid uppladdning"
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnRemoveFile",
        "name": "afOnRemoveFile",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Emits which file that was deleted."
            }],
          "text": "S\u00E4nder ut vilken fil som tagits bort"
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnCancelFile",
        "name": "afOnCancelFile",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Emits which file that was canceled"
            }],
          "text": "S\u00E4nder ut vilken fil som har avbrutis uppladdning"
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnRetryFile",
        "name": "afOnRetryFile",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Emits which file is trying to retry its upload"
            }],
          "text": "S\u00E4nder ut vilken fil som f\u00F6rs\u00F6ker laddas upp igen"
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }];
  }
  static get methods() {
    return {
      "afMValidateFile": {
        "complexType": {
          "signature": "(scanInfo: any) => Promise<void>",
          "parameters": [{
              "tags": [],
              "text": ""
            }],
          "references": {
            "Promise": {
              "location": "global"
            }
          },
          "return": "Promise<void>"
        },
        "docs": {
          "text": "Validera filens status.",
          "tags": [{
              "name": "en",
              "text": "Validate file status"
            }]
        }
      },
      "afMGetAllFiles": {
        "complexType": {
          "signature": "() => Promise<File[]>",
          "parameters": [],
          "references": {
            "Promise": {
              "location": "global"
            },
            "File": {
              "location": "global"
            }
          },
          "return": "Promise<File[]>"
        },
        "docs": {
          "text": "F\u00E5 ut alla uppladdade filer",
          "tags": [{
              "name": "en",
              "text": "Get all uploaded files"
            }]
        }
      },
      "afMImportFiles": {
        "complexType": {
          "signature": "(files: File[]) => Promise<void>",
          "parameters": [{
              "tags": [],
              "text": ""
            }],
          "references": {
            "Promise": {
              "location": "global"
            },
            "File": {
              "location": "global"
            }
          },
          "return": "Promise<void>"
        },
        "docs": {
          "text": "Importera en array med filer till komponenten utan att trigga uppladdningsevent. Ett fil objekt m\u00E5ste inneh\u00E5lla id, status och filen sj\u00E4lv.",
          "tags": [{
              "name": "en",
              "text": "Import an array with files without triggering upload event. A file object needs to contain an id, status and the file itself."
            }]
        }
      },
      "afMGetFormControlElement": {
        "complexType": {
          "signature": "() => Promise<HTMLInputElement>",
          "parameters": [],
          "references": {
            "Promise": {
              "location": "global"
            },
            "HTMLInputElement": {
              "location": "global"
            }
          },
          "return": "Promise<HTMLInputElement>"
        },
        "docs": {
          "text": "",
          "tags": []
        }
      }
    };
  }
  static get elementRef() { return "hostElement"; }
}
