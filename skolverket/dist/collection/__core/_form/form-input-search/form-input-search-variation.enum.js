export var FormInputSearchVariation;
(function (FormInputSearchVariation) {
  FormInputSearchVariation["SMALL"] = "small";
  FormInputSearchVariation["MEDIUM"] = "medium";
  FormInputSearchVariation["LARGE"] = "large";
})(FormInputSearchVariation || (FormInputSearchVariation = {}));
