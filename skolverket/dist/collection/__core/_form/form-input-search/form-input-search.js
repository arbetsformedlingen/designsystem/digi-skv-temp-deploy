import { h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { FormInputSearchVariation } from './form-input-search-variation.enum';
import { FormInputType } from '../form-input/form-input-type.enum';
import { ButtonType } from '../../_button/button/button-type.enum';
import { detectClickOutside } from '../../../global/utils/detectClickOutside';
import { detectFocusOutside } from '../../../global/utils/detectFocusOutside';
import { FormInputButtonVariation } from '../form-input/form-input-button-variation.enum';
/**
 * @enums FormInputSearchVariation - form-input-search-variation.enum.ts
 * @swedishName Sökfält
 */
export class FormInputSearch {
  constructor() {
    this.afLabel = undefined;
    this.afLabelDescription = undefined;
    this.afType = FormInputType.SEARCH;
    this.afButtonVariation = FormInputButtonVariation.PRIMARY;
    this.afName = undefined;
    this.afId = randomIdGenerator('digi-form-input-search');
    this.value = undefined;
    this.afValue = undefined;
    this.afAutocomplete = undefined;
    this.afAriaAutocomplete = undefined;
    this.afAriaActivedescendant = undefined;
    this.afAriaLabelledby = undefined;
    this.afAriaDescribedby = undefined;
    this.afVariation = FormInputSearchVariation.MEDIUM;
    this.afHideButton = undefined;
    this.afButtonType = ButtonType.SUBMIT;
    this.afButtonText = undefined;
    this.afButtonAriaLabel = undefined;
    this.afButtonAriaLabelledby = undefined;
    this.afAutofocus = undefined;
  }
  onValueChanged(value) {
    this.afValue = value;
  }
  onAfValueChanged(value) {
    this.value = value;
  }
  /**
   * Hämtar en referens till inputelementet. Bra för att t.ex. sätta fokus programmatiskt.
   * @en Returns a reference to the input element. Handy for setting focus programmatically.
   */
  async afMGetFormControlElement() {
    const formControlElement = await this._input.afMGetFormControlElement();
    return formControlElement;
  }
  blurHandler(e) {
    this.afOnBlur.emit(e);
  }
  changeHandler(e) {
    this.afValue = this.value = e.target.value;
    this.afOnChange.emit(e);
  }
  focusHandler(e) {
    this.afOnFocus.emit(e);
  }
  focusoutHandler(e) {
    this.afOnFocusout.emit(e);
  }
  keyupHandler(e) {
    this.afOnKeyup.emit(e);
  }
  inputHandler(e) {
    this.afOnInput.emit(e);
  }
  clickHandler(e) {
    this.afOnClick.emit(e);
  }
  clickOutsideListener(e) {
    const target = e.target;
    const selector = `[data-af-identifier="${this.afId}"]`;
    if (detectClickOutside(target, selector)) {
      this.afOnClickOutside.emit(e);
    }
    else {
      this.afOnClickInside.emit(e);
    }
  }
  focusOutsideListener(e) {
    const target = e.target;
    const selector = `[data-af-identifier="${this.afId}"]`;
    if (detectFocusOutside(target, selector)) {
      this.afOnFocusOutside.emit(e);
    }
    else {
      this.afOnFocusInside.emit(e);
    }
  }
  componentWillLoad() {
    this.afValue ? (this.value = this.afValue) : (this.afValue = this.value);
  }
  get cssModifiers() {
    return {
      'digi-form-input-search--small': this.afVariation === FormInputSearchVariation.SMALL,
      'digi-form-input-search--medium': this.afVariation === FormInputSearchVariation.MEDIUM,
      'digi-form-input-search--large': this.afVariation === FormInputSearchVariation.LARGE
    };
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-form-input-search': true }, this.cssModifiers), "data-af-identifier": this.afId }, h("digi-form-input", { class: {
        'digi-form-input-search__input': true,
        'digi-form-input-search__input--no-button': this.afHideButton
      }, ref: (el) => (this._input = el), onAfOnBlur: (e) => this.blurHandler(e), onAfOnChange: (e) => this.changeHandler(e), onAfOnFocus: (e) => this.focusHandler(e), onAfOnFocusout: (e) => this.focusoutHandler(e), onAfOnKeyup: (e) => this.keyupHandler(e), onAfOnInput: (e) => this.inputHandler(e), afLabel: this.afLabel, afLabelDescription: this.afLabelDescription, afAriaActivedescendant: this.afAriaActivedescendant, afAriaDescribedby: this.afAriaDescribedby, afAriaLabelledby: this.afAriaLabelledby, afAriaAutocomplete: this.afAriaAutocomplete, afAutocomplete: this.afAutocomplete, afName: this.afName, afType: this.afType, afValue: this.afValue, afVariation: this.afVariation, afAutofocus: this.afAutofocus ? this.afAutofocus : null, afButtonVariation: this.afButtonVariation }, !this.afHideButton && (h("digi-button", { class: "digi-form-input-search__button", onAfOnClick: (e) => this.clickHandler(e), afType: this.afButtonType, afAriaLabel: this.afButtonAriaLabel, afAriaLabelledby: this.afButtonAriaLabelledby, afSize: this.afVariation, slot: "button" }, h("digi-icon", { slot: "icon", afName: `search` }), this.afButtonText)))));
  }
  static get is() { return "digi-form-input-search"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["form-input-search.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["form-input-search.css"]
    };
  }
  static get properties() {
    return {
      "afLabel": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": true,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The label text"
            }],
          "text": "Texten till labelelementet"
        },
        "attribute": "af-label",
        "reflect": false
      },
      "afLabelDescription": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "A description text"
            }],
          "text": "Valfri beskrivande text"
        },
        "attribute": "af-label-description",
        "reflect": false
      },
      "afType": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "FormInputType",
          "resolved": "FormInputType.COLOR | FormInputType.DATE | FormInputType.DATETIME_LOCAL | FormInputType.EMAIL | FormInputType.HIDDEN | FormInputType.MONTH | FormInputType.NUMBER | FormInputType.PASSWORD | FormInputType.SEARCH | FormInputType.TEL | FormInputType.TEXT | FormInputType.TIME | FormInputType.URL | FormInputType.WEEK",
          "references": {
            "FormInputType": {
              "location": "import",
              "path": "../form-input/form-input-type.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input type attribute"
            }],
          "text": "S\u00E4tter attributet 'type'."
        },
        "attribute": "af-type",
        "reflect": false,
        "defaultValue": "FormInputType.SEARCH"
      },
      "afButtonVariation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "FormInputButtonVariation",
          "resolved": "FormInputButtonVariation.PRIMARY | FormInputButtonVariation.SECONDARY",
          "references": {
            "FormInputButtonVariation": {
              "location": "import",
              "path": "../form-input/form-input-button-variation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input layout when there is a button"
            }],
          "text": "Layout f\u00F6r hur inmatningsf\u00E4ltet ska visas d\u00E5 det finns en knapp"
        },
        "attribute": "af-button-variation",
        "reflect": false,
        "defaultValue": "FormInputButtonVariation.PRIMARY"
      },
      "afName": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input name attribute"
            }],
          "text": "S\u00E4tter attributet 'name'."
        },
        "attribute": "af-name",
        "reflect": false
      },
      "afId": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input id attribute. Defaults to random string."
            }],
          "text": "S\u00E4tter attributet 'id'. Om inget v\u00E4ljs s\u00E5 skapas ett slumpm\u00E4ssigt id."
        },
        "attribute": "af-id",
        "reflect": false,
        "defaultValue": "randomIdGenerator('digi-form-input-search')"
      },
      "value": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input value attribute"
            }, {
              "name": "ignore",
              "text": undefined
            }],
          "text": "S\u00E4tter attributet 'value'."
        },
        "attribute": "value",
        "reflect": false
      },
      "afValue": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input value attribute"
            }],
          "text": "S\u00E4tter attributet 'value'."
        },
        "attribute": "af-value",
        "reflect": false
      },
      "afAutocomplete": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input autocomplete attribute"
            }],
          "text": "S\u00E4tter attributet 'autocomplete'."
        },
        "attribute": "af-autocomplete",
        "reflect": false
      },
      "afAriaAutocomplete": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input aria-autocomplete attribute"
            }],
          "text": "S\u00E4tter attributet 'aria-autocomplete'."
        },
        "attribute": "af-aria-autocomplete",
        "reflect": false
      },
      "afAriaActivedescendant": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input aria-activedescendant attribute"
            }],
          "text": "S\u00E4tter attributet 'aria-activedescendant'."
        },
        "attribute": "af-aria-activedescendant",
        "reflect": false
      },
      "afAriaLabelledby": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input aria-labelledby attribute"
            }],
          "text": "S\u00E4tter attributet 'aria-labelledby'."
        },
        "attribute": "af-aria-labelledby",
        "reflect": false
      },
      "afAriaDescribedby": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input aria-describedby attribute"
            }],
          "text": "S\u00E4tter attributet 'aria-describedby'."
        },
        "attribute": "af-aria-describedby",
        "reflect": false
      },
      "afVariation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "FormInputSearchVariation",
          "resolved": "FormInputSearchVariation.LARGE | FormInputSearchVariation.MEDIUM | FormInputSearchVariation.SMALL",
          "references": {
            "FormInputSearchVariation": {
              "location": "import",
              "path": "./form-input-search-variation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set input variation."
            }],
          "text": "S\u00E4tter variant. Kan vara 'small', 'medium' eller 'large'"
        },
        "attribute": "af-variation",
        "reflect": false,
        "defaultValue": "FormInputSearchVariation.MEDIUM"
      },
      "afHideButton": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Hide the button"
            }],
          "text": "D\u00F6lj knappen"
        },
        "attribute": "af-hide-button",
        "reflect": false
      },
      "afButtonType": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "ButtonType",
          "resolved": "ButtonType.BUTTON | ButtonType.RESET | ButtonType.SUBMIT",
          "references": {
            "ButtonType": {
              "location": "import",
              "path": "../../_button/button/button-type.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set button element's type attribute."
            }],
          "text": "S\u00E4tter knappelementets attribut 'type'. B\u00F6r vara 'submit' eller 'button'"
        },
        "attribute": "af-button-type",
        "reflect": false,
        "defaultValue": "ButtonType.SUBMIT"
      },
      "afButtonText": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The button text"
            }],
          "text": "Knappens text"
        },
        "attribute": "af-button-text",
        "reflect": false
      },
      "afButtonAriaLabel": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The buttons aria label text"
            }],
          "text": "Knappens aria label text"
        },
        "attribute": "af-button-aria-label",
        "reflect": false
      },
      "afButtonAriaLabelledby": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [],
          "text": "Button aria-labelledby attribute"
        },
        "attribute": "af-button-aria-labelledby",
        "reflect": false
      },
      "afAutofocus": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input autofocus attribute"
            }],
          "text": "S\u00E4tter attributet 'autofocus'."
        },
        "attribute": "af-autofocus",
        "reflect": false
      }
    };
  }
  static get events() {
    return [{
        "method": "afOnFocusOutside",
        "name": "afOnFocusOutside",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "When focus is outside component."
            }],
          "text": "Vid fokus utanf\u00F6r komponenten."
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnFocusInside",
        "name": "afOnFocusInside",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "When focus is inside component."
            }],
          "text": "Vid fokus inuti komponenten."
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnClickOutside",
        "name": "afOnClickOutside",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "When click outside component."
            }],
          "text": "Vid klick utanf\u00F6r komponenten."
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnClickInside",
        "name": "afOnClickInside",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "When click inside component."
            }],
          "text": "Vid klick inuti komponenten."
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnChange",
        "name": "afOnChange",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The input element's 'onchange' event."
            }],
          "text": "Inputelementets 'onchange'-event."
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnBlur",
        "name": "afOnBlur",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The input element's 'onblur' event."
            }],
          "text": "Inputelementets 'onblur'-event."
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnKeyup",
        "name": "afOnKeyup",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The input element's 'onkeyup' event."
            }],
          "text": "Inputelementets 'onkeyup'-event."
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnFocus",
        "name": "afOnFocus",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The input element's 'onfocus' event."
            }],
          "text": "Inputelementets 'onfocus'-event."
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnFocusout",
        "name": "afOnFocusout",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The input element's 'onfocusout' event."
            }],
          "text": "Inputelementets 'onfocusout'-event."
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnInput",
        "name": "afOnInput",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The input element's 'oninput' event."
            }],
          "text": "Inputelementets 'oninput'-event."
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnClick",
        "name": "afOnClick",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The button element's 'onclick' event."
            }],
          "text": "Knappelementets 'onclick'-event."
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }];
  }
  static get methods() {
    return {
      "afMGetFormControlElement": {
        "complexType": {
          "signature": "() => Promise<any>",
          "parameters": [],
          "references": {
            "Promise": {
              "location": "global"
            }
          },
          "return": "Promise<any>"
        },
        "docs": {
          "text": "H\u00E4mtar en referens till inputelementet. Bra f\u00F6r att t.ex. s\u00E4tta fokus programmatiskt.",
          "tags": [{
              "name": "en",
              "text": "Returns a reference to the input element. Handy for setting focus programmatically."
            }]
        }
      }
    };
  }
  static get elementRef() { return "hostElement"; }
  static get watchers() {
    return [{
        "propName": "value",
        "methodName": "onValueChanged"
      }, {
        "propName": "afValue",
        "methodName": "onAfValueChanged"
      }];
  }
  static get listeners() {
    return [{
        "name": "click",
        "method": "clickOutsideListener",
        "target": "window",
        "capture": false,
        "passive": false
      }, {
        "name": "focusin",
        "method": "focusOutsideListener",
        "target": "document",
        "capture": false,
        "passive": false
      }];
  }
}
