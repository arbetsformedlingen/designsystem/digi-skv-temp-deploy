import { h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { FormInputButtonVariation } from './form-input-button-variation.enum';
import { FormInputType } from './form-input-type.enum';
import { FormInputValidation } from './form-input-validation.enum';
import { FormInputVariation } from './form-input-variation.enum';
/**
 * @enums FormInputType - form-input-type.enum.ts
 * @enums FormInputValidation - form-input-validation.enum.ts
 * @enums FormInputVariation - form-input-variation.enum.ts
 * @swedishName Inmatningsfält
 */
export class FormInput {
  constructor() {
    this.hasActiveValidationMessage = false;
    this.hasButton = undefined;
    this.dirty = false;
    this.touched = false;
    this.afLabel = undefined;
    this.afLabelDescription = undefined;
    this.afType = FormInputType.TEXT;
    this.afButtonVariation = FormInputButtonVariation.PRIMARY;
    this.afAutofocus = undefined;
    this.afVariation = FormInputVariation.MEDIUM;
    this.afName = undefined;
    this.afId = randomIdGenerator('digi-form-input');
    this.afMaxlength = undefined;
    this.afMinlength = undefined;
    this.afRequired = undefined;
    this.afRequiredText = undefined;
    this.afAnnounceIfOptional = false;
    this.afAnnounceIfOptionalText = undefined;
    this.value = undefined;
    this.afValue = undefined;
    this.afValidation = FormInputValidation.NEUTRAL;
    this.afValidationText = undefined;
    this.afRole = undefined;
    this.afAutocomplete = undefined;
    this.afMin = undefined;
    this.afMax = undefined;
    this.afList = undefined;
    this.afStep = undefined;
    this.afDirname = undefined;
    this.afAriaAutocomplete = undefined;
    this.afAriaActivedescendant = undefined;
    this.afAriaLabelledby = undefined;
    this.afAriaDescribedby = undefined;
  }
  onValueChanged(value) {
    this.afValue = value;
  }
  onAfValueChanged(value) {
    this.value = value;
  }
  afValidationTextWatch() {
    this.setActiveValidationMessage();
  }
  /**
   * Hämtar en referens till inputelementet. Bra för att t.ex. sätta fokus programmatiskt.
   * @en Returns a reference to the input element. Handy for setting focus programmatically.
   */
  async afMGetFormControlElement() {
    return this._input;
  }
  componentWillLoad() {
    this.afValue ? (this.value = this.afValue) : (this.afValue = this.value);
    this.setActiveValidationMessage();
    this.setHasButton();
  }
  componentWillUpdate() {
    this.setHasButton();
  }
  setActiveValidationMessage() {
    this.hasActiveValidationMessage = !!this.afValidationText;
  }
  get cssModifiers() {
    return {
      'digi-form-input--small': this.afVariation === FormInputVariation.SMALL,
      'digi-form-input--medium': this.afVariation === FormInputVariation.MEDIUM,
      'digi-form-input--large': this.afVariation === FormInputVariation.LARGE,
      'digi-form-input--neutral': this.afValidation === FormInputValidation.NEUTRAL,
      'digi-form-input--success': this.afValidation === FormInputValidation.SUCCESS,
      'digi-form-input--error': this.afValidation === FormInputValidation.ERROR,
      'digi-form-input--warning': this.afValidation === FormInputValidation.WARNING,
      [`digi-form-input--button-variation-${this.afButtonVariation}`]: !!this.afButtonVariation,
      [`digi-form-input--has-button-${this.hasButton}`]: true,
      'digi-form-input--empty': !this.afValue &&
        (!this.afValidation || this.afValidation === FormInputValidation.NEUTRAL)
    };
  }
  blurHandler(e) {
    if (!this.touched) {
      this.afOnTouched.emit(e);
      this.touched = true;
    }
    this.afOnBlur.emit(e);
  }
  changeHandler(e) {
    this.setActiveValidationMessage();
    this.afOnChange.emit(e);
  }
  focusHandler(e) {
    this.afOnFocus.emit(e);
  }
  focusoutHandler(e) {
    this.afOnFocusout.emit(e);
  }
  keyupHandler(e) {
    this.afOnKeyup.emit(e);
  }
  inputHandler(e) {
    this.afValue = this.value =
      this.afType === FormInputType.NUMBER
        ? parseFloat(e.target.value)
        : e.target.value;
    if (!this.dirty) {
      this.afOnDirty.emit(e);
      this.dirty = true;
    }
    this.setActiveValidationMessage();
    this.afOnInput.emit(e);
  }
  setHasButton() {
    this.hasButton = !!this.hostElement.querySelector('[slot="button"]');
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-form-input': true }, this.cssModifiers) }, h("digi-form-label", { class: "digi-form-input__label", afFor: this.afId, afLabel: this.afLabel, afDescription: this.afLabelDescription, afRequired: this.afRequired, afAnnounceIfOptional: this.afAnnounceIfOptional, afRequiredText: this.afRequiredText, afAnnounceIfOptionalText: this.afAnnounceIfOptionalText }), h("div", { class: "digi-form-input__input-wrapper" }, h("input", { class: "digi-form-input__input", ref: (el) => (this._input = el), onBlur: (e) => this.blurHandler(e), onChange: (e) => this.changeHandler(e), onFocus: (e) => this.focusHandler(e), onFocusout: (e) => this.focusoutHandler(e), onKeyUp: (e) => this.keyupHandler(e), onInput: (e) => this.inputHandler(e), "aria-activedescendant": this.afAriaActivedescendant, "aria-describedby": this.afAriaDescribedby, "aria-labelledby": this.afAriaLabelledby, "aria-autocomplete": this.afAriaAutocomplete, "aria-invalid": this.afValidation != FormInputValidation.NEUTRAL ? 'true' : 'false', autocomplete: this.afAutocomplete, autofocus: this.afAutofocus ? this.afAutofocus : null, maxLength: this.afMaxlength, minLength: this.afMinlength, max: this.afMax, min: this.afMin, step: this.afStep, list: this.afList, role: this.afRole, dirName: this.afDirname, required: this.afRequired, id: this.afId, name: this.afName, type: this.afType, value: this.afValue }), h("slot", { name: "button" })), h("div", { class: "digi-form-input__footer" }, h("div", { class: "digi-form-input__validation", "aria-atomic": "true", role: "alert", id: `${this.afId}--validation-message` }, this.hasActiveValidationMessage &&
      this.afValidation != FormInputValidation.NEUTRAL && (h("digi-form-validation-message", { class: "digi-form-input__validation-message", "af-variation": this.afValidation }, this.afValidationText))))));
  }
  static get is() { return "digi-form-input"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["form-input.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["form-input.css"]
    };
  }
  static get properties() {
    return {
      "afLabel": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": true,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The label text"
            }],
          "text": "Texten till labelelementet"
        },
        "attribute": "af-label",
        "reflect": false
      },
      "afLabelDescription": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "A description text"
            }],
          "text": "Valfri beskrivande text"
        },
        "attribute": "af-label-description",
        "reflect": false
      },
      "afType": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "FormInputType",
          "resolved": "FormInputType.COLOR | FormInputType.DATE | FormInputType.DATETIME_LOCAL | FormInputType.EMAIL | FormInputType.HIDDEN | FormInputType.MONTH | FormInputType.NUMBER | FormInputType.PASSWORD | FormInputType.SEARCH | FormInputType.TEL | FormInputType.TEXT | FormInputType.TIME | FormInputType.URL | FormInputType.WEEK",
          "references": {
            "FormInputType": {
              "location": "import",
              "path": "./form-input-type.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input type attribute"
            }],
          "text": "S\u00E4tter attributet 'type'."
        },
        "attribute": "af-type",
        "reflect": false,
        "defaultValue": "FormInputType.TEXT"
      },
      "afButtonVariation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "FormInputButtonVariation",
          "resolved": "FormInputButtonVariation.PRIMARY | FormInputButtonVariation.SECONDARY",
          "references": {
            "FormInputButtonVariation": {
              "location": "import",
              "path": "./form-input-button-variation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input layout when there is a button"
            }],
          "text": "Layout f\u00F6r hur inmatningsf\u00E4ltet ska visas d\u00E5 det finns en knapp"
        },
        "attribute": "af-button-variation",
        "reflect": false,
        "defaultValue": "FormInputButtonVariation.PRIMARY"
      },
      "afAutofocus": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input autofocus attribute"
            }],
          "text": "S\u00E4tter attributet 'autofocus'."
        },
        "attribute": "af-autofocus",
        "reflect": false
      },
      "afVariation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "FormInputVariation",
          "resolved": "FormInputVariation.LARGE | FormInputVariation.MEDIUM | FormInputVariation.SMALL",
          "references": {
            "FormInputVariation": {
              "location": "import",
              "path": "./form-input-variation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set input variation."
            }],
          "text": "S\u00E4tter variant. Kan vara 'small', 'medium' eller 'large'"
        },
        "attribute": "af-variation",
        "reflect": false,
        "defaultValue": "FormInputVariation.MEDIUM"
      },
      "afName": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input name attribute"
            }],
          "text": "S\u00E4tter attributet 'name'."
        },
        "attribute": "af-name",
        "reflect": false
      },
      "afId": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input id attribute. Defaults to random string."
            }],
          "text": "S\u00E4tter attributet 'id'. Om inget v\u00E4ljs s\u00E5 skapas ett slumpm\u00E4ssigt id."
        },
        "attribute": "af-id",
        "reflect": false,
        "defaultValue": "randomIdGenerator('digi-form-input')"
      },
      "afMaxlength": {
        "type": "number",
        "mutable": false,
        "complexType": {
          "original": "number",
          "resolved": "number",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input maxlength attribute"
            }],
          "text": "S\u00E4tter attributet 'maxlength'."
        },
        "attribute": "af-maxlength",
        "reflect": false
      },
      "afMinlength": {
        "type": "number",
        "mutable": false,
        "complexType": {
          "original": "number",
          "resolved": "number",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input minlength attribute"
            }],
          "text": "S\u00E4tter attributet 'minlength'."
        },
        "attribute": "af-minlength",
        "reflect": false
      },
      "afRequired": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input required attribute"
            }],
          "text": "S\u00E4tter attributet 'required'."
        },
        "attribute": "af-required",
        "reflect": false
      },
      "afRequiredText": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set text for afRequired."
            }],
          "text": "S\u00E4tter text f\u00F6r afRequired."
        },
        "attribute": "af-required-text",
        "reflect": false
      },
      "afAnnounceIfOptional": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set this to true if the form contains more required fields than optional fields."
            }],
          "text": "S\u00E4tt denna till true om formul\u00E4ret inneh\u00E5ller fler obligatoriska f\u00E4lt \u00E4n valfria."
        },
        "attribute": "af-announce-if-optional",
        "reflect": false,
        "defaultValue": "false"
      },
      "afAnnounceIfOptionalText": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set text for afAnnounceIfOptional."
            }],
          "text": "S\u00E4tter text f\u00F6r afAnnounceIfOptional."
        },
        "attribute": "af-announce-if-optional-text",
        "reflect": false
      },
      "value": {
        "type": "any",
        "mutable": false,
        "complexType": {
          "original": "string | number",
          "resolved": "number | string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input value attribute"
            }, {
              "name": "ignore",
              "text": undefined
            }],
          "text": "S\u00E4tter attributet 'value'."
        },
        "attribute": "value",
        "reflect": false
      },
      "afValue": {
        "type": "any",
        "mutable": false,
        "complexType": {
          "original": "string | number",
          "resolved": "number | string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input value attribute"
            }],
          "text": "S\u00E4tter attributet 'value'."
        },
        "attribute": "af-value",
        "reflect": false
      },
      "afValidation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "FormInputValidation",
          "resolved": "FormInputValidation.ERROR | FormInputValidation.NEUTRAL | FormInputValidation.SUCCESS | FormInputValidation.WARNING",
          "references": {
            "FormInputValidation": {
              "location": "import",
              "path": "./form-input-validation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set input validation status"
            }],
          "text": "S\u00E4tter valideringsstatus. Kan vara 'success', 'error', 'warning', 'neutral' eller ingenting."
        },
        "attribute": "af-validation",
        "reflect": false,
        "defaultValue": "FormInputValidation.NEUTRAL"
      },
      "afValidationText": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set input validation message"
            }],
          "text": "S\u00E4tter valideringsmeddelandet"
        },
        "attribute": "af-validation-text",
        "reflect": false
      },
      "afRole": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input role attribute"
            }],
          "text": "S\u00E4tter attributet 'role'."
        },
        "attribute": "af-role",
        "reflect": false
      },
      "afAutocomplete": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input autocomplete attribute"
            }],
          "text": "S\u00E4tter attributet 'autocomplete'."
        },
        "attribute": "af-autocomplete",
        "reflect": false
      },
      "afMin": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input min attribute"
            }],
          "text": "S\u00E4tter attributet 'min'."
        },
        "attribute": "af-min",
        "reflect": false
      },
      "afMax": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input max attribute"
            }],
          "text": "S\u00E4tter attributet 'max'."
        },
        "attribute": "af-max",
        "reflect": false
      },
      "afList": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input list attribute"
            }],
          "text": "S\u00E4tter attributet 'list'."
        },
        "attribute": "af-list",
        "reflect": false
      },
      "afStep": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input step attribute"
            }],
          "text": "S\u00E4tter attributet 'step'."
        },
        "attribute": "af-step",
        "reflect": false
      },
      "afDirname": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input dirname attribute"
            }],
          "text": "S\u00E4tter attributet 'dirname'."
        },
        "attribute": "af-dirname",
        "reflect": false
      },
      "afAriaAutocomplete": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input aria-autocomplete attribute"
            }],
          "text": "S\u00E4tter attributet 'aria-autocomplete'."
        },
        "attribute": "af-aria-autocomplete",
        "reflect": false
      },
      "afAriaActivedescendant": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input aria-activedescendant attribute"
            }],
          "text": "S\u00E4tter attributet 'aria-activedescendant'."
        },
        "attribute": "af-aria-activedescendant",
        "reflect": false
      },
      "afAriaLabelledby": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input aria-labelledby attribute"
            }],
          "text": "S\u00E4tter attributet 'aria-labelledby'."
        },
        "attribute": "af-aria-labelledby",
        "reflect": false
      },
      "afAriaDescribedby": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input aria-describedby attribute"
            }],
          "text": "S\u00E4tter attributet 'aria-describedby'."
        },
        "attribute": "af-aria-describedby",
        "reflect": false
      }
    };
  }
  static get states() {
    return {
      "hasActiveValidationMessage": {},
      "hasButton": {},
      "dirty": {},
      "touched": {}
    };
  }
  static get events() {
    return [{
        "method": "afOnChange",
        "name": "afOnChange",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The input element's 'onchange' event."
            }],
          "text": "Inputelementets 'onchange'-event."
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnBlur",
        "name": "afOnBlur",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The input element's 'onblur' event."
            }],
          "text": "Inputelementets 'onblur'-event."
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnKeyup",
        "name": "afOnKeyup",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The input element's 'onkeyup' event."
            }],
          "text": "Inputelementets 'onkeyup'-event."
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnFocus",
        "name": "afOnFocus",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The input element's 'onfocus' event."
            }],
          "text": "Inputelementets 'onfocus'-event."
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnFocusout",
        "name": "afOnFocusout",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The input element's 'onfocusout' event."
            }],
          "text": "Inputelementets 'onfocusout'-event."
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnInput",
        "name": "afOnInput",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The input element's 'oninput' event."
            }],
          "text": "Inputelementets 'oninput'-event."
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnDirty",
        "name": "afOnDirty",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "First time the input element receives an input"
            }],
          "text": "Sker vid inputf\u00E4ltets f\u00F6rsta 'oninput'"
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnTouched",
        "name": "afOnTouched",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "First time the input element is blurred"
            }],
          "text": "Sker vid inputf\u00E4ltets f\u00F6rsta 'onblur'"
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }];
  }
  static get methods() {
    return {
      "afMGetFormControlElement": {
        "complexType": {
          "signature": "() => Promise<HTMLInputElement>",
          "parameters": [],
          "references": {
            "Promise": {
              "location": "global"
            },
            "HTMLInputElement": {
              "location": "global"
            }
          },
          "return": "Promise<HTMLInputElement>"
        },
        "docs": {
          "text": "H\u00E4mtar en referens till inputelementet. Bra f\u00F6r att t.ex. s\u00E4tta fokus programmatiskt.",
          "tags": [{
              "name": "en",
              "text": "Returns a reference to the input element. Handy for setting focus programmatically."
            }]
        }
      }
    };
  }
  static get elementRef() { return "hostElement"; }
  static get watchers() {
    return [{
        "propName": "value",
        "methodName": "onValueChanged"
      }, {
        "propName": "afValue",
        "methodName": "onAfValueChanged"
      }, {
        "propName": "afValidationText",
        "methodName": "afValidationTextWatch"
      }];
  }
}
