export var FormInputType;
(function (FormInputType) {
  FormInputType["COLOR"] = "color";
  FormInputType["DATE"] = "date";
  FormInputType["DATETIME_LOCAL"] = "datetime-local";
  FormInputType["EMAIL"] = "email";
  FormInputType["HIDDEN"] = "hidden";
  FormInputType["MONTH"] = "month";
  FormInputType["NUMBER"] = "number";
  FormInputType["PASSWORD"] = "password";
  FormInputType["SEARCH"] = "search";
  FormInputType["TEL"] = "tel";
  FormInputType["TEXT"] = "text";
  FormInputType["TIME"] = "time";
  FormInputType["URL"] = "url";
  FormInputType["WEEK"] = "week";
})(FormInputType || (FormInputType = {}));
