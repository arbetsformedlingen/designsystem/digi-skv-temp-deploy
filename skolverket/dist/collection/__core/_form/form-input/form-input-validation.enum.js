export var FormInputValidation;
(function (FormInputValidation) {
  FormInputValidation["SUCCESS"] = "success";
  FormInputValidation["ERROR"] = "error";
  FormInputValidation["WARNING"] = "warning";
  FormInputValidation["NEUTRAL"] = "neutral";
})(FormInputValidation || (FormInputValidation = {}));
