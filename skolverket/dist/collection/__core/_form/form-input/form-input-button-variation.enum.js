export var FormInputButtonVariation;
(function (FormInputButtonVariation) {
  FormInputButtonVariation["PRIMARY"] = "primary";
  FormInputButtonVariation["SECONDARY"] = "secondary";
})(FormInputButtonVariation || (FormInputButtonVariation = {}));
