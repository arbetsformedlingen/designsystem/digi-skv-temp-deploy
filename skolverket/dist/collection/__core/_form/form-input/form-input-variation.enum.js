export var FormInputVariation;
(function (FormInputVariation) {
  FormInputVariation["SMALL"] = "small";
  FormInputVariation["MEDIUM"] = "medium";
  FormInputVariation["LARGE"] = "large";
})(FormInputVariation || (FormInputVariation = {}));
