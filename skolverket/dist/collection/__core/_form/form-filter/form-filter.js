import { h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { logger } from '../../../global/utils/logger';
import { detectClickOutside } from '../../../global/utils/detectClickOutside';
import { detectFocusOutside } from '../../../global/utils/detectFocusOutside';
/**
 * @slot default - Ska vara formulärselement. Till exempel digi-form-checkbox.
 * @swedishName Multifilter
 */
export class formFilter {
  constructor() {
    this._updateActiveItems = false;
    this.activeList = [];
    this.isActive = false;
    this.activeListItemIndex = 0;
    this.hasActiveItems = false;
    this.focusInList = false;
    this.listItems = [];
    this.afId = randomIdGenerator('digi-form-filter');
    this.afFilterButtonText = undefined;
    this.afSubmitButtonText = undefined;
    this.afSubmitButtonTextAriaLabel = undefined;
    this.afResetButtonText = 'Rensa';
    this.afResetButtonTextAriaLabel = undefined;
    this.afHideResetButton = false;
    this.afAlignRight = undefined;
    this.afName = undefined;
    this.afFilterButtonAriaLabel = undefined;
    this.afFilterButtonAriaDescribedby = undefined;
    this.afAutofocus = undefined;
  }
  clickHandler(e) {
    const target = e.target;
    const selector = `#${this.afId}-identifier`;
    if (detectClickOutside(target, selector) && this.isActive) {
      this.emitFilterClosedEvent();
    }
  }
  focusoutHandler(e) {
    const target = e.target;
    const selector = `#${this.afId}-identifier`;
    const focusOutsideFilter = detectFocusOutside(target, selector);
    if (focusOutsideFilter && this.isActive) {
      this.emitFilterClosedEvent();
    }
    else {
      this.focusInList = !detectFocusOutside(target, `#${this.afId}-filter-list`);
      //Update tabindex on focused input element
      if (this.isActive && this.focusInList && target.tagName === 'INPUT') {
        this.setTabIndex();
        this.updateTabIndex(target);
      }
      this.afOnFocusout.emit(e);
    }
  }
  changeHandler(e) {
    if (e.target.tagName === 'INPUT') {
      this.setSelectedItems(e.target);
      this.hasActiveItems = this.activeList.length > 0 ? true : false;
      const dataKey = e.target.closest('.digi-form-filter__item')
        ? e.target.closest('.digi-form-filter__item').getAttribute('data-key')
        : this.activeListItemIndex;
      this.activeListItemIndex = Number(dataKey);
      this.afOnChange.emit(e);
    }
  }
  debounce(func, timeout = 300) {
    let timer;
    return (...args) => {
      clearTimeout(timer);
      timer = setTimeout(() => {
        func.apply(this, args);
      }, timeout);
    };
  }
  setSelectedItems(item) {
    this.activeList.some((el) => el.id === item.id)
      ? (this.activeList = this.activeList.filter((f) => f.id !== item.id))
      : this.activeList.push(item);
  }
  setInactive(focusToggleButton = false) {
    this.isActive = false;
    if (focusToggleButton) {
      document.getElementById(`${this.afId}-toggle-button`).focus();
    }
    document.querySelector(`#${this.afId}-dropdown-menu .digi-form-filter__scroll`).scrollTop = 0;
  }
  setActive() {
    this.isActive = true;
    this.activeListItemIndex = 0;
    setTimeout(() => {
      this.focusActiveItem();
    }, 200);
  }
  toggleDropdown() {
    return this.isActive ? this.setInactive() : this.setActive();
  }
  inputItem(identifier, index) {
    const item = document.querySelector(`#${identifier} .digi-form-filter__item:nth-child(${index}) input`);
    return item;
  }
  getFilterData() {
    return {
      filter: this.afName,
      items: this.activeList.map((item) => {
        return {
          id: item.id,
          text: item.labels[0].htmlFor === item.id ? item.labels[0].innerText : ''
        };
      })
    };
  }
  emitFilterClosedEvent() {
    this.setInactive(true);
    this.afOnFilterClosed.emit(this.getFilterData());
  }
  submitFilters(setInactive = true) {
    if (setInactive) {
      this.setInactive(true);
    }
    this.afOnSubmitFilters.emit(this.getFilterData());
  }
  resetFilters() {
    this.listItems.map((_, i) => {
      this.inputItem(`${this.afId}-filter-list`, i + 1).checked = false;
    });
    this.activeList = [];
    this.hasActiveItems = false;
    this.afOnResetFilters.emit(this.afName);
  }
  setupListElements(collection) {
    if (!collection || collection.length <= 0) {
      logger.warn(`The slot contains no filter elements.`, this.hostElement);
      return;
    }
    this.listItems = Array.from(collection).map((element, i) => ({
      index: i,
      outerHTML: element.cloneNode().outerHTML
    }));
  }
  componentWillLoad() {
    this.setupListElements(this.hostElement.children);
  }
  setupActiveItems() {
    this.setTabIndex();
    setTimeout(() => {
      const rootElement = document.getElementById(`${this.afId}-identifier`);
      let activeInputs = Array.from(rootElement === null || rootElement === void 0 ? void 0 : rootElement.querySelectorAll('.digi-form-filter__list input:checked'));
      this.activeList = [];
      activeInputs.forEach((inputEl) => {
        this.setSelectedItems(inputEl);
      });
      this.hasActiveItems = this.activeList.length > 0 ? true : false;
    });
  }
  componentDidLoad() {
    this.setupActiveItems();
  }
  componentDidUpdate() {
    if (!this._updateActiveItems) {
      return;
    }
    this._updateActiveItems = false;
    this.setupActiveItems();
  }
  focusActiveItem() {
    const element = this.inputItem(`${this.afId}-filter-list`, this.activeListItemIndex + 1);
    element.focus();
  }
  incrementActiveItem() {
    if (this.activeListItemIndex < this.listItems.length - 1) {
      this.activeListItemIndex = this.activeListItemIndex + 1;
      this.focusActiveItem();
      return;
    }
    this.tabHandler();
  }
  setTabIndex() {
    const tabindexes = document.querySelectorAll(`#${this.afId}-dropdown-menu`);
    if (!tabindexes) {
      logger.warn(`No input elements was found to set tabindex on.`, this.hostElement);
      return;
    }
    Array.from(tabindexes).map((tabindex) => {
      tabindex.setAttribute('tabindex', '-1');
    });
  }
  updateTabIndex(element) {
    if (!element) {
      logger.warn(`No input element was found to update tabindex on.`, this.hostElement);
      return;
    }
    element.setAttribute('tabindex', '0');
  }
  decrementActiveItem() {
    if (this.activeListItemIndex === 0) {
      this._toggleButton.querySelector('button').focus();
      return;
    }
    if (this.activeListItemIndex > 0) {
      this.activeListItemIndex = this.activeListItemIndex - 1;
    }
    this.focusActiveItem();
  }
  downHandler() {
    this.incrementActiveItem();
  }
  upHandler() {
    this.decrementActiveItem();
  }
  homeHandler() {
    this.activeListItemIndex = 0;
    this.focusActiveItem();
  }
  endHandler() {
    this.activeListItemIndex = this.listItems.length - 1;
    this.focusActiveItem();
  }
  enterHandler(e) {
    e.detail.target.click();
  }
  escHandler() {
    if (this.isActive) {
      this.setInactive(true);
    }
  }
  tabHandler() {
    if (this.isActive && this.focusInList) {
      setTimeout(() => {
        this._submitButton.querySelector('button').focus();
      }, 10);
    }
  }
  shiftHandler() {
    if (this.isActive && this.focusInList) {
      this._toggleButton.querySelector('button').focus();
      this.activeListItemIndex =
        this.activeListItemIndex >= 0
          ? this.activeListItemIndex - 1
          : this.activeListItemIndex;
    }
  }
  get cssModifiers() {
    return {
      'digi-form-filter--open': this.isActive
    };
  }
  get cssModifiersToggleButtonIndicator() {
    return {
      'digi-form-filter__toggle-button-indicator--active': this.hasActiveItems
    };
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-form-filter': true }, this.cssModifiers), role: "application", id: `${this.afId}-identifier` }, h("digi-util-keydown-handler", { onAfOnEsc: () => this.escHandler() }, h("digi-button", { id: `${this.afId}-toggle-button`, class: "digi-form-filter__toggle-button", onAfOnClick: () => this.toggleDropdown(), ref: (el) => (this._toggleButton = el), "af-aria-haspopup": "true", "af-variation": "secondary", "af-aria-label": this.afFilterButtonAriaLabel, "af-aria-labelledby": this.afFilterButtonAriaDescribedby, "af-aria-controls": `${this.afId}-dropdown-menu`, "af-aria-expanded": this.isActive, "af-autofocus": this.afAutofocus ? this.afAutofocus : null }, h("i", { class: Object.assign({ 'digi-form-filter__toggle-button-indicator': true }, this.cssModifiersToggleButtonIndicator) }), h("span", { class: "digi-form-filter__toggle-button-text" }, this.afFilterButtonText), h("digi-icon", { class: "digi-form-filter__toggle-icon", slot: "icon-secondary", afName: `chevron-down` })), h("div", { hidden: !this.isActive, id: `${this.afId}-dropdown-menu`, class: {
        'digi-form-filter__dropdown': true,
        'digi-form-filter__dropdown--right': this.afAlignRight
      } }, h("div", { class: "digi-form-filter__scroll" }, h("fieldset", null, h("legend", { class: "digi-form-filter__legend" }, this.afName), h("digi-util-keydown-handler", { onAfOnDown: () => this.downHandler(), onAfOnUp: () => this.upHandler(), onAfOnHome: () => this.homeHandler(), onAfOnEnd: () => this.endHandler(), onAfOnEnter: (e) => this.enterHandler(e), onAfOnTab: () => this.tabHandler(), onAfOnShiftTab: () => this.shiftHandler() }, h("ul", { class: "digi-form-filter__list", "aria-labelledby": `${this.afId}-toggle-button`, id: `${this.afId}-filter-list` }, this.listItems.map((item) => {
      return (h("li", { class: "digi-form-filter__item", key: item.index, "data-key": item.index, innerHTML: item.outerHTML }));
    }))), h("digi-util-mutation-observer", { hidden: true, ref: (el) => (this._observer = el), onAfOnChange: this.debounce(() => {
        this.setupListElements(this._observer.children);
        this._updateActiveItems = true;
      }) }, h("slot", null)))), h("div", { class: "digi-form-filter__footer" }, h("digi-util-keydown-handler", { onAfOnUp: () => this.focusActiveItem() }, h("digi-button", { ref: (el) => (this._submitButton = el), "af-variation": "primary", "af-aria-label": this.afSubmitButtonTextAriaLabel, onClick: () => {
        this.submitFilters();
        this._toggleButton.querySelector('button').focus();
      }, class: "digi-form-filter__submit-button" }, this.afSubmitButtonText)), !this.afHideResetButton && (h("digi-button", { "af-variation": "secondary", "af-aria-label": this.afResetButtonTextAriaLabel, onClick: () => this.resetFilters(), class: "digi-form-filter__reset-button" }, this.afResetButtonText)))))));
  }
  static get is() { return "digi-form-filter"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["form-filter.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["form-filter.css"]
    };
  }
  static get properties() {
    return {
      "afId": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Prefix for all id attributes inside the component. Example: 'my-cool-id' generates 'my-cool-id-filter-list', 'my-cool-id-dropdown-menu' and so on. Defaults to random string."
            }],
          "text": "Prefixet f\u00F6r alla komponentens olika interna 'id'-attribut p\u00E5 dropdowns, filterlistor etc. Ex. 'my-cool-id' genererar 'my-cool-id-filter-list', 'my-cool-id-dropdown-menu' och s\u00E5 vidare. Ges inget v\u00E4rde s\u00E5 genereras ett slumpm\u00E4ssigt."
        },
        "attribute": "af-id",
        "reflect": false,
        "defaultValue": "randomIdGenerator('digi-form-filter')"
      },
      "afFilterButtonText": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": true,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set the filter button text"
            }],
          "text": "Texten i filterknappen"
        },
        "attribute": "af-filter-button-text",
        "reflect": false
      },
      "afSubmitButtonText": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": true,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set the submit button text"
            }],
          "text": "Texten i submitknappen"
        },
        "attribute": "af-submit-button-text",
        "reflect": false
      },
      "afSubmitButtonTextAriaLabel": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set the submit button aria-label text"
            }],
          "text": "Sk\u00E4rml\u00E4sartext f\u00F6r submitknappen"
        },
        "attribute": "af-submit-button-text-aria-label",
        "reflect": false
      },
      "afResetButtonText": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set the reset button text"
            }],
          "text": "Texten i rensaknappen"
        },
        "attribute": "af-reset-button-text",
        "reflect": false,
        "defaultValue": "'Rensa'"
      },
      "afResetButtonTextAriaLabel": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set the reset button aria-label text"
            }],
          "text": "Sk\u00E4rml\u00E4sartext f\u00F6r resetknappen"
        },
        "attribute": "af-reset-button-text-aria-label",
        "reflect": false
      },
      "afHideResetButton": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Hides the reset button when set to true"
            }],
          "text": "D\u00F6ljer rensaknappen n\u00E4r v\u00E4rdet s\u00E4tts till true"
        },
        "attribute": "af-hide-reset-button",
        "reflect": false,
        "defaultValue": "false"
      },
      "afAlignRight": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Align dropdown from right to left"
            }],
          "text": "Justera dropdown fr\u00E5n h\u00F6ger till v\u00E4nster"
        },
        "attribute": "af-align-right",
        "reflect": false
      },
      "afName": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Binding name of filterlist selected filters on submit. Also used by the legend element."
            }],
          "text": "Namnet p\u00E5 filterlistans valda filter vid submit. Anv\u00E4nds \u00E4ven av legendelementet inne i filterlistan."
        },
        "attribute": "af-name",
        "reflect": false
      },
      "afFilterButtonAriaLabel": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input aria-label attribute on filter button"
            }],
          "text": "S\u00E4tter attributet 'aria-label' p\u00E5 filterknappen"
        },
        "attribute": "af-filter-button-aria-label",
        "reflect": false
      },
      "afFilterButtonAriaDescribedby": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input aria-describedby attribute on filter button"
            }],
          "text": "S\u00E4tter attributet 'aria-describedby' p\u00E5 filterknappen"
        },
        "attribute": "af-filter-button-aria-describedby",
        "reflect": false
      },
      "afAutofocus": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input autofocus attribute"
            }],
          "text": "S\u00E4tter attributet 'autofocus'."
        },
        "attribute": "af-autofocus",
        "reflect": false
      }
    };
  }
  static get states() {
    return {
      "isActive": {},
      "activeListItemIndex": {},
      "hasActiveItems": {},
      "focusInList": {},
      "listItems": {}
    };
  }
  static get events() {
    return [{
        "method": "afOnFocusout",
        "name": "afOnFocusout",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [],
          "text": ""
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnSubmitFilters",
        "name": "afOnSubmitFilters",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "At filter submit"
            }],
          "text": "Vid uppdatering av filtret"
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnFilterClosed",
        "name": "afOnFilterClosed",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "When the filter is closed without confirming the selected options"
            }],
          "text": "N\u00E4r filtret st\u00E4ngs utan att valda alternativ bekr\u00E4ftats"
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnResetFilters",
        "name": "afOnResetFilters",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "At filter reset"
            }],
          "text": "Vid reset av filtret"
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnChange",
        "name": "afOnChange",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "When a filter is changed"
            }],
          "text": "N\u00E4r ett filter \u00E4ndras"
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }];
  }
  static get elementRef() { return "hostElement"; }
  static get listeners() {
    return [{
        "name": "click",
        "method": "clickHandler",
        "target": "window",
        "capture": false,
        "passive": false
      }, {
        "name": "focusin",
        "method": "focusoutHandler",
        "target": "document",
        "capture": false,
        "passive": false
      }, {
        "name": "change",
        "method": "changeHandler",
        "target": undefined,
        "capture": false,
        "passive": false
      }];
  }
}
