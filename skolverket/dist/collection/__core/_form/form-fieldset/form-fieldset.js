import { h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
/**
 * @slot default - Ska vara formulärselement. Till exempel digi-form-input.
 * @swedishName Fältgruppering
 */
export class FormFieldset {
  constructor() {
    this.afLegend = undefined;
    this.afName = undefined;
    this.afForm = undefined;
    this.afId = randomIdGenerator('digi-form-fieldset');
  }
  render() {
    return (h("fieldset", { class: "digi-form-fieldset", name: this.afName, form: this.afForm, id: this.afId }, this.afLegend && (h("legend", { class: "digi-form-fieldset__legend" }, this.afLegend)), h("slot", null)));
  }
  static get is() { return "digi-form-fieldset"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["form-fieldset.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["form-fieldset.css"]
    };
  }
  static get properties() {
    return {
      "afLegend": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The legend text."
            }],
          "text": "Legend-elementets text."
        },
        "attribute": "af-legend",
        "reflect": false
      },
      "afName": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The fieldset name attribute."
            }],
          "text": "Fieldset-elementets namn attribut."
        },
        "attribute": "af-name",
        "reflect": false
      },
      "afForm": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The fieldset form attribute."
            }],
          "text": "Fieldset-elementets form attribut."
        },
        "attribute": "af-form",
        "reflect": false
      },
      "afId": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input id attribute. Defaults to random string."
            }],
          "text": "S\u00E4tter attributet 'id'. Om inget v\u00E4ljs s\u00E5 skapas ett slumpm\u00E4ssigt id."
        },
        "attribute": "af-id",
        "reflect": false,
        "defaultValue": "randomIdGenerator('digi-form-fieldset')"
      }
    };
  }
}
