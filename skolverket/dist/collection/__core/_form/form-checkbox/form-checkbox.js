import { h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { FormCheckboxVariation } from './form-checkbox-variation.enum';
import { FormCheckboxValidation } from './form-checkbox-validation.enum';
import { FormCheckboxLayout } from './form-checkbox-layout.enum';
/**
 * @enums FormCheckboxValidation - form-checkbox-validation.enum.ts
 * @enums FormCheckboxVariation - form-checkbox-variation.enum.ts
 * @swedishName Kryssruta
 */
export class FormCheckbox {
  constructor() {
    this.dirty = false;
    this.touched = false;
    this.afLabel = undefined;
    this.afVariation = FormCheckboxVariation.PRIMARY;
    this.afLayout = FormCheckboxLayout.INLINE;
    this.afIndeterminate = false;
    this.afName = undefined;
    this.checked = undefined;
    this.afChecked = undefined;
    this.afId = randomIdGenerator('digi-form-checkbox');
    this.afRequired = undefined;
    this.value = undefined;
    this.afValue = undefined;
    this.afValidation = undefined;
    this.afAriaLabelledby = undefined;
    this.afAriaLabel = undefined;
    this.afDescription = undefined;
    this.afAriaDescribedby = undefined;
    this.afAutofocus = undefined;
  }
  onCheckedChanged(checked) {
    this.afChecked = checked;
  }
  onAfCheckedChanged(checked) {
    this.checked = checked;
  }
  onValueChanged(value) {
    this.afValue = value;
  }
  onAfValueChanged(value) {
    this.value = value;
  }
  /**
   * Hämta en referens till inputelementet. Bra för att t.ex. sätta fokus programmatiskt.
   * @en Returns a reference to the input element. Handy for setting focus programmatically.
   */
  async afMGetFormControlElement() {
    return this._input;
  }
  componentWillLoad() {
    this.afValue ? (this.value = this.afValue) : (this.afValue = this.value);
    this.afChecked
      ? (this.checked = this.afChecked)
      : (this.afChecked = this.checked);
  }
  get cssModifiers() {
    return {
      'digi-form-checkbox--error': this.afValidation === FormCheckboxValidation.ERROR,
      'digi-form-checkbox--primary': this.afVariation === FormCheckboxVariation.PRIMARY,
      'digi-form-checkbox--secondary': this.afVariation === FormCheckboxVariation.SECONDARY,
      'digi-form-checkbox--indeterminate': this.afIndeterminate,
      [`digi-form-checkbox--layout-${this.afLayout}`]: !!this.afLayout
    };
  }
  blurHandler(e) {
    if (!this.touched) {
      this.afOnTouched.emit(e);
      this.touched = true;
    }
    this.afOnBlur.emit(e);
  }
  changeHandler(e) {
    this.checked = this.afChecked = e.target.checked;
    this.afValue = this.value = e.target.value;
    this.afOnChange.emit(e);
  }
  focusHandler(e) {
    this.afOnFocus.emit(e);
  }
  focusoutHandler(e) {
    this.afOnFocusout.emit(e);
  }
  inputHandler(e) {
    if (!this.dirty) {
      this.afOnDirty.emit(e);
      this.dirty = true;
    }
    this.afOnInput.emit(e);
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-form-checkbox': true }, this.cssModifiers) }, h("input", { class: "digi-form-checkbox__input", ref: (el) => (this._input = el), onInput: (e) => this.inputHandler(e), onBlur: (e) => this.blurHandler(e), onChange: (e) => this.changeHandler(e), onFocus: (e) => this.focusHandler(e), onFocusout: (e) => this.focusoutHandler(e), "aria-describedby": !this.afAriaDescribedby && !this.afDescription
        ? null
        : `${!!this.afAriaDescribedby ? `${this.afAriaDescribedby} ` : ''}${!!this.afDescription ? `${this.afId}-description` : ''}`, "aria-labelledby": this.afAriaLabelledby, "aria-label": this.afAriaLabel, "aria-invalid": this.afValidation === FormCheckboxValidation.ERROR ? 'true' : 'false', required: this.afRequired, id: this.afId, name: this.afName, type: "checkbox", checked: this.afChecked, value: this.afValue, indeterminate: this.afIndeterminate, autofocus: this.afAutofocus ? this.afAutofocus : null }), h("label", { htmlFor: this.afId, class: "digi-form-checkbox__label" }, h("span", { class: "digi-form-checkbox__box" }), this.afLabel), !!this.afDescription && (h("p", { id: `${this.afId}-description`, class: "digi-form-checkbox__description" }, this.afDescription))));
  }
  static get is() { return "digi-form-checkbox"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["form-checkbox.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["form-checkbox.css"]
    };
  }
  static get properties() {
    return {
      "afLabel": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": true,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The label text"
            }],
          "text": "Texten till labelelementet"
        },
        "attribute": "af-label",
        "reflect": false
      },
      "afVariation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "FormCheckboxVariation",
          "resolved": "FormCheckboxVariation.PRIMARY | FormCheckboxVariation.SECONDARY",
          "references": {
            "FormCheckboxVariation": {
              "location": "import",
              "path": "./form-checkbox-variation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set checkbox variation."
            }],
          "text": "S\u00E4tter variant. Kan vara 'primary' eller 'secondary'"
        },
        "attribute": "af-variation",
        "reflect": false,
        "defaultValue": "FormCheckboxVariation.PRIMARY"
      },
      "afLayout": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "FormCheckboxLayout",
          "resolved": "FormCheckboxLayout.BLOCK | FormCheckboxLayout.INLINE",
          "references": {
            "FormCheckboxLayout": {
              "location": "import",
              "path": "./form-checkbox-layout.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set checkbox layout."
            }],
          "text": "S\u00E4tter layouten."
        },
        "attribute": "af-layout",
        "reflect": false,
        "defaultValue": "FormCheckboxLayout.INLINE"
      },
      "afIndeterminate": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set indeterminate property."
            }],
          "text": "S\u00E4tter property 'indeterminate' p\u00E5 inputelementet."
        },
        "attribute": "af-indeterminate",
        "reflect": false,
        "defaultValue": "false"
      },
      "afName": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input name attribute"
            }],
          "text": "S\u00E4tter attributet 'name'."
        },
        "attribute": "af-name",
        "reflect": false
      },
      "checked": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input checked attribute"
            }, {
              "name": "ignore",
              "text": undefined
            }],
          "text": "S\u00E4tter attributet 'checked'."
        },
        "attribute": "checked",
        "reflect": false
      },
      "afChecked": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input checked attribute"
            }],
          "text": "S\u00E4tter attributet 'checked'."
        },
        "attribute": "af-checked",
        "reflect": false
      },
      "afId": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input id attribute. Defaults to random string."
            }],
          "text": "S\u00E4tter attributet 'id'. Om inget v\u00E4ljs s\u00E5 skapas ett slumpm\u00E4ssigt id."
        },
        "attribute": "af-id",
        "reflect": false,
        "defaultValue": "randomIdGenerator('digi-form-checkbox')"
      },
      "afRequired": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input required attribute"
            }],
          "text": "S\u00E4tter attributet 'required'"
        },
        "attribute": "af-required",
        "reflect": false
      },
      "value": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input value attribute"
            }, {
              "name": "ignore",
              "text": undefined
            }],
          "text": "S\u00E4tter attributet 'value'"
        },
        "attribute": "value",
        "reflect": false
      },
      "afValue": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input value attribute"
            }],
          "text": "S\u00E4tter attributet 'value'."
        },
        "attribute": "af-value",
        "reflect": false
      },
      "afValidation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "FormCheckboxValidation",
          "resolved": "FormCheckboxValidation",
          "references": {
            "FormCheckboxValidation": {
              "location": "import",
              "path": "./form-checkbox-validation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set input validation"
            }],
          "text": "S\u00E4tter valideringsstatus. Kan vara 'error' eller ingenting."
        },
        "attribute": "af-validation",
        "reflect": false
      },
      "afAriaLabelledby": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input aria-labelledby attribute"
            }],
          "text": "S\u00E4tter attributet 'aria-labelledby'"
        },
        "attribute": "af-aria-labelledby",
        "reflect": false
      },
      "afAriaLabel": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input aria-label attribute"
            }],
          "text": "S\u00E4tter attributet 'aria-label'"
        },
        "attribute": "af-aria-label",
        "reflect": false
      },
      "afDescription": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Additional description text"
            }],
          "text": "Ytterligare beskrivande text"
        },
        "attribute": "af-description",
        "reflect": false
      },
      "afAriaDescribedby": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input aria-describedby attribute"
            }],
          "text": "S\u00E4tter attributet 'aria-describedby'"
        },
        "attribute": "af-aria-describedby",
        "reflect": false
      },
      "afAutofocus": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input autofocus attribute"
            }],
          "text": "S\u00E4tter attributet 'autofocus'."
        },
        "attribute": "af-autofocus",
        "reflect": false
      }
    };
  }
  static get states() {
    return {
      "dirty": {},
      "touched": {}
    };
  }
  static get events() {
    return [{
        "method": "afOnChange",
        "name": "afOnChange",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The input element's 'onchange' event."
            }],
          "text": "Inputelementets 'onchange'-event."
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnBlur",
        "name": "afOnBlur",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The input element's 'onblur' event."
            }],
          "text": "Inputelementets 'onblur'-event."
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnFocus",
        "name": "afOnFocus",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The input element's 'onfocus' event."
            }],
          "text": "Inputelementets 'onfocus'-event."
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnFocusout",
        "name": "afOnFocusout",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The input element's 'onfocusout' event."
            }],
          "text": "Inputelementets 'onfocusout'-event."
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnInput",
        "name": "afOnInput",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The input element's 'oninput' event."
            }],
          "text": "Inputelementets 'oninput'-event."
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnDirty",
        "name": "afOnDirty",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "First time the input element receives an input"
            }],
          "text": "Sker vid inputf\u00E4ltets f\u00F6rsta 'oninput'"
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnTouched",
        "name": "afOnTouched",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "First time the input element is blurred"
            }],
          "text": "Sker vid inputf\u00E4ltets f\u00F6rsta 'onblur'"
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }];
  }
  static get methods() {
    return {
      "afMGetFormControlElement": {
        "complexType": {
          "signature": "() => Promise<HTMLInputElement>",
          "parameters": [],
          "references": {
            "Promise": {
              "location": "global"
            },
            "HTMLInputElement": {
              "location": "global"
            }
          },
          "return": "Promise<HTMLInputElement>"
        },
        "docs": {
          "text": "H\u00E4mta en referens till inputelementet. Bra f\u00F6r att t.ex. s\u00E4tta fokus programmatiskt.",
          "tags": [{
              "name": "en",
              "text": "Returns a reference to the input element. Handy for setting focus programmatically."
            }]
        }
      }
    };
  }
  static get elementRef() { return "hostElement"; }
  static get watchers() {
    return [{
        "propName": "checked",
        "methodName": "onCheckedChanged"
      }, {
        "propName": "checked",
        "methodName": "onAfCheckedChanged"
      }, {
        "propName": "value",
        "methodName": "onValueChanged"
      }, {
        "propName": "afValue",
        "methodName": "onAfValueChanged"
      }];
  }
}
