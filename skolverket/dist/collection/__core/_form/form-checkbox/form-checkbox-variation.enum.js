export var FormCheckboxVariation;
(function (FormCheckboxVariation) {
  FormCheckboxVariation["PRIMARY"] = "primary";
  FormCheckboxVariation["SECONDARY"] = "secondary";
})(FormCheckboxVariation || (FormCheckboxVariation = {}));
