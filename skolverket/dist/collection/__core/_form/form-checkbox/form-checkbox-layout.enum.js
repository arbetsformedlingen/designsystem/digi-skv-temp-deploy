export var FormCheckboxLayout;
(function (FormCheckboxLayout) {
  FormCheckboxLayout["INLINE"] = "inline";
  FormCheckboxLayout["BLOCK"] = "block";
})(FormCheckboxLayout || (FormCheckboxLayout = {}));
