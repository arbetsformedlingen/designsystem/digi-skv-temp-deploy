export var FormTextareaValidation;
(function (FormTextareaValidation) {
  FormTextareaValidation["SUCCESS"] = "success";
  FormTextareaValidation["ERROR"] = "error";
  FormTextareaValidation["WARNING"] = "warning";
  FormTextareaValidation["NEUTRAL"] = "neutral";
})(FormTextareaValidation || (FormTextareaValidation = {}));
