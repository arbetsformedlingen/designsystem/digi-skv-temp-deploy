import { h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { FormTextareaVariation } from './form-textarea-variation.enum';
import { FormTextareaValidation } from './form-textarea-validation.enum';
/**
 * @enums FormTextareaValidation - form-textarea-validation.enum.ts
 * @enums FormTextareaVariation - form-textarea-variation.enum.ts
 * @swedishName Textarea
 */
export class FormTextarea {
  constructor() {
    this.hasActiveValidationMessage = false;
    this.dirty = false;
    this.touched = false;
    this.afLabel = undefined;
    this.afLabelDescription = undefined;
    this.afVariation = FormTextareaVariation.MEDIUM;
    this.afName = undefined;
    this.afId = randomIdGenerator('digi-form-textarea');
    this.afMaxlength = undefined;
    this.afMinlength = undefined;
    this.afRequired = undefined;
    this.afRequiredText = undefined;
    this.afAnnounceIfOptional = false;
    this.afAnnounceIfOptionalText = undefined;
    this.value = undefined;
    this.afValue = undefined;
    this.afValidation = FormTextareaValidation.NEUTRAL;
    this.afValidationText = undefined;
    this.afRole = undefined;
    this.afAriaActivedescendant = undefined;
    this.afAriaLabelledby = undefined;
    this.afAriaDescribedby = undefined;
    this.afAutofocus = undefined;
  }
  onValueChanged(value) {
    this.afValue = value;
  }
  onAfValueChanged(value) {
    this.value = value;
  }
  afValidationTextWatch() {
    this.setActiveValidationMessage();
  }
  /**
   * Hämtar en referens till textareaelementet. Bra för att t.ex. sätta fokus programmatiskt.
   * @en Returns a reference to the textarea element. Handy for setting focus programmatically.
   */
  async afMGetFormControlElement() {
    return this._textarea;
  }
  componentWillLoad() {
    this.afValue ? (this.value = this.afValue) : (this.afValue = this.value);
    this.setActiveValidationMessage();
  }
  setActiveValidationMessage() {
    this.hasActiveValidationMessage = !!this.afValidationText;
  }
  get cssModifiers() {
    return {
      'digi-form-textarea--small': this.afVariation === FormTextareaVariation.SMALL,
      'digi-form-textarea--medium': this.afVariation === FormTextareaVariation.MEDIUM,
      'digi-form-textarea--large': this.afVariation === FormTextareaVariation.LARGE,
      'digi-form-textarea--auto': this.afVariation === FormTextareaVariation.AUTO,
      'digi-form-textarea--neutral': this.afValidation === FormTextareaValidation.NEUTRAL,
      'digi-form-textarea--success': this.afValidation === FormTextareaValidation.SUCCESS,
      'digi-form-textarea--error': this.afValidation === FormTextareaValidation.ERROR,
      'digi-form-textarea--warning': this.afValidation === FormTextareaValidation.WARNING,
      'digi-form-textarea--empty': !this.afValue &&
        (!this.afValidation || this.afValidation === FormTextareaValidation.NEUTRAL)
    };
  }
  blurHandler(e) {
    if (!this.touched) {
      this.afOnTouched.emit(e);
      this.touched = true;
    }
    this.setActiveValidationMessage();
    this.afOnBlur.emit(e);
  }
  changeHandler(e) {
    this.afOnChange.emit(e);
  }
  focusHandler(e) {
    this.afOnFocus.emit(e);
  }
  focusoutHandler(e) {
    this.afOnFocusout.emit(e);
  }
  keyupHandler(e) {
    this.afOnKeyup.emit(e);
  }
  inputHandler(e) {
    this.afValue = this.value = e.target.value;
    if (!this.dirty) {
      this.afOnDirty.emit(e);
      this.dirty = true;
    }
    this.setActiveValidationMessage();
    this.afOnInput.emit(e);
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-form-textarea': true }, this.cssModifiers) }, h("div", { class: "digi-form-textarea__label" }, h("digi-form-label", { afFor: this.afId, afLabel: this.afLabel, afDescription: this.afLabelDescription, afRequired: this.afRequired, afAnnounceIfOptional: this.afAnnounceIfOptional, afRequiredText: this.afRequiredText, afAnnounceIfOptionalText: this.afAnnounceIfOptionalText })), h("div", { class: "digi-form-textarea__content" }, h("textarea", { class: "digi-form-textarea__textarea", ref: (el) => (this._textarea = el), onBlur: (e) => this.blurHandler(e), onChange: (e) => this.changeHandler(e), onFocus: (e) => this.focusHandler(e), onFocusout: (e) => this.focusoutHandler(e), onKeyUp: (e) => this.keyupHandler(e), onInput: (e) => this.inputHandler(e), "aria-activedescendant": this.afAriaActivedescendant, "aria-describedby": this.afAriaDescribedby, "aria-labelledby": this.afAriaLabelledby, "aria-invalid": this.afValidation != FormTextareaValidation.NEUTRAL ? 'true' : 'false', maxLength: this.afMaxlength, minLength: this.afMinlength, role: this.afRole, required: this.afRequired, id: this.afId, name: this.afName, value: this.afValue, autofocus: this.afAutofocus ? this.afAutofocus : null })), h("div", { class: "digi-form-textarea__footer" }, h("div", { class: "digi-form-textarea__validation", "aria-atomic": "true", role: "alert", id: `${this.afId}--validation-message` }, this.hasActiveValidationMessage &&
      this.afValidation != FormTextareaValidation.NEUTRAL && (h("digi-form-validation-message", { class: "digi-form-textarea__validation-message", "af-variation": this.afValidation }, this.afValidationText))))));
  }
  static get is() { return "digi-form-textarea"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["form-textarea.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["form-textarea.css"]
    };
  }
  static get properties() {
    return {
      "afLabel": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": true,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The label text"
            }],
          "text": "Texten till labelelementet"
        },
        "attribute": "af-label",
        "reflect": false
      },
      "afLabelDescription": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "A description text"
            }],
          "text": "Valfri beskrivande text"
        },
        "attribute": "af-label-description",
        "reflect": false
      },
      "afVariation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "FormTextareaVariation",
          "resolved": "FormTextareaVariation.AUTO | FormTextareaVariation.LARGE | FormTextareaVariation.MEDIUM | FormTextareaVariation.SMALL",
          "references": {
            "FormTextareaVariation": {
              "location": "import",
              "path": "./form-textarea-variation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set variation."
            }],
          "text": "S\u00E4tter variant. Kan vara 'small', 'medium', 'large' eller 'auto'"
        },
        "attribute": "af-variation",
        "reflect": false,
        "defaultValue": "FormTextareaVariation.MEDIUM"
      },
      "afName": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "set name attribute"
            }],
          "text": "S\u00E4tter attributet 'name'."
        },
        "attribute": "af-name",
        "reflect": false
      },
      "afId": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set id attribute. Defaults to random string."
            }],
          "text": "S\u00E4tter attributet 'id'. Om inget v\u00E4ljs s\u00E5 skapas ett slumpm\u00E4ssigt id."
        },
        "attribute": "af-id",
        "reflect": false,
        "defaultValue": "randomIdGenerator('digi-form-textarea')"
      },
      "afMaxlength": {
        "type": "number",
        "mutable": false,
        "complexType": {
          "original": "number",
          "resolved": "number",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set maxlength attribute"
            }],
          "text": "S\u00E4tter attributet 'maxlength'."
        },
        "attribute": "af-maxlength",
        "reflect": false
      },
      "afMinlength": {
        "type": "number",
        "mutable": false,
        "complexType": {
          "original": "number",
          "resolved": "number",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set minlength attribute"
            }],
          "text": "S\u00E4tter attributet 'minlength'."
        },
        "attribute": "af-minlength",
        "reflect": false
      },
      "afRequired": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set required attribute"
            }],
          "text": "S\u00E4tter attributet 'required'."
        },
        "attribute": "af-required",
        "reflect": false
      },
      "afRequiredText": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set text for afRequired."
            }],
          "text": "S\u00E4tter text f\u00F6r afRequired."
        },
        "attribute": "af-required-text",
        "reflect": false
      },
      "afAnnounceIfOptional": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set this to true if the form contains more required fields than optional fields."
            }],
          "text": "S\u00E4tt denna till true om formul\u00E4ret inneh\u00E5ller fler obligatoriska f\u00E4lt \u00E4n valfria."
        },
        "attribute": "af-announce-if-optional",
        "reflect": false,
        "defaultValue": "false"
      },
      "afAnnounceIfOptionalText": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set text for afAnnounceIfOptional."
            }],
          "text": "S\u00E4tter text f\u00F6r afAnnounceIfOptional."
        },
        "attribute": "af-announce-if-optional-text",
        "reflect": false
      },
      "value": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set value attribute"
            }, {
              "name": "ignore",
              "text": undefined
            }],
          "text": "S\u00E4tter attributet 'value'."
        },
        "attribute": "value",
        "reflect": false
      },
      "afValue": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input value attribute"
            }],
          "text": "S\u00E4tter attributet 'value'."
        },
        "attribute": "af-value",
        "reflect": false
      },
      "afValidation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "FormTextareaValidation",
          "resolved": "FormTextareaValidation.ERROR | FormTextareaValidation.NEUTRAL | FormTextareaValidation.SUCCESS | FormTextareaValidation.WARNING",
          "references": {
            "FormTextareaValidation": {
              "location": "import",
              "path": "./form-textarea-validation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set validation status"
            }],
          "text": "S\u00E4tter valideringsstatus. Kan vara 'success', 'error', 'warning', 'neutral' eller ingenting."
        },
        "attribute": "af-validation",
        "reflect": false,
        "defaultValue": "FormTextareaValidation.NEUTRAL"
      },
      "afValidationText": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set input validation message"
            }],
          "text": "S\u00E4tter valideringsmeddelandet"
        },
        "attribute": "af-validation-text",
        "reflect": false
      },
      "afRole": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set role attribute"
            }],
          "text": "S\u00E4tter attributet 'role'."
        },
        "attribute": "af-role",
        "reflect": false
      },
      "afAriaActivedescendant": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set aria-activedescendant attribute"
            }],
          "text": "S\u00E4tter attributet 'aria-activedescendant'."
        },
        "attribute": "af-aria-activedescendant",
        "reflect": false
      },
      "afAriaLabelledby": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set aria-labelledby attribute"
            }],
          "text": "S\u00E4tter attributet 'aria-labelledby'."
        },
        "attribute": "af-aria-labelledby",
        "reflect": false
      },
      "afAriaDescribedby": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set aria-describedby attribute"
            }],
          "text": "S\u00E4tter attributet 'aria-describedby'."
        },
        "attribute": "af-aria-describedby",
        "reflect": false
      },
      "afAutofocus": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input autofocus attribute"
            }],
          "text": "S\u00E4tter attributet 'autofocus'."
        },
        "attribute": "af-autofocus",
        "reflect": false
      }
    };
  }
  static get states() {
    return {
      "hasActiveValidationMessage": {},
      "dirty": {},
      "touched": {}
    };
  }
  static get events() {
    return [{
        "method": "afOnChange",
        "name": "afOnChange",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The textarea element's 'onchange' event."
            }],
          "text": "Textareatelementets 'onchange'-event."
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnBlur",
        "name": "afOnBlur",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The textarea element's 'onblur' event."
            }],
          "text": "Textareatelementets 'onblur'-event."
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnKeyup",
        "name": "afOnKeyup",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The textarea element's 'onkeyup' event."
            }],
          "text": "Textareatelementets 'onkeyup'-event."
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnFocus",
        "name": "afOnFocus",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The textarea element's 'onfocus' event."
            }],
          "text": "Textareatelementets 'onfocus'-event."
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnFocusout",
        "name": "afOnFocusout",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The textarea element's 'onfocusout' event."
            }],
          "text": "Textareatelementets 'onfocusout'-event."
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnInput",
        "name": "afOnInput",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The textarea element's 'oninput' event."
            }],
          "text": "Textareatelementets 'oninput'-event."
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnDirty",
        "name": "afOnDirty",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "First time the textarea element receives an input"
            }],
          "text": "Sker vid textareaelementets f\u00F6rsta 'oninput'"
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnTouched",
        "name": "afOnTouched",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "First time the textelement is blurred"
            }],
          "text": "Sker vid textareaelementets f\u00F6rsta 'onblur'"
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }];
  }
  static get methods() {
    return {
      "afMGetFormControlElement": {
        "complexType": {
          "signature": "() => Promise<HTMLTextAreaElement>",
          "parameters": [],
          "references": {
            "Promise": {
              "location": "global"
            },
            "HTMLTextAreaElement": {
              "location": "global"
            }
          },
          "return": "Promise<HTMLTextAreaElement>"
        },
        "docs": {
          "text": "H\u00E4mtar en referens till textareaelementet. Bra f\u00F6r att t.ex. s\u00E4tta fokus programmatiskt.",
          "tags": [{
              "name": "en",
              "text": "Returns a reference to the textarea element. Handy for setting focus programmatically."
            }]
        }
      }
    };
  }
  static get elementRef() { return "hostElement"; }
  static get watchers() {
    return [{
        "propName": "value",
        "methodName": "onValueChanged"
      }, {
        "propName": "afValue",
        "methodName": "onAfValueChanged"
      }, {
        "propName": "afValidationText",
        "methodName": "afValidationTextWatch"
      }];
  }
}
