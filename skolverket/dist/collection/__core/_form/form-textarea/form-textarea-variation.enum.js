export var FormTextareaVariation;
(function (FormTextareaVariation) {
  FormTextareaVariation["SMALL"] = "small";
  FormTextareaVariation["MEDIUM"] = "medium";
  FormTextareaVariation["LARGE"] = "large";
  FormTextareaVariation["AUTO"] = "auto";
})(FormTextareaVariation || (FormTextareaVariation = {}));
