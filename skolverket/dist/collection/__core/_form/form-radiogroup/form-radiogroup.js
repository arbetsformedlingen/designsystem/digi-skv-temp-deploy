import { h, Host } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator/randomIdGenerator.util';
/**
 * @slot radiobuttons
 *
 * @swedishName Radiogrupp
 */
export class FormRadiogroup {
  constructor() {
    this.radiobuttons = undefined;
    this.afValue = undefined;
    this.value = undefined;
    this.afName = randomIdGenerator('digi-form-radiogroup');
  }
  OnAfValueChange(value) {
    this.value = value;
  }
  OnValueChange(value) {
    this.afValue = value;
    this.setRadiobuttons(value);
  }
  radiobuttonChangeListener(e) {
    if (Array.from(this.radiobuttons).includes(e.target)) {
      e.preventDefault();
      this.afValue = e.detail.target.value;
      this.afOnGroupChange.emit(e);
    }
  }
  componentWillLoad() { }
  componentDidLoad() {
    this.setRadiobuttons(this.value || this.afValue);
    if (this.value)
      this.afValue = this.value;
  }
  componentWillUpdate() { }
  setRadiobuttons(value) {
    this.radiobuttons = this.hostElement.querySelectorAll('digi-form-radiobutton');
    this.radiobuttons.forEach(r => {
      r.afName = this.afName;
      if (value && r.afValue == value) {
        setTimeout(() => r.afMGetFormControlElement().then(e => e.checked || e.click()), 0);
      }
    });
  }
  get cssModifiers() {
    return {};
  }
  render() {
    return (h(Host, null, h("div", { class: Object.assign({ 'digi-form-radiogroup': true }, this.cssModifiers) }, h("digi-util-mutation-observer", { onAfOnChange: (e) => this.radiobuttonChangeListener(e) }, h("slot", null)))));
  }
  static get is() { return "digi-form-radiogroup"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["form-radiogroup.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["form-radiogroup.css"]
    };
  }
  static get properties() {
    return {
      "afValue": {
        "type": "string",
        "mutable": true,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Radiogroup value"
            }],
          "text": "V\u00E4rdet p\u00E5 radiogruppen"
        },
        "attribute": "af-value",
        "reflect": false
      },
      "value": {
        "type": "string",
        "mutable": true,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Radiogroup value"
            }, {
              "name": "ignore",
              "text": undefined
            }],
          "text": "V\u00E4rdet p\u00E5 radiogruppen"
        },
        "attribute": "value",
        "reflect": false
      },
      "afName": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Radiogroup name"
            }],
          "text": "Namnet p\u00E5 radiogruppen"
        },
        "attribute": "af-name",
        "reflect": false,
        "defaultValue": "randomIdGenerator('digi-form-radiogroup')"
      }
    };
  }
  static get states() {
    return {
      "radiobuttons": {}
    };
  }
  static get events() {
    return [{
        "method": "afOnGroupChange",
        "name": "afOnGroupChange",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Event when value changes"
            }],
          "text": "Event n\u00E4r v\u00E4rdet \u00E4ndras"
        },
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        }
      }];
  }
  static get elementRef() { return "hostElement"; }
  static get watchers() {
    return [{
        "propName": "afValue",
        "methodName": "OnAfValueChange"
      }, {
        "propName": "value",
        "methodName": "OnValueChange"
      }];
  }
}
