import { h } from '@stencil/core';
import { FormValidationMessageVariation } from './form-validation-message-variation.enum';
/**
 * @slot default - Ska vara en textnod eller liknande element. Renderas inne i ett spanelement.
 *
 * @enums FormValidationMessageVariation - form-validation-message-variation.enum.ts
 * @swedishName Valideringsmeddelande
 */
export class FormValidationMessage {
  constructor() {
    this.afVariation = FormValidationMessageVariation.SUCCESS;
  }
  get cssModifiers() {
    return {
      'digi-form-validation-message--success': this.afVariation === FormValidationMessageVariation.SUCCESS,
      'digi-form-validation-message--error': this.afVariation === FormValidationMessageVariation.ERROR,
      'digi-form-validation-message--warning': this.afVariation === FormValidationMessageVariation.WARNING
    };
  }
  get icon() {
    switch (this.afVariation) {
      case FormValidationMessageVariation.SUCCESS:
        return 'check-circle';
      case FormValidationMessageVariation.ERROR:
        return 'exclamation-circle-filled';
      case FormValidationMessageVariation.WARNING:
        return 'exclamation-triangle-warning';
    }
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-form-validation-message': true }, this.cssModifiers) }, h("div", { class: "digi-form-validation-message__icon", "aria-hidden": "true" }, h("digi-icon", { afName: this.icon })), h("span", { class: "digi-form-validation-message__text" }, h("slot", null))));
  }
  static get is() { return "digi-form-validation-message"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["form-validation-message.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["form-validation-message.css"]
    };
  }
  static get properties() {
    return {
      "afVariation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "FormValidationMessageVariation",
          "resolved": "FormValidationMessageVariation.ERROR | FormValidationMessageVariation.SUCCESS | FormValidationMessageVariation.WARNING",
          "references": {
            "FormValidationMessageVariation": {
              "location": "import",
              "path": "./form-validation-message-variation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set validation status"
            }],
          "text": "S\u00E4tter valideringsstatus. Kan vara 'success', 'error' eller 'warning'."
        },
        "attribute": "af-variation",
        "reflect": false,
        "defaultValue": "FormValidationMessageVariation.SUCCESS"
      }
    };
  }
  static get elementRef() { return "hostElement"; }
}
