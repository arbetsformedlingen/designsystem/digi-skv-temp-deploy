export var FormValidationMessageVariation;
(function (FormValidationMessageVariation) {
  FormValidationMessageVariation["SUCCESS"] = "success";
  FormValidationMessageVariation["ERROR"] = "error";
  FormValidationMessageVariation["WARNING"] = "warning";
})(FormValidationMessageVariation || (FormValidationMessageVariation = {}));
