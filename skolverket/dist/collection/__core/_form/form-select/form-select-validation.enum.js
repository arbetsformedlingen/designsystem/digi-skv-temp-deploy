export var FormSelectValidation;
(function (FormSelectValidation) {
  FormSelectValidation["SUCCESS"] = "success";
  FormSelectValidation["ERROR"] = "error";
  FormSelectValidation["WARNING"] = "warning";
  FormSelectValidation["NEUTRAL"] = "neutral";
})(FormSelectValidation || (FormSelectValidation = {}));
