export var FormSelectVariation;
(function (FormSelectVariation) {
  FormSelectVariation["SMALL"] = "small";
  FormSelectVariation["MEDIUM"] = "medium";
  FormSelectVariation["LARGE"] = "large";
})(FormSelectVariation || (FormSelectVariation = {}));
