import { h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { logger } from '../../../global/utils/logger';
import { FormSelectValidation } from './form-select-validation.enum';
import { FormSelectVariation } from './form-select-variation.enum';
/**
 * @slot default - Should be an option element
 *
 * @enums FormSelectValidation - form-select-validation.enum
 * @enums FormSelectVariation - form-select-variation.enum
 * @swedishName Väljare
 */
export class FormSelect {
  constructor() {
    this.optionItems = [];
    this.dirty = false;
    this.touched = false;
    this.hasPlaceholder = false;
    this.afLabel = undefined;
    this.afRequired = undefined;
    this.afRequiredText = undefined;
    this.afPlaceholder = undefined;
    this.afAnnounceIfOptional = false;
    this.afAnnounceIfOptionalText = undefined;
    this.afId = randomIdGenerator('digi-form-select');
    this.afName = undefined;
    this.afDescription = undefined;
    this.value = undefined;
    this.afValue = undefined;
    this.afDisableValidation = undefined;
    this.afVariation = FormSelectVariation.MEDIUM;
    this.afValidation = FormSelectValidation.NEUTRAL;
    this.afValidationText = undefined;
    this.afStartSelected = undefined;
    this.afAutofocus = undefined;
  }
  onValueChanged(value) {
    this.afValue = value;
  }
  onAfValueChanged(value) {
    this.value = value;
  }
  getOptions() {
    this.checkIfPlaceholder();
    let options = this._observer.children;
    if (!options || options.length <= 0) {
      logger.warn(`The slot contains no option elements.`, this.hostElement);
      return;
    }
    this.optionItems = [...Array.from(options)]
      .filter((el) => el.tagName.toLowerCase() === 'option')
      .map((el) => {
      return {
        value: el['value'],
        text: el['text']
      };
    });
  }
  checkIfPlaceholder() {
    this.hasPlaceholder = !!this.afPlaceholder;
  }
  componentWillLoad() {
    this.afValue ? (this.value = this.afValue) : (this.afValue = this.value);
  }
  componentDidLoad() {
    this.getOptions();
    this.afStartSelected &&
      (this.afValue = this.optionItems[this.afStartSelected].value);
  }
  /**
   * Returnerar en referens till formulärkontrollelementet.
   * @en Returns a reference to the form control element.
   */
  async afMGetFormControlElement() {
    return this._select;
  }
  get cssModifiers() {
    return {
      'digi-form-select--small': this.afVariation === FormSelectVariation.SMALL,
      'digi-form-select--medium': this.afVariation === FormSelectVariation.MEDIUM,
      'digi-form-select--large': this.afVariation === FormSelectVariation.LARGE,
      'digi-form-select--neutral': this.afValidation === FormSelectValidation.NEUTRAL,
      'digi-form-select--success': this.afValidation === FormSelectValidation.SUCCESS,
      'digi-form-select--warning': this.afValidation === FormSelectValidation.WARNING,
      'digi-form-select--error': this.afValidation === FormSelectValidation.ERROR,
      'digi-form-select--empty': !this.afValidation || this.afValidation === FormSelectValidation.NEUTRAL
    };
  }
  changeHandler(e) {
    if (!this.dirty) {
      this.afOnDirty.emit(e);
      this.dirty = true;
    }
    this.value = this.afValue = e.target.value;
    this.afOnChange.emit(e);
  }
  focusHandler(e) {
    this.afOnFocus.emit(e);
  }
  focusoutHandler(e) {
    this.afOnFocusout.emit(e);
  }
  blurHandler(e) {
    this.afOnBlur.emit(e);
  }
  getSelectedItem(option) {
    return this.afValue == option.value;
  }
  showValidation() {
    return !this.afDisableValidation &&
      this.afValidation !== FormSelectValidation.NEUTRAL &&
      this.afValidationText
      ? true
      : false;
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-form-select': true }, this.cssModifiers) }, h("div", { class: "digi-form-select__label-group" }, h("digi-form-label", { afFor: this.afId, afLabel: this.afLabel, afId: `${this.afId}-label`, afDescription: this.afDescription, afRequired: this.afRequired, afAnnounceIfOptional: this.afAnnounceIfOptional, afRequiredText: this.afRequiredText, afAnnounceIfOptionalText: this.afAnnounceIfOptionalText })), h("div", { class: "digi-form-select__select-wrapper" }, h("select", { class: "digi-form-select__select", name: this.afName, id: this.afId, ref: (el) => (this._select = el), required: this.afRequired ? this.afRequired : null, onFocus: (e) => this.focusHandler(e), onFocusout: (e) => this.focusoutHandler(e), onBlur: (e) => this.blurHandler(e), onChange: (e) => this.changeHandler(e), autoFocus: this.afAutofocus ? this.afAutofocus : null }, this.hasPlaceholder && (h("option", { class: {
        'digi-form-select__select-option': true,
        'digi-form-select__select-option--initial-value': true
      }, disabled: true, selected: !this.afValue, value: "" }, this.afPlaceholder)), this.optionItems.map((option, index) => {
      return (h("option", { key: index, class: "digi-form-select__select-option", value: option.value, selected: this.getSelectedItem(option) }, option.text));
    })), h("digi-icon", { class: "digi-form-select__icon", slot: "icon", afName: `input-select-marker` })), h("digi-util-mutation-observer", { hidden: true, ref: (el) => (this._observer = el), onAfOnChange: () => this.getOptions() }, h("slot", null)), h("div", { class: "digi-form-select__footer" }, h("div", { "aria-atomic": "true", role: "alert", id: `${this.afId}--validation-message` }, this.showValidation() && (h("digi-form-validation-message", { "af-variation": this.afValidation }, this.afValidationText))))));
  }
  static get is() { return "digi-form-select"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["form-select.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["form-select.css"]
    };
  }
  static get properties() {
    return {
      "afLabel": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": true,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Sets a label for select."
            }],
          "text": "S\u00E4tter en text f\u00F6r select."
        },
        "attribute": "af-label",
        "reflect": false
      },
      "afRequired": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Is the associated field required?"
            }],
          "text": "\u00C4r det ihopkopplade f\u00E4ltet tvingat?"
        },
        "attribute": "af-required",
        "reflect": false
      },
      "afRequiredText": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set text for afRequired."
            }],
          "text": "S\u00E4tter text f\u00F6r afRequired."
        },
        "attribute": "af-required-text",
        "reflect": false
      },
      "afPlaceholder": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "A placeholder for the select"
            }],
          "text": "En platsh\u00E5llare f\u00F6r v\u00E4ljaren"
        },
        "attribute": "af-placeholder",
        "reflect": false
      },
      "afAnnounceIfOptional": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Normally a required field is visually and semantically emphasized.\r\nThis flips that and emphasizes the field if it is not required.\r\nSet this to true if your form contains more required fields than not."
            }],
          "text": "Normalt betonas ett obligatoriskt f\u00E4lt visuellt och semantiskt.\r\nDetta v\u00E4nder p\u00E5 det och betonar f\u00E4ltet om det inte kr\u00E4vs.\r\nSt\u00E4ll in detta till sant om ditt formul\u00E4r inneh\u00E5ller fler obligatoriska f\u00E4lt \u00E4n inte."
        },
        "attribute": "af-announce-if-optional",
        "reflect": false,
        "defaultValue": "false"
      },
      "afAnnounceIfOptionalText": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set text for afAnnounceIfOptional."
            }],
          "text": "S\u00E4tter text f\u00F6r afAnnounceIfOptional."
        },
        "attribute": "af-announce-if-optional-text",
        "reflect": false
      },
      "afId": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Sets an id attribute. Standard \u00E4r slumpm\u00E4ssig str\u00E4ng."
            }],
          "text": "S\u00E4tter ett id attribut."
        },
        "attribute": "af-id",
        "reflect": false,
        "defaultValue": "randomIdGenerator('digi-form-select')"
      },
      "afName": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Sets the select name."
            }],
          "text": "S\u00E4tter v\u00E4ljarens namn."
        },
        "attribute": "af-name",
        "reflect": false
      },
      "afDescription": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "An optional description text."
            }],
          "text": "En valfri beskrivningstext."
        },
        "attribute": "af-description",
        "reflect": false
      },
      "value": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input value attribute"
            }, {
              "name": "ignore",
              "text": undefined
            }],
          "text": "S\u00E4tter attributet 'value'"
        },
        "attribute": "value",
        "reflect": false
      },
      "afValue": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input value attribute"
            }],
          "text": "S\u00E4tter attributet 'value'."
        },
        "attribute": "af-value",
        "reflect": false
      },
      "afDisableValidation": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Disables the validation."
            }],
          "text": "Inaktiverar valideringen"
        },
        "attribute": "af-disable-validation",
        "reflect": false
      },
      "afVariation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "FormSelectVariation",
          "resolved": "FormSelectVariation.LARGE | FormSelectVariation.MEDIUM | FormSelectVariation.SMALL",
          "references": {
            "FormSelectVariation": {
              "location": "import",
              "path": "./form-select-variation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set select input size."
            }],
          "text": "S\u00E4tter v\u00E4ljarens storlek."
        },
        "attribute": "af-variation",
        "reflect": false,
        "defaultValue": "FormSelectVariation.MEDIUM"
      },
      "afValidation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "FormSelectValidation",
          "resolved": "FormSelectValidation.ERROR | FormSelectValidation.NEUTRAL | FormSelectValidation.SUCCESS | FormSelectValidation.WARNING",
          "references": {
            "FormSelectValidation": {
              "location": "import",
              "path": "./form-select-validation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set select validation"
            }],
          "text": "S\u00E4tter v\u00E4ljarens validering"
        },
        "attribute": "af-validation",
        "reflect": false,
        "defaultValue": "FormSelectValidation.NEUTRAL"
      },
      "afValidationText": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Sets the validation text."
            }],
          "text": "S\u00E4tter valideringstexten."
        },
        "attribute": "af-validation-text",
        "reflect": false
      },
      "afStartSelected": {
        "type": "number",
        "mutable": false,
        "complexType": {
          "original": "number",
          "resolved": "number",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "deprecated",
              "text": "Anv\u00E4nd afValue ist\u00E4llet"
            }, {
              "name": "en",
              "text": "Sets initial selected value."
            }],
          "text": "S\u00E4tter ett f\u00F6rvalt v\u00E4rde."
        },
        "attribute": "af-start-selected",
        "reflect": false
      },
      "afAutofocus": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input autofocus attribute"
            }],
          "text": "S\u00E4tter attributet 'autofocus'."
        },
        "attribute": "af-autofocus",
        "reflect": false
      }
    };
  }
  static get states() {
    return {
      "optionItems": {},
      "dirty": {},
      "touched": {},
      "hasPlaceholder": {}
    };
  }
  static get events() {
    return [{
        "method": "afOnChange",
        "name": "afOnChange",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [],
          "text": ""
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnFocus",
        "name": "afOnFocus",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [],
          "text": ""
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnFocusout",
        "name": "afOnFocusout",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [],
          "text": ""
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnBlur",
        "name": "afOnBlur",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [],
          "text": ""
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnDirty",
        "name": "afOnDirty",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "First time the select element is changed."
            }],
          "text": "Sker f\u00F6rsta g\u00E5ngen v\u00E4ljarelementet \u00E4ndras."
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }];
  }
  static get methods() {
    return {
      "afMGetFormControlElement": {
        "complexType": {
          "signature": "() => Promise<HTMLSelectElement>",
          "parameters": [],
          "references": {
            "Promise": {
              "location": "global"
            },
            "HTMLSelectElement": {
              "location": "global"
            }
          },
          "return": "Promise<HTMLSelectElement>"
        },
        "docs": {
          "text": "Returnerar en referens till formul\u00E4rkontrollelementet.",
          "tags": [{
              "name": "en",
              "text": "Returns a reference to the form control element."
            }]
        }
      }
    };
  }
  static get elementRef() { return "hostElement"; }
  static get watchers() {
    return [{
        "propName": "value",
        "methodName": "onValueChanged"
      }, {
        "propName": "afValue",
        "methodName": "onAfValueChanged"
      }];
  }
}
