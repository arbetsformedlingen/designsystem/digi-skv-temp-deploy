import { h } from '@stencil/core';
export class IconExternalLinkAlt {
  constructor() {
    this.afTitle = undefined;
    this.afDesc = undefined;
    this.afSvgAriaHidden = true;
  }
  render() {
    return (h("svg", { class: "digi-icon-external-link-alt", width: "29", height: "26", viewBox: "0 0 29 26", xmlns: "http://www.w3.org/2000/svg", "aria-hidden": this.afSvgAriaHidden ? 'true' : 'false' }, this.afTitle && h("title", null, this.afTitle), this.afDesc && h("desc", null, this.afDesc), h("path", { class: "digi-icon-external-link-alt__shape", d: "M22.556 12.28v11.283A2.427 2.427 0 0120.139 26H2.417A2.427 2.427 0 010 23.562V5.688A2.427 2.427 0 012.417 3.25h17.117c.538 0 .807.656.427 1.04L18.752 5.51a.602.602 0 01-.427.178H2.72a.303.303 0 00-.302.305v17.266c0 .168.135.305.302.305h17.118a.303.303 0 00.302-.305v-9.76c0-.16.064-.316.177-.43l1.208-1.219a.604.604 0 011.032.431zM28.396 0h-6.847c-.536 0-.807.657-.427 1.04l2.426 2.448L9.844 17.311a.613.613 0 000 .862l1.139 1.149a.6.6 0 00.854 0L25.542 5.499l2.427 2.447A.604.604 0 0029 7.516V.608C29 .273 28.73 0 28.396 0z", fill: "currentColor", "fill-rule": "nonzero" })));
  }
  static get is() { return "digi-icon-external-link-alt"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["icon-external-link-alt.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["icon-external-link-alt.css"]
    };
  }
  static get properties() {
    return {
      "afTitle": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Adds a title element inside the svg"
            }],
          "text": "L\u00E4gger till ett titleelement i svg:n"
        },
        "attribute": "af-title",
        "reflect": false
      },
      "afDesc": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Adds a desc element inside the svg"
            }],
          "text": "L\u00E4gger till ett descelement i svg:n"
        },
        "attribute": "af-desc",
        "reflect": false
      },
      "afSvgAriaHidden": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Hides the icon for screen readers. Default is set to true."
            }],
          "text": "F\u00F6r att d\u00F6lja ikonen f\u00F6r sk\u00E4rml\u00E4sare. Default \u00E4r satt till true."
        },
        "attribute": "af-svg-aria-hidden",
        "reflect": false,
        "defaultValue": "true"
      }
    };
  }
}
