import { h } from '@stencil/core';
export class IconSpinner {
  constructor() {
    this._path = 'M25 42c9.389 0 17-7.611 17-17S34.389 8 25 8 8 15.611 8 25s7.611 17 17 17zm0 8C11.193 50 0 38.807 0 25S11.193 0 25 0s25 11.193 25 25-11.193 25-25 25z';
    this.afTitle = undefined;
    this.afDesc = undefined;
    this.afSvgAriaHidden = true;
  }
  render() {
    return (h("svg", { class: "digi-icon-spinner", height: "50", viewBox: "0 0 50 50", width: "50", "aria-hidden": this.afSvgAriaHidden ? 'true' : 'false', xmlns: "http://www.w3.org/2000/svg" }, this.afTitle && h("title", null, this.afTitle), this.afDesc && h("desc", null, this.afDesc), h("g", { class: "digi-icon-spinner__shape", fill: "none", "fill-rule": "evenodd" }, h("mask", { fill: "#fff", id: "iconSpinnerPath" }, h("path", { d: this._path })), h("path", { d: this._path, fill: "#D0CFE1", "fill-rule": "nonzero" }), h("path", { d: "M19 26L54-5v62z", fill: "currentColor", mask: "url(#iconSpinnerPath)" }))));
  }
  static get is() { return "digi-icon-spinner"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["icon-spinner.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["icon-spinner.css"]
    };
  }
  static get properties() {
    return {
      "afTitle": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Adds a title element inside the svg"
            }],
          "text": "L\u00E4gger till ett titleelement i svg:n"
        },
        "attribute": "af-title",
        "reflect": false
      },
      "afDesc": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Adds a desc element inside the svg"
            }],
          "text": "L\u00E4gger till ett descelement i svg:n"
        },
        "attribute": "af-desc",
        "reflect": false
      },
      "afSvgAriaHidden": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Hides the icon for screen readers. Default is set to true."
            }],
          "text": "F\u00F6r att d\u00F6lja ikonen f\u00F6r sk\u00E4rml\u00E4sare. Default \u00E4r satt till true."
        },
        "attribute": "af-svg-aria-hidden",
        "reflect": false,
        "defaultValue": "true"
      }
    };
  }
}
