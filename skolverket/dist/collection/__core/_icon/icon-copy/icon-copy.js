import { h } from '@stencil/core';
export class IconCopy {
  constructor() {
    this.afTitle = undefined;
    this.afDesc = undefined;
    this.afSvgAriaHidden = true;
  }
  render() {
    return (h("svg", { class: "digi-icon-copy", width: "22", height: "26", viewBox: "0 0 22 26", xmlns: "http://www.w3.org/2000/svg", "aria-hidden": this.afSvgAriaHidden ? 'true' : 'false' }, this.afTitle && h("title", null, this.afTitle), this.afDesc && h("desc", null, this.afDesc), h("path", { class: "digi-icon-copy__shape", d: "M15.714 22.75v2.031c0 .673-.527 1.219-1.178 1.219H1.179C.528 26 0 25.454 0 24.781V6.094c0-.673.528-1.219 1.179-1.219h3.535v15.031c0 1.568 1.234 2.844 2.75 2.844h8.25zm0-17.469V0h-8.25c-.65 0-1.178.546-1.178 1.219v18.687c0 .673.527 1.219 1.178 1.219h13.357c.651 0 1.179-.546 1.179-1.219V6.5h-5.107c-.648 0-1.179-.548-1.179-1.219zm5.94-1.575L18.418.356A1.16 1.16 0 0017.583 0h-.297v4.875H22v-.308a1.24 1.24 0 00-.345-.861z", fill: "currentColor", "fill-rule": "nonzero" })));
  }
  static get is() { return "digi-icon-copy"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["icon-copy.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["icon-copy.css"]
    };
  }
  static get properties() {
    return {
      "afTitle": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Adds a title element inside the svg"
            }],
          "text": "L\u00E4gger till ett titleelement i svg:n"
        },
        "attribute": "af-title",
        "reflect": false
      },
      "afDesc": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Adds a desc element inside the svg"
            }],
          "text": "L\u00E4gger till ett descelement i svg:n"
        },
        "attribute": "af-desc",
        "reflect": false
      },
      "afSvgAriaHidden": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Hides the icon for screen readers. Default is set to true."
            }],
          "text": "F\u00F6r att d\u00F6lja ikonen f\u00F6r sk\u00E4rml\u00E4sare. Default \u00E4r satt till true."
        },
        "attribute": "af-svg-aria-hidden",
        "reflect": false,
        "defaultValue": "true"
      }
    };
  }
}
