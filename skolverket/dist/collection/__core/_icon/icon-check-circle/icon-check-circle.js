import { h } from '@stencil/core';
export class IconCheckCircle {
  constructor() {
    this.afTitle = undefined;
    this.afDesc = undefined;
    this.afSvgAriaHidden = true;
  }
  render() {
    return (h("svg", { class: "digi-icon-check-circle", width: "25", height: "25", viewBox: "0 0 25 25", "aria-hidden": this.afSvgAriaHidden ? 'true' : 'false', xmlns: "http://www.w3.org/2000/svg" }, this.afTitle && h("title", null, this.afTitle), this.afDesc && h("desc", null, this.afDesc), h("path", { class: "digi-icon-check-circle__shape", d: "M25 12.5C25 19.404 19.404 25 12.5 25S0 19.404 0 12.5 5.596 0 12.5 0 25 5.596 25 12.5zm-13.946 6.619l9.274-9.275a.806.806 0 000-1.14l-1.14-1.14a.806.806 0 00-1.14 0l-7.564 7.563-3.531-3.531a.807.807 0 00-1.14 0l-1.141 1.14a.806.806 0 000 1.14l5.242 5.243a.806.806 0 001.14 0z", fill: "currentColor", "fill-rule": "nonzero" })));
  }
  static get is() { return "digi-icon-check-circle"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["icon-check-circle.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["icon-check-circle.css"]
    };
  }
  static get properties() {
    return {
      "afTitle": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Adds a title element inside the svg"
            }],
          "text": "L\u00E4gger till ett titleelement i svg:n"
        },
        "attribute": "af-title",
        "reflect": false
      },
      "afDesc": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Adds a desc element inside the svg"
            }],
          "text": "L\u00E4gger till ett descelement i svg:n"
        },
        "attribute": "af-desc",
        "reflect": false
      },
      "afSvgAriaHidden": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Hides the icon for screen readers. Default is set to true."
            }],
          "text": "F\u00F6r att d\u00F6lja ikonen f\u00F6r sk\u00E4rml\u00E4sare. Default \u00E4r satt till true."
        },
        "attribute": "af-svg-aria-hidden",
        "reflect": false,
        "defaultValue": "true"
      }
    };
  }
}
