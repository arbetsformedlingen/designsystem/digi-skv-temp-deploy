import { h } from '@stencil/core';
export class IconExclamationCircle {
  constructor() {
    this.afTitle = undefined;
    this.afDesc = undefined;
    this.afSvgAriaHidden = true;
  }
  render() {
    return (h("svg", { class: "digi-icon-exclamation-circle", width: "42", height: "42", viewBox: "0 0 42 42", xmlns: "http://www.w3.org/2000/svg", "aria-hidden": this.afSvgAriaHidden ? 'true' : 'false' }, this.afTitle && h("title", null, this.afTitle), this.afDesc && h("desc", null, this.afDesc), h("g", { fill: "currentColor", class: "digi-icon-exclamation-circle__shape" }, h("path", { d: "M21 3C11.059 3 3 11.059 3 21s8.059 18 18 18 18-8.059 18-18S30.941 3 21 3zm0-3c11.598 0 21 9.402 21 21s-9.402 21-21 21S0 32.598 0 21 9.402 0 21 0z", "fill-rule": "nonzero" }), h("path", { d: "M25 10l-.875 11h-5.25L18 10zM21.5 31c-2.178 0-3.5-1.58-3.5-3.5s1.322-3.5 3.5-3.5 3.5 1.58 3.5 3.5-1.322 3.5-3.5 3.5z" }))));
  }
  static get is() { return "digi-icon-exclamation-circle"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["icon-exclamation-circle.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["icon-exclamation-circle.css"]
    };
  }
  static get properties() {
    return {
      "afTitle": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Adds a title element inside the svg"
            }],
          "text": "L\u00E4gger till ett titleelement i svg:n"
        },
        "attribute": "af-title",
        "reflect": false
      },
      "afDesc": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Adds a desc element inside the svg"
            }],
          "text": "L\u00E4gger till ett descelement i svg:n"
        },
        "attribute": "af-desc",
        "reflect": false
      },
      "afSvgAriaHidden": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Hides the icon for screen readers. Default is set to true."
            }],
          "text": "F\u00F6r att d\u00F6lja ikonen f\u00F6r sk\u00E4rml\u00E4sare. Default \u00E4r satt till true."
        },
        "attribute": "af-svg-aria-hidden",
        "reflect": false,
        "defaultValue": "true"
      }
    };
  }
}
