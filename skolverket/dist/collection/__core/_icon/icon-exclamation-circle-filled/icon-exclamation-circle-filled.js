import { h } from '@stencil/core';
export class IconExclamationCircleFilled {
  constructor() {
    this.afTitle = undefined;
    this.afDesc = undefined;
    this.afSvgAriaHidden = true;
  }
  render() {
    return (h("svg", { class: "digi-icon-exclamation-circle-filled", width: "25", height: "25", viewBox: "0 0 25 25", xmlns: "http://www.w3.org/2000/svg", "aria-hidden": this.afSvgAriaHidden ? 'true' : 'false' }, this.afTitle && h("title", null, this.afTitle), this.afDesc && h("desc", null, this.afDesc), h("path", { class: "digi-icon-exclamation-circle-filled__shape", d: "M25 12.5C25 19.405 19.403 25 12.5 25S0 19.405 0 12.5C0 5.599 5.597 0 12.5 0S25 5.599 25 12.5zm-12.5 2.52a2.319 2.319 0 100 4.637 2.319 2.319 0 000-4.637zm-2.201-8.334l.374 6.855c.017.32.282.572.604.572h2.446a.605.605 0 00.604-.572l.374-6.855a.605.605 0 00-.604-.638h-3.194a.605.605 0 00-.604.638z", fill: "currentColor", "fill-rule": "nonzero" })));
  }
  static get is() { return "digi-icon-exclamation-circle-filled"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["icon-exclamation-circle-filled.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["icon-exclamation-circle-filled.css"]
    };
  }
  static get properties() {
    return {
      "afTitle": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Adds a title element inside the svg"
            }],
          "text": "L\u00E4gger till ett titleelement i svg:n"
        },
        "attribute": "af-title",
        "reflect": false
      },
      "afDesc": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Adds a desc element inside the svg"
            }],
          "text": "L\u00E4gger till ett descelement i svg:n"
        },
        "attribute": "af-desc",
        "reflect": false
      },
      "afSvgAriaHidden": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Hides the icon for screen readers. Default is set to true."
            }],
          "text": "F\u00F6r att d\u00F6lja ikonen f\u00F6r sk\u00E4rml\u00E4sare. Default \u00E4r satt till true."
        },
        "attribute": "af-svg-aria-hidden",
        "reflect": false,
        "defaultValue": "true"
      }
    };
  }
}
