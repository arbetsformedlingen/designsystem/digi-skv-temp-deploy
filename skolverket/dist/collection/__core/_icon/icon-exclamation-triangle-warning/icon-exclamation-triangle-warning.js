import { h } from '@stencil/core';
export class IconExclamationTriangleWarning {
  constructor() {
    this.afTitle = undefined;
    this.afDesc = undefined;
    this.afSvgAriaHidden = true;
  }
  render() {
    return (h("svg", { class: "digi-icon-exclamation-triangle-warning", width: "22", height: "20", viewBox: "0 0 22 20", xmlns: "http://www.w3.org/2000/svg", "aria-hidden": this.afSvgAriaHidden ? 'true' : 'false' }, this.afTitle && h("title", null, this.afTitle), this.afDesc && h("desc", null, this.afDesc), h("defs", null, h("path", { d: "M9.5 8.38l.252 4.207a.455.455 0 00.458.424h1.58a.455.455 0 00.458-.424l.251-4.207a.454.454 0 00-.457-.477H9.958a.454.454 0 00-.457.477zm3.104 6.734c0 .872-.718 1.578-1.604 1.578-.886 0-1.604-.706-1.604-1.578 0-.87.718-1.577 1.604-1.577.886 0 1.604.706 1.604 1.577zm-.016-13.522c-.704-1.2-2.47-1.202-3.176 0L.247 17.218c-.703 1.2.178 2.704 1.588 2.704h18.33c1.407 0 2.292-1.502 1.587-2.704L12.588 1.592zM2.032 17.782l8.77-14.95a.231.231 0 01.397 0l8.77 14.95a.225.225 0 01-.2.337H2.23a.225.225 0 01-.198-.338z", id: "iconExclamationTriangelWarningPath" })), h("g", { fill: "none", "fill-rule": "nonzero" }, h("path", { d: "M11.874 2.573l8.3 14.941A1 1 0 0119.3 19H2.7a1 1 0 01-.875-1.486l8.3-14.94a1 1 0 011.75 0z", fill: "var(--digi--icon-exclamation-triangle-warning--background)" }), h("g", null, h("mask", { id: "iconExclamationTriangelWarningPathB", fill: "#FFF" }, h("use", { href: "#iconExclamationTriangelWarningPath" })), h("use", { fill: "none", "fill-rule": "nonzero", href: "#iconExclamationTriangelWarningPath" }), h("g", { mask: "url(#iconExclamationTriangelWarningPathB)", fill: "var(--digi--icon-exclamation-triangle-warning--outline)" }, h("path", { d: "M-1.571-2.308h25.143v24.615H-1.571z" }))))));
  }
  static get is() { return "digi-icon-exclamation-triangle-warning"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["icon-exclamation-triangle-warning.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["icon-exclamation-triangle-warning.css"]
    };
  }
  static get properties() {
    return {
      "afTitle": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Adds a title element inside the svg"
            }],
          "text": "L\u00E4gger till ett titleelement i svg:n"
        },
        "attribute": "af-title",
        "reflect": false
      },
      "afDesc": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Adds a desc element inside the svg"
            }],
          "text": "L\u00E4gger till ett descelement i svg:n"
        },
        "attribute": "af-desc",
        "reflect": false
      },
      "afSvgAriaHidden": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Hides the icon for screen readers. Default is set to true."
            }],
          "text": "F\u00F6r att d\u00F6lja ikonen f\u00F6r sk\u00E4rml\u00E4sare. Default \u00E4r satt till true."
        },
        "attribute": "af-svg-aria-hidden",
        "reflect": false,
        "defaultValue": "true"
      }
    };
  }
}
