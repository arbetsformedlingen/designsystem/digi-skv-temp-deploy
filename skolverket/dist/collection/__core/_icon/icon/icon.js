import { h } from '@stencil/core';
import { icon } from '../../../global/icon/icon';
/**
 *
 * @swedishName Ikon
 */
export class Icon {
  constructor() {
    this.currentIcon = undefined;
    this.afName = undefined;
    this.afTitle = undefined;
    this.afDesc = undefined;
    this.afSvgAriaHidden = true;
  }
  setIcon() {
    const { IconComponent } = icon[this.afName];
    this.currentIcon = IconComponent;
  }
  componentWillLoad() {
    this.setIcon();
  }
  render() {
    return this.currentIcon ? (h(this.currentIcon, { afSvgAriaHidden: this.afSvgAriaHidden }, this.afTitle && h("title", null, this.afTitle), this.afDesc && h("desc", null, this.afDesc))) : null;
  }
  static get is() { return "digi-icon"; }
  static get originalStyleUrls() {
    return {
      "$": ["icon.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["icon.css"]
    };
  }
  static get properties() {
    return {
      "afName": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "IconName",
          "resolved": "\"search\" | \"chevron-left\" | \"accessibility-deaf\" | \"bars\" | \"check-circle-reg\" | \"check\" | \"chevron-down\" | \"chevron-right\" | \"chevron-up\" | \"dislike\" | \"exclamation-circle\" | \"exclamation-triangle-warning\" | \"globe\" | \"info-circle-reg\" | \"lattlast\" | \"like\" | \"minus\" | \"plus\" | \"sort\" | \"x\" | \"input-select-marker\" | \"notification-danger\" | \"notification-warning\" | \"notification-info\" | \"close\" | \"check-circle\" | \"check-circle-reg-alt\" | \"copy\" | \"danger-outline\" | \"download\" | \"exclamation-circle-filled\" | \"exclamation-triangle\" | \"external-link-alt\" | \"paperclip\" | \"spinner\" | \"trash\"",
          "references": {
            "IconName": {
              "location": "import",
              "path": "../../../global/icon/icon"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [],
          "text": ""
        },
        "attribute": "af-name",
        "reflect": false
      },
      "afTitle": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Adds a title element inside the svg"
            }],
          "text": "L\u00E4gger till ett titleelement i svg:n"
        },
        "attribute": "af-title",
        "reflect": false
      },
      "afDesc": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Adds a desc element inside the svg"
            }],
          "text": "L\u00E4gger till ett descelement i svg:n"
        },
        "attribute": "af-desc",
        "reflect": false
      },
      "afSvgAriaHidden": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Hides the icon for screen readers. Default is set to true."
            }],
          "text": "F\u00F6r att d\u00F6lja ikonen f\u00F6r sk\u00E4rml\u00E4sare. Default \u00E4r satt till true."
        },
        "attribute": "af-svg-aria-hidden",
        "reflect": false,
        "defaultValue": "true"
      }
    };
  }
  static get states() {
    return {
      "currentIcon": {}
    };
  }
  static get watchers() {
    return [{
        "propName": "afName",
        "methodName": "setIcon"
      }];
  }
}
