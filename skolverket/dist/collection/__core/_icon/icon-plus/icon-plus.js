import { h } from '@stencil/core';
export class IconPlus {
  constructor() {
    this.afTitle = undefined;
    this.afDesc = undefined;
    this.afSvgAriaHidden = true;
  }
  render() {
    return (h("svg", { class: "digi-icon-plus", width: "22", height: "26", viewBox: "0 0 22 26", "aria-hidden": this.afSvgAriaHidden ? 'true' : 'false', xmlns: "http://www.w3.org/2000/svg" }, this.afTitle && h("title", null, this.afTitle), this.afDesc && h("desc", null, this.afDesc), h("path", { class: "digi-icon-plus__shape", d: "M21.6,13c0,0.7-0.5,1.2-1.2,1.2h-7.9V22c0,0.7-0.5,1.2-1.2,1.2s-1.2-0.5-1.2-1.2v-7.9H2.3c-0.7,0-1.2-0.5-1.2-1.2 c0-0.6,0.5-1.2,1.2-1.2h7.9V4c0-0.7,0.5-1.2,1.2-1.2s1.2,0.5,1.2,1.2v7.9h7.9C21.1,11.8,21.6,12.4,21.6,13z", fill: "currentColor", "fill-rule": "nonzero" })));
  }
  static get is() { return "digi-icon-plus"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["icon-plus.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["icon-plus.css"]
    };
  }
  static get properties() {
    return {
      "afTitle": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Adds a title element inside the svg"
            }],
          "text": "L\u00E4gger till ett titleelement i svg:n"
        },
        "attribute": "af-title",
        "reflect": false
      },
      "afDesc": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Adds a desc element inside the svg"
            }],
          "text": "L\u00E4gger till ett descelement i svg:n"
        },
        "attribute": "af-desc",
        "reflect": false
      },
      "afSvgAriaHidden": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Hides the icon for screen readers. Default is set to true."
            }],
          "text": "F\u00F6r att d\u00F6lja ikonen f\u00F6r sk\u00E4rml\u00E4sare. Default \u00E4r satt till true."
        },
        "attribute": "af-svg-aria-hidden",
        "reflect": false,
        "defaultValue": "true"
      }
    };
  }
}
