import { h } from '@stencil/core';
import { TagSize } from './tag-size.enum';
/**
 * @swedishName Tagg
 */
export class Tag {
  constructor() {
    this.afText = undefined;
    this.afNoIcon = false;
    this.afSize = TagSize.SMALL;
  }
  clickHandler(e) {
    this.afOnClick.emit(e);
  }
  render() {
    return (h("digi-button", { onAfOnClick: (e) => this.clickHandler(e), "af-variation": "secondary", "af-size": this.afSize, class: "digi-tag" }, this.afText, !this.afNoIcon && h("digi-icon", { afName: `x`, slot: "icon-secondary" })));
  }
  static get is() { return "digi-tag"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["tag.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["tag.css"]
    };
  }
  static get properties() {
    return {
      "afText": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": true,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set the tag text."
            }],
          "text": "S\u00E4tter taggens text."
        },
        "attribute": "af-text",
        "reflect": false
      },
      "afNoIcon": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Removes the tag icon. Defaults to false."
            }],
          "text": "Tar bort taggens ikon. Falskt som f\u00F6rvalt."
        },
        "attribute": "af-no-icon",
        "reflect": false,
        "defaultValue": "false"
      },
      "afSize": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "TagSize",
          "resolved": "TagSize.LARGE | TagSize.MEDIUM | TagSize.SMALL",
          "references": {
            "TagSize": {
              "location": "import",
              "path": "./tag-size.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Sets tag size."
            }],
          "text": "S\u00E4tter taggens storlek."
        },
        "attribute": "af-size",
        "reflect": false,
        "defaultValue": "TagSize.SMALL"
      }
    };
  }
  static get events() {
    return [{
        "method": "afOnClick",
        "name": "afOnClick",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The tag elements 'onclick' event."
            }],
          "text": "Taggelementets 'onclick'-event."
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }];
  }
}
