export var TagSize;
(function (TagSize) {
  TagSize["SMALL"] = "small";
  TagSize["MEDIUM"] = "medium";
  TagSize["LARGE"] = "large";
})(TagSize || (TagSize = {}));
