export var LinkExternalVariation;
(function (LinkExternalVariation) {
  LinkExternalVariation["SMALL"] = "small";
  LinkExternalVariation["LARGE"] = "large";
})(LinkExternalVariation || (LinkExternalVariation = {}));
