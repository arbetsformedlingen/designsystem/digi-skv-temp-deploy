import { h } from '@stencil/core';
import { LinkExternalVariation } from './link-external-variation.enum';
/**
 * @slot default - Ska vara en textnod
 *
 * @enums LinkExternalVariation - link-external-variation.enum.ts
 *@swedishName Extern länk
 */
export class LinkExternal {
  constructor() {
    this.afHref = undefined;
    this.afVariation = LinkExternalVariation.SMALL;
    this.afTarget = undefined;
    this.afOverrideLink = false;
  }
  clickLinkHandler(e) {
    e.stopImmediatePropagation();
    this.afOnClick.emit(e.detail);
  }
  initRouting() {
    const tabIndex = this.hostElement.getAttribute('tabIndex');
    if (!!tabIndex) {
      setTimeout(async () => {
        const linkElement = await this.hostElement
          .querySelector('digi-link')
          .afMGetLinkElement();
        tabIndex === '0' && linkElement.setAttribute('tabIndex', '-1');
      }, 0);
    }
  }
  componentDidLoad() {
    this.initRouting();
  }
  get cssModifiers() {
    return {
      [`digi-link-external--variation-${this.afVariation}`]: !!this.afVariation
    };
  }
  render() {
    return (h("digi-link", { class: Object.assign({ 'digi-link-external': true }, this.cssModifiers), afVariation: this.afVariation, afHref: this.afHref, afOverrideLink: this.afOverrideLink, onAfOnClick: (e) => this.clickLinkHandler(e), afTarget: this.afTarget }, h("digi-icon", { class: "digi-link-external__icon", "aria-hidden": "true", slot: "icon", afName: `external-link-alt` }), h("slot", null)));
  }
  static get is() { return "digi-link-external"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["link-external.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["link-external.css"]
    };
  }
  static get properties() {
    return {
      "afHref": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": true,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set `href` attribute."
            }],
          "text": "S\u00E4tter attributet 'href'."
        },
        "attribute": "af-href",
        "reflect": false
      },
      "afVariation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "`${LinkExternalVariation}`",
          "resolved": "\"large\" | \"small\"",
          "references": {
            "LinkExternalVariation": {
              "location": "import",
              "path": "./link-external-variation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Sets the variation of the link."
            }],
          "text": "S\u00E4tter variant. Kan vara 'small' eller 'large'."
        },
        "attribute": "af-variation",
        "reflect": false,
        "defaultValue": "LinkExternalVariation.SMALL"
      },
      "afTarget": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set `target` attribute. If value is '_blank', the component automatically adds a 'ref' attribute with 'noopener noreferrer'."
            }],
          "text": "S\u00E4tter attributet 'target'. Om v\u00E4rdet \u00E4r '_blank' s\u00E5 l\u00E4gger komponenten automatikt till ett \u00B4ref\u00B4-attribut med 'noopener noreferrer'."
        },
        "attribute": "af-target",
        "reflect": false
      },
      "afOverrideLink": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Override default link behavior. Should only be used if default link behaviour is a problem with e.g. routing"
            }],
          "text": "Kringg\u00E5r l\u00E4nkens vanliga beteende.\r\nB\u00F6r endast anv\u00E4ndas om det vanliga beteendet \u00E4r problematiskt pga dynamisk routing eller liknande."
        },
        "attribute": "af-override-link",
        "reflect": false,
        "defaultValue": "false"
      }
    };
  }
  static get events() {
    return [{
        "method": "afOnClick",
        "name": "afOnClick",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The link element's 'onclick' event."
            }],
          "text": "L\u00E4nkelementets 'onclick'-event."
        },
        "complexType": {
          "original": "MouseEvent",
          "resolved": "MouseEvent",
          "references": {
            "MouseEvent": {
              "location": "global"
            }
          }
        }
      }];
  }
  static get elementRef() { return "hostElement"; }
}
