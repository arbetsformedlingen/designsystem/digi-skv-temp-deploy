export var LinkVariation;
(function (LinkVariation) {
  LinkVariation["SMALL"] = "small";
  LinkVariation["LARGE"] = "large";
})(LinkVariation || (LinkVariation = {}));
