import { h } from '@stencil/core';
import { LinkVariation } from './link-variation.enum';
/**
 * @slot default - Ska vara en textnod
 * @slot icon - Ska vara en ikon (eller en ikonlik) komponent
 *
 * @enums LinkVariation - link-variation.enum.ts
 * @swedishName Länk
 */
export class Link {
  constructor() {
    this.hasIcon = undefined;
    this.relation = undefined;
    this.afHref = undefined;
    this.afVariation = LinkVariation.SMALL;
    this.afTarget = undefined;
    this.afOverrideLink = false;
  }
  /**
   * Hämtar en referens till länkelementet. Bra för att t.ex. sätta fokus programmatiskt.
   * @en Returns a reference to the link element. Handy for setting focus programmatically.
   */
  async afMGetLinkElement() {
    return this._link;
  }
  clickLinkHandler(e) {
    if (this.afOverrideLink) {
      e.preventDefault();
    }
    this.afOnClick.emit(e);
  }
  setRel() {
    var _a;
    this.relation =
      ((_a = this.afTarget) === null || _a === void 0 ? void 0 : _a.toLowerCase()) === '_blank' ? 'noopener noreferrer' : null;
  }
  setHasIcon() {
    this.hasIcon = !!this.hostElement.querySelector('[slot="icon"]');
  }
  initRouting() {
    const tabIndex = this.hostElement.getAttribute('tabIndex');
    !!tabIndex && tabIndex === '0' && this._link.setAttribute('tabIndex', '-1');
  }
  componentWillLoad() {
    this.setHasIcon();
    this.setRel();
  }
  componentWillUpdate() {
    this.setHasIcon();
    this.setRel();
  }
  componentDidLoad() {
    this.initRouting();
  }
  get cssModifiers() {
    return {
      [`digi-link--variation-${this.afVariation}`]: !!this.afVariation,
      'digi-link--has-icon': this.hasIcon
    };
  }
  render() {
    return (h("a", { class: Object.assign({ 'digi-link': true }, this.cssModifiers), href: this.afHref, onClick: (e) => this.clickLinkHandler(e), target: this.afTarget, rel: this.relation, ref: (el) => (this._link = el) }, this.hasIcon && (h("div", { class: "digi-link__icon-wrapper", "aria-hidden": "true" }, h("slot", { name: "icon" }))), h("slot", null)));
  }
  static get is() { return "digi-link"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["link.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["link.css"]
    };
  }
  static get properties() {
    return {
      "afHref": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": true,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set `href` attribute."
            }],
          "text": "S\u00E4tter attributet 'href'."
        },
        "attribute": "af-href",
        "reflect": false
      },
      "afVariation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "`${LinkVariation}`",
          "resolved": "\"large\" | \"small\"",
          "references": {
            "LinkVariation": {
              "location": "import",
              "path": "./link-variation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Sets the variation of the link."
            }],
          "text": "S\u00E4tter variant. Kan vara 'small' eller 'large'."
        },
        "attribute": "af-variation",
        "reflect": false,
        "defaultValue": "LinkVariation.SMALL"
      },
      "afTarget": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set `target` attribute. If value is '_blank', the component automatically adds a 'ref' attribute with 'noopener noreferrer'."
            }],
          "text": "S\u00E4tter attributet 'target'. Om v\u00E4rdet \u00E4r '_blank' s\u00E5 l\u00E4gger komponenten automatikt till ett \u00B4ref\u00B4-attribut med 'noopener noreferrer'."
        },
        "attribute": "af-target",
        "reflect": false
      },
      "afOverrideLink": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Override default link behavior. Should only be used if default link behaviour is a problem with e.g. routing"
            }],
          "text": "Kringg\u00E5r l\u00E4nkens vanliga beteende.\r\nB\u00F6r endast anv\u00E4ndas om det vanliga beteendet \u00E4r problematiskt pga dynamisk routing eller liknande."
        },
        "attribute": "af-override-link",
        "reflect": false,
        "defaultValue": "false"
      }
    };
  }
  static get states() {
    return {
      "hasIcon": {},
      "relation": {}
    };
  }
  static get events() {
    return [{
        "method": "afOnClick",
        "name": "afOnClick",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The link element's 'onclick' event."
            }],
          "text": "L\u00E4nkelementets 'onclick'-event."
        },
        "complexType": {
          "original": "MouseEvent",
          "resolved": "MouseEvent",
          "references": {
            "MouseEvent": {
              "location": "global"
            }
          }
        }
      }];
  }
  static get methods() {
    return {
      "afMGetLinkElement": {
        "complexType": {
          "signature": "() => Promise<any>",
          "parameters": [],
          "references": {
            "Promise": {
              "location": "global"
            }
          },
          "return": "Promise<any>"
        },
        "docs": {
          "text": "H\u00E4mtar en referens till l\u00E4nkelementet. Bra f\u00F6r att t.ex. s\u00E4tta fokus programmatiskt.",
          "tags": [{
              "name": "en",
              "text": "Returns a reference to the link element. Handy for setting focus programmatically."
            }]
        }
      }
    };
  }
  static get elementRef() { return "hostElement"; }
  static get watchers() {
    return [{
        "propName": "afTarget",
        "methodName": "setRel"
      }];
  }
}
