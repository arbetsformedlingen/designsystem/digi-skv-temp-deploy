import { h } from '@stencil/core';
import { TypographyTimeVariation } from './typography-time-variation.enum';
/**
 * @enums TypographyTimeVariation - typography-time-variation.enum.ts
 *@swedishName Datum
 */
export class TypographyTime {
  constructor() {
    this.formatedDate = undefined;
    this.datetime = undefined;
    this.afDateTime = undefined;
    this.afVariation = TypographyTimeVariation.PRIMARY;
  }
  primaryFormat(date) {
    return date.toLocaleString('default', {
      year: 'numeric',
      month: '2-digit',
      day: '2-digit'
    });
  }
  prettyFormat(date) {
    return date.toLocaleString('default', {
      day: 'numeric',
      month: 'long',
      year: 'numeric'
    });
  }
  distanceFormat(date) {
    const oneDay = 1000 * 3600 * 24;
    const distance = Math.floor(Date.now() - date.getTime());
    let days = Math.floor(distance / oneDay);
    days = Math.abs(days);
    if (distance > oneDay) {
      return `För ${days} dagar sedan`;
    }
    if (distance < oneDay) {
      return days === 0 ? `Idag` : `Om ${days} dagar`;
    }
  }
  formatDate(dateToFormat) {
    const date = new Date(dateToFormat);
    switch (this.afVariation) {
      case TypographyTimeVariation.PRIMARY:
        this.formatedDate = this.primaryFormat(date);
        break;
      case TypographyTimeVariation.PRETTY:
        this.formatedDate = this.prettyFormat(date);
        break;
      case TypographyTimeVariation.DISTANCE:
        this.formatedDate = this.distanceFormat(date);
        break;
    }
    this.datetime = this.primaryFormat(date);
  }
  componentWillLoad() {
    this.formatDate(this.afDateTime);
  }
  render() {
    return (h("time", { dateTime: this.datetime, class: "digi-typography-time" }, this.formatedDate));
  }
  static get is() { return "digi-typography-time"; }
  static get encapsulation() { return "scoped"; }
  static get properties() {
    return {
      "afDateTime": {
        "type": "any",
        "mutable": false,
        "complexType": {
          "original": "number | Date | string",
          "resolved": "Date | number | string",
          "references": {
            "Date": {
              "location": "global"
            }
          }
        },
        "required": true,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set the date time to a Date object, date string or a valid date formatted number."
            }],
          "text": "Ska vara ett datumobjekt, datumstr\u00E4ng eller ett datumformatterat nummer"
        },
        "attribute": "af-date-time",
        "reflect": false
      },
      "afVariation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "`${TypographyTimeVariation}`",
          "resolved": "\"distance\" | \"pretty\" | \"primary\"",
          "references": {
            "TypographyTimeVariation": {
              "location": "import",
              "path": "./typography-time-variation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set variation. Can be 'primary', 'pretty' or 'distance'."
            }],
          "text": "S\u00E4tter variant. Best\u00E4mmer hur det ska formatteras. Kan vara 'primary', 'pretty' eller 'distance'."
        },
        "attribute": "af-variation",
        "reflect": false,
        "defaultValue": "TypographyTimeVariation.PRIMARY"
      }
    };
  }
  static get states() {
    return {
      "formatedDate": {},
      "datetime": {}
    };
  }
  static get watchers() {
    return [{
        "propName": "afDateTime",
        "methodName": "formatDate"
      }];
  }
}
