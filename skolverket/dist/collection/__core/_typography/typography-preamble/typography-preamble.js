import { h } from '@stencil/core';
/**
 * @slot default - Should be a text node
 * @swedishName Ingress
 */
export class TypographyPreamble {
  render() {
    return (h("p", { class: "digi-typography-preamble" }, h("slot", null)));
  }
  static get is() { return "digi-typography-preamble"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["typography-preamble.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["typography-preamble.css"]
    };
  }
}
