import { h } from '@stencil/core';
import { TypographyMetaVariation } from './typography-meta-variation.enum';
/**
 * @slot default - Kan användas för olika element, ett text element är föredraget.
 * @slot secondary - Kan användas för olika element, ett text element är föredraget.
 *
 * @enums TypographyMetaVariation - typography-meta-variation.enum.ts
 * @swedishName Metatext
 */
export class TypographyMeta {
  constructor() {
    this.afVariation = TypographyMetaVariation.PRIMARY;
  }
  get cssModifiers() {
    return {
      [`digi-typography-meta--variation-${this.afVariation}`]: !!this.afVariation
    };
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-typography-meta': true }, this.cssModifiers) }, h("div", { class: "digi-typography-meta__meta" }, h("slot", null)), h("div", { class: "digi-typography-meta__meta-secondary" }, h("slot", { name: "secondary" }))));
  }
  static get is() { return "digi-typography-meta"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["typography-meta.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["typography-meta.css"]
    };
  }
  static get properties() {
    return {
      "afVariation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "`${TypographyMetaVariation}`",
          "resolved": "\"primary\" | \"secondary\"",
          "references": {
            "TypographyMetaVariation": {
              "location": "import",
              "path": "./typography-meta-variation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set variation. Can be primary or secondary."
            }],
          "text": "S\u00E4tter variant. Kan vara prim\u00E4r eller sekund\u00E4r."
        },
        "attribute": "af-variation",
        "reflect": false,
        "defaultValue": "TypographyMetaVariation.PRIMARY"
      }
    };
  }
}
