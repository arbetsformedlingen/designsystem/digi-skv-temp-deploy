export var TypographyMetaVariation;
(function (TypographyMetaVariation) {
  TypographyMetaVariation["PRIMARY"] = "primary";
  TypographyMetaVariation["SECONDARY"] = "secondary";
})(TypographyMetaVariation || (TypographyMetaVariation = {}));
