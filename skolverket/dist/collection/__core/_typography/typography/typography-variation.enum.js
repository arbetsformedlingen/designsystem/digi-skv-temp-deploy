export var TypographyVariation;
(function (TypographyVariation) {
  TypographyVariation["SMALL"] = "small";
  TypographyVariation["LARGE"] = "large";
})(TypographyVariation || (TypographyVariation = {}));
