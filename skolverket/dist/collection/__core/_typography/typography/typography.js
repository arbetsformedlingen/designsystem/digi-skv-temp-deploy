import { h } from '@stencil/core';
import { TypographyVariation } from './typography-variation.enum';
/**
 * @slot default - Kan innehålla vad som helst
 *
 * @enums TypographyVariation - typography-variation.enum.ts
 * @swedishName Typografi
 */
export class Typography {
  constructor() {
    this.afVariation = TypographyVariation.SMALL;
  }
  get cssModifiers() {
    return {
      'digi-typography--large': this.afVariation === TypographyVariation.LARGE,
      'digi-typography--small': this.afVariation === TypographyVariation.SMALL,
    };
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-typography': true }, this.cssModifiers) }, h("slot", null)));
  }
  static get is() { return "digi-typography"; }
  static get originalStyleUrls() {
    return {
      "$": ["typography.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["typography.css"]
    };
  }
  static get properties() {
    return {
      "afVariation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "TypographyVariation",
          "resolved": "TypographyVariation.LARGE | TypographyVariation.SMALL",
          "references": {
            "TypographyVariation": {
              "location": "import",
              "path": "./typography-variation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set variation. Can be 'small' or 'large'"
            }],
          "text": "S\u00E4tter variant. Kan vara 'small' eller 'large'"
        },
        "attribute": "af-variation",
        "reflect": false,
        "defaultValue": "TypographyVariation.SMALL"
      }
    };
  }
}
