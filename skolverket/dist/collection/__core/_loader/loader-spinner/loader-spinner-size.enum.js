export var LoaderSpinnerSize;
(function (LoaderSpinnerSize) {
  LoaderSpinnerSize["SMALL"] = "small";
  LoaderSpinnerSize["MEDIUM"] = "medium";
  LoaderSpinnerSize["LARGE"] = "large";
})(LoaderSpinnerSize || (LoaderSpinnerSize = {}));
