import { h, Host } from '@stencil/core';
import { LoaderSpinnerSize } from './loader-spinner-size.enum';
/**
 * @enums LoaderSpinnerSize - loader-spinner-size.enum.ts
 *
 * @swedishName Spinner
 */
export class LoaderSpinner {
  constructor() {
    this.afSize = LoaderSpinnerSize.MEDIUM;
    this.afText = undefined;
  }
  componentWillLoad() { }
  componentDidLoad() { }
  componentWillUpdate() { }
  get cssModifiers() {
    return {
      'digi-loader-spinner--small': this.afSize == 'small',
      'digi-loader-spinner--medium': this.afSize == 'medium',
      'digi-loader-spinner--large': this.afSize == 'large'
    };
  }
  render() {
    return (h(Host, null, h("div", { "aria-label": this.afText, "aria-live": "assertive", class: Object.assign({ 'digi-loader-spinner': true }, this.cssModifiers) }, h("div", { class: "digi-loader-spinner__container", "aria-hidden": "true" }, h("digi-icon", { class: "digi-loader-spinner__spinner", afName: `spinner` })), this.afText && (h("div", { class: "digi-loader-spinner__label" }, this.afText)))));
  }
  static get is() { return "digi-loader-spinner"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["loader-spinner.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["loader-spinner.css"]
    };
  }
  static get properties() {
    return {
      "afSize": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "LoaderSpinnerSize",
          "resolved": "LoaderSpinnerSize.LARGE | LoaderSpinnerSize.MEDIUM | LoaderSpinnerSize.SMALL",
          "references": {
            "LoaderSpinnerSize": {
              "location": "import",
              "path": "./loader-spinner-size.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set spinner icon size. Defaults to medium size."
            }],
          "text": "S\u00E4tter spinnerns storlek. 'medium' \u00E4r f\u00F6rvalt."
        },
        "attribute": "af-size",
        "reflect": false,
        "defaultValue": "LoaderSpinnerSize.MEDIUM"
      },
      "afText": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set spinners text."
            }],
          "text": "S\u00E4tter spinnerns text."
        },
        "attribute": "af-text",
        "reflect": false
      }
    };
  }
  static get elementRef() { return "hostElement"; }
}
