export var NavigationVerticalMenuVariation;
(function (NavigationVerticalMenuVariation) {
  NavigationVerticalMenuVariation["PRIMARY"] = "primary";
  NavigationVerticalMenuVariation["SECONDARY"] = "secondary";
})(NavigationVerticalMenuVariation || (NavigationVerticalMenuVariation = {}));
