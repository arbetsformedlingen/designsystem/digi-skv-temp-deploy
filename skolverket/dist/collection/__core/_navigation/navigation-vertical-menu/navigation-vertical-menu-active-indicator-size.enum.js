export var NavigationVerticalMenuActiveIndicatorSize;
(function (NavigationVerticalMenuActiveIndicatorSize) {
  NavigationVerticalMenuActiveIndicatorSize["PRIMARY"] = "primary";
  NavigationVerticalMenuActiveIndicatorSize["SECONDARY"] = "secondary";
})(NavigationVerticalMenuActiveIndicatorSize || (NavigationVerticalMenuActiveIndicatorSize = {}));
