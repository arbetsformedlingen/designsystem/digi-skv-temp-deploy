import { h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
/**
 * @swedishName Sidofältsknapp
 */
export class NavigationSidebarButton {
  constructor() {
    this.isActive = false;
    this.afText = undefined;
    this.afAriaLabel = undefined;
    this.afId = randomIdGenerator('digi-navigation-sidebar-button');
  }
  toggleHandler() {
    this.isActive = !this.isActive;
    this.afOnToggle.emit(this.isActive);
  }
  render() {
    return (h("digi-button", { id: this.afId, class: "digi-navigation-sidebar-button", "af-variation": "function", "af-aria-label": this.afAriaLabel, onClick: () => this.toggleHandler() }, this.afText, h("digi-icon", { slot: "icon", afName: `bars` })));
  }
  static get is() { return "digi-navigation-sidebar-button"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["navigation-sidebar-button.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["navigation-sidebar-button.css"]
    };
  }
  static get properties() {
    return {
      "afText": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set text for toggle button"
            }],
          "text": "S\u00E4tter texten f\u00F6r toggleknappen."
        },
        "attribute": "af-text",
        "reflect": false
      },
      "afAriaLabel": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input aria-labelledby attribute"
            }],
          "text": "S\u00E4tter attributet 'aria-labelledby'."
        },
        "attribute": "af-aria-label",
        "reflect": false
      },
      "afId": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Label id attribute. Defaults to random string."
            }],
          "text": "S\u00E4tter attributet 'id' p\u00E5 det omslutande elementet. Ges inget v\u00E4rde s\u00E5 genereras ett slumpm\u00E4ssigt."
        },
        "attribute": "af-id",
        "reflect": false,
        "defaultValue": "randomIdGenerator('digi-navigation-sidebar-button')"
      }
    };
  }
  static get states() {
    return {
      "isActive": {}
    };
  }
  static get events() {
    return [{
        "method": "afOnToggle",
        "name": "afOnToggle",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The toggle button's 'onclick'-event"
            }],
          "text": "Toggleknappens 'onclick'-event"
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }];
  }
}
