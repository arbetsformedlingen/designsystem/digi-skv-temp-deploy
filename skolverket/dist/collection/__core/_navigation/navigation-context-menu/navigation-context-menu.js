import { h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { logger } from '../../../global/utils/logger';
import { detectClickOutside } from '../../../global/utils/detectClickOutside';
import { detectFocusOutside } from '../../../global/utils/detectFocusOutside';
import { NavigationContextMenuItemType } from '../navigation-context-menu-item/navigation-context-menu-item-type.enum';
/**
 * @slot default - Ska innehålla en eller flera navigation-context-menu-item.
 * @swedishName Rullgardinsmeny
 */
export class NavigationContextMenu {
  constructor() {
    this.listItems = [];
    this.selectedListItemIndex = 0;
    this.isActive = false;
    this.activeListItemIndex = 1;
    this.afText = undefined;
    this.afId = randomIdGenerator('digi-navigation-context-menu');
    this.afStartSelected = 1;
    this.afNavigationContextMenuItems = undefined;
    this._afNavigationContextMenuItems = undefined;
    this.afIcon = undefined;
  }
  clickHandler(e) {
    const target = e.target;
    const selector = `#${this.afId}-identifier`;
    if (detectClickOutside(target, selector) && this.isActive) {
      this.setInactive();
    }
  }
  focusoutHandler(e) {
    const target = e.target;
    const selector = `#${this.afId}-identifier`;
    if (detectFocusOutside(target, selector) && this.isActive) {
      this.setInactive();
      this.afOnBlur.emit(e);
    }
  }
  setComponentTag() {
    this._afNavigationContextMenuItems = this.afNavigationContextMenuItems;
    this.generateListItems(this.hostElement.children);
  }
  generateListItems(collection) {
    this.listItems = [];
    let slotChildren = Array.from(collection).filter((item) => item.getAttribute('slot') === null &&
      !item.classList.contains('digi-navigation-context-menu'));
    this.digiIcon =
      this.afIcon !== undefined ? 'digi-icon-' + this.afIcon : undefined;
    slotChildren.map((item, i) => {
      this.listItems.push({
        index: i + 1,
        type: item['afType'],
        text: item['afText'],
        value: item.getAttribute('data-value'),
        href: item['afHref'],
        dir: item['afDir'],
        lang: item['afLang']
      });
      item.outerHTML = '';
    });
    if (this.listItems.length === 0) {
      if (this._afNavigationContextMenuItems) {
        try {
          if (this._afNavigationContextMenuItems.constructor === Array) {
            this.listItems = this._afNavigationContextMenuItems;
          }
          else if (typeof this._afNavigationContextMenuItems === 'string') {
            this.listItems = JSON.parse(this._afNavigationContextMenuItems);
          }
          else {
            throw `Invalid type in "navigation-context-menu-items" attribute`;
          }
          /**
           * Quick helper to set correct type depending on if value or href is used.
           */
          const getType = (item) => {
            if (item.type)
              return item.type;
            if (item.href)
              return NavigationContextMenuItemType.LINK;
            if (item.value)
              return NavigationContextMenuItemType.BUTTON;
          };
          this.listItems.map((item, i) => {
            item.index = i + 1;
            item.type = getType(item);
          });
        }
        catch (e) {
          logger.warn(`Invalid JSON in "navigation-context-menu-items" attribute`, this.hostElement, e);
          return;
        }
      }
    }
    if (this.listItems.length === 0) {
      logger.warn(`The slot contains no items or array items.`, this.hostElement);
      return;
    }
    this.setActiveListItemIndex();
    this.selectedListItemIndex = this.afStartSelected
      ? this.afStartSelected
      : this.activeListItemIndex;
  }
  componentWillLoad() {
    this._afNavigationContextMenuItems = this.afNavigationContextMenuItems;
    this.generateListItems(this.hostElement.children);
  }
  debounce(func, timeout = 300) {
    let timer;
    return (...args) => {
      clearTimeout(timer);
      timer = setTimeout(() => {
        func.apply(this, args);
      }, timeout);
    };
  }
  setInactive(focusTrigger = false) {
    this.isActive = false;
    this._toggleButton.querySelector('button').setAttribute('tabindex', null);
    //this.setActiveListItemIndex();
    this.afOnInactive.emit();
    if (focusTrigger) {
      this._toggleButton.querySelector('button').focus();
    }
  }
  setActiveListItemIndex() {
    if (this.afStartSelected) {
      this.activeListItemIndex = this.afStartSelected;
    }
    else {
      this.activeListItemIndex = this.activeListItemIndex;
    }
  }
  setActive() {
    this.isActive = true;
    this._toggleButton.querySelector('button').setAttribute('tabindex', -1);
    setTimeout(() => {
      this.focusActiveItem();
    }, 100);
    this.afOnActive.emit();
  }
  toggleMenu(e) {
    const toggle = this.isActive ? this.setInactive() : this.setActive();
    this.afOnToggle.emit(e);
    return toggle;
  }
  focusActiveItem() {
    const li = document.querySelector(`.digi-navigation-context-menu__item:nth-child(${this.activeListItemIndex})`);
    const findElement = Array.from(li.children).find((el) => el.getAttribute('data-type'));
    const elementType = findElement.getAttribute('data-type');
    if (findElement) {
      let el = elementType === 'button'
        ? findElement.querySelector(elementType)
        : findElement;
      el.focus();
      this.afOnChange.emit(findElement);
    }
  }
  homeHandler() {
    this.activeListItemIndex = 1;
    this.focusActiveItem();
  }
  endHandler() {
    this.activeListItemIndex = this.listItems.length;
    this.focusActiveItem();
  }
  decrementActiveItem() {
    if (this.activeListItemIndex > 1) {
      this.activeListItemIndex = this.activeListItemIndex - 1;
      this.focusActiveItem();
    }
  }
  incrementActiveItem() {
    if (this.activeListItemIndex <= this.listItems.length - 1) {
      this.activeListItemIndex = Number(this.activeListItemIndex) + 1;
      this.focusActiveItem();
    }
  }
  upHandler() {
    this.decrementActiveItem();
  }
  downHandler() {
    this.incrementActiveItem();
  }
  itemClickHandler(e, select = false) {
    if (select) {
      this.selectedListItemIndex = e.target
        .closest('.digi-navigation-context-menu__item')
        .getAttribute('data-key');
      let buttonValue = e.target
        .closest('.digi-navigation-context-menu__button')
        .getAttribute('data-value');
      this.activeListItemIndex = this.selectedListItemIndex;
      let eventvalue = buttonValue ? buttonValue : e.target.textContent;
      this.handleSelect(eventvalue);
    }
    this.focusActiveItem();
    this.setInactive(true);
  }
  handleSelect(value) {
    this.afOnSelect.emit(value);
  }
  showCheckIcon(i, type) {
    return i === Number(this.selectedListItemIndex) && type === 'button'
      ? true
      : false;
  }
  get cssModifiers() {
    return {
      'digi-navigation-context-menu--active': this.isActive
    };
  }
  render() {
    return (h("div", { class: "digi-navigation-context-menu", id: `${this.afId}-identifier` }, h("digi-util-keydown-handler", { onAfOnEsc: () => this.setInactive(true) }, h("digi-button", { ref: (el) => (this._toggleButton = el), onClick: (e) => this.toggleMenu(e), id: `${this.afId}-trigger`, "af-variation": "function", "aria-controls": this.afId, "aria-expanded": this.isActive || null, "aria-haspopup": true, class: Object.assign({ 'digi-navigation-context-menu__trigger-button': true }, this.cssModifiers) }, h(this.digiIcon, { slot: "icon" }), this.afText, h("digi-icon", { class: "digi-navigation-context-menu__toggle-icon", slot: "icon-secondary", afName: `chevron-down` })), h("div", { class: "digi-navigation-context-menu__content", hidden: !this.isActive }, h("digi-util-keyup-handler", { onAfOnHome: () => this.homeHandler(), onAfOnEnd: () => this.endHandler(), onAfOnUp: () => this.upHandler(), onAfOnDown: () => this.downHandler(), onAfOnTab: () => this.setInactive(), onAfOnShiftTab: () => this.setInactive() }, h("ul", { id: this.afId, class: {
        'digi-navigation-context-menu__items': true,
        'digi-navigation-context-menu__items--bordered': this.listItems.length > 4
      }, role: "menu", "aria-describedby": `${this.afId}-trigger` }, this.listItems.map((item) => {
      return (h("li", { role: "none", tabindex: "-1", class: {
          'digi-navigation-context-menu__item': true,
          'digi-navigation-context-menu__item--active': Number(item.index) === Number(this.activeListItemIndex),
          'digi-navigation-context-menu__item--selected': Number(item.index) === Number(this.selectedListItemIndex)
        }, key: item.index, "data-key": item.index }, this.showCheckIcon(item.index, item.type) && (h("digi-icon", { class: "digi-navigation-context-menu__button-icon", slot: "icon", afName: `check` })), item.type === 'button' && (h("digi-button", { "af-tabindex": "-1", role: "menuitem", "data-type": item.type, "data-value": item.value, class: "digi-navigation-context-menu__button", afDir: item.dir, afLang: item.lang, onClick: (e) => this.itemClickHandler(e, true) }, item.text)), item.type === 'link' && (h("a", { tabindex: "-1", role: "menuitem", class: "digi-navigation-context-menu__link", href: item.href, "data-type": item.type, onClick: (e) => this.itemClickHandler(e), dir: item.dir, lang: item.lang }, item.text))));
    }))))), h("digi-util-mutation-observer", { hidden: true, ref: (el) => (this._observer = el), onAfOnChange: this.debounce(() => {
        this.generateListItems(this._observer.children);
      }) }, h("slot", null))));
  }
  static get is() { return "digi-navigation-context-menu"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["navigation-context-menu.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["navigation-context-menu.css"]
    };
  }
  static get properties() {
    return {
      "afText": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": true,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set text for toggle button"
            }],
          "text": "S\u00E4tter texten f\u00F6r toggleknappen."
        },
        "attribute": "af-text",
        "reflect": false
      },
      "afId": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input id attribute. Defaults to random string."
            }],
          "text": "S\u00E4tter attributet 'id'. Om inget v\u00E4ljs s\u00E5 skapas ett slumpm\u00E4ssigt id."
        },
        "attribute": "af-id",
        "reflect": false,
        "defaultValue": "randomIdGenerator('digi-navigation-context-menu')"
      },
      "afStartSelected": {
        "type": "number",
        "mutable": false,
        "complexType": {
          "original": "number",
          "resolved": "number",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Sets initial selected value"
            }],
          "text": "S\u00E4tter det initialt valda v\u00E4rdet"
        },
        "attribute": "af-start-selected",
        "reflect": false,
        "defaultValue": "1"
      },
      "afNavigationContextMenuItems": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string | INavigationContextMenuItem[]",
          "resolved": "INavigationContextMenuItem[] | string",
          "references": {
            "INavigationContextMenuItem": {
              "location": "import",
              "path": "./navigation-context-menu.interface"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set array instead of using children slot."
            }],
          "text": "S\u00E4tt array ist\u00E4llet f\u00F6r att anv\u00E4nda children-slot."
        },
        "attribute": "af-navigation-context-menu-items",
        "reflect": false
      },
      "afIcon": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set name on icon (<digi-icon-Value)"
            }],
          "text": "S\u00E4tt namn p\u00E5 ikonen att ladda in. (<digi-icon-V\u00C4RDE>)"
        },
        "attribute": "af-icon",
        "reflect": false
      }
    };
  }
  static get states() {
    return {
      "selectedListItemIndex": {},
      "isActive": {},
      "activeListItemIndex": {},
      "_afNavigationContextMenuItems": {}
    };
  }
  static get events() {
    return [{
        "method": "afOnInactive",
        "name": "afOnInactive",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "When component gets inactive"
            }],
          "text": "N\u00E4r komponenten st\u00E4ngs"
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnActive",
        "name": "afOnActive",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "When component gets active"
            }],
          "text": "N\u00E4r komponenten \u00F6ppnas"
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnBlur",
        "name": "afOnBlur",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "When focus is move outside of component"
            }],
          "text": "N\u00E4r fokus s\u00E4tts utanf\u00F6r komponenten"
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnChange",
        "name": "afOnChange",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "When navigating to a new list item"
            }],
          "text": "Vid navigering till nytt listobjekt"
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnToggle",
        "name": "afOnToggle",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The toggle button's 'onclick'-event"
            }],
          "text": "Toggleknappens 'onclick'-event"
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnSelect",
        "name": "afOnSelect",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "List item buttons' 'onclick'-event"
            }],
          "text": "'onclick'-event p\u00E5 knappelementen i listan"
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }];
  }
  static get elementRef() { return "hostElement"; }
  static get watchers() {
    return [{
        "propName": "afNavigationContextMenuItems",
        "methodName": "setComponentTag"
      }];
  }
  static get listeners() {
    return [{
        "name": "click",
        "method": "clickHandler",
        "target": "window",
        "capture": false,
        "passive": false
      }, {
        "name": "focusin",
        "method": "focusoutHandler",
        "target": "document",
        "capture": false,
        "passive": false
      }];
  }
}
