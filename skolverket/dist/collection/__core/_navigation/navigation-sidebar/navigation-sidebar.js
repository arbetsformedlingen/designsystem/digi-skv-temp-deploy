import { h, Host } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { NavigationSidebarPosition } from './navigation-sidebar-position.enum';
import { NavigationSidebarVariation } from './navigation-sidebar-variation.enum';
import { NavigationSidebarMobilePosition } from './navigation-sidebar-mobile-position.enum';
import { NavigationSidebarMobileVariation } from './navigation-sidebar-mobile-variation.enum';
import { NavigationSidebarHeadingLevel } from './navigation-sidebar-heading-level.enum';
import { NavigationSidebarCloseButtonPosition } from './navigation-sidebar-close-button-position.enum';
import { UtilBreakpointObserverBreakpoints } from '../../_util/util-breakpoint-observer/util-breakpoint-observer-breakpoints.enum';
/**
 * @slot default - Ska innehålla navigationskomponent
 * @slot footer - Kan innehålla vad som helst
 *
 * @enums NavigationSidebarPosition - navigation-sidebar-position.enum.ts
 * @enums NavigationSidebarVariation - navigation-sidebar-variation.enum.ts
 * @enums NavigationSidebarMobileVariation - navigation-sidebar-mobile-variation.enum.ts
 * @enums NavigationSidebarHeadingLevel - navigation-sidebar-heading-level.enum.ts
 * @enums NavigationSidebarCloseButtonPosition - navigation-sidebar-close-button-position.enum.ts
 * @swedishName Sidofält
 */
export class NavigationSidebar {
  constructor() {
    // private _closeButton;
    this._focusableElementsSelectors = 'a, input, select, textarea, button, button:not(hidden), iframe, object, [tabindex="0"]';
    this.focusableElements = [];
    this.isMobile = undefined;
    this.showHeader = true;
    this.afPosition = NavigationSidebarPosition.START;
    this.afVariation = NavigationSidebarVariation.OVER;
    this.afMobilePosition = undefined;
    this.afMobileVariation = NavigationSidebarMobileVariation.DEFAULT;
    this.afHeading = undefined;
    this.afHeadingLevel = NavigationSidebarHeadingLevel.H2;
    this.afCloseButtonPosition = NavigationSidebarCloseButtonPosition.START;
    this.afStickyHeader = undefined;
    this.afCloseButtonText = undefined;
    this.afCloseButtonAriaLabel = undefined;
    this.afHideHeader = undefined;
    this.afFocusableElement = undefined;
    this.afCloseFocusableElement = undefined;
    this.afActive = false;
    this.afBackdrop = true;
    this.afId = randomIdGenerator('digi-navigation-context-menu');
  }
  toggleHandler(e) {
    this._closeFocusableElement = e.target.querySelector('button');
  }
  breakpointHandler(e) {
    if (e.target.matches('digi-util-breakpoint-observer')) {
      this.isMobile =
        e.detail.value === UtilBreakpointObserverBreakpoints.SMALL ? true : false;
    }
  }
  toggleTransitionEndHandler(e) {
    if (e.target.matches('.digi-navigation-sidebar__wrapper') &&
      e.propertyName === 'visibility') {
      this.setFocus();
    }
  }
  clickHandler(e) {
    // If clicking on toggle button, refresh the focusable items list
    // with the new sub level of items
    if (this.shouldUseFocusTrap) {
      const el = e.detail.target;
      if (el.tagName.toLowerCase() === 'button') {
        setTimeout(() => {
          this.getFocusableItems();
        }, 100);
      }
    }
  }
  setMobileView(isMobile) {
    isMobile && this.afActive
      ? (this.disablePageScroll(), this.getFocusableItems())
      : this.enablePageScroll();
  }
  activeChange(active) {
    if (this.isMobile ||
      (this.afBackdrop &&
        !this.isMobile &&
        this.afVariation !== NavigationSidebarVariation.STATIC)) {
      this.pageScrollToggler();
    }
    if (this.afVariation === NavigationSidebarVariation.PUSH) {
      this.pushPageContentToggler();
    }
    this.afOnToggle.emit(active);
  }
  headerChange(hide) {
    this.setShowHeader(hide);
  }
  setShowHeader(hide) {
    this.showHeader = !hide;
  }
  setHasFooter() {
    const footer = !!this.hostElement.querySelector('[slot="footer"]');
    if (footer) {
      this._hasFooter = footer;
    }
  }
  setFocus() {
    if (this.afActive) {
      let el;
      let firstNavLink;
      // Set first nav link
      if (!this.afHideHeader) {
        firstNavLink = this.focusableElements[1];
      }
      else {
        firstNavLink = this.firstFocusableEl;
      }
      if (!!this.afFocusableElement) {
        el = this.hostElement.querySelector(`${this.afFocusableElement}`);
      }
      else {
        if (!!this.afHeading) {
          el = this._heading;
        }
        else {
          el = firstNavLink;
        }
      }
      if (!!el) {
        el.focus();
      }
    }
    else {
      if (!!this.afCloseFocusableElement) {
        const el = document.querySelector(`${this.afCloseFocusableElement}`);
        el.focus();
      }
      else if (!!this._closeFocusableElement) {
        const el = this._closeFocusableElement;
        el.focus();
      }
    }
  }
  pushPageContentToggler() {
    document.body.classList.remove('digi--has-open-sidebar-right-push');
    document.body.classList.remove('digi--has-open-sidebar-left-push');
    if (this.afActive && this.afVariation === NavigationSidebarVariation.PUSH) {
      this.afPosition === NavigationSidebarPosition.END
        ? document.body.classList.add('digi--has-open-sidebar-right-push')
        : document.body.classList.add('digi--has-open-sidebar-left-push');
    }
  }
  pageScrollToggler() {
    this.afActive ? this.disablePageScroll() : this.enablePageScroll();
  }
  enablePageScroll() {
    document.body.style.height = '';
    document.body.style.overflowY = '';
    document.body.style.paddingRight = '';
  }
  disablePageScroll() {
    document.body.style.height = '100vh';
    document.body.style.overflowY = 'hidden';
    // document.body.style.paddingRight = '17px';
  }
  closeHandler(e) {
    this.afActive = false;
    this.afOnClose.emit(e);
  }
  escHandler(e) {
    if (this.isMobile || this.afVariation !== NavigationSidebarVariation.STATIC) {
      this.afActive = false;
      this.afOnEsc.emit(e);
    }
  }
  tabHandler(e) {
    if (this.shouldUseFocusTrap) {
      if (document.activeElement === this.lastFocusableEl) {
        e.detail.preventDefault();
        this.firstFocusableEl.focus();
      }
    }
  }
  backdropClickHandler(e) {
    this.afActive = false;
    this.afOnBackdropClick.emit(e);
  }
  shiftHandler(e) {
    if (this.shouldUseFocusTrap) {
      if (document.activeElement === this.firstFocusableEl) {
        e.detail.preventDefault();
        this.lastFocusableEl.focus();
      }
    }
  }
  componentWillLoad() {
    this.setHasFooter();
    if (!this.afMobilePosition) {
      this.afMobilePosition =
        this.afPosition === NavigationSidebarPosition.START
          ? NavigationSidebarMobilePosition.START
          : NavigationSidebarMobilePosition.END;
    }
    if (this.afVariation === NavigationSidebarVariation.PUSH) {
      this.pushPageContentToggler();
      const reducedMotion = this.reducedMotion();
      if (!reducedMotion) {
        document.body.style.transition = 'var(--digi--page--transition)';
      }
    }
  }
  componentDidLoad() {
    setTimeout(() => {
      this.getFocusableItems();
    }, 100);
    this.headerChange(this.afHideHeader);
    if (this.afBackdrop &&
      this.afVariation !== NavigationSidebarVariation.STATIC) {
      this.pageScrollToggler();
    }
  }
  componentWillUpdate() {
    this.setHasFooter();
    this.pushPageContentToggler();
  }
  get shouldUseFocusTrap() {
    return ((this.isMobile || this.afVariation !== NavigationSidebarVariation.STATIC) &&
      this.focusableElements.length > 0);
  }
  getFocusableItems() {
    const allElements = this._wrapper.querySelectorAll(this._focusableElementsSelectors);
    // Filters out visible items
    this.focusableElements = Array.prototype.slice
      .call(allElements)
      .filter(function (item) {
      return item.offsetParent !== null;
    });
    // Sets first and last focusable element
    if (this.focusableElements.length > 0) {
      this.firstFocusableEl = this.focusableElements[0];
      this.lastFocusableEl = this.focusableElements[this.focusableElements.length - 1];
    }
  }
  reducedMotion() {
    const reduced = window.matchMedia('(prefers-reduced-motion: reduce)');
    return !reduced || reduced.matches ? true : false;
  }
  get cssModifiers() {
    return {
      'digi-navigation-sidebar__wrapper--start': this.afPosition === NavigationSidebarPosition.START,
      'digi-navigation-sidebar__wrapper--end': this.afPosition === NavigationSidebarPosition.END,
      'digi-navigation-sidebar__wrapper--over': this.afVariation === NavigationSidebarVariation.OVER,
      'digi-navigation-sidebar__wrapper--push': this.afVariation === NavigationSidebarVariation.PUSH,
      'digi-navigation-sidebar__wrapper--static': this.afVariation === NavigationSidebarVariation.STATIC,
      'digi-navigation-sidebar__wrapper--mobile--start': this.afMobilePosition === NavigationSidebarMobilePosition.START,
      'digi-navigation-sidebar__wrapper--mobile--end': this.afMobilePosition === NavigationSidebarMobilePosition.END,
      'digi-navigation-sidebar__wrapper--mobile--default': this.afMobileVariation === NavigationSidebarMobileVariation.DEFAULT,
      'digi-navigation-sidebar__wrapper--mobile--fullwidth': this.afMobileVariation === NavigationSidebarMobileVariation.FULLWIDTH,
      'digi-navigation-sidebar__wrapper--mobile--active': this.isMobile,
      'digi-navigation-sidebar__wrapper--active': this.afActive,
      'digi-navigation-sidebar__wrapper--backdrop': this.afBackdrop,
      'digi-navigation-sidebar__wrapper--sticky-header': this.afStickyHeader
    };
  }
  get cssModifiersBackdrop() {
    return {
      'digi-navigation-sidebar__backdrop--active': this.afActive && this.afBackdrop,
      'digi-navigation-sidebar__backdrop--active--mobile': this.afActive &&
        this.isMobile &&
        this.afMobileVariation !== NavigationSidebarMobileVariation.FULLWIDTH,
      'digi-navigation-sidebar__backdrop--hidden': !this.isMobile && this.afVariation === NavigationSidebarVariation.STATIC
    };
  }
  render() {
    return (h(Host, null, h("digi-util-breakpoint-observer", null, h("digi-util-keydown-handler", { onAfOnEsc: (e) => this.escHandler(e), onAfOnTab: (e) => this.tabHandler(e), onAfOnShiftTab: (e) => this.shiftHandler(e) }, h("div", { class: "digi-navigation-sidebar", "aria-hidden": !this.afActive ? 'true' : 'false' }, h("div", { "aria-hidden": "true", class: Object.assign({ 'digi-navigation-sidebar__backdrop': true }, this.cssModifiersBackdrop), onClick: (e) => this.backdropClickHandler(e) }), h("div", { class: Object.assign({ 'digi-navigation-sidebar__wrapper': true }, this.cssModifiers) }, h("div", { class: "digi-navigation-sidebar__inner", ref: (el) => {
        this._wrapper = el;
      } }, this.showHeader ? (h("div", { class: {
        'digi-navigation-sidebar__header': true,
        'digi-navigation-sidebar__header--close-button--start': this.afCloseButtonPosition ===
          NavigationSidebarCloseButtonPosition.START,
        'digi-navigation-sidebar__header--close-button--end': this.afCloseButtonPosition ===
          NavigationSidebarCloseButtonPosition.END,
        'digi-navigation-sidebar__header--reversed': !!this.afHeading
      } }, this.afHeading && (h(this.afHeadingLevel, { ref: (el) => (this._heading = el), class: "digi-navigation-sidebar__heading", tabindex: "-1" }, this.afHeading)), h("digi-button", {
      // ref={(el) => (this._closeButton = el)}
      onClick: (e) => this.closeHandler(e), "af-variation": "function", "af-aria-label": this.afCloseButtonAriaLabel, class: "digi-navigation-sidebar__close-button"
    }, this.afCloseButtonText, h("digi-icon", { slot: "icon-secondary", afName: `x` })))) : (h("div", null)), h("div", { class: "digi-navigation-sidebar__content" }, h("div", { class: "digi-navigation-sidebar__nav-wrapper" }, h("slot", null)), this._hasFooter && (h("div", { class: "digi-navigation-sidebar__footer" }, h("slot", { name: "footer" })))))))))));
  }
  static get is() { return "digi-navigation-sidebar"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["navigation-sidebar.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["navigation-sidebar.css"]
    };
  }
  static get properties() {
    return {
      "afPosition": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "NavigationSidebarPosition",
          "resolved": "NavigationSidebarPosition.END | NavigationSidebarPosition.START",
          "references": {
            "NavigationSidebarPosition": {
              "location": "import",
              "path": "./navigation-sidebar-position.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Sets sidebar position"
            }],
          "text": "Positionerar menyn"
        },
        "attribute": "af-position",
        "reflect": false,
        "defaultValue": "NavigationSidebarPosition.START"
      },
      "afVariation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "NavigationSidebarVariation",
          "resolved": "NavigationSidebarVariation.OVER | NavigationSidebarVariation.PUSH | NavigationSidebarVariation.STATIC",
          "references": {
            "NavigationSidebarVariation": {
              "location": "import",
              "path": "./navigation-sidebar-variation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Sets sidebar variation"
            }],
          "text": "S\u00E4tt variant"
        },
        "attribute": "af-variation",
        "reflect": false,
        "defaultValue": "NavigationSidebarVariation.OVER"
      },
      "afMobilePosition": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "NavigationSidebarMobilePosition",
          "resolved": "NavigationSidebarMobilePosition.END | NavigationSidebarMobilePosition.START",
          "references": {
            "NavigationSidebarMobilePosition": {
              "location": "import",
              "path": "./navigation-sidebar-mobile-position.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Sets sidebar position in mobile. Defaults to the same as for desktop."
            }],
          "text": "Positionerar menyn i mobilen. S\u00E4tter du inget blir det samma som f\u00F6r st\u00F6rre sk\u00E4rmar."
        },
        "attribute": "af-mobile-position",
        "reflect": false
      },
      "afMobileVariation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "NavigationSidebarMobileVariation",
          "resolved": "NavigationSidebarMobileVariation.DEFAULT | NavigationSidebarMobileVariation.FULLWIDTH",
          "references": {
            "NavigationSidebarMobileVariation": {
              "location": "import",
              "path": "./navigation-sidebar-mobile-variation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Sets sidebar variation for mobile."
            }],
          "text": "S\u00E4tt variant f\u00F6r mobilen."
        },
        "attribute": "af-mobile-variation",
        "reflect": false,
        "defaultValue": "NavigationSidebarMobileVariation.DEFAULT"
      },
      "afHeading": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The heading text"
            }],
          "text": "Rubrikens text"
        },
        "attribute": "af-heading",
        "reflect": false
      },
      "afHeadingLevel": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "NavigationSidebarHeadingLevel",
          "resolved": "NavigationSidebarHeadingLevel.H1 | NavigationSidebarHeadingLevel.H2 | NavigationSidebarHeadingLevel.H3 | NavigationSidebarHeadingLevel.H4 | NavigationSidebarHeadingLevel.H5 | NavigationSidebarHeadingLevel.H6",
          "references": {
            "NavigationSidebarHeadingLevel": {
              "location": "import",
              "path": "./navigation-sidebar-heading-level.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set heading level. Default is 'h2'"
            }],
          "text": "S\u00E4tt rubrikens vikt. 'h2' \u00E4r f\u00F6rvalt."
        },
        "attribute": "af-heading-level",
        "reflect": false,
        "defaultValue": "NavigationSidebarHeadingLevel.H2"
      },
      "afCloseButtonPosition": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "NavigationSidebarCloseButtonPosition",
          "resolved": "NavigationSidebarCloseButtonPosition.END | NavigationSidebarCloseButtonPosition.START",
          "references": {
            "NavigationSidebarCloseButtonPosition": {
              "location": "import",
              "path": "./navigation-sidebar-close-button-position.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Positions the close button, defaults to start"
            }],
          "text": "Justerar st\u00E4ng-knappens position"
        },
        "attribute": "af-close-button-position",
        "reflect": false,
        "defaultValue": "NavigationSidebarCloseButtonPosition.START"
      },
      "afStickyHeader": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Sticks the header area to the top"
            }],
          "text": "L\u00E5ser rubrikdelen mot toppen"
        },
        "attribute": "af-sticky-header",
        "reflect": false
      },
      "afCloseButtonText": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "The close buttons text"
            }],
          "text": "St\u00E4ng-knappens text"
        },
        "attribute": "af-close-button-text",
        "reflect": false
      },
      "afCloseButtonAriaLabel": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set button `aria-label` attribute to the close button"
            }],
          "text": "S\u00E4tt attributet 'aria-label' p\u00E5 st\u00E4ng-knappen"
        },
        "attribute": "af-close-button-aria-label",
        "reflect": false
      },
      "afHideHeader": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Hide header area in sidebar"
            }],
          "text": "D\u00F6lj rubrikdelen i sidof\u00E4ltet"
        },
        "attribute": "af-hide-header",
        "reflect": false
      },
      "afFocusableElement": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Element that is focused when menu is opened,\r\ndefaults to title or close button"
            }],
          "text": "Element som ska f\u00E5 fokus n\u00E4r menyn \u00F6ppnas. S\u00E4tts ingen\r\ns\u00E5 f\u00E5r rubriken eller st\u00E4ng-knappen fokus"
        },
        "attribute": "af-focusable-element",
        "reflect": false
      },
      "afCloseFocusableElement": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Element that is focused when menu is closed,\r\ndefaults to toggle button that opened the sidebar"
            }],
          "text": "Element som ska f\u00E5 fokus n\u00E4r menyn st\u00E4ngs.\r\nS\u00E4tts ingen s\u00E5 f\u00E5r knappen som \u00F6ppnade menyn fokus"
        },
        "attribute": "af-close-focusable-element",
        "reflect": false
      },
      "afActive": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Toggles sidebar. Pass in true for default active"
            }],
          "text": "\u00C4ndrar aktiv status"
        },
        "attribute": "af-active",
        "reflect": false,
        "defaultValue": "false"
      },
      "afBackdrop": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Applies a unscrollable backdrop over the page content when the sidebar is active"
            }],
          "text": "L\u00E4gger en skugga bakom menyn som t\u00E4cker sidinneh\u00E5llet n\u00E4r menyn \u00E4r aktiv"
        },
        "attribute": "af-backdrop",
        "reflect": false,
        "defaultValue": "true"
      },
      "afId": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input id attribute. Defaults to random string."
            }],
          "text": "S\u00E4tter attributet 'id'. Om inget v\u00E4ljs s\u00E5 skapas ett slumpm\u00E4ssigt id."
        },
        "attribute": "af-id",
        "reflect": false,
        "defaultValue": "randomIdGenerator('digi-navigation-context-menu')"
      }
    };
  }
  static get states() {
    return {
      "isMobile": {},
      "showHeader": {}
    };
  }
  static get events() {
    return [{
        "method": "afOnClose",
        "name": "afOnClose",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Close button's 'onclick' event"
            }],
          "text": "St\u00E4ngknappens 'onclick'-event"
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnEsc",
        "name": "afOnEsc",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "At close/open of sidebar using esc-key"
            }],
          "text": "St\u00E4ngning av sidof\u00E4lt med esc-tangenten"
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnBackdropClick",
        "name": "afOnBackdropClick",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "At close of sidebar when clicking on backdrop"
            }],
          "text": "St\u00E4ngning av sidof\u00E4lt med klick p\u00E5 skuggan bakom menyn"
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnToggle",
        "name": "afOnToggle",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "At close/open of sidebar"
            }],
          "text": "Vid \u00F6ppning/st\u00E4ngning av sidof\u00E4lt"
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }];
  }
  static get elementRef() { return "hostElement"; }
  static get watchers() {
    return [{
        "propName": "isMobile",
        "methodName": "setMobileView"
      }, {
        "propName": "afActive",
        "methodName": "activeChange"
      }, {
        "propName": "afHideHeader",
        "methodName": "headerChange"
      }];
  }
  static get listeners() {
    return [{
        "name": "afOnToggle",
        "method": "toggleHandler",
        "target": "window",
        "capture": false,
        "passive": false
      }, {
        "name": "afOnChange",
        "method": "breakpointHandler",
        "target": undefined,
        "capture": false,
        "passive": false
      }, {
        "name": "transitionend",
        "method": "toggleTransitionEndHandler",
        "target": undefined,
        "capture": false,
        "passive": false
      }, {
        "name": "afOnClick",
        "method": "clickHandler",
        "target": undefined,
        "capture": false,
        "passive": false
      }];
  }
}
