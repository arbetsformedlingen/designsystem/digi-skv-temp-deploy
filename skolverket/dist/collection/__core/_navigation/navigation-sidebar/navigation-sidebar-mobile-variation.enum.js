export var NavigationSidebarMobileVariation;
(function (NavigationSidebarMobileVariation) {
  NavigationSidebarMobileVariation["DEFAULT"] = "default";
  NavigationSidebarMobileVariation["FULLWIDTH"] = "fullwidth";
})(NavigationSidebarMobileVariation || (NavigationSidebarMobileVariation = {}));
