export var NavigationSidebarVariation;
(function (NavigationSidebarVariation) {
  NavigationSidebarVariation["OVER"] = "over";
  NavigationSidebarVariation["PUSH"] = "push";
  NavigationSidebarVariation["STATIC"] = "static";
})(NavigationSidebarVariation || (NavigationSidebarVariation = {}));
