export var NavigationSidebarMobilePosition;
(function (NavigationSidebarMobilePosition) {
  NavigationSidebarMobilePosition["START"] = "start";
  NavigationSidebarMobilePosition["END"] = "end";
})(NavigationSidebarMobilePosition || (NavigationSidebarMobilePosition = {}));
