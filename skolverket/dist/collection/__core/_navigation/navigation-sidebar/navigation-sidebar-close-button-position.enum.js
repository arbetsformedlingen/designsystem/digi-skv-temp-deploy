export var NavigationSidebarCloseButtonPosition;
(function (NavigationSidebarCloseButtonPosition) {
  NavigationSidebarCloseButtonPosition["START"] = "start";
  NavigationSidebarCloseButtonPosition["END"] = "end";
})(NavigationSidebarCloseButtonPosition || (NavigationSidebarCloseButtonPosition = {}));
