export var NavigationSidebarPosition;
(function (NavigationSidebarPosition) {
  NavigationSidebarPosition["START"] = "start";
  NavigationSidebarPosition["END"] = "end";
})(NavigationSidebarPosition || (NavigationSidebarPosition = {}));
