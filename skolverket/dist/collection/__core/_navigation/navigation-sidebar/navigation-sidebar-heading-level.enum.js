export var NavigationSidebarHeadingLevel;
(function (NavigationSidebarHeadingLevel) {
  NavigationSidebarHeadingLevel["H1"] = "h1";
  NavigationSidebarHeadingLevel["H2"] = "h2";
  NavigationSidebarHeadingLevel["H3"] = "h3";
  NavigationSidebarHeadingLevel["H4"] = "h4";
  NavigationSidebarHeadingLevel["H5"] = "h5";
  NavigationSidebarHeadingLevel["H6"] = "h6";
})(NavigationSidebarHeadingLevel || (NavigationSidebarHeadingLevel = {}));
