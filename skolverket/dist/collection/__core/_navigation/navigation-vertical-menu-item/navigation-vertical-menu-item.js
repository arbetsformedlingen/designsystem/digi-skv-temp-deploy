import { h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
/**
 * @slot - Expects navigation-vertical-menu-item
 * @swedishName Vertikalt menyval
 */
export class NavigationVerticalMenuItem {
  constructor() {
    this.hasSubnav = false;
    this.hasIcon = false;
    this.isActive = undefined;
    this.activeSubnav = undefined;
    this.listItems = [];
    this.afText = undefined;
    this.afHref = undefined;
    this.afActiveSubnav = undefined;
    this.afActive = false;
    this.afId = randomIdGenerator('digi-navigation-vertical-menu-item');
    this.afOverrideLink = false;
    this.afHasSubnav = false;
  }
  componentWillLoad() {
    this.activeSubnav = this.afActiveSubnav;
  }
  componentDidLoad() {
    this.setSubnav();
    this.setHasIcon();
    this.setHasSubnav();
    this.setIsActive();
  }
  componentWillUpdate() {
    this.setSubnav();
    this.setHasIcon();
    this.setHasSubnav();
  }
  setHasSubnav() {
    var _a, _b;
    this.activeSubnav = this.activeSubnav;
    this.hasSubnav = this.activeSubnav !== undefined;
    if (this.hasSubnav) {
      if (!!this.$subnav) {
        this.$subnav.hidden = !this.activeSubnav;
      }
      this.activeSubnav
        ? (_a = this.$parent) === null || _a === void 0 ? void 0 : _a.classList.add('digi-navigation-vertical-menu__item--active')
        : (_b = this.$parent) === null || _b === void 0 ? void 0 : _b.classList.remove('digi-navigation-vertical-menu__item--active');
    }
  }
  setIsActive() {
    this.isActive = this.afActive;
    this.isActive
      ? this.hostElement.parentElement.classList.add('active')
      : this.hostElement.parentElement.classList.remove('active');
  }
  setSubnav() {
    if (this.hasSubnav) {
      this.$subnav = this.hostElement.parentElement.querySelector('ul');
      this.$parent = this.hostElement.parentElement;
      // Check if UL-element exist or is rendered later dynamically
      if (this.$subnav) {
        this.$subnav.setAttribute('id', `${this.afId}-collapse-menu`);
      }
    }
  }
  setHasIcon() {
    const icon = !!this.hostElement.querySelector('[slot="icon"]');
    this.hasIcon = icon;
  }
  clickHandler(e) {
    const el = e.target;
    if (el.tagName.toLowerCase() === 'a') {
      if (this.afOverrideLink) {
        e.preventDefault();
      }
      if (el.classList.contains('digi-navigation-vertical-menu-item__has-subnav')) {
        this.activeSubnav = true;
      }
    }
    else {
      this.activeSubnav = !this.activeSubnav;
    }
    this.afOnClick.emit(e);
  }
  get toggleButtonText() {
    return this.activeSubnav ? 'Stäng meny' : 'Öppna meny';
  }
  get cssModifiers() {
    return {
      'digi-navigation-vertical-menu-item': true,
      'digi-navigation-vertical-menu-item--active': this.isActive,
      'digi-navigation-vertical-menu-item--active-subnav': this.activeSubnav,
      'digi-navigation-vertical-menu-item--icon': this.hasIcon
    };
  }
  render() {
    return (h("host", { class: {
        'digi-navigation-vertical-menu-item--has-subnav': this.hasSubnav,
        'digi-navigation-vertical-menu-item--active-subnav': this.activeSubnav
      } }, h("div", { class: Object.assign({ 'digi-navigation-vertical-menu-item': true }, this.cssModifiers) }, this.hasSubnav && this.afHref && (h("div", { class: "digi-navigation-vertical-menu-item__inner digi-navigation-vertical-menu-item__inner--combined" }, h("a", { id: `${this.afId}-link`, href: this.afHref, class: {
        'digi-navigation-vertical-menu-item__link': true,
        'digi-navigation-vertical-menu-item__has-subnav': true,
        'digi-navigation-vertical-menu-item__link--active': this.isActive,
        'digi-navigation-vertical-menu-item__link--expanded': this.activeSubnav
      }, onClick: (e) => this.clickHandler(e) }, h("div", { class: "digi-navigation-vertical-menu-item__icon", "aria-hidden": "true", hidden: !this.hasIcon }, h("slot", { name: "icon" })), h("span", { class: "digi-navigation-vertical-menu-item__text" }, this.afText)), h("button", { type: "button", id: `${this.afId}-toggle-button`, class: {
        'digi-navigation-vertical-menu-item__button digi-navigation-vertical-menu-item__button--toggle': true,
        'digi-navigation-vertical-menu-item__button--active': this.isActive
      }, onClick: (e) => this.clickHandler(e), "aria-controls": `${this.afId}-collapse-menu`, "aria-expanded": this.activeSubnav ? 'true' : 'false', "aria-label": `${this.toggleButtonText} ${this.afText}` }, h("span", { class: "digi-navigation-vertical-menu-item__toggle-icon" }, h("span", null, h("digi-icon", { afName: this.activeSubnav ? `minus` : `plus` })))))), this.hasSubnav && !this.afHref && (h("div", { class: "digi-navigation-vertical-menu-item__inner" }, h("button", { id: `${this.afId}-toggle-button`, class: "digi-navigation-vertical-menu-item__button digi-navigation-vertical-menu-item__button--only-toggle", onClick: (e) => this.clickHandler(e), "aria-controls": `${this.afId}-collapse-menu`, "aria-expanded": this.activeSubnav ? 'true' : 'false', type: "button" }, h("span", { class: "digi-navigation-vertical-menu-item__icon", "aria-hidden": "true", hidden: !this.hasIcon }, h("slot", { name: "icon" })), h("span", { class: "digi-navigation-vertical-menu-item__text" }, this.afText), h("span", { class: "digi-navigation-vertical-menu-item__toggle-icon" }, h("span", null, h("digi-icon", { afName: this.activeSubnav ? `minus` : `plus` })))))), !this.hasSubnav && (h("div", { class: "digi-navigation-vertical-menu-item__inner" }, h("a", { id: `${this.afId}-link`, href: this.afHref, class: {
        'digi-navigation-vertical-menu-item__link': true,
        'digi-navigation-vertical-menu-item__link--active': this.isActive
      }, onClick: (e) => this.clickHandler(e) }, h("div", { class: "digi-navigation-vertical-menu-item__icon", "aria-hidden": "true", hidden: !this.hasIcon }, h("slot", { name: "icon" })), h("span", { class: "digi-navigation-vertical-menu-item__text" }, this.afText)))))));
  }
  static get is() { return "digi-navigation-vertical-menu-item"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["navigation-vertical-menu-item.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["navigation-vertical-menu-item.css"]
    };
  }
  static get properties() {
    return {
      "afText": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Sets the item text"
            }],
          "text": "S\u00E4tter texten i komponenten"
        },
        "attribute": "af-text",
        "reflect": false
      },
      "afHref": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set href attribute"
            }],
          "text": "S\u00E4tter attributet 'href'."
        },
        "attribute": "af-href",
        "reflect": false
      },
      "afActiveSubnav": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Toggles subnavigation. Pass in true for default active"
            }],
          "text": "\u00C4ndrar aktiv status p\u00E5 undermenyn. S\u00E4tt som 'true' f\u00F6r att ha \u00F6ppen fr\u00E5n b\u00F6rjan"
        },
        "attribute": "af-active-subnav",
        "reflect": false
      },
      "afActive": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Sets active item. Pass in true for default active"
            }],
          "text": "\u00C4ndrar aktiv status"
        },
        "attribute": "af-active",
        "reflect": false,
        "defaultValue": "false"
      },
      "afId": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set id attribute. Defaults to random string."
            }],
          "text": "S\u00E4tter attributet 'id'. Om inget v\u00E4ljs s\u00E5 skapas ett slumpm\u00E4ssigt id."
        },
        "attribute": "af-id",
        "reflect": false,
        "defaultValue": "randomIdGenerator('digi-navigation-vertical-menu-item')"
      },
      "afOverrideLink": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Override default anchor link behavior and sets focus programmatically. Should only be used if default anchor link behaviour is a problem with e.g. routing"
            }],
          "text": "Kringg\u00E5r l\u00E4nkens vanliga beteende.\r\nB\u00F6r endast anv\u00E4ndas om det vanliga beteendet \u00E4r problematiskt pga dynamisk routing eller liknande."
        },
        "attribute": "af-override-link",
        "reflect": false,
        "defaultValue": "false"
      },
      "afHasSubnav": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [],
          "text": ""
        },
        "attribute": "af-has-subnav",
        "reflect": false,
        "defaultValue": "false"
      }
    };
  }
  static get states() {
    return {
      "hasSubnav": {},
      "hasIcon": {},
      "isActive": {},
      "activeSubnav": {},
      "listItems": {}
    };
  }
  static get events() {
    return [{
        "method": "afOnClick",
        "name": "afOnClick",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Link and toggle buttons 'onclick' event"
            }],
          "text": "L\u00E4nken och toggle-knappens 'onclick'-event"
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }];
  }
  static get elementRef() { return "hostElement"; }
  static get watchers() {
    return [{
        "propName": "afActiveSubnav",
        "methodName": "setHasSubnav"
      }, {
        "propName": "afActive",
        "methodName": "setIsActive"
      }];
  }
}
