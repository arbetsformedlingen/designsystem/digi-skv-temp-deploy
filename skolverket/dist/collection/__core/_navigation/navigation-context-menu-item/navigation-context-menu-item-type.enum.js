export var NavigationContextMenuItemType;
(function (NavigationContextMenuItemType) {
  NavigationContextMenuItemType["LINK"] = "link";
  NavigationContextMenuItemType["BUTTON"] = "button";
})(NavigationContextMenuItemType || (NavigationContextMenuItemType = {}));
