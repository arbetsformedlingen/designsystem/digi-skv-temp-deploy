import { logger } from '../../../global/utils/logger';
import { NavigationContextMenuItemType } from './navigation-context-menu-item-type.enum';
/**
 * @enums NavigationContextMenuItemType - navigation-context-menu-item-type.enum.ts
 * @swedishName Rullgardinsmenyval
 */
export class NavigationContextMenuItem {
  constructor() {
    this.afText = undefined;
    this.afHref = undefined;
    this.afType = undefined;
    this.afLang = undefined;
    this.afDir = undefined;
  }
  componentWillLoad() {
    if (this.afType !== NavigationContextMenuItemType.BUTTON &&
      this.afType !== NavigationContextMenuItemType.LINK) {
      logger.warn(`The navigation-context-menu-item is of unallowed afType. Only button and link is allowed.`, this.hostElement);
      return;
    }
  }
  static get is() { return "digi-navigation-context-menu-item"; }
  static get encapsulation() { return "scoped"; }
  static get properties() {
    return {
      "afText": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": true,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Sets the item text"
            }],
          "text": "S\u00E4tter texten i komponenten"
        },
        "attribute": "af-text",
        "reflect": false
      },
      "afHref": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set href attribute"
            }],
          "text": "S\u00E4tter attributet 'href'."
        },
        "attribute": "af-href",
        "reflect": false
      },
      "afType": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "NavigationContextMenuItemType",
          "resolved": "NavigationContextMenuItemType.BUTTON | NavigationContextMenuItemType.LINK",
          "references": {
            "NavigationContextMenuItemType": {
              "location": "import",
              "path": "./navigation-context-menu-item-type.enum"
            }
          }
        },
        "required": true,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set type. Can be 'link' or 'button'"
            }],
          "text": "S\u00E4tter typ. Kan vara 'link' eller 'button'."
        },
        "attribute": "af-type",
        "reflect": false
      },
      "afLang": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set lang attribute"
            }],
          "text": "S\u00E4tter attributet 'lang'."
        },
        "attribute": "af-lang",
        "reflect": false
      },
      "afDir": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set dir attribute"
            }],
          "text": "S\u00E4tter attributet 'dir'."
        },
        "attribute": "af-dir",
        "reflect": false
      }
    };
  }
  static get elementRef() { return "hostElement"; }
}
