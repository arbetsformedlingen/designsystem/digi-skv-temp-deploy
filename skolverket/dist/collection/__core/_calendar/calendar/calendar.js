import { h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { detectClickOutside } from '../../../global/utils/detectClickOutside';
import { detectFocusOutside } from '../../../global/utils/detectFocusOutside';
import { logger } from '../../../global/utils/logger';
/**
 * @swedishName Kalender
 */
export class Calendar {
  constructor() {
    this.selectedDates = [];
    this.activeCalendar = [];
    this.months = [];
    this.focusedDate = undefined;
    this.dirty = false;
    this.afId = randomIdGenerator('digi-calendar');
    this.afInitSelectedMonth = new Date().getMonth();
    this.afShowWeekNumber = false;
    this.afSelectedDate = undefined;
    this.afActive = true;
    this.afMultipleDates = false;
  }
  focusoutHandler(e) {
    const target = e.target;
    const selector = `#${this.afId}-calendar`;
    if (detectFocusOutside(target, selector)) {
      this.afOnFocusOutside.emit(e);
    }
  }
  clickHandler(e) {
    const target = e.target;
    const selector = `#${this.afId}-calendar`;
    if (detectClickOutside(target, selector)) {
      this.afOnClickOutside.emit(e);
    }
  }
  drawCalendar(focusedDate) {
    const date = {
      year: focusedDate.getFullYear(),
      month: focusedDate.getMonth()
    };
    this.lastWeekOfPreviousMonth = this.getWeekDays(this.getLastDayOfPrevMonth(date));
    this.firstWeekOfNextMonth = this.getWeekDays(this.getFirstDayOfNextMonth(date));
    this.weeksBetween = this.getWeeksBetween(this.getLastDayOfPrevMonth(date), this.getFirstDayOfNextMonth(date));
    this.weekDaysbetween = this.getWeekdaysOfMonth(this.weeksBetween);
    this.activeCalendar = this.appendFirstWeekOfNextMonth()
      ? [
        this.lastWeekOfPreviousMonth,
        ...this.weekDaysbetween,
        this.appendFirstWeekOfNextMonth()
      ]
      : [this.lastWeekOfPreviousMonth, ...this.weekDaysbetween];
    if (this.afShowWeekNumber) {
      this.activeCalendar.forEach((el, i) => {
        this.activeCalendar[i].unshift(this.getWeekNumbers(el[0]));
      });
    }
    this.appendFirstWeekOfNextMonth();
  }
  appendFirstWeekOfNextMonth() {
    const lastArray = JSON.stringify(this.weekDaysbetween.slice(-1)[0]);
    const stringFirstweek = JSON.stringify(this.firstWeekOfNextMonth);
    if (!this.firstWeekOfNextMonth.some((date) => this.isSameMonth(date, this.focusedDate))) {
      return;
    }
    else if (lastArray !== stringFirstweek) {
      return this.firstWeekOfNextMonth;
    }
  }
  getLastDayOfPrevMonth(date) {
    return new Date(date.year, date.month, 0);
  }
  getWeekDays(date) {
    const today = date || new Date();
    const week = [];
    for (let i = 1; i <= 7; i++) {
      const first = today.getDate() - today.getDay() + i;
      const day = new Date(today.setDate(first));
      week.push(day);
    }
    return week;
  }
  getFirstDayOfNextMonth(date) {
    return new Date(date.year, date.month + 1, 1);
  }
  getWeeksBetween(lastDayOfPrevMonth, firstDayOnNextMonth) {
    const week = 7 * 24 * 60 * 60 * 1000;
    const d1ms = lastDayOfPrevMonth.getTime();
    const d2ms = firstDayOnNextMonth.getTime();
    const diff = Math.abs(d2ms - d1ms);
    return Math.floor(diff / week);
  }
  getWeekdaysOfMonth(weeksBetween) {
    let weeks = [];
    for (let i = 1; i <= weeksBetween; i++) {
      weeks = [...weeks, this.getWeekDays(this.getUpcoming(i))];
    }
    return weeks;
  }
  getUpcoming(weeks) {
    return new Date(this.focusedDate.getFullYear(), this.focusedDate.getMonth(), 7 * weeks);
  }
  onDirty() {
    if (!this.dirty) {
      this.dirty = true;
      this.afOnDirty.emit(this.dirty);
    }
  }
  selectDateHandler(dateSelected) {
    this.onDirty();
    if (this.afMultipleDates) {
      if (this.isDateSelected(dateSelected)) {
        this.selectedDates = this.selectedDates.filter((d) => {
          return !this.isSameDate(d, dateSelected);
        });
      }
      else {
        this.selectedDates = [...this.selectedDates, dateSelected];
      }
      if (!!this.afSelectedDate &&
        this.isSameDate(dateSelected, this.afSelectedDate)) {
        this.afSelectedDate = undefined;
      }
      else {
        this.afSelectedDate = new Date(dateSelected);
        this.focusedDate = new Date(dateSelected);
      }
      this.afOnDateSelectedChange.emit(this.selectedDates);
    }
    else {
      if (!!this.afSelectedDate &&
        this.isSameDate(dateSelected, this.afSelectedDate)) {
        this.afSelectedDate = undefined;
      }
      else {
        this.afSelectedDate = new Date(dateSelected);
        this.focusedDate = new Date(dateSelected);
      }
      this.afOnDateSelectedChange.emit([dateSelected]);
    }
  }
  getMonthName(monthNumber) {
    let date = new Date(this.focusedDate);
    let monthName = new Date(date.setMonth(monthNumber)).toLocaleDateString('default', {
      month: 'short'
    });
    return monthName.replace('.', '');
  }
  getFullMonthName(monthNumber) {
    let date = new Date(this.focusedDate);
    return new Date(date.setMonth(monthNumber)).toLocaleDateString('default', {
      month: 'long',
      year: 'numeric'
    });
  }
  // Get the week number of a specific date. Taken from https://weeknumber.com/how-to/javascript
  getWeekNumbers(dateInput) {
    let date = new Date(dateInput);
    date.getTime();
    date.setHours(0, 0, 0, 0);
    date.setDate(date.getDate() + 3 - ((date.getDay() + 6) % 7));
    var week1 = new Date(date.getFullYear(), 0, 4);
    return (1 +
      Math.round(((date.getTime() - week1.getTime()) / 86400000 -
        3 +
        ((week1.getDay() + 6) % 7)) /
        7));
  }
  isSameMonth(date, currentDate) {
    return (`${date.getFullYear()}${date.getMonth()}` ===
      `${currentDate.getFullYear()}${currentDate.getMonth()}`);
  }
  isSameDate(date, newDate) {
    const d1 = `${date.getDate()}${date.getMonth()}${date.getFullYear()}`;
    const d2 = `${newDate.getDate()}${newDate.getMonth()}${newDate.getFullYear()}`;
    return d1 === d2;
  }
  isDateSelected(date) {
    if (this.afMultipleDates) {
      let dates = this.selectedDates;
      let d1 = `${date.getDate()}${date.getMonth()}${date.getFullYear()}`;
      let checkSelected = dates.filter((d) => {
        let dateFormat = `${d.getDate()}${d.getMonth()}${d.getFullYear()}`;
        return d1 === dateFormat;
      });
      if (checkSelected.length) {
        return true;
      }
      else {
        return false;
      }
    }
    else {
      return this.isSameDate(date, new Date(this.afSelectedDate));
    }
  }
  isDisabledDate(date) {
    return !this.isSameMonth(date, this.focusedDate);
  }
  subtractMonth() {
    let month = new Date(this.focusedDate);
    month.setMonth(month.getMonth() - 1);
    this.afInitSelectedMonth = month.getMonth();
    this.focusedDate = month;
  }
  addMonth() {
    let month = new Date(this.focusedDate);
    month.setMonth(month.getMonth() + 1);
    this.afInitSelectedMonth = month.getMonth();
    this.focusedDate = month;
  }
  getCurrent(date) {
    return this.isSameDate(date, new Date()) ? 'date' : null;
  }
  setTabIndex(date) {
    if (this.dirty) {
      return this.isSameDate(date, this.focusedDate) ? 0 : -1;
    }
    else {
      return this.isSameMonth(date, new Date())
        ? this.isSameDate(date, new Date()) && !this.isDisabledDate(date)
          ? 0
          : -1
        : this.isSameMonth(date, this.focusedDate) && date.getDate() === 1
          ? 0
          : -1;
    }
  }
  navigateHandler(e, daysToAdd) {
    !!e.detail && e.detail.preventDefault();
    if (e.detail.key === 'Tab') {
      return;
    }
    this.onDirty();
    const date = new Date(e.detail.target.dataset.date);
    this.focusedDate = new Date(date.setDate(date.getDate() + daysToAdd));
    this.afInitSelectedMonth = this.focusedDate.getMonth();
    this.resetFocus();
  }
  shiftTabHandler() {
    this._nextButton.querySelector('button').focus();
  }
  resetFocus() {
    setTimeout(() => {
      const dates = this._tbody.querySelectorAll('.digi-calendar__date');
      if (dates.length <= 0) {
        logger.warn(`Could not find an element with data-date attribute matching navigated date.`, this._tbody);
        return;
      }
      Array.from(dates).map((date) => {
        let d = date;
        d.tabIndex = -1;
        d.classList.remove('digi-calendar__date--focused');
        if (d.dataset.date === this.getFullDate(this.focusedDate)) {
          d.focus();
          d.tabIndex = 0;
          d.classList.add('digi-calendar__date--focused');
        }
      });
    }, 100);
  }
  getFullDate(date) {
    return date.toLocaleString('default', {
      day: 'numeric',
      month: 'numeric',
      year: 'numeric'
    });
  }
  getDateName(date) {
    return date.toLocaleString('default', {
      weekday: 'long',
      year: 'numeric',
      month: 'long',
      day: 'numeric'
    });
  }
  setFocused(date) {
    if (this.dirty) {
      return this.isSameDate(date, this.focusedDate);
    }
    else {
      return this.isSameMonth(date, new Date())
        ? this.isSameDate(date, new Date()) && !this.isDisabledDate(date)
        : this.isSameMonth(date, this.focusedDate) && date.getDate() === 1;
    }
  }
  ariaPressed(date) {
    return this.isSameDate(date, new Date(this.afSelectedDate))
      ? 'true'
      : 'false';
  }
  componentWillLoad() {
    const startSelectedMonth = new Date(new Date().getFullYear(), this.afInitSelectedMonth);
    this.focusedDate = new Date();
    this.drawCalendar(startSelectedMonth);
  }
  componentWillUpdate() {
    this.drawCalendar(this.focusedDate);
  }
  cssDateModifiers(date) {
    return {
      'digi-calendar__date--today': this.isSameDate(date, new Date()) && !this.isDisabledDate(date),
      'digi-calendar__date--disabled': this.isDisabledDate(date),
      'digi-calendar__date--focused': this.setFocused(date),
      'digi-calendar__date--selected': this.isDateSelected(date)
    };
  }
  render() {
    return (h("div", { class: {
        'digi-calendar': true,
        'digi-calendar--hide': !this.afActive
      }, id: `${this.afId}-calendar` }, h("div", { class: "digi-calendar__header" }, h("button", { class: "digi-calendar__month-select-button", type: "button", "aria-label": 'Gå till ' + this.getFullMonthName(this.afInitSelectedMonth - 1), onClick: () => this.subtractMonth() }, h("digi-icon", { afName: `chevron-left`, slot: "icon", "aria-hidden": "true" }), h("span", { class: "digi-calendar__month-select-name" }, this.getMonthName(this.afInitSelectedMonth - 1))), h("div", { class: "digi-calendar__month" }, this.getFullMonthName(this.afInitSelectedMonth)), h("button", { class: "digi-calendar__month-select-button", type: "button", "aria-label": 'Gå till ' + this.getFullMonthName(this.afInitSelectedMonth + 1), onClick: () => this.addMonth() }, h("span", { class: "digi-calendar__month-select-name" }, this.getMonthName(this.afInitSelectedMonth + 1)), h("digi-icon", { afName: `chevron-right`, slot: "icon", "aria-hidden": "true" }))), h("digi-util-keydown-handler", { onAfOnDown: (e) => this.navigateHandler(e, 7), onAfOnUp: (e) => this.navigateHandler(e, -7), onAfOnLeft: (e) => this.navigateHandler(e, -1), onAfOnRight: (e) => this.navigateHandler(e, 1), onAfOnShiftTab: () => this.shiftTabHandler() }, h("table", { role: "presentation", class: "digi-calendar__table" }, h("thead", { role: "presentation" }, h("tr", { role: "presentation" }, this.afShowWeekNumber && (h("th", { role: "presentation", abbr: "Vecka", class: "digi-calendar__table-header-week" }, "vecka")), h("th", { role: "presentation", abbr: "M\u00E5ndag", class: "digi-calendar__table-header" }, "m\u00E5"), h("th", { role: "presentation", abbr: "Tisdag", class: "digi-calendar__table-header" }, "ti"), h("th", { role: "presentation", abbr: "Onsdag", class: "digi-calendar__table-header" }, "on"), h("th", { role: "presentation", abbr: "Torsdag", class: "digi-calendar__table-header" }, "to"), h("th", { role: "presentation", abbr: "Fredag", class: "digi-calendar__table-header" }, "fr"), h("th", { role: "presentation", abbr: "L\u00F6rdag", class: "digi-calendar__table-header" }, "l\u00F6"), h("th", { role: "presentation", abbr: "S\u00F6ndag", class: "digi-calendar__table-header" }, "s\u00F6"))), h("tbody", { role: "presentation", ref: (el) => (this._tbody = el) }, this.activeCalendar.map((week) => {
      return (h("tr", { role: "presentation" }, week.map((day, i) => {
        if (this.afShowWeekNumber && i === 0) {
          return (h("td", { role: "presentation", class: "digi-calendar__td-week" }, h("div", { class: "digi-calendar__date--week-number" }, h("span", { "aria-hidden": "true" }, day))));
        }
        else {
          return (h("td", { role: "presentation", class: "digi-calendar__td" }, h("button", { type: "button", class: Object.assign(Object.assign({}, this.cssDateModifiers(day)), { 'digi-calendar__date': true }), onClick: () => this.selectDateHandler(day), "aria-current": this.getCurrent(day), "aria-label": this.getDateName(day), "aria-disabled": this.isDisabledDate(day), tabindex: this.setTabIndex(day), "data-date": this.getFullDate(day), "aria-pressed": this.ariaPressed(day) }, h("span", { "aria-hidden": "true" }, day.getDate()))));
        }
      })));
    })))), h("slot", { name: "calendar-footer" })));
  }
  static get is() { return "digi-calendar"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["calendar.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["calendar.css"]
    };
  }
  static get properties() {
    return {
      "afId": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Prefix for all id attributes inside the component. Example: 'my-cool-id' generates 'my-cool-id-calendar' and so on. Defaults to random string."
            }],
          "text": "Prefix f\u00F6r komponentens alla olika interna 'id'-attribut p\u00E5 kalendern etc. Ex. 'my-cool-id' genererar 'my-cool-id-calendar' osv. Ges inget v\u00E4rde genereras ett slumpm\u00E4ssigt."
        },
        "attribute": "af-id",
        "reflect": false,
        "defaultValue": "randomIdGenerator('digi-calendar')"
      },
      "afInitSelectedMonth": {
        "type": "number",
        "mutable": false,
        "complexType": {
          "original": "number",
          "resolved": "number",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Selected month to display from start. Defaults to the month of todays month. (Januray = 0, December = 11)"
            }],
          "text": "Vald m\u00E5nad att visa fr\u00E5n start. F\u00F6rvalt \u00E4r den m\u00E5nad som dagens datum har. (Januari = 0, December = 11)"
        },
        "attribute": "af-init-selected-month",
        "reflect": false,
        "defaultValue": "new Date().getMonth()"
      },
      "afShowWeekNumber": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Show week number in the calander. Default is false"
            }],
          "text": "Visa veckonummer i kalender. F\u00F6rvalt \u00E4r false"
        },
        "attribute": "af-show-week-number",
        "reflect": false,
        "defaultValue": "false"
      },
      "afSelectedDate": {
        "type": "unknown",
        "mutable": false,
        "complexType": {
          "original": "Date",
          "resolved": "Date",
          "references": {
            "Date": {
              "location": "global"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Selected date in calendar"
            }],
          "text": "Valt datum i kalendern"
        }
      },
      "afActive": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Toggle calendar hide/show. Defaults to true"
            }],
          "text": "Visar d\u00F6ljer kalendern. Visas som f\u00F6rvalt"
        },
        "attribute": "af-active",
        "reflect": false,
        "defaultValue": "true"
      },
      "afMultipleDates": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set this to true to be able to select multiple dates in the calander. Default is false"
            }],
          "text": "S\u00E4tt denna till true f\u00F6r att kunna markera mer en ett datum i kalendern. F\u00F6rvalt \u00E4r false"
        },
        "attribute": "af-multiple-dates",
        "reflect": false,
        "defaultValue": "false"
      }
    };
  }
  static get states() {
    return {
      "activeCalendar": {},
      "months": {},
      "focusedDate": {},
      "dirty": {}
    };
  }
  static get events() {
    return [{
        "method": "afOnDateSelectedChange",
        "name": "afOnDateSelectedChange",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "When a date is selected. Return the dates in an array."
            }],
          "text": "Sker n\u00E4r ett datum har valts eller avvalts. Returnerar datumen i en array."
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnFocusOutside",
        "name": "afOnFocusOutside",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "When focus moves out from the calendar"
            }],
          "text": "Sker n\u00E4r fokus flyttas utanf\u00F6r kalendern"
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnClickOutside",
        "name": "afOnClickOutside",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "When click outside the calendar"
            }],
          "text": "Sker vid klick utanf\u00F6r kalendern"
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }, {
        "method": "afOnDirty",
        "name": "afOnDirty",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "When the calendar dates are touched the first time, when focusedDate is changed the first time."
            }],
          "text": "Sker n\u00E4r kalenderdatumen har r\u00F6rts f\u00F6rsta g\u00E5ngen, n\u00E4r v\u00E4rdet p\u00E5 focusedDate har \u00E4ndrats f\u00F6rsta g\u00E5ngen."
        },
        "complexType": {
          "original": "any",
          "resolved": "any",
          "references": {}
        }
      }];
  }
  static get listeners() {
    return [{
        "name": "focusin",
        "method": "focusoutHandler",
        "target": "document",
        "capture": false,
        "passive": false
      }, {
        "name": "click",
        "method": "clickHandler",
        "target": "window",
        "capture": false,
        "passive": false
      }];
  }
}
