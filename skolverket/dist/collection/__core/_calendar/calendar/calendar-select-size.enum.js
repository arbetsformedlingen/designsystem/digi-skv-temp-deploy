export var CalendarSelectSize;
(function (CalendarSelectSize) {
  CalendarSelectSize["SMALL"] = "small";
  CalendarSelectSize["MEDIUM"] = "medium";
  CalendarSelectSize["LARGE"] = "large";
})(CalendarSelectSize || (CalendarSelectSize = {}));
