export var CalendarButtonSize;
(function (CalendarButtonSize) {
  CalendarButtonSize["SMALL"] = "small";
  CalendarButtonSize["MEDIUM"] = "medium";
  CalendarButtonSize["LARGE"] = "large";
})(CalendarButtonSize || (CalendarButtonSize = {}));
