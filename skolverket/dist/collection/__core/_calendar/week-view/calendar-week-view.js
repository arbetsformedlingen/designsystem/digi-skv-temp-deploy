import { h } from '@stencil/core';
import { format, getISOWeek, addDays, subDays, setDay } from 'date-fns';
import svlocale from 'date-fns/locale/sv';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { ButtonVariation } from '../../_button/button/button-variation.enum';
import { CalendarWeekViewHeadingLevel } from './calendar-week-view-heading-level.enum';
/**
 * @enums CalendarWeekViewHeadingLevel - calendar-week-view-heading-level.enum.ts
 * @swedishName Veckovy
 */
export class CalendarWeekView {
  constructor() {
    this.minDate = new Date();
    this.maxDate = new Date();
    this.dates = [];
    this.weekDays = [];
    this.todaysDate = format(new Date(), 'yyyy-MM-dd', { locale: svlocale });
    this.selectedDate = format(new Date(), 'yyyy-MM-dd', { locale: svlocale });
    this.currentDate = undefined;
    this.afMinWeek = undefined;
    this.afMaxWeek = undefined;
    this.afDates = '';
    this.afHeadingLevel = CalendarWeekViewHeadingLevel.H2;
    this.afId = randomIdGenerator('digi-calendar-week-view');
  }
  selectedDateChangeHandler() {
    this.afOnDateChange.emit(this.selectedDate);
  }
  currentDateChangeHandler() {
    this.setWeekDates();
  }
  setWeekDates() {
    let newDates = [];
    for (let i = 1; i < 6; i++) {
      const day = setDay(new Date(this.currentDate), i);
      const date = format(new Date(day), 'yyyy-MM-dd', { locale: svlocale });
      newDates.push(date);
    }
    this.weekDays = [...newDates];
  }
  afDatesUpdate() {
    const dateList = [...JSON.parse(this.afDates)];
    this.dates = [
      ...dateList.sort((a, b) => {
        var c = new Date(a);
        var d = new Date(b);
        return c - d;
      })
    ];
  }
  /**
   * Kan användas för att manuellt sätta om det aktiva datumet. Formatet måste vara 'yyyy-MM-dd'.
   * @en Can be used to set the active date. The format needs to be 'yyy-MM-dd'.
   */
  async afMSetActiveDate(activeDate) {
    this.selectedDate = format(new Date(activeDate), 'yyyy-MM-dd', {
      locale: svlocale
    });
    this.currentDate = this.selectedDate;
  }
  prevWeek() {
    this.currentDate = subDays(new Date(this.currentDate), 7);
    this.afOnWeekChange.emit('previous');
  }
  nextWeek() {
    this.currentDate = addDays(new Date(this.currentDate), 7);
    this.afOnWeekChange.emit('next');
  }
  initDates() {
    this.minDate = new Date(this.dates[0]);
    this.maxDate = new Date(this.dates[this.dates.length - 1]);
    this.selectedDate = format(new Date(this.dates[0]), 'yyyy-MM-dd', {
      locale: svlocale
    });
    this.currentDate = format(new Date(this.selectedDate), 'yyyy-MM-dd', {
      locale: svlocale
    });
  }
  componentWillLoad() {
    this.afDatesUpdate();
    this.initDates();
  }
  get prevWeekText() {
    const nextWeekDate = subDays(new Date(this.currentDate), 7);
    return `Vecka ${getISOWeek(new Date(nextWeekDate))}`;
  }
  get nextWeekText() {
    const nextWeekDate = addDays(new Date(this.currentDate), 7);
    return `Vecka ${getISOWeek(new Date(nextWeekDate))}`;
  }
  get currentWeek() {
    return getISOWeek(new Date(this.currentDate));
  }
  get currentMonth() {
    const firstDate = new Date(this.weekDays[0]);
    const lastDate = new Date(this.weekDays[this.weekDays.length - 1]);
    return firstDate.getMonth() == lastDate.getMonth()
      ? format(new Date(firstDate), 'MMMM yyyy', { locale: svlocale })
      : format(new Date(firstDate), 'MMMM yyyy', { locale: svlocale }) +
        ' - ' +
        format(new Date(lastDate), 'MMMM yyyy', { locale: svlocale });
  }
  render() {
    return (h("div", { class: "digi-calendar-week-view" }, h("nav", { class: "digi-calendar-week-view__timepicker" }, h("div", { class: "digi-calendar-week-view__dates" }, h("div", { class: "digi-calendar-week-view__flex" }, h("digi-typography", null, h(this.afHeadingLevel, { role: "status", class: "digi-calendar-week-view__heading" }, h("span", { class: "digi-calendar-week-view__month" }, this.currentMonth), h("span", { class: "digi-calendar-week-view__week" }, ' ', "v.", this.currentWeek))), h("ul", { class: "digi-calendar-week-view__date-list" }, this.weekDays.map((dateItem) => {
      const date = format(new Date(dateItem), 'd', {
        locale: svlocale
      });
      const day = format(new Date(dateItem), 'EEE', {
        locale: svlocale
      });
      const fullDay = format(new Date(dateItem), 'EEEE ', {
        locale: svlocale
      });
      return (h("li", { class: "digi-calendar-week-view__date-list-item", "aria-current": dateItem === this.todaysDate && 'date' }, h("digi-button", { onClick: () => (this.selectedDate = dateItem), afAriaLabel: `${fullDay} ${date}`, "aria-hidden": !this.dates.includes(dateItem) ? 'true' : 'false', "af-tabindex": !this.dates.includes(dateItem) ? -1 : null, "aria-pressed": this.selectedDate == dateItem ? 'true' : 'false', class: {
          'digi-calendar-week-view__date-button': true,
          'digi-calendar-week-view__date-button--active': this.selectedDate == dateItem,
          'digi-calendar-week-view__date-button--no-value': !this.dates.includes(dateItem)
        }, "af-variation": ButtonVariation.SECONDARY }, h("span", { class: "digi-calendar-week-view__date-button-content", "aria-hidden": "true" }, h("span", { class: {
          'digi-calendar-week-view__date-button-day': this.selectedDate != dateItem,
          'digi-calendar-week-view__date-button-day--active': this.selectedDate == dateItem
        } }, day), h("span", { class: "digi-calendar-week-view__date-button-date" }, date), h("span", { "aria-hidden": "true", class: {
          'digi-calendar-week-view__date-button-circle': dateItem === this.todaysDate
        } })))));
    })))), h("digi-button", { onClick: () => this.prevWeek(), class: {
        'digi-calendar-week-view__week-button digi-calendar-week-view__week-button--previous': true,
        'digi-calendar-week-view__week-button--keep-left': this.currentWeek == getISOWeek(this.maxDate),
        'digi-calendar-week-view__week-button--hidden': !(this.currentWeek > getISOWeek(this.minDate))
      }, "af-variation": "secondary" }, h("digi-icon", { slot: "icon", afName: `chevron-left` }), this.prevWeekText), h("digi-button", { onClick: () => this.nextWeek(), class: {
        'digi-calendar-week-view__week-button digi-calendar-week-view__week-button--next': true,
        'digi-calendar-week-view__week-button--keep-right': this.currentWeek == getISOWeek(this.minDate),
        'digi-calendar-week-view__week-button--hidden': !(this.currentWeek < getISOWeek(this.maxDate))
      }, "af-variation": "secondary" }, h("digi-icon", { slot: "icon-secondary", afName: `chevron-right` }), this.nextWeekText)), h("div", { role: "status", "aria-live": "assertive", class: "digi-calendar-week-view__result", id: `${this.afId}-result` }, h("digi-typography", null, h("slot", null)))));
  }
  static get is() { return "digi-calendar-week-view"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["calendar-week-view.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["calendar-week-view.css"]
    };
  }
  static get properties() {
    return {
      "afMinWeek": {
        "type": "number",
        "mutable": true,
        "complexType": {
          "original": "number",
          "resolved": "number",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [],
          "text": ""
        },
        "attribute": "af-min-week",
        "reflect": false
      },
      "afMaxWeek": {
        "type": "number",
        "mutable": true,
        "complexType": {
          "original": "number",
          "resolved": "number",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [],
          "text": ""
        },
        "attribute": "af-max-week",
        "reflect": false
      },
      "afDates": {
        "type": "string",
        "mutable": true,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "List of selectable dates"
            }],
          "text": "Lista \u00F6ver valbara datum"
        },
        "attribute": "af-dates",
        "reflect": false,
        "defaultValue": "''"
      },
      "afHeadingLevel": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "CalendarWeekViewHeadingLevel",
          "resolved": "CalendarWeekViewHeadingLevel.H1 | CalendarWeekViewHeadingLevel.H2 | CalendarWeekViewHeadingLevel.H3 | CalendarWeekViewHeadingLevel.H4 | CalendarWeekViewHeadingLevel.H5 | CalendarWeekViewHeadingLevel.H6",
          "references": {
            "CalendarWeekViewHeadingLevel": {
              "location": "import",
              "path": "./calendar-week-view-heading-level.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set heading level. Default is 'h2'"
            }],
          "text": "S\u00E4tt rubrikens vikt. 'h2' \u00E4r f\u00F6rvalt."
        },
        "attribute": "af-heading-level",
        "reflect": false,
        "defaultValue": "CalendarWeekViewHeadingLevel.H2"
      },
      "afId": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Input id attribute. Defaults to random string."
            }],
          "text": "S\u00E4tter attributet 'id'. Om inget v\u00E4ljs s\u00E5 skapas ett slumpm\u00E4ssigt id."
        },
        "attribute": "af-id",
        "reflect": false,
        "defaultValue": "randomIdGenerator('digi-calendar-week-view')"
      }
    };
  }
  static get states() {
    return {
      "dates": {},
      "weekDays": {},
      "todaysDate": {},
      "selectedDate": {},
      "currentDate": {}
    };
  }
  static get events() {
    return [{
        "method": "afOnWeekChange",
        "name": "afOnWeekChange",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "When week changes"
            }],
          "text": "Vid byte av vecka"
        },
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        }
      }, {
        "method": "afOnDateChange",
        "name": "afOnDateChange",
        "bubbles": true,
        "cancelable": true,
        "composed": true,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "When day changes"
            }],
          "text": "Vid byte av dag"
        },
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        }
      }];
  }
  static get methods() {
    return {
      "afMSetActiveDate": {
        "complexType": {
          "signature": "(activeDate: string) => Promise<void>",
          "parameters": [{
              "tags": [],
              "text": ""
            }],
          "references": {
            "Promise": {
              "location": "global"
            }
          },
          "return": "Promise<void>"
        },
        "docs": {
          "text": "Kan anv\u00E4ndas f\u00F6r att manuellt s\u00E4tta om det aktiva datumet. Formatet m\u00E5ste vara 'yyyy-MM-dd'.",
          "tags": [{
              "name": "en",
              "text": "Can be used to set the active date. The format needs to be 'yyy-MM-dd'."
            }]
        }
      }
    };
  }
  static get watchers() {
    return [{
        "propName": "selectedDate",
        "methodName": "selectedDateChangeHandler"
      }, {
        "propName": "currentDate",
        "methodName": "currentDateChangeHandler"
      }, {
        "propName": "afDates",
        "methodName": "afDatesUpdate"
      }];
  }
}
