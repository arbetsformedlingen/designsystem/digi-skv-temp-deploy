import { h } from '@stencil/core';
import { LayoutColumnsElement } from './layout-columns-element.enum';
import { LayoutColumnsVariation } from './layout-columns-variation.enum';
/**
 * @slot default - Kan innehålla vad som helst
 *
 * @enums LayoutColumnsVariation - layout-columns-variation.enum.ts
 * @enums LayoutColumnsElement - layout-columns-element.enum.ts
 * @swedishName Kolumner
 */
export class LayoutColumns {
  constructor() {
    this.afElement = LayoutColumnsElement.DIV;
    this.afVariation = undefined;
  }
  get cssModifiers() {
    return {
      'digi-layout-columns--two': this.afVariation === LayoutColumnsVariation.TWO,
      'digi-layout-columns--three': this.afVariation === LayoutColumnsVariation.THREE,
    };
  }
  render() {
    return (h(this.afElement, { class: Object.assign({ 'digi-layout-columns': true }, this.cssModifiers) }, h("slot", null)));
  }
  static get is() { return "digi-layout-columns"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["layout-columns.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["layout-columns.css"]
    };
  }
  static get properties() {
    return {
      "afElement": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "LayoutColumnsElement",
          "resolved": "LayoutColumnsElement.DIV | LayoutColumnsElement.OL | LayoutColumnsElement.UL",
          "references": {
            "LayoutColumnsElement": {
              "location": "import",
              "path": "./layout-columns-element.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set wrapper element type. can be 'div', 'ul' or 'ol'."
            }],
          "text": "S\u00E4tter elementtypen p\u00E5 det omslutande elementet. Kan vara 'div', 'ul' eller 'ol'."
        },
        "attribute": "af-element",
        "reflect": false,
        "defaultValue": "LayoutColumnsElement.DIV"
      },
      "afVariation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "LayoutColumnsVariation",
          "resolved": "LayoutColumnsVariation.THREE | LayoutColumnsVariation.TWO",
          "references": {
            "LayoutColumnsVariation": {
              "location": "import",
              "path": "./layout-columns-variation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set preset column variations. Can be 'two' or 'three'."
            }],
          "text": "S\u00E4tter f\u00F6rvalda kolumnvarianter. Kan vara 'two' eller 'three'."
        },
        "attribute": "af-variation",
        "reflect": false
      }
    };
  }
}
