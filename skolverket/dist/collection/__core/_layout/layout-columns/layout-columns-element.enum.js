export var LayoutColumnsElement;
(function (LayoutColumnsElement) {
  LayoutColumnsElement["DIV"] = "div";
  LayoutColumnsElement["UL"] = "ul";
  LayoutColumnsElement["OL"] = "ol";
})(LayoutColumnsElement || (LayoutColumnsElement = {}));
