import { h } from '@stencil/core';
import { LayoutContainerVariation } from './layout-container-variation.enum';
/**
 * @slot default - kan innehålla vad som helst
 *
 * @enums LayoutContainerVariation - layout-container-variation.enum.ts
 * @swedishName Container
 */
export class LayoutContainer {
  constructor() {
    this._variation = LayoutContainerVariation.STATIC;
    this.afVariation = LayoutContainerVariation.STATIC;
    this.afNoGutter = undefined;
    this.afVerticalPadding = undefined;
    this.afMarginTop = undefined;
    this.afMarginBottom = undefined;
  }
  variationChangeHandler() {
    this._variation = this.afVariation;
  }
  componentWillLoad() {
    this.variationChangeHandler();
  }
  get cssModifiers() {
    if (this._variation === LayoutContainerVariation.NONE) {
      return {};
    }
    return {
      'digi-layout-container': true,
      'digi-layout-container--static': this._variation === LayoutContainerVariation.STATIC,
      'digi-layout-container--fluid': this._variation === LayoutContainerVariation.FLUID,
      'digi-layout-container--no-gutter': this.afNoGutter,
      'digi-layout-container--vertical-padding': this.afVerticalPadding,
      'digi-layout-container--margin-top': this.afMarginTop,
      'digi-layout-container--margin-bottom': this.afMarginBottom
    };
  }
  render() {
    return (h("div", { class: Object.assign({}, this.cssModifiers) }, h("slot", null)));
  }
  static get is() { return "digi-layout-container"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["layout-container.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["layout-container.css"]
    };
  }
  static get properties() {
    return {
      "afVariation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "LayoutContainerVariation",
          "resolved": "LayoutContainerVariation.FLUID | LayoutContainerVariation.NONE | LayoutContainerVariation.STATIC",
          "references": {
            "LayoutContainerVariation": {
              "location": "import",
              "path": "./layout-container-variation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set container variation. Can be 'static' or 'fluid'."
            }],
          "text": "S\u00E4tter containervarianten. Kan vara 'static' eller 'fluid'."
        },
        "attribute": "af-variation",
        "reflect": false,
        "defaultValue": "LayoutContainerVariation.STATIC"
      },
      "afNoGutter": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Remove gutters from container sides"
            }],
          "text": "Tar bort marginalen fr\u00E5n sidorna av containern"
        },
        "attribute": "af-no-gutter",
        "reflect": false
      },
      "afVerticalPadding": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Adds vertical padding inside the container"
            }],
          "text": "L\u00E4gger till vertikal padding inuti containern"
        },
        "attribute": "af-vertical-padding",
        "reflect": false
      },
      "afMarginTop": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Adds margin on top of the container"
            }],
          "text": "L\u00E4gger till marginal i toppen p\u00E5 containern"
        },
        "attribute": "af-margin-top",
        "reflect": false
      },
      "afMarginBottom": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Adds margin at bottom of the container"
            }],
          "text": "L\u00E4gger till marginal i botten p\u00E5 containern"
        },
        "attribute": "af-margin-bottom",
        "reflect": false
      }
    };
  }
  static get states() {
    return {
      "_variation": {}
    };
  }
  static get watchers() {
    return [{
        "propName": "afVariation",
        "methodName": "variationChangeHandler"
      }];
  }
}
