import { h } from '@stencil/core';
import { LayoutMediaObjectAlignment } from './layout-media-object-alignment.enum';
/**
 * @slot media - Bör innehålla någon sorts mediakompinent (t.ex. en bild eller en video).
 * @slot default - kan innehålla vad som helst. Vanligen är det textinnehåll.
 *
 * @enums LayoutMediaObjectAlignment - layout-media-object-alignment.enum.ts
 * @swedishName Medieobjekt
 */
export class LayoutMediaObject {
  constructor() {
    this.afAlignment = LayoutMediaObjectAlignment.START;
  }
  get cssModifiers() {
    return {
      [`digi-layout-media-object--alignment-${this.afAlignment}`]: !!this.afAlignment
    };
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-layout-media-object': true }, this.cssModifiers) }, h("div", { class: "digi-layout-media-object__first" }, h("slot", { name: "media" })), h("div", { class: "digi-layout-media-object__last" }, h("slot", null))));
  }
  static get is() { return "digi-layout-media-object"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["layout-media-object.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["layout-media-object.css"]
    };
  }
  static get properties() {
    return {
      "afAlignment": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "`${LayoutMediaObjectAlignment}`",
          "resolved": "\"center\" | \"end\" | \"start\" | \"stretch\"",
          "references": {
            "LayoutMediaObjectAlignment": {
              "location": "import",
              "path": "./layout-media-object-alignment.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set alignment of the content. Can be 'center', 'start', 'end' or 'stretch'."
            }],
          "text": "S\u00E4tter justeringen av inneh\u00E5llet. Kan vara 'center', 'start', 'end' eller 'stretch'."
        },
        "attribute": "af-alignment",
        "reflect": false,
        "defaultValue": "LayoutMediaObjectAlignment.START"
      }
    };
  }
}
