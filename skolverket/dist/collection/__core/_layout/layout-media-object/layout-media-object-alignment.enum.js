export var LayoutMediaObjectAlignment;
(function (LayoutMediaObjectAlignment) {
  LayoutMediaObjectAlignment["CENTER"] = "center";
  LayoutMediaObjectAlignment["START"] = "start";
  LayoutMediaObjectAlignment["END"] = "end";
  LayoutMediaObjectAlignment["STRETCH"] = "stretch";
})(LayoutMediaObjectAlignment || (LayoutMediaObjectAlignment = {}));
