import { h } from '@stencil/core';
import { LayoutBlockVariation } from './layout-block-variation.enum';
import { LayoutBlockContainer } from './layout-block-container.enum';
/**
 * @slot default - Kan innehålla vad som helst
 *
 * @enums LayoutBlockContainer - layout-block-container.enum.ts
 * @enums LayoutBlockVariation - layout-block-variation.enum.ts
 *@swedishName Block
 */
export class LayoutBlock {
  constructor() {
    this._container = LayoutBlockContainer.STATIC;
    this.afVariation = LayoutBlockVariation.PRIMARY;
    this.afContainer = LayoutBlockContainer.STATIC;
    this.afVerticalPadding = undefined;
    this.afMarginTop = undefined;
    this.afMarginBottom = undefined;
  }
  containerChangeHandler() {
    this._container = this.afContainer;
  }
  componentWillLoad() {
    this.containerChangeHandler();
  }
  get cssModifiers() {
    return {
      'digi-layout-block--transparent': this.afVariation === LayoutBlockVariation.TRANSPARENT,
      'digi-layout-block--primary': this.afVariation === LayoutBlockVariation.PRIMARY,
      'digi-layout-block--secondary': this.afVariation === LayoutBlockVariation.SECONDARY,
      'digi-layout-block--tertiary': this.afVariation === LayoutBlockVariation.TERTIARY,
      'digi-layout-block--symbol': this.afVariation === LayoutBlockVariation.SYMBOL,
      'digi-layout-block--profile': this.afVariation === LayoutBlockVariation.PROFILE
    };
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-layout-block': true }, this.cssModifiers) }, this._container === LayoutBlockContainer.NONE ? (h("slot", null)) : (h("digi-layout-container", { afVariation: this._container, "af-vertical-padding": this.afVerticalPadding, "af-margin-top": this.afMarginTop, "af-margin-bottom": this.afMarginBottom }, h("slot", null)))));
  }
  static get is() { return "digi-layout-block"; }
  static get encapsulation() { return "scoped"; }
  static get originalStyleUrls() {
    return {
      "$": ["layout-block.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["layout-block.css"]
    };
  }
  static get properties() {
    return {
      "afVariation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "LayoutBlockVariation",
          "resolved": "LayoutBlockVariation.PRIMARY | LayoutBlockVariation.PROFILE | LayoutBlockVariation.SECONDARY | LayoutBlockVariation.SYMBOL | LayoutBlockVariation.TERTIARY | LayoutBlockVariation.TRANSPARENT",
          "references": {
            "LayoutBlockVariation": {
              "location": "import",
              "path": "./layout-block-variation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set variation. Controls background color."
            }],
          "text": "S\u00E4tter variant. Kontrollerar bakgrundsf\u00E4rgen."
        },
        "attribute": "af-variation",
        "reflect": false,
        "defaultValue": "LayoutBlockVariation.PRIMARY"
      },
      "afContainer": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "LayoutBlockContainer",
          "resolved": "LayoutBlockContainer.FLUID | LayoutBlockContainer.NONE | LayoutBlockContainer.STATIC",
          "references": {
            "LayoutBlockContainer": {
              "location": "import",
              "path": "./layout-block-container.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set inner container variation, or remove it completely. Can be 'static', 'fluid' or 'none'."
            }],
          "text": "S\u00E4tter den inre containervarianten, eller tar bort den helt. Kan vara 'static', 'fluid' eller 'none'."
        },
        "attribute": "af-container",
        "reflect": false,
        "defaultValue": "LayoutBlockContainer.STATIC"
      },
      "afVerticalPadding": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Adds vertical padding inside the container"
            }],
          "text": "L\u00E4gger till vertikal padding inuti containern"
        },
        "attribute": "af-vertical-padding",
        "reflect": false
      },
      "afMarginTop": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Adds margin on top of the container"
            }],
          "text": "L\u00E4gger till marginal i toppen p\u00E5 containern"
        },
        "attribute": "af-margin-top",
        "reflect": false
      },
      "afMarginBottom": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Adds margin at bottom of the container"
            }],
          "text": "L\u00E4gger till marginal i botten p\u00E5 containern"
        },
        "attribute": "af-margin-bottom",
        "reflect": false
      }
    };
  }
  static get states() {
    return {
      "_container": {}
    };
  }
  static get watchers() {
    return [{
        "propName": "afContainer",
        "methodName": "containerChangeHandler"
      }];
  }
}
