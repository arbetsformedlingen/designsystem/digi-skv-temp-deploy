export var LayoutBlockContainer;
(function (LayoutBlockContainer) {
  LayoutBlockContainer["STATIC"] = "static";
  LayoutBlockContainer["FLUID"] = "fluid";
  LayoutBlockContainer["NONE"] = "none";
})(LayoutBlockContainer || (LayoutBlockContainer = {}));
