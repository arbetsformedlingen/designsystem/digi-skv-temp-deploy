export var LayoutBlockVariation;
(function (LayoutBlockVariation) {
  LayoutBlockVariation["TRANSPARENT"] = "transparent";
  LayoutBlockVariation["PRIMARY"] = "primary";
  LayoutBlockVariation["SECONDARY"] = "secondary";
  LayoutBlockVariation["TERTIARY"] = "tertiary";
  LayoutBlockVariation["SYMBOL"] = "symbol";
  LayoutBlockVariation["PROFILE"] = "profile";
})(LayoutBlockVariation || (LayoutBlockVariation = {}));
