export var CodeExampleLanguage;
(function (CodeExampleLanguage) {
  CodeExampleLanguage["JSON"] = "JSON";
  CodeExampleLanguage["CSS"] = "CSS";
  CodeExampleLanguage["SCSS"] = "SCSS";
  CodeExampleLanguage["TYPESCRIPT"] = "Typescript";
  CodeExampleLanguage["JAVASCRIPT"] = "Javascript";
  CodeExampleLanguage["BASH"] = "Bash";
  CodeExampleLanguage["HTML"] = "HTML";
  CodeExampleLanguage["GIT"] = "Git";
  CodeExampleLanguage["ANGULAR"] = "Angular";
  CodeExampleLanguage["REACT"] = "React";
})(CodeExampleLanguage || (CodeExampleLanguage = {}));
