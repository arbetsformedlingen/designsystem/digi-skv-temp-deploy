export var CodeExampleVariation;
(function (CodeExampleVariation) {
  CodeExampleVariation["LIGHT"] = "light";
  CodeExampleVariation["DARK"] = "dark";
})(CodeExampleVariation || (CodeExampleVariation = {}));
