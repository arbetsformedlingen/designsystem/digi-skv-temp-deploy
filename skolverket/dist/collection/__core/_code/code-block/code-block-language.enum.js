export var CodeBlockLanguage;
(function (CodeBlockLanguage) {
  CodeBlockLanguage["JSON"] = "json";
  CodeBlockLanguage["CSS"] = "css";
  CodeBlockLanguage["SCSS"] = "scss";
  CodeBlockLanguage["TYPESCRIPT"] = "typescript";
  CodeBlockLanguage["JAVASCRIPT"] = "javascript";
  CodeBlockLanguage["BASH"] = "bash";
  CodeBlockLanguage["HTML"] = "html";
  CodeBlockLanguage["GIT"] = "git";
  CodeBlockLanguage["JSX"] = "jsx";
  CodeBlockLanguage["TSX"] = "tsx";
})(CodeBlockLanguage || (CodeBlockLanguage = {}));
