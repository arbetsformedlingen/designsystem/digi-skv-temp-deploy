import { h } from '@stencil/core';
import { CodeBlockVariation } from './code-block-variation.enum';
import { CodeBlockLanguage } from './code-block-language.enum';
import Prism from 'prismjs';
import 'prismjs/components/prism-typescript';
import 'prismjs/components/prism-bash';
import 'prismjs/components/prism-scss';
import 'prismjs/components/prism-json';
import 'prismjs/components/prism-git';
import { ButtonVariation } from '../../_button/button/button-variation.enum';
import { ButtonSize } from '../../_button/button/button-size.enum';
/**
 * @enums CodeBlockLanguage - code-block-language.enum.ts
 * @enums CodeBlockVariation - code-block-variation.enum.ts
 * @swedishName Kodblock
 */
export class CodeBlock {
  constructor() {
    this.highlightedCode = undefined;
    this.afCode = undefined;
    this.afVariation = CodeBlockVariation.DARK;
    this.afLanguage = CodeBlockLanguage.HTML;
    this.afLang = 'en';
    this.afHideToolbar = false;
  }
  componentWillLoad() {
    Prism.manual = true;
    this.formatCode();
  }
  formatCode() {
    this.highlightedCode = Prism.highlight(this.afCode, Prism.languages[this.afLanguage], this.afLanguage);
  }
  copyButtonClickHandler() {
    navigator.clipboard.writeText(this.afCode).then(() => {
      // console.log('hurra!');
    }, (err) => {
      // console.log('åh nej!', err);
      console.error(err);
    });
  }
  get cssModifiers() {
    return {
      'digi-code-block--light': this.afVariation === CodeBlockVariation.LIGHT,
      'digi-code-block--dark': this.afVariation === CodeBlockVariation.DARK
    };
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-code-block': true }, this.cssModifiers) }, h("pre", { class: `digi-code-block__pre language-${this.afLanguage}` }, h("code", { class: "digi-code-block__code", lang: this.afLang, innerHTML: this.highlightedCode })), !this.afHideToolbar && (h("div", { class: "digi-code-block__toolbar" }, h("digi-button", { class: "digi-code-block__button", onAfOnClick: () => this.copyButtonClickHandler(), afVariation: ButtonVariation.FUNCTION, afSize: ButtonSize.SMALL }, h("digi-icon", { class: "digi-code-block__icon", "aria-hidden": "true", afName: `copy`, slot: "icon" }), "Kopiera kod")))));
  }
  static get is() { return "digi-code-block"; }
  static get originalStyleUrls() {
    return {
      "$": ["code-block.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["code-block.css"]
    };
  }
  static get properties() {
    return {
      "afCode": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "A string of code to use in the code block."
            }],
          "text": "En str\u00E4ng med den kod som du vill visa upp."
        },
        "attribute": "af-code",
        "reflect": false
      },
      "afVariation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "CodeBlockVariation",
          "resolved": "CodeBlockVariation.DARK | CodeBlockVariation.LIGHT",
          "references": {
            "CodeBlockVariation": {
              "location": "import",
              "path": "./code-block-variation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set code block variation. This defines the syntax highlighting and can be either light or dark."
            }],
          "text": "S\u00E4tt f\u00E4rgtemat. Kan vara ljust eller m\u00F6rkt."
        },
        "attribute": "af-variation",
        "reflect": false,
        "defaultValue": "CodeBlockVariation.DARK"
      },
      "afLanguage": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "CodeBlockLanguage",
          "resolved": "CodeBlockLanguage.BASH | CodeBlockLanguage.CSS | CodeBlockLanguage.GIT | CodeBlockLanguage.HTML | CodeBlockLanguage.JAVASCRIPT | CodeBlockLanguage.JSON | CodeBlockLanguage.JSX | CodeBlockLanguage.SCSS | CodeBlockLanguage.TSX | CodeBlockLanguage.TYPESCRIPT",
          "references": {
            "CodeBlockLanguage": {
              "location": "import",
              "path": "./code-block-language.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set code language. Defaults to html."
            }],
          "text": "S\u00E4tt programmeringsspr\u00E5k. 'html' \u00E4r f\u00F6rvalt."
        },
        "attribute": "af-language",
        "reflect": false,
        "defaultValue": "CodeBlockLanguage.HTML"
      },
      "afLang": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set language inside of code element. Defaults to 'en' (english)."
            }],
          "text": "S\u00E4tt spr\u00E5k inuti code-elementet. 'en' (engelska) \u00E4r f\u00F6rvalt."
        },
        "attribute": "af-lang",
        "reflect": false,
        "defaultValue": "'en'"
      },
      "afHideToolbar": {
        "type": "boolean",
        "mutable": false,
        "complexType": {
          "original": "boolean",
          "resolved": "boolean",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Remove toolbar for copying code etc"
            }],
          "text": "Ta bort knappar f\u00F6r att till exempel kopiera kod"
        },
        "attribute": "af-hide-toolbar",
        "reflect": false,
        "defaultValue": "false"
      }
    };
  }
  static get states() {
    return {
      "highlightedCode": {}
    };
  }
  static get watchers() {
    return [{
        "propName": "afCode",
        "methodName": "formatCode"
      }];
  }
}
