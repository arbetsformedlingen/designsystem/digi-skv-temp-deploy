export var CodeBlockVariation;
(function (CodeBlockVariation) {
  CodeBlockVariation["LIGHT"] = "light";
  CodeBlockVariation["DARK"] = "dark";
})(CodeBlockVariation || (CodeBlockVariation = {}));
