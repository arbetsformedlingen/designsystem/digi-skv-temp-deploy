import { h } from '@stencil/core';
import { CodeVariation } from './code-variation.enum';
import { CodeLanguage } from './code-language.enum';
import Prism from 'prismjs';
import 'prismjs/components/prism-typescript';
import 'prismjs/components/prism-bash';
import 'prismjs/components/prism-scss';
import 'prismjs/components/prism-json';
import 'prismjs/components/prism-git';
/**
 * @enums CodeLanguage - code-language.enum.ts
 * @enums CodeVariation - code-variation.enum.ts
 * @swedishName Kod
 */
export class Code {
  constructor() {
    this.highlightedCode = undefined;
    this.afCode = undefined;
    this.afVariation = CodeVariation.LIGHT;
    this.afLanguage = CodeLanguage.HTML;
    this.afLang = 'en';
  }
  componentWillLoad() {
    Prism.manual = true;
    this.formatCode();
  }
  formatCode() {
    if (this.afCode && this.afLanguage) {
      this.highlightedCode = Prism.highlight(this.afCode, Prism.languages[this.afLanguage], this.afLanguage);
    }
  }
  get cssModifiers() {
    return {
      'digi-code--light': this.afVariation === CodeVariation.LIGHT,
      'digi-code--dark': this.afVariation === CodeVariation.DARK,
    };
  }
  render() {
    return (h("span", { class: Object.assign({ 'digi-code': true }, this.cssModifiers) }, h("code", { class: `digi-code__code language-${this.afLanguage}`, lang: this.afLang, innerHTML: this.highlightedCode })));
  }
  static get is() { return "digi-code"; }
  static get originalStyleUrls() {
    return {
      "$": ["code.scss"]
    };
  }
  static get styleUrls() {
    return {
      "$": ["code.css"]
    };
  }
  static get properties() {
    return {
      "afCode": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "A string of code to use in the code block."
            }],
          "text": "En str\u00E4ng med den kod som du vill visa upp."
        },
        "attribute": "af-code",
        "reflect": false
      },
      "afVariation": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "CodeVariation",
          "resolved": "CodeVariation.DARK | CodeVariation.LIGHT",
          "references": {
            "CodeVariation": {
              "location": "import",
              "path": "./code-variation.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set code block variation. This defines the syntax highlighting and can be either light or dark."
            }],
          "text": "S\u00E4tt f\u00E4rgtemat. Kan vara ljust eller m\u00F6rkt."
        },
        "attribute": "af-variation",
        "reflect": false,
        "defaultValue": "CodeVariation.LIGHT"
      },
      "afLanguage": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "CodeLanguage",
          "resolved": "CodeLanguage.BASH | CodeLanguage.CSS | CodeLanguage.GIT | CodeLanguage.HTML | CodeLanguage.JAVASCRIPT | CodeLanguage.JSON | CodeLanguage.SCSS | CodeLanguage.TYPESCRIPT",
          "references": {
            "CodeLanguage": {
              "location": "import",
              "path": "./code-language.enum"
            }
          }
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set code language. Defaults to html."
            }],
          "text": "S\u00E4tt programmeringsspr\u00E5k. 'html' \u00E4r f\u00F6rvalt."
        },
        "attribute": "af-language",
        "reflect": false,
        "defaultValue": "CodeLanguage.HTML"
      },
      "afLang": {
        "type": "string",
        "mutable": false,
        "complexType": {
          "original": "string",
          "resolved": "string",
          "references": {}
        },
        "required": false,
        "optional": false,
        "docs": {
          "tags": [{
              "name": "en",
              "text": "Set language inside of code element. Defaults to 'en' (english)."
            }],
          "text": "S\u00E4tt spr\u00E5k inuti code-elementet. 'en' (engelska) \u00E4r f\u00F6rvalt."
        },
        "attribute": "af-lang",
        "reflect": false,
        "defaultValue": "'en'"
      }
    };
  }
  static get states() {
    return {
      "highlightedCode": {}
    };
  }
  static get watchers() {
    return [{
        "propName": "afCode",
        "methodName": "formatCode"
      }];
  }
}
