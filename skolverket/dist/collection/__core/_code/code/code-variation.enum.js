export var CodeVariation;
(function (CodeVariation) {
  CodeVariation["LIGHT"] = "light";
  CodeVariation["DARK"] = "dark";
})(CodeVariation || (CodeVariation = {}));
