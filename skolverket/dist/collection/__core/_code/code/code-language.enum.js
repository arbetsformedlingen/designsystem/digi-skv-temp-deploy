export var CodeLanguage;
(function (CodeLanguage) {
  CodeLanguage["JSON"] = "json";
  CodeLanguage["CSS"] = "css";
  CodeLanguage["SCSS"] = "scss";
  CodeLanguage["TYPESCRIPT"] = "typescript";
  CodeLanguage["JAVASCRIPT"] = "javascript";
  CodeLanguage["BASH"] = "bash";
  CodeLanguage["HTML"] = "html";
  CodeLanguage["GIT"] = "git";
})(CodeLanguage || (CodeLanguage = {}));
