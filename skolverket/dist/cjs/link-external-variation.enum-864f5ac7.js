'use strict';

exports.LinkExternalVariation = void 0;
(function (LinkExternalVariation) {
  LinkExternalVariation["SMALL"] = "small";
  LinkExternalVariation["LARGE"] = "large";
})(exports.LinkExternalVariation || (exports.LinkExternalVariation = {}));
