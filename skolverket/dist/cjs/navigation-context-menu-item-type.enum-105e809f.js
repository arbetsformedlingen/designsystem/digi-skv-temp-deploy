'use strict';

exports.NavigationContextMenuItemType = void 0;
(function (NavigationContextMenuItemType) {
  NavigationContextMenuItemType["LINK"] = "link";
  NavigationContextMenuItemType["BUTTON"] = "button";
})(exports.NavigationContextMenuItemType || (exports.NavigationContextMenuItemType = {}));
