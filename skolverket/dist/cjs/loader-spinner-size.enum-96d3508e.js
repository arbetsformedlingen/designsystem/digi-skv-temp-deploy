'use strict';

exports.LoaderSpinnerSize = void 0;
(function (LoaderSpinnerSize) {
  LoaderSpinnerSize["SMALL"] = "small";
  LoaderSpinnerSize["MEDIUM"] = "medium";
  LoaderSpinnerSize["LARGE"] = "large";
})(exports.LoaderSpinnerSize || (exports.LoaderSpinnerSize = {}));
