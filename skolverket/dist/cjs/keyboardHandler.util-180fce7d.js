'use strict';

exports.KEY_CODE = void 0;
(function (KEY_CODE) {
  KEY_CODE[KEY_CODE["LEFT_ARROW"] = 37] = "LEFT_ARROW";
  KEY_CODE[KEY_CODE["UP_ARROW"] = 38] = "UP_ARROW";
  KEY_CODE[KEY_CODE["RIGHT_ARROW"] = 39] = "RIGHT_ARROW";
  KEY_CODE[KEY_CODE["DOWN_ARROW"] = 40] = "DOWN_ARROW";
  KEY_CODE[KEY_CODE["ENTER"] = 13] = "ENTER";
  KEY_CODE[KEY_CODE["SPACE"] = 32] = "SPACE";
  KEY_CODE[KEY_CODE["ESCAPE"] = 27] = "ESCAPE";
  KEY_CODE[KEY_CODE["TAB"] = 9] = "TAB";
  KEY_CODE[KEY_CODE["SHIFT"] = 16] = "SHIFT";
  KEY_CODE[KEY_CODE["END"] = 35] = "END";
  KEY_CODE[KEY_CODE["HOME"] = 36] = "HOME";
  KEY_CODE["SHIFT_TAB"] = "shift_tab";
  KEY_CODE["ANY"] = "any";
})(exports.KEY_CODE || (exports.KEY_CODE = {}));

function keyboardHandler(event) {
  const key = (event === null || event === void 0 ? void 0 : event.key) || (event === null || event === void 0 ? void 0 : event.keyCode);
  if (event.shiftKey && (key === 'Tab' || key === exports.KEY_CODE.TAB)) {
    return exports.KEY_CODE.SHIFT_TAB;
  }
  switch (key) {
    case 'Down':
    case 'ArrowDown':
    case exports.KEY_CODE.DOWN_ARROW:
      return exports.KEY_CODE.DOWN_ARROW;
    case 'Up':
    case 'ArrowUp':
    case exports.KEY_CODE.UP_ARROW:
      return exports.KEY_CODE.UP_ARROW;
    case 'Left':
    case 'ArrowLeft':
    case exports.KEY_CODE.LEFT_ARROW:
      return exports.KEY_CODE.LEFT_ARROW;
    case 'Right':
    case 'ArrowRight':
    case exports.KEY_CODE.RIGHT_ARROW:
      return exports.KEY_CODE.RIGHT_ARROW;
    case 'Enter':
    case exports.KEY_CODE.ENTER:
      return exports.KEY_CODE.ENTER;
    case 'Esc':
    case 'Escape':
    case exports.KEY_CODE.ESCAPE:
      return exports.KEY_CODE.ESCAPE;
    case 'Tab':
    case exports.KEY_CODE.TAB:
      return exports.KEY_CODE.TAB;
    case ' ':
    case 'Spacebar':
    case exports.KEY_CODE.SPACE:
      return exports.KEY_CODE.SPACE;
    case 'Home':
    case exports.KEY_CODE.HOME:
      return exports.KEY_CODE.HOME;
    case 'End':
    case exports.KEY_CODE.END:
      return exports.KEY_CODE.END;
    default:
      return exports.KEY_CODE.ANY;
  }
}

exports.keyboardHandler = keyboardHandler;
