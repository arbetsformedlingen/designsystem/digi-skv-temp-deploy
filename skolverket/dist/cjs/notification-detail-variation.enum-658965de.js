'use strict';

exports.NotificationDetailVariation = void 0;
(function (NotificationDetailVariation) {
  NotificationDetailVariation["INFO"] = "info";
  NotificationDetailVariation["WARNING"] = "warning";
  NotificationDetailVariation["DANGER"] = "danger";
})(exports.NotificationDetailVariation || (exports.NotificationDetailVariation = {}));
