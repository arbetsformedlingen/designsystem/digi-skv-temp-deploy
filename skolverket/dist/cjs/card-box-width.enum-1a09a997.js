'use strict';

exports.CardBoxWidth = void 0;
(function (CardBoxWidth) {
  CardBoxWidth["REGULAR"] = "regular";
  CardBoxWidth["FULL"] = "full";
})(exports.CardBoxWidth || (exports.CardBoxWidth = {}));
