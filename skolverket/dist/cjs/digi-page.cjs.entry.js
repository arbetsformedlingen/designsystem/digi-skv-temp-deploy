'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');

const pageCss = ".digi-page.sc-digi-page{display:grid;grid-template-rows:min-content 1fr min-content;min-height:100vh;background:var(--digi--page--background-image) center center;grid-template-columns:100vw}";

const Page = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.afBackground = undefined;
  }
  render() {
    return (index.h("div", { class: "digi-page", style: {
        '--digi--page--background-image': this.afBackground
          ? `url('${index.getAssetPath(`./public/images/${this.afBackground}.svg`)}')`
          : null
      } }, index.h("div", { class: "digi-page__header" }, index.h("slot", { name: "header" })), index.h("div", { class: "digi-page__content" }, index.h("slot", null)), index.h("div", { class: "digi-page__footer" }, index.h("slot", { name: "footer" }))));
  }
  static get assetsDirs() { return ["public"]; }
};
Page.style = pageCss;

exports.digi_page = Page;
