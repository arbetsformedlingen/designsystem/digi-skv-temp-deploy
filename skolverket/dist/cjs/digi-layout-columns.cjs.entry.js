'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');
const layoutColumnsVariation_enum = require('./layout-columns-variation.enum-9b7242b1.js');

const layoutColumnsCss = ".sc-digi-layout-columns-h{--digi--layout-columns--gap--column:var(--digi--gutter--largest);--digi--layout-columns--gap--row:var(--digi--gutter--largest);--digi--layout-columns--min-width:0}.sc-digi-layout-columns-h .digi-layout-columns.sc-digi-layout-columns{display:grid;grid-column-gap:var(--digi--layout-columns--gap--column);grid-row-gap:var(--digi--layout-columns--gap--row);grid-template-columns:repeat(auto-fit, minmax(var(--digi--layout-columns--min-width), 1fr))}.sc-digi-layout-columns-h .digi-layout-columns--two.sc-digi-layout-columns{--digi--layout-columns--min-width:calc(\n    50% - var(--digi--layout-columns--gap--column)\n  )}.sc-digi-layout-columns-h .digi-layout-columns--three.sc-digi-layout-columns{--digi--layout-columns--min-width:calc(\n    33% - var(--digi--layout-columns--gap--column)\n  )}";

const LayoutColumns = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.afElement = layoutColumnsVariation_enum.LayoutColumnsElement.DIV;
    this.afVariation = undefined;
  }
  get cssModifiers() {
    return {
      'digi-layout-columns--two': this.afVariation === layoutColumnsVariation_enum.LayoutColumnsVariation.TWO,
      'digi-layout-columns--three': this.afVariation === layoutColumnsVariation_enum.LayoutColumnsVariation.THREE,
    };
  }
  render() {
    return (index.h(this.afElement, { class: Object.assign({ 'digi-layout-columns': true }, this.cssModifiers) }, index.h("slot", null)));
  }
};
LayoutColumns.style = layoutColumnsCss;

exports.digi_layout_columns = LayoutColumns;
