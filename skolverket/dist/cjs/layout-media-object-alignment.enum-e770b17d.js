'use strict';

exports.LayoutMediaObjectAlignment = void 0;
(function (LayoutMediaObjectAlignment) {
  LayoutMediaObjectAlignment["CENTER"] = "center";
  LayoutMediaObjectAlignment["START"] = "start";
  LayoutMediaObjectAlignment["END"] = "end";
  LayoutMediaObjectAlignment["STRETCH"] = "stretch";
})(exports.LayoutMediaObjectAlignment || (exports.LayoutMediaObjectAlignment = {}));
