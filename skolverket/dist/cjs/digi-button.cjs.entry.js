'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');
const randomIdGenerator_util = require('./randomIdGenerator.util-9002c72c.js');
const buttonSize_enum = require('./button-size.enum-a46b7684.js');
const buttonType_enum = require('./button-type.enum-322621ea.js');
const buttonVariation_enum = require('./button-variation.enum-9364587a.js');

const buttonCss = ".sc-digi-button-h{--digi--button--color--background--primary--default:var(--digi--color--background--inverted-1);--digi--button--color--background--primary--hover:var(--digi--color--background--inverted-2);--digi--button--color--background--primary--focus:var(--digi--color--background--inverted-2);--digi--button--color--background--primary--active:var(--digi--color--background--inverted-3);--digi--button--color--background--secondary--default:var(--digi--color--background--primary);--digi--button--color--background--secondary--hover:var(--digi--color--background--neutral-1);--digi--button--color--background--secondary--focus:var(--digi--color--background--secondary);--digi--button--color--background--secondary--active:var(--digi--color--background--tertiary);--digi--button--color--background--function--default:transparent;--digi--button--color--background--function--hover:transparent;--digi--button--color--background--function--focus:transparent;--digi--button--color--background--function--active:transparent;--digi--button--color--text--primary--default:var(--digi--color--text--inverted);--digi--button--color--text--primary--hover:var(--digi--color--text--inverted);--digi--button--color--text--primary--focus:var(--digi--color--text--inverted);--digi--button--color--text--primary--active:var(--digi--color--text--inverted);--digi--button--color--text--secondary--default:var(--digi--color--text--secondary);--digi--button--color--text--secondary--hover:var(--digi--color--text--secondary);--digi--button--color--text--secondary--focus:var(--digi--color--text--secondary);--digi--button--color--text--secondary--active:var(--digi--color--text--secondary);--digi--button--color--text--function--default:var(--digi--color--text--link);--digi--button--color--text--function--hover:var(--digi--color--text--link-hover);--digi--button--color--text--function--focus:var(--digi--color--text--secondary);--digi--button--color--text--function--active:var(--digi--color--text--secondary);--digi--button--color--border--primary--default:var(--digi--color--border--secondary);--digi--button--color--border--primary--hover:var(--digi--color--border--informative);--digi--button--color--border--primary--focus:var(--digi--color--border--tertiary);--digi--button--color--border--primary--active:var(--digi--color--border--quaternary);--digi--button--color--border--secondary--default:var(--digi--color--border--secondary);--digi--button--color--border--secondary--hover:var(--digi--color--border--informative);--digi--button--color--border--secondary--focus:var(--digi--color--border--primary);--digi--button--color--border--secondary--active:var(--digi--color--border--primary);--digi--button--color--border--function--default:initial;--digi--button--color--border--function--hover:initial;--digi--button--color--border--function--focus:initial;--digi--button--color--border--function--active:initial;--digi--button--border-width--primary--default:var(--digi--border-width--button);--digi--button--border-width--primary--hover:var(--digi--border-width--button);--digi--button--border-width--secondary--default:var(--digi--border-width--button);--digi--button--border-width--secondary--hover:var(--digi--border-width--button);--digi--button--border-width--function--default:initial;--digi--button--border-width--function--hover:initial;--digi--button--border-style--primary--default:var(--digi--border-style--primary);--digi--button--border-style--primary--hover:var(--digi--border-style--primary);--digi--button--border-style--primary--focus:var(--digi--border-style--primary);--digi--button--border-style--primary--active:var(--digi--border-style--primary);--digi--button--border-style--secondary--default:var(--digi--border-style--primary);--digi--button--border-style--secondary--hover:var(--digi--border-style--primary);--digi--button--border-style--secondary--focus:var(--digi--border-style--primary);--digi--button--border-style--secondary--active:var(--digi--border-style--primary);--digi--button--border-style--function--default:var(--digi--border-style--secondary);--digi--button--border-style--function--hover:var(--digi--border-style--secondary);--digi--button--border-style--function--focus:var(--digi--border-style--secondary);--digi--button--border-style--function--active:var(--digi--border-style--secondary);--digi--button--border-radius--primary--default:var(--digi--border-radius--button);--digi--button--border-radius--primary--hover:var(--digi--border-radius--button);--digi--button--border-radius--primary--focus:var(--digi--border-radius--button);--digi--button--border-radius--primary--active:var(--digi--border-radius--button);--digi--button--border-radius--secondary--default:var(--digi--border-radius--button);--digi--button--border-radius--secondary--hover:var(--digi--border-radius--button);--digi--button--border-radius--secondary--focus:var(--digi--border-radius--button);--digi--button--border-radius--secondary--active:var(--digi--border-radius--button);--digi--button--border-radius--function--default:initial;--digi--button--border-radius--function--hover:initial;--digi--button--border-radius--function--focus:initial;--digi--button--border-radius--function--active:initial;--digi--button--padding--small:var(--digi--gutter--button-block-small) var(--digi--gutter--button-inline-small);--digi--button--padding--medium:var(--digi--gutter--button-block-medium) var(--digi--gutter--button-inline-medium);--digi--button--padding--large:var(--digi--gutter--button-block-large) var(--digi--gutter--button-inline-large);--digi--button--font-size--small:var(--digi--global--typography--font-size--interaction-small);--digi--button--font-size--medium:var(--digi--global--typography--font-size--interaction-medium);--digi--button--font-size--large:var(--digi--global--typography--font-size--interaction-large);--digi--button--font-family:var(--digi--global--typography--font-family--default);--digi--button--font-weight:var(--digi--typography--button--font-weight--desktop);--digi--button--font-weight--function:var(--digi--typography--label--font-weight--desktop);--digi--button--width:initial;--digi--button--display:initial;--digi--button--justify-content:initial;--digi--button--text-align:initial;--digi--button--outline:initial;--digi--button--outline--focus:var(--digi--color--border--focus) var(--digi--border-style--primary) var(--digi--border-width--secondary);--digi--button--cursor:pointer;--digi--button--icon--spacing:var(--digi--gutter--large);--digi--button--justify-content--full-width:center;--digi--button--text-align--full-width:center;--digi--button--min-height--small:var(--digi--height--button-small);--digi--button--min-height--large:var(--digi--height--button-large)}.sc-digi-button-h .digi-button.sc-digi-button{font-family:var(--digi--button--font-family);background:var(--COLOR--BACKGROUND--DEFAULT);color:var(--COLOR--TEXT--DEFAULT);padding:var(--PADDING);border-radius:var(--BORDER-RADIUS--DEFAULT);border-width:var(--BORDER-WIDTH--DEFAULT);border-style:var(--BORDER-STYLE--DEFAULT);border-color:var(--COLOR--BORDER--DEFAULT);font-weight:var(--FONT-WEIGHT);font-size:var(--FONT-SIZE);outline:var(--digi--button--outline);text-align:var(--digi--button--text-align);width:var(--digi--button--width);display:var(--digi--button--display);justify-content:var(--digi--button--justify-content);cursor:var(--digi--button--cursor);min-height:var(--MIN-HEIGHT)}.sc-digi-button-h .digi-button--variation-primary.sc-digi-button{--COLOR--BACKGROUND--DEFAULT:var(--digi--button--color--background--primary--default);--COLOR--BACKGROUND--HOVER:var(--digi--button--color--background--primary--hover);--COLOR--BACKGROUND--FOCUS:var(--digi--button--color--background--primary--focus);--COLOR--BACKGROUND--ACTIVE:var(--digi--button--color--background--primary--active);--COLOR--TEXT--DEFAULT:var(--digi--button--color--text--primary--default);--COLOR--TEXT--HOVER:var(--digi--button--color--text--primary--hover);--COLOR--TEXT--FOCUS:var(--digi--button--color--text--primary--focus);--COLOR--TEXT--ACTIVE:var(--digi--button--color--text--primary--active);--COLOR--BORDER--DEFAULT:var(--digi--button--color--border--primary--default);--COLOR--BORDER--HOVER:var(--digi--button--color--border--primary--hover);--COLOR--BORDER--FOCUS:var(--digi--button--color--border--primary--focus);--COLOR--BORDER--ACTIVE:var(--digi--button--color--border--primary--active);--BORDER-RADIUS--DEFAULT:var(--digi--button--border-radius--primary--default);--BORDER-RADIUS--HOVER:var(--digi--button--border-radius--primary--hover);--BORDER-RADIUS--FOCUS:var(--digi--button--border-radius--primary--focus);--BORDER-RADIUS--ACTIVE:var(--digi--button--border-radius--primary--active);--BORDER-WIDTH--DEFAULT:var(--digi--button--border-width--primary--default);--BORDER-WIDTH--HOVER:var(--digi--button--border-width--primary--hover);--BORDER-WIDTH--FOCUS:var(--digi--button--border-width--primary--focus);--BORDER-WIDTH--ACTIVE:var(--digi--button--border-width--primary--active);--BORDER-STYLE--DEFAULT:var(--digi--button--border-style--primary--default);--BORDER-STYLE--HOVER:var(--digi--button--border-style--primary--hover);--BORDER-STYLE--FOCUS:var(--digi--button--border-style--primary--focus);--BORDER-STYLE--ACTIVE:var(--digi--button--border-style--primary--active);--FONT-WEIGHT:var(--digi--button--font-weight)}.sc-digi-button-h .digi-button--variation-secondary.sc-digi-button{--COLOR--BACKGROUND--DEFAULT:var(--digi--button--color--background--secondary--default);--COLOR--BACKGROUND--HOVER:var(--digi--button--color--background--secondary--hover);--COLOR--BACKGROUND--FOCUS:var(--digi--button--color--background--secondary--focus);--COLOR--BACKGROUND--ACTIVE:var(--digi--button--color--background--secondary--active);--COLOR--TEXT--DEFAULT:var(--digi--button--color--text--secondary--default);--COLOR--TEXT--HOVER:var(--digi--button--color--text--secondary--hover);--COLOR--TEXT--FOCUS:var(--digi--button--color--text--secondary--focus);--COLOR--TEXT--ACTIVE:var(--digi--button--color--text--secondary--active);--COLOR--BORDER--DEFAULT:var(--digi--button--color--border--secondary--default);--COLOR--BORDER--HOVER:var(--digi--button--color--border--secondary--hover);--COLOR--BORDER--FOCUS:var(--digi--button--color--border--secondary--focus);--COLOR--BORDER--ACTIVE:var(--digi--button--color--border--secondary--active);--BORDER-RADIUS--DEFAULT:var(--digi--button--border-radius--secondary--default);--BORDER-RADIUS--HOVER:var(--digi--button--border-radius--secondary--hover);--BORDER-RADIUS--FOCUS:var(--digi--button--border-radius--secondary--focus);--BORDER-RADIUS--ACTIVE:var(--digi--button--border-radius--secondary--active);--BORDER-WIDTH--DEFAULT:var(--digi--button--border-width--secondary--default);--BORDER-WIDTH--HOVER:var(--digi--button--border-width--secondary--hover);--BORDER-WIDTH--FOCUS:var(--digi--button--border-width--secondary--focus);--BORDER-WIDTH--ACTIVE:var(--digi--button--border-width--secondary--active);--BORDER-STYLE--DEFAULT:var(--digi--button--border-style--secondary--default);--BORDER-STYLE--HOVER:var(--digi--button--border-style--secondary--hover);--BORDER-STYLE--FOCUS:var(--digi--button--border-style--secondary--focus);--BORDER-STYLE--ACTIVE:var(--digi--button--border-style--secondary--active);--FONT-WEIGHT:var(--digi--button--font-weight)}.sc-digi-button-h .digi-button--variation-function.sc-digi-button{--COLOR--BACKGROUND--DEFAULT:var(--digi--button--color--background--function--default);--COLOR--BACKGROUND--HOVER:var(--digi--button--color--background--function--hover);--COLOR--BACKGROUND--FOCUS:var(--digi--button--color--background--function--focus);--COLOR--BACKGROUND--ACTIVE:var(--digi--button--color--background--function--active);--COLOR--TEXT--DEFAULT:var(--digi--button--color--text--function--default);--COLOR--TEXT--HOVER:var(--digi--button--color--text--function--hover);--COLOR--TEXT--FOCUS:var(--digi--button--color--text--function--focus);--COLOR--TEXT--ACTIVE:var(--digi--button--color--text--function--active);--COLOR--BORDER--DEFAULT:var(--digi--button--color--border--function--default);--COLOR--BORDER--HOVER:var(--digi--button--color--border--function--hover);--COLOR--BORDER--FOCUS:var(--digi--button--color--border--function--focus);--COLOR--BORDER--ACTIVE:var(--digi--button--color--border--function--active);--BORDER-RADIUS--DEFAULT:var(--digi--button--border-radius--function--default);--BORDER-RADIUS--HOVER:var(--digi--button--border-radius--function--hover);--BORDER-RADIUS--FOCUS:var(--digi--button--border-radius--function--focus);--BORDER-RADIUS--ACTIVE:var(--digi--button--border-radius--function--active);--BORDER-WIDTH--DEFAULT:var(--digi--button--border-width--function--default);--BORDER-WIDTH--HOVER:var(--digi--button--border-width--function--hover);--BORDER-WIDTH--FOCUS:var(--digi--button--border-width--function--focus);--BORDER-WIDTH--ACTIVE:var(--digi--button--border-width--function--active);--BORDER-STYLE--DEFAULT:var(--digi--button--border-style--function--default);--BORDER-STYLE--HOVER:var(--digi--button--border-style--function--hover);--BORDER-STYLE--FOCUS:var(--digi--button--border-style--function--focus);--BORDER-STYLE--ACTIVE:var(--digi--button--border-style--function--active);--FONT-WEIGHT:var(--digi--button--font-weight)}.sc-digi-button-h .digi-button--variation-function.sc-digi-button:hover,.sc-digi-button-h .digi-button--variation-function.sc-digi-button:focus{text-decoration:underline}.sc-digi-button-h .digi-button--size-small.sc-digi-button{--FONT-SIZE:var(--digi--button--font-size--small);--PADDING:var(--digi--button--padding--small);--MIN-HEIGHT:var(--digi--button--min-height--small)}.sc-digi-button-h .digi-button--size-medium.sc-digi-button{--FONT-SIZE:var(--digi--button--font-size--medium);--PADDING:var(--digi--button--padding--medium);--MIN-HEIGHT:var(--digi--button--min-height--small)}.sc-digi-button-h .digi-button--size-large.sc-digi-button{--FONT-SIZE:var(--digi--button--font-size--large);--PADDING:var(--digi--button--padding--large);--MIN-HEIGHT:var(--digi--button--min-height--large)}.sc-digi-button-h .digi-button .sc-digi-button-s>[slot^=icon]{--digi--icon--height:1em;--digi--icon--width:1em;--digi--icon--color:currentColor}.sc-digi-button-h .digi-button.sc-digi-button:hover{background:var(--COLOR--BACKGROUND--HOVER);border-radius:var(--BORDER-RADIUS--HOVER);border-width:var(--BORDER-WIDTH--HOVER);border-style:var(--BORDER-STYLE--HOVER);border-color:var(--COLOR--BORDER--HOVER);color:var(--COLOR--TEXT--HOVER)}.sc-digi-button-h .digi-button:hover .sc-digi-button-s>[slot^=icon]{--digi--icon--color:var(--COLOR--TEXT--HOVER)}.sc-digi-button-h .digi-button.sc-digi-button:focus-visible{background:var(--COLOR--BACKGROUND--FOCUS);border-style:var(--BORDER-STYLE--FOCUS);border-color:var(--COLOR--BORDER--FOCUS);color:var(--COLOR--TEXT--HOVER);outline:var(--digi--button--outline--focus)}.sc-digi-button-h .digi-button:focus-visible .sc-digi-button-s>[slot^=icon]{--digi--icon--color:var(--COLOR--TEXT--FOCUS)}.sc-digi-button-h .digi-button.sc-digi-button:active{background:var(--COLOR--BACKGROUND--ACTIVE);color:var(--COLOR--TEXT--ACTIVE)}.sc-digi-button-h .digi-button--icon.sc-digi-button,.sc-digi-button-h .digi-button--icon-secondary.sc-digi-button{--digi--button--display:flex;align-items:center;justify-content:var(--digi--button--justify-content);gap:var(--digi--button--icon--spacing)}.sc-digi-button-h .digi-button--full-width.sc-digi-button{--digi--button--width:100%;--digi--button--text-align:var(--digi--button--text-align--full-width);--digi--button--justify-content:var(--digi--button--justify-content--full-width)}";

const Button = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.afOnClick = index.createEvent(this, "afOnClick", 7);
    this.afOnFocus = index.createEvent(this, "afOnFocus", 7);
    this.afOnBlur = index.createEvent(this, "afOnBlur", 7);
    this.hasIcon = undefined;
    this.hasIconSecondary = undefined;
    this.isFullSize = undefined;
    this.afSize = buttonSize_enum.ButtonSize.MEDIUM;
    this.afTabindex = undefined;
    this.afVariation = buttonVariation_enum.ButtonVariation.PRIMARY;
    this.afType = buttonType_enum.ButtonType.BUTTON;
    this.afId = randomIdGenerator_util.randomIdGenerator('digi-button');
    this.afAriaLabel = undefined;
    this.afAriaLabelledby = undefined;
    this.afAriaControls = undefined;
    this.afForm = undefined;
    this.afAriaPressed = undefined;
    this.afAriaExpanded = undefined;
    this.afAriaHaspopup = undefined;
    this.afLang = undefined;
    this.afDir = undefined;
    this.afFullWidth = false;
    this.afAutofocus = undefined;
  }
  fullWidthHandler() {
    this.isFullSize = this.afFullWidth;
  }
  /**
   * Hämta en referens till buttonelementet. Bra för att t.ex. sätta fokus programmatiskt.
   * @en Returns a reference to the button element. Handy for setting focus programmatically.
   */
  async afMGetButtonElement() {
    return this._button;
  }
  componentWillLoad() {
    this.setHasIcon();
    this.fullWidthHandler();
  }
  componentWillUpdate() {
    this.setHasIcon();
  }
  setHasIcon() {
    const icon = !!this.hostElement.querySelector('[slot="icon"]');
    const iconSecondary = !!this.hostElement.querySelector('[slot="icon-secondary"]');
    if (icon) {
      this.hasIcon = icon;
    }
    if (iconSecondary) {
      this.hasIconSecondary = iconSecondary;
    }
  }
  get cssModifiers() {
    return {
      [`digi-button--variation-${this.afVariation}`]: !!this.afVariation,
      [`digi-button--size-${this.afSize}`]: !!this.afSize,
      'digi-button--icon': this.hasIcon,
      'digi-button--icon-secondary': this.hasIconSecondary,
      'digi-button--full-width': this.isFullSize
    };
  }
  get pressedState() {
    if (this.afAriaPressed === null || this.afAriaPressed === undefined) {
      return;
    }
    return this.afAriaPressed ? 'true' : 'false';
  }
  get expandedState() {
    if (this.afAriaExpanded === null || this.afAriaExpanded === undefined) {
      return;
    }
    return this.afAriaExpanded ? 'true' : 'false';
  }
  get hasPopup() {
    if (this.afAriaHaspopup === null || this.afAriaHaspopup === undefined) {
      return;
    }
    return this.afAriaHaspopup ? 'true' : 'false';
  }
  clickHandler(e) {
    this.afOnClick.emit(e);
  }
  focusHandler(e) {
    this.afOnFocus.emit(e);
  }
  blurHandler(e) {
    this.afOnBlur.emit(e);
  }
  render() {
    return (index.h(index.Host, null, index.h("button", { class: Object.assign({ 'digi-button': true }, this.cssModifiers), type: this.afType, ref: (el) => {
        var _a, _b;
        this._button = el;
        // Stencil has a bug wiith form attribute so we need to set it like this
        // https://github.com/ionic-team/stencil/issues/2703
        this.afForm
          ? (_a = this._button) === null || _a === void 0 ? void 0 : _a.setAttribute('form', this.afForm)
          : (_b = this._button) === null || _b === void 0 ? void 0 : _b.removeAttribute('form');
      }, id: this.afId, tabindex: this.afTabindex, "aria-label": this.afAriaLabel, "aria-labelledby": this.afAriaLabelledby, "aria-controls": this.afAriaControls, "aria-pressed": this.pressedState, "aria-expanded": this.expandedState, "aria-haspopup": this.hasPopup, onClick: (e) => this.clickHandler(e), onFocus: (e) => this.focusHandler(e), onBlur: (e) => this.blurHandler(e), autoFocus: this.afAutofocus ? this.afAutofocus : null, form: this.afForm, lang: this.afLang, dir: this.afDir }, this.hasIcon && (index.h("span", { class: "digi-button__icon" }, index.h("slot", { name: "icon" }))), index.h("span", { class: "digi-button__text" }, index.h("slot", null)), this.hasIconSecondary && (index.h("span", { class: "digi-button__icon digi-button__icon--secondary" }, index.h("slot", { name: "icon-secondary" }))))));
  }
  get hostElement() { return index.getElement(this); }
  static get watchers() { return {
    "afFullWidth": ["fullWidthHandler"]
  }; }
};
Button.style = buttonCss;

exports.digi_button = Button;
