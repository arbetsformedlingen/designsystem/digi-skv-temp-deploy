'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');
const layoutStackedBlocksVariation_enum = require('./layout-stacked-blocks-variation.enum-dd570a85.js');

const layoutStackedBlocksCss = ".sc-digi-layout-stacked-blocks-h{--digi--layout-stacked-blocks--columns:1}.sc-digi-layout-stacked-blocks-h[af-variation=enhanced] .sc-digi-layout-stacked-blocks-s>:first-child{grid-column:1/-1}@media (min-width: 48rem){.sc-digi-layout-stacked-blocks-h{--digi--layout-stacked-blocks--columns:2}}@media (min-width: 62rem){.sc-digi-layout-stacked-blocks-h:not([af-variation=enhanced]){--digi--layout-stacked-blocks--columns:3}}.sc-digi-layout-stacked-blocks-h .digi-layout-stacked-blocks.sc-digi-layout-stacked-blocks{display:grid;grid-template-columns:repeat(auto-fit, minmax(calc((100% - var(--digi--responsive-grid-gutter) * (var(--digi--layout-stacked-blocks--columns) - 1)) / var(--digi--layout-stacked-blocks--columns)), 1fr));gap:var(--digi--responsive-grid-gutter)}";

const LayoutStackedBlocks = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.afVariation = layoutStackedBlocksVariation_enum.LayoutStackedBlocksVariation.REGULAR;
  }
  render() {
    return (index.h("div", { class: {
        'digi-layout-stacked-blocks': true,
        [`digi-layout-stacked-blocks--variation-${this.afVariation}`]: !!this.afVariation
      } }, index.h("slot", null)));
  }
};
LayoutStackedBlocks.style = layoutStackedBlocksCss;

const typographyHeadingSectionCss = ".sc-digi-typography-heading-section-h .sc-digi-typography-heading-section-s>*{--digi--heading-3--font-size:var(--digi--typography--heading-3--font-size--mobile);--digi--heading-3--line-height:var(--digi--typography--heading-3--line-height--mobile);font-family:var(--digi--global--typography--font-family--default);font-size:var(--digi--heading-3--font-size);line-height:var(--digi--heading-3--line-height);font-weight:var(--digi--typography--heading-3--font-weight--mobile);-webkit-margin-after:0;margin-block-end:0;font-weight:var(--digi--global--typography--font-weight--regular)}@media (min-width: 48rem){.sc-digi-typography-heading-section-h .sc-digi-typography-heading-section-s>*{--digi--heading-3--font-size:var(--digi--typography--heading-3--font-size--desktop);--digi--heading-3--line-height:var(--digi--typography--heading-3--line-height--desktop)}}@media (min-width: 62rem){.sc-digi-typography-heading-section-h .sc-digi-typography-heading-section-s>*{--digi--heading-3--font-size:var(--digi--typography--heading-3--font-size--desktop-large);--digi--heading-3--line-height:var(--digi--typography--heading-3--line-height--desktop-large)}}.sc-digi-typography-heading-section-h .sc-digi-typography-heading-section-s>*::after{content:\"\";-webkit-margin-before:var(--digi--margin--text-small);margin-block-start:var(--digi--margin--text-small);display:block;width:var(--digi--container-gutter--medium);height:var(--digi--border-width--secondary);background:var(--digi--color--border--neutral-2)}";

const TypographyHeadingSection = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
  }
  render() {
    return (index.h("span", null, index.h("slot", null)));
  }
};
TypographyHeadingSection.style = typographyHeadingSectionCss;

exports.digi_layout_stacked_blocks = LayoutStackedBlocks;
exports.digi_typography_heading_section = TypographyHeadingSection;
