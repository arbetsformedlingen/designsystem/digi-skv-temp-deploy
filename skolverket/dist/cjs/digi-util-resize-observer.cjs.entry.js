'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');

const utilResizeObserverCss = ".sc-digi-util-resize-observer-h{display:block}";

const UtilResizeObserver = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.afOnChange = index.createEvent(this, "afOnChange", 7);
    this._observer = null;
  }
  componentWillLoad() {
    this.initObserver();
  }
  disconnectedCallback() {
    this.removeObserver();
  }
  initObserver() {
    this._observer = new ResizeObserver(([entry]) => {
      this.afOnChange.emit(entry);
    });
    this._observer.observe(this.hostElement);
  }
  removeObserver() {
    this._observer.disconnect();
  }
  changeHandler(e) {
    this.afOnChange.emit(e);
  }
  render() {
    return index.h("slot", null);
  }
  get hostElement() { return index.getElement(this); }
};
UtilResizeObserver.style = utilResizeObserverCss;

exports.digi_util_resize_observer = UtilResizeObserver;
