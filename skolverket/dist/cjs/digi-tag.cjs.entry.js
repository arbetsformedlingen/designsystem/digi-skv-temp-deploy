'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');
const tagSize_enum = require('./tag-size.enum-774d54e2.js');

const tagCss = ".sc-digi-tag-h{--digi--tag--display:inline-block;--digi--tag--margin-bottom:var(--digi--gutter--smaller)}.sc-digi-tag-h .digi-tag.sc-digi-tag{display:var(--digi--tag--display);margin-bottom:var(--digi--tag--margin-bottom)}";

const Tag = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.afOnClick = index.createEvent(this, "afOnClick", 7);
    this.afText = undefined;
    this.afNoIcon = false;
    this.afSize = tagSize_enum.TagSize.SMALL;
  }
  clickHandler(e) {
    this.afOnClick.emit(e);
  }
  render() {
    return (index.h("digi-button", { onAfOnClick: (e) => this.clickHandler(e), "af-variation": "secondary", "af-size": this.afSize, class: "digi-tag" }, this.afText, !this.afNoIcon && index.h("digi-icon", { afName: `x`, slot: "icon-secondary" })));
  }
};
Tag.style = tagCss;

exports.digi_tag = Tag;
