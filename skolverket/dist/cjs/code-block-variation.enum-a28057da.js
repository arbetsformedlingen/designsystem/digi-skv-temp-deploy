'use strict';

exports.CodeBlockVariation = void 0;
(function (CodeBlockVariation) {
  CodeBlockVariation["LIGHT"] = "light";
  CodeBlockVariation["DARK"] = "dark";
})(exports.CodeBlockVariation || (exports.CodeBlockVariation = {}));
