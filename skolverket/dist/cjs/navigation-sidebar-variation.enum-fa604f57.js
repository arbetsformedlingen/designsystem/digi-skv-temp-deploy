'use strict';

exports.NavigationSidebarCloseButtonPosition = void 0;
(function (NavigationSidebarCloseButtonPosition) {
  NavigationSidebarCloseButtonPosition["START"] = "start";
  NavigationSidebarCloseButtonPosition["END"] = "end";
})(exports.NavigationSidebarCloseButtonPosition || (exports.NavigationSidebarCloseButtonPosition = {}));

exports.NavigationSidebarHeadingLevel = void 0;
(function (NavigationSidebarHeadingLevel) {
  NavigationSidebarHeadingLevel["H1"] = "h1";
  NavigationSidebarHeadingLevel["H2"] = "h2";
  NavigationSidebarHeadingLevel["H3"] = "h3";
  NavigationSidebarHeadingLevel["H4"] = "h4";
  NavigationSidebarHeadingLevel["H5"] = "h5";
  NavigationSidebarHeadingLevel["H6"] = "h6";
})(exports.NavigationSidebarHeadingLevel || (exports.NavigationSidebarHeadingLevel = {}));

exports.NavigationSidebarMobilePosition = void 0;
(function (NavigationSidebarMobilePosition) {
  NavigationSidebarMobilePosition["START"] = "start";
  NavigationSidebarMobilePosition["END"] = "end";
})(exports.NavigationSidebarMobilePosition || (exports.NavigationSidebarMobilePosition = {}));

exports.NavigationSidebarMobileVariation = void 0;
(function (NavigationSidebarMobileVariation) {
  NavigationSidebarMobileVariation["DEFAULT"] = "default";
  NavigationSidebarMobileVariation["FULLWIDTH"] = "fullwidth";
})(exports.NavigationSidebarMobileVariation || (exports.NavigationSidebarMobileVariation = {}));

exports.NavigationSidebarPosition = void 0;
(function (NavigationSidebarPosition) {
  NavigationSidebarPosition["START"] = "start";
  NavigationSidebarPosition["END"] = "end";
})(exports.NavigationSidebarPosition || (exports.NavigationSidebarPosition = {}));

exports.NavigationSidebarVariation = void 0;
(function (NavigationSidebarVariation) {
  NavigationSidebarVariation["OVER"] = "over";
  NavigationSidebarVariation["PUSH"] = "push";
  NavigationSidebarVariation["STATIC"] = "static";
})(exports.NavigationSidebarVariation || (exports.NavigationSidebarVariation = {}));
