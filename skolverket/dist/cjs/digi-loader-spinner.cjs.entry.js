'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');
const loaderSpinnerSize_enum = require('./loader-spinner-size.enum-96d3508e.js');

const loaderSpinnerCss = ".sc-digi-loader-spinner-h{--digi--loader-spinner--icon--size--small:1rem;--digi--loader-spinner--icon--size--medium:2rem;--digi--loader-spinner--icon--size--large:5rem;--digi-loader-spinner--font--size--small:var(--digi--typography--heading-4--font-size--desktop-xsmall);--digi-loader-spinner--font--size--medium:var(--digi--typography--heading-4--font-size--desktop);--digi-loader-spinner--font--size--large:var(--digi--typography--heading-4--font-size--desktop-large)}.sc-digi-loader-spinner-h .digi-loader-spinner.sc-digi-loader-spinner{display:flex;flex-direction:column;align-items:center;width:-moz-max-content;width:max-content;padding:var(--PADDING)}.sc-digi-loader-spinner-h .digi-loader-spinner--small.sc-digi-loader-spinner{--ICON-HEIGHT:var(--digi--loader-spinner--icon--size--small);--FONT-SIZE:var(--digi-loader-spinner--font--size--small);--PADDING:0}.sc-digi-loader-spinner-h .digi-loader-spinner--medium.sc-digi-loader-spinner{--ICON-HEIGHT:var(--digi--loader-spinner--icon--size--medium);--FONT-SIZE:var(--digi-loader-spinner--font--size--medium);--PADDING:var(--digi--gutter--large)}.sc-digi-loader-spinner-h .digi-loader-spinner--large.sc-digi-loader-spinner{--ICON-HEIGHT:var(--digi--loader-spinner--icon--size--large);--FONT-SIZE:var(--digi-loader-spinner--font--size--large);--PADDING:var(--digi--gutter--large)}.sc-digi-loader-spinner-h .digi-loader-spinner__container.sc-digi-loader-spinner{height:-moz-max-content;height:max-content;width:-moz-max-content;width:max-content}.sc-digi-loader-spinner-h .digi-loader-spinner__spinner.sc-digi-loader-spinner{animation:animation__rotation 3s infinite linear;transform-origin:center;--digi--icon--height:var(--ICON-HEIGHT)}@media (prefers-reduced-motion){.sc-digi-loader-spinner-h .digi-loader-spinner__spinner.sc-digi-loader-spinner{animation:none}}@keyframes animation__rotation{from{transform:rotate(0deg)}to{transform:rotate(359deg)}}.sc-digi-loader-spinner-h .digi-loader-spinner__label.sc-digi-loader-spinner{margin-top:var(--digi--gutter--medium)}";

const LoaderSpinner = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.afSize = loaderSpinnerSize_enum.LoaderSpinnerSize.MEDIUM;
    this.afText = undefined;
  }
  componentWillLoad() { }
  componentDidLoad() { }
  componentWillUpdate() { }
  get cssModifiers() {
    return {
      'digi-loader-spinner--small': this.afSize == 'small',
      'digi-loader-spinner--medium': this.afSize == 'medium',
      'digi-loader-spinner--large': this.afSize == 'large'
    };
  }
  render() {
    return (index.h(index.Host, null, index.h("div", { "aria-label": this.afText, "aria-live": "assertive", class: Object.assign({ 'digi-loader-spinner': true }, this.cssModifiers) }, index.h("div", { class: "digi-loader-spinner__container", "aria-hidden": "true" }, index.h("digi-icon", { class: "digi-loader-spinner__spinner", afName: `spinner` })), this.afText && (index.h("div", { class: "digi-loader-spinner__label" }, this.afText)))));
  }
  get hostElement() { return index.getElement(this); }
};
LoaderSpinner.style = loaderSpinnerCss;

exports.digi_loader_spinner = LoaderSpinner;
