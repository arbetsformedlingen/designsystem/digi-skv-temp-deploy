'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');

const UtilIntersectionObserver = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.afOnChange = index.createEvent(this, "afOnChange", 7);
    this.afOnIntersect = index.createEvent(this, "afOnIntersect", 7);
    this.afOnUnintersect = index.createEvent(this, "afOnUnintersect", 7);
    this._observer = null;
    this._hasIntersected = false;
    this._isObserving = false;
    this.afOnce = false;
    this.afOptions = {
      rootMargin: '0px',
      threshold: 0
    };
  }
  afOptionsWatcher(newValue) {
    this._afOptions =
      typeof newValue === 'string' ? JSON.parse(newValue) : newValue;
  }
  componentWillLoad() {
    this.afOptionsWatcher(this.afOptions);
    this.initObserver();
  }
  disconnectedCallback() {
    if (this._isObserving) {
      this.removeObserver();
    }
  }
  initObserver() {
    this._observer = new IntersectionObserver(([entry]) => {
      if (entry && entry.isIntersecting) {
        this.intersectionHandler();
        if (this.afOnce) {
          this.removeObserver();
        }
      }
      else if (entry && this._hasIntersected) {
        this.unintersectionHandler();
      }
    }, this._afOptions);
    this._observer.observe(this.$el);
    this._isObserving = true;
  }
  removeObserver() {
    this._observer.disconnect();
    this._isObserving = false;
  }
  intersectionHandler() {
    this.afOnChange.emit(true);
    this.afOnIntersect.emit();
    this._hasIntersected = true;
  }
  unintersectionHandler() {
    this.afOnChange.emit(false);
    this.afOnUnintersect.emit();
    this._hasIntersected = false;
  }
  render() {
    return index.h("slot", null);
  }
  get $el() { return index.getElement(this); }
  static get watchers() { return {
    "afOptions": ["afOptionsWatcher"]
  }; }
};

exports.digi_util_intersection_observer = UtilIntersectionObserver;
