'use strict';

exports.TypographyTimeVariation = void 0;
(function (TypographyTimeVariation) {
  TypographyTimeVariation["PRIMARY"] = "primary";
  TypographyTimeVariation["PRETTY"] = "pretty";
  TypographyTimeVariation["DISTANCE"] = "distance";
})(exports.TypographyTimeVariation || (exports.TypographyTimeVariation = {}));
