'use strict';

exports.FormCheckboxLayout = void 0;
(function (FormCheckboxLayout) {
  FormCheckboxLayout["INLINE"] = "inline";
  FormCheckboxLayout["BLOCK"] = "block";
})(exports.FormCheckboxLayout || (exports.FormCheckboxLayout = {}));

exports.FormCheckboxValidation = void 0;
(function (FormCheckboxValidation) {
  FormCheckboxValidation["ERROR"] = "error";
})(exports.FormCheckboxValidation || (exports.FormCheckboxValidation = {}));

exports.FormCheckboxVariation = void 0;
(function (FormCheckboxVariation) {
  FormCheckboxVariation["PRIMARY"] = "primary";
  FormCheckboxVariation["SECONDARY"] = "secondary";
})(exports.FormCheckboxVariation || (exports.FormCheckboxVariation = {}));
