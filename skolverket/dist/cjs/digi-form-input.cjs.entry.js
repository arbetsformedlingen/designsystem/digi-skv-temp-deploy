'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');
const randomIdGenerator_util = require('./randomIdGenerator.util-9002c72c.js');
const formInputType_enum = require('./form-input-type.enum-7f96d46d.js');
const formInputVariation_enum = require('./form-input-variation.enum-f95a25ea.js');

const formInputCss = ".sc-digi-form-input-h{--digi--form-input--height--small:var(--digi--input-height--small);--digi--form-input--height--medium:var(--digi--input-height--medium);--digi--form-input--height--large:var(--digi--input-height--large);--digi--form-input--border-radius:var(--digi--border-radius--input);--digi--form-input--padding:0 var(--digi--gutter--medium);--digi--form-input--background--empty:var(--digi--color--background--input-empty);--digi--form-input--background--neutral:var(--digi--color--background--input);--digi--form-input--background--success:var(--digi--color--background--success-2);--digi--form-input--background--warning:var(--digi--color--background--warning-2);--digi--form-input--background--error:var(--digi--color--background--danger-2);--digi--form-input--border--neutral:var(--digi--border-width--input-regular) solid;--digi--form-input--border--error:var(--digi--border-width--input-validation) solid;--digi--form-input--border--success:var(--digi--border-width--input-validation) solid;--digi--form-input--border--warning:var(--digi--border-width--input-validation) solid;--digi--form-input--border-color--neutral:var(--digi--color--border--neutral-3);--digi--form-input--border-color--success:var(--digi--color--border--success);--digi--form-input--border-color--warning:var(--digi--color--border--neutral-3);--digi--form-input--border-color--error:var(--digi--color--border--danger)}.sc-digi-form-input-h .digi-form-input.sc-digi-form-input{display:flex;flex-direction:column;gap:0.4em}.sc-digi-form-input-h .digi-form-input--small.sc-digi-form-input{--HEIGHT:var(--digi--form-input--height--small)}.sc-digi-form-input-h .digi-form-input--medium.sc-digi-form-input{--HEIGHT:var(--digi--form-input--height--medium)}.sc-digi-form-input-h .digi-form-input--large.sc-digi-form-input{--HEIGHT:var(--digi--form-input--height--large)}.sc-digi-form-input-h .digi-form-input--neutral.sc-digi-form-input{--BORDER:var(--digi--form-input--border--neutral);--BORDER-COLOR:var(--digi--form-input--border-color--neutral);--BACKGROUND:var(--digi--form-input--background--neutral)}.sc-digi-form-input-h .digi-form-input--empty.sc-digi-form-input:not(:focus-within){--BACKGROUND:var(--digi--form-input--background--empty)}.sc-digi-form-input-h .digi-form-input--success.sc-digi-form-input{--BORDER:var(--digi--form-input--border--success);--BORDER-COLOR:var(--digi--form-input--border-color--success);--BACKGROUND:var(--digi--form-input--background--success)}.sc-digi-form-input-h .digi-form-input--warning.sc-digi-form-input{--BORDER:var(--digi--form-input--border--warning);--BORDER-COLOR:var(--digi--form-input--border-color--warning);--BACKGROUND:var(--digi--form-input--background--warning)}.sc-digi-form-input-h .digi-form-input--error.sc-digi-form-input{--BORDER:var(--digi--form-input--border--error);--BORDER-COLOR:var(--digi--form-input--border-color--error);--BACKGROUND:var(--digi--form-input--background--error)}.digi-form-input__input-wrapper.sc-digi-form-input{display:flex}.digi-form-input__input-wrapper.sc-digi-form-input-s>[slot^=button] button,.digi-form-input__input-wrapper .sc-digi-form-input-s>[slot^=button] button{--MIN-HEIGHT:100%}.digi-form-input--button-variation-primary .digi-form-input__input-wrapper.sc-digi-form-input-s>[slot^=button] button,.digi-form-input--button-variation-primary .digi-form-input__input-wrapper .sc-digi-form-input-s>[slot^=button] button{border-start-start-radius:0;border-end-start-radius:0}.digi-form-input--button-variation-secondary.digi-form-input--has-button-true.sc-digi-form-input .digi-form-input__input-wrapper.sc-digi-form-input{gap:var(--digi--gutter--medium);flex-wrap:wrap}.digi-form-input__input.sc-digi-form-input{flex:1;height:var(--HEIGHT);font-family:var(--digi--global--typography--font-family--default);font-size:var(--digi--typography--body--font-size--desktop);padding:var(--digi--form-input--padding);color:var(--digi--color--text--primary);background:var(--BACKGROUND);border:var(--BORDER);border-color:var(--BORDER-COLOR);border-radius:var(--digi--form-input--border-radius);box-sizing:border-box}.digi-form-input__input.sc-digi-form-input:focus-visible{box-shadow:var(--digi--focus-shadow);outline:var(--digi--focus-outline)}.digi-form-input--button-variation-primary.digi-form-input--has-button-true.sc-digi-form-input .digi-form-input__input.sc-digi-form-input{border-end-end-radius:0;border-start-end-radius:0}";

const FormInput = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.afOnChange = index.createEvent(this, "afOnChange", 7);
    this.afOnBlur = index.createEvent(this, "afOnBlur", 7);
    this.afOnKeyup = index.createEvent(this, "afOnKeyup", 7);
    this.afOnFocus = index.createEvent(this, "afOnFocus", 7);
    this.afOnFocusout = index.createEvent(this, "afOnFocusout", 7);
    this.afOnInput = index.createEvent(this, "afOnInput", 7);
    this.afOnDirty = index.createEvent(this, "afOnDirty", 7);
    this.afOnTouched = index.createEvent(this, "afOnTouched", 7);
    this.hasActiveValidationMessage = false;
    this.hasButton = undefined;
    this.dirty = false;
    this.touched = false;
    this.afLabel = undefined;
    this.afLabelDescription = undefined;
    this.afType = formInputType_enum.FormInputType.TEXT;
    this.afButtonVariation = formInputType_enum.FormInputButtonVariation.PRIMARY;
    this.afAutofocus = undefined;
    this.afVariation = formInputVariation_enum.FormInputVariation.MEDIUM;
    this.afName = undefined;
    this.afId = randomIdGenerator_util.randomIdGenerator('digi-form-input');
    this.afMaxlength = undefined;
    this.afMinlength = undefined;
    this.afRequired = undefined;
    this.afRequiredText = undefined;
    this.afAnnounceIfOptional = false;
    this.afAnnounceIfOptionalText = undefined;
    this.value = undefined;
    this.afValue = undefined;
    this.afValidation = formInputVariation_enum.FormInputValidation.NEUTRAL;
    this.afValidationText = undefined;
    this.afRole = undefined;
    this.afAutocomplete = undefined;
    this.afMin = undefined;
    this.afMax = undefined;
    this.afList = undefined;
    this.afStep = undefined;
    this.afDirname = undefined;
    this.afAriaAutocomplete = undefined;
    this.afAriaActivedescendant = undefined;
    this.afAriaLabelledby = undefined;
    this.afAriaDescribedby = undefined;
  }
  onValueChanged(value) {
    this.afValue = value;
  }
  onAfValueChanged(value) {
    this.value = value;
  }
  afValidationTextWatch() {
    this.setActiveValidationMessage();
  }
  /**
   * Hämtar en referens till inputelementet. Bra för att t.ex. sätta fokus programmatiskt.
   * @en Returns a reference to the input element. Handy for setting focus programmatically.
   */
  async afMGetFormControlElement() {
    return this._input;
  }
  componentWillLoad() {
    this.afValue ? (this.value = this.afValue) : (this.afValue = this.value);
    this.setActiveValidationMessage();
    this.setHasButton();
  }
  componentWillUpdate() {
    this.setHasButton();
  }
  setActiveValidationMessage() {
    this.hasActiveValidationMessage = !!this.afValidationText;
  }
  get cssModifiers() {
    return {
      'digi-form-input--small': this.afVariation === formInputVariation_enum.FormInputVariation.SMALL,
      'digi-form-input--medium': this.afVariation === formInputVariation_enum.FormInputVariation.MEDIUM,
      'digi-form-input--large': this.afVariation === formInputVariation_enum.FormInputVariation.LARGE,
      'digi-form-input--neutral': this.afValidation === formInputVariation_enum.FormInputValidation.NEUTRAL,
      'digi-form-input--success': this.afValidation === formInputVariation_enum.FormInputValidation.SUCCESS,
      'digi-form-input--error': this.afValidation === formInputVariation_enum.FormInputValidation.ERROR,
      'digi-form-input--warning': this.afValidation === formInputVariation_enum.FormInputValidation.WARNING,
      [`digi-form-input--button-variation-${this.afButtonVariation}`]: !!this.afButtonVariation,
      [`digi-form-input--has-button-${this.hasButton}`]: true,
      'digi-form-input--empty': !this.afValue &&
        (!this.afValidation || this.afValidation === formInputVariation_enum.FormInputValidation.NEUTRAL)
    };
  }
  blurHandler(e) {
    if (!this.touched) {
      this.afOnTouched.emit(e);
      this.touched = true;
    }
    this.afOnBlur.emit(e);
  }
  changeHandler(e) {
    this.setActiveValidationMessage();
    this.afOnChange.emit(e);
  }
  focusHandler(e) {
    this.afOnFocus.emit(e);
  }
  focusoutHandler(e) {
    this.afOnFocusout.emit(e);
  }
  keyupHandler(e) {
    this.afOnKeyup.emit(e);
  }
  inputHandler(e) {
    this.afValue = this.value =
      this.afType === formInputType_enum.FormInputType.NUMBER
        ? parseFloat(e.target.value)
        : e.target.value;
    if (!this.dirty) {
      this.afOnDirty.emit(e);
      this.dirty = true;
    }
    this.setActiveValidationMessage();
    this.afOnInput.emit(e);
  }
  setHasButton() {
    this.hasButton = !!this.hostElement.querySelector('[slot="button"]');
  }
  render() {
    return (index.h("div", { class: Object.assign({ 'digi-form-input': true }, this.cssModifiers) }, index.h("digi-form-label", { class: "digi-form-input__label", afFor: this.afId, afLabel: this.afLabel, afDescription: this.afLabelDescription, afRequired: this.afRequired, afAnnounceIfOptional: this.afAnnounceIfOptional, afRequiredText: this.afRequiredText, afAnnounceIfOptionalText: this.afAnnounceIfOptionalText }), index.h("div", { class: "digi-form-input__input-wrapper" }, index.h("input", { class: "digi-form-input__input", ref: (el) => (this._input = el), onBlur: (e) => this.blurHandler(e), onChange: (e) => this.changeHandler(e), onFocus: (e) => this.focusHandler(e), onFocusout: (e) => this.focusoutHandler(e), onKeyUp: (e) => this.keyupHandler(e), onInput: (e) => this.inputHandler(e), "aria-activedescendant": this.afAriaActivedescendant, "aria-describedby": this.afAriaDescribedby, "aria-labelledby": this.afAriaLabelledby, "aria-autocomplete": this.afAriaAutocomplete, "aria-invalid": this.afValidation != formInputVariation_enum.FormInputValidation.NEUTRAL ? 'true' : 'false', autocomplete: this.afAutocomplete, autofocus: this.afAutofocus ? this.afAutofocus : null, maxLength: this.afMaxlength, minLength: this.afMinlength, max: this.afMax, min: this.afMin, step: this.afStep, list: this.afList, role: this.afRole, dirName: this.afDirname, required: this.afRequired, id: this.afId, name: this.afName, type: this.afType, value: this.afValue }), index.h("slot", { name: "button" })), index.h("div", { class: "digi-form-input__footer" }, index.h("div", { class: "digi-form-input__validation", "aria-atomic": "true", role: "alert", id: `${this.afId}--validation-message` }, this.hasActiveValidationMessage &&
      this.afValidation != formInputVariation_enum.FormInputValidation.NEUTRAL && (index.h("digi-form-validation-message", { class: "digi-form-input__validation-message", "af-variation": this.afValidation }, this.afValidationText))))));
  }
  get hostElement() { return index.getElement(this); }
  static get watchers() { return {
    "value": ["onValueChanged"],
    "afValue": ["onAfValueChanged"],
    "afValidationText": ["afValidationTextWatch"]
  }; }
};
FormInput.style = formInputCss;

exports.digi_form_input = FormInput;
