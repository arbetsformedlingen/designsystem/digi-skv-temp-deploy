'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');
const linkInternalVariation_enum = require('./link-internal-variation.enum-354aedf4.js');

const linkInternalCss = ".sc-digi-link-internal-h{--digi--link-internal--gap:var(--digi--gutter--medium);--digi--link-internal--color--default:var(--digi--color--text--link);--digi--link-internal--color--hover:var(--digi--color--text--link-hover);--digi--link-internal--color--visited:var(--digi--color--text--link-visited);--digi--link-internal--font-size--large:var(--digi--typography--link--font-size--desktop-large);--digi--link-internal--font-size--small:var(--digi--typography--link--font-size--desktop);--digi--link-internal--font-family:var(--digi--global--typography--font-family--default);--digi--link-internal--font-weight:var(--digi--typography--link--font-weight--desktop);--digi--link-internal--text-decoration--default:var(--digi--global--typography--text-decoration--default);--digi--link-internal--text-decoration--hover:var(--digi--typography--link--text-decoration--desktop);--digi--link-internal--text-decoration--icon--default:var(--digi--global--typography--text-decoration--default);--digi--link-internal--text-decoration--icon--hover:var(--digi--typography--link--text-decoration--desktop)}.sc-digi-link-internal-h .digi-link-internal.sc-digi-link-internal{--digi--link--gap:var(--digi--link-internal--gap);--digi--link--color--default:var(--digi--link-internal--color--default);--digi--link--color--hover:var(--digi--link-internal--color--hover);--digi--link--color--visited:var(--digi--link-internal--color--visited);--digi--link--font-size--large:var(--digi--link-internal--font-size--large);--digi--link--font-size--small:var(--digi--link-internal--font-size--small);--digi--link--font-family:var(--digi--link-internal--font-family);--digi--link--font-weight:var(--digi--link-internal--font-weight);--digi--link--text-decoration--default:var(--digi--link-internal--text-decoration--default);--digi--link--text-decoration--hover:var(--digi--link-internal--text-decoration--hover);--digi--link--text-decoration--icon--default:var(--digi--link-internal--text-decoration--icon--default);--digi--link--text-decoration--icon--hover:var(--digi--link-internal--text-decoration--icon--hover)}";

const LinkInternal = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.afOnClick = index.createEvent(this, "afOnClick", 7);
    this.afHref = undefined;
    this.afVariation = linkInternalVariation_enum.LinkInternalVariation.SMALL;
    this.afOverrideLink = false;
  }
  clickLinkHandler(e) {
    e.stopImmediatePropagation();
    this.afOnClick.emit(e.detail);
  }
  initRouting() {
    const tabIndex = this.hostElement.getAttribute('tabIndex');
    if (!!tabIndex) {
      setTimeout(async () => {
        const linkElement = await this.hostElement
          .querySelector('digi-link')
          .afMGetLinkElement();
        tabIndex === '0' && linkElement.setAttribute('tabIndex', '-1');
      }, 0);
    }
  }
  componentDidLoad() {
    this.initRouting();
  }
  get cssModifiers() {
    return {
      [`digi-link-internal--variation-${this.afVariation}`]: !!this.afVariation
    };
  }
  render() {
    return (index.h("digi-link", { class: Object.assign({ 'digi-link-internal': true }, this.cssModifiers), afVariation: this.afVariation, afHref: this.afHref, afOverrideLink: this.afOverrideLink, onAfOnClick: (e) => this.clickLinkHandler(e) }, index.h("digi-icon", { class: "digi-link-internal__icon", "aria-hidden": "true", slot: "icon", afName: `chevron-right` }), index.h("slot", null)));
  }
  get hostElement() { return index.getElement(this); }
};
LinkInternal.style = linkInternalCss;

exports.digi_link_internal = LinkInternal;
