'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');
const randomIdGenerator_util = require('./randomIdGenerator.util-9002c72c.js');
const logger_util = require('./logger.util-a78a149e.js');
const formValidationMessageVariation_enum = require('./form-validation-message-variation.enum-9244655f.js');

const formLabelCss = ".sc-digi-form-label-h .digi-form-label.sc-digi-form-label{font-family:var(--digi--global--typography--font-family--default);font-size:var(--digi--typography--label--font-size--desktop);color:var(--digi--color--text--primary);display:flex;flex-direction:column}.sc-digi-form-label-h .digi-form-label__label.sc-digi-form-label{line-height:var(--digi--typography--label--line-height--desktop);font-weight:var(--digi--typography--label--font-weight--desktop);cursor:pointer}.sc-digi-form-label-h .digi-form-label__label-group.sc-digi-form-label{display:inline-flex;align-items:center;gap:var(--digi--gutter--icon)}.sc-digi-form-label-h .digi-form-label__description.sc-digi-form-label{max-width:var(--digi--paragraph-width--medium);line-height:var(--digi--typography--label-description--line-height--desktop);font-size:var(--digi--typography--label-description--font-size--desktop)}";

const FormLabel = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.labelText = undefined;
    this.hasActionSlot = undefined;
    this.afLabel = undefined;
    this.afId = randomIdGenerator_util.randomIdGenerator('digi-form-label');
    this.afFor = undefined;
    this.afDescription = undefined;
    this.afRequired = undefined;
    this.afAnnounceIfOptional = false;
    this.afRequiredText = 'obligatoriskt';
    this.afAnnounceIfOptionalText = 'frivilligt';
  }
  componentWillLoad() {
    this.setLabelText();
    this.validateLabel();
    this.validateFor();
    this.handleSlotVisibility();
  }
  validateLabel() { }
  validateFor() {
    if (this.afFor)
      return;
    logger_util.logger.warn(`digi-form-label must have a for attribute. Please add a for attribute using af-for`, this.hostElement);
  }
  setLabelText() {
    this.labelText = `${this.afLabel} ${this.requiredText}`;
    if (!this.afLabel) {
      logger_util.logger.warn(`digi-form-label must have a label. Please add a label using af-label`, this.hostElement);
    }
  }
  handleSlotVisibility() {
    this.hasActionSlot = !!this.hostElement.querySelector('[slot="actions"]');
  }
  get requiredText() {
    return this.afRequired && !this.afAnnounceIfOptional
      ? ` (${this.afRequiredText})`
      : !this.afRequired && this.afAnnounceIfOptional
        ? ` (${this.afAnnounceIfOptionalText})`
        : '';
  }
  render() {
    return (index.h("div", { class: "digi-form-label" }, index.h("div", { class: "digi-form-label__label-group" }, this.afFor && this.afLabel && (index.h("label", { class: "digi-form-label__label", htmlFor: this.afFor, id: this.afId }, this.labelText)), this.hasActionSlot && (index.h("div", { class: "digi-form-label__actions" }, index.h("slot", { name: "actions" })))), this.afDescription && (index.h("p", { class: "digi-form-label__description" }, this.afDescription))));
  }
  get hostElement() { return index.getElement(this); }
  static get watchers() { return {
    "afLabel": ["validateLabel", "setLabelText"],
    "afFor": ["validateFor"],
    "afRequired": ["setLabelText"],
    "afAnnounceIfOptional": ["setLabelText"]
  }; }
};
FormLabel.style = formLabelCss;

const formValidationMessageCss = ".sc-digi-form-validation-message-h{--digi--form-validation-message--icon-color--success:var(--digi--color--icons--success);--digi--form-validation-message--icon-color--warning:var(--digi--color--icons--warning);--digi--form-validation-message--icon-color--error:var(--digi--color--icons--danger)}.sc-digi-form-validation-message-h .digi-form-validation-message.sc-digi-form-validation-message{display:flex;flex-wrap:wrap;align-items:center}.sc-digi-form-validation-message-h .digi-form-validation-message--success.sc-digi-form-validation-message{--ICON-COLOR:var(--digi--form-validation-message--icon-color--success)}.sc-digi-form-validation-message-h .digi-form-validation-message--warning.sc-digi-form-validation-message{--ICON-COLOR:var(--digi--form-validation-message--icon-color--warning)}.sc-digi-form-validation-message-h .digi-form-validation-message--error.sc-digi-form-validation-message{--ICON-COLOR:var(--digi--form-validation-message--icon-color--error)}.sc-digi-form-validation-message-h .digi-form-validation-message__icon.sc-digi-form-validation-message{color:var(--ICON-COLOR);-webkit-margin-end:var(--digi--gutter--icon);margin-inline-end:var(--digi--gutter--icon)}.sc-digi-form-validation-message-h .digi-form-validation-message__icon.sc-digi-form-validation-message>*.sc-digi-form-validation-message{--digi--icon--width:1.25em;--digi--icon--height:1.25em;--digi--icon--color:var(--ICON-COLOR);display:flex}.sc-digi-form-validation-message-h .digi-form-validation-message__text.sc-digi-form-validation-message{flex:1;font-family:var(--digi--global--typography--font-family--default);font-size:var(--digi--typography--tag--font-size--desktop);color:var(--digi--color--text--primary)}";

const FormValidationMessage = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.afVariation = formValidationMessageVariation_enum.FormValidationMessageVariation.SUCCESS;
  }
  get cssModifiers() {
    return {
      'digi-form-validation-message--success': this.afVariation === formValidationMessageVariation_enum.FormValidationMessageVariation.SUCCESS,
      'digi-form-validation-message--error': this.afVariation === formValidationMessageVariation_enum.FormValidationMessageVariation.ERROR,
      'digi-form-validation-message--warning': this.afVariation === formValidationMessageVariation_enum.FormValidationMessageVariation.WARNING
    };
  }
  get icon() {
    switch (this.afVariation) {
      case formValidationMessageVariation_enum.FormValidationMessageVariation.SUCCESS:
        return 'check-circle';
      case formValidationMessageVariation_enum.FormValidationMessageVariation.ERROR:
        return 'exclamation-circle-filled';
      case formValidationMessageVariation_enum.FormValidationMessageVariation.WARNING:
        return 'exclamation-triangle-warning';
    }
  }
  render() {
    return (index.h("div", { class: Object.assign({ 'digi-form-validation-message': true }, this.cssModifiers) }, index.h("div", { class: "digi-form-validation-message__icon", "aria-hidden": "true" }, index.h("digi-icon", { afName: this.icon })), index.h("span", { class: "digi-form-validation-message__text" }, index.h("slot", null))));
  }
  get hostElement() { return index.getElement(this); }
};
FormValidationMessage.style = formValidationMessageCss;

exports.digi_form_label = FormLabel;
exports.digi_form_validation_message = FormValidationMessage;
