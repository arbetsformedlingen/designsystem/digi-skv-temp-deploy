'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');
const codeExampleVariation_enum = require('./code-example-variation.enum-a954aaf0.js');
const buttonSize_enum = require('./button-size.enum-a46b7684.js');
const buttonVariation_enum = require('./button-variation.enum-9364587a.js');
const codeBlockVariation_enum = require('./code-block-variation.enum-a28057da.js');

const codeExampleCss = ".sc-digi-code-example-h{--digi--code-example--background:var(--digi--color--background--primary);display:block}.digi-code-example.sc-digi-code-example{background-size:20px 20px;background-position:0 0, 0 10px, 10px -10px, -10px 0px;border:var(--digi--border-width--primary) solid var(--digi--color--border--neutral-2);height:100%;display:flex;flex-direction:row;border-radius:0.3rem;position:relative}.digi-code-example--light.sc-digi-code-example{background-color:#ffffff;background-image:linear-gradient(45deg, #efefef 25%, transparent 25%), linear-gradient(-45deg, #efefef 25%, transparent 25%), linear-gradient(45deg, transparent 75%, #efefef 75%), linear-gradient(-45deg, transparent 75%, #efefef 75%)}.digi-code-example--dark.sc-digi-code-example{background-color:#999999;background-image:linear-gradient(45deg, #666666 25%, transparent 25%), linear-gradient(-45deg, #666666 25%, transparent 25%), linear-gradient(45deg, transparent 75%, #666666 75%), linear-gradient(-45deg, transparent 75%, #666666 75%)}.digi-code-example__container.sc-digi-code-example{display:flex;flex-direction:column;justify-content:space-between;flex-grow:1;position:relative}.digi-code-example--no-controls.sc-digi-code-example{flex-direction:column}.digi-code-example--no-controls.sc-digi-code-example .digi-code-example__copy-button.sc-digi-code-example{margin-top:0;align-self:flex-end;margin-bottom:0.2rem;z-index:1;position:absolute;bottom:0;right:0}.digi-code-example--no-controls.sc-digi-code-example .digi-code-example__copy-button--dark.sc-digi-code-example{--digi--button--color--text--function--default:var(--digi--color--text--inverted);--digi--button--color--text--function--hover:var(--digi--color--text--inverted)}.digi-code-example__example.sc-digi-code-example{padding:var(--digi--padding--largest-2);overflow-x:auto}.digi-code-example__copy-button.sc-digi-code-example{align-self:start;margin-top:var(--digi--padding--large);margin-right:var(--digi--padding--small)}.digi-code-example__codeblock.sc-digi-code-example digi-code-block.sc-digi-code-example{--digi--code-block--border-radius:0;--digi--code-block--border:none}.digi-code-example__controls.sc-digi-code-example{padding:var(--digi--padding--medium);background:var(--digi--color--background--neutral-6);display:flex;flex-direction:column;align-self:stretch;flex-shrink:0;justify-content:space-between;gap:var(--digi--gutter--medium)}.digi-code-example__controls[hidden].sc-digi-code-example{display:none}.digi-code-example--inactive.sc-digi-code-example .digi-code-example__code.sc-digi-code-example{display:flex;flex-direction:column;flex-grow:1}.digi-code-example--inactive.sc-digi-code-example .digi-code-example__codeblock.sc-digi-code-example{flex-grow:1;height:64px;overflow-y:hidden;position:relative}.digi-code-example__code-expand.sc-digi-code-example{display:flex;align-items:center;justify-content:center;gap:var(--digi--gutter--icon);width:100%;padding:0.875rem 0;background:#272727;color:var(--digi--color--text--inverted);font-size:var(--digi--typography--body--font-size--desktop);font-family:var(--digi--global--typography--font-family--default);cursor:pointer;border:none}.digi-code-example__code-expand.sc-digi-code-example:hover{background:var(--digi--color--background--inverted-3);text-decoration:underline}.digi-code-example--no-controls.digi-code-example--inactive.sc-digi-code-example .digi-code-example__codeblock.sc-digi-code-example{height:64px}.digi-code-example__copy-button.sc-digi-code-example{--digi--button--padding--small:0;--digi--button--padding--medium:0;--digi--button--padding--large:0}.digi-code-example__code-expand-icon.sc-digi-code-example{--digi--icon--color:var(--digi--color--text--inverted)}.digi-code-example__fade.sc-digi-code-example{position:absolute;width:100%;bottom:0;height:var(--digi--padding--largest-2)}.digi-code-example__fade--light.sc-digi-code-example{background-image:linear-gradient(0deg, white, transparent)}.digi-code-example__fade--dark.sc-digi-code-example{--digi--color--background--inverted-3:#272727;background-image:linear-gradient(0deg, var(--digi--color--background--inverted-3), transparent)}.slot__controls.sc-digi-code-example{margin-bottom:var(--digi--padding--smaller)}.sc-digi-code-example-s .slot__controls{display:flex;flex-direction:column;gap:var(--digi--padding--smaller)}.sc-digi-code-example-s .slot__controls[hidden]{display:none}.sc-digi-code-example-s .slot__controls digi-form-fieldset,.sc-digi-code-example-s .slot__controls digi-form-select,.sc-digi-code-example-s .slot__controls digi-form-fieldset+digi-form-checkbox{display:block;border-bottom:1px solid var(--digi--color--border--neutral-2);padding-bottom:var(--digi--padding--smaller);margin-bottom:var(--digi--padding--medium)}";

const CodeExample = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.isActive = true;
    this.codeLanguage = undefined;
    this.hasControls = undefined;
    this.showMoreButton = undefined;
    this.containerElement = undefined;
    this.afCode = undefined;
    this.afCodeBlockVariation = codeBlockVariation_enum.CodeBlockVariation.DARK;
    this.afExampleVariation = codeExampleVariation_enum.CodeExampleVariation.LIGHT;
    this.afLanguage = codeExampleVariation_enum.CodeExampleLanguage.HTML;
    this.afHideControls = undefined;
    this.afHideCode = undefined;
  }
  /**
   * Försöker parsa afCode som ett objekt.
   * Returnerar antingen codeExample som sträng - antingen direkt om afCode var en sträng, annars det valda kodspråket.
   */
  get codeExample() {
    let codeString;
    if (typeof this._codeExample === 'object') {
      this.codeLanguage ||
        (this.codeLanguage = this.codeExampleLanguages[0]);
      codeString = this._codeExample[this.codeLanguage];
    }
    else
      codeString = this._codeExample;
    return codeString;
  }
  set codeExample(code) {
    try {
      this._codeExample = JSON.parse(code);
    }
    catch (e) {
      // code är antagligen en sträng och inte objekt.
      this._codeExample = code;
    }
  }
  /**
   * Returnerar tomt om inget kodspråk har skickats med i afCode.
   * Dock returnerar en lista av kodspråk (t.ex. ts eller js) om detta är angett.
   */
  get codeExampleLanguages() {
    return (typeof this._codeExample === 'object' && Object.keys(this._codeExample));
  }
  /**
   * Spara kodexemplet från afCode vid uppdatering av attributet.
   * @en Save the code example from afCode on update of the attribute.
   */
  codeExampleUpdate() {
    this.codeExample = this.afCode;
  }
  componentWillLoad() {
    this.codeExampleUpdate();
    this.hasControls =
      !!this.hostElement.querySelector('[slot="controls"]') ||
        !!this.codeExampleLanguages;
  }
  componentWillUpdate() {
    this.hasControls =
      !!this.hostElement.querySelector('[slot="controls"]') ||
        !!this.codeExampleLanguages;
  }
  componentDidLoad() {
    this.updateShowMoreButton();
  }
  toggleButtonClickHandler() {
    this.isActive = !this.isActive;
  }
  copyButtonClickHandler() {
    navigator.clipboard.writeText(this.codeExample).then(() => {
      // console.log('hurra!');
    }, (err) => {
      // console.log('åh nej!', err);
      console.error(err);
    });
  }
  /**
   * Ändrar kodspråk vid val av radioknapp
   * @param e Event object of the change event.
   */
  codeLanguageChangeHandler(e) {
    this.codeLanguage = e.target.value;
    this.codeExampleUpdate();
  }
  /**
   * Visar "Visa mer" när kodblocket är högre än 180px.
   * @en Show "Visa mer" when code block is higher than 180px.
   */
  updateShowMoreButton() {
    const codeblock = this.containerElement.querySelector('.digi-code-example__codeblock');
    const controls = this.containerElement.querySelector('.digi-code-example__controls > span');
    const example = this.containerElement.querySelector('.digi-code-example__example');
    if (!this._codeHeightObserver && codeblock) {
      this._codeHeightObserver = new ResizeObserver((observable) => {
        const initSetActive = this.showMoreButton == undefined;
        this.showMoreButton =
          observable[0].target.scrollHeight >=
            (!!controls ? controls.scrollHeight - example.scrollHeight + 35 : 64);
        if (initSetActive) {
          this.isActive = this.showMoreButton ? false : true;
        }
      });
      this._codeHeightObserver.observe(codeblock);
    }
  }
  get cssModifiers() {
    return {
      'digi-code-example--active': this.isActive,
      'digi-code-example--inactive': !this.isActive,
      [`digi-code-example--hide-controls-${!!this.afHideControls}`]: true,
      'digi-code-example--light': this.afExampleVariation === codeExampleVariation_enum.CodeExampleVariation.LIGHT,
      'digi-code-example--dark': this.afExampleVariation === codeExampleVariation_enum.CodeExampleVariation.DARK,
      'digi-code-example--no-controls': !this.hasControls && !this.codeExampleLanguages
    };
  }
  capitalize(word) {
    return word
      .toLowerCase()
      .replace(/\w/, (firstLetter) => firstLetter.toUpperCase());
  }
  render() {
    return (index.h("div", { class: Object.assign({ 'digi-code-example': true }, this.cssModifiers), ref: (el) => (this.containerElement = el) }, index.h("div", { class: {
        'digi-code-example__container': true
      } }, index.h("div", { class: "digi-code-example__example" }, index.h("slot", null)), !this.afHideCode && (index.h("div", { class: "digi-code-example__code" }, this.showMoreButton && (index.h("button", { class: "digi-code-example__code-expand", "aria-label": this.isActive ? `Visa mindre kod ` : `Visa mer kod `, "aria-pressed": this.isActive, onClick: () => this.toggleButtonClickHandler() }, this.isActive ? `Visa mindre kod ` : `Visa mer kod `, index.h("digi-icon", { class: {
        'digi-code-example__code-expand-icon': true,
        'digi-code-example__code-expand-icon--open': this.isActive
      }, "aria-hidden": "true", afName: this.isActive ? `chevron-up` : `chevron-down` }))), index.h("div", { class: {
        'digi-code-example__codeblock': true,
        'digi-code-example__codeblock--light': this.afCodeBlockVariation === 'light',
        'digi-code-example__codeblock--dark': this.afCodeBlockVariation === 'dark',
        'digi-code-example__codeblock--fade': !this.isActive && this.showMoreButton
      } }, index.h("digi-code-block", { afCode: this.codeExample || '', afLanguage: this.afLanguage.toLowerCase(), afVariation: this.afCodeBlockVariation, "af-hide-toolbar": true }), !this.isActive ? (index.h("div", { class: {
        'digi-code-example__fade': true,
        'digi-code-example__fade--dark': this.afCodeBlockVariation === 'dark',
        'digi-code-example__fade--light': this.afCodeBlockVariation === 'light'
      }, onClick: () => this.toggleButtonClickHandler() })) : (''))))), this.hasControls || !!this.codeExampleLanguages ? (index.h("div", { class: {
        'digi-code-example__controls': true
      }, hidden: !!this.afHideControls }, index.h("span", null, index.h("slot", { name: "controls" }), index.h("div", { class: "slot__controls" }, this.codeExampleLanguages && (index.h("digi-form-fieldset", { "af-legend": "Kodspr\u00E5k", "af-name": "Code Language", onChange: (e) => this.codeLanguageChangeHandler(e) }, this.codeExampleLanguages.map((codeLang) => (index.h("digi-form-radiobutton", { afName: "Code Language", afChecked: this.codeLanguage === codeLang, afValue: codeLang, afLabel: codeLang }))))))), index.h("digi-button", { class: "digi-code-example__copy-button", onAfOnClick: () => this.copyButtonClickHandler(), "af-variation": buttonVariation_enum.ButtonVariation.FUNCTION, "af-size": buttonSize_enum.ButtonSize.MEDIUM }, index.h("digi-icon", { class: "digi-code-example__icon", "aria-hidden": "true", slot: "icon", afName: `copy` }), "Kopiera kod"))) : (!this.afHideControls && (index.h("digi-button", { class: {
        'digi-code-example__copy-button': true,
        'digi-code-example__copy-button--dark': this.afCodeBlockVariation === 'dark'
      }, onAfOnClick: () => this.copyButtonClickHandler(), "af-variation": buttonVariation_enum.ButtonVariation.FUNCTION, "af-size": buttonSize_enum.ButtonSize.MEDIUM }, index.h("digi-icon", { class: "digi-code-example__icon", "aria-hidden": "true", slot: "icon", afName: `copy` }), "Kopiera kod")))));
  }
  get hostElement() { return index.getElement(this); }
  static get watchers() { return {
    "afCode": ["codeExampleUpdate"]
  }; }
};
CodeExample.style = codeExampleCss;

exports.digi_code_example = CodeExample;
