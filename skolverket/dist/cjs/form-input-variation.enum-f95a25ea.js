'use strict';

exports.FormInputValidation = void 0;
(function (FormInputValidation) {
  FormInputValidation["SUCCESS"] = "success";
  FormInputValidation["ERROR"] = "error";
  FormInputValidation["WARNING"] = "warning";
  FormInputValidation["NEUTRAL"] = "neutral";
})(exports.FormInputValidation || (exports.FormInputValidation = {}));

exports.FormInputVariation = void 0;
(function (FormInputVariation) {
  FormInputVariation["SMALL"] = "small";
  FormInputVariation["MEDIUM"] = "medium";
  FormInputVariation["LARGE"] = "large";
})(exports.FormInputVariation || (exports.FormInputVariation = {}));
