'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');

const typographyPreambleCss = ".sc-digi-typography-preamble-h .digi-typography-preamble.sc-digi-typography-preamble{font-family:var(--digi--global--typography--font-family--default);font-size:var(--digi--typography--preamble--font-size--mobile);font-weight:var(--digi--typography--preamble--font-weight--desktop);line-height:var(--digi--typography--preamble--line-height--mobile);color:var(--digi--color--text--primary);max-width:var(--digi--paragraph-width--medium)}@media (min-width: 62rem){.sc-digi-typography-preamble-h .digi-typography-preamble.sc-digi-typography-preamble{font-size:var(--digi--typography--preamble--font-size--desktop);line-height:var(--digi--typography--preamble--line-height--desktop)}}";

const TypographyPreamble = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
  }
  render() {
    return (index.h("p", { class: "digi-typography-preamble" }, index.h("slot", null)));
  }
};
TypographyPreamble.style = typographyPreambleCss;

exports.digi_typography_preamble = TypographyPreamble;
