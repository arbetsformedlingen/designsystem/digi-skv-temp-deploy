'use strict';

exports.DialogHeadingLevel = void 0;
(function (DialogHeadingLevel) {
  DialogHeadingLevel["H1"] = "h1";
  DialogHeadingLevel["H2"] = "h2";
  DialogHeadingLevel["H3"] = "h3";
  DialogHeadingLevel["H4"] = "h4";
  DialogHeadingLevel["H5"] = "h5";
  DialogHeadingLevel["H6"] = "h6";
})(exports.DialogHeadingLevel || (exports.DialogHeadingLevel = {}));

exports.DialogSize = void 0;
(function (DialogSize) {
  DialogSize["SMALL"] = "small";
  DialogSize["MEDIUM"] = "medium";
  DialogSize["LARGE"] = "large";
})(exports.DialogSize || (exports.DialogSize = {}));

exports.DialogVariation = void 0;
(function (DialogVariation) {
  DialogVariation["PRIMARY"] = "primary";
  DialogVariation["SECONDARY"] = "secondary";
})(exports.DialogVariation || (exports.DialogVariation = {}));

exports.NavigationMainMenuActiveIndicatorSize = void 0;
(function (NavigationMainMenuActiveIndicatorSize) {
  NavigationMainMenuActiveIndicatorSize["PRIMARY"] = "primary";
  NavigationMainMenuActiveIndicatorSize["SECONDARY"] = "secondary";
})(exports.NavigationMainMenuActiveIndicatorSize || (exports.NavigationMainMenuActiveIndicatorSize = {}));

exports.NavigationMainMenuVariation = void 0;
(function (NavigationMainMenuVariation) {
  NavigationMainMenuVariation["PRIMARY"] = "primary";
  NavigationMainMenuVariation["SECONDARY"] = "secondary";
})(exports.NavigationMainMenuVariation || (exports.NavigationMainMenuVariation = {}));

exports.NotificationAlertVariation = void 0;
(function (NotificationAlertVariation) {
  NotificationAlertVariation["INFO"] = "info";
  NotificationAlertVariation["WARNING"] = "warning";
  NotificationAlertVariation["DANGER"] = "danger";
})(exports.NotificationAlertVariation || (exports.NotificationAlertVariation = {}));

exports.PageBlockCardsVariation = void 0;
(function (PageBlockCardsVariation) {
  PageBlockCardsVariation["START"] = "start";
  PageBlockCardsVariation["SUB"] = "sub";
  PageBlockCardsVariation["SECTION"] = "section";
  PageBlockCardsVariation["PROCESS"] = "process";
  PageBlockCardsVariation["ARTICLE"] = "article";
})(exports.PageBlockCardsVariation || (exports.PageBlockCardsVariation = {}));

exports.PageBlockHeroBackground = void 0;
(function (PageBlockHeroBackground) {
  PageBlockHeroBackground["AUTO"] = "auto";
  PageBlockHeroBackground["TRANSPARENT"] = "transparent";
})(exports.PageBlockHeroBackground || (exports.PageBlockHeroBackground = {}));

exports.PageBlockHeroVariation = void 0;
(function (PageBlockHeroVariation) {
  PageBlockHeroVariation["START"] = "start";
  PageBlockHeroVariation["SUB"] = "sub";
  PageBlockHeroVariation["SECTION"] = "section";
  PageBlockHeroVariation["PROCESS"] = "process";
})(exports.PageBlockHeroVariation || (exports.PageBlockHeroVariation = {}));

exports.PageBlockListsVariation = void 0;
(function (PageBlockListsVariation) {
  PageBlockListsVariation["START"] = "start";
  PageBlockListsVariation["SUB"] = "sub";
  PageBlockListsVariation["SECTION"] = "section";
  PageBlockListsVariation["PROCESS"] = "process";
  PageBlockListsVariation["ARTICLE"] = "article";
})(exports.PageBlockListsVariation || (exports.PageBlockListsVariation = {}));

exports.PageBlockSidebarVariation = void 0;
(function (PageBlockSidebarVariation) {
  PageBlockSidebarVariation["START"] = "start";
  PageBlockSidebarVariation["SUB"] = "sub";
  PageBlockSidebarVariation["SECTION"] = "section";
  PageBlockSidebarVariation["PROCESS"] = "process";
  PageBlockSidebarVariation["ARTICLE"] = "article";
})(exports.PageBlockSidebarVariation || (exports.PageBlockSidebarVariation = {}));

exports.PageBackground = void 0;
(function (PageBackground) {
  PageBackground["STRIPED_1"] = "striped_1";
  PageBackground["STRIPED_2"] = "striped_2";
  PageBackground["STRIPED_3"] = "striped_3";
  PageBackground["SQUARE_1"] = "square_1";
  PageBackground["SQUARE_2"] = "square_2";
  PageBackground["SQUARE_3"] = "square_3";
  PageBackground["DOTTED_1"] = "dotted_1";
  PageBackground["DOTTED_2"] = "dotted_2";
  PageBackground["DOTTED_3"] = "dotted_3";
})(exports.PageBackground || (exports.PageBackground = {}));
