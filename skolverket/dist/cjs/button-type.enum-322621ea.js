'use strict';

exports.ButtonType = void 0;
(function (ButtonType) {
  ButtonType["BUTTON"] = "button";
  ButtonType["SUBMIT"] = "submit";
  ButtonType["RESET"] = "reset";
})(exports.ButtonType || (exports.ButtonType = {}));
