'use strict';

exports.FormSelectValidation = void 0;
(function (FormSelectValidation) {
  FormSelectValidation["SUCCESS"] = "success";
  FormSelectValidation["ERROR"] = "error";
  FormSelectValidation["WARNING"] = "warning";
  FormSelectValidation["NEUTRAL"] = "neutral";
})(exports.FormSelectValidation || (exports.FormSelectValidation = {}));

exports.FormSelectVariation = void 0;
(function (FormSelectVariation) {
  FormSelectVariation["SMALL"] = "small";
  FormSelectVariation["MEDIUM"] = "medium";
  FormSelectVariation["LARGE"] = "large";
})(exports.FormSelectVariation || (exports.FormSelectVariation = {}));
