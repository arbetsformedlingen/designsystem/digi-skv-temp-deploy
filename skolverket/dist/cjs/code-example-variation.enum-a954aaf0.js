'use strict';

exports.CodeExampleLanguage = void 0;
(function (CodeExampleLanguage) {
  CodeExampleLanguage["JSON"] = "JSON";
  CodeExampleLanguage["CSS"] = "CSS";
  CodeExampleLanguage["SCSS"] = "SCSS";
  CodeExampleLanguage["TYPESCRIPT"] = "Typescript";
  CodeExampleLanguage["JAVASCRIPT"] = "Javascript";
  CodeExampleLanguage["BASH"] = "Bash";
  CodeExampleLanguage["HTML"] = "HTML";
  CodeExampleLanguage["GIT"] = "Git";
  CodeExampleLanguage["ANGULAR"] = "Angular";
  CodeExampleLanguage["REACT"] = "React";
})(exports.CodeExampleLanguage || (exports.CodeExampleLanguage = {}));

exports.CodeExampleVariation = void 0;
(function (CodeExampleVariation) {
  CodeExampleVariation["LIGHT"] = "light";
  CodeExampleVariation["DARK"] = "dark";
})(exports.CodeExampleVariation || (exports.CodeExampleVariation = {}));
