'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');

const layoutRowsCss = ".sc-digi-layout-rows-h{display:grid;grid-gap:var(--digi--responsive-grid-gutter);grid-auto-flow:row;width:100%}";

const LayoutRows = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
  }
  render() {
    return index.h("slot", null);
  }
};
LayoutRows.style = layoutRowsCss;

exports.digi_layout_rows = LayoutRows;
