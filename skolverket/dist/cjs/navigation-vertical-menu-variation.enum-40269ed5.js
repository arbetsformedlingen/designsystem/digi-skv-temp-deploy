'use strict';

exports.NavigationVerticalMenuActiveIndicatorSize = void 0;
(function (NavigationVerticalMenuActiveIndicatorSize) {
  NavigationVerticalMenuActiveIndicatorSize["PRIMARY"] = "primary";
  NavigationVerticalMenuActiveIndicatorSize["SECONDARY"] = "secondary";
})(exports.NavigationVerticalMenuActiveIndicatorSize || (exports.NavigationVerticalMenuActiveIndicatorSize = {}));

exports.NavigationVerticalMenuVariation = void 0;
(function (NavigationVerticalMenuVariation) {
  NavigationVerticalMenuVariation["PRIMARY"] = "primary";
  NavigationVerticalMenuVariation["SECONDARY"] = "secondary";
})(exports.NavigationVerticalMenuVariation || (exports.NavigationVerticalMenuVariation = {}));
