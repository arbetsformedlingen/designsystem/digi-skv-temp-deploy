'use strict';

exports.FormValidationMessageVariation = void 0;
(function (FormValidationMessageVariation) {
  FormValidationMessageVariation["SUCCESS"] = "success";
  FormValidationMessageVariation["ERROR"] = "error";
  FormValidationMessageVariation["WARNING"] = "warning";
})(exports.FormValidationMessageVariation || (exports.FormValidationMessageVariation = {}));
