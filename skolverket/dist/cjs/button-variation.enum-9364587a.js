'use strict';

exports.ButtonVariation = void 0;
(function (ButtonVariation) {
  ButtonVariation["PRIMARY"] = "primary";
  ButtonVariation["SECONDARY"] = "secondary";
  ButtonVariation["FUNCTION"] = "function";
})(exports.ButtonVariation || (exports.ButtonVariation = {}));
