'use strict';

exports.ProgressStepsHeadingLevel = void 0;
(function (ProgressStepsHeadingLevel) {
  ProgressStepsHeadingLevel["H1"] = "h1";
  ProgressStepsHeadingLevel["H2"] = "h2";
  ProgressStepsHeadingLevel["H3"] = "h3";
  ProgressStepsHeadingLevel["H4"] = "h4";
  ProgressStepsHeadingLevel["H5"] = "h5";
  ProgressStepsHeadingLevel["H6"] = "h6";
})(exports.ProgressStepsHeadingLevel || (exports.ProgressStepsHeadingLevel = {}));

exports.ProgressStepsStatus = void 0;
(function (ProgressStepsStatus) {
  ProgressStepsStatus["CURRENT"] = "current";
  ProgressStepsStatus["UPCOMING"] = "upcoming";
  ProgressStepsStatus["DONE"] = "done";
})(exports.ProgressStepsStatus || (exports.ProgressStepsStatus = {}));

exports.ProgressStepsVariation = void 0;
(function (ProgressStepsVariation) {
  ProgressStepsVariation["PRIMARY"] = "primary";
  ProgressStepsVariation["SECONDARY"] = "secondary";
})(exports.ProgressStepsVariation || (exports.ProgressStepsVariation = {}));
