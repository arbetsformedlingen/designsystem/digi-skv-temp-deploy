'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');
const randomIdGenerator_util = require('./randomIdGenerator.util-9002c72c.js');
const navigationSidebarVariation_enum = require('./navigation-sidebar-variation.enum-fa604f57.js');
const utilBreakpointObserverBreakpoints_enum = require('./util-breakpoint-observer-breakpoints.enum-9f89c8ce.js');

const navigationSidebarCss = ".sc-digi-navigation-sidebar-h{--digi--navigation-sidebar--wrapper--position:fixed;--digi--navigation-sidebar--wrapper--background-color:var(--digi--color--background--primary);--digi--navigation-sidebar--wrapper--transition:all var(--digi--animation--duration--base);--digi--navigation-sidebar--wrapper--padding:0;--digi--navigation-sidebar--wrapper--box-shadow:0 0.125rem 0.375rem 0 rgba(0, 0, 0, 0.7);--digi--navigation-sidebar--wrapper--box-shadow--backdrop:0 0 3.125rem rgba(0, 0, 0, 0.35);--digi--navigation-sidebar--wrapper--border:solid var(--digi--border-width--primary) var(--digi--color--border--primary);--digi--navigation-sidebar--wrapper--width:21.875rem;--digi--navigation-sidebar--wrapper--width--small--fullwidth:100%;--digi--navigation-sidebar--wrapper--width--small:90vw;--digi--navigation-sidebar--wrapper--margin:0 0 0 -21.875rem;--digi--navigation-sidebar--wrapper--z-index:1999;--digi--navigation-sidebar--header--padding:var(--digi--padding--medium);--digi--navigation-sidebar--header--border:solid var(--digi--border-width--primary) var(--digi--color--border--neutral-4);--digi--navigation-sidebar--heading--font-family:var(--digi--global--typography--font-family--default);--digi--navigation-sidebar--heading--font-size:var(--digi--typography--heading-3--font-size--desktop);--digi--navigation-sidebar--heading--font-size--desktop:var(--digi--typography--heading-3--font-size--desktop-large);--digi--navigation-sidebar--heading--font-weight:var(--digi--typography--heading-3--font-weight--desktop);--digi--navigation-sidebar--heading--color:var(--digi--color--text--primary);--digi--navigation-sidebar--backdrop--transition:all var(--digi--animation--duration--base);--digi--navigation-sidebar--backdrop--background:rgba(0, 0, 0, 0.7);--digi--navigation-sidebar--backdrop--z-index:1998;--digi--navigation-sidebar--close-button--padding:0;position:relative;display:block}.sc-digi-navigation-sidebar-h .digi-navigation-sidebar.sc-digi-navigation-sidebar{height:100%}.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__wrapper.sc-digi-navigation-sidebar{top:0;bottom:0;z-index:var(--digi--navigation-sidebar--wrapper--z-index);visibility:hidden;opacity:0;height:100%;background-color:var(--digi--navigation-sidebar--wrapper--background-color);padding:var(--digi--navigation-sidebar--wrapper--padding);box-sizing:border-box;width:var(--digi--navigation-sidebar--wrapper--width);margin:var(--digi--navigation-sidebar--wrapper--margin)}@media (prefers-reduced-motion){.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__wrapper.sc-digi-navigation-sidebar{--digi--navigation-sidebar--wrapper--transition:$animation__duration--reduced}}@media screen and (max-width: 47.9375rem){.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__wrapper.sc-digi-navigation-sidebar{transition:var(--digi--navigation-sidebar--wrapper--transition)}.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__wrapper.digi-navigation-sidebar__wrapper--mobile--default.sc-digi-navigation-sidebar{--digi--navigation-sidebar--wrapper--width:var(--digi--navigation-sidebar--wrapper--width--small)}.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__wrapper.digi-navigation-sidebar__wrapper--mobile--fullwidth.sc-digi-navigation-sidebar{--digi--navigation-sidebar--wrapper--width:var(--digi--navigation-sidebar--wrapper--width--small--fullwidth)}}.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__wrapper--active.sc-digi-navigation-sidebar{opacity:1;visibility:initial}@media screen and (max-width: 47.9375rem){.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__wrapper--static.sc-digi-navigation-sidebar{position:var(--digi--navigation-sidebar--wrapper--position)}}@media screen and (min-width: 48rem){.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__wrapper--static.sc-digi-navigation-sidebar{--digi--navigation-sidebar--wrapper--width:100%}}@media screen and (min-width: 48rem){.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__wrapper--over.sc-digi-navigation-sidebar{box-shadow:var(--digi--navigation-sidebar--wrapper--box-shadow)}}.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__wrapper--over.sc-digi-navigation-sidebar,.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__wrapper--push.sc-digi-navigation-sidebar{transition:var(--digi--navigation-sidebar--wrapper--transition);position:var(--digi--navigation-sidebar--wrapper--position)}@media screen and (min-width: 48rem){.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__wrapper--push.digi-navigation-sidebar__wrapper--start.sc-digi-navigation-sidebar{border-right:var(--digi--navigation-sidebar--wrapper--border)}}@media screen and (min-width: 48rem){.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__wrapper--push.digi-navigation-sidebar__wrapper--end.sc-digi-navigation-sidebar{border-left:var(--digi--navigation-sidebar--wrapper--border)}}@media screen and (min-width: 48rem){.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__wrapper--backdrop.digi-navigation-sidebar__wrapper--push.sc-digi-navigation-sidebar,.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__wrapper--backdrop.digi-navigation-sidebar__wrapper--over.sc-digi-navigation-sidebar{--digi--navigation-sidebar--wrapper--box-shadow:var(--digi--navigation-sidebar--wrapper--box-shadow--backdrop);box-shadow:var(--digi--navigation-sidebar--wrapper--box-shadow)}}@media screen and (min-width: 48rem){.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__wrapper--start.sc-digi-navigation-sidebar{left:0}.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__wrapper--start.digi-navigation-sidebar__wrapper--active.sc-digi-navigation-sidebar{--digi--navigation-sidebar--wrapper--margin:0}}@media screen and (min-width: 48rem){.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__wrapper--end.sc-digi-navigation-sidebar{right:0;--digi--navigation-sidebar--wrapper--margin:0 -21.875rem 0 0}.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__wrapper--end.digi-navigation-sidebar__wrapper--active.sc-digi-navigation-sidebar{--digi--navigation-sidebar--wrapper--margin:0}}@media screen and (max-width: 47.9375rem){.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__wrapper--mobile--start.sc-digi-navigation-sidebar{left:0}.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__wrapper--mobile--start.digi-navigation-sidebar__wrapper--active.sc-digi-navigation-sidebar{--digi--navigation-sidebar--wrapper--margin:0}}@media screen and (max-width: 47.9375rem){.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__wrapper--mobile--end.sc-digi-navigation-sidebar{right:0;--digi--navigation-sidebar--wrapper--margin:0 -21.875rem 0 0}.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__wrapper--mobile--end.digi-navigation-sidebar__wrapper--active.sc-digi-navigation-sidebar{--digi--navigation-sidebar--wrapper--margin:0}}.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__wrapper--sticky-header.sc-digi-navigation-sidebar .digi-navigation-sidebar__inner.sc-digi-navigation-sidebar{display:flex;flex-direction:column;max-height:100vh}.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__wrapper--sticky-header.sc-digi-navigation-sidebar .digi-navigation-sidebar__header.sc-digi-navigation-sidebar{border-bottom:var(--digi--navigation-sidebar--header--border)}.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__wrapper--sticky-header.sc-digi-navigation-sidebar .digi-navigation-sidebar__content.sc-digi-navigation-sidebar{overflow-y:auto;overflow-x:hidden;max-height:100vh}@media screen and (min-width: 48rem){.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__wrapper--sticky-header.digi-navigation-sidebar__wrapper--static.sc-digi-navigation-sidebar .digi-navigation-sidebar__inner.sc-digi-navigation-sidebar{max-height:unset}}@media screen and (min-width: 48rem){.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__wrapper--sticky-header.digi-navigation-sidebar__wrapper--static.sc-digi-navigation-sidebar .digi-navigation-sidebar__content.sc-digi-navigation-sidebar{overflow-y:unset;overflow-x:unset;max-height:unset}}.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__wrapper.sc-digi-navigation-sidebar:not(.digi-navigation-sidebar__wrapper--sticky-header) .digi-navigation-sidebar__inner.sc-digi-navigation-sidebar{overflow-y:auto;overflow-x:hidden;max-height:100vh}@media screen and (min-width: 48rem){.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__wrapper.sc-digi-navigation-sidebar:not(.digi-navigation-sidebar__wrapper--sticky-header).digi-navigation-sidebar__wrapper--static .digi-navigation-sidebar__inner.sc-digi-navigation-sidebar{overflow-y:unset;overflow-x:unset;max-height:unset}}.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__header.sc-digi-navigation-sidebar{display:flex;flex-wrap:wrap;align-items:center;padding:var(--digi--navigation-sidebar--header--padding)}.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__header--close-button--end.sc-digi-navigation-sidebar{justify-content:flex-end}.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__heading.sc-digi-navigation-sidebar{font-family:var(--digi--navigation-sidebar--heading--font-family);font-size:var(--digi--navigation-sidebar--heading--font-size);font-weight:var(--digi--navigation-sidebar--heading--font-weight);color:var(--digi--navigation-sidebar--heading--color);margin:0 auto 0 0}.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__heading.sc-digi-navigation-sidebar:focus{outline:none}@media (min-width: 48rem){.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__heading.sc-digi-navigation-sidebar{font-size:var(--digi--navigation-sidebar--heading--font-size--desktop)}}.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__close-button.sc-digi-navigation-sidebar{--digi--button--padding--medium:var(--digi--navigation-sidebar--close-button--padding);--digi--button--outline--focus:solid 2px var(--digi--color--border--secondary)}.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__backdrop.sc-digi-navigation-sidebar{visibility:hidden;opacity:0;transition:var(--digi--navigation-sidebar--backdrop--transition);position:relative;z-index:var(--digi--navigation-sidebar--backdrop--z-index)}@media (prefers-reduced-motion){.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__backdrop.sc-digi-navigation-sidebar{--digi--navigation-sidebar--backdrop--transition:$animation__duration--reduced}}.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__backdrop--active.sc-digi-navigation-sidebar,.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__backdrop--active--mobile.sc-digi-navigation-sidebar{background:var(--digi--navigation-sidebar--backdrop--background);width:100%;height:100%;position:fixed;top:0;left:0;visibility:visible;opacity:1}@media screen and (min-width: 48rem){.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__backdrop.sc-digi-navigation-sidebar{display:none}.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__backdrop--active.sc-digi-navigation-sidebar{display:block}}@media screen and (max-width: 47.9375rem){.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__backdrop.sc-digi-navigation-sidebar{display:none}.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__backdrop--active--mobile.sc-digi-navigation-sidebar{display:block}}.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__backdrop--hidden.sc-digi-navigation-sidebar{display:none}.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__backdrop--hidden--active--mobile.sc-digi-navigation-sidebar{display:block}.sc-digi-navigation-sidebar-h .digi-navigation-sidebar__backdrop--hidden.sc-digi-navigation-sidebar{display:none}";

const NavigationSidebar = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.afOnClose = index.createEvent(this, "afOnClose", 7);
    this.afOnEsc = index.createEvent(this, "afOnEsc", 7);
    this.afOnBackdropClick = index.createEvent(this, "afOnBackdropClick", 7);
    this.afOnToggle = index.createEvent(this, "afOnToggle", 7);
    // private _closeButton;
    this._focusableElementsSelectors = 'a, input, select, textarea, button, button:not(hidden), iframe, object, [tabindex="0"]';
    this.focusableElements = [];
    this.isMobile = undefined;
    this.showHeader = true;
    this.afPosition = navigationSidebarVariation_enum.NavigationSidebarPosition.START;
    this.afVariation = navigationSidebarVariation_enum.NavigationSidebarVariation.OVER;
    this.afMobilePosition = undefined;
    this.afMobileVariation = navigationSidebarVariation_enum.NavigationSidebarMobileVariation.DEFAULT;
    this.afHeading = undefined;
    this.afHeadingLevel = navigationSidebarVariation_enum.NavigationSidebarHeadingLevel.H2;
    this.afCloseButtonPosition = navigationSidebarVariation_enum.NavigationSidebarCloseButtonPosition.START;
    this.afStickyHeader = undefined;
    this.afCloseButtonText = undefined;
    this.afCloseButtonAriaLabel = undefined;
    this.afHideHeader = undefined;
    this.afFocusableElement = undefined;
    this.afCloseFocusableElement = undefined;
    this.afActive = false;
    this.afBackdrop = true;
    this.afId = randomIdGenerator_util.randomIdGenerator('digi-navigation-context-menu');
  }
  toggleHandler(e) {
    this._closeFocusableElement = e.target.querySelector('button');
  }
  breakpointHandler(e) {
    if (e.target.matches('digi-util-breakpoint-observer')) {
      this.isMobile =
        e.detail.value === utilBreakpointObserverBreakpoints_enum.UtilBreakpointObserverBreakpoints.SMALL ? true : false;
    }
  }
  toggleTransitionEndHandler(e) {
    if (e.target.matches('.digi-navigation-sidebar__wrapper') &&
      e.propertyName === 'visibility') {
      this.setFocus();
    }
  }
  clickHandler(e) {
    // If clicking on toggle button, refresh the focusable items list
    // with the new sub level of items
    if (this.shouldUseFocusTrap) {
      const el = e.detail.target;
      if (el.tagName.toLowerCase() === 'button') {
        setTimeout(() => {
          this.getFocusableItems();
        }, 100);
      }
    }
  }
  setMobileView(isMobile) {
    isMobile && this.afActive
      ? (this.disablePageScroll(), this.getFocusableItems())
      : this.enablePageScroll();
  }
  activeChange(active) {
    if (this.isMobile ||
      (this.afBackdrop &&
        !this.isMobile &&
        this.afVariation !== navigationSidebarVariation_enum.NavigationSidebarVariation.STATIC)) {
      this.pageScrollToggler();
    }
    if (this.afVariation === navigationSidebarVariation_enum.NavigationSidebarVariation.PUSH) {
      this.pushPageContentToggler();
    }
    this.afOnToggle.emit(active);
  }
  headerChange(hide) {
    this.setShowHeader(hide);
  }
  setShowHeader(hide) {
    this.showHeader = !hide;
  }
  setHasFooter() {
    const footer = !!this.hostElement.querySelector('[slot="footer"]');
    if (footer) {
      this._hasFooter = footer;
    }
  }
  setFocus() {
    if (this.afActive) {
      let el;
      let firstNavLink;
      // Set first nav link
      if (!this.afHideHeader) {
        firstNavLink = this.focusableElements[1];
      }
      else {
        firstNavLink = this.firstFocusableEl;
      }
      if (!!this.afFocusableElement) {
        el = this.hostElement.querySelector(`${this.afFocusableElement}`);
      }
      else {
        if (!!this.afHeading) {
          el = this._heading;
        }
        else {
          el = firstNavLink;
        }
      }
      if (!!el) {
        el.focus();
      }
    }
    else {
      if (!!this.afCloseFocusableElement) {
        const el = document.querySelector(`${this.afCloseFocusableElement}`);
        el.focus();
      }
      else if (!!this._closeFocusableElement) {
        const el = this._closeFocusableElement;
        el.focus();
      }
    }
  }
  pushPageContentToggler() {
    document.body.classList.remove('digi--has-open-sidebar-right-push');
    document.body.classList.remove('digi--has-open-sidebar-left-push');
    if (this.afActive && this.afVariation === navigationSidebarVariation_enum.NavigationSidebarVariation.PUSH) {
      this.afPosition === navigationSidebarVariation_enum.NavigationSidebarPosition.END
        ? document.body.classList.add('digi--has-open-sidebar-right-push')
        : document.body.classList.add('digi--has-open-sidebar-left-push');
    }
  }
  pageScrollToggler() {
    this.afActive ? this.disablePageScroll() : this.enablePageScroll();
  }
  enablePageScroll() {
    document.body.style.height = '';
    document.body.style.overflowY = '';
    document.body.style.paddingRight = '';
  }
  disablePageScroll() {
    document.body.style.height = '100vh';
    document.body.style.overflowY = 'hidden';
    // document.body.style.paddingRight = '17px';
  }
  closeHandler(e) {
    this.afActive = false;
    this.afOnClose.emit(e);
  }
  escHandler(e) {
    if (this.isMobile || this.afVariation !== navigationSidebarVariation_enum.NavigationSidebarVariation.STATIC) {
      this.afActive = false;
      this.afOnEsc.emit(e);
    }
  }
  tabHandler(e) {
    if (this.shouldUseFocusTrap) {
      if (document.activeElement === this.lastFocusableEl) {
        e.detail.preventDefault();
        this.firstFocusableEl.focus();
      }
    }
  }
  backdropClickHandler(e) {
    this.afActive = false;
    this.afOnBackdropClick.emit(e);
  }
  shiftHandler(e) {
    if (this.shouldUseFocusTrap) {
      if (document.activeElement === this.firstFocusableEl) {
        e.detail.preventDefault();
        this.lastFocusableEl.focus();
      }
    }
  }
  componentWillLoad() {
    this.setHasFooter();
    if (!this.afMobilePosition) {
      this.afMobilePosition =
        this.afPosition === navigationSidebarVariation_enum.NavigationSidebarPosition.START
          ? navigationSidebarVariation_enum.NavigationSidebarMobilePosition.START
          : navigationSidebarVariation_enum.NavigationSidebarMobilePosition.END;
    }
    if (this.afVariation === navigationSidebarVariation_enum.NavigationSidebarVariation.PUSH) {
      this.pushPageContentToggler();
      const reducedMotion = this.reducedMotion();
      if (!reducedMotion) {
        document.body.style.transition = 'var(--digi--page--transition)';
      }
    }
  }
  componentDidLoad() {
    setTimeout(() => {
      this.getFocusableItems();
    }, 100);
    this.headerChange(this.afHideHeader);
    if (this.afBackdrop &&
      this.afVariation !== navigationSidebarVariation_enum.NavigationSidebarVariation.STATIC) {
      this.pageScrollToggler();
    }
  }
  componentWillUpdate() {
    this.setHasFooter();
    this.pushPageContentToggler();
  }
  get shouldUseFocusTrap() {
    return ((this.isMobile || this.afVariation !== navigationSidebarVariation_enum.NavigationSidebarVariation.STATIC) &&
      this.focusableElements.length > 0);
  }
  getFocusableItems() {
    const allElements = this._wrapper.querySelectorAll(this._focusableElementsSelectors);
    // Filters out visible items
    this.focusableElements = Array.prototype.slice
      .call(allElements)
      .filter(function (item) {
      return item.offsetParent !== null;
    });
    // Sets first and last focusable element
    if (this.focusableElements.length > 0) {
      this.firstFocusableEl = this.focusableElements[0];
      this.lastFocusableEl = this.focusableElements[this.focusableElements.length - 1];
    }
  }
  reducedMotion() {
    const reduced = window.matchMedia('(prefers-reduced-motion: reduce)');
    return !reduced || reduced.matches ? true : false;
  }
  get cssModifiers() {
    return {
      'digi-navigation-sidebar__wrapper--start': this.afPosition === navigationSidebarVariation_enum.NavigationSidebarPosition.START,
      'digi-navigation-sidebar__wrapper--end': this.afPosition === navigationSidebarVariation_enum.NavigationSidebarPosition.END,
      'digi-navigation-sidebar__wrapper--over': this.afVariation === navigationSidebarVariation_enum.NavigationSidebarVariation.OVER,
      'digi-navigation-sidebar__wrapper--push': this.afVariation === navigationSidebarVariation_enum.NavigationSidebarVariation.PUSH,
      'digi-navigation-sidebar__wrapper--static': this.afVariation === navigationSidebarVariation_enum.NavigationSidebarVariation.STATIC,
      'digi-navigation-sidebar__wrapper--mobile--start': this.afMobilePosition === navigationSidebarVariation_enum.NavigationSidebarMobilePosition.START,
      'digi-navigation-sidebar__wrapper--mobile--end': this.afMobilePosition === navigationSidebarVariation_enum.NavigationSidebarMobilePosition.END,
      'digi-navigation-sidebar__wrapper--mobile--default': this.afMobileVariation === navigationSidebarVariation_enum.NavigationSidebarMobileVariation.DEFAULT,
      'digi-navigation-sidebar__wrapper--mobile--fullwidth': this.afMobileVariation === navigationSidebarVariation_enum.NavigationSidebarMobileVariation.FULLWIDTH,
      'digi-navigation-sidebar__wrapper--mobile--active': this.isMobile,
      'digi-navigation-sidebar__wrapper--active': this.afActive,
      'digi-navigation-sidebar__wrapper--backdrop': this.afBackdrop,
      'digi-navigation-sidebar__wrapper--sticky-header': this.afStickyHeader
    };
  }
  get cssModifiersBackdrop() {
    return {
      'digi-navigation-sidebar__backdrop--active': this.afActive && this.afBackdrop,
      'digi-navigation-sidebar__backdrop--active--mobile': this.afActive &&
        this.isMobile &&
        this.afMobileVariation !== navigationSidebarVariation_enum.NavigationSidebarMobileVariation.FULLWIDTH,
      'digi-navigation-sidebar__backdrop--hidden': !this.isMobile && this.afVariation === navigationSidebarVariation_enum.NavigationSidebarVariation.STATIC
    };
  }
  render() {
    return (index.h(index.Host, null, index.h("digi-util-breakpoint-observer", null, index.h("digi-util-keydown-handler", { onAfOnEsc: (e) => this.escHandler(e), onAfOnTab: (e) => this.tabHandler(e), onAfOnShiftTab: (e) => this.shiftHandler(e) }, index.h("div", { class: "digi-navigation-sidebar", "aria-hidden": !this.afActive ? 'true' : 'false' }, index.h("div", { "aria-hidden": "true", class: Object.assign({ 'digi-navigation-sidebar__backdrop': true }, this.cssModifiersBackdrop), onClick: (e) => this.backdropClickHandler(e) }), index.h("div", { class: Object.assign({ 'digi-navigation-sidebar__wrapper': true }, this.cssModifiers) }, index.h("div", { class: "digi-navigation-sidebar__inner", ref: (el) => {
        this._wrapper = el;
      } }, this.showHeader ? (index.h("div", { class: {
        'digi-navigation-sidebar__header': true,
        'digi-navigation-sidebar__header--close-button--start': this.afCloseButtonPosition ===
          navigationSidebarVariation_enum.NavigationSidebarCloseButtonPosition.START,
        'digi-navigation-sidebar__header--close-button--end': this.afCloseButtonPosition ===
          navigationSidebarVariation_enum.NavigationSidebarCloseButtonPosition.END,
        'digi-navigation-sidebar__header--reversed': !!this.afHeading
      } }, this.afHeading && (index.h(this.afHeadingLevel, { ref: (el) => (this._heading = el), class: "digi-navigation-sidebar__heading", tabindex: "-1" }, this.afHeading)), index.h("digi-button", {
      // ref={(el) => (this._closeButton = el)}
      onClick: (e) => this.closeHandler(e), "af-variation": "function", "af-aria-label": this.afCloseButtonAriaLabel, class: "digi-navigation-sidebar__close-button"
    }, this.afCloseButtonText, index.h("digi-icon", { slot: "icon-secondary", afName: `x` })))) : (index.h("div", null)), index.h("div", { class: "digi-navigation-sidebar__content" }, index.h("div", { class: "digi-navigation-sidebar__nav-wrapper" }, index.h("slot", null)), this._hasFooter && (index.h("div", { class: "digi-navigation-sidebar__footer" }, index.h("slot", { name: "footer" })))))))))));
  }
  get hostElement() { return index.getElement(this); }
  static get watchers() { return {
    "isMobile": ["setMobileView"],
    "afActive": ["activeChange"],
    "afHideHeader": ["headerChange"]
  }; }
};
NavigationSidebar.style = navigationSidebarCss;

exports.digi_navigation_sidebar = NavigationSidebar;
