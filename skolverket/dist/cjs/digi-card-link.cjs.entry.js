'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');
require('./button-size.enum-a46b7684.js');
require('./button-type.enum-322621ea.js');
require('./button-variation.enum-9364587a.js');
require('./expandable-accordion-variation.enum-2e394d6b.js');
require('./calendar-week-view-heading-level.enum-696abd05.js');
require('./form-radiobutton-variation.enum-cd8ac648.js');
require('./code-block-variation.enum-a28057da.js');
require('./code-example-variation.enum-a954aaf0.js');
require('./code-variation.enum-4c625938.js');
require('./form-checkbox-variation.enum-04455f3e.js');
require('./form-file-upload-variation.enum-26e4a9bd.js');
require('./form-input-search-variation.enum-5ea4a951.js');
require('./form-input-type.enum-7f96d46d.js');
require('./form-input-variation.enum-f95a25ea.js');
require('./form-select-variation.enum-139eab4f.js');
require('./form-textarea-variation.enum-8f1a5f71.js');
require('./form-validation-message-variation.enum-9244655f.js');
require('./layout-block-variation.enum-ae7e0e5f.js');
require('./layout-columns-variation.enum-9b7242b1.js');
require('./layout-container-variation.enum-7af59e50.js');
require('./layout-media-object-alignment.enum-e770b17d.js');
require('./link-external-variation.enum-864f5ac7.js');
require('./link-internal-variation.enum-354aedf4.js');
require('./link-variation.enum-55cb3944.js');
require('./loader-spinner-size.enum-96d3508e.js');
require('./media-figure-alignment.enum-5327a385.js');
require('./navigation-context-menu-item-type.enum-105e809f.js');
require('./navigation-sidebar-variation.enum-fa604f57.js');
require('./navigation-vertical-menu-variation.enum-40269ed5.js');
require('./progress-step-variation.enum-596ab007.js');
require('./progress-steps-variation.enum-3bbe5656.js');
require('./progressbar-variation.enum-81e49354.js');
require('./tag-size.enum-774d54e2.js');
require('./typography-meta-variation.enum-c7469a03.js');
require('./typography-time-variation.enum-4526d4ae.js');
const typographyVariation_enum = require('./typography-variation.enum-08e58c63.js');
require('./util-breakpoint-observer-breakpoints.enum-9f89c8ce.js');

const cardLinkCss = ".sc-digi-card-link-h{--digi--card-link--padding:var(--digi--padding--largest);display:grid}.sc-digi-card-link-h .sc-digi-card-link-s>[slot^=link-heading]{display:flex;-webkit-margin-after:0.5rem;margin-block-end:0.5rem}.sc-digi-card-link-h .sc-digi-card-link-s>[slot^=link-heading] a{font-family:var(--digi--global--typography--font-family--default);text-decoration:none;color:var(--digi--color--text--secondary);font-size:var(--digi--global--typography--font-size--large);letter-spacing:calc(var(--digi--global--typography--font-size--large) / 100 * -1);font-weight:var(--digi--global--typography--font-weight--semibold)}.sc-digi-card-link-h .sc-digi-card-link-s>[slot^=link-heading] a:hover{color:var(--digi--global--color--profile--purple--dark);text-decoration:underline}.sc-digi-card-link-h .sc-digi-card-link-s>[slot^=link-heading] a:focus{outline:none;color:var(--digi--color--text--secondary)}.sc-digi-card-link-h .sc-digi-card-link-s>[slot^=link-heading] a:focus-visible{outline:var(--digi--border-width--secondary) solid var(--digi--color--border--focus)}.sc-digi-card-link-h .sc-digi-card-link-s>[slot^=link-heading] a:visited{color:var(--digi--color--text--secondary)}@media (min-width: 48rem){.sc-digi-card-link-h .sc-digi-card-link-s>[slot^=link-heading] a{font-size:var(--digi--global--typography--font-size--larger);letter-spacing:calc(var(--digi--global--typography--font-size--larger) / 100 * -1);line-height:1.44}}.sc-digi-card-link-h .sc-digi-card-link-s>[slot^=link-heading] a:hover{color:var(--digi--global--color--profile--purple--dark);text-decoration:underline}.sc-digi-card-link-h .sc-digi-card-link-s>[slot^=link-heading] a::after{background-color:currentColor;padding:0 0.5em;-webkit-mask-image:url(\"data:image/svg+xml;charset=utf-8, %3Csvg%0A%09width%3D%2224%22%0A%09height%3D%2224%22%0A%09viewBox%3D%220%200%2024%2024%22%0A%09xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%0A%3E%0A%09%3Cpath%0A%09%09d%3D%22M288.917%2C415.517%2C287.5%2C414.1l3.96-3.96-3.96-3.96%2C1.414-1.414%2C5.373%2C5.374Z%22%0A%09%09transform%3D%22translate(-278.17%20-398.103)%22%0A%09%2F%3E%0A%3C%2Fsvg%3E\");mask-image:url(\"data:image/svg+xml;charset=utf-8, %3Csvg%0A%09width%3D%2224%22%0A%09height%3D%2224%22%0A%09viewBox%3D%220%200%2024%2024%22%0A%09xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%0A%3E%0A%09%3Cpath%0A%09%09d%3D%22M288.917%2C415.517%2C287.5%2C414.1l3.96-3.96-3.96-3.96%2C1.414-1.414%2C5.373%2C5.374Z%22%0A%09%09transform%3D%22translate(-278.17%20-398.103)%22%0A%09%2F%3E%0A%3C%2Fsvg%3E\");-webkit-mask-repeat:no-repeat;mask-repeat:no-repeat;-webkit-mask-position:center;mask-position:center;-webkit-clip-path:padding-box inset(0.25em 0);clip-path:padding-box inset(0.25em 0);content:\"\";-webkit-mask-size:cover;mask-size:cover}.sc-digi-card-link-h .sc-digi-card-link-s>[slot^=link-heading] a::before{content:\"\";position:absolute;top:0;left:0;width:100%;height:100%;border-radius:var(--digi--border-radius--primary)}.sc-digi-card-link-h .sc-digi-card-link-s>[slot^=link-heading] a:focus-visible{outline:none}.sc-digi-card-link-h .sc-digi-card-link-s>[slot^=link-heading] a:focus-visible::before{content:\"\";outline:var(--digi--focus-outline)}.sc-digi-card-link-h .sc-digi-card-link-s>[slot^=link-heading] a svg{width:1.3em;height:1.3em}.sc-digi-card-link-h .sc-digi-card-link-s>img[slot^=image]{display:block;width:100%;height:auto}.sc-digi-card-link-h .digi-card-link.sc-digi-card-link{display:grid;grid-template-rows:auto 1fr;position:relative;border-radius:var(--digi--border-radius--primary);background:var(--digi--color--background--primary)}.sc-digi-card-link-h .digi-card-link__image.sc-digi-card-link{border-radius:var(--digi--border-radius--primary) var(--digi--border-radius--primary) 0 0;overflow:hidden;aspect-ratio:16/9;background-color:var(--digi--color--background--tertiary)}.sc-digi-card-link-h .digi-card-link__text.sc-digi-card-link{position:relative}.digi-card-link__content.sc-digi-card-link{display:flex;padding:var(--digi--card-link--padding);border:var(--digi--border-width--tertiary) var(--digi--border-style--primary) var(--digi--global--color--profile--purple--opacity15);border-radius:0 0 var(--digi--border-radius--primary) var(--digi--border-radius--primary)}.digi-card-link--image-false.sc-digi-card-link .digi-card-link__content.sc-digi-card-link{border-radius:var(--digi--border-radius--primary);grid-row:1/3}.digi-card-link--image-true.sc-digi-card-link .digi-card-link__content.sc-digi-card-link{-webkit-border-before:none;border-block-start:none}";

const CardLink = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.hasImage = undefined;
  }
  componentWillLoad() {
    this.setHasImage();
  }
  componentWillUpdate() {
    this.setHasImage();
  }
  setHasImage() {
    this.hasImage = !!this.hostElement.querySelector('[slot="image"]');
  }
  get cssModifiers() {
    return {
      [`digi-card-link--image-${this.hasImage}`]: true
    };
  }
  render() {
    return (index.h("div", { class: Object.assign({ 'digi-card-link': true }, this.cssModifiers) }, this.hasImage && (index.h("div", { class: "digi-card-link__image" }, index.h("slot", { name: "image" }))), index.h("digi-typography", { afVariation: typographyVariation_enum.TypographyVariation.LARGE, class: "digi-card-link__content" }, index.h("div", { class: "digi-card-link__heading" }, index.h("slot", { name: "link-heading" })), index.h("div", { class: "digi-card-link__text" }, index.h("slot", null)))));
  }
  get hostElement() { return index.getElement(this); }
};
CardLink.style = cardLinkCss;

exports.digi_card_link = CardLink;
