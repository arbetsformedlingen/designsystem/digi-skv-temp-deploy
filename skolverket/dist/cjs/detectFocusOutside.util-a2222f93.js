'use strict';

const detectClosest_util = require('./detectClosest.util-8778dfa8.js');

function detectFocusOutside(target, selector) {
  return !detectClosest_util.detectClosest(target, selector);
}

exports.detectFocusOutside = detectFocusOutside;
