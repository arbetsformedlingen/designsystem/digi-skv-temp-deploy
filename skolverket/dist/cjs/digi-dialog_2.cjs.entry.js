'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');
const cardBoxGutter_enum = require('./card-box-gutter.enum-585dded6.js');
require('./card-box-width.enum-1a09a997.js');
const pageBackground_enum = require('./page-background.enum-c35f4c9a.js');
require('./layout-grid-vertical-spacing.enum-2b857671.js');
require('./layout-stacked-blocks-variation.enum-dd570a85.js');
require('./list-link-variation.enum-c43133c0.js');
require('./navigation-breadcrumbs-variation.enum-b46c1f09.js');
require('./notification-detail-variation.enum-658965de.js');
require('./page-footer-variation.enum-ec14686c.js');
require('./table-variation.enum-bc26c29e.js');
require('./button-size.enum-a46b7684.js');
require('./button-type.enum-322621ea.js');
const buttonVariation_enum = require('./button-variation.enum-9364587a.js');
require('./expandable-accordion-variation.enum-2e394d6b.js');
require('./calendar-week-view-heading-level.enum-696abd05.js');
require('./form-radiobutton-variation.enum-cd8ac648.js');
require('./code-block-variation.enum-a28057da.js');
require('./code-example-variation.enum-a954aaf0.js');
require('./code-variation.enum-4c625938.js');
require('./form-checkbox-variation.enum-04455f3e.js');
require('./form-file-upload-variation.enum-26e4a9bd.js');
require('./form-input-search-variation.enum-5ea4a951.js');
require('./form-input-type.enum-7f96d46d.js');
require('./form-input-variation.enum-f95a25ea.js');
require('./form-select-variation.enum-139eab4f.js');
require('./form-textarea-variation.enum-8f1a5f71.js');
require('./form-validation-message-variation.enum-9244655f.js');
require('./layout-block-variation.enum-ae7e0e5f.js');
require('./layout-columns-variation.enum-9b7242b1.js');
require('./layout-container-variation.enum-7af59e50.js');
require('./layout-media-object-alignment.enum-e770b17d.js');
require('./link-external-variation.enum-864f5ac7.js');
require('./link-internal-variation.enum-354aedf4.js');
require('./link-variation.enum-55cb3944.js');
require('./loader-spinner-size.enum-96d3508e.js');
require('./media-figure-alignment.enum-5327a385.js');
require('./navigation-context-menu-item-type.enum-105e809f.js');
require('./navigation-sidebar-variation.enum-fa604f57.js');
require('./navigation-vertical-menu-variation.enum-40269ed5.js');
require('./progress-step-variation.enum-596ab007.js');
require('./progress-steps-variation.enum-3bbe5656.js');
require('./progressbar-variation.enum-81e49354.js');
require('./tag-size.enum-774d54e2.js');
require('./typography-meta-variation.enum-c7469a03.js');
require('./typography-time-variation.enum-4526d4ae.js');
const typographyVariation_enum = require('./typography-variation.enum-08e58c63.js');
require('./util-breakpoint-observer-breakpoints.enum-9f89c8ce.js');

const dialogCss = ".sc-digi-dialog-h .sc-digi-dialog-s>[slot^=heading]{--digi--heading-4--font-size:var(--digi--typography--heading-4--font-size--mobile);--digi--heading-4--line-height:var(--digi--typography--heading-4--line-height--mobile);font-family:var(--digi--global--typography--font-family--default);font-size:var(--digi--heading-4--font-size);line-height:var(--digi--heading-4--line-height);font-weight:var(--digi--typography--heading-4--font-weight--mobile);-webkit-margin-after:0;margin-block-end:0}@media (min-width: 48rem){.sc-digi-dialog-h .sc-digi-dialog-s>[slot^=heading]{--digi--heading-4--font-size:var(--digi--typography--heading-4--font-size--desktop);--digi--heading-4--line-height:var(--digi--typography--heading-4--line-height--desktop)}}@media (min-width: 62rem){.sc-digi-dialog-h .sc-digi-dialog-s>[slot^=heading]{--digi--heading-4--font-size:var(--digi--typography--heading-4--font-size--desktop-large);--digi--heading-4--line-height:var(--digi--typography--heading-4--line-height--desktop-large)}}.digi-dialog.sc-digi-dialog{border:none;padding:0;border-radius:var(--digi--border-radius--primary);box-shadow:0px 8px 16px 0px rgba(105, 40, 89, 0.2);max-width:calc(100vw - var(--digi--container-gutter--medium) * 2);max-height:calc(100vh - var(--digi--container-gutter--medium) * 2);overflow:hidden}.digi-dialog.sc-digi-dialog::backdrop{background:rgba(0, 0, 0, 0.4)}@media (max-width: 47.9375rem){.digi-dialog.sc-digi-dialog{width:100vw;height:calc(100vh - var(--digi--container-gutter--medium));margin:0;top:var(--digi--container-gutter--medium);max-width:none;max-height:none}}.digi-dialog__inner.sc-digi-dialog{display:grid;grid-template-rows:min-content 1fr min-content;grid-template-areas:\"header close\" \"content content\" \"actions actions\";gap:var(--digi--responsive-grid-gutter);max-height:calc(100vh - var(--digi--container-gutter--medium) * 2)}@media (max-width: 47.9375rem){.digi-dialog__inner.sc-digi-dialog{height:calc(100vh - var(--digi--container-gutter--medium));max-height:none}}.digi-dialog__header.sc-digi-dialog{grid-area:header;padding:var(--digi--padding--largest) 0 0 var(--digi--padding--largest)}@media (min-width: 48rem){.digi-dialog__header.sc-digi-dialog{padding:var(--digi--padding--largest-3) 0 0 var(--digi--padding--largest-3)}}.digi-dialog__close-button-wrapper.sc-digi-dialog{grid-area:close;justify-self:end;padding:var(--digi--padding--largest) var(--digi--padding--largest) 0 0}@media (min-width: 48rem){.digi-dialog__close-button-wrapper.sc-digi-dialog{padding:var(--digi--padding--largest-3) var(--digi--padding--largest-3) 0 0}}.digi-dialog__close-button.sc-digi-dialog{font-family:var(--digi--global--typography--font-family--default);text-decoration:none;color:var(--digi--color--text--secondary);font-size:var(--digi--global--typography--font-size--large);letter-spacing:calc(var(--digi--global--typography--font-size--large) / 100 * -1);font-weight:var(--digi--global--typography--font-weight--semibold);display:flex;flex-direction:row;font-weight:var(--digi--global--typography--font-weight--semibold);font-size:var(--digi--typography--button--font-size--desktop);gap:var(--digi--gutter--icon);align-items:flex-start}.digi-dialog__close-button.sc-digi-dialog:hover{color:var(--digi--global--color--profile--purple--dark);text-decoration:underline}.digi-dialog__close-button.sc-digi-dialog:focus{outline:none;color:var(--digi--color--text--secondary)}.digi-dialog__close-button.sc-digi-dialog:focus-visible{outline:var(--digi--border-width--secondary) solid var(--digi--color--border--focus)}.digi-dialog__close-button.sc-digi-dialog:visited{color:var(--digi--color--text--secondary)}.digi-dialog__content.sc-digi-dialog{grid-area:content;padding-inline:var(--digi--padding--largest);overflow:auto}@media (min-width: 48rem){.digi-dialog__content.sc-digi-dialog{padding-inline:var(--digi--padding--largest-3)}}.digi-dialog__actions.sc-digi-dialog{grid-area:actions;padding:var(--digi--padding--largest);background:var(--digi--color--background--tertiary);display:flex;flex-wrap:wrap;justify-content:flex-end;gap:var(--digi--gutter--small)}@media (min-width: 48rem){.digi-dialog__actions.sc-digi-dialog{padding-inline:var(--digi--padding--largest-3)}}.digi-dialog__actions.sc-digi-dialog:empty{display:none}";

const Dialog = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.afOnOpen = index.createEvent(this, "afOnOpen", 7);
    this.afOnClose = index.createEvent(this, "afOnClose", 7);
    this.afOpen = false;
    this.afHideCloseButton = false;
  }
  afOpenChanged(newVal, oldVal) {
    if (newVal === oldVal)
      return;
    this.afOpen ? this.showModal() : this.close();
    this.toggleEmitter(this.afOpen ? 'open' : 'close');
  }
  async showModal() {
    this._dialog.showModal();
    this.afOpen = true;
    this.toggleEmitter('open');
  }
  async close() {
    this._dialog.close();
    this.afOpen = false;
    this.toggleEmitter('close');
  }
  toggleEmitter(state) {
    state === 'open' ? this.afOnOpen.emit() : this.afOnClose.emit();
  }
  clickOutsideHandler(e) {
    if (e.detail.target !== this._dialog)
      return;
    this.close();
  }
  componentDidLoad() {
    this.afOpen && this.showModal();
  }
  render() {
    return (index.h("dialog", { class: "digi-dialog", ref: (el) => {
        this._dialog = el;
      }, onClose: () => this.close() }, index.h("digi-util-detect-click-outside", { onAfOnClickOutside: (e) => this.clickOutsideHandler(e) }, index.h("digi-card-box", { afGutter: cardBoxGutter_enum.CardBoxGutter.NONE }, index.h("digi-typography", null, index.h("div", { class: "digi-dialog__inner" }, index.h("header", { class: "digi-dialog__header" }, index.h("slot", { name: "heading" })), index.h("div", { class: "digi-dialog__content" }, index.h("slot", null)), index.h("div", { class: "digi-dialog__actions" }, index.h("slot", { name: "actions" })), !this.afHideCloseButton && (index.h("div", { class: "digi-dialog__close-button-wrapper" }, index.h("button", { class: "digi-dialog__close-button", onClick: () => this.close(), type: "button" }, "St\u00E4ng ", index.h("digi-icon", { "aria-hidden": "true", afName: `x` }))))))))));
  }
  static get watchers() { return {
    "afOpen": ["afOpenChanged"]
  }; }
};
Dialog.style = dialogCss;

const notificationAlertCss = ".sc-digi-notification-alert-h{--digi--notification-alert--border-color--info:var(--digi--color--border--informative);--digi--notification-alert--border-color--warning:var(--digi--color--border--warning);--digi--notification-alert--border-color--danger:var(--digi--color--border--danger);--digi--notification-alert--background-color--info:var(--digi--color--background--notification-info);--digi--notification-alert--background-color--warning:var(--digi--color--background--notification-warning);--digi--notification-alert--background-color--danger:var(--digi--color--background--notification-danger)}.sc-digi-notification-alert-h .sc-digi-notification-alert-s>[slot^=heading]{--digi--heading-4--font-size:var(--digi--typography--heading-4--font-size--mobile);--digi--heading-4--line-height:var(--digi--typography--heading-4--line-height--mobile);font-family:var(--digi--global--typography--font-family--default);font-size:var(--digi--heading-4--font-size);line-height:var(--digi--heading-4--line-height);font-weight:var(--digi--typography--heading-4--font-weight--mobile);-webkit-margin-after:0;margin-block-end:0}@media (min-width: 48rem){.sc-digi-notification-alert-h .sc-digi-notification-alert-s>[slot^=heading]{--digi--heading-4--font-size:var(--digi--typography--heading-4--font-size--desktop);--digi--heading-4--line-height:var(--digi--typography--heading-4--line-height--desktop)}}@media (min-width: 62rem){.sc-digi-notification-alert-h .sc-digi-notification-alert-s>[slot^=heading]{--digi--heading-4--font-size:var(--digi--typography--heading-4--font-size--desktop-large);--digi--heading-4--line-height:var(--digi--typography--heading-4--line-height--desktop-large)}}.sc-digi-notification-alert-h .sc-digi-notification-alert-s>a[slot^=link]{font-family:var(--digi--global--typography--font-family--default);text-decoration:none;color:var(--digi--color--text--secondary);font-size:var(--digi--global--typography--font-size--large);letter-spacing:calc(var(--digi--global--typography--font-size--large) / 100 * -1);font-weight:var(--digi--global--typography--font-weight--semibold);display:block;border-radius:var(--digi--border-radius--primary);padding:calc(var(--digi--grid-gutter--smaller) * 1.5) var(--digi--grid-gutter--smaller);-webkit-padding-start:0;padding-inline-start:0;-webkit-padding-after:0;padding-block-end:0}.sc-digi-notification-alert-h .sc-digi-notification-alert-s>a[slot^=link]:hover{color:var(--digi--global--color--profile--purple--dark);text-decoration:underline}.sc-digi-notification-alert-h .sc-digi-notification-alert-s>a[slot^=link]:focus{outline:none;color:var(--digi--color--text--secondary)}.sc-digi-notification-alert-h .sc-digi-notification-alert-s>a[slot^=link]:focus-visible{outline:var(--digi--border-width--secondary) solid var(--digi--color--border--focus)}.sc-digi-notification-alert-h .sc-digi-notification-alert-s>a[slot^=link]:visited{color:var(--digi--color--text--secondary)}.sc-digi-notification-alert-h .sc-digi-notification-alert-s>a[slot^=link]::after{background-color:currentColor;padding:0 0.5em;-webkit-mask-image:url(\"data:image/svg+xml;charset=utf-8, %3Csvg%0A%09width%3D%2224%22%0A%09height%3D%2224%22%0A%09viewBox%3D%220%200%2024%2024%22%0A%09xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%0A%3E%0A%09%3Cpath%0A%09%09d%3D%22M288.917%2C415.517%2C287.5%2C414.1l3.96-3.96-3.96-3.96%2C1.414-1.414%2C5.373%2C5.374Z%22%0A%09%09transform%3D%22translate(-278.17%20-398.103)%22%0A%09%2F%3E%0A%3C%2Fsvg%3E\");mask-image:url(\"data:image/svg+xml;charset=utf-8, %3Csvg%0A%09width%3D%2224%22%0A%09height%3D%2224%22%0A%09viewBox%3D%220%200%2024%2024%22%0A%09xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%0A%3E%0A%09%3Cpath%0A%09%09d%3D%22M288.917%2C415.517%2C287.5%2C414.1l3.96-3.96-3.96-3.96%2C1.414-1.414%2C5.373%2C5.374Z%22%0A%09%09transform%3D%22translate(-278.17%20-398.103)%22%0A%09%2F%3E%0A%3C%2Fsvg%3E\");-webkit-mask-repeat:no-repeat;mask-repeat:no-repeat;-webkit-mask-position:center;mask-position:center;-webkit-clip-path:padding-box inset(0.25em 0);clip-path:padding-box inset(0.25em 0);content:\"\"}.sc-digi-notification-alert-h .sc-digi-notification-alert-s>[slot^=link]{display:block}.sc-digi-notification-alert-h .sc-digi-notification-alert-s>[slot^=link] a{font-family:var(--digi--global--typography--font-family--default);text-decoration:none;color:var(--digi--color--text--secondary);font-size:var(--digi--global--typography--font-size--large);letter-spacing:calc(var(--digi--global--typography--font-size--large) / 100 * -1);font-weight:var(--digi--global--typography--font-weight--semibold);display:block;border-radius:var(--digi--border-radius--primary);padding:calc(var(--digi--grid-gutter--smaller) * 1.5) var(--digi--grid-gutter--smaller);-webkit-padding-start:0;padding-inline-start:0;-webkit-padding-after:0;padding-block-end:0}.sc-digi-notification-alert-h .sc-digi-notification-alert-s>[slot^=link] a:hover{color:var(--digi--global--color--profile--purple--dark);text-decoration:underline}.sc-digi-notification-alert-h .sc-digi-notification-alert-s>[slot^=link] a:focus{outline:none;color:var(--digi--color--text--secondary)}.sc-digi-notification-alert-h .sc-digi-notification-alert-s>[slot^=link] a:focus-visible{outline:var(--digi--border-width--secondary) solid var(--digi--color--border--focus)}.sc-digi-notification-alert-h .sc-digi-notification-alert-s>[slot^=link] a:visited{color:var(--digi--color--text--secondary)}.sc-digi-notification-alert-h .sc-digi-notification-alert-s>[slot^=link] a::after{background-color:currentColor;padding:0 0.5em;-webkit-mask-image:url(\"data:image/svg+xml;charset=utf-8, %3Csvg%0A%09width%3D%2224%22%0A%09height%3D%2224%22%0A%09viewBox%3D%220%200%2024%2024%22%0A%09xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%0A%3E%0A%09%3Cpath%0A%09%09d%3D%22M288.917%2C415.517%2C287.5%2C414.1l3.96-3.96-3.96-3.96%2C1.414-1.414%2C5.373%2C5.374Z%22%0A%09%09transform%3D%22translate(-278.17%20-398.103)%22%0A%09%2F%3E%0A%3C%2Fsvg%3E\");mask-image:url(\"data:image/svg+xml;charset=utf-8, %3Csvg%0A%09width%3D%2224%22%0A%09height%3D%2224%22%0A%09viewBox%3D%220%200%2024%2024%22%0A%09xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%0A%3E%0A%09%3Cpath%0A%09%09d%3D%22M288.917%2C415.517%2C287.5%2C414.1l3.96-3.96-3.96-3.96%2C1.414-1.414%2C5.373%2C5.374Z%22%0A%09%09transform%3D%22translate(-278.17%20-398.103)%22%0A%09%2F%3E%0A%3C%2Fsvg%3E\");-webkit-mask-repeat:no-repeat;mask-repeat:no-repeat;-webkit-mask-position:center;mask-position:center;-webkit-clip-path:padding-box inset(0.25em 0);clip-path:padding-box inset(0.25em 0);content:\"\"}.digi-notification-alert.sc-digi-notification-alert{border:var(--digi--border-width--tertiary) var(--digi--border-style--primary) var(--digi--global--color--profile--purple--opacity15);border-radius:var(--digi--border-radius--primary);-webkit-border-before:var(--digi--border-width--secondary) solid var(--digi--color--border--secondary);border-block-start:var(--digi--border-width--secondary) solid var(--digi--color--border--secondary);border-block-start-color:var(--BORDER-COLOR);background:var(--BACKGROUND-COLOR)}.digi-notification-alert--variation-info.sc-digi-notification-alert{--BORDER-COLOR:var(--digi--notification-alert--border-color--info);--ICON-COLOR:var(--digi--notification-alert--border-color--info);--BACKGROUND-COLOR:var(--digi--notification-alert--background-color--info)}.digi-notification-alert--variation-danger.sc-digi-notification-alert{--BORDER-COLOR:var(--digi--notification-alert--border-color--danger);--ICON-COLOR:var(--digi--notification-alert--border-color--danger);--BACKGROUND-COLOR:var(--digi--notification-alert--background-color--danger)}.digi-notification-alert--variation-warning.sc-digi-notification-alert{--BORDER-COLOR:var(--digi--notification-alert--border-color--warning);--ICON-COLOR:var(--digi--notification-alert--border-color--warning);--BACKGROUND-COLOR:var(--digi--notification-alert--background-color--warning)}.digi-notification-alert__inner.sc-digi-notification-alert{display:grid;grid-template-columns:min-content min-content;grid-template-rows:min-content auto min-content;grid-template-areas:\"icon close\" \"content content\" \"actions actions\";gap:calc(var(--digi--gutter--icon) * 2);max-width:var(--digi--container-width--largest);margin:0 auto;padding:var(--digi--gutter--medium) var(--digi--gutter--larger)}@media (max-width: 47.9375rem){.digi-notification-alert__inner.sc-digi-notification-alert{justify-content:space-between;align-items:center}}@media (min-width: 48rem){.digi-notification-alert__inner.sc-digi-notification-alert{grid-template-columns:min-content 1fr auto;grid-template-rows:min-content auto;grid-template-areas:\"icon content actions\" \"icon content actions\"}.digi-notification-alert--closeable.sc-digi-notification-alert .digi-notification-alert__inner.sc-digi-notification-alert{grid-template-areas:\"icon content close\" \"icon content actions\"}}.digi-notification-alert__icon.sc-digi-notification-alert{grid-area:icon}.digi-notification-alert__icon.sc-digi-notification-alert digi-icon.sc-digi-notification-alert{--digi--icon--color:var(--ICON-COLOR);--digi-icon--fixed-width:32px;--digi-icon--fixed-height:32px}.digi-notification-alert__content.sc-digi-notification-alert{grid-area:content}.digi-notification-alert__close-button.sc-digi-notification-alert{grid-area:close;justify-self:end}.digi-notification-alert__actions.sc-digi-notification-alert{grid-area:actions}.digi-notification-alert__text.sc-digi-notification-alert{--digi--typography--body--font-size--desktop:var(--digi--global--typography--font-size--smaller);--digi--typography--body--font-size--mobile:var(--digi--global--typography--font-size--smaller);--digi--typography--body--line-height--desktop:var(--digi--global--typography--line-height--smaller);--digi--typography--body--line-height--mobile:var(--digi--global--typography--line-height--smaller)}";

const NotificationAlert = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.afOnClose = index.createEvent(this, "afOnClose", 7);
    this.afVariation = pageBackground_enum.NotificationAlertVariation.INFO;
    this.afCloseable = false;
  }
  clickHandler(e) {
    this.afOnClose.emit(e);
  }
  get cssModifiers() {
    return {
      [`digi-notification-alert--variation-${this.afVariation}`]: !!this.afVariation,
      'digi-notification-alert--closeable': this.afCloseable
    };
  }
  render() {
    return (index.h("div", { class: Object.assign({ 'digi-notification-alert': true }, this.cssModifiers) }, index.h("div", { class: "digi-notification-alert__inner" }, index.h("div", { class: "digi-notification-alert__icon" }, index.h("digi-icon", { afName: `notification-${this.afVariation}`, "aria-hidden": "true" })), this.afCloseable && (index.h("digi-button", { "af-variation": buttonVariation_enum.ButtonVariation.FUNCTION, onAfOnClick: (e) => this.clickHandler(e), class: "digi-notification-alert__close-button" }, index.h("span", { class: "digi-notification-alert__close-button__text" }, "St\u00E4ng"), index.h("digi-icon", { afName: `close`, slot: "icon-secondary", "aria-hidden": "true" }))), index.h("div", { class: "digi-notification-alert__content" }, index.h("slot", { name: "heading" }), index.h("digi-typography", { class: "digi-notification-alert__text", "af-variation": typographyVariation_enum.TypographyVariation.SMALL }, index.h("slot", null)), index.h("slot", { name: "link" })), index.h("div", { class: "digi-notification-alert__actions" }, index.h("slot", { name: "actions" })))));
  }
};
NotificationAlert.style = notificationAlertCss;

exports.digi_dialog = Dialog;
exports.digi_notification_alert = NotificationAlert;
