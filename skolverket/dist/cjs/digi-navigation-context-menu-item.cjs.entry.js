'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');
const logger_util = require('./logger.util-a78a149e.js');
const navigationContextMenuItemType_enum = require('./navigation-context-menu-item-type.enum-105e809f.js');

const NavigationContextMenuItem = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.afText = undefined;
    this.afHref = undefined;
    this.afType = undefined;
    this.afLang = undefined;
    this.afDir = undefined;
  }
  componentWillLoad() {
    if (this.afType !== navigationContextMenuItemType_enum.NavigationContextMenuItemType.BUTTON &&
      this.afType !== navigationContextMenuItemType_enum.NavigationContextMenuItemType.LINK) {
      logger_util.logger.warn(`The navigation-context-menu-item is of unallowed afType. Only button and link is allowed.`, this.hostElement);
      return;
    }
  }
  get hostElement() { return index.getElement(this); }
};

exports.digi_navigation_context_menu_item = NavigationContextMenuItem;
