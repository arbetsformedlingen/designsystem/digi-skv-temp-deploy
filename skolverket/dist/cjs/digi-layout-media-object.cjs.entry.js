'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');
const layoutMediaObjectAlignment_enum = require('./layout-media-object-alignment.enum-e770b17d.js');

const layoutMediaObjectCss = ".sc-digi-layout-media-object-h{--digi--layout-media-object--gutter:var(--digi--gutter--largest);--digi--layout-media-object--alignment:flex-start;--digi--layout-media-object--flex-wrap:wrap}.sc-digi-layout-media-object-h .digi-layout-media-object.sc-digi-layout-media-object{display:flex;align-items:var(--digi--layout-media-object--alignment);gap:var(--digi--layout-media-object--gutter);flex-wrap:var(--digi--layout-media-object--flex-wrap)}.sc-digi-layout-media-object-h .digi-layout-media-object--alignment-center.sc-digi-layout-media-object{--digi--layout-media-object--alignment:center}.sc-digi-layout-media-object-h .digi-layout-media-object--alignment-end.sc-digi-layout-media-object{--digi--layout-media-object--alignment:flex-end}.sc-digi-layout-media-object-h .digi-layout-media-object--alignment-stretch.sc-digi-layout-media-object{--digi--layout-media-object--alignment:stretch}.digi-layout-media-object__last.sc-digi-layout-media-object{flex:1}";

const LayoutMediaObject = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.afAlignment = layoutMediaObjectAlignment_enum.LayoutMediaObjectAlignment.START;
  }
  get cssModifiers() {
    return {
      [`digi-layout-media-object--alignment-${this.afAlignment}`]: !!this.afAlignment
    };
  }
  render() {
    return (index.h("div", { class: Object.assign({ 'digi-layout-media-object': true }, this.cssModifiers) }, index.h("div", { class: "digi-layout-media-object__first" }, index.h("slot", { name: "media" })), index.h("div", { class: "digi-layout-media-object__last" }, index.h("slot", null))));
  }
};
LayoutMediaObject.style = layoutMediaObjectCss;

exports.digi_layout_media_object = LayoutMediaObject;
