'use strict';

exports.LayoutStackedBlocksVariation = void 0;
(function (LayoutStackedBlocksVariation) {
  LayoutStackedBlocksVariation["REGULAR"] = "regular";
  LayoutStackedBlocksVariation["ENHANCED"] = "enhanced";
})(exports.LayoutStackedBlocksVariation || (exports.LayoutStackedBlocksVariation = {}));
