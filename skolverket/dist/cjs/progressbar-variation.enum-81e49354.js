'use strict';

exports.ProgressbarRole = void 0;
(function (ProgressbarRole) {
  ProgressbarRole["NONE"] = "";
  ProgressbarRole["STATUS"] = "status";
})(exports.ProgressbarRole || (exports.ProgressbarRole = {}));

exports.ProgressbarVariation = void 0;
(function (ProgressbarVariation) {
  ProgressbarVariation["PRIMARY"] = "primary";
  ProgressbarVariation["SECONDARY"] = "secondary";
})(exports.ProgressbarVariation || (exports.ProgressbarVariation = {}));
