'use strict';

exports.MediaFigureAlignment = void 0;
(function (MediaFigureAlignment) {
  MediaFigureAlignment["START"] = "start";
  MediaFigureAlignment["CENTER"] = "center";
  MediaFigureAlignment["END"] = "end";
})(exports.MediaFigureAlignment || (exports.MediaFigureAlignment = {}));
