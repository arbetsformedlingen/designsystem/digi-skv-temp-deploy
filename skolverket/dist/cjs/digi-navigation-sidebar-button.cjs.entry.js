'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');
const randomIdGenerator_util = require('./randomIdGenerator.util-9002c72c.js');

const navigationSidebarButtonCss = ".sc-digi-navigation-sidebar-button-h{--digi--navigation-sidebar-button--display:inline-flex}.sc-digi-navigation-sidebar-button-h .digi-navigation-sidebar-button.sc-digi-navigation-sidebar-button{display:var(--digi--navigation-sidebar-button--display)}";

const NavigationSidebarButton = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.afOnToggle = index.createEvent(this, "afOnToggle", 7);
    this.isActive = false;
    this.afText = undefined;
    this.afAriaLabel = undefined;
    this.afId = randomIdGenerator_util.randomIdGenerator('digi-navigation-sidebar-button');
  }
  toggleHandler() {
    this.isActive = !this.isActive;
    this.afOnToggle.emit(this.isActive);
  }
  render() {
    return (index.h("digi-button", { id: this.afId, class: "digi-navigation-sidebar-button", "af-variation": "function", "af-aria-label": this.afAriaLabel, onClick: () => this.toggleHandler() }, this.afText, index.h("digi-icon", { slot: "icon", afName: `bars` })));
  }
};
NavigationSidebarButton.style = navigationSidebarButtonCss;

exports.digi_navigation_sidebar_button = NavigationSidebarButton;
