'use strict';

exports.PageFooterVariation = void 0;
(function (PageFooterVariation) {
  PageFooterVariation["PRIMARY"] = "primary";
  PageFooterVariation["SECONDARY"] = "secondary";
})(exports.PageFooterVariation || (exports.PageFooterVariation = {}));
