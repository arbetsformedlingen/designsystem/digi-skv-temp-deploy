'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');
const progressStepsVariation_enum = require('./progress-steps-variation.enum-3bbe5656.js');
const logger_util = require('./logger.util-a78a149e.js');

const progressStepsCss = ".sc-digi-progress-steps-h{display:block}";

const ProgressSteps = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.afHeadingLevel = progressStepsVariation_enum.ProgressStepsHeadingLevel.H2;
    this.afVariation = progressStepsVariation_enum.ProgressStepsVariation.PRIMARY;
    this.afCurrentStep = 1;
    this.steps = [];
  }
  async afMNext() {
    this.afCurrentStep += 1;
  }
  async afMPrevious() {
    this.afCurrentStep -= 1;
  }
  componentWillLoad() {
    this.getSteps();
    this.setStepProps();
  }
  componentWillUpdate() {
    this.getSteps();
    this.setStepProps();
  }
  getSteps() {
    let steps = this.hostElement.querySelectorAll('digi-progress-step');
    if (!steps || steps.length <= 0) {
      logger_util.logger.warn(`The slot contains no children elements.`, this.hostElement);
      return;
    }
    this.steps = [...Array.from(steps)].map((step) => {
      return {
        outerHTML: step.outerHTML,
        ref: step
      };
    });
  }
  setStepProps() {
    if (this.steps && this.afCurrentStep < 1) {
      logger_util.logger.warn(`Current step is set to ${this.afCurrentStep} which is not allowed.`, this.hostElement);
      this.afCurrentStep = 1;
      return;
    }
    if (this.steps && this.afCurrentStep > this.steps.length + 1) {
      logger_util.logger.warn(`Current step is set to ${this.afCurrentStep} which is more than the amount of available steps (${this.steps.length}).`, this.hostElement);
      this.afCurrentStep = this.steps.length + 1;
      return;
    }
    this.steps &&
      this.steps.forEach((step, index) => {
        step.ref.afVariation = this.afVariation;
        try {
          const topLevel = Number.parseInt(this.afHeadingLevel.split('h')[1]);
          step.ref.afHeadingLevel = 'h' + (topLevel);
          const isLast = index == this.steps.length - 1;
          step.ref.afIsLast = isLast;
          let status;
          if (index < this.afCurrentStep - 1) {
            status = progressStepsVariation_enum.ProgressStepsStatus.DONE;
          }
          else if (index == this.afCurrentStep - 1) {
            status = progressStepsVariation_enum.ProgressStepsStatus.CURRENT;
          }
          else {
            status = progressStepsVariation_enum.ProgressStepsStatus.UPCOMING;
          }
          step.ref.afStepStatus = status;
        }
        catch (e) {
          // Nothing
        }
      });
  }
  render() {
    return (index.h("div", { role: "list" }, index.h("digi-typography", null, index.h("slot", null))));
  }
  get hostElement() { return index.getElement(this); }
};
ProgressSteps.style = progressStepsCss;

exports.digi_progress_steps = ProgressSteps;
