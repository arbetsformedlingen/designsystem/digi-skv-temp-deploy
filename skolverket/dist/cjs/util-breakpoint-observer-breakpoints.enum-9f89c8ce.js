'use strict';

exports.UtilBreakpointObserverBreakpoints = void 0;
(function (UtilBreakpointObserverBreakpoints) {
  UtilBreakpointObserverBreakpoints["SMALL"] = "small";
  UtilBreakpointObserverBreakpoints["MEDIUM"] = "medium";
  UtilBreakpointObserverBreakpoints["LARGE"] = "large";
  UtilBreakpointObserverBreakpoints["XLARGE"] = "xlarge";
})(exports.UtilBreakpointObserverBreakpoints || (exports.UtilBreakpointObserverBreakpoints = {}));
