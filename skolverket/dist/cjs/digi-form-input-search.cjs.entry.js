'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');
const randomIdGenerator_util = require('./randomIdGenerator.util-9002c72c.js');
const formInputSearchVariation_enum = require('./form-input-search-variation.enum-5ea4a951.js');
const formInputType_enum = require('./form-input-type.enum-7f96d46d.js');
const buttonType_enum = require('./button-type.enum-322621ea.js');
const detectClickOutside_util = require('./detectClickOutside.util-5528b0c5.js');
const detectFocusOutside_util = require('./detectFocusOutside.util-a2222f93.js');
require('./detectClosest.util-8778dfa8.js');

const formInputSearchCss = ".sc-digi-form-input-search-h{width:100%}";

const FormInputSearch = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.afOnFocusOutside = index.createEvent(this, "afOnFocusOutside", 7);
    this.afOnFocusInside = index.createEvent(this, "afOnFocusInside", 7);
    this.afOnClickOutside = index.createEvent(this, "afOnClickOutside", 7);
    this.afOnClickInside = index.createEvent(this, "afOnClickInside", 7);
    this.afOnChange = index.createEvent(this, "afOnChange", 7);
    this.afOnBlur = index.createEvent(this, "afOnBlur", 7);
    this.afOnKeyup = index.createEvent(this, "afOnKeyup", 7);
    this.afOnFocus = index.createEvent(this, "afOnFocus", 7);
    this.afOnFocusout = index.createEvent(this, "afOnFocusout", 7);
    this.afOnInput = index.createEvent(this, "afOnInput", 7);
    this.afOnClick = index.createEvent(this, "afOnClick", 7);
    this.afLabel = undefined;
    this.afLabelDescription = undefined;
    this.afType = formInputType_enum.FormInputType.SEARCH;
    this.afButtonVariation = formInputType_enum.FormInputButtonVariation.PRIMARY;
    this.afName = undefined;
    this.afId = randomIdGenerator_util.randomIdGenerator('digi-form-input-search');
    this.value = undefined;
    this.afValue = undefined;
    this.afAutocomplete = undefined;
    this.afAriaAutocomplete = undefined;
    this.afAriaActivedescendant = undefined;
    this.afAriaLabelledby = undefined;
    this.afAriaDescribedby = undefined;
    this.afVariation = formInputSearchVariation_enum.FormInputSearchVariation.MEDIUM;
    this.afHideButton = undefined;
    this.afButtonType = buttonType_enum.ButtonType.SUBMIT;
    this.afButtonText = undefined;
    this.afButtonAriaLabel = undefined;
    this.afButtonAriaLabelledby = undefined;
    this.afAutofocus = undefined;
  }
  onValueChanged(value) {
    this.afValue = value;
  }
  onAfValueChanged(value) {
    this.value = value;
  }
  /**
   * Hämtar en referens till inputelementet. Bra för att t.ex. sätta fokus programmatiskt.
   * @en Returns a reference to the input element. Handy for setting focus programmatically.
   */
  async afMGetFormControlElement() {
    const formControlElement = await this._input.afMGetFormControlElement();
    return formControlElement;
  }
  blurHandler(e) {
    this.afOnBlur.emit(e);
  }
  changeHandler(e) {
    this.afValue = this.value = e.target.value;
    this.afOnChange.emit(e);
  }
  focusHandler(e) {
    this.afOnFocus.emit(e);
  }
  focusoutHandler(e) {
    this.afOnFocusout.emit(e);
  }
  keyupHandler(e) {
    this.afOnKeyup.emit(e);
  }
  inputHandler(e) {
    this.afOnInput.emit(e);
  }
  clickHandler(e) {
    this.afOnClick.emit(e);
  }
  clickOutsideListener(e) {
    const target = e.target;
    const selector = `[data-af-identifier="${this.afId}"]`;
    if (detectClickOutside_util.detectClickOutside(target, selector)) {
      this.afOnClickOutside.emit(e);
    }
    else {
      this.afOnClickInside.emit(e);
    }
  }
  focusOutsideListener(e) {
    const target = e.target;
    const selector = `[data-af-identifier="${this.afId}"]`;
    if (detectFocusOutside_util.detectFocusOutside(target, selector)) {
      this.afOnFocusOutside.emit(e);
    }
    else {
      this.afOnFocusInside.emit(e);
    }
  }
  componentWillLoad() {
    this.afValue ? (this.value = this.afValue) : (this.afValue = this.value);
  }
  get cssModifiers() {
    return {
      'digi-form-input-search--small': this.afVariation === formInputSearchVariation_enum.FormInputSearchVariation.SMALL,
      'digi-form-input-search--medium': this.afVariation === formInputSearchVariation_enum.FormInputSearchVariation.MEDIUM,
      'digi-form-input-search--large': this.afVariation === formInputSearchVariation_enum.FormInputSearchVariation.LARGE
    };
  }
  render() {
    return (index.h("div", { class: Object.assign({ 'digi-form-input-search': true }, this.cssModifiers), "data-af-identifier": this.afId }, index.h("digi-form-input", { class: {
        'digi-form-input-search__input': true,
        'digi-form-input-search__input--no-button': this.afHideButton
      }, ref: (el) => (this._input = el), onAfOnBlur: (e) => this.blurHandler(e), onAfOnChange: (e) => this.changeHandler(e), onAfOnFocus: (e) => this.focusHandler(e), onAfOnFocusout: (e) => this.focusoutHandler(e), onAfOnKeyup: (e) => this.keyupHandler(e), onAfOnInput: (e) => this.inputHandler(e), afLabel: this.afLabel, afLabelDescription: this.afLabelDescription, afAriaActivedescendant: this.afAriaActivedescendant, afAriaDescribedby: this.afAriaDescribedby, afAriaLabelledby: this.afAriaLabelledby, afAriaAutocomplete: this.afAriaAutocomplete, afAutocomplete: this.afAutocomplete, afName: this.afName, afType: this.afType, afValue: this.afValue, afVariation: this.afVariation, afAutofocus: this.afAutofocus ? this.afAutofocus : null, afButtonVariation: this.afButtonVariation }, !this.afHideButton && (index.h("digi-button", { class: "digi-form-input-search__button", onAfOnClick: (e) => this.clickHandler(e), afType: this.afButtonType, afAriaLabel: this.afButtonAriaLabel, afAriaLabelledby: this.afButtonAriaLabelledby, afSize: this.afVariation, slot: "button" }, index.h("digi-icon", { slot: "icon", afName: `search` }), this.afButtonText)))));
  }
  get hostElement() { return index.getElement(this); }
  static get watchers() { return {
    "value": ["onValueChanged"],
    "afValue": ["onAfValueChanged"]
  }; }
};
FormInputSearch.style = formInputSearchCss;

exports.digi_form_input_search = FormInputSearch;
