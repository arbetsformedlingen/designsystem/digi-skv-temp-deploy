'use strict';

exports.LayoutBlockContainer = void 0;
(function (LayoutBlockContainer) {
  LayoutBlockContainer["STATIC"] = "static";
  LayoutBlockContainer["FLUID"] = "fluid";
  LayoutBlockContainer["NONE"] = "none";
})(exports.LayoutBlockContainer || (exports.LayoutBlockContainer = {}));

exports.LayoutBlockVariation = void 0;
(function (LayoutBlockVariation) {
  LayoutBlockVariation["TRANSPARENT"] = "transparent";
  LayoutBlockVariation["PRIMARY"] = "primary";
  LayoutBlockVariation["SECONDARY"] = "secondary";
  LayoutBlockVariation["TERTIARY"] = "tertiary";
  LayoutBlockVariation["SYMBOL"] = "symbol";
  LayoutBlockVariation["PROFILE"] = "profile";
})(exports.LayoutBlockVariation || (exports.LayoutBlockVariation = {}));
