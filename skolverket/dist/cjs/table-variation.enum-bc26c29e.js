'use strict';

exports.TableVariation = void 0;
(function (TableVariation) {
  TableVariation["PRIMARY"] = "primary";
  TableVariation["SECONDARY"] = "secondary";
})(exports.TableVariation || (exports.TableVariation = {}));
