'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');
const randomIdGenerator_util = require('./randomIdGenerator.util-9002c72c.js');

const formFieldsetCss = ".sc-digi-form-fieldset-h{--digi--form-fieldset--padding:0;--digi--form-fieldset--border:none;--digi--form-fieldset--legend--font-weight:var(--digi--typography--description--font-weight--desktop);--digi--form-fieldset--legend--font-family:var(--digi--global--typography--font-family--default);--digi--form-fieldset--legend--font-size:var(--digi--typography--description--font-size--desktop);--digi--form-fieldset--legend--color:var(--digi--color--text--primary);--digi--form-fieldset--legend--margin:var(--digi--margin--h2-large)}.sc-digi-form-fieldset-h .digi-form-fieldset.sc-digi-form-fieldset{padding:var(--digi--form-fieldset--padding);border:var(--digi--form-fieldset--border)}.sc-digi-form-fieldset-h .digi-form-fieldset__legend.sc-digi-form-fieldset{font-weight:var(--digi--form-fieldset--legend--font-weight);font-family:var(--digi--form-fieldset--legend--font-family);font-size:var(--digi--form-fieldset--legend--font-size);color:var(--digi--form-fieldset--legend--color);-webkit-margin-after:var(--digi--form-fieldset--legend--margin);margin-block-end:var(--digi--form-fieldset--legend--margin)}";

const FormFieldset = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.afLegend = undefined;
    this.afName = undefined;
    this.afForm = undefined;
    this.afId = randomIdGenerator_util.randomIdGenerator('digi-form-fieldset');
  }
  render() {
    return (index.h("fieldset", { class: "digi-form-fieldset", name: this.afName, form: this.afForm, id: this.afId }, this.afLegend && (index.h("legend", { class: "digi-form-fieldset__legend" }, this.afLegend)), index.h("slot", null)));
  }
};
FormFieldset.style = formFieldsetCss;

exports.digi_form_fieldset = FormFieldset;
