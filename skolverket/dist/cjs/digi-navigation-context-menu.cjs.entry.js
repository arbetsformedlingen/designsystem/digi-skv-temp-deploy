'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');
const randomIdGenerator_util = require('./randomIdGenerator.util-9002c72c.js');
const logger_util = require('./logger.util-a78a149e.js');
const detectClickOutside_util = require('./detectClickOutside.util-5528b0c5.js');
const detectFocusOutside_util = require('./detectFocusOutside.util-a2222f93.js');
const navigationContextMenuItemType_enum = require('./navigation-context-menu-item-type.enum-105e809f.js');
require('./detectClosest.util-8778dfa8.js');

const navigationContextMenuCss = ".sc-digi-navigation-context-menu-h{--digi--navigation-context-menu--toggle-icon--transition:ease-in-out 0.2s all;--digi--navigation-context-menu--toggle-icon--transform:rotate(-180deg);--digi--navigation-context-menu--content--position:absolute;--digi--navigation-context-menu--content--background-color:var(--digi--color--background--primary);--digi--navigation-context-menu--content--border-radius:var(--digi--border-radius--secondary);--digi--navigation-context-menu--content--box-shadow:0 0.125rem 0.375rem 0 rgba(0, 0, 0, 0.7);--digi--navigation-context-menu--button--font-size:var(--digi--typography--body--font-size--desktop);--digi--navigation-context-menu--button--font-size--desktop:var(--digi--typography--body--font-size--desktop-large);--digi--navigation-context-menu--button--background:transparent;--digi--navigation-context-menu--button--background--hover:transparent;--digi--navigation-context-menu--button--color:var(--digi--color--text--primary);--digi--navigation-context-menu--button--border:0;--digi--navigation-context-menu--button--font-weight:normal;--digi--navigation-context-menu--button--padding:var(--digi--gutter--smallest-2) var(--digi--gutter--large) var(--digi--gutter--smallest-2) var(--digi--gutter--largest-3);--digi--navigation-context-menu--button--bordered--padding:var(--digi--gutter--large) var(--digi--gutter--large) var(--digi--gutter--large) var(--digi--gutter--largest-3);--digi--navigation-context-menu--button--outline--focus:none;--digi--navigation-context-menu--button--width:100%;--digi--navigation-context-menu--button--text-align:left;--digi--navigation-context-menu--button--display:flex;--digi--navigation-context-menu--button-icon--width:0.875rem;--digi--navigation-context-menu--button-icon--margin-right:-1.125rem;--digi--navigation-context-menu--button-icon--margin-left:0.25rem;--digi--navigation-context-menu--button-icon--display:inline-flex;--digi--navigation-context-menu--link--font-size:var(--digi--typography--body--font-size--desktop);--digi--navigation-context-menu--link--font-size--desktop:var(--digi--typography--body--font-size--desktop-large);--digi--navigation-context-menu--link--text-decoration:none;--digi--navigation-context-menu--link--color:var(--digi--color--text--primary);--digi--navigation-context-menu--link--font-family:var(--digi--global--typography--font-family--default);--digi--navigation-context-menu--link--focus--outline:none;--digi--navigation-context-menu--link--padding:var(--digi--gutter--smallest-2) var(--digi--gutter--large);--digi--navigation-context-menu--link--bordered--padding:var(--digi--gutter--large);--digi--navigation-context-menu--link--display:inline-block;--digi--navigation-context-menu--link--width:100%;--digi--navigation-context-menu--button-trigger--font-size:var(--digi--typography--body--font-size--desktop);--digi--navigation-context-menu--button-trigger--font-size--desktop:var(--digi--typography--body--font-size--desktop-large);--digi--navigation-context-menu--button-trigger--padding:0;--digi--navigation-context-menu--item--font-size:var(--digi--typography--body--font-size--desktop);--digi--navigation-context-menu--item--font-size--desktop:var(--digi--typography--body--font-size--desktop-large);--digi--navigation-context-menu--item--background-color--active:var(--digi--color--background--neutral-6);--digi--navigation-context-menu--item--display:flex;--digi--navigation-context-menu--item--font-family:var(--digi--global--typography--font-family--default);--digi--navigation-context-menu--item--font-wight--selected:600;--digi--navigation-context-menu--item--border-top:var(--digi--border-width--primary) solid var(--digi--color--border--neutral-4);--digi--navigation-context-menu--items--margin:0;--digi--navigation-context-menu--items--padding:var(--digi--gutter--large) 0}.sc-digi-navigation-context-menu-h .digi-navigation-context-menu__toggle-icon.sc-digi-navigation-context-menu{transition:var(--digi--navigation-context-menu--toggle-icon--transition)}.sc-digi-navigation-context-menu-h .digi-navigation-context-menu--active.sc-digi-navigation-context-menu .digi-navigation-context-menu__toggle-icon.sc-digi-navigation-context-menu{transform:var(--digi--navigation-context-menu--toggle-icon--transform)}.sc-digi-navigation-context-menu-h .digi-navigation-context-menu__trigger-button.sc-digi-navigation-context-menu{--digi--button--padding--medium:var(--digi--navigation-context-menu--button-trigger--padding);--digi--button--font-size--medium:var(--digi--navigation-context-menu--button-trigger--font-size)}@media (min-width: 62rem){.sc-digi-navigation-context-menu-h .digi-navigation-context-menu__trigger-button.sc-digi-navigation-context-menu{--digi--navigation-context-menu--button-trigger--font-size:var(--digi--navigation-context-menu--button-trigger--font-size--desktop)}}.sc-digi-navigation-context-menu-h .digi-navigation-context-menu__content.sc-digi-navigation-context-menu{position:var(--digi--navigation-context-menu--content--position);background-color:var(--digi--navigation-context-menu--content--background-color);border-radius:var(--digi--navigation-context-menu--content--border-radius);box-shadow:var(--digi--navigation-context-menu--content--box-shadow)}.sc-digi-navigation-context-menu-h .digi-navigation-context-menu__item.sc-digi-navigation-context-menu{font-size:var(--digi--navigation-context-menu--item--font-size);display:var(--digi--navigation-context-menu--item--display);font-family:var(--digi--navigation-context-menu--item--font-family)}@media (min-width: 62rem){.sc-digi-navigation-context-menu-h .digi-navigation-context-menu__item.sc-digi-navigation-context-menu{--digi--navigation-context-menu--item--font-size:var(--digi--navigation-context-menu--item--font-size--desktop)}}.sc-digi-navigation-context-menu-h .digi-navigation-context-menu__item.sc-digi-navigation-context-menu:hover{background-color:var(--digi--navigation-context-menu--item--background-color--active)}.sc-digi-navigation-context-menu-h .digi-navigation-context-menu__item--active.sc-digi-navigation-context-menu{background-color:var(--digi--navigation-context-menu--item--background-color--active)}.sc-digi-navigation-context-menu-h .digi-navigation-context-menu__item--selected.sc-digi-navigation-context-menu{--digi--button--font-weight--medium:var(--digi--navigation-context-menu--item--font-wight--selected);background-color:var(--digi--navigation-context-menu--item--background-color--active)}.sc-digi-navigation-context-menu-h .digi-navigation-context-menu__button.sc-digi-navigation-context-menu{--digi--button--font-size--medium:var(--digi--navigation-context-menu--button--font-size);--digi--button--color--background--primary--default:var(--digi--navigation-context-menu--button--background);--digi--button--color--background--primary--focus:var(--digi--navigation-context-menu--button--background);--digi--button--color--text--primary--default:var(--digi--navigation-context-menu--button--color);--digi--button--color--text--primary--hover:var(--digi--navigation-context-menu--button--color);--digi--button--border-width--primary--default:var(--digi--navigation-context-menu--button--border);--digi--button--border-width--primary--hover:var(--digi--navigation-context-menu--button--border);--digi--button--font-weight:var(--digi--navigation-context-menu--button--font-weight);--digi--button--padding--medium:var(--digi--navigation-context-menu--button--padding);--digi--button--color--background--primary--hover:var(--digi--navigation-context-menu--button--background--hover);--digi--button--outline--focus:var(--digi--navigation-context-menu--button--outline--focus);--digi--button--width:var(--digi--navigation-context-menu--button--width);--digi--button--text-align:var(--digi--navigation-context-menu--button--text-align);--digi--button--display:var(--digi--navigation-context-menu--button--display);flex:1 0 0}@media (min-width: 62rem){.sc-digi-navigation-context-menu-h .digi-navigation-context-menu__button.sc-digi-navigation-context-menu{--digi--navigation-context-menu--button--font-size:var(--digi--navigation-context-menu--button--font-size--desktop)}}.sc-digi-navigation-context-menu-h .digi-navigation-context-menu__button-icon.sc-digi-navigation-context-menu{margin-right:var(--digi--navigation-context-menu--button-icon--margin-right);margin-left:var(--digi--navigation-context-menu--button-icon--margin-left);display:var(--digi--navigation-context-menu--button-icon--display);--digi--icon--width:var(--digi--navigation-context-menu--button-icon--width)}.sc-digi-navigation-context-menu-h .digi-navigation-context-menu__link.sc-digi-navigation-context-menu{font-size:var(--digi--navigation-context-menu--link--font-size);-webkit-text-decoration:var(--digi--navigation-context-menu--link--text-decoration);text-decoration:var(--digi--navigation-context-menu--link--text-decoration);color:var(--digi--navigation-context-menu--link--color);font-family:var(--digi--navigation-context-menu--link--font-family);padding:var(--digi--navigation-context-menu--link--padding);display:var(--digi--navigation-context-menu--link--display);box-sizing:border-box;width:var(--digi--navigation-context-menu--link--width)}@media (min-width: 62rem){.sc-digi-navigation-context-menu-h .digi-navigation-context-menu__link.sc-digi-navigation-context-menu{--digi--navigation-context-menu--link--font-size:var(--digi--navigation-context-menu--link--font-size--desktop)}}.sc-digi-navigation-context-menu-h .digi-navigation-context-menu__link.sc-digi-navigation-context-menu:focus{outline:var(--digi--navigation-context-menu--link--focus--outline)}.sc-digi-navigation-context-menu-h .digi-navigation-context-menu__items.sc-digi-navigation-context-menu{list-style:none;margin:var(--digi--navigation-context-menu--items--margin);padding:var(--digi--navigation-context-menu--items--padding)}.sc-digi-navigation-context-menu-h .digi-navigation-context-menu__items--bordered.sc-digi-navigation-context-menu{--digi--navigation-context-menu--button--padding:var(--digi--navigation-context-menu--button--bordered--padding)}.sc-digi-navigation-context-menu-h .digi-navigation-context-menu__items--bordered.sc-digi-navigation-context-menu .digi-navigation-context-menu__link.sc-digi-navigation-context-menu{padding:var(--digi--navigation-context-menu--link--bordered--padding)}.sc-digi-navigation-context-menu-h .digi-navigation-context-menu__items--bordered.sc-digi-navigation-context-menu .digi-navigation-context-menu__item.sc-digi-navigation-context-menu:not(:first-child){border-top:var(--digi--navigation-context-menu--item--border-top)}";

const NavigationContextMenu = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.afOnInactive = index.createEvent(this, "afOnInactive", 7);
    this.afOnActive = index.createEvent(this, "afOnActive", 7);
    this.afOnBlur = index.createEvent(this, "afOnBlur", 7);
    this.afOnChange = index.createEvent(this, "afOnChange", 7);
    this.afOnToggle = index.createEvent(this, "afOnToggle", 7);
    this.afOnSelect = index.createEvent(this, "afOnSelect", 7);
    this.listItems = [];
    this.selectedListItemIndex = 0;
    this.isActive = false;
    this.activeListItemIndex = 1;
    this.afText = undefined;
    this.afId = randomIdGenerator_util.randomIdGenerator('digi-navigation-context-menu');
    this.afStartSelected = 1;
    this.afNavigationContextMenuItems = undefined;
    this._afNavigationContextMenuItems = undefined;
    this.afIcon = undefined;
  }
  clickHandler(e) {
    const target = e.target;
    const selector = `#${this.afId}-identifier`;
    if (detectClickOutside_util.detectClickOutside(target, selector) && this.isActive) {
      this.setInactive();
    }
  }
  focusoutHandler(e) {
    const target = e.target;
    const selector = `#${this.afId}-identifier`;
    if (detectFocusOutside_util.detectFocusOutside(target, selector) && this.isActive) {
      this.setInactive();
      this.afOnBlur.emit(e);
    }
  }
  setComponentTag() {
    this._afNavigationContextMenuItems = this.afNavigationContextMenuItems;
    this.generateListItems(this.hostElement.children);
  }
  generateListItems(collection) {
    this.listItems = [];
    let slotChildren = Array.from(collection).filter((item) => item.getAttribute('slot') === null &&
      !item.classList.contains('digi-navigation-context-menu'));
    this.digiIcon =
      this.afIcon !== undefined ? 'digi-icon-' + this.afIcon : undefined;
    slotChildren.map((item, i) => {
      this.listItems.push({
        index: i + 1,
        type: item['afType'],
        text: item['afText'],
        value: item.getAttribute('data-value'),
        href: item['afHref'],
        dir: item['afDir'],
        lang: item['afLang']
      });
      item.outerHTML = '';
    });
    if (this.listItems.length === 0) {
      if (this._afNavigationContextMenuItems) {
        try {
          if (this._afNavigationContextMenuItems.constructor === Array) {
            this.listItems = this._afNavigationContextMenuItems;
          }
          else if (typeof this._afNavigationContextMenuItems === 'string') {
            this.listItems = JSON.parse(this._afNavigationContextMenuItems);
          }
          else {
            throw `Invalid type in "navigation-context-menu-items" attribute`;
          }
          /**
           * Quick helper to set correct type depending on if value or href is used.
           */
          const getType = (item) => {
            if (item.type)
              return item.type;
            if (item.href)
              return navigationContextMenuItemType_enum.NavigationContextMenuItemType.LINK;
            if (item.value)
              return navigationContextMenuItemType_enum.NavigationContextMenuItemType.BUTTON;
          };
          this.listItems.map((item, i) => {
            item.index = i + 1;
            item.type = getType(item);
          });
        }
        catch (e) {
          logger_util.logger.warn(`Invalid JSON in "navigation-context-menu-items" attribute`, this.hostElement, e);
          return;
        }
      }
    }
    if (this.listItems.length === 0) {
      logger_util.logger.warn(`The slot contains no items or array items.`, this.hostElement);
      return;
    }
    this.setActiveListItemIndex();
    this.selectedListItemIndex = this.afStartSelected
      ? this.afStartSelected
      : this.activeListItemIndex;
  }
  componentWillLoad() {
    this._afNavigationContextMenuItems = this.afNavigationContextMenuItems;
    this.generateListItems(this.hostElement.children);
  }
  debounce(func, timeout = 300) {
    let timer;
    return (...args) => {
      clearTimeout(timer);
      timer = setTimeout(() => {
        func.apply(this, args);
      }, timeout);
    };
  }
  setInactive(focusTrigger = false) {
    this.isActive = false;
    this._toggleButton.querySelector('button').setAttribute('tabindex', null);
    //this.setActiveListItemIndex();
    this.afOnInactive.emit();
    if (focusTrigger) {
      this._toggleButton.querySelector('button').focus();
    }
  }
  setActiveListItemIndex() {
    if (this.afStartSelected) {
      this.activeListItemIndex = this.afStartSelected;
    }
    else {
      this.activeListItemIndex = this.activeListItemIndex;
    }
  }
  setActive() {
    this.isActive = true;
    this._toggleButton.querySelector('button').setAttribute('tabindex', -1);
    setTimeout(() => {
      this.focusActiveItem();
    }, 100);
    this.afOnActive.emit();
  }
  toggleMenu(e) {
    const toggle = this.isActive ? this.setInactive() : this.setActive();
    this.afOnToggle.emit(e);
    return toggle;
  }
  focusActiveItem() {
    const li = document.querySelector(`.digi-navigation-context-menu__item:nth-child(${this.activeListItemIndex})`);
    const findElement = Array.from(li.children).find((el) => el.getAttribute('data-type'));
    const elementType = findElement.getAttribute('data-type');
    if (findElement) {
      let el = elementType === 'button'
        ? findElement.querySelector(elementType)
        : findElement;
      el.focus();
      this.afOnChange.emit(findElement);
    }
  }
  homeHandler() {
    this.activeListItemIndex = 1;
    this.focusActiveItem();
  }
  endHandler() {
    this.activeListItemIndex = this.listItems.length;
    this.focusActiveItem();
  }
  decrementActiveItem() {
    if (this.activeListItemIndex > 1) {
      this.activeListItemIndex = this.activeListItemIndex - 1;
      this.focusActiveItem();
    }
  }
  incrementActiveItem() {
    if (this.activeListItemIndex <= this.listItems.length - 1) {
      this.activeListItemIndex = Number(this.activeListItemIndex) + 1;
      this.focusActiveItem();
    }
  }
  upHandler() {
    this.decrementActiveItem();
  }
  downHandler() {
    this.incrementActiveItem();
  }
  itemClickHandler(e, select = false) {
    if (select) {
      this.selectedListItemIndex = e.target
        .closest('.digi-navigation-context-menu__item')
        .getAttribute('data-key');
      let buttonValue = e.target
        .closest('.digi-navigation-context-menu__button')
        .getAttribute('data-value');
      this.activeListItemIndex = this.selectedListItemIndex;
      let eventvalue = buttonValue ? buttonValue : e.target.textContent;
      this.handleSelect(eventvalue);
    }
    this.focusActiveItem();
    this.setInactive(true);
  }
  handleSelect(value) {
    this.afOnSelect.emit(value);
  }
  showCheckIcon(i, type) {
    return i === Number(this.selectedListItemIndex) && type === 'button'
      ? true
      : false;
  }
  get cssModifiers() {
    return {
      'digi-navigation-context-menu--active': this.isActive
    };
  }
  render() {
    return (index.h("div", { class: "digi-navigation-context-menu", id: `${this.afId}-identifier` }, index.h("digi-util-keydown-handler", { onAfOnEsc: () => this.setInactive(true) }, index.h("digi-button", { ref: (el) => (this._toggleButton = el), onClick: (e) => this.toggleMenu(e), id: `${this.afId}-trigger`, "af-variation": "function", "aria-controls": this.afId, "aria-expanded": this.isActive || null, "aria-haspopup": true, class: Object.assign({ 'digi-navigation-context-menu__trigger-button': true }, this.cssModifiers) }, index.h(this.digiIcon, { slot: "icon" }), this.afText, index.h("digi-icon", { class: "digi-navigation-context-menu__toggle-icon", slot: "icon-secondary", afName: `chevron-down` })), index.h("div", { class: "digi-navigation-context-menu__content", hidden: !this.isActive }, index.h("digi-util-keyup-handler", { onAfOnHome: () => this.homeHandler(), onAfOnEnd: () => this.endHandler(), onAfOnUp: () => this.upHandler(), onAfOnDown: () => this.downHandler(), onAfOnTab: () => this.setInactive(), onAfOnShiftTab: () => this.setInactive() }, index.h("ul", { id: this.afId, class: {
        'digi-navigation-context-menu__items': true,
        'digi-navigation-context-menu__items--bordered': this.listItems.length > 4
      }, role: "menu", "aria-describedby": `${this.afId}-trigger` }, this.listItems.map((item) => {
      return (index.h("li", { role: "none", tabindex: "-1", class: {
          'digi-navigation-context-menu__item': true,
          'digi-navigation-context-menu__item--active': Number(item.index) === Number(this.activeListItemIndex),
          'digi-navigation-context-menu__item--selected': Number(item.index) === Number(this.selectedListItemIndex)
        }, key: item.index, "data-key": item.index }, this.showCheckIcon(item.index, item.type) && (index.h("digi-icon", { class: "digi-navigation-context-menu__button-icon", slot: "icon", afName: `check` })), item.type === 'button' && (index.h("digi-button", { "af-tabindex": "-1", role: "menuitem", "data-type": item.type, "data-value": item.value, class: "digi-navigation-context-menu__button", afDir: item.dir, afLang: item.lang, onClick: (e) => this.itemClickHandler(e, true) }, item.text)), item.type === 'link' && (index.h("a", { tabindex: "-1", role: "menuitem", class: "digi-navigation-context-menu__link", href: item.href, "data-type": item.type, onClick: (e) => this.itemClickHandler(e), dir: item.dir, lang: item.lang }, item.text))));
    }))))), index.h("digi-util-mutation-observer", { hidden: true, ref: (el) => (this._observer = el), onAfOnChange: this.debounce(() => {
        this.generateListItems(this._observer.children);
      }) }, index.h("slot", null))));
  }
  get hostElement() { return index.getElement(this); }
  static get watchers() { return {
    "afNavigationContextMenuItems": ["setComponentTag"]
  }; }
};
NavigationContextMenu.style = navigationContextMenuCss;

exports.digi_navigation_context_menu = NavigationContextMenu;
