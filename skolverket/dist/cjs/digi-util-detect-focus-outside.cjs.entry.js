'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');
const randomIdGenerator_util = require('./randomIdGenerator.util-9002c72c.js');
const detectFocusOutside_util = require('./detectFocusOutside.util-a2222f93.js');
require('./detectClosest.util-8778dfa8.js');

const UtilDetectFocusOutside = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.afOnFocusOutside = index.createEvent(this, "afOnFocusOutside", 7);
    this.afOnFocusInside = index.createEvent(this, "afOnFocusInside", 7);
    this.afDataIdentifier = randomIdGenerator_util.randomIdGenerator('data-digi-util-detect-focus-outside');
    this.$el.setAttribute(this.afDataIdentifier, '');
  }
  focusinHandler(e) {
    const target = e.target;
    const selector = `[${this.afDataIdentifier}]`;
    if (detectFocusOutside_util.detectFocusOutside(target, selector)) {
      this.afOnFocusOutside.emit(e);
    }
    else {
      this.afOnFocusInside.emit(e);
    }
  }
  render() {
    return index.h("slot", null);
  }
  get $el() { return index.getElement(this); }
};

exports.digi_util_detect_focus_outside = UtilDetectFocusOutside;
