'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');
const cardBoxWidth_enum = require('./card-box-width.enum-1a09a997.js');
const cardBoxGutter_enum = require('./card-box-gutter.enum-585dded6.js');

const cardBoxCss = ".sc-digi-card-box-h{--digi--card-box--padding--inline:var(--digi--responsive-grid-gutter--outer);--digi--card-box--padding--block:var(--digi--padding--largest)}@media (min-width: 48rem){.sc-digi-card-box-h{--digi--card-box--padding--block:var(--digi--card-box--padding--inline)}}@media (max-width: 47.9375rem){digi-layout-grid .sc-digi-card-box-h{grid-column:1/-1}}.sc-digi-card-box-h .digi-card-box.sc-digi-card-box{box-shadow:0px 8px 16px 0px rgba(105, 40, 89, 0.2);padding:var(--digi--card-box--padding--block) var(--digi--card-box--padding--inline);-webkit-border-before:var(--digi--border-width--secondary) solid var(--digi--color--border--secondary);border-block-start:var(--digi--border-width--secondary) solid var(--digi--color--border--secondary);border-radius:var(--digi--border-radius--primary);background:var(--digi--color--background--primary)}@media (max-width: 47.9375rem){digi-layout-container .sc-digi-card-box-h .digi-card-box.sc-digi-card-box{border-radius:0}digi-layout-container .sc-digi-card-box-h .digi-card-box--width-regular.sc-digi-card-box{width:100vw;-webkit-margin-start:calc(var(--digi--card-box--padding--inline) * -1);margin-inline-start:calc(var(--digi--card-box--padding--inline) * -1)}}.sc-digi-card-box-h .digi-card-box--gutter-none.sc-digi-card-box{--digi--card-box--padding--inline:0;--digi--card-box--padding--block:0}.sc-digi-card-box-h .digi-card-box--width-full.sc-digi-card-box{width:calc(100% + var(--digi--layout-container--gutter, 0) * 2)}digi-layout-container .sc-digi-card-box-h .digi-card-box--width-full.sc-digi-card-box{-webkit-margin-start:calc(var(--digi--layout-container--gutter, 0) * -1);margin-inline-start:calc(var(--digi--layout-container--gutter, 0) * -1)}";

const CardBox = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.afWidth = cardBoxWidth_enum.CardBoxWidth.REGULAR;
    this.afGutter = cardBoxGutter_enum.CardBoxGutter.REGULAR;
  }
  get cssModifiers() {
    return {
      [`digi-card-box--width-${this.afWidth}`]: !!this.afWidth,
      [`digi-card-box--gutter-${this.afGutter}`]: !!this.afGutter
    };
  }
  render() {
    return (index.h("div", { class: Object.assign({ 'digi-card-box': true }, this.cssModifiers) }, index.h("slot", null)));
  }
};
CardBox.style = cardBoxCss;

exports.digi_card_box = CardBox;
