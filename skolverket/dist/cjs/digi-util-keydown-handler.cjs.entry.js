'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');
const keyboardHandler_util = require('./keyboardHandler.util-180fce7d.js');

const UtilKeydownHandler = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.afOnEsc = index.createEvent(this, "afOnEsc", 7);
    this.afOnEnter = index.createEvent(this, "afOnEnter", 7);
    this.afOnTab = index.createEvent(this, "afOnTab", 7);
    this.afOnSpace = index.createEvent(this, "afOnSpace", 7);
    this.afOnShiftTab = index.createEvent(this, "afOnShiftTab", 7);
    this.afOnUp = index.createEvent(this, "afOnUp", 7);
    this.afOnDown = index.createEvent(this, "afOnDown", 7);
    this.afOnLeft = index.createEvent(this, "afOnLeft", 7);
    this.afOnRight = index.createEvent(this, "afOnRight", 7);
    this.afOnHome = index.createEvent(this, "afOnHome", 7);
    this.afOnEnd = index.createEvent(this, "afOnEnd", 7);
    this.afOnKeyDown = index.createEvent(this, "afOnKeyDown", 7);
  }
  keydownHandler(e) {
    const key = keyboardHandler_util.keyboardHandler(e);
    switch (key) {
      case keyboardHandler_util.KEY_CODE.SHIFT_TAB:
        this.afOnShiftTab.emit(e);
      case keyboardHandler_util.KEY_CODE.DOWN_ARROW:
        this.afOnDown.emit(e);
        this.afOnKeyDown.emit(e);
        break;
      case keyboardHandler_util.KEY_CODE.UP_ARROW:
        this.afOnUp.emit(e);
        this.afOnKeyDown.emit(e);
        break;
      case keyboardHandler_util.KEY_CODE.LEFT_ARROW:
        this.afOnLeft.emit(e);
        this.afOnKeyDown.emit(e);
        break;
      case keyboardHandler_util.KEY_CODE.RIGHT_ARROW:
        this.afOnRight.emit(e);
        this.afOnKeyDown.emit(e);
        break;
      case keyboardHandler_util.KEY_CODE.ENTER:
        this.afOnEnter.emit(e);
        this.afOnKeyDown.emit(e);
        break;
      case keyboardHandler_util.KEY_CODE.ESCAPE:
        this.afOnEsc.emit(e);
        this.afOnKeyDown.emit(e);
        break;
      case keyboardHandler_util.KEY_CODE.TAB:
        this.afOnTab.emit(e);
        this.afOnKeyDown.emit(e);
        break;
      case keyboardHandler_util.KEY_CODE.SPACE:
        this.afOnSpace.emit(e);
        this.afOnKeyDown.emit(e);
        break;
      case keyboardHandler_util.KEY_CODE.HOME:
        this.afOnHome.emit(e);
        this.afOnKeyDown.emit(e);
        break;
      case keyboardHandler_util.KEY_CODE.END:
        this.afOnEnd.emit(e);
        this.afOnKeyDown.emit(e);
        break;
      case keyboardHandler_util.KEY_CODE.ANY:
        this.afOnKeyDown.emit(e);
        break;
      default:
        this.afOnKeyDown.emit(e);
        return;
    }
  }
  render() {
    return index.h("slot", null);
  }
};

exports.digi_util_keydown_handler = UtilKeydownHandler;
