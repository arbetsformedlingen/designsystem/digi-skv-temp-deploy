'use strict';

function detectClosest(target, selector) {
  return !!target.closest(selector);
}

exports.detectClosest = detectClosest;
