'use strict';

exports.LinkVariation = void 0;
(function (LinkVariation) {
  LinkVariation["SMALL"] = "small";
  LinkVariation["LARGE"] = "large";
})(exports.LinkVariation || (exports.LinkVariation = {}));
