'use strict';

exports.ButtonSize = void 0;
(function (ButtonSize) {
  ButtonSize["SMALL"] = "small";
  ButtonSize["MEDIUM"] = "medium";
  ButtonSize["LARGE"] = "large";
})(exports.ButtonSize || (exports.ButtonSize = {}));
