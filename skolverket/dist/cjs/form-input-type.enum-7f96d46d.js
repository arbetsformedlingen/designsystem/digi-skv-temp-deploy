'use strict';

exports.FormInputButtonVariation = void 0;
(function (FormInputButtonVariation) {
  FormInputButtonVariation["PRIMARY"] = "primary";
  FormInputButtonVariation["SECONDARY"] = "secondary";
})(exports.FormInputButtonVariation || (exports.FormInputButtonVariation = {}));

exports.FormInputType = void 0;
(function (FormInputType) {
  FormInputType["COLOR"] = "color";
  FormInputType["DATE"] = "date";
  FormInputType["DATETIME_LOCAL"] = "datetime-local";
  FormInputType["EMAIL"] = "email";
  FormInputType["HIDDEN"] = "hidden";
  FormInputType["MONTH"] = "month";
  FormInputType["NUMBER"] = "number";
  FormInputType["PASSWORD"] = "password";
  FormInputType["SEARCH"] = "search";
  FormInputType["TEL"] = "tel";
  FormInputType["TEXT"] = "text";
  FormInputType["TIME"] = "time";
  FormInputType["URL"] = "url";
  FormInputType["WEEK"] = "week";
})(exports.FormInputType || (exports.FormInputType = {}));
