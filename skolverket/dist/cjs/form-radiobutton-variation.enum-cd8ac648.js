'use strict';

exports.CodeBlockLanguage = void 0;
(function (CodeBlockLanguage) {
  CodeBlockLanguage["JSON"] = "json";
  CodeBlockLanguage["CSS"] = "css";
  CodeBlockLanguage["SCSS"] = "scss";
  CodeBlockLanguage["TYPESCRIPT"] = "typescript";
  CodeBlockLanguage["JAVASCRIPT"] = "javascript";
  CodeBlockLanguage["BASH"] = "bash";
  CodeBlockLanguage["HTML"] = "html";
  CodeBlockLanguage["GIT"] = "git";
  CodeBlockLanguage["JSX"] = "jsx";
  CodeBlockLanguage["TSX"] = "tsx";
})(exports.CodeBlockLanguage || (exports.CodeBlockLanguage = {}));

exports.FormRadiobuttonLayout = void 0;
(function (FormRadiobuttonLayout) {
  FormRadiobuttonLayout["INLINE"] = "inline";
  FormRadiobuttonLayout["BLOCK"] = "block";
})(exports.FormRadiobuttonLayout || (exports.FormRadiobuttonLayout = {}));

exports.FormRadiobuttonValidation = void 0;
(function (FormRadiobuttonValidation) {
  FormRadiobuttonValidation["ERROR"] = "error";
})(exports.FormRadiobuttonValidation || (exports.FormRadiobuttonValidation = {}));

exports.FormRadiobuttonVariation = void 0;
(function (FormRadiobuttonVariation) {
  FormRadiobuttonVariation["PRIMARY"] = "primary";
  FormRadiobuttonVariation["SECONDARY"] = "secondary";
})(exports.FormRadiobuttonVariation || (exports.FormRadiobuttonVariation = {}));
