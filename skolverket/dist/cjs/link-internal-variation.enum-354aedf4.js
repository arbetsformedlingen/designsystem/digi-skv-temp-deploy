'use strict';

exports.LinkInternalVariation = void 0;
(function (LinkInternalVariation) {
  LinkInternalVariation["SMALL"] = "small";
  LinkInternalVariation["LARGE"] = "large";
})(exports.LinkInternalVariation || (exports.LinkInternalVariation = {}));
