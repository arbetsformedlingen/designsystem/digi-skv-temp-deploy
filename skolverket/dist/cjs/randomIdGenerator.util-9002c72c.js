'use strict';

function randomIdGenerator(prefix = 'digi') {
  return `${prefix}-${Math.random()
    .toString(36)
    .substring(2, 7)}`;
}

exports.randomIdGenerator = randomIdGenerator;
