'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');
require('./button-size.enum-a46b7684.js');
const buttonType_enum = require('./button-type.enum-322621ea.js');
const buttonVariation_enum = require('./button-variation.enum-9364587a.js');
const expandableAccordionVariation_enum = require('./expandable-accordion-variation.enum-2e394d6b.js');
require('./calendar-week-view-heading-level.enum-696abd05.js');
require('./form-radiobutton-variation.enum-cd8ac648.js');
require('./code-block-variation.enum-a28057da.js');
require('./code-example-variation.enum-a954aaf0.js');
require('./code-variation.enum-4c625938.js');
require('./form-checkbox-variation.enum-04455f3e.js');
require('./form-file-upload-variation.enum-26e4a9bd.js');
require('./form-input-search-variation.enum-5ea4a951.js');
require('./form-input-type.enum-7f96d46d.js');
require('./form-input-variation.enum-f95a25ea.js');
require('./form-select-variation.enum-139eab4f.js');
require('./form-textarea-variation.enum-8f1a5f71.js');
require('./form-validation-message-variation.enum-9244655f.js');
require('./layout-block-variation.enum-ae7e0e5f.js');
require('./layout-columns-variation.enum-9b7242b1.js');
require('./layout-container-variation.enum-7af59e50.js');
require('./layout-media-object-alignment.enum-e770b17d.js');
require('./link-external-variation.enum-864f5ac7.js');
require('./link-internal-variation.enum-354aedf4.js');
require('./link-variation.enum-55cb3944.js');
require('./loader-spinner-size.enum-96d3508e.js');
require('./media-figure-alignment.enum-5327a385.js');
require('./navigation-context-menu-item-type.enum-105e809f.js');
require('./navigation-sidebar-variation.enum-fa604f57.js');
require('./navigation-vertical-menu-variation.enum-40269ed5.js');
require('./progress-step-variation.enum-596ab007.js');
require('./progress-steps-variation.enum-3bbe5656.js');
require('./progressbar-variation.enum-81e49354.js');
require('./tag-size.enum-774d54e2.js');
require('./typography-meta-variation.enum-c7469a03.js');
require('./typography-time-variation.enum-4526d4ae.js');
require('./typography-variation.enum-08e58c63.js');
require('./util-breakpoint-observer-breakpoints.enum-9f89c8ce.js');
const randomIdGenerator_util = require('./randomIdGenerator.util-9002c72c.js');

const expandableAccordionCss = ".sc-digi-expandable-accordion-h{--digi--expandable-accordion--header--toggle-icon--transition:ease-in-out var(--digi--animation--duration--base) all;--digi--expandable-accordion--header--font-size:var(--digi--typography--accordion--font-size--desktop);--digi--expandable-accordion--header--font-size--small:var(--digi--typography--accordion--font-size--mobile);--digi--expandable-accordion--header--font-weight:var(--digi--typography--accordion--font-weight--desktop);--digi--expandable-accordion--icon--size:0.875rem;--digi--expandable-accordion--icon--margin-right:var(--digi--margin--medium);--digi--expandable-accordion--border-bottom-width:var(--digi--border-width--secondary);--digi--expandable-accordion--border-bottom-color:var(--digi--color--border--informative);--digi--expandable-accordion--content--padding:var(--digi--padding--medium) 0;--digi--expandable-accordion--content--transition:ease-in-out var(--digi--animation--duration--base) height;display:block}.sc-digi-expandable-accordion-h .digi-expandable-accordion.sc-digi-expandable-accordion::after{content:\"\";display:block;max-width:var(--digi--paragraph-width--medium);border-bottom-width:var(--digi--expandable-accordion--border-bottom-width);border-bottom-color:var(--digi--expandable-accordion--border-bottom-color);border-bottom-style:solid;margin-top:calc(var(--digi--expandable-accordion--border-bottom-width) * -1)}.sc-digi-expandable-accordion-h .digi-expandable-accordion--variation-secondary.sc-digi-expandable-accordion::after{content:none}.digi-expandable-accordion__header.sc-digi-expandable-accordion{display:flex;align-items:center}.digi-expandable-accordion--variation-primary.sc-digi-expandable-accordion .digi-expandable-accordion__header.sc-digi-expandable-accordion{max-width:var(--digi--paragraph-width--medium);border-bottom-width:var(--digi--expandable-accordion--border-bottom-width);border-bottom-color:var(--digi--expandable-accordion--border-bottom-color);border-bottom-style:solid}.digi-expandable-accordion__toggle-icon.sc-digi-expandable-accordion{transition:var(--digi--expandable-accordion--header--toggle-icon--transition)}.digi-expandable-accordion--expanded-true.sc-digi-expandable-accordion .digi-expandable-accordion__toggle-icon.sc-digi-expandable-accordion{transform:rotate(-180deg)}.digi-expandable-accordion__heading.sc-digi-expandable-accordion{margin:0;flex:1}.digi-expandable-accordion__button.sc-digi-expandable-accordion{font-family:var(--digi--global--typography--font-family--default);font-size:var(--digi--expandable-accordion--header--font-size--small);font-weight:var(--digi--expandable-accordion--header--font-weight);width:100%;display:flex;gap:var(--digi--expandable-accordion--icon--margin-right);align-items:center;text-align:start;cursor:pointer}@media (min-width: 48rem){.digi-expandable-accordion__button.sc-digi-expandable-accordion{font-size:var(--digi--expandable-accordion--header--font-size)}}.digi-expandable-accordion--variation-primary.sc-digi-expandable-accordion .digi-expandable-accordion__button.sc-digi-expandable-accordion{color:var(--digi--color--text--primary);background-color:transparent;padding:var(--digi--expandable-accordion--content--padding)}.digi-expandable-accordion--variation-primary.sc-digi-expandable-accordion .digi-expandable-accordion__button.sc-digi-expandable-accordion:hover{text-decoration:underline}.digi-expandable-accordion--variation-secondary.sc-digi-expandable-accordion .digi-expandable-accordion__button.sc-digi-expandable-accordion{justify-content:space-between;min-height:var(--digi--input-height--large);padding:0 var(--digi--gutter--medium);border-radius:var(--digi--border-radius--primary);background-color:var(--digi--color--background--input-empty);border:1px solid var(--digi--color--background--tertiary)}.digi-expandable-accordion--variation-secondary.sc-digi-expandable-accordion .digi-expandable-accordion__button.sc-digi-expandable-accordion:focus-visible{outline:var(--digi--focus-outline);background-color:var(--digi--color--background--tertiary)}.digi-expandable-accordion--variation-secondary.sc-digi-expandable-accordion .digi-expandable-accordion__button.sc-digi-expandable-accordion:hover{background-color:var(--digi--color--background--tertiary)}.digi-expandable-accordion--variation-secondary.digi-expandable-accordion--expanded-true.sc-digi-expandable-accordion .digi-expandable-accordion__button.sc-digi-expandable-accordion{color:var(--digi--color--text--inverted);background-color:var(--digi--color--background--inverted-1);border-color:var(--digi--color--background--inverted-1);-webkit-border-after:none;border-block-end:none;border-end-end-radius:0;border-end-start-radius:0}.digi-expandable-accordion__toggle-label.sc-digi-expandable-accordion{display:inline-flex;align-items:center;gap:var(--digi--gutter--icon);font-size:var(--digi--global--typography--font-size--interaction-medium);color:var(--digi--color--text--secondary)}.digi-expandable-accordion--expanded-true.sc-digi-expandable-accordion .digi-expandable-accordion__toggle-label.sc-digi-expandable-accordion{color:currentColor}.digi-expandable-accordion--expanded-true.sc-digi-expandable-accordion .digi-expandable-accordion__toggle-label.sc-digi-expandable-accordion digi-icon.sc-digi-expandable-accordion{--digi--icon--color:currentColor}.digi-expandable-accordion__content.sc-digi-expandable-accordion{overflow-y:hidden}.digi-expandable-accordion--expanded-false.sc-digi-expandable-accordion .digi-expandable-accordion__content.sc-digi-expandable-accordion{height:0}.digi-expandable-accordion--variation-primary.sc-digi-expandable-accordion .digi-expandable-accordion__content.sc-digi-expandable-accordion>div.sc-digi-expandable-accordion{padding:var(--digi--expandable-accordion--content--padding)}.digi-expandable-accordion--variation-primary.digi-expandable-accordion--animation-true.sc-digi-expandable-accordion .digi-expandable-accordion__content.sc-digi-expandable-accordion{transition:var(--digi--expandable-accordion--content--transition)}.digi-expandable-accordion--variation-secondary.sc-digi-expandable-accordion .digi-expandable-accordion__content.sc-digi-expandable-accordion{background:var(--digi--color--background--input-empty)}.digi-expandable-accordion--variation-secondary.sc-digi-expandable-accordion .digi-expandable-accordion__content.sc-digi-expandable-accordion>div.sc-digi-expandable-accordion{display:flex;flex-direction:column}.digi-expandable-accordion--variation-secondary.sc-digi-expandable-accordion .digi-expandable-accordion__content.sc-digi-expandable-accordion digi-typography.sc-digi-expandable-accordion{display:block;padding:var(--digi--container-gutter--small)}.digi-expandable-accordion--variation-secondary.digi-expandable-accordion--expanded-true.sc-digi-expandable-accordion .digi-expandable-accordion__content.sc-digi-expandable-accordion{border:1px solid var(--digi--color--background--inverted-1);border-radius:var(--digi--border-radius--primary);border-start-end-radius:0;border-start-start-radius:0}.digi-expandable-accordion__toggle-inside.sc-digi-expandable-accordion{text-align:center;--digi--button--color--background--function--hover:var(--digi--color--background--tertiary)}@keyframes heightAnim{from{height:0%}to{height:100%}}";

const ExpandableAccordion = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.afOnClick = index.createEvent(this, "afOnClick", 7);
    this.afVariation = expandableAccordionVariation_enum.ExpandableAccordionVariation.PRIMARY;
    this.afHeading = undefined;
    this.afHeadingLevel = expandableAccordionVariation_enum.ExpandableAccordionHeaderLevel.H2;
    this.afExpanded = false;
    this.afAnimation = true;
    this.afId = randomIdGenerator_util.randomIdGenerator('digi-expandable-accordion');
  }
  get cssModifiers() {
    return {
      [`digi-expandable-accordion--expanded-${!!this.afExpanded}`]: true,
      [`digi-expandable-accordion--animation-${!!this.afAnimation}`]: true,
      [`digi-expandable-accordion--variation-${this.afVariation}`]: !!this.afVariation
    };
  }
  /**
   * Hämtar en höjden till innehållet i Utfällbar yta och animera innehållet.
   * @en Gets a height to the content for expandable and animate the content.
   */
  componentDidLoad() {
    const content = this.hostElement.children[0].querySelector(`#${this.afId}-content`);
    const sectionHeight = content.firstElementChild.offsetHeight;
    if (this.afExpanded) {
      content.setAttribute('style', 'height: ' + sectionHeight + 'px');
    }
  }
  animateContentHeight() {
    const content = this.hostElement.children[0].querySelector(`#${this.afId}-content`);
    const sectionHeight = content.firstElementChild.offsetHeight;
    if (this.afExpanded) {
      content.setAttribute('style', 'height: ' + sectionHeight + 'px');
    }
    else {
      content.setAttribute('style', 'height: ' + 0 + 'px');
    }
  }
  clickToggleHandler(e, resetFocus = false) {
    e.preventDefault();
    this.afExpanded = !this.afExpanded;
    if (resetFocus) {
      this._button.focus();
    }
    this.afOnClick.emit(e);
  }
  render() {
    return (index.h("article", { "aria-expanded": this.afExpanded ? 'true' : 'false', class: Object.assign({ 'digi-expandable-accordion': true }, this.cssModifiers) }, index.h("header", { class: "digi-expandable-accordion__header" }, index.h(this.afHeadingLevel, { class: "digi-expandable-accordion__heading" }, index.h("button", { type: "button", class: "digi-expandable-accordion__button", onClick: (e) => this.clickToggleHandler(e), "aria-pressed": this.afExpanded ? 'true' : 'false', "aria-expanded": this.afExpanded ? 'true' : 'false', "aria-controls": `${this.afId}-content`, ref: (el) => (this._button = el) }, this.afVariation !== expandableAccordionVariation_enum.ExpandableAccordionVariation.SECONDARY && (index.h("digi-icon", { afName: `chevron-down`, class: "digi-expandable-accordion__toggle-icon" })), this.afHeading, this.afVariation === expandableAccordionVariation_enum.ExpandableAccordionVariation.SECONDARY && (index.h("span", { class: "digi-expandable-accordion__toggle-label" }, this.afExpanded ? 'Dölj' : 'Visa', index.h("digi-icon", { afName: this.afExpanded ? 'chevron-up' : 'chevron-down', slot: "icon-secondary" })))))), index.h("section", { class: "digi-expandable-accordion__content", id: `${this.afId}-content` }, index.h("div", null, index.h("digi-typography", null, index.h("slot", null)), this.afVariation === expandableAccordionVariation_enum.ExpandableAccordionVariation.SECONDARY && (index.h("digi-button", { class: "digi-expandable-accordion__toggle-inside", afVariation: buttonVariation_enum.ButtonVariation.FUNCTION, afType: buttonType_enum.ButtonType.BUTTON, onAfOnClick: (e) => this.clickToggleHandler(e.detail, true), afFullWidth: true, afAriaPressed: this.afExpanded, afAriaExpanded: this.afExpanded, afAriaControls: `${this.afId}-content` }, this.afExpanded ? 'Dölj' : 'Visa', index.h("digi-icon", { afName: this.afExpanded ? 'chevron-up' : 'chevron-down', slot: "icon-secondary" })))))));
  }
  get hostElement() { return index.getElement(this); }
  static get watchers() { return {
    "afExpanded": ["animateContentHeight"]
  }; }
};
ExpandableAccordion.style = expandableAccordionCss;

exports.digi_expandable_accordion = ExpandableAccordion;
