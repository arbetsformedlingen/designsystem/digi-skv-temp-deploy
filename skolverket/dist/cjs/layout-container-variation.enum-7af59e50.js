'use strict';

exports.LayoutContainerVariation = void 0;
(function (LayoutContainerVariation) {
  LayoutContainerVariation["STATIC"] = "static";
  LayoutContainerVariation["FLUID"] = "fluid";
  LayoutContainerVariation["NONE"] = "none";
})(exports.LayoutContainerVariation || (exports.LayoutContainerVariation = {}));
