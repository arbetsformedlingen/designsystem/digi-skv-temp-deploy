'use strict';

exports.FormInputSearchVariation = void 0;
(function (FormInputSearchVariation) {
  FormInputSearchVariation["SMALL"] = "small";
  FormInputSearchVariation["MEDIUM"] = "medium";
  FormInputSearchVariation["LARGE"] = "large";
})(exports.FormInputSearchVariation || (exports.FormInputSearchVariation = {}));
