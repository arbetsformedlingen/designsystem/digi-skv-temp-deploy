'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');
const randomIdGenerator_util = require('./randomIdGenerator.util-9002c72c.js');
const detectClickOutside_util = require('./detectClickOutside.util-5528b0c5.js');
require('./detectClosest.util-8778dfa8.js');

const UtilDetectClickOutside = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.afOnClickOutside = index.createEvent(this, "afOnClickOutside", 7);
    this.afOnClickInside = index.createEvent(this, "afOnClickInside", 7);
    this.afDataIdentifier = randomIdGenerator_util.randomIdGenerator('data-digi-util-detect-click-outside');
    this.$el.setAttribute(this.afDataIdentifier, '');
  }
  clickHandler(e) {
    const target = e.target;
    const selector = `[${this.afDataIdentifier}]`;
    if (detectClickOutside_util.detectClickOutside(target, selector)) {
      this.afOnClickOutside.emit(e);
    }
    else {
      this.afOnClickInside.emit(e);
    }
  }
  render() {
    return index.h("slot", null);
  }
  get $el() { return index.getElement(this); }
};

exports.digi_util_detect_click_outside = UtilDetectClickOutside;
