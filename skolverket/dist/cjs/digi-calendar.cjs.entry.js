'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');
const randomIdGenerator_util = require('./randomIdGenerator.util-9002c72c.js');
const detectClickOutside_util = require('./detectClickOutside.util-5528b0c5.js');
const detectFocusOutside_util = require('./detectFocusOutside.util-a2222f93.js');
const logger_util = require('./logger.util-a78a149e.js');
require('./detectClosest.util-8778dfa8.js');

const calendarCss = ".sc-digi-calendar-h{--digi--calendar--display:inline-block;--digi--calendar--header--font-family:var(--digi--global--typography--font-family--default);--digi--calendar--header--font-weight:var(--digi--global--typography--font-weight--semibold);--digi--calendar--header--display:flex;--digi--calendar--header--align-items:center;--digi--calendar--header--justify-content:space-between;--digi--calendar--header--text-align:center;--digi--calendar--header--button--padding:var(--digi--gutter--medium) var(--digi--gutter--largest);--digi--calendar--header--button--background:transparent;--digi--calendar--header--button--box-shadow:none;--digi--calendar--header--button--background--hover:var(--digi--global--color--cta--blue--dark);--digi--calendar--table--border-spacing:0;--digi--calendar--table--border-collapse:collapse;--digi--calendar--table--font-family:var(--digi--global--typography--font-family--default);--digi--calendar--table--font-weight:var(--digi--global--typography--font-weight--semibold);--digi--calendar--table-header--padding:var(--digi--gutter--medium) 0;--digi--calendar--table-header--font-weight-normal:var(--digi--typography--body--font-weight--desktop);--digi--calendar--date--border-color:none;--digi--calendar--date--box-shadow:none;--digi--calendar--date--box-shadow--hover:inset 0 0 0 var(--digi--border-width--primary) var(--digi--color--text--link);--digi--calendar--date--background-color:var(--digi--color--background--primary);--digi--calendar--date--background-color--hover:var(--digi--global--color--cta--blue--dark);--digi--calendar--date--padding:var(--digi--gutter--large);--digi--calendar--date--color:var(--digi--color--text--primary);--digi--calendar--date--focused--box-shadow:inset 0 0 0 var(--digi--border-width--primary) var(--digi--color--text--link);--digi--calendar--date--focused--background-color:var(--digi--global--color--cta--blue--dark);--digi--calendar--date--today--box-shadow:none;--digi--calendar--date--today--background-color:var(--digi--color--background--neutral-1);--digi--calendar--date--today--background-color--hover:var(--digi--global--color--cta--blue--dark);--digi--calendar--date--today--background-color--focused:var(--digi--global--color--cta--blue--dark);--digi--calendar--date--today--border-color:var(--digi--color--background--neutral-1);--digi--calendar--date--today--color:var(--digi--color--text--primary);--digi--calendar--date--selected--color:var(--digi--color--text--inverted);--digi--calendar--date--selected--background-color:var(--digi--color--background--complementary-1);--digi--calendar--week--font-style:italic;--digi--calendar--week--font-size:var(--digi--global--typography--font-size--small);--digi--calendar--week--border-color:var(--digi--color--background--neutral-3);--digi--calendar--week--border-width:var(--digi--border-width--primary)}.sc-digi-calendar-h .digi-calendar.sc-digi-calendar{display:var(--digi--calendar--display);background:var(--digi--calendar--header--button--background)}.sc-digi-calendar-h .digi-calendar--hide.sc-digi-calendar{--digi--calendar--display:none}.sc-digi-calendar-h .digi-calendar__header.sc-digi-calendar{display:var(--digi--calendar--header--display);align-items:var(--digi--calendar--header--align-items);justify-content:var(--digi--calendar--header--justify-content);text-align:var(--digi--calendar--header--text-align);font-size:var(--digi--global--typography--font-size--base);font-family:var(--digi--calendar--header--font-family);font-weight:var(--digi--calendar--header--font-weight)}.sc-digi-calendar-h .digi-calendar__header.sc-digi-calendar .digi-calendar__month-select-name.sc-digi-calendar{padding-inline:var(--digi--padding--smaller)}.sc-digi-calendar-h .digi-calendar__header.sc-digi-calendar .digi-calendar__month-select-button.sc-digi-calendar{font-family:inherit;font-size:inherit;font-weight:inherit;display:flex;align-items:center;background:var(--digi--calendar--header--button--background);border:var(--digi--calendar--date--box-shadow);padding:var(--digi--calendar--header--button--padding)}.sc-digi-calendar-h .digi-calendar__header.sc-digi-calendar .digi-calendar__month-select-button.sc-digi-calendar:last-child{border-radius:0 4px 0 0}.sc-digi-calendar-h .digi-calendar__header.sc-digi-calendar .digi-calendar__month-select-button.sc-digi-calendar:first-child{border-radius:4px 0 0 0}.sc-digi-calendar-h .digi-calendar__header.sc-digi-calendar .digi-calendar__month-select-button.sc-digi-calendar:focus,.sc-digi-calendar-h .digi-calendar__header.sc-digi-calendar .digi-calendar__month-select-button.sc-digi-calendar:hover{--digi--calendar--header--button--background:var(--digi--calendar--header--button--background--hover);text-decoration:underline;cursor:pointer}.sc-digi-calendar-h .digi-calendar__table.sc-digi-calendar{border-collapse:var(--digi--calendar--table--border-collapse);border-spacing:var(--digi--calendar--table--border-spacing);font-family:var(--digi--calendar--table--font-family);table-layout:fixed}.sc-digi-calendar-h .digi-calendar__table-header-week.sc-digi-calendar{font-style:italic;padding:var(--digi--calendar--table-header--padding);font-weight:var(--digi--calendar--table-header--font-weight-normal);width:50px}.sc-digi-calendar-h .digi-calendar__table-header.sc-digi-calendar{padding:var(--digi--calendar--table-header--padding);font-weight:var(--digi--calendar--table--font-weight);width:44px}.sc-digi-calendar-h .digi-calendar__td-week.sc-digi-calendar{border-right:var(--digi--calendar--week--border-width) solid var(--digi--calendar--week--border-color)}.sc-digi-calendar-h .digi-calendar__date--week-number.sc-digi-calendar{font-style:var(--digi--calendar--week--font-style);font-size:var(--digi--calendar--week--font-size);display:flex;align-items:center;justify-content:center}.sc-digi-calendar-h .digi-calendar__date--week-number.sc-digi-calendar:hover{cursor:default}.sc-digi-calendar-h .digi-calendar__date.sc-digi-calendar{padding:var(--digi--calendar--date--padding);color:var(--digi--calendar--date--color);background-color:var(--digi--calendar--date--background-color);border-color:var(--digi--calendar--date--border-color);border-radius:var(--digi--border-radius--secondary);font-family:inherit;font-size:inherit;font-weight:inherit;text-align:center;border:none;width:100%}.sc-digi-calendar-h .digi-calendar__date--disabled.sc-digi-calendar{--digi--calendar--date--color:transparent;pointer-events:none}.sc-digi-calendar-h .digi-calendar__date--focused.sc-digi-calendar{--digi--calendar--date--box-shadow:var(--digi--calendar--date--focused--box-shadow);--digi--calendar--date--background-color:var(--digi--calendar--date--focused--background-color)}.sc-digi-calendar-h .digi-calendar__date--today.sc-digi-calendar{--digi--calendar--date--box-shadow:var(--digi--calendar--date--today--box-shadow);--digi--calendar--date--background-color:var(--digi--calendar--date--today--background-color);--digi--calendar--date--color:var(--digi--calendar--date--today--color);--digi--calendar--date--border-color:var(--digi--calendar--date--today--border-color)}.sc-digi-calendar-h .digi-calendar__date.sc-digi-calendar:hover{--digi--calendar--date--box-shadow:var(--digi--calendar--date--box-shadow--hover);--digi--calendar--date--background-color:var(--digi--calendar--date--background-color--hover);cursor:pointer}.sc-digi-calendar-h .digi-calendar__date.sc-digi-calendar:hover span.sc-digi-calendar{text-decoration:underline}.sc-digi-calendar-h .digi-calendar__date--selected.sc-digi-calendar{--digi--calendar--date--background-color:var(--digi--calendar--date--selected--background-color);--digi--calendar--date--color:var(--digi--calendar--date--selected--color)}.sc-digi-calendar-h .digi-calendar__date--selected.sc-digi-calendar:hover{--digi--calendar--date--background-color:var(--digi--calendar--date--selected--background-color)}.sc-digi-calendar-h .digi-calendar__date--selected.sc-digi-calendar:hover span.sc-digi-calendar{text-decoration:none}";

const Calendar = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.afOnDateSelectedChange = index.createEvent(this, "afOnDateSelectedChange", 7);
    this.afOnFocusOutside = index.createEvent(this, "afOnFocusOutside", 7);
    this.afOnClickOutside = index.createEvent(this, "afOnClickOutside", 7);
    this.afOnDirty = index.createEvent(this, "afOnDirty", 7);
    this.selectedDates = [];
    this.activeCalendar = [];
    this.months = [];
    this.focusedDate = undefined;
    this.dirty = false;
    this.afId = randomIdGenerator_util.randomIdGenerator('digi-calendar');
    this.afInitSelectedMonth = new Date().getMonth();
    this.afShowWeekNumber = false;
    this.afSelectedDate = undefined;
    this.afActive = true;
    this.afMultipleDates = false;
  }
  focusoutHandler(e) {
    const target = e.target;
    const selector = `#${this.afId}-calendar`;
    if (detectFocusOutside_util.detectFocusOutside(target, selector)) {
      this.afOnFocusOutside.emit(e);
    }
  }
  clickHandler(e) {
    const target = e.target;
    const selector = `#${this.afId}-calendar`;
    if (detectClickOutside_util.detectClickOutside(target, selector)) {
      this.afOnClickOutside.emit(e);
    }
  }
  drawCalendar(focusedDate) {
    const date = {
      year: focusedDate.getFullYear(),
      month: focusedDate.getMonth()
    };
    this.lastWeekOfPreviousMonth = this.getWeekDays(this.getLastDayOfPrevMonth(date));
    this.firstWeekOfNextMonth = this.getWeekDays(this.getFirstDayOfNextMonth(date));
    this.weeksBetween = this.getWeeksBetween(this.getLastDayOfPrevMonth(date), this.getFirstDayOfNextMonth(date));
    this.weekDaysbetween = this.getWeekdaysOfMonth(this.weeksBetween);
    this.activeCalendar = this.appendFirstWeekOfNextMonth()
      ? [
        this.lastWeekOfPreviousMonth,
        ...this.weekDaysbetween,
        this.appendFirstWeekOfNextMonth()
      ]
      : [this.lastWeekOfPreviousMonth, ...this.weekDaysbetween];
    if (this.afShowWeekNumber) {
      this.activeCalendar.forEach((el, i) => {
        this.activeCalendar[i].unshift(this.getWeekNumbers(el[0]));
      });
    }
    this.appendFirstWeekOfNextMonth();
  }
  appendFirstWeekOfNextMonth() {
    const lastArray = JSON.stringify(this.weekDaysbetween.slice(-1)[0]);
    const stringFirstweek = JSON.stringify(this.firstWeekOfNextMonth);
    if (!this.firstWeekOfNextMonth.some((date) => this.isSameMonth(date, this.focusedDate))) {
      return;
    }
    else if (lastArray !== stringFirstweek) {
      return this.firstWeekOfNextMonth;
    }
  }
  getLastDayOfPrevMonth(date) {
    return new Date(date.year, date.month, 0);
  }
  getWeekDays(date) {
    const today = date || new Date();
    const week = [];
    for (let i = 1; i <= 7; i++) {
      const first = today.getDate() - today.getDay() + i;
      const day = new Date(today.setDate(first));
      week.push(day);
    }
    return week;
  }
  getFirstDayOfNextMonth(date) {
    return new Date(date.year, date.month + 1, 1);
  }
  getWeeksBetween(lastDayOfPrevMonth, firstDayOnNextMonth) {
    const week = 7 * 24 * 60 * 60 * 1000;
    const d1ms = lastDayOfPrevMonth.getTime();
    const d2ms = firstDayOnNextMonth.getTime();
    const diff = Math.abs(d2ms - d1ms);
    return Math.floor(diff / week);
  }
  getWeekdaysOfMonth(weeksBetween) {
    let weeks = [];
    for (let i = 1; i <= weeksBetween; i++) {
      weeks = [...weeks, this.getWeekDays(this.getUpcoming(i))];
    }
    return weeks;
  }
  getUpcoming(weeks) {
    return new Date(this.focusedDate.getFullYear(), this.focusedDate.getMonth(), 7 * weeks);
  }
  onDirty() {
    if (!this.dirty) {
      this.dirty = true;
      this.afOnDirty.emit(this.dirty);
    }
  }
  selectDateHandler(dateSelected) {
    this.onDirty();
    if (this.afMultipleDates) {
      if (this.isDateSelected(dateSelected)) {
        this.selectedDates = this.selectedDates.filter((d) => {
          return !this.isSameDate(d, dateSelected);
        });
      }
      else {
        this.selectedDates = [...this.selectedDates, dateSelected];
      }
      if (!!this.afSelectedDate &&
        this.isSameDate(dateSelected, this.afSelectedDate)) {
        this.afSelectedDate = undefined;
      }
      else {
        this.afSelectedDate = new Date(dateSelected);
        this.focusedDate = new Date(dateSelected);
      }
      this.afOnDateSelectedChange.emit(this.selectedDates);
    }
    else {
      if (!!this.afSelectedDate &&
        this.isSameDate(dateSelected, this.afSelectedDate)) {
        this.afSelectedDate = undefined;
      }
      else {
        this.afSelectedDate = new Date(dateSelected);
        this.focusedDate = new Date(dateSelected);
      }
      this.afOnDateSelectedChange.emit([dateSelected]);
    }
  }
  getMonthName(monthNumber) {
    let date = new Date(this.focusedDate);
    let monthName = new Date(date.setMonth(monthNumber)).toLocaleDateString('default', {
      month: 'short'
    });
    return monthName.replace('.', '');
  }
  getFullMonthName(monthNumber) {
    let date = new Date(this.focusedDate);
    return new Date(date.setMonth(monthNumber)).toLocaleDateString('default', {
      month: 'long',
      year: 'numeric'
    });
  }
  // Get the week number of a specific date. Taken from https://weeknumber.com/how-to/javascript
  getWeekNumbers(dateInput) {
    let date = new Date(dateInput);
    date.getTime();
    date.setHours(0, 0, 0, 0);
    date.setDate(date.getDate() + 3 - ((date.getDay() + 6) % 7));
    var week1 = new Date(date.getFullYear(), 0, 4);
    return (1 +
      Math.round(((date.getTime() - week1.getTime()) / 86400000 -
        3 +
        ((week1.getDay() + 6) % 7)) /
        7));
  }
  isSameMonth(date, currentDate) {
    return (`${date.getFullYear()}${date.getMonth()}` ===
      `${currentDate.getFullYear()}${currentDate.getMonth()}`);
  }
  isSameDate(date, newDate) {
    const d1 = `${date.getDate()}${date.getMonth()}${date.getFullYear()}`;
    const d2 = `${newDate.getDate()}${newDate.getMonth()}${newDate.getFullYear()}`;
    return d1 === d2;
  }
  isDateSelected(date) {
    if (this.afMultipleDates) {
      let dates = this.selectedDates;
      let d1 = `${date.getDate()}${date.getMonth()}${date.getFullYear()}`;
      let checkSelected = dates.filter((d) => {
        let dateFormat = `${d.getDate()}${d.getMonth()}${d.getFullYear()}`;
        return d1 === dateFormat;
      });
      if (checkSelected.length) {
        return true;
      }
      else {
        return false;
      }
    }
    else {
      return this.isSameDate(date, new Date(this.afSelectedDate));
    }
  }
  isDisabledDate(date) {
    return !this.isSameMonth(date, this.focusedDate);
  }
  subtractMonth() {
    let month = new Date(this.focusedDate);
    month.setMonth(month.getMonth() - 1);
    this.afInitSelectedMonth = month.getMonth();
    this.focusedDate = month;
  }
  addMonth() {
    let month = new Date(this.focusedDate);
    month.setMonth(month.getMonth() + 1);
    this.afInitSelectedMonth = month.getMonth();
    this.focusedDate = month;
  }
  getCurrent(date) {
    return this.isSameDate(date, new Date()) ? 'date' : null;
  }
  setTabIndex(date) {
    if (this.dirty) {
      return this.isSameDate(date, this.focusedDate) ? 0 : -1;
    }
    else {
      return this.isSameMonth(date, new Date())
        ? this.isSameDate(date, new Date()) && !this.isDisabledDate(date)
          ? 0
          : -1
        : this.isSameMonth(date, this.focusedDate) && date.getDate() === 1
          ? 0
          : -1;
    }
  }
  navigateHandler(e, daysToAdd) {
    !!e.detail && e.detail.preventDefault();
    if (e.detail.key === 'Tab') {
      return;
    }
    this.onDirty();
    const date = new Date(e.detail.target.dataset.date);
    this.focusedDate = new Date(date.setDate(date.getDate() + daysToAdd));
    this.afInitSelectedMonth = this.focusedDate.getMonth();
    this.resetFocus();
  }
  shiftTabHandler() {
    this._nextButton.querySelector('button').focus();
  }
  resetFocus() {
    setTimeout(() => {
      const dates = this._tbody.querySelectorAll('.digi-calendar__date');
      if (dates.length <= 0) {
        logger_util.logger.warn(`Could not find an element with data-date attribute matching navigated date.`, this._tbody);
        return;
      }
      Array.from(dates).map((date) => {
        let d = date;
        d.tabIndex = -1;
        d.classList.remove('digi-calendar__date--focused');
        if (d.dataset.date === this.getFullDate(this.focusedDate)) {
          d.focus();
          d.tabIndex = 0;
          d.classList.add('digi-calendar__date--focused');
        }
      });
    }, 100);
  }
  getFullDate(date) {
    return date.toLocaleString('default', {
      day: 'numeric',
      month: 'numeric',
      year: 'numeric'
    });
  }
  getDateName(date) {
    return date.toLocaleString('default', {
      weekday: 'long',
      year: 'numeric',
      month: 'long',
      day: 'numeric'
    });
  }
  setFocused(date) {
    if (this.dirty) {
      return this.isSameDate(date, this.focusedDate);
    }
    else {
      return this.isSameMonth(date, new Date())
        ? this.isSameDate(date, new Date()) && !this.isDisabledDate(date)
        : this.isSameMonth(date, this.focusedDate) && date.getDate() === 1;
    }
  }
  ariaPressed(date) {
    return this.isSameDate(date, new Date(this.afSelectedDate))
      ? 'true'
      : 'false';
  }
  componentWillLoad() {
    const startSelectedMonth = new Date(new Date().getFullYear(), this.afInitSelectedMonth);
    this.focusedDate = new Date();
    this.drawCalendar(startSelectedMonth);
  }
  componentWillUpdate() {
    this.drawCalendar(this.focusedDate);
  }
  cssDateModifiers(date) {
    return {
      'digi-calendar__date--today': this.isSameDate(date, new Date()) && !this.isDisabledDate(date),
      'digi-calendar__date--disabled': this.isDisabledDate(date),
      'digi-calendar__date--focused': this.setFocused(date),
      'digi-calendar__date--selected': this.isDateSelected(date)
    };
  }
  render() {
    return (index.h("div", { class: {
        'digi-calendar': true,
        'digi-calendar--hide': !this.afActive
      }, id: `${this.afId}-calendar` }, index.h("div", { class: "digi-calendar__header" }, index.h("button", { class: "digi-calendar__month-select-button", type: "button", "aria-label": 'Gå till ' + this.getFullMonthName(this.afInitSelectedMonth - 1), onClick: () => this.subtractMonth() }, index.h("digi-icon", { afName: `chevron-left`, slot: "icon", "aria-hidden": "true" }), index.h("span", { class: "digi-calendar__month-select-name" }, this.getMonthName(this.afInitSelectedMonth - 1))), index.h("div", { class: "digi-calendar__month" }, this.getFullMonthName(this.afInitSelectedMonth)), index.h("button", { class: "digi-calendar__month-select-button", type: "button", "aria-label": 'Gå till ' + this.getFullMonthName(this.afInitSelectedMonth + 1), onClick: () => this.addMonth() }, index.h("span", { class: "digi-calendar__month-select-name" }, this.getMonthName(this.afInitSelectedMonth + 1)), index.h("digi-icon", { afName: `chevron-right`, slot: "icon", "aria-hidden": "true" }))), index.h("digi-util-keydown-handler", { onAfOnDown: (e) => this.navigateHandler(e, 7), onAfOnUp: (e) => this.navigateHandler(e, -7), onAfOnLeft: (e) => this.navigateHandler(e, -1), onAfOnRight: (e) => this.navigateHandler(e, 1), onAfOnShiftTab: () => this.shiftTabHandler() }, index.h("table", { role: "presentation", class: "digi-calendar__table" }, index.h("thead", { role: "presentation" }, index.h("tr", { role: "presentation" }, this.afShowWeekNumber && (index.h("th", { role: "presentation", abbr: "Vecka", class: "digi-calendar__table-header-week" }, "vecka")), index.h("th", { role: "presentation", abbr: "M\u00E5ndag", class: "digi-calendar__table-header" }, "m\u00E5"), index.h("th", { role: "presentation", abbr: "Tisdag", class: "digi-calendar__table-header" }, "ti"), index.h("th", { role: "presentation", abbr: "Onsdag", class: "digi-calendar__table-header" }, "on"), index.h("th", { role: "presentation", abbr: "Torsdag", class: "digi-calendar__table-header" }, "to"), index.h("th", { role: "presentation", abbr: "Fredag", class: "digi-calendar__table-header" }, "fr"), index.h("th", { role: "presentation", abbr: "L\u00F6rdag", class: "digi-calendar__table-header" }, "l\u00F6"), index.h("th", { role: "presentation", abbr: "S\u00F6ndag", class: "digi-calendar__table-header" }, "s\u00F6"))), index.h("tbody", { role: "presentation", ref: (el) => (this._tbody = el) }, this.activeCalendar.map((week) => {
      return (index.h("tr", { role: "presentation" }, week.map((day, i) => {
        if (this.afShowWeekNumber && i === 0) {
          return (index.h("td", { role: "presentation", class: "digi-calendar__td-week" }, index.h("div", { class: "digi-calendar__date--week-number" }, index.h("span", { "aria-hidden": "true" }, day))));
        }
        else {
          return (index.h("td", { role: "presentation", class: "digi-calendar__td" }, index.h("button", { type: "button", class: Object.assign(Object.assign({}, this.cssDateModifiers(day)), { 'digi-calendar__date': true }), onClick: () => this.selectDateHandler(day), "aria-current": this.getCurrent(day), "aria-label": this.getDateName(day), "aria-disabled": this.isDisabledDate(day), tabindex: this.setTabIndex(day), "data-date": this.getFullDate(day), "aria-pressed": this.ariaPressed(day) }, index.h("span", { "aria-hidden": "true" }, day.getDate()))));
        }
      })));
    })))), index.h("slot", { name: "calendar-footer" })));
  }
};
Calendar.style = calendarCss;

exports.digi_calendar = Calendar;
