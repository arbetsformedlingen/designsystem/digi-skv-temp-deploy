'use strict';

exports.FormTextareaValidation = void 0;
(function (FormTextareaValidation) {
  FormTextareaValidation["SUCCESS"] = "success";
  FormTextareaValidation["ERROR"] = "error";
  FormTextareaValidation["WARNING"] = "warning";
  FormTextareaValidation["NEUTRAL"] = "neutral";
})(exports.FormTextareaValidation || (exports.FormTextareaValidation = {}));

exports.FormTextareaVariation = void 0;
(function (FormTextareaVariation) {
  FormTextareaVariation["SMALL"] = "small";
  FormTextareaVariation["MEDIUM"] = "medium";
  FormTextareaVariation["LARGE"] = "large";
  FormTextareaVariation["AUTO"] = "auto";
})(exports.FormTextareaVariation || (exports.FormTextareaVariation = {}));
