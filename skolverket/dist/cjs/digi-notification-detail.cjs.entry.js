'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');
const notificationDetailVariation_enum = require('./notification-detail-variation.enum-658965de.js');
require('./button-size.enum-a46b7684.js');
require('./button-type.enum-322621ea.js');
require('./button-variation.enum-9364587a.js');
require('./expandable-accordion-variation.enum-2e394d6b.js');
require('./calendar-week-view-heading-level.enum-696abd05.js');
require('./form-radiobutton-variation.enum-cd8ac648.js');
require('./code-block-variation.enum-a28057da.js');
require('./code-example-variation.enum-a954aaf0.js');
require('./code-variation.enum-4c625938.js');
require('./form-checkbox-variation.enum-04455f3e.js');
require('./form-file-upload-variation.enum-26e4a9bd.js');
require('./form-input-search-variation.enum-5ea4a951.js');
require('./form-input-type.enum-7f96d46d.js');
require('./form-input-variation.enum-f95a25ea.js');
require('./form-select-variation.enum-139eab4f.js');
require('./form-textarea-variation.enum-8f1a5f71.js');
require('./form-validation-message-variation.enum-9244655f.js');
require('./layout-block-variation.enum-ae7e0e5f.js');
require('./layout-columns-variation.enum-9b7242b1.js');
require('./layout-container-variation.enum-7af59e50.js');
require('./layout-media-object-alignment.enum-e770b17d.js');
require('./link-external-variation.enum-864f5ac7.js');
require('./link-internal-variation.enum-354aedf4.js');
require('./link-variation.enum-55cb3944.js');
require('./loader-spinner-size.enum-96d3508e.js');
require('./media-figure-alignment.enum-5327a385.js');
require('./navigation-context-menu-item-type.enum-105e809f.js');
require('./navigation-sidebar-variation.enum-fa604f57.js');
require('./navigation-vertical-menu-variation.enum-40269ed5.js');
require('./progress-step-variation.enum-596ab007.js');
require('./progress-steps-variation.enum-3bbe5656.js');
require('./progressbar-variation.enum-81e49354.js');
require('./tag-size.enum-774d54e2.js');
require('./typography-meta-variation.enum-c7469a03.js');
require('./typography-time-variation.enum-4526d4ae.js');
const typographyVariation_enum = require('./typography-variation.enum-08e58c63.js');
require('./util-breakpoint-observer-breakpoints.enum-9f89c8ce.js');

const notificationDetailCss = ".sc-digi-notification-detail-h{--digi--notification-detail--border-color--info:var(--digi--color--border--informative);--digi--notification-detail--border-color--warning:var(--digi--color--border--warning);--digi--notification-detail--border-color--danger:var(--digi--color--border--danger)}.sc-digi-notification-detail-h .sc-digi-notification-detail-s>[slot^=heading]{--digi--heading-4--font-size:var(--digi--typography--heading-4--font-size--mobile);--digi--heading-4--line-height:var(--digi--typography--heading-4--line-height--mobile);font-family:var(--digi--global--typography--font-family--default);font-size:var(--digi--heading-4--font-size);line-height:var(--digi--heading-4--line-height);font-weight:var(--digi--typography--heading-4--font-weight--mobile);-webkit-margin-after:0;margin-block-end:0}@media (min-width: 48rem){.sc-digi-notification-detail-h .sc-digi-notification-detail-s>[slot^=heading]{--digi--heading-4--font-size:var(--digi--typography--heading-4--font-size--desktop);--digi--heading-4--line-height:var(--digi--typography--heading-4--line-height--desktop)}}@media (min-width: 62rem){.sc-digi-notification-detail-h .sc-digi-notification-detail-s>[slot^=heading]{--digi--heading-4--font-size:var(--digi--typography--heading-4--font-size--desktop-large);--digi--heading-4--line-height:var(--digi--typography--heading-4--line-height--desktop-large)}}.digi-notification-detail.sc-digi-notification-detail{-webkit-border-start:var(--digi--border-width--secondary) solid var(--digi--color--border--secondary);border-inline-start:var(--digi--border-width--secondary) solid var(--digi--color--border--secondary);border-inline-start-color:var(--BORDER-COLOR);background:var(--digi--color--background--notification-info)}.digi-notification-detail--variation-info.sc-digi-notification-detail{--BORDER-COLOR:var(--digi--notification-detail--border-color--info);--ICON-COLOR:var(--digi--notification-detail--border-color--info)}.digi-notification-detail--variation-danger.sc-digi-notification-detail{--BORDER-COLOR:var(--digi--notification-detail--border-color--danger);--ICON-COLOR:var(--digi--notification-detail--border-color--danger)}.digi-notification-detail--variation-warning.sc-digi-notification-detail{--BORDER-COLOR:var(--digi--notification-detail--border-color--warning);--ICON-COLOR:var(--digi--notification-detail--border-color--warning)}.digi-notification-detail__inner.sc-digi-notification-detail{display:grid;grid-template-columns:min-content 1fr;grid-template-areas:\"icon content\";gap:calc(var(--digi--gutter--icon) * 2);padding:var(--digi--gutter--medium) var(--digi--gutter--larger);-webkit-padding-end:calc(var(--digi--gutter--larger) * 2);padding-inline-end:calc(var(--digi--gutter--larger) * 2)}@media (max-width: 47.9375rem){.digi-notification-detail__inner.sc-digi-notification-detail{justify-content:space-between;align-items:center}}.digi-notification-detail__icon.sc-digi-notification-detail{grid-area:icon}.digi-notification-detail__icon.sc-digi-notification-detail digi-icon.sc-digi-notification-detail{--digi--icon--color:var(--ICON-COLOR);--digi-icon--fixed-width:32px;--digi-icon--fixed-height:32px}.digi-notification-detail__content.sc-digi-notification-detail{grid-area:content}.digi-notification-detail__text.sc-digi-notification-detail{--digi--typography--body--font-size--desktop:var(--digi--global--typography--font-size--smaller);--digi--typography--body--font-size--mobile:var(--digi--global--typography--font-size--smaller);--digi--typography--body--line-height--desktop:var(--digi--global--typography--line-height--smaller);--digi--typography--body--line-height--mobile:var(--digi--global--typography--line-height--smaller)}";

const NotificationDetail = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.afVariation = notificationDetailVariation_enum.NotificationDetailVariation.INFO;
  }
  get cssModifiers() {
    return {
      [`digi-notification-detail--variation-${this.afVariation}`]: !!this.afVariation
    };
  }
  render() {
    return (index.h("div", { class: Object.assign({ 'digi-notification-detail': true }, this.cssModifiers) }, index.h("div", { class: "digi-notification-detail__inner" }, index.h("div", { class: "digi-notification-detail__icon" }, index.h("digi-icon", { afName: `notification-${this.afVariation}`, "aria-hidden": "true" })), index.h("div", { class: "digi-notification-detail__content" }, index.h("slot", { name: "heading" }), index.h("digi-typography", { class: "digi-notification-detail__text", "af-variation": typographyVariation_enum.TypographyVariation.SMALL }, index.h("slot", null))))));
  }
};
NotificationDetail.style = notificationDetailCss;

exports.digi_notification_detail = NotificationDetail;
