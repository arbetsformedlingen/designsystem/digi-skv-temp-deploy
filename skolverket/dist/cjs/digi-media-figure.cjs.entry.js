'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');
const mediaFigureAlignment_enum = require('./media-figure-alignment.enum-5327a385.js');

const mediaFigureCss = ".sc-digi-media-figure-h .digi-media-figure__figcaption.sc-digi-media-figure{font-family:var(--digi--global--typography--font-family--default);font-size:var(--digi--typography--body--font-size--mobile);color:var(--digi--color--text--neutral);margin-top:var(--digi--gutter--smaller)}.digi-media-figure--align-start.sc-digi-media-figure .digi-media-figure__figcaption.sc-digi-media-figure{text-align:start}.digi-media-figure--align-center.sc-digi-media-figure .digi-media-figure__figcaption.sc-digi-media-figure{text-align:center}.digi-media-figure--align-end.sc-digi-media-figure .digi-media-figure__figcaption.sc-digi-media-figure{text-align:end}";

const MediaFigure = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.afFigcaption = undefined;
    this.afAlignment = mediaFigureAlignment_enum.MediaFigureAlignment.START;
  }
  get cssModifiers() {
    return {
      'digi-media-figure--align-start': this.afAlignment === 'start',
      'digi-media-figure--align-center': this.afAlignment === 'center',
      'digi-media-figure--align-end': this.afAlignment === 'end',
    };
  }
  render() {
    return (index.h("figure", { class: Object.assign({ 'digi-media-figure': true }, this.cssModifiers) }, index.h("div", { class: "digi-media-figure__image" }, index.h("slot", null)), this.afFigcaption && (index.h("figcaption", { class: "digi-media-figure__figcaption" }, this.afFigcaption))));
  }
};
MediaFigure.style = mediaFigureCss;

exports.digi_media_figure = MediaFigure;
