'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');
const randomIdGenerator_util = require('./randomIdGenerator.util-9002c72c.js');

const formRadiogroupCss = "";

const FormRadiogroup = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.afOnGroupChange = index.createEvent(this, "afOnGroupChange", 7);
    this.radiobuttons = undefined;
    this.afValue = undefined;
    this.value = undefined;
    this.afName = randomIdGenerator_util.randomIdGenerator('digi-form-radiogroup');
  }
  OnAfValueChange(value) {
    this.value = value;
  }
  OnValueChange(value) {
    this.afValue = value;
    this.setRadiobuttons(value);
  }
  radiobuttonChangeListener(e) {
    if (Array.from(this.radiobuttons).includes(e.target)) {
      e.preventDefault();
      this.afValue = e.detail.target.value;
      this.afOnGroupChange.emit(e);
    }
  }
  componentWillLoad() { }
  componentDidLoad() {
    this.setRadiobuttons(this.value || this.afValue);
    if (this.value)
      this.afValue = this.value;
  }
  componentWillUpdate() { }
  setRadiobuttons(value) {
    this.radiobuttons = this.hostElement.querySelectorAll('digi-form-radiobutton');
    this.radiobuttons.forEach(r => {
      r.afName = this.afName;
      if (value && r.afValue == value) {
        setTimeout(() => r.afMGetFormControlElement().then(e => e.checked || e.click()), 0);
      }
    });
  }
  get cssModifiers() {
    return {};
  }
  render() {
    return (index.h(index.Host, null, index.h("div", { class: Object.assign({ 'digi-form-radiogroup': true }, this.cssModifiers) }, index.h("digi-util-mutation-observer", { onAfOnChange: (e) => this.radiobuttonChangeListener(e) }, index.h("slot", null)))));
  }
  get hostElement() { return index.getElement(this); }
  static get watchers() { return {
    "afValue": ["OnAfValueChange"],
    "value": ["OnValueChange"]
  }; }
};
FormRadiogroup.style = formRadiogroupCss;

exports.digi_form_radiogroup = FormRadiogroup;
