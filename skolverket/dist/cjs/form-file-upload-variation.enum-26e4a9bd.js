'use strict';

exports.FormFileUploadHeadingLevel = void 0;
(function (FormFileUploadHeadingLevel) {
  FormFileUploadHeadingLevel["H1"] = "h1";
  FormFileUploadHeadingLevel["H2"] = "h2";
  FormFileUploadHeadingLevel["H3"] = "h3";
  FormFileUploadHeadingLevel["H4"] = "h4";
  FormFileUploadHeadingLevel["H5"] = "h5";
  FormFileUploadHeadingLevel["H6"] = "h6";
})(exports.FormFileUploadHeadingLevel || (exports.FormFileUploadHeadingLevel = {}));

exports.FormFileUploadValidation = void 0;
(function (FormFileUploadValidation) {
  FormFileUploadValidation["ENABLED"] = "enabled";
  FormFileUploadValidation["DISABLED"] = "disabled";
})(exports.FormFileUploadValidation || (exports.FormFileUploadValidation = {}));

exports.FormFileUploadVariation = void 0;
(function (FormFileUploadVariation) {
  FormFileUploadVariation["PRIMARY"] = "primary";
  FormFileUploadVariation["SECONDARY"] = "secondary";
  FormFileUploadVariation["TERTIARY"] = "tertiary";
})(exports.FormFileUploadVariation || (exports.FormFileUploadVariation = {}));
