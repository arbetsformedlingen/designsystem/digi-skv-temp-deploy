'use strict';

exports.TypographyMetaVariation = void 0;
(function (TypographyMetaVariation) {
  TypographyMetaVariation["PRIMARY"] = "primary";
  TypographyMetaVariation["SECONDARY"] = "secondary";
})(exports.TypographyMetaVariation || (exports.TypographyMetaVariation = {}));
