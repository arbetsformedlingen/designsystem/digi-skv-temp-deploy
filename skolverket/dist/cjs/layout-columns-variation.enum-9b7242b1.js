'use strict';

exports.LayoutColumnsElement = void 0;
(function (LayoutColumnsElement) {
  LayoutColumnsElement["DIV"] = "div";
  LayoutColumnsElement["UL"] = "ul";
  LayoutColumnsElement["OL"] = "ol";
})(exports.LayoutColumnsElement || (exports.LayoutColumnsElement = {}));

exports.LayoutColumnsVariation = void 0;
(function (LayoutColumnsVariation) {
  LayoutColumnsVariation["THREE"] = "three";
  LayoutColumnsVariation["TWO"] = "two";
})(exports.LayoutColumnsVariation || (exports.LayoutColumnsVariation = {}));
