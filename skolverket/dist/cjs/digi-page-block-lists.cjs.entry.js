'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');
require('./card-box-gutter.enum-585dded6.js');
const cardBoxWidth_enum = require('./card-box-width.enum-1a09a997.js');
require('./page-background.enum-c35f4c9a.js');
require('./layout-grid-vertical-spacing.enum-2b857671.js');
require('./layout-stacked-blocks-variation.enum-dd570a85.js');
require('./list-link-variation.enum-c43133c0.js');
require('./navigation-breadcrumbs-variation.enum-b46c1f09.js');
require('./notification-detail-variation.enum-658965de.js');
require('./page-footer-variation.enum-ec14686c.js');
require('./table-variation.enum-bc26c29e.js');

const pageBlockListsCss = ".digi-page-block-lists.sc-digi-page-block-lists{display:block;background:var(--digi--layout-page-block-lists--background, transparent);padding:var(--digi--responsive-grid-gutter) 0}.digi-page-block-lists--variation-start.sc-digi-page-block-lists,.digi-page-block-lists--variation-sub.sc-digi-page-block-lists{--digi--layout-page-block-lists--background:var(--digi--global--color--profile--apricot--opacity50)}.digi-page-block-lists--variation-section.sc-digi-page-block-lists{--digi--layout-page-block-lists--background:var(--digi--color--background--primary)}.digi-page-block-lists__inner.sc-digi-page-block-lists{display:grid;grid-template-columns:1fr 2fr;gap:var(--digi--responsive-grid-gutter)}@media (max-width: 47.9375rem){.digi-page-block-lists__inner.sc-digi-page-block-lists{grid-template-columns:1fr}}.digi-page-block-lists__inner.sc-digi-page-block-lists digi-layout-stacked-blocks.sc-digi-page-block-lists{width:100%}";

const PageBlockLists = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.afVariation = undefined;
  }
  get cssModifiers() {
    return {
      [`digi-page-block-lists--variation-${this.afVariation}`]: !!this.afVariation
    };
  }
  render() {
    return (index.h("digi-layout-container", { class: Object.assign({ 'digi-page-block-lists': true }, this.cssModifiers) }, index.h("digi-card-box", { afWidth: cardBoxWidth_enum.CardBoxWidth.FULL }, index.h("div", { class: "digi-page-block-lists__inner" }, index.h("digi-typography-heading-section", null, index.h("slot", { name: "heading" })), index.h("digi-layout-stacked-blocks", null, index.h("slot", null))))));
  }
  static get assetsDirs() { return ["public"]; }
};
PageBlockLists.style = pageBlockListsCss;

exports.digi_page_block_lists = PageBlockLists;
