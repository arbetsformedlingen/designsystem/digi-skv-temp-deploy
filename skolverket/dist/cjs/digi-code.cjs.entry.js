'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');
const codeVariation_enum = require('./code-variation.enum-4c625938.js');
const prismGit = require('./prism-git-921abd75.js');

const codeCss = "digi-code{--digi--code--background:var(--digi--color--background--primary)}digi-code .digi-code{border-radius:0.3em;border:var(--digi--border-width--primary) solid var(--digi--color--border--neutral-2);padding:0 0.5em;display:inline-block}digi-code .digi-code--light{background:#fefefe;}digi-code .digi-code--light code[class*=language-]{color:#545454;background:none;font-family:var(--digi--global--typography--font-family--mono);text-align:left;white-space:pre;word-spacing:normal;word-break:normal;word-wrap:normal;line-height:1.5;-moz-tab-size:4;-o-tab-size:4;tab-size:4;-webkit-hyphens:none;hyphens:none}digi-code .digi-code--light code[class*=language-]{white-space:normal}digi-code .digi-code--light .token.comment,digi-code .digi-code--light .token.prolog,digi-code .digi-code--light .token.doctype,digi-code .digi-code--light .token.cdata{color:#696969}digi-code .digi-code--light .token.punctuation{color:#545454}digi-code .digi-code--light .token.property,digi-code .digi-code--light .token.tag,digi-code .digi-code--light .token.constant,digi-code .digi-code--light .token.symbol,digi-code .digi-code--light .token.deleted{color:#007faa}digi-code .digi-code--light .token.boolean,digi-code .digi-code--light .token.number{color:#008000}digi-code .digi-code--light .token.selector,digi-code .digi-code--light .token.attr-name,digi-code .digi-code--light .token.string,digi-code .digi-code--light .token.char,digi-code .digi-code--light .token.builtin,digi-code .digi-code--light .token.inserted{color:#aa5d00}digi-code .digi-code--light .token.operator,digi-code .digi-code--light .token.entity,digi-code .digi-code--light .token.url,digi-code .digi-code--light .language-css .token.string,digi-code .digi-code--light .style .token.string,digi-code .digi-code--light .token.variable{color:#008000}digi-code .digi-code--light .token.atrule,digi-code .digi-code--light .token.attr-value,digi-code .digi-code--light .token.function{color:#aa5d00}digi-code .digi-code--light .token.keyword{color:#d91e18}digi-code .digi-code--light .token.regex,digi-code .digi-code--light .token.important{color:#d91e18}digi-code .digi-code--light .token.important,digi-code .digi-code--light .token.bold{font-weight:bold}digi-code .digi-code--light .token.italic{font-style:italic}digi-code .digi-code--light .token.entity{cursor:help}@media screen and (-ms-high-contrast: active){digi-code .digi-code--light code[class*=language-]{color:windowText;background:window}digi-code .digi-code--light code[class*=language-]{background:window}digi-code .digi-code--light .token.important{background:highlight;color:window;font-weight:normal}digi-code .digi-code--light .token.atrule,digi-code .digi-code--light .token.attr-value,digi-code .digi-code--light .token.function,digi-code .digi-code--light .token.keyword,digi-code .digi-code--light .token.operator,digi-code .digi-code--light .token.selector{font-weight:bold}digi-code .digi-code--light .token.attr-value,digi-code .digi-code--light .token.comment,digi-code .digi-code--light .token.doctype,digi-code .digi-code--light .token.function,digi-code .digi-code--light .token.keyword,digi-code .digi-code--light .token.operator,digi-code .digi-code--light .token.property,digi-code .digi-code--light .token.string{color:highlight}digi-code .digi-code--light .token.attr-value,digi-code .digi-code--light .token.url{font-weight:normal}}digi-code .digi-code--dark{--digi--color--background--inverted-3:#272727;background:var(--digi--color--background--inverted-3);}digi-code .digi-code--dark code[class*=language-]{color:#f8f8f2;background:none;font-family:var(--digi--global--typography--font-family--mono);text-align:left;white-space:pre;word-spacing:normal;word-break:normal;word-wrap:normal;line-height:1.5;-moz-tab-size:4;-o-tab-size:4;tab-size:4;-webkit-hyphens:none;hyphens:none}digi-code .digi-code--dark code[class*=language-]{white-space:normal}digi-code .digi-code--dark .token.comment,digi-code .digi-code--dark .token.prolog,digi-code .digi-code--dark .token.doctype,digi-code .digi-code--dark .token.cdata{color:#d4d0ab}digi-code .digi-code--dark .token.punctuation{color:#fefefe}digi-code .digi-code--dark .token.property,digi-code .digi-code--dark .token.tag,digi-code .digi-code--dark .token.constant,digi-code .digi-code--dark .token.symbol,digi-code .digi-code--dark .token.deleted{color:#ffa07a}digi-code .digi-code--dark .token.boolean,digi-code .digi-code--dark .token.number{color:#00e0e0}digi-code .digi-code--dark .token.selector,digi-code .digi-code--dark .token.attr-name,digi-code .digi-code--dark .token.string,digi-code .digi-code--dark .token.char,digi-code .digi-code--dark .token.builtin,digi-code .digi-code--dark .token.inserted{color:#abe338}digi-code .digi-code--dark .token.operator,digi-code .digi-code--dark .token.entity,digi-code .digi-code--dark .token.url,digi-code .digi-code--dark .language-css .token.string,digi-code .digi-code--dark .style .token.string,digi-code .digi-code--dark .token.variable{color:#00e0e0}digi-code .digi-code--dark .token.atrule,digi-code .digi-code--dark .token.attr-value,digi-code .digi-code--dark .token.function{color:#ffd700}digi-code .digi-code--dark .token.keyword{color:#00e0e0}digi-code .digi-code--dark .token.regex,digi-code .digi-code--dark .token.important{color:#ffd700}digi-code .digi-code--dark .token.important,digi-code .digi-code--dark .token.bold{font-weight:bold}digi-code .digi-code--dark .token.italic{font-style:italic}digi-code .digi-code--dark .token.entity{cursor:help}@media screen and (-ms-high-contrast: active){digi-code .digi-code--dark code[class*=language-]{color:windowText;background:window}digi-code .digi-code--dark code[class*=language-]{background:window}digi-code .digi-code--dark .token.important{background:highlight;color:window;font-weight:normal}digi-code .digi-code--dark .token.atrule,digi-code .digi-code--dark .token.attr-value,digi-code .digi-code--dark .token.function,digi-code .digi-code--dark .token.keyword,digi-code .digi-code--dark .token.operator,digi-code .digi-code--dark .token.selector{font-weight:bold}digi-code .digi-code--dark .token.attr-value,digi-code .digi-code--dark .token.comment,digi-code .digi-code--dark .token.doctype,digi-code .digi-code--dark .token.function,digi-code .digi-code--dark .token.keyword,digi-code .digi-code--dark .token.operator,digi-code .digi-code--dark .token.property,digi-code .digi-code--dark .token.string{color:highlight}digi-code .digi-code--dark .token.attr-value,digi-code .digi-code--dark .token.url{font-weight:normal}}";

const Code = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.highlightedCode = undefined;
    this.afCode = undefined;
    this.afVariation = codeVariation_enum.CodeVariation.LIGHT;
    this.afLanguage = codeVariation_enum.CodeLanguage.HTML;
    this.afLang = 'en';
  }
  componentWillLoad() {
    prismGit.prism.manual = true;
    this.formatCode();
  }
  formatCode() {
    if (this.afCode && this.afLanguage) {
      this.highlightedCode = prismGit.prism.highlight(this.afCode, prismGit.prism.languages[this.afLanguage], this.afLanguage);
    }
  }
  get cssModifiers() {
    return {
      'digi-code--light': this.afVariation === codeVariation_enum.CodeVariation.LIGHT,
      'digi-code--dark': this.afVariation === codeVariation_enum.CodeVariation.DARK,
    };
  }
  render() {
    return (index.h("span", { class: Object.assign({ 'digi-code': true }, this.cssModifiers) }, index.h("code", { class: `digi-code__code language-${this.afLanguage}`, lang: this.afLang, innerHTML: this.highlightedCode })));
  }
  static get watchers() { return {
    "afCode": ["formatCode"]
  }; }
};
Code.style = codeCss;

exports.digi_code = Code;
