'use strict';

exports.TypographyVariation = void 0;
(function (TypographyVariation) {
  TypographyVariation["SMALL"] = "small";
  TypographyVariation["LARGE"] = "large";
})(exports.TypographyVariation || (exports.TypographyVariation = {}));
