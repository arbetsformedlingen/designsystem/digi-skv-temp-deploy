'use strict';

exports.CalendarWeekViewHeadingLevel = void 0;
(function (CalendarWeekViewHeadingLevel) {
  CalendarWeekViewHeadingLevel["H1"] = "h1";
  CalendarWeekViewHeadingLevel["H2"] = "h2";
  CalendarWeekViewHeadingLevel["H3"] = "h3";
  CalendarWeekViewHeadingLevel["H4"] = "h4";
  CalendarWeekViewHeadingLevel["H5"] = "h5";
  CalendarWeekViewHeadingLevel["H6"] = "h6";
})(exports.CalendarWeekViewHeadingLevel || (exports.CalendarWeekViewHeadingLevel = {}));
