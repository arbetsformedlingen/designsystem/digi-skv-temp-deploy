'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');
const randomIdGenerator_util = require('./randomIdGenerator.util-9002c72c.js');

const notificationCookieCss = ".sc-digi-notification-cookie-h{--digi--notification-cookie--border-color--info:var(--digi--color--border--informative);--digi--notification-cookie--border-color--warning:var(--digi--color--border--warning);--digi--notification-cookie--border-color--danger:var(--digi--color--border--danger);--digi--notification-cookie--background-color--info:var(--digi--color--background--notification-info);--digi--notification-cookie--background-color--warning:var(--digi--color--background--notification-warning);--digi--notification-cookie--background-color--danger:var(--digi--color--background--notification-danger)}.digi-notification-cookie__alert.sc-digi-notification-cookie{--digi--notification-alert--border-color--info:var(--digi--color--border--secondary);--digi--notification-alert--background-color--info:var(--digi--color--background--secondary)}.digi-notification-cookie__content.sc-digi-notification-cookie{display:grid;grid-auto-flow:row;gap:var(--digi--responsive-grid-gutter)}@media (min-width: 48rem){.digi-notification-cookie__content.sc-digi-notification-cookie{grid-auto-flow:column;grid-template-columns:1fr auto}}.digi-notification-cookie__actions.sc-digi-notification-cookie{display:flex;flex-direction:column;gap:var(--digi--gutter--icon)}@media (min-width: 48rem){.digi-notification-cookie__actions.sc-digi-notification-cookie{flex-direction:row}}.digi-notification-cookie__form-controls.sc-digi-notification-cookie{--digi--form-fieldset--padding:4px 0}.digi-notification-cookie__form-controls.sc-digi-notification-cookie>*.sc-digi-notification-cookie:not(:last-child){-webkit-margin-after:var(--digi--gutter--large);margin-block-end:var(--digi--gutter--large)}";

const NotificationCookie = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.afOnAcceptAllCookies = index.createEvent(this, "afOnAcceptAllCookies", 7);
    this.afOnSubmitSettings = index.createEvent(this, "afOnSubmitSettings", 7);
    this.modalIsOpen = false;
    this.afBannerText = 'Vi använder kakor (cookies) för att webbplatsen ska fungera på ett så bra sätt som möjligt. Här kan du välja vilka cookies du vill godkänna.';
    this.afBannerHeadingText = 'Vi använder kakor';
    this.afModalHeadingText = 'Anpassa inställningar för kakor (cookies)';
    this.afRequiredCookiesText = 'Nödvändiga kakor gör att våra tjänster är säkra och fungerar som de ska. Därför går de inte att inaktivera.';
    this.afId = randomIdGenerator_util.randomIdGenerator('digi-notification-cookie');
  }
  clickPrimaryButtonHandler() {
    this.afOnAcceptAllCookies.emit();
  }
  formSubmitHandler(e) {
    this.afOnSubmitSettings.emit(e);
  }
  openModal() {
    this.modalIsOpen = true;
  }
  closeModal() {
    this.modalIsOpen = false;
  }
  render() {
    return (index.h(index.Host, null, index.h("digi-notification-alert", { class: "digi-notification-cookie__alert" }, index.h("h2", { slot: "heading" }, this.afBannerHeadingText), index.h("div", { class: "digi-notification-cookie__content" }, index.h("p", null, this.afBannerText)), index.h("div", { class: "digi-notification-cookie__link", slot: "link" }, index.h("slot", { name: "link" })), index.h("div", { class: "digi-notification-cookie__actions", slot: "actions" }, index.h("digi-button", { afType: "button", afFullWidth: true, onAfOnClick: () => this.clickPrimaryButtonHandler() }, "Godk\u00E4nn alla kakor"), index.h("digi-button", { afType: "button", afFullWidth: true, afVariation: "secondary", onAfOnClick: () => this.openModal() }, "Inst\u00E4llningar"))), index.h("digi-dialog", { afOpen: this.modalIsOpen, onAfOnClose: () => this.closeModal() }, index.h("h2", { slot: "heading" }, this.afModalHeadingText), index.h("p", null, this.afRequiredCookiesText), index.h("form", { method: "dialog", id: `${this.afId}-form`, onSubmit: (e) => this.formSubmitHandler(e) }, index.h("digi-form-fieldset", { class: "digi-notification-cookie__form-controls", afName: "cookie-settings", afId: `${this.afId}-cookie-settings` }, index.h("slot", { name: "settings" }))), index.h("digi-button", { afType: "button", afVariation: "secondary", onAfOnClick: () => this.closeModal(), slot: "actions" }, "Avbryt"), index.h("digi-button", { afType: "submit", afForm: `${this.afId}-form`, slot: "actions" }, "Spara och godk\u00E4nn"))));
  }
};
NotificationCookie.style = notificationCookieCss;

exports.digi_notification_cookie = NotificationCookie;
