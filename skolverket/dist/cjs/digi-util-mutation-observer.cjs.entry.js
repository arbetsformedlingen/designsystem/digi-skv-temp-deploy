'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');
const randomIdGenerator_util = require('./randomIdGenerator.util-9002c72c.js');

const UtilMutationObserver = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.afOnChange = index.createEvent(this, "afOnChange", 7);
    this._observer = null;
    this._isObserving = false;
    this.afId = randomIdGenerator_util.randomIdGenerator('digi-util-mutation-observer');
    this.afOptions = {
      attributes: false,
      childList: true,
      subtree: false
    };
  }
  afOptionsWatcher(newValue) {
    this._afOptions =
      typeof newValue === 'string' ? JSON.parse(newValue) : newValue;
  }
  componentWillLoad() {
    this.afOptionsWatcher(this.afOptions);
    this.initObserver();
  }
  disconnectedCallback() {
    if (this._isObserving) {
      this.removeObserver();
    }
  }
  initObserver() {
    this._observer = new MutationObserver((mutationsList) => {
      for (const mutation of mutationsList) {
        if (mutation.type === 'childList' || mutation.type === 'attributes') {
          this.changeHandler(mutation);
        }
      }
    });
    this._observer.observe(this.$el, this._afOptions);
    this._isObserving = true;
  }
  removeObserver() {
    this._observer.disconnect();
    this._isObserving = false;
  }
  changeHandler(e) {
    this.afOnChange.emit(e);
  }
  render() {
    index.h("div", { id: this.afId, ref: (el) => (this.$el = el) }, index.h("slot", null));
  }
  get $el() { return index.getElement(this); }
  static get watchers() { return {
    "afOptions": ["afOptionsWatcher"]
  }; }
};

exports.digi_util_mutation_observer = UtilMutationObserver;
