'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');
const randomIdGenerator_util = require('./randomIdGenerator.util-9002c72c.js');

const navigationTabCss = ".sc-digi-navigation-tab-h{--digi--navigation-tab--box-shadow--focus:solid 2px var(--digi--color--border--secondary)}.sc-digi-navigation-tab-h .digi-navigation-tab.sc-digi-navigation-tab:focus-visible{box-shadow:var(--digi--navigation-tab--box-shadow--focus);outline:none}";

const NavigationTab = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.afOnToggle = index.createEvent(this, "afOnToggle", 7);
    this.afAriaLabel = undefined;
    this.afId = randomIdGenerator_util.randomIdGenerator('digi-navigation-tab');
    this.afActive = undefined;
  }
  toggleHandler(activeTab) {
    if (activeTab || null) {
      this.afOnToggle.emit(activeTab);
    }
  }
  render() {
    return (index.h("div", { class: "digi-navigation-tab", tabindex: "0", role: "tabpanel", id: this.afId, "aria-label": this.afAriaLabel, hidden: !this.afActive }, index.h("slot", null)));
  }
  static get watchers() { return {
    "afActive": ["toggleHandler"]
  }; }
};
NavigationTab.style = navigationTabCss;

exports.digi_navigation_tab = NavigationTab;
