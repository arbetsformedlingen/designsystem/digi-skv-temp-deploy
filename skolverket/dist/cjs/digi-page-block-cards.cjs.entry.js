'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');
require('./card-box-gutter.enum-585dded6.js');
const cardBoxWidth_enum = require('./card-box-width.enum-1a09a997.js');
require('./page-background.enum-c35f4c9a.js');
require('./layout-grid-vertical-spacing.enum-2b857671.js');
require('./layout-stacked-blocks-variation.enum-dd570a85.js');
require('./list-link-variation.enum-c43133c0.js');
require('./navigation-breadcrumbs-variation.enum-b46c1f09.js');
require('./notification-detail-variation.enum-658965de.js');
require('./page-footer-variation.enum-ec14686c.js');
require('./table-variation.enum-bc26c29e.js');

const pageBlockCardsCss = ".digi-page-block-cards.sc-digi-page-block-cards{display:block;background:var(--digi--layout-page-block-cards--background, transparent);padding:var(--digi--responsive-grid-gutter) 0}.digi-page-block-cards--variation-start.sc-digi-page-block-cards,.digi-page-block-cards--variation-sub.sc-digi-page-block-cards{--digi--layout-page-block-cards--background:var(--digi--global--color--profile--apricot--opacity50)}.digi-page-block-cards--variation-section.sc-digi-page-block-cards{--digi--layout-page-block-cards--background:var(--digi--color--background--primary)}.digi-page-block-cards__inner.sc-digi-page-block-cards{display:flex;flex-direction:column;gap:var(--digi--responsive-grid-gutter)}";

const PageBlockCards = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.afVariation = undefined;
  }
  get cssModifiers() {
    return {
      [`digi-page-block-cards--variation-${this.afVariation}`]: !!this.afVariation
    };
  }
  render() {
    return (index.h("digi-layout-container", { class: Object.assign({ 'digi-page-block-cards': true }, this.cssModifiers) }, index.h("digi-card-box", { afWidth: cardBoxWidth_enum.CardBoxWidth.FULL }, index.h("div", { class: "digi-page-block-cards__inner" }, index.h("digi-typography-heading-section", null, index.h("slot", { name: "heading" })), index.h("digi-layout-stacked-blocks", null, index.h("slot", null))))));
  }
  static get assetsDirs() { return ["public"]; }
};
PageBlockCards.style = pageBlockCardsCss;

exports.digi_page_block_cards = PageBlockCards;
