'use strict';

exports.CardBoxGutter = void 0;
(function (CardBoxGutter) {
  CardBoxGutter["REGULAR"] = "regular";
  CardBoxGutter["NONE"] = "none";
})(exports.CardBoxGutter || (exports.CardBoxGutter = {}));
