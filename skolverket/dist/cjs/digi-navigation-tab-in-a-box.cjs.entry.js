'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');
require('./card-box-gutter.enum-585dded6.js');
const cardBoxWidth_enum = require('./card-box-width.enum-1a09a997.js');
require('./page-background.enum-c35f4c9a.js');
require('./layout-grid-vertical-spacing.enum-2b857671.js');
require('./layout-stacked-blocks-variation.enum-dd570a85.js');
require('./list-link-variation.enum-c43133c0.js');
require('./navigation-breadcrumbs-variation.enum-b46c1f09.js');
require('./notification-detail-variation.enum-658965de.js');
require('./page-footer-variation.enum-ec14686c.js');
require('./table-variation.enum-bc26c29e.js');

const slugify = (str) => str
  .toLowerCase()
  .trim()
  .replace(/å/g, 'a')
  .replace(/ä/g, 'a')
  .replace(/ö/g, 'o')
  .replace(/[^\w\s-]/g, '')
  .replace(/[\s_-]+/g, '-')
  .replace(/^-+|-+$/g, '');

const navigationTabInABoxCss = ".sc-digi-navigation-tab-in-a-box-h{display:contents}";

const NavigationTabInABox = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.afOnToggle = index.createEvent(this, "afOnToggle", 7);
    this.afAriaLabel = undefined;
    this.afId = undefined;
    this.afActive = undefined;
  }
  toggleHandler(activeTab) {
    if (activeTab || null) {
      this.afOnToggle.emit(activeTab);
    }
  }
  render() {
    return (index.h("digi-navigation-tab", { afAriaLabel: this.afAriaLabel, afId: this.afId || slugify(this.afAriaLabel), afActive: this.afActive, onAfOnToggle: (e) => this.afOnToggle.emit(e.detail) }, index.h("digi-card-box", { afWidth: cardBoxWidth_enum.CardBoxWidth.FULL }, index.h("digi-typography", null, index.h("slot", null)))));
  }
  static get watchers() { return {
    "afActive": ["toggleHandler"]
  }; }
};
NavigationTabInABox.style = navigationTabInABoxCss;

exports.digi_navigation_tab_in_a_box = NavigationTabInABox;
