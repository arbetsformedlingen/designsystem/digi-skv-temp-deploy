'use strict';

exports.TagSize = void 0;
(function (TagSize) {
  TagSize["SMALL"] = "small";
  TagSize["MEDIUM"] = "medium";
  TagSize["LARGE"] = "large";
})(exports.TagSize || (exports.TagSize = {}));
