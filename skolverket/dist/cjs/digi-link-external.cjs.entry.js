'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');
const linkExternalVariation_enum = require('./link-external-variation.enum-864f5ac7.js');

const linkExternalCss = ".sc-digi-link-external-h{--digi--link-external--gap:var(--digi--gutter--medium);--digi--link-external--color--default:var(--digi--color--text--link);--digi--link-external--color--hover:var(--digi--color--text--link-hover);--digi--link-external--color--visited:var(--digi--color--text--link-visited);--digi--link-external--font-size--large:var(--digi--typography--link--font-size--desktop-large);--digi--link-external--font-size--small:var(--digi--typography--link--font-size--desktop);--digi--link-external--font-family:var(--digi--global--typography--font-family--default);--digi--link-external--font-weight:var(--digi--typography--link--font-weight--desktop);--digi--link-external--text-decoration--default:var(--digi--global--typography--text-decoration--default);--digi--link-external--text-decoration--hover:var(--digi--typography--link--text-decoration--desktop);--digi--link-external--text-decoration--icon--default:var(--digi--global--typography--text-decoration--default);--digi--link-external--text-decoration--icon--hover:var(--digi--typography--link--text-decoration--desktop)}.sc-digi-link-external-h .digi-link-external.sc-digi-link-external{--digi--link--gap:var(--digi--link-external--gap);--digi--link--color--default:var(--digi--link-external--color--default);--digi--link--color--hover:var(--digi--link-external--color--hover);--digi--link--color--visited:var(--digi--link-external--color--visited);--digi--link--font-size--large:var(--digi--link-external--font-size--large);--digi--link--font-size--small:var(--digi--link-external--font-size--small);--digi--link--font-family:var(--digi--link-external--font-family);--digi--link--font-weight:var(--digi--link-external--font-weight);--digi--link--text-decoration--default:var(--digi--link-external--text-decoration--default);--digi--link--text-decoration--hover:var(--digi--link-external--text-decoration--hover);--digi--link--text-decoration--icon--default:var(--digi--link-external--text-decoration--icon--default);--digi--link--text-decoration--icon--hover:var(--digi--link-external--text-decoration--icon--hover)}";

const LinkExternal = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.afOnClick = index.createEvent(this, "afOnClick", 7);
    this.afHref = undefined;
    this.afVariation = linkExternalVariation_enum.LinkExternalVariation.SMALL;
    this.afTarget = undefined;
    this.afOverrideLink = false;
  }
  clickLinkHandler(e) {
    e.stopImmediatePropagation();
    this.afOnClick.emit(e.detail);
  }
  initRouting() {
    const tabIndex = this.hostElement.getAttribute('tabIndex');
    if (!!tabIndex) {
      setTimeout(async () => {
        const linkElement = await this.hostElement
          .querySelector('digi-link')
          .afMGetLinkElement();
        tabIndex === '0' && linkElement.setAttribute('tabIndex', '-1');
      }, 0);
    }
  }
  componentDidLoad() {
    this.initRouting();
  }
  get cssModifiers() {
    return {
      [`digi-link-external--variation-${this.afVariation}`]: !!this.afVariation
    };
  }
  render() {
    return (index.h("digi-link", { class: Object.assign({ 'digi-link-external': true }, this.cssModifiers), afVariation: this.afVariation, afHref: this.afHref, afOverrideLink: this.afOverrideLink, onAfOnClick: (e) => this.clickLinkHandler(e), afTarget: this.afTarget }, index.h("digi-icon", { class: "digi-link-external__icon", "aria-hidden": "true", slot: "icon", afName: `external-link-alt` }), index.h("slot", null)));
  }
  get hostElement() { return index.getElement(this); }
};
LinkExternal.style = linkExternalCss;

exports.digi_link_external = LinkExternal;
