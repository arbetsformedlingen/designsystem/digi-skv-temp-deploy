'use strict';

exports.LayoutGridVerticalSpacing = void 0;
(function (LayoutGridVerticalSpacing) {
  LayoutGridVerticalSpacing["REGULAR"] = "regular";
  LayoutGridVerticalSpacing["NONE"] = "none";
})(exports.LayoutGridVerticalSpacing || (exports.LayoutGridVerticalSpacing = {}));
