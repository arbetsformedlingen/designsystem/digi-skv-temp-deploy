'use strict';

exports.ListLinkLayout = void 0;
(function (ListLinkLayout) {
  ListLinkLayout["BLOCK"] = "block";
  ListLinkLayout["INLINE"] = "inline";
})(exports.ListLinkLayout || (exports.ListLinkLayout = {}));

exports.ListLinkType = void 0;
(function (ListLinkType) {
  ListLinkType["UNORDERED"] = "ul";
  ListLinkType["ORDERED"] = "ol";
})(exports.ListLinkType || (exports.ListLinkType = {}));

exports.ListLinkVariation = void 0;
(function (ListLinkVariation) {
  ListLinkVariation["REGULAR"] = "regular";
  ListLinkVariation["COMPACT"] = "compact";
})(exports.ListLinkVariation || (exports.ListLinkVariation = {}));
