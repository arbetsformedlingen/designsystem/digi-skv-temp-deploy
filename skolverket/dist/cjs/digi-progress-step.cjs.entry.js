'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');
const randomIdGenerator_util = require('./randomIdGenerator.util-9002c72c.js');
const progressStepVariation_enum = require('./progress-step-variation.enum-596ab007.js');

const progressStepCss = ".sc-digi-progress-step-h{--digi--progress-step--indicator--color--primary:var(--digi--color--icons--success);--digi--progress-step--indicator--color--secondary:var(--digi--color--icons--secondary);--digi--progress-step--heading--font-size:var(--digi--typography--heading-3--font-size--desktop);--digi--progress-step--heading--font-weight:var(--digi--typography--heading-3--font-weight--desktop)}.sc-digi-progress-step-h .digi-progress-step.sc-digi-progress-step{--INDICATOR--LINE--COLOR:var(--digi--color--border--primary);font-family:var(--digi--global--typography--font-family--default);display:flex;gap:var(--digi--padding--medium);flex-direction:row}.sc-digi-progress-step-h .digi-progress-step--primary.sc-digi-progress-step{--INDICATOR--CIRCLE--COLOR:var(--digi--progress-step--indicator--color--primary)}.sc-digi-progress-step-h .digi-progress-step--secondary.sc-digi-progress-step{--INDICATOR--CIRCLE--COLOR:var(--digi--progress-step--indicator--color--secondary)}.sc-digi-progress-step-h .digi-progress-step--done.sc-digi-progress-step{--INDICATOR--LINE--COLOR:var(--INDICATOR--CIRCLE--COLOR)}.sc-digi-progress-step-h .digi-progress-step--done.sc-digi-progress-step .digi-progress-step__indicator--circle.sc-digi-progress-step{background-color:var(--INDICATOR--CIRCLE--COLOR)}.sc-digi-progress-step-h .digi-progress-step--current.sc-digi-progress-step .digi-progress-step__indicator--circle.sc-digi-progress-step{background-color:var(--INDICATOR--CIRCLE--COLOR);box-shadow:inset 0 0 0 2px var(--INDICATOR--CIRCLE--COLOR), inset 0 0 0 4px white}.sc-digi-progress-step-h .digi-progress-step--upcoming.sc-digi-progress-step{--INDICATOR--CIRCLE--COLOR:var(--INDICATOR--LINE--COLOR)}.sc-digi-progress-step-h .digi-progress-step__indicator.sc-digi-progress-step{display:flex;align-items:center;flex-basis:26px;flex-shrink:0;flex-direction:column;transform:translateY(2px);margin-left:2px}.sc-digi-progress-step-h .digi-progress-step__indicator--circle.sc-digi-progress-step{height:26px;width:100%;border-radius:13px;display:block;flex-shrink:0;box-shadow:inset 0 0 0 2px var(--INDICATOR--CIRCLE--COLOR)}.sc-digi-progress-step-h .digi-progress-step__indicator--line.sc-digi-progress-step{width:2px;flex-grow:1;display:block;background-color:var(--INDICATOR--LINE--COLOR)}.sc-digi-progress-step-h .digi-progress-step__content.sc-digi-progress-step{max-width:var(--digi--paragraph-width--medium);padding-bottom:var(--digi--padding--largest)}.sc-digi-progress-step-h .digi-progress-step__content--heading.sc-digi-progress-step{font-weight:var(--digi--progress-step--heading--font-weight);font-size:var(--digi--progress-step--heading--font-size);margin:0;margin-bottom:var(--digi--padding--smaller)}.sc-digi-progress-step-h .digi-progress-step--last.sc-digi-progress-step .digi-progress-step__indicator--line.sc-digi-progress-step{display:none}";

const ProgressStep = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.afHeading = undefined;
    this.afHeadingLevel = progressStepVariation_enum.ProgressStepHeadingLevel.H2;
    this.afStepStatus = progressStepVariation_enum.ProgressStepStatus.UPCOMING;
    this.afVariation = progressStepVariation_enum.ProgressStepVariation.PRIMARY;
    this.afIsLast = false;
    this.afId = randomIdGenerator_util.randomIdGenerator('digi-progress-step');
  }
  render() {
    return (index.h("div", { "aria-current": this.afStepStatus == progressStepVariation_enum.ProgressStepStatus.CURRENT && 'step', role: "listitem", class: {
        'digi-progress-step': true,
        [`digi-progress-step--${this.afStepStatus}`]: true,
        'digi-progress-step--last': this.afIsLast,
        'digi-progress-step--primary': this.afVariation == progressStepVariation_enum.ProgressStepVariation.PRIMARY,
        'digi-progress-step--secondary': this.afVariation == progressStepVariation_enum.ProgressStepVariation.SECONDARY
      } }, index.h("span", { class: "digi-progress-step__indicator" }, index.h("span", { class: "digi-progress-step__indicator--circle" }), index.h("span", { class: "digi-progress-step__indicator--line" })), index.h("div", { class: "digi-progress-step__content" }, index.h(this.afHeadingLevel, { class: "digi-progress-step__content--heading", id: `${this.afId}--heading` }, this.afHeading), index.h("digi-typography", null, index.h("slot", null)))));
  }
  get hostElement() { return index.getElement(this); }
};
ProgressStep.style = progressStepCss;

exports.digi_progress_step = ProgressStep;
