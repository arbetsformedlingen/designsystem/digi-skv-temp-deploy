'use strict';

exports.CodeLanguage = void 0;
(function (CodeLanguage) {
  CodeLanguage["JSON"] = "json";
  CodeLanguage["CSS"] = "css";
  CodeLanguage["SCSS"] = "scss";
  CodeLanguage["TYPESCRIPT"] = "typescript";
  CodeLanguage["JAVASCRIPT"] = "javascript";
  CodeLanguage["BASH"] = "bash";
  CodeLanguage["HTML"] = "html";
  CodeLanguage["GIT"] = "git";
})(exports.CodeLanguage || (exports.CodeLanguage = {}));

exports.CodeVariation = void 0;
(function (CodeVariation) {
  CodeVariation["LIGHT"] = "light";
  CodeVariation["DARK"] = "dark";
})(exports.CodeVariation || (exports.CodeVariation = {}));
