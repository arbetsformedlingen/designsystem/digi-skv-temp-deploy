'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');
const randomIdGenerator_util = require('./randomIdGenerator.util-9002c72c.js');
const buttonSize_enum = require('./button-size.enum-a46b7684.js');
require('./button-type.enum-322621ea.js');
const buttonVariation_enum = require('./button-variation.enum-9364587a.js');
require('./expandable-accordion-variation.enum-2e394d6b.js');
require('./calendar-week-view-heading-level.enum-696abd05.js');
require('./form-radiobutton-variation.enum-cd8ac648.js');
require('./code-block-variation.enum-a28057da.js');
require('./code-example-variation.enum-a954aaf0.js');
require('./code-variation.enum-4c625938.js');
require('./form-checkbox-variation.enum-04455f3e.js');
require('./form-file-upload-variation.enum-26e4a9bd.js');
require('./form-input-search-variation.enum-5ea4a951.js');
require('./form-input-type.enum-7f96d46d.js');
require('./form-input-variation.enum-f95a25ea.js');
require('./form-select-variation.enum-139eab4f.js');
require('./form-textarea-variation.enum-8f1a5f71.js');
require('./form-validation-message-variation.enum-9244655f.js');
require('./layout-block-variation.enum-ae7e0e5f.js');
require('./layout-columns-variation.enum-9b7242b1.js');
require('./layout-container-variation.enum-7af59e50.js');
require('./layout-media-object-alignment.enum-e770b17d.js');
require('./link-external-variation.enum-864f5ac7.js');
require('./link-internal-variation.enum-354aedf4.js');
require('./link-variation.enum-55cb3944.js');
require('./loader-spinner-size.enum-96d3508e.js');
require('./media-figure-alignment.enum-5327a385.js');
require('./navigation-context-menu-item-type.enum-105e809f.js');
require('./navigation-sidebar-variation.enum-fa604f57.js');
require('./navigation-vertical-menu-variation.enum-40269ed5.js');
require('./progress-step-variation.enum-596ab007.js');
require('./progress-steps-variation.enum-3bbe5656.js');
require('./progressbar-variation.enum-81e49354.js');
require('./tag-size.enum-774d54e2.js');
require('./typography-meta-variation.enum-c7469a03.js');
require('./typography-time-variation.enum-4526d4ae.js');
require('./typography-variation.enum-08e58c63.js');
require('./util-breakpoint-observer-breakpoints.enum-9f89c8ce.js');

const navigationPaginationCss = ".digi-navigation-pagination.sc-digi-navigation-pagination{padding:var(--digi--gutter--largest);display:grid;grid-template-rows:min-content auto;grid-template-areas:\"result\" \"pagination\"}@media (min-width: 48rem){.digi-navigation-pagination.sc-digi-navigation-pagination{gap:var(--digi--gutter--medium)}}.digi-navigation-pagination__result.sc-digi-navigation-pagination{grid-area:result}@media (max-width: 47.9375rem){.digi-navigation-pagination__result.sc-digi-navigation-pagination{display:none}}.digi-navigation-pagination__pagination.sc-digi-navigation-pagination{grid-area:pagination;display:grid;grid-template-columns:1fr;grid-template-areas:\"select\" \"buttons\";gap:var(--digi--gutter--medium);justify-content:space-between;align-items:end}@media (min-width: 48rem){.digi-navigation-pagination__pagination.sc-digi-navigation-pagination{grid-template-columns:auto auto;grid-template-areas:\"select buttons\"}}.digi-navigation-pagination__buttons.sc-digi-navigation-pagination{grid-area:buttons;display:grid;grid-template-columns:repeat(2, 1fr);gap:var(--digi--gutter--icon);align-items:center}@media (min-width: 48rem){.digi-navigation-pagination__buttons.sc-digi-navigation-pagination{grid-template-columns:repeat(2, auto)}}.digi-navigation-pagination__button.sc-digi-navigation-pagination{grid-column:1}.digi-navigation-pagination__button--hidden.sc-digi-navigation-pagination{display:none}.digi-navigation-pagination__button.sc-digi-navigation-pagination:nth-child(2){grid-column:2}.digi-navigation-pagination__select.sc-digi-navigation-pagination{min-width:150px}.digi-navigation-pagination__aria-label.sc-digi-navigation-pagination{clip:rect(0 0 0 0);height:1px;padding:0;white-space:nowrap;width:1px;overflow:hidden;position:absolute}";

const NavigationPagination = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.afOnPageChange = index.createEvent(this, "afOnPageChange", 7);
    this.pages = [];
    this.currentPage = undefined;
    this.afInitActivePage = 1;
    this.afTotalPages = undefined;
    this.afCurrentResultStart = undefined;
    this.afId = randomIdGenerator_util.randomIdGenerator('digi-navigation-pagination');
    this.afCurrentResultEnd = undefined;
    this.afTotalResults = 0;
    this.afResultName = 'träffar';
  }
  afInitActivePageChanged() {
    this.setCurrentPage(this.afInitActivePage, false);
  }
  /**
   * Kan användas för att manuellt sätta om den aktiva sidan.
   * @en Can be used to set the active page.
   */
  async afMSetCurrentPage(pageNumber) {
    this.setCurrentPage(pageNumber, false);
  }
  prevPage() {
    this.currentPage--;
    this.afOnPageChange.emit(this.currentPage);
  }
  nextPage() {
    this.currentPage++;
    this.afOnPageChange.emit(this.currentPage);
  }
  setCurrentPage(pageToSet = this.currentPage, emitEvent = true) {
    if (pageToSet === this.currentPage) {
      return;
    }
    this.currentPage =
      pageToSet <= 1
        ? 1
        : pageToSet <= this.afTotalPages
          ? pageToSet
          : this.afTotalPages;
    console.log(pageToSet, this.currentPage);
    if (emitEvent) {
      this.afOnPageChange.emit(this.currentPage);
    }
  }
  componentWillLoad() {
    this.setCurrentPage(this.afInitActivePage, false);
    this.updateTotalPages();
  }
  updateTotalPages() {
    if (this.afTotalPages) {
      this.pages = [...Array(this.afTotalPages)];
    }
  }
  isCurrentPage(currentPage, page) {
    if (currentPage === page || null) {
      return 'page';
    }
  }
  labelledby() {
    if (this.afTotalResults > 0) {
      return `
        ${this.afId}-aria-label
        ${this.afId}-result-show
        ${this.afId}-result-current
        ${this.afId}-result-of
        ${this.afId}-result-total
        ${this.afId}-result-name
      `;
    }
    else {
      return `${this.afId}-aria-label`;
    }
  }
  render() {
    return (index.h("div", { class: "digi-navigation-pagination" }, index.h("span", { id: `${this.afId}-aria-label`, "aria-hidden": "true", class: "digi-navigation-pagination__aria-label" }, "Paginering"), this.afTotalResults > 0 && (index.h("div", { class: {
        'digi-navigation-pagination__result': true,
        'digi-navigation-pagination__result--pages': this.pages.length > 1
      }, "aria-hidden": this.pages.length > 1 ? 'true' : 'false', id: `${this.afId}-result` }, index.h("digi-typography", null, index.h("span", { id: `${this.afId}-result-show` }, "Visar "), index.h("strong", { id: `${this.afId}-result-current` }, this.afCurrentResultStart, "-", this.afCurrentResultEnd), index.h("span", { id: `${this.afId}-result-of` }, " av "), index.h("strong", { id: `${this.afId}-result-total` }, this.afTotalResults), this.afResultName && (index.h("span", { id: `${this.afId}-result-name` }, ` ${this.afResultName}`))))), this.pages.length > 1 && (index.h("nav", { "aria-labelledby": this.labelledby(), class: {
        'digi-navigation-pagination__pagination': true
      } }, index.h("div", { class: "digi-navigation-pagination__select" }, index.h("digi-form-select", { afLabel: `Sida`, afValue: `${this.currentPage}`, onAfOnChange: (e) => {
        this.setCurrentPage(parseInt(e.detail.target.value), true);
      } }, this.pages.map((_, index$1) => (index.h("option", { value: `${index$1 + 1}`, selected: index$1 + 1 === this.currentPage }, index$1 + 1, " av ", this.afTotalPages))))), index.h("div", { class: "digi-navigation-pagination__buttons" }, index.h("digi-button", { onClick: () => this.prevPage(), afVariation: buttonVariation_enum.ButtonVariation.SECONDARY, afSize: buttonSize_enum.ButtonSize.LARGE, afFullWidth: true, class: {
        'digi-navigation-pagination__button digi-navigation-pagination__button--previous': true,
        'digi-navigation-pagination__button--keep-left': this.currentPage === this.pages.length,
        'digi-navigation-pagination__button--hidden': this.currentPage === 1
      } }, index.h("digi-icon", { slot: "icon", afName: `chevron-left` }), index.h("span", null, "F\u00F6reg\u00E5ende")), index.h("digi-button", { onClick: () => this.nextPage(), afVariation: buttonVariation_enum.ButtonVariation.SECONDARY, afSize: buttonSize_enum.ButtonSize.LARGE, afFullWidth: true, class: {
        'digi-navigation-pagination__button digi-navigation-pagination__button--next': true,
        'digi-navigation-pagination__button--keep-right': this.currentPage === 1,
        'digi-navigation-pagination__button--hidden': this.currentPage === this.afTotalPages
      }, "af-variation": "secondary" }, index.h("digi-icon", { slot: "icon-secondary", afName: `chevron-right` }), index.h("span", null, "N\u00E4sta")))))));
  }
  static get watchers() { return {
    "afInitActivePage": ["afInitActivePageChanged"],
    "afTotalPages": ["updateTotalPages"]
  }; }
};
NavigationPagination.style = navigationPaginationCss;

exports.digi_navigation_pagination = NavigationPagination;
