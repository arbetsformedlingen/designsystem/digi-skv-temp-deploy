'use strict';

exports.NavigationBreadcrumbsVariation = void 0;
(function (NavigationBreadcrumbsVariation) {
  NavigationBreadcrumbsVariation["REGULAR"] = "regular";
  NavigationBreadcrumbsVariation["COMPRESSED"] = "compressed";
})(exports.NavigationBreadcrumbsVariation || (exports.NavigationBreadcrumbsVariation = {}));
