'use strict';

var LoggerType;
(function (LoggerType) {
  LoggerType["LOG"] = "log";
  LoggerType["INFO"] = "info";
  LoggerType["WARN"] = "warn";
  LoggerType["ERROR"] = "error";
})(LoggerType || (LoggerType = {}));

var LoggerSource;
(function (LoggerSource) {
  LoggerSource["CORE"] = "@af/digi-core";
  LoggerSource["NG"] = "@af/digi-ng";
})(LoggerSource || (LoggerSource = {}));

const colors = {
  log: {
    bg: '#1616b2',
    text: '#fff'
  },
  info: {
    bg: '#058470',
    text: '#fff'
  },
  warn: {
    bg: '#fddc41',
    text: '#333'
  },
  error: {
    bg: '#e21c50',
    text: '#fff'
  }
};
function outputLog(message, host, type = LoggerType.LOG, source = LoggerSource.CORE) {
  console[type](`%c${source}: ${message || ''}`, `color: ${colors[type].text}; background-color: ${colors[type].bg};`, host || null);
}
const logger = {
  log: (message, host, source = LoggerSource.CORE) => {
    outputLog(message, host, LoggerType.LOG, source);
  },
  info: (message, host, source = LoggerSource.CORE) => {
    outputLog(message, host, LoggerType.INFO, source);
  },
  warn: (message, host, source = LoggerSource.CORE) => {
    outputLog(message, host, LoggerType.WARN, source);
  },
  error: (message, host, source = LoggerSource.CORE) => {
    outputLog(message, host, LoggerType.ERROR, source);
  }
};

exports.logger = logger;
