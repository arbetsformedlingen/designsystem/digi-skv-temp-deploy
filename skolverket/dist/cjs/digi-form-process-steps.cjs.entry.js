'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');
require('./button-size.enum-a46b7684.js');
const buttonType_enum = require('./button-type.enum-322621ea.js');
const buttonVariation_enum = require('./button-variation.enum-9364587a.js');
require('./expandable-accordion-variation.enum-2e394d6b.js');
require('./calendar-week-view-heading-level.enum-696abd05.js');
require('./form-radiobutton-variation.enum-cd8ac648.js');
require('./code-block-variation.enum-a28057da.js');
require('./code-example-variation.enum-a954aaf0.js');
require('./code-variation.enum-4c625938.js');
require('./form-checkbox-variation.enum-04455f3e.js');
require('./form-file-upload-variation.enum-26e4a9bd.js');
require('./form-input-search-variation.enum-5ea4a951.js');
require('./form-input-type.enum-7f96d46d.js');
require('./form-input-variation.enum-f95a25ea.js');
require('./form-select-variation.enum-139eab4f.js');
require('./form-textarea-variation.enum-8f1a5f71.js');
require('./form-validation-message-variation.enum-9244655f.js');
require('./layout-block-variation.enum-ae7e0e5f.js');
require('./layout-columns-variation.enum-9b7242b1.js');
require('./layout-container-variation.enum-7af59e50.js');
require('./layout-media-object-alignment.enum-e770b17d.js');
require('./link-external-variation.enum-864f5ac7.js');
require('./link-internal-variation.enum-354aedf4.js');
require('./link-variation.enum-55cb3944.js');
require('./loader-spinner-size.enum-96d3508e.js');
require('./media-figure-alignment.enum-5327a385.js');
require('./navigation-context-menu-item-type.enum-105e809f.js');
require('./navigation-sidebar-variation.enum-fa604f57.js');
require('./navigation-vertical-menu-variation.enum-40269ed5.js');
require('./progress-step-variation.enum-596ab007.js');
require('./progress-steps-variation.enum-3bbe5656.js');
require('./progressbar-variation.enum-81e49354.js');
require('./tag-size.enum-774d54e2.js');
require('./typography-meta-variation.enum-c7469a03.js');
require('./typography-time-variation.enum-4526d4ae.js');
require('./typography-variation.enum-08e58c63.js');
require('./util-breakpoint-observer-breakpoints.enum-9f89c8ce.js');
const randomIdGenerator_util = require('./randomIdGenerator.util-9002c72c.js');

const formProcessStepsCss = ".digi-form-process-steps.sc-digi-form-process-steps{background:var(--digi--color--background--primary);padding:var(--digi--gutter--largest-3) var(--digi--gutter--largest);overflow:hidden;display:flex;flex-direction:column;opacity:0;transition:opacity 0.05s ease-in-out}.digi-form-process-steps--fallback-is-set.sc-digi-form-process-steps{opacity:1}.digi-form-process-steps__toggle.sc-digi-form-process-steps{display:none}.digi-form-process-steps--fallback-true.sc-digi-form-process-steps .digi-form-process-steps__toggle.sc-digi-form-process-steps{display:flex;align-items:center;gap:var(--digi--gutter--larger);justify-content:space-between;-webkit-padding-after:var(--digi--gutter--medium);padding-block-end:var(--digi--gutter--medium);-webkit-border-after:var(--digi--border-width--tertiary) var(--digi--border-style--primary) var(--digi--global--color--profile--purple--opacity15);border-block-end:var(--digi--border-width--tertiary) var(--digi--border-style--primary) var(--digi--global--color--profile--purple--opacity15)}.digi-form-process-steps--fallback-true.sc-digi-form-process-steps .digi-form-process-steps__toggle.sc-digi-form-process-steps:focus-visible{outline:var(--digi--focus-outline)}.digi-form-process-steps__toggle-heading.sc-digi-form-process-steps{display:grid;grid-template-columns:auto 1fr;grid-auto-flow:columns;gap:var(--digi--gutter--smallest-4);align-items:center;text-align:start}.digi-form-process-steps__toggle-heading.sc-digi-form-process-steps::before{content:attr(data-current-step);font-family:var(--digi--global--typography--font-family--default);font-size:var(--digi--global--typography--font-size--small);width:var(--digi--global--spacing--larger);height:var(--digi--global--spacing--larger);border-radius:50%;display:flex;align-items:center;justify-content:center;background:var(--digi--color--background--inverted-1);border:1px solid var(--digi--color--border--primary);color:var(--digi--color--text--inverted)}.digi-form-process-steps__toggle-text.sc-digi-form-process-steps span.sc-digi-form-process-steps{font-family:var(--digi--global--typography--font-family--default);font-size:var(--digi--typography--font-size--desktop);font-weight:var(--digi--global--typography--font-weight--semibold);line-height:1.5;text-transform:uppercase;letter-spacing:0.02em;color:var(--digi--color--text--primary)}.digi-form-process-steps__toggle-text.sc-digi-form-process-steps p.sc-digi-form-process-steps{font-family:var(--digi--global--typography--font-family--default);text-decoration:none;color:var(--digi--color--text--secondary);font-size:var(--digi--global--typography--font-size--large);letter-spacing:calc(var(--digi--global--typography--font-size--large) / 100 * -1);font-weight:var(--digi--global--typography--font-weight--semibold);font-size:var(--digi--global--typography--font-size--small)}.digi-form-process-steps__toggle-text.sc-digi-form-process-steps p.sc-digi-form-process-steps:hover{color:var(--digi--global--color--profile--purple--dark);text-decoration:underline}.digi-form-process-steps__toggle-text.sc-digi-form-process-steps p.sc-digi-form-process-steps:focus{outline:none;color:var(--digi--color--text--secondary)}.digi-form-process-steps__toggle-text.sc-digi-form-process-steps p.sc-digi-form-process-steps:focus-visible{outline:var(--digi--border-width--secondary) solid var(--digi--color--border--focus)}.digi-form-process-steps__toggle-text.sc-digi-form-process-steps p.sc-digi-form-process-steps:visited{color:var(--digi--color--text--secondary)}.digi-form-process-steps__toggle-text.sc-digi-form-process-steps p.sc-digi-form-process-steps:hover{text-decoration:none}.digi-form-process-steps__toggle-label.sc-digi-form-process-steps{font-family:var(--digi--global--typography--font-family--default);font-size:var(--digi--typography--accordion--font-size--mobile);font-weight:var(--digi--typography--accordion--font-weight--desktop);display:inline-flex;align-items:center;gap:var(--digi--gutter--icon);font-size:var(--digi--global--typography--font-size--interaction-medium);color:var(--digi--color--text--secondary)}.digi-form-process-steps__toggle-label.sc-digi-form-process-steps digi-icon.sc-digi-form-process-steps{--digi--icon--color:currentColor}.digi-form-process-steps__items.sc-digi-form-process-steps{display:inline-flex;flex-direction:row;gap:var(--digi--gutter--largest);counter-reset:steps;margin:0;padding:0;list-style:none}.digi-form-process-steps--fallback-true.sc-digi-form-process-steps .digi-form-process-steps__items.sc-digi-form-process-steps{display:flex;flex-direction:column;gap:0}.digi-form-process-steps--fallback-true.digi-form-process-steps--expanded-false.sc-digi-form-process-steps .digi-form-process-steps__content.sc-digi-form-process-steps{overflow-y:hidden;height:0;visibility:hidden}.digi-form-process-steps--fallback-true.digi-form-process-steps--expanded-true.sc-digi-form-process-steps .digi-form-process-steps__content.sc-digi-form-process-steps{display:flex;gap:var(--digi--gutter--medium);flex-direction:column}.digi-form-process-steps__toggle-inside.sc-digi-form-process-steps{--digi--button--color--background--function--default:var(--digi--color--background--secondary);--digi--button--color--background--function--hover:var(--digi--color--background--tertiary)}.sc-digi-form-process-steps-s>li{margin:0;padding:0;list-style:none}";

const FormProcessSteps = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.isFallback = false;
    this.isExpanded = false;
    this.fallbackIsSet = false;
    this.listWidth = undefined;
    this.containerWidth = undefined;
    this.steps = undefined;
    this.afCurrentStep = undefined;
    this.afId = randomIdGenerator_util.randomIdGenerator('digi-form-process-steps');
  }
  handleWidthChange() {
    this.isFallback = this.listWidth > this.containerWidth;
    if (!this.fallbackIsSet) {
      this.fallbackIsSet = true;
    }
  }
  componentWillLoad() {
    this.setTypeOnChildren();
  }
  componentDidLoad() {
    this.measureItemsList();
  }
  componentWillUpdate() {
    this.setTypeOnChildren();
  }
  handleFocusWithin() {
    this.isExpanded = true;
  }
  measureItemsList() {
    const itemsList = this.hostElement.querySelector('ol');
    this.listWidth = itemsList.scrollWidth;
  }
  setTypeOnChildren() {
    const steps = this.hostElement.querySelectorAll('digi-form-process-step');
    this.steps = steps;
    steps.forEach((step, i) => {
      if (i + 1 === this.afCurrentStep) {
        step.afType = 'current';
      }
      else if (i + 1 < this.afCurrentStep) {
        step.afType = 'completed';
      }
      else {
        step.afType = 'upcoming';
      }
    });
  }
  setContextOnChildren() {
    const steps = this.hostElement.querySelectorAll('digi-form-process-step');
    steps.forEach((step) => (step.afContext = this.isFallback ? 'fallback' : 'regular'));
  }
  resizeHandler() {
    this.containerWidth = this._contentElement.getBoundingClientRect().width;
  }
  clickToggleHandler(e, resetFocus = false) {
    e.preventDefault();
    this.isExpanded = !this.isExpanded;
    if (resetFocus) {
      this._button.focus();
    }
  }
  get cssModifiers() {
    return {
      [`digi-form-process-steps--expanded-${this.isExpanded}`]: true,
      [`digi-form-process-steps--fallback-${this.isFallback}`]: true,
      [`digi-form-process-steps--fallback-is-set`]: this.fallbackIsSet
    };
  }
  render() {
    var _a;
    return (index.h("digi-util-resize-observer", { onAfOnChange: () => this.resizeHandler() }, index.h("div", { class: Object.assign({ 'digi-form-process-steps': true }, this.cssModifiers) }, this.isFallback && (index.h("button", { class: "digi-form-process-steps__toggle", type: "button", "aria-pressed": this.isExpanded ? 'true' : 'false', "aria-expanded": this.isExpanded ? 'true' : 'false', "aria-controls": `${this.afId}-content`, onClick: (e) => this.clickToggleHandler(e, true), ref: (el) => (this._button = el) }, index.h("div", { class: "digi-form-process-steps__toggle-heading", "data-current-step": this.afCurrentStep }, index.h("div", { class: "digi-form-process-steps__toggle-text" }, index.h("span", null, "Steg ", this.afCurrentStep, " av ", this.steps.length), index.h("p", null, (_a = this.steps[this.afCurrentStep - 1]) === null || _a === void 0 ? void 0 : _a.textContent))), index.h("span", { class: "digi-form-process-steps__toggle-label" }, this.isExpanded ? 'Dölj steg' : 'Visa alla steg', index.h("digi-icon", { afName: this.isExpanded ? 'chevron-up' : 'chevron-down', "aria-hidden": true, slot: "icon-secondary" })))), index.h("div", { class: "digi-form-process-steps__content", ref: (el) => (this._contentElement = el) }, index.h("ol", { class: "digi-form-process-steps__items", id: `${this.afId}-items`, onFocusin: () => this.handleFocusWithin() }, index.h("slot", null)), this.isFallback && (index.h("digi-button", { class: "digi-form-process-steps__toggle-inside", afVariation: buttonVariation_enum.ButtonVariation.FUNCTION, afType: buttonType_enum.ButtonType.BUTTON, onAfOnClick: (e) => this.clickToggleHandler(e.detail, true), afFullWidth: true, afAriaPressed: this.isExpanded, afAriaExpanded: this.isExpanded, afAriaControls: `${this.afId}-content` }, this.isExpanded ? 'Dölj' : 'Visa alla steg', index.h("digi-icon", { afName: this.isExpanded ? 'chevron-up' : 'chevron-down', "aria-hidden": true, slot: "icon-secondary" })))))));
  }
  get hostElement() { return index.getElement(this); }
  static get watchers() { return {
    "listWidth": ["handleWidthChange"],
    "containerWidth": ["handleWidthChange"],
    "afCurrentStep": ["setTypeOnChildren"],
    "isFallback": ["setContextOnChildren"]
  }; }
};
FormProcessSteps.style = formProcessStepsCss;

exports.digi_form_process_steps = FormProcessSteps;
