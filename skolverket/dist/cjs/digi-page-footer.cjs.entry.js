'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');
const pageFooterVariation_enum = require('./page-footer-variation.enum-ec14686c.js');

const pageFooterCss = ".sc-digi-page-footer-h{--digi--page-footer--columns:1}.digi-page-footer.sc-digi-page-footer{display:block;padding-block:var(--digi--global--spacing--largest-4);-webkit-border-before:var(--digi--border-width--secondary) solid var(--digi--color--border--secondary);border-block-start:var(--digi--border-width--secondary) solid var(--digi--color--border--secondary);background:var(--digi--color--background--tertiary)}.digi-page-footer__section.sc-digi-page-footer{grid-column:1/-1;display:grid;gap:var(--digi--responsive-grid-gutter);grid-template-columns:repeat(auto-fit, minmax(calc((100% - var(--digi--responsive-grid-gutter) * (var(--digi--page-footer--columns) - 1)) / var(--digi--page-footer--columns)), 1fr))}.digi-page-footer__section--top.sc-digi-page-footer{-webkit-padding-after:var(--digi--responsive-grid-gutter);padding-block-end:var(--digi--responsive-grid-gutter);-webkit-border-after:var(--digi--border-width--tertiary) var(--digi--border-style--primary) var(--digi--global--color--profile--purple--opacity60);border-block-end:var(--digi--border-width--tertiary) var(--digi--border-style--primary) var(--digi--global--color--profile--purple--opacity60)}@media (min-width: 48rem){.digi-page-footer__section--top.sc-digi-page-footer{--digi--page-footer--columns:2}}@media (min-width: 62rem){.digi-page-footer__section--top.sc-digi-page-footer{--digi--page-footer--columns:3}}@media (min-width: 80rem){.digi-page-footer__section--top.sc-digi-page-footer{--digi--page-footer--columns:4}}@media (min-width: 62rem){.digi-page-footer__section--bottom.sc-digi-page-footer{--digi--page-footer--columns:4}}@media (min-width: 80rem){.digi-page-footer__section--bottom.sc-digi-page-footer{--digi--page-footer--columns:2}}@media (min-width: 80rem){.digi-page-footer__top-first.sc-digi-page-footer{grid-column:span calc(var(--digi--page-footer--columns) / 2)}}@media (max-width: 61.9375rem){.digi-page-footer__bottom-end.sc-digi-page-footer{--digi--list-link--direction--override:column}}@media (min-width: 62rem){.digi-page-footer__bottom-end.sc-digi-page-footer{justify-self:end}}@media (max-width: 61.9375rem){.digi-page-footer__logo.sc-digi-page-footer{-webkit-padding-start:var(--digi--grid-gutter--smaller);padding-inline-start:var(--digi--grid-gutter--smaller)}}";

const PageFooter = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.afVariation = pageFooterVariation_enum.PageFooterVariation.PRIMARY;
  }
  get cssModifiers() {
    return {
      [`digi-page-footer--variation-${this.afVariation}`]: !!this.afVariation
    };
  }
  render() {
    return (index.h("digi-typography", { class: Object.assign({ 'digi-page-footer': true }, this.cssModifiers) }, index.h("digi-layout-grid", null, index.h("div", { class: "digi-page-footer__section digi-page-footer__section--top" }, index.h("div", { class: "digi-page-footer__top-first" }, index.h("slot", { name: "top-first" })), index.h("slot", { name: "top" })), index.h("div", { class: "digi-page-footer__section digi-page-footer__section--bottom" }, index.h("div", { class: "digi-page-footer__logo" }, index.h("digi-logo", { "af-title": "Skolverket", "af-desc": "Skolverket logo" })), index.h("div", { class: "digi-page-footer__bottom-end" }, index.h("slot", { name: "bottom" }))))));
  }
};
PageFooter.style = pageFooterCss;

exports.digi_page_footer = PageFooter;
