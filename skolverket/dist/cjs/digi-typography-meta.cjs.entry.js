'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');
const typographyMetaVariation_enum = require('./typography-meta-variation.enum-c7469a03.js');

const typographyMetaCss = ".sc-digi-typography-meta-h{--digi--typography-meta--font-weight--semibold:var(--digi--typography--preamble--font-weight--desktop);--digi--typography-meta--font-size:var(--digi--typography--preamble--font-size--desktop);--digi--typography-meta--font-family:var(--digi--global--typography--font-family--default);--digi--typography-meta--meta--font-weight:var(--digi--typography--body--font-weight--desktop);--digi--typography-meta--meta-secondary--font-weight:var(--digi--typography--body--font-weight--desktop)}.sc-digi-typography-meta-h .digi-typography-meta__meta.sc-digi-typography-meta,.sc-digi-typography-meta-h .digi-typography-meta__meta.sc-digi-typography-meta-s>*{font-weight:var(--digi--typography-meta--meta--font-weight)}.sc-digi-typography-meta-h .digi-typography-meta__meta-secondary.sc-digi-typography-meta,.sc-digi-typography-meta-h .digi-typography-meta__meta-secondary.sc-digi-typography-meta-s>*{font-weight:var(--digi--typography-meta--meta-secondary--font-weight)}.sc-digi-typography-meta-h .digi-typography-meta.sc-digi-typography-meta{font-size:var(--digi--typography-meta--font-size);font-family:var(--digi--typography-meta--font-family)}.sc-digi-typography-meta-h .digi-typography-meta--variation-primary.sc-digi-typography-meta .digi-typography-meta__meta.sc-digi-typography-meta{--digi--typography-meta--meta--font-weight:var(--digi--typography-meta--font-weight--semibold)}.sc-digi-typography-meta-h .digi-typography-meta--variation-secondary.sc-digi-typography-meta .digi-typography-meta__meta-secondary.sc-digi-typography-meta{--digi--typography-meta--meta-secondary--font-weight:var(--digi--typography-meta--font-weight--semibold)}";

const TypographyMeta = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.afVariation = typographyMetaVariation_enum.TypographyMetaVariation.PRIMARY;
  }
  get cssModifiers() {
    return {
      [`digi-typography-meta--variation-${this.afVariation}`]: !!this.afVariation
    };
  }
  render() {
    return (index.h("div", { class: Object.assign({ 'digi-typography-meta': true }, this.cssModifiers) }, index.h("div", { class: "digi-typography-meta__meta" }, index.h("slot", null)), index.h("div", { class: "digi-typography-meta__meta-secondary" }, index.h("slot", { name: "secondary" }))));
  }
};
TypographyMeta.style = typographyMetaCss;

exports.digi_typography_meta = TypographyMeta;
