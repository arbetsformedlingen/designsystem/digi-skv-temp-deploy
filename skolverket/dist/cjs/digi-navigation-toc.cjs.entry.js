'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');

const navigationTocCss = ".sc-digi-navigation-toc-h .sc-digi-navigation-toc-s>[slot^=heading]{--digi--heading-4--font-size:var(--digi--typography--heading-4--font-size--mobile);--digi--heading-4--line-height:var(--digi--typography--heading-4--line-height--mobile);font-family:var(--digi--global--typography--font-family--default);font-size:var(--digi--heading-4--font-size);line-height:var(--digi--heading-4--line-height);font-weight:var(--digi--typography--heading-4--font-weight--mobile);-webkit-margin-after:0;margin-block-end:0}@media (min-width: 48rem){.sc-digi-navigation-toc-h .sc-digi-navigation-toc-s>[slot^=heading]{--digi--heading-4--font-size:var(--digi--typography--heading-4--font-size--desktop);--digi--heading-4--line-height:var(--digi--typography--heading-4--line-height--desktop)}}@media (min-width: 62rem){.sc-digi-navigation-toc-h .sc-digi-navigation-toc-s>[slot^=heading]{--digi--heading-4--font-size:var(--digi--typography--heading-4--font-size--desktop-large);--digi--heading-4--line-height:var(--digi--typography--heading-4--line-height--desktop-large)}}.sc-digi-navigation-toc-h .sc-digi-navigation-toc-s>li{min-height:var(--digi--height--button-small);display:flex;align-items:center;padding-inline:var(--digi--gutter--large);border-radius:var(--digi--border-radius--primary)}.sc-digi-navigation-toc-h .sc-digi-navigation-toc-s>li a{text-decoration:none}.sc-digi-navigation-toc-h .sc-digi-navigation-toc-s>li a:hover{text-decoration:underline}.sc-digi-navigation-toc-h .sc-digi-navigation-toc-s>li a:focus,.sc-digi-navigation-toc-h .sc-digi-navigation-toc-s>li a:focus-visible{outline:none}.sc-digi-navigation-toc-h .sc-digi-navigation-toc-s>li:not(:active):focus-within{outline:var(--digi--focus-outline)}.digi-navigation-toc.sc-digi-navigation-toc{-webkit-padding-after:var(--digi--gutter--smallest-4);padding-block-end:var(--digi--gutter--smallest-4);-webkit-border-start:var(--digi--border-width--secondary) solid var(--digi--color--border--secondary);border-inline-start:var(--digi--border-width--secondary) solid var(--digi--color--border--secondary);border-inline-start-color:var(--digi--color--background--tertiary);border-inline-start-width:6px}@media (min-width: 48rem){.digi-navigation-toc.sc-digi-navigation-toc{-webkit-padding-after:var(--digi--gutter--small);padding-block-end:var(--digi--gutter--small)}}.digi-navigation-toc__heading.sc-digi-navigation-toc:not(:empty){min-height:var(--digi--height--button-small);display:flex;align-items:center;padding-inline:var(--digi--gutter--large)}.digi-navigation-toc__items.sc-digi-navigation-toc{list-style:none;padding:0;margin:0;display:flex;flex-direction:column}";

const NavigationToc = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.currentHash = undefined;
  }
  currentHashChanged() {
    this.setAriaCurrent();
  }
  componentWillLoad() {
    this.setHash();
  }
  setHash() {
    var _a;
    this.currentHash = ((_a = window === null || window === void 0 ? void 0 : window.location) === null || _a === void 0 ? void 0 : _a.hash) || '';
  }
  setAriaCurrent() {
    const links = this.hostElement.querySelectorAll('a');
    links.forEach((link) => {
      if (link.hash === this.currentHash) {
        link.setAttribute('aria-current', 'true');
      }
      else {
        link.removeAttribute('aria-current');
      }
    });
  }
  render() {
    return (index.h("div", { class: {
        'digi-navigation-toc': true
      } }, index.h("div", { class: "digi-navigation-toc__heading" }, index.h("slot", { name: "heading" })), index.h("digi-typography", null, index.h("ul", { class: "digi-navigation-toc__items" }, index.h("slot", null)))));
  }
  get hostElement() { return index.getElement(this); }
  static get watchers() { return {
    "currentHash": ["currentHashChanged"]
  }; }
};
NavigationToc.style = navigationTocCss;

exports.digi_navigation_toc = NavigationToc;
