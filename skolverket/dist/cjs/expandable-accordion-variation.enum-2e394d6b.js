'use strict';

exports.CalendarButtonSize = void 0;
(function (CalendarButtonSize) {
  CalendarButtonSize["SMALL"] = "small";
  CalendarButtonSize["MEDIUM"] = "medium";
  CalendarButtonSize["LARGE"] = "large";
})(exports.CalendarButtonSize || (exports.CalendarButtonSize = {}));

exports.CalendarSelectSize = void 0;
(function (CalendarSelectSize) {
  CalendarSelectSize["SMALL"] = "small";
  CalendarSelectSize["MEDIUM"] = "medium";
  CalendarSelectSize["LARGE"] = "large";
})(exports.CalendarSelectSize || (exports.CalendarSelectSize = {}));

exports.ExpandableAccordionHeaderLevel = void 0;
(function (ExpandableAccordionHeaderLevel) {
  ExpandableAccordionHeaderLevel["H1"] = "h1";
  ExpandableAccordionHeaderLevel["H2"] = "h2";
  ExpandableAccordionHeaderLevel["H3"] = "h3";
  ExpandableAccordionHeaderLevel["H4"] = "h4";
  ExpandableAccordionHeaderLevel["H5"] = "h5";
  ExpandableAccordionHeaderLevel["H6"] = "h6";
})(exports.ExpandableAccordionHeaderLevel || (exports.ExpandableAccordionHeaderLevel = {}));

exports.ExpandableAccordionToggleType = void 0;
(function (ExpandableAccordionToggleType) {
  ExpandableAccordionToggleType["IMPLICIT"] = "implicit";
  ExpandableAccordionToggleType["EXPLICIT"] = "explicit";
})(exports.ExpandableAccordionToggleType || (exports.ExpandableAccordionToggleType = {}));

exports.ExpandableAccordionVariation = void 0;
(function (ExpandableAccordionVariation) {
  ExpandableAccordionVariation["PRIMARY"] = "primary";
  ExpandableAccordionVariation["SECONDARY"] = "secondary";
})(exports.ExpandableAccordionVariation || (exports.ExpandableAccordionVariation = {}));
