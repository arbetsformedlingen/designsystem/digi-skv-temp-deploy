'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-dbd3fd76.js');
const typographyTimeVariation_enum = require('./typography-time-variation.enum-4526d4ae.js');

const TypographyTime = class {
  constructor(hostRef) {
    index.registerInstance(this, hostRef);
    this.formatedDate = undefined;
    this.datetime = undefined;
    this.afDateTime = undefined;
    this.afVariation = typographyTimeVariation_enum.TypographyTimeVariation.PRIMARY;
  }
  primaryFormat(date) {
    return date.toLocaleString('default', {
      year: 'numeric',
      month: '2-digit',
      day: '2-digit'
    });
  }
  prettyFormat(date) {
    return date.toLocaleString('default', {
      day: 'numeric',
      month: 'long',
      year: 'numeric'
    });
  }
  distanceFormat(date) {
    const oneDay = 1000 * 3600 * 24;
    const distance = Math.floor(Date.now() - date.getTime());
    let days = Math.floor(distance / oneDay);
    days = Math.abs(days);
    if (distance > oneDay) {
      return `För ${days} dagar sedan`;
    }
    if (distance < oneDay) {
      return days === 0 ? `Idag` : `Om ${days} dagar`;
    }
  }
  formatDate(dateToFormat) {
    const date = new Date(dateToFormat);
    switch (this.afVariation) {
      case typographyTimeVariation_enum.TypographyTimeVariation.PRIMARY:
        this.formatedDate = this.primaryFormat(date);
        break;
      case typographyTimeVariation_enum.TypographyTimeVariation.PRETTY:
        this.formatedDate = this.prettyFormat(date);
        break;
      case typographyTimeVariation_enum.TypographyTimeVariation.DISTANCE:
        this.formatedDate = this.distanceFormat(date);
        break;
    }
    this.datetime = this.primaryFormat(date);
  }
  componentWillLoad() {
    this.formatDate(this.afDateTime);
  }
  render() {
    return (index.h("time", { dateTime: this.datetime, class: "digi-typography-time" }, this.formatedDate));
  }
  static get watchers() { return {
    "afDateTime": ["formatDate"]
  }; }
};

exports.digi_typography_time = TypographyTime;
