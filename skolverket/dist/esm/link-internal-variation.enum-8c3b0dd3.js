var LinkInternalVariation;
(function (LinkInternalVariation) {
  LinkInternalVariation["SMALL"] = "small";
  LinkInternalVariation["LARGE"] = "large";
})(LinkInternalVariation || (LinkInternalVariation = {}));

export { LinkInternalVariation as L };
