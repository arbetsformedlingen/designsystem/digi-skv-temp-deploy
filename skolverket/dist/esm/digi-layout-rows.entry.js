import { r as registerInstance, h } from './index-7f342a75.js';

const layoutRowsCss = ".sc-digi-layout-rows-h{display:grid;grid-gap:var(--digi--responsive-grid-gutter);grid-auto-flow:row;width:100%}";

const LayoutRows = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
  }
  render() {
    return h("slot", null);
  }
};
LayoutRows.style = layoutRowsCss;

export { LayoutRows as digi_layout_rows };
