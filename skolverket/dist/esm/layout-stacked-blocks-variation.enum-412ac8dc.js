var LayoutStackedBlocksVariation;
(function (LayoutStackedBlocksVariation) {
  LayoutStackedBlocksVariation["REGULAR"] = "regular";
  LayoutStackedBlocksVariation["ENHANCED"] = "enhanced";
})(LayoutStackedBlocksVariation || (LayoutStackedBlocksVariation = {}));

export { LayoutStackedBlocksVariation as L };
