import { d as detectClosest } from './detectClosest.util-2d3999b7.js';

function detectClickOutside(target, selector) {
  return !detectClosest(target, selector);
}

export { detectClickOutside as d };
