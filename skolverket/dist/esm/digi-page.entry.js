import { r as registerInstance, h, a as getAssetPath } from './index-7f342a75.js';

const pageCss = ".digi-page.sc-digi-page{display:grid;grid-template-rows:min-content 1fr min-content;min-height:100vh;background:var(--digi--page--background-image) center center;grid-template-columns:100vw}";

const Page = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.afBackground = undefined;
  }
  render() {
    return (h("div", { class: "digi-page", style: {
        '--digi--page--background-image': this.afBackground
          ? `url('${getAssetPath(`./public/images/${this.afBackground}.svg`)}')`
          : null
      } }, h("div", { class: "digi-page__header" }, h("slot", { name: "header" })), h("div", { class: "digi-page__content" }, h("slot", null)), h("div", { class: "digi-page__footer" }, h("slot", { name: "footer" }))));
  }
  static get assetsDirs() { return ["public"]; }
};
Page.style = pageCss;

export { Page as digi_page };
