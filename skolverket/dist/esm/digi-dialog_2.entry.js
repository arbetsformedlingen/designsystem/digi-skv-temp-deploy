import { r as registerInstance, c as createEvent, h } from './index-7f342a75.js';
import { C as CardBoxGutter } from './card-box-gutter.enum-4af8f128.js';
import './card-box-width.enum-be3a34bc.js';
import { d as NotificationAlertVariation } from './page-background.enum-013f61b0.js';
import './layout-grid-vertical-spacing.enum-cbd27319.js';
import './layout-stacked-blocks-variation.enum-412ac8dc.js';
import './list-link-variation.enum-1f6ee434.js';
import './navigation-breadcrumbs-variation.enum-1ca5e289.js';
import './notification-detail-variation.enum-06ceda96.js';
import './page-footer-variation.enum-6e6e5a39.js';
import './table-variation.enum-cd5a5aa0.js';
import './button-size.enum-8999fdcb.js';
import './button-type.enum-95f504f6.js';
import { B as ButtonVariation } from './button-variation.enum-3cc46e30.js';
import './expandable-accordion-variation.enum-9166e001.js';
import './calendar-week-view-heading-level.enum-05020882.js';
import './form-radiobutton-variation.enum-60c58c7d.js';
import './code-block-variation.enum-42e2bb44.js';
import './code-example-variation.enum-fbd04208.js';
import './code-variation.enum-ae00e62c.js';
import './form-checkbox-variation.enum-ad992b3e.js';
import './form-file-upload-variation.enum-882cb26e.js';
import './form-input-search-variation.enum-04106f0e.js';
import './form-input-type.enum-58a900d7.js';
import './form-input-variation.enum-07b4b98a.js';
import './form-select-variation.enum-fbd25aa0.js';
import './form-textarea-variation.enum-9d7b381e.js';
import './form-validation-message-variation.enum-5784e9ac.js';
import './layout-block-variation.enum-dbd50b66.js';
import './layout-columns-variation.enum-26e6db94.js';
import './layout-container-variation.enum-d17100f2.js';
import './layout-media-object-alignment.enum-f3156fc7.js';
import './link-external-variation.enum-f6d7a12f.js';
import './link-internal-variation.enum-8c3b0dd3.js';
import './link-variation.enum-81c1b9cf.js';
import './loader-spinner-size.enum-4999231f.js';
import './media-figure-alignment.enum-79554408.js';
import './navigation-context-menu-item-type.enum-178d1050.js';
import './navigation-sidebar-variation.enum-443b2b03.js';
import './navigation-vertical-menu-variation.enum-37c685d3.js';
import './progress-step-variation.enum-1f263b15.js';
import './progress-steps-variation.enum-fb681025.js';
import './progressbar-variation.enum-de87bf7f.js';
import './tag-size.enum-2927e760.js';
import './typography-meta-variation.enum-1eb26125.js';
import './typography-time-variation.enum-79f1b0de.js';
import { T as TypographyVariation } from './typography-variation.enum-b073e180.js';
import './util-breakpoint-observer-breakpoints.enum-8088d0a3.js';

const dialogCss = ".sc-digi-dialog-h .sc-digi-dialog-s>[slot^=heading]{--digi--heading-4--font-size:var(--digi--typography--heading-4--font-size--mobile);--digi--heading-4--line-height:var(--digi--typography--heading-4--line-height--mobile);font-family:var(--digi--global--typography--font-family--default);font-size:var(--digi--heading-4--font-size);line-height:var(--digi--heading-4--line-height);font-weight:var(--digi--typography--heading-4--font-weight--mobile);-webkit-margin-after:0;margin-block-end:0}@media (min-width: 48rem){.sc-digi-dialog-h .sc-digi-dialog-s>[slot^=heading]{--digi--heading-4--font-size:var(--digi--typography--heading-4--font-size--desktop);--digi--heading-4--line-height:var(--digi--typography--heading-4--line-height--desktop)}}@media (min-width: 62rem){.sc-digi-dialog-h .sc-digi-dialog-s>[slot^=heading]{--digi--heading-4--font-size:var(--digi--typography--heading-4--font-size--desktop-large);--digi--heading-4--line-height:var(--digi--typography--heading-4--line-height--desktop-large)}}.digi-dialog.sc-digi-dialog{border:none;padding:0;border-radius:var(--digi--border-radius--primary);box-shadow:0px 8px 16px 0px rgba(105, 40, 89, 0.2);max-width:calc(100vw - var(--digi--container-gutter--medium) * 2);max-height:calc(100vh - var(--digi--container-gutter--medium) * 2);overflow:hidden}.digi-dialog.sc-digi-dialog::backdrop{background:rgba(0, 0, 0, 0.4)}@media (max-width: 47.9375rem){.digi-dialog.sc-digi-dialog{width:100vw;height:calc(100vh - var(--digi--container-gutter--medium));margin:0;top:var(--digi--container-gutter--medium);max-width:none;max-height:none}}.digi-dialog__inner.sc-digi-dialog{display:grid;grid-template-rows:min-content 1fr min-content;grid-template-areas:\"header close\" \"content content\" \"actions actions\";gap:var(--digi--responsive-grid-gutter);max-height:calc(100vh - var(--digi--container-gutter--medium) * 2)}@media (max-width: 47.9375rem){.digi-dialog__inner.sc-digi-dialog{height:calc(100vh - var(--digi--container-gutter--medium));max-height:none}}.digi-dialog__header.sc-digi-dialog{grid-area:header;padding:var(--digi--padding--largest) 0 0 var(--digi--padding--largest)}@media (min-width: 48rem){.digi-dialog__header.sc-digi-dialog{padding:var(--digi--padding--largest-3) 0 0 var(--digi--padding--largest-3)}}.digi-dialog__close-button-wrapper.sc-digi-dialog{grid-area:close;justify-self:end;padding:var(--digi--padding--largest) var(--digi--padding--largest) 0 0}@media (min-width: 48rem){.digi-dialog__close-button-wrapper.sc-digi-dialog{padding:var(--digi--padding--largest-3) var(--digi--padding--largest-3) 0 0}}.digi-dialog__close-button.sc-digi-dialog{font-family:var(--digi--global--typography--font-family--default);text-decoration:none;color:var(--digi--color--text--secondary);font-size:var(--digi--global--typography--font-size--large);letter-spacing:calc(var(--digi--global--typography--font-size--large) / 100 * -1);font-weight:var(--digi--global--typography--font-weight--semibold);display:flex;flex-direction:row;font-weight:var(--digi--global--typography--font-weight--semibold);font-size:var(--digi--typography--button--font-size--desktop);gap:var(--digi--gutter--icon);align-items:flex-start}.digi-dialog__close-button.sc-digi-dialog:hover{color:var(--digi--global--color--profile--purple--dark);text-decoration:underline}.digi-dialog__close-button.sc-digi-dialog:focus{outline:none;color:var(--digi--color--text--secondary)}.digi-dialog__close-button.sc-digi-dialog:focus-visible{outline:var(--digi--border-width--secondary) solid var(--digi--color--border--focus)}.digi-dialog__close-button.sc-digi-dialog:visited{color:var(--digi--color--text--secondary)}.digi-dialog__content.sc-digi-dialog{grid-area:content;padding-inline:var(--digi--padding--largest);overflow:auto}@media (min-width: 48rem){.digi-dialog__content.sc-digi-dialog{padding-inline:var(--digi--padding--largest-3)}}.digi-dialog__actions.sc-digi-dialog{grid-area:actions;padding:var(--digi--padding--largest);background:var(--digi--color--background--tertiary);display:flex;flex-wrap:wrap;justify-content:flex-end;gap:var(--digi--gutter--small)}@media (min-width: 48rem){.digi-dialog__actions.sc-digi-dialog{padding-inline:var(--digi--padding--largest-3)}}.digi-dialog__actions.sc-digi-dialog:empty{display:none}";

const Dialog = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.afOnOpen = createEvent(this, "afOnOpen", 7);
    this.afOnClose = createEvent(this, "afOnClose", 7);
    this.afOpen = false;
    this.afHideCloseButton = false;
  }
  afOpenChanged(newVal, oldVal) {
    if (newVal === oldVal)
      return;
    this.afOpen ? this.showModal() : this.close();
    this.toggleEmitter(this.afOpen ? 'open' : 'close');
  }
  async showModal() {
    this._dialog.showModal();
    this.afOpen = true;
    this.toggleEmitter('open');
  }
  async close() {
    this._dialog.close();
    this.afOpen = false;
    this.toggleEmitter('close');
  }
  toggleEmitter(state) {
    state === 'open' ? this.afOnOpen.emit() : this.afOnClose.emit();
  }
  clickOutsideHandler(e) {
    if (e.detail.target !== this._dialog)
      return;
    this.close();
  }
  componentDidLoad() {
    this.afOpen && this.showModal();
  }
  render() {
    return (h("dialog", { class: "digi-dialog", ref: (el) => {
        this._dialog = el;
      }, onClose: () => this.close() }, h("digi-util-detect-click-outside", { onAfOnClickOutside: (e) => this.clickOutsideHandler(e) }, h("digi-card-box", { afGutter: CardBoxGutter.NONE }, h("digi-typography", null, h("div", { class: "digi-dialog__inner" }, h("header", { class: "digi-dialog__header" }, h("slot", { name: "heading" })), h("div", { class: "digi-dialog__content" }, h("slot", null)), h("div", { class: "digi-dialog__actions" }, h("slot", { name: "actions" })), !this.afHideCloseButton && (h("div", { class: "digi-dialog__close-button-wrapper" }, h("button", { class: "digi-dialog__close-button", onClick: () => this.close(), type: "button" }, "St\u00E4ng ", h("digi-icon", { "aria-hidden": "true", afName: `x` }))))))))));
  }
  static get watchers() { return {
    "afOpen": ["afOpenChanged"]
  }; }
};
Dialog.style = dialogCss;

const notificationAlertCss = ".sc-digi-notification-alert-h{--digi--notification-alert--border-color--info:var(--digi--color--border--informative);--digi--notification-alert--border-color--warning:var(--digi--color--border--warning);--digi--notification-alert--border-color--danger:var(--digi--color--border--danger);--digi--notification-alert--background-color--info:var(--digi--color--background--notification-info);--digi--notification-alert--background-color--warning:var(--digi--color--background--notification-warning);--digi--notification-alert--background-color--danger:var(--digi--color--background--notification-danger)}.sc-digi-notification-alert-h .sc-digi-notification-alert-s>[slot^=heading]{--digi--heading-4--font-size:var(--digi--typography--heading-4--font-size--mobile);--digi--heading-4--line-height:var(--digi--typography--heading-4--line-height--mobile);font-family:var(--digi--global--typography--font-family--default);font-size:var(--digi--heading-4--font-size);line-height:var(--digi--heading-4--line-height);font-weight:var(--digi--typography--heading-4--font-weight--mobile);-webkit-margin-after:0;margin-block-end:0}@media (min-width: 48rem){.sc-digi-notification-alert-h .sc-digi-notification-alert-s>[slot^=heading]{--digi--heading-4--font-size:var(--digi--typography--heading-4--font-size--desktop);--digi--heading-4--line-height:var(--digi--typography--heading-4--line-height--desktop)}}@media (min-width: 62rem){.sc-digi-notification-alert-h .sc-digi-notification-alert-s>[slot^=heading]{--digi--heading-4--font-size:var(--digi--typography--heading-4--font-size--desktop-large);--digi--heading-4--line-height:var(--digi--typography--heading-4--line-height--desktop-large)}}.sc-digi-notification-alert-h .sc-digi-notification-alert-s>a[slot^=link]{font-family:var(--digi--global--typography--font-family--default);text-decoration:none;color:var(--digi--color--text--secondary);font-size:var(--digi--global--typography--font-size--large);letter-spacing:calc(var(--digi--global--typography--font-size--large) / 100 * -1);font-weight:var(--digi--global--typography--font-weight--semibold);display:block;border-radius:var(--digi--border-radius--primary);padding:calc(var(--digi--grid-gutter--smaller) * 1.5) var(--digi--grid-gutter--smaller);-webkit-padding-start:0;padding-inline-start:0;-webkit-padding-after:0;padding-block-end:0}.sc-digi-notification-alert-h .sc-digi-notification-alert-s>a[slot^=link]:hover{color:var(--digi--global--color--profile--purple--dark);text-decoration:underline}.sc-digi-notification-alert-h .sc-digi-notification-alert-s>a[slot^=link]:focus{outline:none;color:var(--digi--color--text--secondary)}.sc-digi-notification-alert-h .sc-digi-notification-alert-s>a[slot^=link]:focus-visible{outline:var(--digi--border-width--secondary) solid var(--digi--color--border--focus)}.sc-digi-notification-alert-h .sc-digi-notification-alert-s>a[slot^=link]:visited{color:var(--digi--color--text--secondary)}.sc-digi-notification-alert-h .sc-digi-notification-alert-s>a[slot^=link]::after{background-color:currentColor;padding:0 0.5em;-webkit-mask-image:url(\"data:image/svg+xml;charset=utf-8, %3Csvg%0A%09width%3D%2224%22%0A%09height%3D%2224%22%0A%09viewBox%3D%220%200%2024%2024%22%0A%09xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%0A%3E%0A%09%3Cpath%0A%09%09d%3D%22M288.917%2C415.517%2C287.5%2C414.1l3.96-3.96-3.96-3.96%2C1.414-1.414%2C5.373%2C5.374Z%22%0A%09%09transform%3D%22translate(-278.17%20-398.103)%22%0A%09%2F%3E%0A%3C%2Fsvg%3E\");mask-image:url(\"data:image/svg+xml;charset=utf-8, %3Csvg%0A%09width%3D%2224%22%0A%09height%3D%2224%22%0A%09viewBox%3D%220%200%2024%2024%22%0A%09xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%0A%3E%0A%09%3Cpath%0A%09%09d%3D%22M288.917%2C415.517%2C287.5%2C414.1l3.96-3.96-3.96-3.96%2C1.414-1.414%2C5.373%2C5.374Z%22%0A%09%09transform%3D%22translate(-278.17%20-398.103)%22%0A%09%2F%3E%0A%3C%2Fsvg%3E\");-webkit-mask-repeat:no-repeat;mask-repeat:no-repeat;-webkit-mask-position:center;mask-position:center;-webkit-clip-path:padding-box inset(0.25em 0);clip-path:padding-box inset(0.25em 0);content:\"\"}.sc-digi-notification-alert-h .sc-digi-notification-alert-s>[slot^=link]{display:block}.sc-digi-notification-alert-h .sc-digi-notification-alert-s>[slot^=link] a{font-family:var(--digi--global--typography--font-family--default);text-decoration:none;color:var(--digi--color--text--secondary);font-size:var(--digi--global--typography--font-size--large);letter-spacing:calc(var(--digi--global--typography--font-size--large) / 100 * -1);font-weight:var(--digi--global--typography--font-weight--semibold);display:block;border-radius:var(--digi--border-radius--primary);padding:calc(var(--digi--grid-gutter--smaller) * 1.5) var(--digi--grid-gutter--smaller);-webkit-padding-start:0;padding-inline-start:0;-webkit-padding-after:0;padding-block-end:0}.sc-digi-notification-alert-h .sc-digi-notification-alert-s>[slot^=link] a:hover{color:var(--digi--global--color--profile--purple--dark);text-decoration:underline}.sc-digi-notification-alert-h .sc-digi-notification-alert-s>[slot^=link] a:focus{outline:none;color:var(--digi--color--text--secondary)}.sc-digi-notification-alert-h .sc-digi-notification-alert-s>[slot^=link] a:focus-visible{outline:var(--digi--border-width--secondary) solid var(--digi--color--border--focus)}.sc-digi-notification-alert-h .sc-digi-notification-alert-s>[slot^=link] a:visited{color:var(--digi--color--text--secondary)}.sc-digi-notification-alert-h .sc-digi-notification-alert-s>[slot^=link] a::after{background-color:currentColor;padding:0 0.5em;-webkit-mask-image:url(\"data:image/svg+xml;charset=utf-8, %3Csvg%0A%09width%3D%2224%22%0A%09height%3D%2224%22%0A%09viewBox%3D%220%200%2024%2024%22%0A%09xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%0A%3E%0A%09%3Cpath%0A%09%09d%3D%22M288.917%2C415.517%2C287.5%2C414.1l3.96-3.96-3.96-3.96%2C1.414-1.414%2C5.373%2C5.374Z%22%0A%09%09transform%3D%22translate(-278.17%20-398.103)%22%0A%09%2F%3E%0A%3C%2Fsvg%3E\");mask-image:url(\"data:image/svg+xml;charset=utf-8, %3Csvg%0A%09width%3D%2224%22%0A%09height%3D%2224%22%0A%09viewBox%3D%220%200%2024%2024%22%0A%09xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%0A%3E%0A%09%3Cpath%0A%09%09d%3D%22M288.917%2C415.517%2C287.5%2C414.1l3.96-3.96-3.96-3.96%2C1.414-1.414%2C5.373%2C5.374Z%22%0A%09%09transform%3D%22translate(-278.17%20-398.103)%22%0A%09%2F%3E%0A%3C%2Fsvg%3E\");-webkit-mask-repeat:no-repeat;mask-repeat:no-repeat;-webkit-mask-position:center;mask-position:center;-webkit-clip-path:padding-box inset(0.25em 0);clip-path:padding-box inset(0.25em 0);content:\"\"}.digi-notification-alert.sc-digi-notification-alert{border:var(--digi--border-width--tertiary) var(--digi--border-style--primary) var(--digi--global--color--profile--purple--opacity15);border-radius:var(--digi--border-radius--primary);-webkit-border-before:var(--digi--border-width--secondary) solid var(--digi--color--border--secondary);border-block-start:var(--digi--border-width--secondary) solid var(--digi--color--border--secondary);border-block-start-color:var(--BORDER-COLOR);background:var(--BACKGROUND-COLOR)}.digi-notification-alert--variation-info.sc-digi-notification-alert{--BORDER-COLOR:var(--digi--notification-alert--border-color--info);--ICON-COLOR:var(--digi--notification-alert--border-color--info);--BACKGROUND-COLOR:var(--digi--notification-alert--background-color--info)}.digi-notification-alert--variation-danger.sc-digi-notification-alert{--BORDER-COLOR:var(--digi--notification-alert--border-color--danger);--ICON-COLOR:var(--digi--notification-alert--border-color--danger);--BACKGROUND-COLOR:var(--digi--notification-alert--background-color--danger)}.digi-notification-alert--variation-warning.sc-digi-notification-alert{--BORDER-COLOR:var(--digi--notification-alert--border-color--warning);--ICON-COLOR:var(--digi--notification-alert--border-color--warning);--BACKGROUND-COLOR:var(--digi--notification-alert--background-color--warning)}.digi-notification-alert__inner.sc-digi-notification-alert{display:grid;grid-template-columns:min-content min-content;grid-template-rows:min-content auto min-content;grid-template-areas:\"icon close\" \"content content\" \"actions actions\";gap:calc(var(--digi--gutter--icon) * 2);max-width:var(--digi--container-width--largest);margin:0 auto;padding:var(--digi--gutter--medium) var(--digi--gutter--larger)}@media (max-width: 47.9375rem){.digi-notification-alert__inner.sc-digi-notification-alert{justify-content:space-between;align-items:center}}@media (min-width: 48rem){.digi-notification-alert__inner.sc-digi-notification-alert{grid-template-columns:min-content 1fr auto;grid-template-rows:min-content auto;grid-template-areas:\"icon content actions\" \"icon content actions\"}.digi-notification-alert--closeable.sc-digi-notification-alert .digi-notification-alert__inner.sc-digi-notification-alert{grid-template-areas:\"icon content close\" \"icon content actions\"}}.digi-notification-alert__icon.sc-digi-notification-alert{grid-area:icon}.digi-notification-alert__icon.sc-digi-notification-alert digi-icon.sc-digi-notification-alert{--digi--icon--color:var(--ICON-COLOR);--digi-icon--fixed-width:32px;--digi-icon--fixed-height:32px}.digi-notification-alert__content.sc-digi-notification-alert{grid-area:content}.digi-notification-alert__close-button.sc-digi-notification-alert{grid-area:close;justify-self:end}.digi-notification-alert__actions.sc-digi-notification-alert{grid-area:actions}.digi-notification-alert__text.sc-digi-notification-alert{--digi--typography--body--font-size--desktop:var(--digi--global--typography--font-size--smaller);--digi--typography--body--font-size--mobile:var(--digi--global--typography--font-size--smaller);--digi--typography--body--line-height--desktop:var(--digi--global--typography--line-height--smaller);--digi--typography--body--line-height--mobile:var(--digi--global--typography--line-height--smaller)}";

const NotificationAlert = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.afOnClose = createEvent(this, "afOnClose", 7);
    this.afVariation = NotificationAlertVariation.INFO;
    this.afCloseable = false;
  }
  clickHandler(e) {
    this.afOnClose.emit(e);
  }
  get cssModifiers() {
    return {
      [`digi-notification-alert--variation-${this.afVariation}`]: !!this.afVariation,
      'digi-notification-alert--closeable': this.afCloseable
    };
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-notification-alert': true }, this.cssModifiers) }, h("div", { class: "digi-notification-alert__inner" }, h("div", { class: "digi-notification-alert__icon" }, h("digi-icon", { afName: `notification-${this.afVariation}`, "aria-hidden": "true" })), this.afCloseable && (h("digi-button", { "af-variation": ButtonVariation.FUNCTION, onAfOnClick: (e) => this.clickHandler(e), class: "digi-notification-alert__close-button" }, h("span", { class: "digi-notification-alert__close-button__text" }, "St\u00E4ng"), h("digi-icon", { afName: `close`, slot: "icon-secondary", "aria-hidden": "true" }))), h("div", { class: "digi-notification-alert__content" }, h("slot", { name: "heading" }), h("digi-typography", { class: "digi-notification-alert__text", "af-variation": TypographyVariation.SMALL }, h("slot", null)), h("slot", { name: "link" })), h("div", { class: "digi-notification-alert__actions" }, h("slot", { name: "actions" })))));
  }
};
NotificationAlert.style = notificationAlertCss;

export { Dialog as digi_dialog, NotificationAlert as digi_notification_alert };
