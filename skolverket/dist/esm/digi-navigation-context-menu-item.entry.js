import { r as registerInstance, g as getElement } from './index-7f342a75.js';
import { l as logger } from './logger.util-b54855d5.js';
import { N as NavigationContextMenuItemType } from './navigation-context-menu-item-type.enum-178d1050.js';

const NavigationContextMenuItem = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.afText = undefined;
    this.afHref = undefined;
    this.afType = undefined;
    this.afLang = undefined;
    this.afDir = undefined;
  }
  componentWillLoad() {
    if (this.afType !== NavigationContextMenuItemType.BUTTON &&
      this.afType !== NavigationContextMenuItemType.LINK) {
      logger.warn(`The navigation-context-menu-item is of unallowed afType. Only button and link is allowed.`, this.hostElement);
      return;
    }
  }
  get hostElement() { return getElement(this); }
};

export { NavigationContextMenuItem as digi_navigation_context_menu_item };
