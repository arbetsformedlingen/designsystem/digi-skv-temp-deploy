var CodeBlockLanguage;
(function (CodeBlockLanguage) {
  CodeBlockLanguage["JSON"] = "json";
  CodeBlockLanguage["CSS"] = "css";
  CodeBlockLanguage["SCSS"] = "scss";
  CodeBlockLanguage["TYPESCRIPT"] = "typescript";
  CodeBlockLanguage["JAVASCRIPT"] = "javascript";
  CodeBlockLanguage["BASH"] = "bash";
  CodeBlockLanguage["HTML"] = "html";
  CodeBlockLanguage["GIT"] = "git";
  CodeBlockLanguage["JSX"] = "jsx";
  CodeBlockLanguage["TSX"] = "tsx";
})(CodeBlockLanguage || (CodeBlockLanguage = {}));

var FormRadiobuttonLayout;
(function (FormRadiobuttonLayout) {
  FormRadiobuttonLayout["INLINE"] = "inline";
  FormRadiobuttonLayout["BLOCK"] = "block";
})(FormRadiobuttonLayout || (FormRadiobuttonLayout = {}));

var FormRadiobuttonValidation;
(function (FormRadiobuttonValidation) {
  FormRadiobuttonValidation["ERROR"] = "error";
})(FormRadiobuttonValidation || (FormRadiobuttonValidation = {}));

var FormRadiobuttonVariation;
(function (FormRadiobuttonVariation) {
  FormRadiobuttonVariation["PRIMARY"] = "primary";
  FormRadiobuttonVariation["SECONDARY"] = "secondary";
})(FormRadiobuttonVariation || (FormRadiobuttonVariation = {}));

export { CodeBlockLanguage as C, FormRadiobuttonLayout as F, FormRadiobuttonValidation as a, FormRadiobuttonVariation as b };
