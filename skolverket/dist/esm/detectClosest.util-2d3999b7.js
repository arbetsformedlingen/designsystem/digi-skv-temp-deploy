function detectClosest(target, selector) {
  return !!target.closest(selector);
}

export { detectClosest as d };
