import { r as registerInstance, c as createEvent, h, g as getElement } from './index-7f342a75.js';
import { r as randomIdGenerator } from './randomIdGenerator.util-a9066813.js';
import { b as FormFileUploadVariation, a as FormFileUploadValidation, F as FormFileUploadHeadingLevel } from './form-file-upload-variation.enum-882cb26e.js';
import { B as ButtonType } from './button-type.enum-95f504f6.js';
import { B as ButtonVariation } from './button-variation.enum-3cc46e30.js';

const formFileUploadCss = ".digi--util--fs--xs.sc-digi-form-file-upload{font-size:var(--digi--global--typography--font-size--small) !important}.digi--util--fs--s.sc-digi-form-file-upload{font-size:calc(var(--digi--global--typography--font-size--small) + 1px) !important}.digi--util--fs--m.sc-digi-form-file-upload{font-size:var(--digi--global--typography--font-size--base) !important}.digi--util--fs--l.sc-digi-form-file-upload{font-size:var(--digi--global--typography--font-size--large) !important}.digi--util--fw--sb.sc-digi-form-file-upload{font-weight:var(--digi--global--typography--font-weight--semibold) !important}.digi--util--pt--1.sc-digi-form-file-upload{padding-top:var(--digi--global--spacing--smallest-2) !important}.digi--util--pt--10.sc-digi-form-file-upload{padding-top:var(--digi--global--spacing--smallest) !important}.digi--util--pt--20.sc-digi-form-file-upload{padding-top:var(--digi--global--spacing--base) !important}.digi--util--pt--30.sc-digi-form-file-upload{padding-top:var(--digi--global--spacing--largest-2) !important}.digi--util--pt--40.sc-digi-form-file-upload{padding-top:var(--digi--global--spacing--largest-4) !important}.digi--util--pt--50.sc-digi-form-file-upload{padding-top:2.5rem !important}.digi--util--pt--60.sc-digi-form-file-upload{padding-top:var(--digi--global--spacing--largest-5) !important}.digi--util--pt--70.sc-digi-form-file-upload{padding-top:var(--digi--global--spacing--largest-6) !important}.digi--util--pt--80.sc-digi-form-file-upload{padding-top:4.5rem !important}.digi--util--pt--90.sc-digi-form-file-upload{padding-top:5rem !important}.digi--util--pb--1.sc-digi-form-file-upload{padding-bottom:var(--digi--global--spacing--smallest-2) !important}.digi--util--pb--10.sc-digi-form-file-upload{padding-bottom:var(--digi--global--spacing--smallest) !important}.digi--util--pb--20.sc-digi-form-file-upload{padding-bottom:var(--digi--global--spacing--base) !important}.digi--util--pb--30.sc-digi-form-file-upload{padding-bottom:var(--digi--global--spacing--largest-2) !important}.digi--util--pb--40.sc-digi-form-file-upload{padding-bottom:var(--digi--global--spacing--largest-4) !important}.digi--util--pb--50.sc-digi-form-file-upload{padding-bottom:2.5rem !important}.digi--util--pb--60.sc-digi-form-file-upload{padding-bottom:var(--digi--global--spacing--largest-5) !important}.digi--util--pb--70.sc-digi-form-file-upload{padding-bottom:var(--digi--global--spacing--largest-6) !important}.digi--util--pb--80.sc-digi-form-file-upload{padding-bottom:4.5rem !important}.digi--util--pb--90.sc-digi-form-file-upload{padding-bottom:5rem !important}.digi--util--p--1.sc-digi-form-file-upload{padding:var(--digi--global--spacing--smallest-2) !important}.digi--util--p--10.sc-digi-form-file-upload{padding:var(--digi--global--spacing--smallest) !important}.digi--util--p--20.sc-digi-form-file-upload{padding:var(--digi--global--spacing--base) !important}.digi--util--p--30.sc-digi-form-file-upload{padding:var(--digi--global--spacing--largest-2) !important}.digi--util--p--40.sc-digi-form-file-upload{padding:var(--digi--global--spacing--largest-4) !important}.digi--util--p--50.sc-digi-form-file-upload{padding:2.5rem !important}.digi--util--p--60.sc-digi-form-file-upload{padding:var(--digi--global--spacing--largest-5) !important}.digi--util--p--70.sc-digi-form-file-upload{padding:var(--digi--global--spacing--largest-6) !important}.digi--util--p--80.sc-digi-form-file-upload{padding:4.5rem !important}.digi--util--p--90.sc-digi-form-file-upload{padding:5rem !important}.digi--util--ptb--1.sc-digi-form-file-upload{padding-top:var(--digi--global--spacing--smallest-2) !important;padding-bottom:var(--digi--global--spacing--smallest-2) !important}.digi--util--ptb--10.sc-digi-form-file-upload{padding-top:var(--digi--global--spacing--smallest) !important;padding-bottom:var(--digi--global--spacing--smallest) !important}.digi--util--ptb--20.sc-digi-form-file-upload{padding-top:var(--digi--global--spacing--base) !important;padding-bottom:var(--digi--global--spacing--base) !important}.digi--util--ptb--30.sc-digi-form-file-upload{padding-top:var(--digi--global--spacing--largest-2) !important;padding-bottom:var(--digi--global--spacing--largest-2) !important}.digi--util--ptb--40.sc-digi-form-file-upload{padding-top:var(--digi--global--spacing--largest-4) !important;padding-bottom:var(--digi--global--spacing--largest-4) !important}.digi--util--ptb--50.sc-digi-form-file-upload{padding-top:2.5rem !important;padding-bottom:2.5rem !important}.digi--util--ptb--60.sc-digi-form-file-upload{padding-top:var(--digi--global--spacing--largest-5) !important;padding-bottom:var(--digi--global--spacing--largest-5) !important}.digi--util--ptb--70.sc-digi-form-file-upload{padding-top:var(--digi--global--spacing--largest-6) !important;padding-bottom:var(--digi--global--spacing--largest-6) !important}.digi--util--ptb--80.sc-digi-form-file-upload{padding-top:4.5rem !important;padding-bottom:4.5rem !important}.digi--util--ptb--90.sc-digi-form-file-upload{padding-top:5rem !important;padding-bottom:5rem !important}.digi--util--plr--1.sc-digi-form-file-upload{padding-left:var(--digi--global--spacing--smallest-2) !important;padding-right:var(--digi--global--spacing--smallest-2) !important}.digi--util--plr--10.sc-digi-form-file-upload{padding-left:var(--digi--global--spacing--smallest) !important;padding-right:var(--digi--global--spacing--smallest) !important}.digi--util--plr--20.sc-digi-form-file-upload{padding-left:var(--digi--global--spacing--base) !important;padding-right:var(--digi--global--spacing--base) !important}.digi--util--plr--30.sc-digi-form-file-upload{padding-left:var(--digi--global--spacing--largest-2) !important;padding-right:var(--digi--global--spacing--largest-2) !important}.digi--util--plr--40.sc-digi-form-file-upload{padding-left:var(--digi--global--spacing--largest-4) !important;padding-right:var(--digi--global--spacing--largest-4) !important}.digi--util--plr--50.sc-digi-form-file-upload{padding-left:2.5rem !important;padding-right:2.5rem !important}.digi--util--plr--60.sc-digi-form-file-upload{padding-left:var(--digi--global--spacing--largest-5) !important;padding-right:var(--digi--global--spacing--largest-5) !important}.digi--util--plr--70.sc-digi-form-file-upload{padding-left:var(--digi--global--spacing--largest-6) !important;padding-right:var(--digi--global--spacing--largest-6) !important}.digi--util--plr--80.sc-digi-form-file-upload{padding-left:4.5rem !important;padding-right:4.5rem !important}.digi--util--plr--90.sc-digi-form-file-upload{padding-left:5rem !important;padding-right:5rem !important}.digi--util--sr-only.sc-digi-form-file-upload{border:0;clip:rect(1px, 1px, 1px, 1px);-webkit-clip-path:inset(50%);clip-path:inset(50%);height:1px;margin:-1px;overflow:hidden;padding:0;position:absolute;width:1px;white-space:nowrap}.sc-digi-form-file-upload-h{--digi--form-file-upload--color--background--primary--default:var(--digi--color--background--secondary);--digi--form-file-upload--color--background--primary--hover:var(--digi--global--color--cta--hover--base);--digi--form-file-upload--color--background--secondary--default:var(--digi--color--background--secondary);--digi--form-file-upload--color--background--secondary--hover:var(--digi--global--color--cta--hover--base);--digi--form-file-upload--color--background--tertiary--default:var(--digi--color--background--secondary);--digi--form-file-upload--color--background--tertiary--hover:var(--digi--global--color--cta--hover--base);--digi--form-file-upload--color--border--primary--default:var(--digi--global--color--neutral--grayscale--darkest);--digi--form-file-upload--color--border--primary--hover:var(--digi--global--color--cta--hover--base);--digi--form-file-upload--color--border--secondary--default:var(--digi--global--color--neutral--grayscale--darkest);--digi--form-file-upload--color--border--secondary--hover:var(--digi--global--color--cta--hover--base);--digi--form-file-upload--color--border--tertiary--default:var(--digi--global--color--neutral--grayscale--darkest);--digi--form-file-upload--color--border--tertiary--hover:var(--digi--global--color--cta--hover--base);--digi--form-file-upload--border-width--primary--default:var(--digi--border-width--secondary);--digi--form-file-upload--border-width--primary--hover:var(--digi--border-width--primary);--digi--form-file-upload--border-width--secondary--default:var(--digi--border-width--secondary);--digi--form-file-upload--border-width--secondary--hover:var(--digi--border-width--primary);--digi--form-file-upload--border-width--tertiary--default:var(--digi--border-width--secondary);--digi--form-file-upload--border-width--tertiary--hover:var(--digi--border-width--primary);--digi--form-file-upload--border-style--primary--default:solid;--digi--form-file-upload--border-style--secondary--default:dashed;--digi--form-file-upload--border-style--tertiary--default:dashed;--digi--form-file-upload--border-radius--primary--default:var(--digi--border-radius--secondary);--digi--form-file-upload--border-radius--secondary--default:var(--digi--border-radius--secondary);--digi--form-file-upload--border-radius--tertiary--default:var(--digi--border-radius--secondary);--digi--form-file-upload--padding--primary:var(--digi--gutter--medium) var(--digi--gutter--largest);--digi--form-file-upload--padding--secondary:var(--digi--gutter--largest) var(--digi--gutter--largest);--digi--form-file-upload--padding--tertiary:var(--digi--gutter--largest) var(--digi--gutter--largest-3);--digi--form-file-upload--font-size--small:var(--digi--global--typography--font-size--small);--digi--form-file-upload--font-size--medium:var(--digi--global--typography--font-size--base);--digi--form-file-upload--font-size--large:var(--digi--global--typography--font-size--large);--digi--form-file-upload--heading--font-family:var(--digi--global--typography--font-family--default);--digi--form-file-upload--heading--font-weight:var(--digi--typography--heading-4--font-weight--desktop);--digi--form-file-upload--heading--font-size:var(--digi--typography--heading-4--font-size--desktop);--digi--form-file-upload--heading--font-size--large:var(--digi--typography--heading-4--font-size--desktop-large);--digi--form-file-upload--font-family:var(--digi--global--typography--font-family--default);--digi--form-file-upload--font-weight:var(--digi--typography--form-file-upload--font-weight--desktop);--digi--form-file-upload--button-text--color--default:var(--digi--global--color--cta--link--base);--digi--form-file-upload--button-text--color--hover:var(--digi--global--color--function--info--light);--digi--form-file-upload--width--primary:max-content;--digi--form-file-upload--width--secondary:300px;--digi--form-file-upload--width--tertiary:100%}.sc-digi-form-file-upload-h .digi-form-file-upload--primary.sc-digi-form-file-upload{--COLOR--BACKGROUND--DEFAULT:var(--digi--form-file-upload--color--background--primary--default);--COLOR--BACKGROUND--HOVER:var(--digi--form-file-upload--color--background--primary--hover);--COLOR--BORDER--DEFAULT:var(--digi--form-file-upload--color--border--primary--default);--COLOR--BORDER--HOVER:var(--digi--form-file-upload--color--border--primary--default);--BORDER-STYLE--DEFAULT:var(--digi--form-file-upload--border-style--primary--default);--BORDER-RADIUS--DEFAULT:var(--digi--form-file-upload--border-radius--primary--default);--BORDER-WIDTH--DEFAULT:var(--digi--form-file-upload--border-width--primary--default);--WIDTH:var(--digi--form-file-upload--width--primary);--FONT-WEIGHT:var(--digi--form-file-upload--font-weight);--PADDING:var(--digi--form-file-upload--padding--primary)}.sc-digi-form-file-upload-h .digi-form-file-upload--secondary.sc-digi-form-file-upload{--COLOR--BACKGROUND--DEFAULT:var(--digi--form-file-upload--color--background--secondary--default);--COLOR--BACKGROUND--HOVER:var(--digi--form-file-upload--color--background--secondary--hover);--COLOR--BORDER--DEFAULT:var(--digi--form-file-upload--color--border--secondary--default);--COLOR--BORDER--HOVER:var(--digi--form-file-upload--color--border--secondary--hover);--BORDER-STYLE--DEFAULT:var(--digi--form-file-upload--border-style--secondary--default);--BORDER-RADIUS--DEFAULT:var(--digi--form-file-upload--border-radius--secondary--default);--BORDER-WIDTH--DEFAULT:var(--digi--form-file-upload--border-width--secondary--default);--WIDTH:var(--digi--form-file-upload--width--secondary);--FONT-WEIGHT:var(--digi--form-file-upload--font-weight);--PADDING:var(--digi--form-file-upload--padding--secondary)}.sc-digi-form-file-upload-h .digi-form-file-upload--tertiary.sc-digi-form-file-upload{--COLOR--BACKGROUND--DEFAULT:var(--digi--form-file-upload--color--background--tertiary--default);--COLOR--BACKGROUND--HOVER:var(--digi--form-file-upload--color--background--tertiary--hover);--COLOR--BORDER--DEFAULT:var(--digi--form-file-upload--color--border--tertiary--default);--COLOR--BORDER--HOVER:var(--digi--form-file-upload--color--border--tertiary--hover);--BORDER-STYLE--DEFAULT:var(--digi--form-file-upload--border-style--tertiary--default);--BORDER-RADIUS--DEFAULT:var(--digi--form-file-upload--border-radius--tertiary--default);--BORDER-WIDTH--DEFAULT:var(--digi--form-file-upload--border-width--tertiary--default);--WIDTH:var(--digi--form-file-upload--width--tertiary);--FONT-WEIGHT:var(--digi--form-file-upload--font-weight);--PADDING:var(--digi--form-file-upload--padding--tertiary)}.sc-digi-form-file-upload-h .digi-form-file-upload__icon.sc-digi-form-file-upload{margin-bottom:var(--digi--margin--medium);opacity:0.7;--digi--icon--height:2.5rem}.sc-digi-form-file-upload-h .digi-form-file-upload__spinner.sc-digi-form-file-upload{animation:animation__rotation 3s infinite linear;transform-origin:center;--digi--icon--height:1rem}@media (prefers-reduced-motion){.sc-digi-form-file-upload-h .digi-form-file-upload__spinner.sc-digi-form-file-upload{animation:none}}@keyframes animation__rotation{from{transform:rotate(0deg)}to{transform:rotate(359deg)}}.sc-digi-form-file-upload-h .digi-form-file-upload__input.sc-digi-form-file-upload{display:none}.sc-digi-form-file-upload-h .digi-form-file-upload__button.sc-digi-form-file-upload{margin-top:auto;text-align:center;background:none;border:none;cursor:pointer;margin:0;font-weight:var(--digi--typography--button--font-weight--desktop);font-size:var(--digi--typography--body--font-size--desktop);color:var(--digi--form-file-upload--button-text--color--default);display:flex;align-items:center;font-family:var(--digi--global--typography--font-family--default)}.sc-digi-form-file-upload-h .digi-form-file-upload__button.sc-digi-form-file-upload:hover{color:var(--digi--form-file-upload--button-text--color--hover)}.sc-digi-form-file-upload-h .digi-form-file-upload__button.sc-digi-form-file-upload:hover .digi-form-file-upload__button-icon.sc-digi-form-file-upload{--digi--icon--color:var(--digi--form-file-upload--button-text--color--hover)}.sc-digi-form-file-upload-h .digi-form-file-upload__button.sc-digi-form-file-upload:focus{border-color:var(--digi--global--color--function--info--base)}.sc-digi-form-file-upload-h .digi-form-file-upload__button-icon.sc-digi-form-file-upload{width:20px;height:20px;margin-left:5px}@media (min-width: 500px){.sc-digi-form-file-upload-h .digi-form-file-upload__button--mobile.sc-digi-form-file-upload{display:none}}.sc-digi-form-file-upload-h .digi-form-file-upload__upload-area.sc-digi-form-file-upload{border-color:var(--COLOR--BORDER--DEFAULT);border-radius:var(--BORDER-RADIUS--DEFAULT);border-width:var(--BORDER-WIDTH--DEFAULT);border-style:var(--BORDER-STYLE--DEFAULT);max-width:var(--WIDTH);display:flex;flex-direction:column;background-color:var(--COLOR--BACKGROUND--DEFAULT);align-items:center;padding:var(--PADDING);margin-bottom:var(--digi--gutter--medium);position:relative;cursor:pointer}.sc-digi-form-file-upload-h .digi-form-file-upload__upload-area--hover.sc-digi-form-file-upload{border-color:var(--COLOR--BORDER--HOVER)}.sc-digi-form-file-upload-h .digi-form-file-upload__upload-area-overlay.sc-digi-form-file-upload{position:absolute;width:100%;height:100%;background-color:var(--COLOR--BACKGROUND--HOVER);opacity:0.1;top:0;left:0;z-index:10}.sc-digi-form-file-upload-h .digi-form-file-upload__text-area.sc-digi-form-file-upload{display:flex;flex-direction:column;align-items:center;justify-content:center;text-align:center}.sc-digi-form-file-upload-h .digi-form-file-upload__text.sc-digi-form-file-upload{display:flex;flex-direction:column;align-items:center;text-align:center}@media screen and (min-width: 48rem){.sc-digi-form-file-upload-h .digi-form-file-upload__text--tertiary.sc-digi-form-file-upload{flex-direction:row;align-items:center}}.sc-digi-form-file-upload-h .digi-form-file-upload__files.sc-digi-form-file-upload{border-radius:var(--BORDER-RADIUS--DEFAULT);max-width:100%;background-color:var(--COLOR--BACKGROUND--DEFAULT);padding:var(--digi--gutter--largest-2) var(--digi--padding--large);margin-bottom:var(--digi--gutter--medium);margin-top:var(--digi--gutter--medium);padding-bottom:var(--digi--padding--smaller)}.sc-digi-form-file-upload-h .digi-form-file-upload__files.sc-digi-form-file-upload ul.sc-digi-form-file-upload{max-width:none;padding:0;margin:0}.sc-digi-form-file-upload-h .digi-form-file-upload__files.sc-digi-form-file-upload h4.sc-digi-form-file-upload{margin:0;padding:0;font-weight:var(--digi--typography--body--font-weight--desktop)}.sc-digi-form-file-upload-h .digi-form-file-upload__files-heading.sc-digi-form-file-upload{font-family:var(--digi--form-file-upload--heading--font-family);font-size:var(--digi--form-file-upload--heading--font-size);font-weight:var(--digi--form-file-upload--heading--font-weight);color:var(--digi--color--text--primary);margin:0;display:block}@media (min-width: 62rem){.sc-digi-form-file-upload-h .digi-form-file-upload__files-heading.sc-digi-form-file-upload{font-size:var(--digi--form-file-upload--heading--font-size--large)}}.sc-digi-form-file-upload-h .digi-form-file-upload__file.sc-digi-form-file-upload{margin:var(--digi--margin--smaller) 0;padding:var(--digi--gutter--medium) 0}.sc-digi-form-file-upload-h .digi-form-file-upload__file-container.sc-digi-form-file-upload{list-style-type:none}.sc-digi-form-file-upload-h .digi-form-file-upload__file-container.sc-digi-form-file-upload:nth-child(n+2){border-top:var(--BORDER-WIDTH--DEFAULT) solid var(--COLOR--BORDER--DEFAULT)}.sc-digi-form-file-upload-h .digi-form-file-upload__file-header.sc-digi-form-file-upload{display:flex;align-items:center}.sc-digi-form-file-upload-h .digi-form-file-upload__file-footer.sc-digi-form-file-upload{padding-left:30px}.sc-digi-form-file-upload-h .digi-form-file-upload__file-name.sc-digi-form-file-upload{overflow:hidden;margin-right:var(--digi--margin--smaller);text-overflow:ellipsis;max-width:18.75rem;padding:var(--digi--padding--smaller);white-space:nowrap;height:-moz-max-content;height:max-content;margin:0 auto 0 0;padding:0 0.5rem 0 0.5rem}@media (max-width: 500px){.sc-digi-form-file-upload-h .digi-form-file-upload__file-name.sc-digi-form-file-upload{padding:0 0.5rem 0 0;max-width:12.75rem}}.sc-digi-form-file-upload-h .digi-form-file-upload__file-icon.sc-digi-form-file-upload{display:flex;justify-content:center;align-items:center}.sc-digi-form-file-upload-h .digi-form-file-upload__file-info.sc-digi-form-file-upload{width:100%}.sc-digi-form-file-upload-h .digi-form-file-upload__file.sc-digi-form-file-upload span.sc-digi-form-file-upload{height:-moz-max-content;height:max-content;margin-left:var(--digi--margin--smaller)}@media (max-width: 500px){.sc-digi-form-file-upload-h .digi-form-file-upload__file.sc-digi-form-file-upload span.sc-digi-form-file-upload{margin-left:0;margin-right:auto}}.sc-digi-form-file-upload-h .digi-form-file-upload__file-button.sc-digi-form-file-upload{margin-left:auto;font-weight:var(--digi--global--typography--font-weight--semibold)}.sc-digi-form-file-upload-h .digi-form-file-upload__file--error.sc-digi-form-file-upload{display:flex;color:red}@media (max-width: 500px){.sc-digi-form-file-upload-h .digi-form-file-upload__file--error.sc-digi-form-file-upload span.sc-digi-form-file-upload{display:none}}.sc-digi-form-file-upload-h .digi-form-file-upload__error.sc-digi-form-file-upload{color:var(--digi--color--icons--danger)}.sc-digi-form-file-upload-h .visually-hidden.sc-digi-form-file-upload{border:0;clip:rect(1px, 1px, 1px, 1px);-webkit-clip-path:inset(50%);clip-path:inset(50%);height:1px;margin:-1px;overflow:hidden;padding:0;position:absolute;width:1px;white-space:nowrap}@media (max-width: 500px){.sc-digi-form-file-upload-h .hidden-mobile.sc-digi-form-file-upload{display:none}}";

const FormFileUpload = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.afOnUploadFile = createEvent(this, "afOnUploadFile", 7);
    this.afOnRemoveFile = createEvent(this, "afOnRemoveFile", 7);
    this.afOnCancelFile = createEvent(this, "afOnCancelFile", 7);
    this.afOnRetryFile = createEvent(this, "afOnRetryFile", 7);
    this.toBase64 = (file) => new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        let encoded = reader.result.toString().replace(/^data:(.*,)?/, '');
        if (encoded.length % 4 > 0) {
          encoded += '='.repeat(4 - (encoded.length % 4));
        }
        resolve(encoded);
      };
      reader.onerror = (error) => reject(error);
    });
    this._input = undefined;
    this.files = [];
    this.error = false;
    this.errorMessage = undefined;
    this.fileHover = false;
    this.afLabel = 'Ladda upp fil';
    this.afLabelDescription = undefined;
    this.afVariation = FormFileUploadVariation.PRIMARY;
    this.afId = randomIdGenerator('digi-form-file-upload');
    this.afFileTypes = undefined;
    this.afFileMaxSize = 10;
    this.afMaxFiles = undefined;
    this.afName = undefined;
    this.afUploadBtnText = undefined;
    this.afRequired = undefined;
    this.afRequiredText = undefined;
    this.afAnnounceIfOptional = false;
    this.afHeadingFiles = 'Uppladdade filer';
    this.afAnnounceIfOptionalText = undefined;
    this.afValidation = FormFileUploadValidation.ENABLED;
    this.afHeadingLevel = FormFileUploadHeadingLevel.H2;
  }
  /**
   * Validera filens status.
   * @en Validate file status
   */
  async afMValidateFile(scanInfo) {
    this.validateFile(scanInfo);
  }
  /**
   * Få ut alla uppladdade filer
   * @en Get all uploaded files
   */
  async afMGetAllFiles() {
    if (this.afValidation === 'enabled') {
      const validatedFiles = this.files.filter((file) => file['status'] === 'OK');
      return validatedFiles;
    }
    else {
      return this.files;
    }
  }
  /**
   * Importera en array med filer till komponenten utan att trigga uppladdningsevent. Ett fil objekt måste innehålla id, status och filen själv.
   * @en Import an array with files without triggering upload event. A file object needs to contain an id, status and the file itself.
   */
  async afMImportFiles(files) {
    for (const importedFile of files) {
      let duplicate = false;
      for (const item of this.files) {
        if (item.name == importedFile.name) {
          this.error = true;
          this.errorMessage = `Det finns redan en fil med filnamnet (${importedFile.name})`;
          duplicate = true;
        }
      }
      if (!duplicate) {
        this.files = [...this.files, importedFile];
      }
    }
  }
  async afMGetFormControlElement() {
    return this._input;
  }
  get fileMaxSize() {
    // 1MB is 1048576 in Bytes
    return this.afFileMaxSize * 1048576;
  }
  validateFile(scanInfo) {
    const index = this.files.findIndex((file) => file['name'] == scanInfo['name'] || file['id'] == scanInfo['id']);
    if (index === -1) {
      return { message: 'Something went wrong' };
    }
    let file = this.files[index];
    if (scanInfo.status === 'OK') {
      file['status'] = 'OK';
      delete file['error'];
      this.removeFileFromList(file['id']);
      this.files = [...this.files, file];
    }
    else {
      file['status'] = 'error';
      file['error'] =
        scanInfo.error === undefined ? 'Fil bedömt som osäker' : scanInfo.error;
      this.removeFileFromList(file['id']);
      this.files = [...this.files, file];
    }
  }
  emitFile(incomingFile) {
    const index = this.getIndexOfFile(incomingFile['id']);
    if (this.afValidation === 'enabled') {
      this.afOnUploadFile.emit(this.files[index]);
    }
    else {
      this.files[index]['status'] = 'OK';
      this.afOnUploadFile.emit(this.files[index]);
    }
  }
  onRetryFileHandler(e, id) {
    e.preventDefault();
    const fileFromList = this.removeFileFromList(id);
    fileFromList['status'] = 'pending';
    delete fileFromList['error'];
    this.files = [...this.files, fileFromList];
    this.afOnRetryFile.emit(fileFromList);
  }
  onCancelFileHandler(e, id) {
    e.preventDefault();
    const fileFromList = this.removeFileFromList(id);
    fileFromList['status'] = 'error';
    fileFromList['error'] = 'Uppladdning avbruten';
    this.files = [...this.files, fileFromList];
    this.afOnCancelFile.emit(fileFromList);
  }
  removeFileFromList(id) {
    const index = this.getIndexOfFile(id);
    if (index === -1) {
      return;
    }
    const removedFile = this.files[index];
    this.files = [...this.files.slice(0, index), ...this.files.slice(index + 1)];
    return removedFile;
  }
  getIndexOfFile(id) {
    const index = this.files.findIndex((file) => file['id'] == id);
    return index;
  }
  onButtonUploadFileHandler(e) {
    e.preventDefault();
    if (!e.target.files)
      return;
    const file = e.target.files[0];
    this.addUploadedFiles(file);
  }
  onRemoveFileHandler(e, id) {
    e.preventDefault();
    const fileToRemove = this.removeFileFromList(id);
    this.afOnRemoveFile.emit(fileToRemove);
    this._input.value = '';
    if (this.files.length < 1)
      this.error = false;
  }
  async addUploadedFiles(file) {
    this.error = false;
    if (this.files.length >= this.afMaxFiles) {
      this.error = true;
      this.errorMessage = 'Max antal filer uppladdade!';
      return;
    }
    else if (file.size > this.fileMaxSize) {
      this.error = true;
      this.errorMessage = 'Filens storlek är för stor!';
      return;
    }
    else {
      for (const item of this.files) {
        if (item.name == file.name) {
          this.error = true;
          this.errorMessage = 'Det finns redan en fil med det filnamnet!';
          return;
        }
      }
    }
    file['id'] = randomIdGenerator('file');
    file['status'] = 'pending';
    file['base64'] = await this.toBase64(file);
    this.files = [...this.files, file];
    this.emitFile(file);
  }
  handleDrop(e) {
    e.stopPropagation();
    e.preventDefault();
    this.fileHover = false;
    if (e.dataTransfer.files) {
      const file = e.dataTransfer.files[0];
      if (file.type.includes(this.afFileTypes) || this.afFileTypes == '*') {
        this.addUploadedFiles(file);
      }
      else {
        this.error = true;
        this.errorMessage = 'Filtypen tillåts inte att ladda upp';
      }
    }
  }
  handleAllowDrop(e) {
    if (this.afVariation === 'secondary')
      return;
    e.stopPropagation();
    e.preventDefault();
  }
  handleOnDragEnter(e) {
    this.handleAllowDrop(e);
    this.fileHover = !this.fileHover;
  }
  handleOnDragLeave(e) {
    this.handleAllowDrop(e);
    this.fileHover = !this.fileHover;
  }
  handleInputClick(e) {
    e.cancelBubble = true;
    this._input.click();
  }
  componentWillLoad() { }
  componentDidLoad() { }
  componentWillUpdate() { }
  get cssModifiers() {
    return {
      'digi-form-file-upload--primary': this.afVariation == 'primary',
      'digi-form-file-upload--secondary': this.afVariation == 'secondary',
      'digi-form-file-upload--tertiary': this.afVariation == 'tertiary'
    };
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-form-file-upload': true }, this.cssModifiers) }, h("digi-form-label", { afFor: this.afId, afLabel: this.afLabel, afDescription: this.afLabelDescription, afRequired: this.afRequired, afAnnounceIfOptional: this.afAnnounceIfOptional, afRequiredText: this.afRequiredText, afAnnounceIfOptionalText: this.afAnnounceIfOptionalText }), h("div", { class: {
        'digi-form-file-upload__upload-area': true,
        'digi-form-file-upload__upload-area--hover': this.fileHover
      }, onDrop: (e) => this.handleDrop(e), onDragOver: (e) => this.handleAllowDrop(e), onDragEnter: (e) => this.handleOnDragEnter(e), onDragLeave: (e) => this.handleOnDragLeave(e), onClick: (e) => this.handleInputClick(e) }, this.fileHover && this.afVariation != 'secondary' && (h("div", { class: "digi-form-file-upload__upload-area-overlay" })), this.afVariation == 'tertiary' && (h("digi-icon-upload", { style: { '--digi--icon--height': '40px' }, class: "digi-form-file-upload__icon", "aria-hidden": "true" })), h("div", { class: "digi-form-file-upload__text-area" }, h("digi-button", { afVariation: ButtonVariation.FUNCTION, afType: ButtonType.BUTTON, onClick: (e) => this.handleInputClick(e) }, h("digi-icon-paperclip", { "aria-hidden": "true", slot: "icon" }), this.afUploadBtnText ? this.afUploadBtnText : 'Välj fil'), h("input", { ref: (el) => {
        this._input = el;
      }, onChange: (e) => this.onButtonUploadFileHandler(e), class: "digi-form-file-upload__input", multiple: true, type: "file", id: this.afId, accept: this.afFileTypes, name: this.afName ? this.afName : null, required: this.afRequired ? this.afRequired : null, onDrop: (e) => this.handleDrop(e), onDragOver: (e) => this.handleAllowDrop(e), "aria-errormessage": "digi-form-file-upload__error", "aria-describedBy": "digi-form-file-upload__error" }), this.afVariation != FormFileUploadVariation.SECONDARY &&
      'eller dra och släpp en fil inuti det streckade området')), this.error && (h("digi-form-validation-message", { id: "digi-form-file-upload__error", class: "digi-form-file-upload__error", role: "alert", "aria-label": this.errorMessage, "af-variation": "error" }, this.errorMessage)), this.files.length > 0 && (h("div", { class: "digi-form-file-upload__files" }, h(this.afHeadingLevel, { class: "digi-form-file-upload__files-heading" }, this.afHeadingFiles), h("ul", { "aria-live": "assertive" }, this.files.map((file) => {
      return (h("li", { class: "digi-form-file-upload__file-container" }, file['status'] == 'pending' && (h("div", { class: "digi-form-file-upload__file" }, h("div", { class: "digi-form-file-upload__file-header" }, h("digi-icon-spinner", { class: "digi-form-file-upload__spinner hidden-mobile" }), h("span", null, "Laddar upp... ", h("span", { class: 'hidden-mobile' }, "|")), h("p", { class: "digi-form-file-upload__file-name hidden-mobile" }, file.name), h("button", { type: "button", onClick: (e) => this.onCancelFileHandler(e, file['id']), class: "digi-form-file-upload__button hidden-mobile", "aria-label": `Avbryt uppladdningen av ${file.name}` }, "Avbryt uppladdning"), h("button", { type: "button", onClick: (e) => this.onCancelFileHandler(e, file['id']), class: "digi-form-file-upload__button digi-form-file-upload__button--mobile", "aria-label": `Avbryt uppladdningen av ${file.name}` }, h("digi-icon-x-button-outline", { style: {
          '--digi--icon--height': '20px',
          '--digi--icon--color': 'var(--digi--global--color--function--info--base)'
        }, "aria-hidden": "true" }))))), file['status'] == 'OK' && (h("div", { class: "digi-form-file-upload__file" }, h("div", { class: "digi-form-file-upload__file-header" }, h("digi-icon-check-circle-reg", { style: {
          '--digi--icon--height': '20px',
          '--digi--icon--color': 'green'
        }, class: 'hidden-mobile' }), h("p", { class: "digi-form-file-upload__file-name" }, file.name), h("button", { type: "button", onClick: (e) => this.onRemoveFileHandler(e, file['id']), class: "digi-form-file-upload__button hidden-mobile", "aria-label": `Ta bort ${file.name}` }, "Ta bort fil"), h("button", { type: "button", onClick: (e) => this.onRemoveFileHandler(e, file['id']), class: "digi-form-file-upload__button digi-form-file-upload__button--mobile", "aria-label": `Ta bort ${file.name}` }, h("digi-icon-trash", { style: {
          '--digi--icon--height': '20px',
          '--digi--icon--color': 'var(--digi--global--color--function--info--base)'
        } }))))), file['status'] == 'error' && (h("div", { class: "digi-form-file-upload__file" }, h("div", { class: "digi-form-file-upload__file-header" }, h("digi-icon-danger-outline", { style: {
          '--digi--icon--height': '20px',
          '--digi--icon--color': 'red'
        }, "aria-hidden": "true", class: 'hidden-mobile' }), h("span", { class: "digi-form-file-upload__file--error", "aria-label": `${file['name']} gick inte att ladda upp. ${file['error']}` }, "Avbruten ", h("span", null, "|")), h("p", { class: "digi-form-file-upload__file-name\n\t\t\t\t\t\t\t\t\t\t\t\t\tdigi-form-file-upload__file--error hidden-mobile\n\t\t\t\t\t\t\t\t\t\t\t\t\t" }, file.name), this.afValidation === 'disabled' ||
        (file['error'] === 'Uppladdning avbruten' && (h("div", { style: { display: 'flex' } }, h("button", { type: "button", onClick: (e) => this.onRetryFileHandler(e, file['id']), class: "digi-form-file-upload__button digi-form-file-upload__button--file hidden-mobile", "aria-label": `Misslyckad uppladdning, försök ladda upp filen igen ${file.name}` }, "F\u00F6rs\u00F6k igen", h("span", null, "|")), h("button", { type: "button", onClick: (e) => this.onRetryFileHandler(e, file['id']), class: "digi-form-file-upload__button digi-form-file-upload__button--mobile digi-form-file-upload__button--file", "aria-label": `Misslyckad uppladdning, försök ladda upp filen igen ${file.name}`, style: {
            'margin-right': '10px'
          } }, h("digi-icon-update", { style: {
            '--digi--icon--height': '20px',
            '--digi--icon--color': 'var(--digi--global--color--function--info--base)'
          } }))))), h("button", { type: "button", onClick: (e) => this.onRemoveFileHandler(e, file['id']), class: "digi-form-file-upload__button digi-form-file-upload__button--file hidden-mobile", "aria-label": `Misslyckad uppladdning, ta bort ${file.name} från fillistan` }, "Ta bort fil"), h("button", { type: "button", onClick: (e) => this.onRemoveFileHandler(e, file['id']), class: "digi-form-file-upload__button digi-form-file-upload__button--mobile digi-form-file-upload__button--file", "aria-label": `Misslyckad uppladdning, ta bort ${file.name} från fillistan` }, h("digi-icon-x-button-outline", { style: {
          '--digi--icon--height': '20px',
          '--digi--icon--color': 'var(--digi--global--color--function--info--base)'
        } }))), h("div", { class: "digi-form-file-upload__file-footer" }, h("p", { class: 'hidden-mobile', role: "status", "aria-label": `${file['name']} gick inte att ladda upp. ${file['error']}` }, `Filen gick inte att ladda upp. ${file['error']}`))))));
    }))))));
  }
  get hostElement() { return getElement(this); }
};
FormFileUpload.style = formFileUploadCss;

export { FormFileUpload as digi_form_file_upload };
