var FormInputValidation;
(function (FormInputValidation) {
  FormInputValidation["SUCCESS"] = "success";
  FormInputValidation["ERROR"] = "error";
  FormInputValidation["WARNING"] = "warning";
  FormInputValidation["NEUTRAL"] = "neutral";
})(FormInputValidation || (FormInputValidation = {}));

var FormInputVariation;
(function (FormInputVariation) {
  FormInputVariation["SMALL"] = "small";
  FormInputVariation["MEDIUM"] = "medium";
  FormInputVariation["LARGE"] = "large";
})(FormInputVariation || (FormInputVariation = {}));

export { FormInputValidation as F, FormInputVariation as a };
