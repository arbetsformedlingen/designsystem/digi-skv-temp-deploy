import { r as registerInstance, h, g as getElement } from './index-7f342a75.js';
import { r as randomIdGenerator } from './randomIdGenerator.util-a9066813.js';
import { P as ProgressbarRole, a as ProgressbarVariation } from './progressbar-variation.enum-de87bf7f.js';

const progressbarCss = ".digi--util--fs--xs.sc-digi-progressbar{font-size:var(--digi--global--typography--font-size--small) !important}.digi--util--fs--s.sc-digi-progressbar{font-size:calc(var(--digi--global--typography--font-size--small) + 1px) !important}.digi--util--fs--m.sc-digi-progressbar{font-size:var(--digi--global--typography--font-size--base) !important}.digi--util--fs--l.sc-digi-progressbar{font-size:var(--digi--global--typography--font-size--large) !important}.digi--util--fw--sb.sc-digi-progressbar{font-weight:var(--digi--global--typography--font-weight--semibold) !important}.digi--util--pt--1.sc-digi-progressbar{padding-top:var(--digi--global--spacing--smallest-2) !important}.digi--util--pt--10.sc-digi-progressbar{padding-top:var(--digi--global--spacing--smallest) !important}.digi--util--pt--20.sc-digi-progressbar{padding-top:var(--digi--global--spacing--base) !important}.digi--util--pt--30.sc-digi-progressbar{padding-top:var(--digi--global--spacing--largest-2) !important}.digi--util--pt--40.sc-digi-progressbar{padding-top:var(--digi--global--spacing--largest-4) !important}.digi--util--pt--50.sc-digi-progressbar{padding-top:2.5rem !important}.digi--util--pt--60.sc-digi-progressbar{padding-top:var(--digi--global--spacing--largest-5) !important}.digi--util--pt--70.sc-digi-progressbar{padding-top:var(--digi--global--spacing--largest-6) !important}.digi--util--pt--80.sc-digi-progressbar{padding-top:4.5rem !important}.digi--util--pt--90.sc-digi-progressbar{padding-top:5rem !important}.digi--util--pb--1.sc-digi-progressbar{padding-bottom:var(--digi--global--spacing--smallest-2) !important}.digi--util--pb--10.sc-digi-progressbar{padding-bottom:var(--digi--global--spacing--smallest) !important}.digi--util--pb--20.sc-digi-progressbar{padding-bottom:var(--digi--global--spacing--base) !important}.digi--util--pb--30.sc-digi-progressbar{padding-bottom:var(--digi--global--spacing--largest-2) !important}.digi--util--pb--40.sc-digi-progressbar{padding-bottom:var(--digi--global--spacing--largest-4) !important}.digi--util--pb--50.sc-digi-progressbar{padding-bottom:2.5rem !important}.digi--util--pb--60.sc-digi-progressbar{padding-bottom:var(--digi--global--spacing--largest-5) !important}.digi--util--pb--70.sc-digi-progressbar{padding-bottom:var(--digi--global--spacing--largest-6) !important}.digi--util--pb--80.sc-digi-progressbar{padding-bottom:4.5rem !important}.digi--util--pb--90.sc-digi-progressbar{padding-bottom:5rem !important}.digi--util--p--1.sc-digi-progressbar{padding:var(--digi--global--spacing--smallest-2) !important}.digi--util--p--10.sc-digi-progressbar{padding:var(--digi--global--spacing--smallest) !important}.digi--util--p--20.sc-digi-progressbar{padding:var(--digi--global--spacing--base) !important}.digi--util--p--30.sc-digi-progressbar{padding:var(--digi--global--spacing--largest-2) !important}.digi--util--p--40.sc-digi-progressbar{padding:var(--digi--global--spacing--largest-4) !important}.digi--util--p--50.sc-digi-progressbar{padding:2.5rem !important}.digi--util--p--60.sc-digi-progressbar{padding:var(--digi--global--spacing--largest-5) !important}.digi--util--p--70.sc-digi-progressbar{padding:var(--digi--global--spacing--largest-6) !important}.digi--util--p--80.sc-digi-progressbar{padding:4.5rem !important}.digi--util--p--90.sc-digi-progressbar{padding:5rem !important}.digi--util--ptb--1.sc-digi-progressbar{padding-top:var(--digi--global--spacing--smallest-2) !important;padding-bottom:var(--digi--global--spacing--smallest-2) !important}.digi--util--ptb--10.sc-digi-progressbar{padding-top:var(--digi--global--spacing--smallest) !important;padding-bottom:var(--digi--global--spacing--smallest) !important}.digi--util--ptb--20.sc-digi-progressbar{padding-top:var(--digi--global--spacing--base) !important;padding-bottom:var(--digi--global--spacing--base) !important}.digi--util--ptb--30.sc-digi-progressbar{padding-top:var(--digi--global--spacing--largest-2) !important;padding-bottom:var(--digi--global--spacing--largest-2) !important}.digi--util--ptb--40.sc-digi-progressbar{padding-top:var(--digi--global--spacing--largest-4) !important;padding-bottom:var(--digi--global--spacing--largest-4) !important}.digi--util--ptb--50.sc-digi-progressbar{padding-top:2.5rem !important;padding-bottom:2.5rem !important}.digi--util--ptb--60.sc-digi-progressbar{padding-top:var(--digi--global--spacing--largest-5) !important;padding-bottom:var(--digi--global--spacing--largest-5) !important}.digi--util--ptb--70.sc-digi-progressbar{padding-top:var(--digi--global--spacing--largest-6) !important;padding-bottom:var(--digi--global--spacing--largest-6) !important}.digi--util--ptb--80.sc-digi-progressbar{padding-top:4.5rem !important;padding-bottom:4.5rem !important}.digi--util--ptb--90.sc-digi-progressbar{padding-top:5rem !important;padding-bottom:5rem !important}.digi--util--plr--1.sc-digi-progressbar{padding-left:var(--digi--global--spacing--smallest-2) !important;padding-right:var(--digi--global--spacing--smallest-2) !important}.digi--util--plr--10.sc-digi-progressbar{padding-left:var(--digi--global--spacing--smallest) !important;padding-right:var(--digi--global--spacing--smallest) !important}.digi--util--plr--20.sc-digi-progressbar{padding-left:var(--digi--global--spacing--base) !important;padding-right:var(--digi--global--spacing--base) !important}.digi--util--plr--30.sc-digi-progressbar{padding-left:var(--digi--global--spacing--largest-2) !important;padding-right:var(--digi--global--spacing--largest-2) !important}.digi--util--plr--40.sc-digi-progressbar{padding-left:var(--digi--global--spacing--largest-4) !important;padding-right:var(--digi--global--spacing--largest-4) !important}.digi--util--plr--50.sc-digi-progressbar{padding-left:2.5rem !important;padding-right:2.5rem !important}.digi--util--plr--60.sc-digi-progressbar{padding-left:var(--digi--global--spacing--largest-5) !important;padding-right:var(--digi--global--spacing--largest-5) !important}.digi--util--plr--70.sc-digi-progressbar{padding-left:var(--digi--global--spacing--largest-6) !important;padding-right:var(--digi--global--spacing--largest-6) !important}.digi--util--plr--80.sc-digi-progressbar{padding-left:4.5rem !important;padding-right:4.5rem !important}.digi--util--plr--90.sc-digi-progressbar{padding-left:5rem !important;padding-right:5rem !important}.digi--util--sr-only.sc-digi-progressbar{border:0;clip:rect(1px, 1px, 1px, 1px);-webkit-clip-path:inset(50%);clip-path:inset(50%);height:1px;margin:-1px;overflow:hidden;padding:0;position:absolute;width:1px;white-space:nowrap}.sc-digi-progressbar-h{--digi--progressbar--margin:0 0 var(--digi--margin--medium);--digi--progressbar--label--font-size:var(--digi--typography--label--font-size--desktop);--digi--progressbar--label--margin:var(--digi--gutter--medium);--digi--progressbar--border-color:var(--digi--color--border--primary);--digi--progressbar--border-color--active:var(--digi--color--border--success);--digi--progressbar--border-color--secondary--active:var(--digi--color--border--secondary);--digi--progressbar--border-width:var(--digi--border-width--primary);--digi--progressbar--gap:var(--digi--padding--largest);--digi--progressbar--gap--medium:var(--digi--padding--largest);--digi--progressbar--gap--large:var(--digi--padding--largest-2);--digi--progressbar--gap--x-large:var(--digi--padding--largest-2);--digi--progressbar--step--background:var(--digi--color--background--primary);--digi--progressbar--step--background--active:var(--digi--color--background--complementary-1);--digi--progressbar--step--background--secondary--active:var(--digi--color--background--inverted-1);--digi--progressbar--step--size:var(--digi--global--spacing--base);--digi--progressbar--step--size--large:var(--digi--global--spacing--large);--digi--progressbar--step--size--active:var(--digi--global--spacing--larger);--digi--progressbar--step--size--active--large:var(--digi--global--spacing--largest);--digi--progressbar--step--line--width:var(--digi--border-width--primary);--digi--progressbar--step--line--color:var(--digi--color--border--primary);--digi--progressbar--step--line--color--current:var(--digi--color--border--primary);--digi--progressbar--step--line--color--active:var(--digi--color--border--success);--digi--progressbar--step--line--color--secondary--active:var(--digi--color--border--secondary)}@media (min-width: 62rem){.sc-digi-progressbar-h{--digi--progressbar--step--size:var(--digi--progressbar--step--size--large);--digi--progressbar--step--size--active:var(--digi--progressbar--step--size--active--large)}}.sc-digi-progressbar-h .digi-progressbar.sc-digi-progressbar{font-size:var(--digi--progressbar--label--font-size);margin:var(--digi--progressbar--margin)}.sc-digi-progressbar-h .digi-progressbar--secondary.sc-digi-progressbar{--digi--progressbar--step--background--active:var(--digi--progressbar--step--background--secondary--active);--digi--progressbar--border-color--active:var(--digi--progressbar--border-color--secondary--active);--digi--progressbar--step--line--color--active:var(--digi--progressbar--step--line--color--secondary--active)}.sc-digi-progressbar-h .digi-progressbar.sc-digi-progressbar:not(.digi-progressbar--linear){--digi--progressbar--step--line--color--active:var(--digi--progressbar--step--line--color)}.sc-digi-progressbar-h .digi-progressbar__label.sc-digi-progressbar{font-family:var(--digi--global--typography--font-family--default);font-size:var(--digi--progressbar--label--font-size);line-height:var(--digi--typography--body--line-height--desktop);color:var(--digi--color--text--primary);margin:var(--digi--progressbar--label--margin)}.sc-digi-progressbar-h .digi-progressbar__list.sc-digi-progressbar{position:relative;display:inline-flex;list-style:none;padding:0;margin:0;display:grid;grid-template-columns:repeat(auto-fit, calc(var(--digi--progressbar--step--size) + var(--digi--progressbar--gap)))}@media (min-width: 48rem){.sc-digi-progressbar-h .digi-progressbar__list.sc-digi-progressbar{--digi--progressbar--gap:var(--digi--progressbar--gap--medium)}}@media (min-width: 62rem){.sc-digi-progressbar-h .digi-progressbar__list.sc-digi-progressbar{--digi--progressbar--gap:var(--digi--progressbar--gap--large)}}@media (min-width: 80rem){.sc-digi-progressbar-h .digi-progressbar__list.sc-digi-progressbar{--digi--progressbar--gap:var(--digi--progressbar--gap--x-large)}}.sc-digi-progressbar-h .digi-progressbar__step.sc-digi-progressbar{position:relative;height:var(--digi--progressbar--step--size--active)}.sc-digi-progressbar-h .digi-progressbar__step.sc-digi-progressbar:last-child{width:var(--digi--progressbar--step--size--active)}.sc-digi-progressbar-h .digi-progressbar__step.sc-digi-progressbar:last-child::before{display:none}.sc-digi-progressbar-h .digi-progressbar__step.sc-digi-progressbar::before{content:\"\";position:absolute;top:50%;transform:translateY(-50%);display:inline-block;width:100%;border-top:solid var(--digi--progressbar--step--line--width) var(--digi--progressbar--step--line--color)}.sc-digi-progressbar-h .digi-progressbar__step--active.sc-digi-progressbar{--digi--progressbar--border-color:var(--digi--progressbar--border-color--active);--digi--progressbar--step--background:var(--digi--progressbar--step--background--active);--digi--progressbar--step--size:var(--digi--progressbar--step--size--active);--digi--progressbar--step--line--color:var(--digi--progressbar--step--line--color--active)}.sc-digi-progressbar-h .digi-progressbar__step--active.sc-digi-progressbar .digi-progressbar__step-indicator.sc-digi-progressbar{margin-left:calc((var(--digi--progressbar--step--size--large) - var(--digi--progressbar--step--size--active--large)) / 2)}.sc-digi-progressbar-h .digi-progressbar__step--current.sc-digi-progressbar{--digi--progressbar--step--line--color:var(--digi--progressbar--step--line--color--current)}.sc-digi-progressbar-h .digi-progressbar__step-indicator.sc-digi-progressbar{content:\"\";position:absolute;display:inline-flex;align-items:center;justify-content:center;top:50%;transform:translateY(-50%);width:var(--digi--progressbar--step--size);height:var(--digi--progressbar--step--size);border-radius:50%;border:solid var(--digi--progressbar--border-width) var(--digi--progressbar--border-color);background:var(--digi--progressbar--step--background)}.sc-digi-progressbar-h .digi-progressbar__step-number.sc-digi-progressbar{border:0;clip:rect(1px, 1px, 1px, 1px);-webkit-clip-path:inset(50%);clip-path:inset(50%);height:1px;margin:-1px;overflow:hidden;padding:0;position:absolute;width:1px;white-space:nowrap}";

const Progressbar = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.completedSteps = undefined;
    this.totalSteps = [];
    this.listItems = undefined;
    this.hasSlottedItems = undefined;
    this.afCompletedSteps = undefined;
    this.afTotalSteps = undefined;
    this.afStepsSeparator = "av";
    this.afStepsLabel = "frågor besvarade";
    this.afRole = ProgressbarRole.STATUS;
    this.afVariation = ProgressbarVariation.PRIMARY;
    this.afId = randomIdGenerator('digi-progressbar');
  }
  componentWillLoad() {
    this.changeTotalStepsHandler(this.afTotalSteps);
    this.getItems();
  }
  componentDidLoad() {
    this.getItems();
  }
  changeTotalStepsHandler(value) {
    let arr = [];
    let n = 0;
    while (n < value) {
      n++;
      arr.push(n);
    }
    this.totalSteps = arr;
  }
  changeCompletedStepsHandler(value) {
    this.completedSteps = value;
  }
  getItems(update) {
    let items;
    if (update) {
      items = this._observer.children;
    }
    else {
      items = this.hostElement.querySelectorAll('.digi-progressbar__slot li');
    }
    if (items.length > 0) {
      this.hasSlottedItems = true;
      this.listItems = [...items]
        .filter((element) => element.tagName.toLowerCase() === 'li')
        .map((element) => {
        const el = element;
        const checked = el.matches('.completed');
        return {
          text: element.textContent,
          checked: checked
        };
      });
    }
  }
  get cssModifiers() {
    return {
      'digi-progressbar': true,
      'digi-progressbar--primary': this.afVariation === ProgressbarVariation.PRIMARY,
      'digi-progressbar--secondary': this.afVariation === ProgressbarVariation.SECONDARY,
      'digi-progressbar--linear': !this.hasSlottedItems
    };
  }
  get slottedContent() {
    return;
  }
  render() {
    return (h("div", { class: this.cssModifiers, role: this.afRole === ProgressbarRole.STATUS ? ProgressbarRole.STATUS : null }, h("p", { class: "digi-progressbar__label" }, this.afCompletedSteps, " ", this.afStepsSeparator, " ", this.afTotalSteps, " ", this.afStepsLabel), !this.hasSlottedItems
      ? (h("ol", { class: "digi-progressbar__list", "aria-hidden": "true" }, this.totalSteps.map((item, i) => {
        return (h("li", { class: {
            'digi-progressbar__step': true,
            'digi-progressbar__step--active': this.afCompletedSteps > i,
            'digi-progressbar__step--current': this.afCompletedSteps === i + 1
          } }, h("span", { class: "digi-progressbar__step-indicator" }, h("span", { class: "digi-progressbar__step-number" }, item))));
      })))
      : (h("ol", { class: "digi-progressbar__list", "aria-hidden": "true" }, this.listItems.map((item, index) => {
        return (h("li", { class: {
            'digi-progressbar__step': true,
            'digi-progressbar__step--active': item.checked,
          } }, h("span", { class: "digi-progressbar__step-indicator" }, h("span", { class: "digi-progressbar__step-number" }, index + 1))));
      }))), h("div", { hidden: true, class: "digi-progressbar__slot" }, h("digi-util-mutation-observer", { ref: (el) => (this._observer = el), onAfOnChange: () => this.getItems() }, h("slot", null)))));
  }
  get hostElement() { return getElement(this); }
  static get watchers() { return {
    "afTotalSteps": ["changeTotalStepsHandler"],
    "afCompletedSteps": ["changeCompletedStepsHandler"]
  }; }
};
Progressbar.style = progressbarCss;

export { Progressbar as digi_progressbar };
