var NotificationDetailVariation;
(function (NotificationDetailVariation) {
  NotificationDetailVariation["INFO"] = "info";
  NotificationDetailVariation["WARNING"] = "warning";
  NotificationDetailVariation["DANGER"] = "danger";
})(NotificationDetailVariation || (NotificationDetailVariation = {}));

export { NotificationDetailVariation as N };
