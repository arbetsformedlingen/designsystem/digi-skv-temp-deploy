import { r as registerInstance, h, c as createEvent, g as getElement } from './index-7f342a75.js';
import { C as CodeBlockVariation } from './code-block-variation.enum-42e2bb44.js';
import { C as CodeBlockLanguage, b as FormRadiobuttonVariation, F as FormRadiobuttonLayout, a as FormRadiobuttonValidation } from './form-radiobutton-variation.enum-60c58c7d.js';
import { p as prism } from './prism-git-2023026c.js';
import { B as ButtonVariation } from './button-variation.enum-3cc46e30.js';
import { B as ButtonSize } from './button-size.enum-8999fdcb.js';
import { r as randomIdGenerator } from './randomIdGenerator.util-a9066813.js';

const codeBlockCss = "digi-code-block{--digi--code-block--background:var(--digi--color--background--primary);--digi--code-block--border-radius:0.3rem;--digi--code-block--border:1px solid var(--digi--color--border--neutral-3);display:block}digi-code-block .digi-code-block{border-radius:var(--digi--code-block--border-radius);border:var(--digi--code-block--border);overflow:hidden;position:relative}digi-code-block .digi-code-block pre{margin:0}digi-code-block .digi-code-block--light{}digi-code-block .digi-code-block--light code[class*=language-],digi-code-block .digi-code-block--light pre[class*=language-]{color:#545454;background:none;font-family:var(--digi--global--typography--font-family--mono);text-align:left;white-space:pre-wrap;word-spacing:normal;word-break:normal;word-wrap:normal;line-height:1.5;-moz-tab-size:4;-o-tab-size:4;tab-size:4;-webkit-hyphens:none;hyphens:none}digi-code-block .digi-code-block--light pre[class*=language-]{padding:1em;overflow:auto}digi-code-block .digi-code-block--light :not(pre)>code[class*=language-],digi-code-block .digi-code-block--light pre[class*=language-]{background:#fefefe}digi-code-block .digi-code-block--light :not(pre)>code[class*=language-]{padding:0.1em;white-space:normal}digi-code-block .digi-code-block--light .token.comment,digi-code-block .digi-code-block--light .token.prolog,digi-code-block .digi-code-block--light .token.doctype,digi-code-block .digi-code-block--light .token.cdata{color:#696969}digi-code-block .digi-code-block--light .token.punctuation{color:#545454}digi-code-block .digi-code-block--light .token.property,digi-code-block .digi-code-block--light .token.tag,digi-code-block .digi-code-block--light .token.constant,digi-code-block .digi-code-block--light .token.symbol,digi-code-block .digi-code-block--light .token.deleted{color:#007faa}digi-code-block .digi-code-block--light .token.boolean,digi-code-block .digi-code-block--light .token.number{color:#008000}digi-code-block .digi-code-block--light .token.selector,digi-code-block .digi-code-block--light .token.attr-name,digi-code-block .digi-code-block--light .token.string,digi-code-block .digi-code-block--light .token.char,digi-code-block .digi-code-block--light .token.builtin,digi-code-block .digi-code-block--light .token.inserted{color:#aa5d00}digi-code-block .digi-code-block--light .token.operator,digi-code-block .digi-code-block--light .token.entity,digi-code-block .digi-code-block--light .token.url,digi-code-block .digi-code-block--light .language-css .token.string,digi-code-block .digi-code-block--light .style .token.string,digi-code-block .digi-code-block--light .token.variable{color:#008000}digi-code-block .digi-code-block--light .token.atrule,digi-code-block .digi-code-block--light .token.attr-value,digi-code-block .digi-code-block--light .token.function{color:#aa5d00}digi-code-block .digi-code-block--light .token.keyword{color:#d91e18}digi-code-block .digi-code-block--light .token.regex,digi-code-block .digi-code-block--light .token.important{color:#d91e18}digi-code-block .digi-code-block--light .token.important,digi-code-block .digi-code-block--light .token.bold{font-weight:bold}digi-code-block .digi-code-block--light .token.italic{font-style:italic}digi-code-block .digi-code-block--light .token.entity{cursor:help}@media screen and (-ms-high-contrast: active){digi-code-block .digi-code-block--light code[class*=language-],digi-code-block .digi-code-block--light pre[class*=language-]{color:windowText;background:window}digi-code-block .digi-code-block--light :not(pre)>code[class*=language-],digi-code-block .digi-code-block--light pre[class*=language-]{background:window}digi-code-block .digi-code-block--light .token.important{background:highlight;color:window;font-weight:normal}digi-code-block .digi-code-block--light .token.atrule,digi-code-block .digi-code-block--light .token.attr-value,digi-code-block .digi-code-block--light .token.function,digi-code-block .digi-code-block--light .token.keyword,digi-code-block .digi-code-block--light .token.operator,digi-code-block .digi-code-block--light .token.selector{font-weight:bold}digi-code-block .digi-code-block--light .token.attr-value,digi-code-block .digi-code-block--light .token.comment,digi-code-block .digi-code-block--light .token.doctype,digi-code-block .digi-code-block--light .token.function,digi-code-block .digi-code-block--light .token.keyword,digi-code-block .digi-code-block--light .token.operator,digi-code-block .digi-code-block--light .token.property,digi-code-block .digi-code-block--light .token.string{color:highlight}digi-code-block .digi-code-block--light .token.attr-value,digi-code-block .digi-code-block--light .token.url{font-weight:normal}}digi-code-block .digi-code-block--dark{}digi-code-block .digi-code-block--dark .digi-code-block__button{--digi--button--color--text--function--default:var(--digi--color--text--inverted);--digi--button--color--text--function--hover:var(--digi--color--text--inverted)}digi-code-block .digi-code-block--dark code[class*=language-],digi-code-block .digi-code-block--dark pre[class*=language-]{color:#f8f8f2;background:none;font-family:var(--digi--global--typography--font-family--mono);text-align:left;white-space:pre-wrap;word-spacing:normal;word-break:normal;word-wrap:normal;line-height:1.5;-moz-tab-size:4;-o-tab-size:4;tab-size:4;-webkit-hyphens:none;hyphens:none}digi-code-block .digi-code-block--dark pre[class*=language-]{padding:1em;overflow:auto}digi-code-block .digi-code-block--dark :not(pre)>code[class*=language-],digi-code-block .digi-code-block--dark pre[class*=language-]{--digi--color--background--inverted-3:#272727;background:var(--digi--color--background--inverted-3)}digi-code-block .digi-code-block--dark :not(pre)>code[class*=language-]{padding:0.1em;white-space:normal}digi-code-block .digi-code-block--dark .token.comment,digi-code-block .digi-code-block--dark .token.prolog,digi-code-block .digi-code-block--dark .token.doctype,digi-code-block .digi-code-block--dark .token.cdata{color:#d4d0ab}digi-code-block .digi-code-block--dark .token.punctuation{color:#fefefe}digi-code-block .digi-code-block--dark .token.property,digi-code-block .digi-code-block--dark .token.tag,digi-code-block .digi-code-block--dark .token.constant,digi-code-block .digi-code-block--dark .token.symbol,digi-code-block .digi-code-block--dark .token.deleted{color:#ffa07a}digi-code-block .digi-code-block--dark .token.boolean,digi-code-block .digi-code-block--dark .token.number{color:#00e0e0}digi-code-block .digi-code-block--dark .token.selector,digi-code-block .digi-code-block--dark .token.attr-name,digi-code-block .digi-code-block--dark .token.string,digi-code-block .digi-code-block--dark .token.char,digi-code-block .digi-code-block--dark .token.builtin,digi-code-block .digi-code-block--dark .token.inserted{color:#abe338}digi-code-block .digi-code-block--dark .token.operator,digi-code-block .digi-code-block--dark .token.entity,digi-code-block .digi-code-block--dark .token.url,digi-code-block .digi-code-block--dark .language-css .token.string,digi-code-block .digi-code-block--dark .style .token.string,digi-code-block .digi-code-block--dark .token.variable{color:#00e0e0}digi-code-block .digi-code-block--dark .token.atrule,digi-code-block .digi-code-block--dark .token.attr-value,digi-code-block .digi-code-block--dark .token.function{color:#ffd700}digi-code-block .digi-code-block--dark .token.keyword{color:#00e0e0}digi-code-block .digi-code-block--dark .token.regex,digi-code-block .digi-code-block--dark .token.important{color:#ffd700}digi-code-block .digi-code-block--dark .token.important,digi-code-block .digi-code-block--dark .token.bold{font-weight:bold}digi-code-block .digi-code-block--dark .token.italic{font-style:italic}digi-code-block .digi-code-block--dark .token.entity{cursor:help}@media screen and (-ms-high-contrast: active){digi-code-block .digi-code-block--dark code[class*=language-],digi-code-block .digi-code-block--dark pre[class*=language-]{color:windowText;background:window}digi-code-block .digi-code-block--dark :not(pre)>code[class*=language-],digi-code-block .digi-code-block--dark pre[class*=language-]{background:window}digi-code-block .digi-code-block--dark .token.important{background:highlight;color:window;font-weight:normal}digi-code-block .digi-code-block--dark .token.atrule,digi-code-block .digi-code-block--dark .token.attr-value,digi-code-block .digi-code-block--dark .token.function,digi-code-block .digi-code-block--dark .token.keyword,digi-code-block .digi-code-block--dark .token.operator,digi-code-block .digi-code-block--dark .token.selector{font-weight:bold}digi-code-block .digi-code-block--dark .token.attr-value,digi-code-block .digi-code-block--dark .token.comment,digi-code-block .digi-code-block--dark .token.doctype,digi-code-block .digi-code-block--dark .token.function,digi-code-block .digi-code-block--dark .token.keyword,digi-code-block .digi-code-block--dark .token.operator,digi-code-block .digi-code-block--dark .token.property,digi-code-block .digi-code-block--dark .token.string{color:highlight}digi-code-block .digi-code-block--dark .token.attr-value,digi-code-block .digi-code-block--dark .token.url{font-weight:normal}}digi-code-block .digi-code-block__toolbar{padding-bottom:0.5rem;display:flex;justify-content:flex-end;position:absolute;bottom:0;width:100%}digi-code-block .digi-code-block__button{margin-right:var(--digi--margin--smaller)}";

const CodeBlock = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.highlightedCode = undefined;
    this.afCode = undefined;
    this.afVariation = CodeBlockVariation.DARK;
    this.afLanguage = CodeBlockLanguage.HTML;
    this.afLang = 'en';
    this.afHideToolbar = false;
  }
  componentWillLoad() {
    prism.manual = true;
    this.formatCode();
  }
  formatCode() {
    this.highlightedCode = prism.highlight(this.afCode, prism.languages[this.afLanguage], this.afLanguage);
  }
  copyButtonClickHandler() {
    navigator.clipboard.writeText(this.afCode).then(() => {
      // console.log('hurra!');
    }, (err) => {
      // console.log('åh nej!', err);
      console.error(err);
    });
  }
  get cssModifiers() {
    return {
      'digi-code-block--light': this.afVariation === CodeBlockVariation.LIGHT,
      'digi-code-block--dark': this.afVariation === CodeBlockVariation.DARK
    };
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-code-block': true }, this.cssModifiers) }, h("pre", { class: `digi-code-block__pre language-${this.afLanguage}` }, h("code", { class: "digi-code-block__code", lang: this.afLang, innerHTML: this.highlightedCode })), !this.afHideToolbar && (h("div", { class: "digi-code-block__toolbar" }, h("digi-button", { class: "digi-code-block__button", onAfOnClick: () => this.copyButtonClickHandler(), afVariation: ButtonVariation.FUNCTION, afSize: ButtonSize.SMALL }, h("digi-icon", { class: "digi-code-block__icon", "aria-hidden": "true", afName: `copy`, slot: "icon" }), "Kopiera kod")))));
  }
  static get watchers() { return {
    "afCode": ["formatCode"]
  }; }
};
CodeBlock.style = codeBlockCss;

const formRadiobuttonCss = ".sc-digi-form-radiobutton-h{--digi--form-radiobutton--border-color:var(--digi--color--border--primary);--digi--form-radiobutton--border-color--primary:var(--digi--color--border--radio-primary);--digi--form-radiobutton--border-color--secondary:var(--digi--color--border--radio-secondary);--digi--form-radiobutton--border-color--error:var(--digi--color--border--danger);--digi--form-radiobutton--box-shadow--error:var(--digi--color--border--danger);--digi--form-radiobutton--background-color:var(--digi--color--background--primary);--digi--form-radiobutton--background-color--primary:var(--digi--color--background--radio-primary);--digi--form-radiobutton--background-color--secondary:var(--digi--color--background--radio-secondary);--digi--form-radiobutton--background-color--error:var(--digi--color--background--danger-2);--digi--form-radiobutton--label--background-color--hover:var(--digi--color--background--input-empty);--digi--form-radiobutton--shadow--color--focus:var(--digi--color--border--radio-focus);--digi--form-radiobutton--border-width:var(--digi--border-width--primary);--digi--form-radiobutton--color--marker:var(--digi--color--background--radio-marker);--digi--form-radiobutton--size:1.25rem;--digi--form-radiobutton--marker--size:calc(var(--digi--typography--input--font-size--desktop-large) * 0.4);--digi--form-radiobutton--height:var(--digi--input-height--large);--digi--form-radiobutton--border-radius:var(--digi--border-radius--primary);--digi--form-radiobutton--padding:var(--digi--gutter--icon)}.sc-digi-form-radiobutton-h .digi-form-radiobutton.sc-digi-form-radiobutton{--BACKGROUND--DEFAULT:var(--digi--form-radiobutton--background-color);--BORDER-COLOR--DEFAULT:var(--digi--form-radiobutton--border-color)}.sc-digi-form-radiobutton-h .digi-form-radiobutton--primary.sc-digi-form-radiobutton{--BACKGROUND--CHECKED:var(--digi--form-radiobutton--background-color--primary);--BORDER-COLOR--CHECKED:var(--digi--form-radiobutton--border-color--primary)}.sc-digi-form-radiobutton-h .digi-form-radiobutton--secondary.sc-digi-form-radiobutton{--BACKGROUND--CHECKED:var(--digi--form-radiobutton--background-color--secondary);--BORDER-COLOR--CHECKED:var(--digi--form-radiobutton--border-color--secondary)}.sc-digi-form-radiobutton-h .digi-form-radiobutton--error.sc-digi-form-radiobutton .digi-form-radiobutton__input.sc-digi-form-radiobutton:not(:checked)~.digi-form-radiobutton__label.sc-digi-form-radiobutton .digi-form-radiobutton__circle.sc-digi-form-radiobutton{--BACKGROUND--CHECKED:var(--digi--form-radiobutton--background-color--error);--BORDER-COLOR--CHECKED:var(--digi--form-radiobutton--border-color--error)}.sc-digi-form-radiobutton-h .digi-form-radiobutton--error.sc-digi-form-radiobutton .digi-form-radiobutton__input.sc-digi-form-radiobutton~.digi-form-radiobutton__label.sc-digi-form-radiobutton .digi-form-radiobutton__circle.sc-digi-form-radiobutton{background-color:var(--BACKGROUND--CHECKED);border-color:var(--BORDER-COLOR--CHECKED);box-shadow:0 0 0 var(--digi--border-width--primary) var(--digi--form-radiobutton--box-shadow--error)}.digi-form-radiobutton__input.sc-digi-form-radiobutton{position:absolute;opacity:0;cursor:pointer;height:0;width:0;top:0;left:0}.digi-form-radiobutton__label.sc-digi-form-radiobutton{display:inline-flex;align-items:center;gap:var(--digi--gutter--icon);font-family:var(--digi--global--typography--font-family--default);font-size:var(--digi--typography--body--font-size--desktop);color:var(--digi--color--text--primary);font-weight:var(--digi--typography--body--font-weight--desktop);cursor:pointer}.digi-form-radiobutton--layout-block.sc-digi-form-radiobutton .digi-form-radiobutton__label.sc-digi-form-radiobutton{display:flex;padding:0 var(--digi--form-radiobutton--padding);min-height:var(--digi--form-radiobutton--height);border-radius:var(--digi--border-radius--primary)}.digi-form-radiobutton--layout-block.sc-digi-form-radiobutton .digi-form-radiobutton__label.sc-digi-form-radiobutton:hover{background-color:var(--digi--form-radiobutton--label--background-color--hover)}.digi-form-radiobutton__circle.sc-digi-form-radiobutton{cursor:pointer;background-color:var(--BACKGROUND--DEFAULT);border-color:var(--BORDER-COLOR--DEFAULT);border-width:var(--digi--form-radiobutton--border-width);border-style:solid;height:var(--digi--form-radiobutton--size);width:var(--digi--form-radiobutton--size);border-radius:var(--digi--form-radiobutton--size);position:relative}.digi-form-radiobutton__input.sc-digi-form-radiobutton:focus-visible~.digi-form-radiobutton__label.sc-digi-form-radiobutton .digi-form-radiobutton__circle.sc-digi-form-radiobutton{box-shadow:0 0 var(--digi--border-width--primary) var(--digi--form-radiobutton--shadow--color--focus)}.digi-form-radiobutton--layout-block.sc-digi-form-radiobutton .digi-form-radiobutton__input.sc-digi-form-radiobutton:focus-visible~.digi-form-radiobutton__label.sc-digi-form-radiobutton{box-shadow:none;outline:var(--digi--focus-outline);background-color:var(--digi--form-radiobutton--label--background-color--hover)}.digi-form-radiobutton__input.sc-digi-form-radiobutton:checked~.digi-form-radiobutton__label.sc-digi-form-radiobutton .digi-form-radiobutton__circle.sc-digi-form-radiobutton{background-color:var(--BACKGROUND--CHECKED);border-color:var(--BORDER-COLOR--CHECKED)}.digi-form-radiobutton__input.sc-digi-form-radiobutton:checked~.digi-form-radiobutton__label.sc-digi-form-radiobutton .digi-form-radiobutton__circle.sc-digi-form-radiobutton::after{content:\"\";width:var(--digi--form-radiobutton--marker--size);height:var(--digi--form-radiobutton--marker--size);border-radius:var(--digi--form-radiobutton--marker--size);background:var(--digi--form-radiobutton--color--marker);position:absolute;left:calc(50% - var(--digi--form-radiobutton--marker--size) / 2);top:calc(50% - var(--digi--form-radiobutton--marker--size) / 2)}";

const FormRadiobutton = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.afOnChange = createEvent(this, "afOnChange", 7);
    this.afOnBlur = createEvent(this, "afOnBlur", 7);
    this.afOnFocus = createEvent(this, "afOnFocus", 7);
    this.afOnFocusout = createEvent(this, "afOnFocusout", 7);
    this.afOnInput = createEvent(this, "afOnInput", 7);
    this.afOnDirty = createEvent(this, "afOnDirty", 7);
    this.afOnTouched = createEvent(this, "afOnTouched", 7);
    this.dirty = false;
    this.touched = false;
    this.afLabel = undefined;
    this.afVariation = FormRadiobuttonVariation.PRIMARY;
    this.afLayout = FormRadiobuttonLayout.INLINE;
    this.afName = undefined;
    this.afId = randomIdGenerator('digi-form-radiobutton');
    this.afRequired = undefined;
    this.value = undefined;
    this.afValue = undefined;
    this.checked = undefined;
    this.afChecked = undefined;
    this.afValidation = undefined;
    this.afAriaLabelledby = undefined;
    this.afAriaDescribedby = undefined;
    this.afAutofocus = undefined;
  }
  onValueChanged(value) {
    this.afValue = value;
  }
  onAfValueChanged(value) {
    this.value = value;
  }
  onCheckedChanged(checked) {
    this.afChecked = checked;
  }
  onAfCheckedChanged(checked) {
    this.checked = checked;
  }
  /**
   * Hämta en referens till inputelementet. Bra för att t.ex. sätta fokus programmatiskt.
   * @en Returns a reference to the input element. Handy for setting focus programmatically.
   */
  async afMGetFormControlElement() {
    return this._input;
  }
  componentDidLoad() {
    if (this.value) {
      this.afValue = this.value;
    }
  }
  get cssModifiers() {
    return {
      'digi-form-radiobutton--error': this.afValidation === FormRadiobuttonValidation.ERROR,
      'digi-form-radiobutton--primary': this.afVariation === FormRadiobuttonVariation.PRIMARY,
      'digi-form-radiobutton--secondary': this.afVariation === FormRadiobuttonVariation.SECONDARY,
      [`digi-form-radiobutton--layout-${this.afLayout}`]: !!this.afLayout
    };
  }
  blurHandler(e) {
    if (!this.touched) {
      this.afOnTouched.emit(e);
      this.touched = true;
    }
    this.afOnBlur.emit(e);
  }
  changeHandler(e) {
    this.afOnChange.emit(e);
  }
  focusHandler(e) {
    this.afOnFocus.emit(e);
  }
  focusoutHandler(e) {
    this.afOnFocusout.emit(e);
  }
  inputHandler(e) {
    if (!this.dirty) {
      this.afOnDirty.emit(e);
      this.dirty = true;
    }
    this.afOnInput.emit(e);
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-form-radiobutton': true }, this.cssModifiers) }, h("input", { class: "digi-form-radiobutton__input", ref: (el) => (this._input = el), onInput: (e) => this.inputHandler(e), onBlur: (e) => this.blurHandler(e), onChange: (e) => this.changeHandler(e), onFocus: (e) => this.focusHandler(e), onFocusout: (e) => this.focusoutHandler(e), "aria-describedby": this.afAriaDescribedby, "aria-labelledby": this.afAriaLabelledby, required: this.afRequired, id: this.afId, name: this.afName, type: "radio", checked: this.afChecked, value: this.afValue, autofocus: this.afAutofocus ? this.afAutofocus : null }), h("label", { htmlFor: this.afId, class: "digi-form-radiobutton__label" }, h("span", { class: "digi-form-radiobutton__circle" }), this.afLabel)));
  }
  get hostElement() { return getElement(this); }
  static get watchers() { return {
    "value": ["onValueChanged"],
    "afValue": ["onAfValueChanged"],
    "checked": ["onCheckedChanged"],
    "afChecked": ["onAfCheckedChanged"]
  }; }
};
FormRadiobutton.style = formRadiobuttonCss;

export { CodeBlock as digi_code_block, FormRadiobutton as digi_form_radiobutton };
