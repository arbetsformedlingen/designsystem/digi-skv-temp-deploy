var LayoutGridVerticalSpacing;
(function (LayoutGridVerticalSpacing) {
  LayoutGridVerticalSpacing["REGULAR"] = "regular";
  LayoutGridVerticalSpacing["NONE"] = "none";
})(LayoutGridVerticalSpacing || (LayoutGridVerticalSpacing = {}));

export { LayoutGridVerticalSpacing as L };
