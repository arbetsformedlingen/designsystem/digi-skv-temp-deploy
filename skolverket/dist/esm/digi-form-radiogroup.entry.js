import { r as registerInstance, c as createEvent, h, H as Host, g as getElement } from './index-7f342a75.js';
import { r as randomIdGenerator } from './randomIdGenerator.util-a9066813.js';

const formRadiogroupCss = "";

const FormRadiogroup = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.afOnGroupChange = createEvent(this, "afOnGroupChange", 7);
    this.radiobuttons = undefined;
    this.afValue = undefined;
    this.value = undefined;
    this.afName = randomIdGenerator('digi-form-radiogroup');
  }
  OnAfValueChange(value) {
    this.value = value;
  }
  OnValueChange(value) {
    this.afValue = value;
    this.setRadiobuttons(value);
  }
  radiobuttonChangeListener(e) {
    if (Array.from(this.radiobuttons).includes(e.target)) {
      e.preventDefault();
      this.afValue = e.detail.target.value;
      this.afOnGroupChange.emit(e);
    }
  }
  componentWillLoad() { }
  componentDidLoad() {
    this.setRadiobuttons(this.value || this.afValue);
    if (this.value)
      this.afValue = this.value;
  }
  componentWillUpdate() { }
  setRadiobuttons(value) {
    this.radiobuttons = this.hostElement.querySelectorAll('digi-form-radiobutton');
    this.radiobuttons.forEach(r => {
      r.afName = this.afName;
      if (value && r.afValue == value) {
        setTimeout(() => r.afMGetFormControlElement().then(e => e.checked || e.click()), 0);
      }
    });
  }
  get cssModifiers() {
    return {};
  }
  render() {
    return (h(Host, null, h("div", { class: Object.assign({ 'digi-form-radiogroup': true }, this.cssModifiers) }, h("digi-util-mutation-observer", { onAfOnChange: (e) => this.radiobuttonChangeListener(e) }, h("slot", null)))));
  }
  get hostElement() { return getElement(this); }
  static get watchers() { return {
    "afValue": ["OnAfValueChange"],
    "value": ["OnValueChange"]
  }; }
};
FormRadiogroup.style = formRadiogroupCss;

export { FormRadiogroup as digi_form_radiogroup };
