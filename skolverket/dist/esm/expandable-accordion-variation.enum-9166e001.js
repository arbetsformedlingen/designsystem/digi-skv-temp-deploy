var CalendarButtonSize;
(function (CalendarButtonSize) {
  CalendarButtonSize["SMALL"] = "small";
  CalendarButtonSize["MEDIUM"] = "medium";
  CalendarButtonSize["LARGE"] = "large";
})(CalendarButtonSize || (CalendarButtonSize = {}));

var CalendarSelectSize;
(function (CalendarSelectSize) {
  CalendarSelectSize["SMALL"] = "small";
  CalendarSelectSize["MEDIUM"] = "medium";
  CalendarSelectSize["LARGE"] = "large";
})(CalendarSelectSize || (CalendarSelectSize = {}));

var ExpandableAccordionHeaderLevel;
(function (ExpandableAccordionHeaderLevel) {
  ExpandableAccordionHeaderLevel["H1"] = "h1";
  ExpandableAccordionHeaderLevel["H2"] = "h2";
  ExpandableAccordionHeaderLevel["H3"] = "h3";
  ExpandableAccordionHeaderLevel["H4"] = "h4";
  ExpandableAccordionHeaderLevel["H5"] = "h5";
  ExpandableAccordionHeaderLevel["H6"] = "h6";
})(ExpandableAccordionHeaderLevel || (ExpandableAccordionHeaderLevel = {}));

var ExpandableAccordionToggleType;
(function (ExpandableAccordionToggleType) {
  ExpandableAccordionToggleType["IMPLICIT"] = "implicit";
  ExpandableAccordionToggleType["EXPLICIT"] = "explicit";
})(ExpandableAccordionToggleType || (ExpandableAccordionToggleType = {}));

var ExpandableAccordionVariation;
(function (ExpandableAccordionVariation) {
  ExpandableAccordionVariation["PRIMARY"] = "primary";
  ExpandableAccordionVariation["SECONDARY"] = "secondary";
})(ExpandableAccordionVariation || (ExpandableAccordionVariation = {}));

export { CalendarButtonSize as C, ExpandableAccordionHeaderLevel as E, CalendarSelectSize as a, ExpandableAccordionToggleType as b, ExpandableAccordionVariation as c };
