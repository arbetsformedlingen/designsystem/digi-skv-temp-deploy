import { r as registerInstance, c as createEvent, h, g as getElement } from './index-7f342a75.js';
import { r as randomIdGenerator } from './randomIdGenerator.util-a9066813.js';
import { d as detectFocusOutside } from './detectFocusOutside.util-5ce9ecfe.js';
import './detectClosest.util-2d3999b7.js';

const UtilDetectFocusOutside = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.afOnFocusOutside = createEvent(this, "afOnFocusOutside", 7);
    this.afOnFocusInside = createEvent(this, "afOnFocusInside", 7);
    this.afDataIdentifier = randomIdGenerator('data-digi-util-detect-focus-outside');
    this.$el.setAttribute(this.afDataIdentifier, '');
  }
  focusinHandler(e) {
    const target = e.target;
    const selector = `[${this.afDataIdentifier}]`;
    if (detectFocusOutside(target, selector)) {
      this.afOnFocusOutside.emit(e);
    }
    else {
      this.afOnFocusInside.emit(e);
    }
  }
  render() {
    return h("slot", null);
  }
  get $el() { return getElement(this); }
};

export { UtilDetectFocusOutside as digi_util_detect_focus_outside };
