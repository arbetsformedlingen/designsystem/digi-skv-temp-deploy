import { r as registerInstance, c as createEvent, h, g as getElement } from './index-7f342a75.js';
import { r as randomIdGenerator } from './randomIdGenerator.util-a9066813.js';
import { l as logger } from './logger.util-b54855d5.js';

const navigationTabsCss = ".sc-digi-navigation-tabs-h{--digi--navigation-tabs--padding:var(--digi--padding--medium) var(--digi--padding--largest);--digi--navigation-tabs--box-shadow--selected:inset 0 -4px 0 0 var(--digi--color--border--secondary);--digi--navigation-tabs--divider--color--indicator:var(--digi--global--color--neutral--grayscale--darkest-2);--digi--navigation-tabs--divider:1px solid var(--digi--navigation-tabs--divider--color--indicator);--digi--navigation-tabs--background-color--active:var(--digi--color--background--neutral-1);--digi--navigation-tabs--background-color--indicator:var(--digi--navigation-tabs--background-color--active);--digi--navigation-tabs--tab--border:none;--digi--navigation-tabs--tab--background-color:transparent}.digi-navigation-tabs__tablist.sc-digi-navigation-tabs{display:flex;align-items:flex-end;gap:var(--digi--padding--medium)}.digi-navigation-tabs__tab.sc-digi-navigation-tabs{--digi--heading-4--font-size:var(--digi--typography--heading-4--font-size--mobile);--digi--heading-4--line-height:var(--digi--typography--heading-4--line-height--mobile);font-family:var(--digi--global--typography--font-family--default);font-size:var(--digi--heading-4--font-size);line-height:var(--digi--heading-4--line-height);font-weight:var(--digi--typography--heading-4--font-weight--mobile);-webkit-margin-after:0;margin-block-end:0;min-width:calc(var(--digi--global--spacing--smaller-2) * 15);text-align:center;padding:var(--digi--global--spacing--smallest) var(--digi--global--spacing--largest-4);background:var(--digi--color--background--neutral-1);border:var(--digi--navigation-tabs--tab--border);border-radius:var(--digi--border-radius--primary) var(--digi--border-radius--primary) 0 0;color:var(--digi--color--background--inverted-1)}@media (min-width: 48rem){.digi-navigation-tabs__tab.sc-digi-navigation-tabs{--digi--heading-4--font-size:var(--digi--typography--heading-4--font-size--desktop);--digi--heading-4--line-height:var(--digi--typography--heading-4--line-height--desktop)}}@media (min-width: 62rem){.digi-navigation-tabs__tab.sc-digi-navigation-tabs{--digi--heading-4--font-size:var(--digi--typography--heading-4--font-size--desktop-large);--digi--heading-4--line-height:var(--digi--typography--heading-4--line-height--desktop-large)}}.digi-navigation-tabs__tab[aria-selected].sc-digi-navigation-tabs{background:var(--digi--color--background--inverted-1);color:var(--digi--color--text--inverted);-webkit-padding-before:var(--digi--global--spacing--smaller);padding-block-start:var(--digi--global--spacing--smaller)}.digi-navigation-tabs__tab.sc-digi-navigation-tabs:focus{outline:none}.digi-navigation-tabs__tab.sc-digi-navigation-tabs:not([aria-selected]):hover{background-color:var(--digi--global--color--profile--purple--opacity15)}.digi-navigation-tabs__tab.sc-digi-navigation-tabs:focus-visible{outline:var(--digi--focus-outline)}.digi-navigation-tabs__tab-text.sc-digi-navigation-tabs{position:relative;top:calc(var(--digi--global--spacing--smallest-5) * -1)}";

const NavigationTabs = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.afOnChange = createEvent(this, "afOnChange", 7);
    this.afOnClick = createEvent(this, "afOnClick", 7);
    this.afOnFocus = createEvent(this, "afOnFocus", 7);
    this.tabPanels = [];
    this.activeTab = 0;
    this.currentTabIndex = this.activeTab;
    this.afAriaLabel = '';
    this.afInitActiveTab = 0;
    this.afId = randomIdGenerator('digi-navigation-tabs');
  }
  /**
   * Sätter om aktiv flik.
   * @en Sets the active tab.
   */
  async afMSetActiveTab(tabIndex) {
    this.setActiveTab(tabIndex);
  }
  changeHandler(activeTabIndex) {
    this.afOnChange.emit(activeTabIndex);
  }
  setTabFocus(id) {
    id = this.tabId(this.tabPanels[id].afId);
    if (document.getElementById(`${id}`)) {
      document.getElementById(`${id}`).focus();
    }
  }
  clickHandler(e, i) {
    this.setActiveTab(i);
    this.afOnClick.emit(e);
  }
  focusHandler(e) {
    const isActiveTab = this.tabPanels.findIndex((i) => this.tabId(i.afId) === document.activeElement.id) === this.activeTab;
    this.currentTabIndex = isActiveTab ? this.activeTab : this.currentTabIndex;
    this.afOnFocus.emit(e);
  }
  leftHandler() {
    this.decrementCurrentTabIndex();
  }
  rightHandler() {
    this.incrementCurrentTabIndex();
  }
  homeHandler() {
    this.setTabFocus('0');
  }
  endHandler() {
    this.setTabFocus(this.tabPanels.length - 1);
  }
  tabId(prefix) {
    return `${prefix}-tab`;
  }
  decrementCurrentTabIndex() {
    this.currentTabIndex > 0
      ? (this.currentTabIndex = this.currentTabIndex - 1)
      : (this.currentTabIndex = this.tabPanels.length - 1);
  }
  incrementCurrentTabIndex() {
    this.currentTabIndex < this.tabPanels.length - 1
      ? (this.currentTabIndex = this.currentTabIndex + 1)
      : (this.currentTabIndex = 0);
  }
  componentDidLoad() {
    this.getTabs();
  }
  getTabs(e = null) {
    let tablist;
    tablist = this.hostElement.querySelectorAll(`#${this.afId}-observer digi-navigation-tab`);
    if (!tablist) {
      logger.warn(`navigation-tabs tablist is empty, have you missed anything?`, this.hostElement);
      return;
    }
    this.tabPanels = [...tablist];
    let activeTabIndex = this.afInitActiveTab ? this.afInitActiveTab : 0;
    // If tabs are added or removed
    if (e) {
      if (!e.detail.addedNodes || !e.detail.removedNodes) {
        return;
      }
      const added = e.detail.addedNodes.item(0), removed = e.detail.removedNodes.item(0);
      activeTabIndex = this.currentTabIndex;
      if (added) {
        // If added tab is same position or before current tab, jump to the right
        Object.values(e.target.children).indexOf(added) <= this.currentTabIndex &&
          (activeTabIndex += 1);
      }
      else if (removed) {
        // If removed tab is before current tab or is last, jump to the left
        if ((removed.dataset.position == e.target.children.length &&
          removed.dataset.position == this.currentTabIndex) ||
          removed.dataset.position < this.currentTabIndex) {
          activeTabIndex -= 1;
        }
      }
    }
    this.setActiveTab(activeTabIndex);
  }
  setActiveTab(newTabIndex) {
    this.activeTab = newTabIndex;
    this.currentTabIndex = this.activeTab;
    this.tabPanels.forEach((tab, i) => {
      tab.setAttribute('af-active', i === newTabIndex);
      tab.setAttribute('data-position', i);
    });
  }
  render() {
    return (h("div", { class: "digi-navigation-tabs" }, h("digi-util-keydown-handler", { onAfOnLeft: () => this.leftHandler(), onAfOnRight: () => this.rightHandler(), onAfOnHome: () => this.homeHandler(), onAfOnEnd: () => this.endHandler() }, h("div", { class: "digi-navigation-tabs__tablist", role: "tablist", "aria-label": this.afAriaLabel }, this.tabPanels.map((tab, i) => {
      return (h("button", { class: "digi-navigation-tabs__tab", role: "tab", type: "button", "aria-selected": this.activeTab === i ? 'true' : null, "aria-controls": tab.afId, tabindex: this.activeTab !== i ? '-1' : null, id: `${this.tabId(tab.afId)}`, onClick: (e) => this.clickHandler(e, i), onFocus: (e) => this.focusHandler(e) }, h("span", { class: "digi-navigation-tabs__tab-text" }, tab.afAriaLabel)));
    }))), h("digi-util-mutation-observer", { onAfOnChange: (e) => this.getTabs(e), id: `${this.afId}-observer` }, h("slot", null))));
  }
  get hostElement() { return getElement(this); }
  static get watchers() { return {
    "activeTab": ["changeHandler"],
    "currentTabIndex": ["setTabFocus"]
  }; }
};
NavigationTabs.style = navigationTabsCss;

export { NavigationTabs as digi_navigation_tabs };
