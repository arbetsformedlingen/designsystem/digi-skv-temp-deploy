import { r as registerInstance, h } from './index-7f342a75.js';
import { L as LayoutGridVerticalSpacing } from './layout-grid-vertical-spacing.enum-cbd27319.js';

const layoutGridCss = ".sc-digi-layout-grid-h{--digi--layout-grid--columns:4;width:100%}@media (min-width: 48rem){.sc-digi-layout-grid-h{--digi--layout-grid--columns:8}}@media (min-width: 62rem){.sc-digi-layout-grid-h{--digi--layout-grid--columns:12}}.sc-digi-layout-grid-h .digi-layout-grid.sc-digi-layout-grid{display:grid;grid-template-columns:repeat(var(--digi--layout-grid--columns), minmax(0, 1fr));grid-column-gap:var(--digi--responsive-grid-gutter);grid-row-gap:var(--digi--responsive-grid-gutter)}.sc-digi-layout-grid-h .digi-layout-grid--vertical-spacing-none.sc-digi-layout-grid{grid-row-gap:0}";

const LayoutGrid = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.afVerticalSpacing = LayoutGridVerticalSpacing.REGULAR;
  }
  get cssModifiers() {
    return {
      [`digi-layout-grid--vertical-spacing-${this.afVerticalSpacing}`]: !!this.afVerticalSpacing
    };
  }
  render() {
    return (h("digi-layout-container", null, h("div", { class: Object.assign({ 'digi-layout-grid': true }, this.cssModifiers) }, h("slot", null))));
  }
};
LayoutGrid.style = layoutGridCss;

export { LayoutGrid as digi_layout_grid };
