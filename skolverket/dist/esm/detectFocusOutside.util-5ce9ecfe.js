import { d as detectClosest } from './detectClosest.util-2d3999b7.js';

function detectFocusOutside(target, selector) {
  return !detectClosest(target, selector);
}

export { detectFocusOutside as d };
