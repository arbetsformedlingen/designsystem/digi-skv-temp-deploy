var CodeExampleLanguage;
(function (CodeExampleLanguage) {
  CodeExampleLanguage["JSON"] = "JSON";
  CodeExampleLanguage["CSS"] = "CSS";
  CodeExampleLanguage["SCSS"] = "SCSS";
  CodeExampleLanguage["TYPESCRIPT"] = "Typescript";
  CodeExampleLanguage["JAVASCRIPT"] = "Javascript";
  CodeExampleLanguage["BASH"] = "Bash";
  CodeExampleLanguage["HTML"] = "HTML";
  CodeExampleLanguage["GIT"] = "Git";
  CodeExampleLanguage["ANGULAR"] = "Angular";
  CodeExampleLanguage["REACT"] = "React";
})(CodeExampleLanguage || (CodeExampleLanguage = {}));

var CodeExampleVariation;
(function (CodeExampleVariation) {
  CodeExampleVariation["LIGHT"] = "light";
  CodeExampleVariation["DARK"] = "dark";
})(CodeExampleVariation || (CodeExampleVariation = {}));

export { CodeExampleLanguage as C, CodeExampleVariation as a };
