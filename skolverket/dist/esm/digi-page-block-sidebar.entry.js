import { r as registerInstance, h } from './index-7f342a75.js';

const pageBlockSidebarCss = ".sc-digi-page-block-sidebar-h{--digi--layout-page-block-sidebar--sidebar-columns:4}@media (min-width: 48rem){.sc-digi-page-block-sidebar-h{--digi--layout-page-block-sidebar--sidebar-columns:4}}@media (min-width: 62rem){.sc-digi-page-block-sidebar-h{--digi--layout-page-block-sidebar--sidebar-columns:5}}.digi-page-block-sidebar.sc-digi-page-block-sidebar{display:block;background:var(--digi--layout-page-block-sidebar--background, transparent);padding:var(--digi--responsive-grid-gutter) 0}.digi-page-block-sidebar--variation-start.sc-digi-page-block-sidebar,.digi-page-block-sidebar--variation-sub.sc-digi-page-block-sidebar{--digi--layout-page-block-sidebar--background:var(--digi--global--color--profile--apricot--opacity50)}.digi-page-block-sidebar--variation-section.sc-digi-page-block-sidebar{--digi--layout-page-block-sidebar--background:var(--digi--color--background--primary)}.digi-page-block-sidebar__sidebar.sc-digi-page-block-sidebar{grid-column:1/-1}@media (min-width: 48rem){.digi-page-block-sidebar__sidebar.sc-digi-page-block-sidebar{grid-column:1/var(--digi--layout-page-block-sidebar--sidebar-columns)}}.digi-page-block-sidebar__content.sc-digi-page-block-sidebar{grid-column:1/-1;grid-row:2}@media (min-width: 48rem){.digi-page-block-sidebar__content.sc-digi-page-block-sidebar{grid-row:1;grid-column:var(--digi--layout-page-block-sidebar--sidebar-columns)/calc(var(--digi--layout-grid--columns) + 1)}}";

const PageBlockSidebar = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.afVariation = undefined;
  }
  get cssModifiers() {
    return {
      [`digi-page-block-sidebar--variation-${this.afVariation}`]: !!this.afVariation
    };
  }
  render() {
    return (h("digi-layout-grid", { class: Object.assign({ 'digi-page-block-sidebar': true }, this.cssModifiers) }, h("div", { class: "digi-page-block-sidebar__sidebar" }, h("slot", { name: "sidebar" })), h("div", { class: "digi-page-block-sidebar__content" }, h("slot", null))));
  }
  static get assetsDirs() { return ["public"]; }
};
PageBlockSidebar.style = pageBlockSidebarCss;

export { PageBlockSidebar as digi_page_block_sidebar };
