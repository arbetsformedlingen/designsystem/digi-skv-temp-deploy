import { r as registerInstance, h, g as getElement } from './index-7f342a75.js';
import './button-size.enum-8999fdcb.js';
import { B as ButtonType } from './button-type.enum-95f504f6.js';
import { B as ButtonVariation } from './button-variation.enum-3cc46e30.js';
import './expandable-accordion-variation.enum-9166e001.js';
import './calendar-week-view-heading-level.enum-05020882.js';
import './form-radiobutton-variation.enum-60c58c7d.js';
import './code-block-variation.enum-42e2bb44.js';
import './code-example-variation.enum-fbd04208.js';
import './code-variation.enum-ae00e62c.js';
import './form-checkbox-variation.enum-ad992b3e.js';
import './form-file-upload-variation.enum-882cb26e.js';
import './form-input-search-variation.enum-04106f0e.js';
import './form-input-type.enum-58a900d7.js';
import './form-input-variation.enum-07b4b98a.js';
import './form-select-variation.enum-fbd25aa0.js';
import './form-textarea-variation.enum-9d7b381e.js';
import './form-validation-message-variation.enum-5784e9ac.js';
import './layout-block-variation.enum-dbd50b66.js';
import './layout-columns-variation.enum-26e6db94.js';
import './layout-container-variation.enum-d17100f2.js';
import './layout-media-object-alignment.enum-f3156fc7.js';
import './link-external-variation.enum-f6d7a12f.js';
import './link-internal-variation.enum-8c3b0dd3.js';
import './link-variation.enum-81c1b9cf.js';
import './loader-spinner-size.enum-4999231f.js';
import './media-figure-alignment.enum-79554408.js';
import './navigation-context-menu-item-type.enum-178d1050.js';
import './navigation-sidebar-variation.enum-443b2b03.js';
import './navigation-vertical-menu-variation.enum-37c685d3.js';
import './progress-step-variation.enum-1f263b15.js';
import './progress-steps-variation.enum-fb681025.js';
import './progressbar-variation.enum-de87bf7f.js';
import './tag-size.enum-2927e760.js';
import './typography-meta-variation.enum-1eb26125.js';
import './typography-time-variation.enum-79f1b0de.js';
import './typography-variation.enum-b073e180.js';
import './util-breakpoint-observer-breakpoints.enum-8088d0a3.js';
import { r as randomIdGenerator } from './randomIdGenerator.util-a9066813.js';

const formProcessStepsCss = ".digi-form-process-steps.sc-digi-form-process-steps{background:var(--digi--color--background--primary);padding:var(--digi--gutter--largest-3) var(--digi--gutter--largest);overflow:hidden;display:flex;flex-direction:column;opacity:0;transition:opacity 0.05s ease-in-out}.digi-form-process-steps--fallback-is-set.sc-digi-form-process-steps{opacity:1}.digi-form-process-steps__toggle.sc-digi-form-process-steps{display:none}.digi-form-process-steps--fallback-true.sc-digi-form-process-steps .digi-form-process-steps__toggle.sc-digi-form-process-steps{display:flex;align-items:center;gap:var(--digi--gutter--larger);justify-content:space-between;-webkit-padding-after:var(--digi--gutter--medium);padding-block-end:var(--digi--gutter--medium);-webkit-border-after:var(--digi--border-width--tertiary) var(--digi--border-style--primary) var(--digi--global--color--profile--purple--opacity15);border-block-end:var(--digi--border-width--tertiary) var(--digi--border-style--primary) var(--digi--global--color--profile--purple--opacity15)}.digi-form-process-steps--fallback-true.sc-digi-form-process-steps .digi-form-process-steps__toggle.sc-digi-form-process-steps:focus-visible{outline:var(--digi--focus-outline)}.digi-form-process-steps__toggle-heading.sc-digi-form-process-steps{display:grid;grid-template-columns:auto 1fr;grid-auto-flow:columns;gap:var(--digi--gutter--smallest-4);align-items:center;text-align:start}.digi-form-process-steps__toggle-heading.sc-digi-form-process-steps::before{content:attr(data-current-step);font-family:var(--digi--global--typography--font-family--default);font-size:var(--digi--global--typography--font-size--small);width:var(--digi--global--spacing--larger);height:var(--digi--global--spacing--larger);border-radius:50%;display:flex;align-items:center;justify-content:center;background:var(--digi--color--background--inverted-1);border:1px solid var(--digi--color--border--primary);color:var(--digi--color--text--inverted)}.digi-form-process-steps__toggle-text.sc-digi-form-process-steps span.sc-digi-form-process-steps{font-family:var(--digi--global--typography--font-family--default);font-size:var(--digi--typography--font-size--desktop);font-weight:var(--digi--global--typography--font-weight--semibold);line-height:1.5;text-transform:uppercase;letter-spacing:0.02em;color:var(--digi--color--text--primary)}.digi-form-process-steps__toggle-text.sc-digi-form-process-steps p.sc-digi-form-process-steps{font-family:var(--digi--global--typography--font-family--default);text-decoration:none;color:var(--digi--color--text--secondary);font-size:var(--digi--global--typography--font-size--large);letter-spacing:calc(var(--digi--global--typography--font-size--large) / 100 * -1);font-weight:var(--digi--global--typography--font-weight--semibold);font-size:var(--digi--global--typography--font-size--small)}.digi-form-process-steps__toggle-text.sc-digi-form-process-steps p.sc-digi-form-process-steps:hover{color:var(--digi--global--color--profile--purple--dark);text-decoration:underline}.digi-form-process-steps__toggle-text.sc-digi-form-process-steps p.sc-digi-form-process-steps:focus{outline:none;color:var(--digi--color--text--secondary)}.digi-form-process-steps__toggle-text.sc-digi-form-process-steps p.sc-digi-form-process-steps:focus-visible{outline:var(--digi--border-width--secondary) solid var(--digi--color--border--focus)}.digi-form-process-steps__toggle-text.sc-digi-form-process-steps p.sc-digi-form-process-steps:visited{color:var(--digi--color--text--secondary)}.digi-form-process-steps__toggle-text.sc-digi-form-process-steps p.sc-digi-form-process-steps:hover{text-decoration:none}.digi-form-process-steps__toggle-label.sc-digi-form-process-steps{font-family:var(--digi--global--typography--font-family--default);font-size:var(--digi--typography--accordion--font-size--mobile);font-weight:var(--digi--typography--accordion--font-weight--desktop);display:inline-flex;align-items:center;gap:var(--digi--gutter--icon);font-size:var(--digi--global--typography--font-size--interaction-medium);color:var(--digi--color--text--secondary)}.digi-form-process-steps__toggle-label.sc-digi-form-process-steps digi-icon.sc-digi-form-process-steps{--digi--icon--color:currentColor}.digi-form-process-steps__items.sc-digi-form-process-steps{display:inline-flex;flex-direction:row;gap:var(--digi--gutter--largest);counter-reset:steps;margin:0;padding:0;list-style:none}.digi-form-process-steps--fallback-true.sc-digi-form-process-steps .digi-form-process-steps__items.sc-digi-form-process-steps{display:flex;flex-direction:column;gap:0}.digi-form-process-steps--fallback-true.digi-form-process-steps--expanded-false.sc-digi-form-process-steps .digi-form-process-steps__content.sc-digi-form-process-steps{overflow-y:hidden;height:0;visibility:hidden}.digi-form-process-steps--fallback-true.digi-form-process-steps--expanded-true.sc-digi-form-process-steps .digi-form-process-steps__content.sc-digi-form-process-steps{display:flex;gap:var(--digi--gutter--medium);flex-direction:column}.digi-form-process-steps__toggle-inside.sc-digi-form-process-steps{--digi--button--color--background--function--default:var(--digi--color--background--secondary);--digi--button--color--background--function--hover:var(--digi--color--background--tertiary)}.sc-digi-form-process-steps-s>li{margin:0;padding:0;list-style:none}";

const FormProcessSteps = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.isFallback = false;
    this.isExpanded = false;
    this.fallbackIsSet = false;
    this.listWidth = undefined;
    this.containerWidth = undefined;
    this.steps = undefined;
    this.afCurrentStep = undefined;
    this.afId = randomIdGenerator('digi-form-process-steps');
  }
  handleWidthChange() {
    this.isFallback = this.listWidth > this.containerWidth;
    if (!this.fallbackIsSet) {
      this.fallbackIsSet = true;
    }
  }
  componentWillLoad() {
    this.setTypeOnChildren();
  }
  componentDidLoad() {
    this.measureItemsList();
  }
  componentWillUpdate() {
    this.setTypeOnChildren();
  }
  handleFocusWithin() {
    this.isExpanded = true;
  }
  measureItemsList() {
    const itemsList = this.hostElement.querySelector('ol');
    this.listWidth = itemsList.scrollWidth;
  }
  setTypeOnChildren() {
    const steps = this.hostElement.querySelectorAll('digi-form-process-step');
    this.steps = steps;
    steps.forEach((step, i) => {
      if (i + 1 === this.afCurrentStep) {
        step.afType = 'current';
      }
      else if (i + 1 < this.afCurrentStep) {
        step.afType = 'completed';
      }
      else {
        step.afType = 'upcoming';
      }
    });
  }
  setContextOnChildren() {
    const steps = this.hostElement.querySelectorAll('digi-form-process-step');
    steps.forEach((step) => (step.afContext = this.isFallback ? 'fallback' : 'regular'));
  }
  resizeHandler() {
    this.containerWidth = this._contentElement.getBoundingClientRect().width;
  }
  clickToggleHandler(e, resetFocus = false) {
    e.preventDefault();
    this.isExpanded = !this.isExpanded;
    if (resetFocus) {
      this._button.focus();
    }
  }
  get cssModifiers() {
    return {
      [`digi-form-process-steps--expanded-${this.isExpanded}`]: true,
      [`digi-form-process-steps--fallback-${this.isFallback}`]: true,
      [`digi-form-process-steps--fallback-is-set`]: this.fallbackIsSet
    };
  }
  render() {
    var _a;
    return (h("digi-util-resize-observer", { onAfOnChange: () => this.resizeHandler() }, h("div", { class: Object.assign({ 'digi-form-process-steps': true }, this.cssModifiers) }, this.isFallback && (h("button", { class: "digi-form-process-steps__toggle", type: "button", "aria-pressed": this.isExpanded ? 'true' : 'false', "aria-expanded": this.isExpanded ? 'true' : 'false', "aria-controls": `${this.afId}-content`, onClick: (e) => this.clickToggleHandler(e, true), ref: (el) => (this._button = el) }, h("div", { class: "digi-form-process-steps__toggle-heading", "data-current-step": this.afCurrentStep }, h("div", { class: "digi-form-process-steps__toggle-text" }, h("span", null, "Steg ", this.afCurrentStep, " av ", this.steps.length), h("p", null, (_a = this.steps[this.afCurrentStep - 1]) === null || _a === void 0 ? void 0 : _a.textContent))), h("span", { class: "digi-form-process-steps__toggle-label" }, this.isExpanded ? 'Dölj steg' : 'Visa alla steg', h("digi-icon", { afName: this.isExpanded ? 'chevron-up' : 'chevron-down', "aria-hidden": true, slot: "icon-secondary" })))), h("div", { class: "digi-form-process-steps__content", ref: (el) => (this._contentElement = el) }, h("ol", { class: "digi-form-process-steps__items", id: `${this.afId}-items`, onFocusin: () => this.handleFocusWithin() }, h("slot", null)), this.isFallback && (h("digi-button", { class: "digi-form-process-steps__toggle-inside", afVariation: ButtonVariation.FUNCTION, afType: ButtonType.BUTTON, onAfOnClick: (e) => this.clickToggleHandler(e.detail, true), afFullWidth: true, afAriaPressed: this.isExpanded, afAriaExpanded: this.isExpanded, afAriaControls: `${this.afId}-content` }, this.isExpanded ? 'Dölj' : 'Visa alla steg', h("digi-icon", { afName: this.isExpanded ? 'chevron-up' : 'chevron-down', "aria-hidden": true, slot: "icon-secondary" })))))));
  }
  get hostElement() { return getElement(this); }
  static get watchers() { return {
    "listWidth": ["handleWidthChange"],
    "containerWidth": ["handleWidthChange"],
    "afCurrentStep": ["setTypeOnChildren"],
    "isFallback": ["setContextOnChildren"]
  }; }
};
FormProcessSteps.style = formProcessStepsCss;

export { FormProcessSteps as digi_form_process_steps };
