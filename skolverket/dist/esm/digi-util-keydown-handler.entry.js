import { r as registerInstance, c as createEvent, h } from './index-7f342a75.js';
import { K as KEY_CODE, k as keyboardHandler } from './keyboardHandler.util-3b8f0675.js';

const UtilKeydownHandler = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.afOnEsc = createEvent(this, "afOnEsc", 7);
    this.afOnEnter = createEvent(this, "afOnEnter", 7);
    this.afOnTab = createEvent(this, "afOnTab", 7);
    this.afOnSpace = createEvent(this, "afOnSpace", 7);
    this.afOnShiftTab = createEvent(this, "afOnShiftTab", 7);
    this.afOnUp = createEvent(this, "afOnUp", 7);
    this.afOnDown = createEvent(this, "afOnDown", 7);
    this.afOnLeft = createEvent(this, "afOnLeft", 7);
    this.afOnRight = createEvent(this, "afOnRight", 7);
    this.afOnHome = createEvent(this, "afOnHome", 7);
    this.afOnEnd = createEvent(this, "afOnEnd", 7);
    this.afOnKeyDown = createEvent(this, "afOnKeyDown", 7);
  }
  keydownHandler(e) {
    const key = keyboardHandler(e);
    switch (key) {
      case KEY_CODE.SHIFT_TAB:
        this.afOnShiftTab.emit(e);
      case KEY_CODE.DOWN_ARROW:
        this.afOnDown.emit(e);
        this.afOnKeyDown.emit(e);
        break;
      case KEY_CODE.UP_ARROW:
        this.afOnUp.emit(e);
        this.afOnKeyDown.emit(e);
        break;
      case KEY_CODE.LEFT_ARROW:
        this.afOnLeft.emit(e);
        this.afOnKeyDown.emit(e);
        break;
      case KEY_CODE.RIGHT_ARROW:
        this.afOnRight.emit(e);
        this.afOnKeyDown.emit(e);
        break;
      case KEY_CODE.ENTER:
        this.afOnEnter.emit(e);
        this.afOnKeyDown.emit(e);
        break;
      case KEY_CODE.ESCAPE:
        this.afOnEsc.emit(e);
        this.afOnKeyDown.emit(e);
        break;
      case KEY_CODE.TAB:
        this.afOnTab.emit(e);
        this.afOnKeyDown.emit(e);
        break;
      case KEY_CODE.SPACE:
        this.afOnSpace.emit(e);
        this.afOnKeyDown.emit(e);
        break;
      case KEY_CODE.HOME:
        this.afOnHome.emit(e);
        this.afOnKeyDown.emit(e);
        break;
      case KEY_CODE.END:
        this.afOnEnd.emit(e);
        this.afOnKeyDown.emit(e);
        break;
      case KEY_CODE.ANY:
        this.afOnKeyDown.emit(e);
        break;
      default:
        this.afOnKeyDown.emit(e);
        return;
    }
  }
  render() {
    return h("slot", null);
  }
};

export { UtilKeydownHandler as digi_util_keydown_handler };
