import { r as registerInstance, h, g as getElement } from './index-7f342a75.js';
import { r as randomIdGenerator } from './randomIdGenerator.util-a9066813.js';
import { l as logger } from './logger.util-b54855d5.js';
import { F as FormValidationMessageVariation } from './form-validation-message-variation.enum-5784e9ac.js';

const formLabelCss = ".sc-digi-form-label-h .digi-form-label.sc-digi-form-label{font-family:var(--digi--global--typography--font-family--default);font-size:var(--digi--typography--label--font-size--desktop);color:var(--digi--color--text--primary);display:flex;flex-direction:column}.sc-digi-form-label-h .digi-form-label__label.sc-digi-form-label{line-height:var(--digi--typography--label--line-height--desktop);font-weight:var(--digi--typography--label--font-weight--desktop);cursor:pointer}.sc-digi-form-label-h .digi-form-label__label-group.sc-digi-form-label{display:inline-flex;align-items:center;gap:var(--digi--gutter--icon)}.sc-digi-form-label-h .digi-form-label__description.sc-digi-form-label{max-width:var(--digi--paragraph-width--medium);line-height:var(--digi--typography--label-description--line-height--desktop);font-size:var(--digi--typography--label-description--font-size--desktop)}";

const FormLabel = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.labelText = undefined;
    this.hasActionSlot = undefined;
    this.afLabel = undefined;
    this.afId = randomIdGenerator('digi-form-label');
    this.afFor = undefined;
    this.afDescription = undefined;
    this.afRequired = undefined;
    this.afAnnounceIfOptional = false;
    this.afRequiredText = 'obligatoriskt';
    this.afAnnounceIfOptionalText = 'frivilligt';
  }
  componentWillLoad() {
    this.setLabelText();
    this.validateLabel();
    this.validateFor();
    this.handleSlotVisibility();
  }
  validateLabel() { }
  validateFor() {
    if (this.afFor)
      return;
    logger.warn(`digi-form-label must have a for attribute. Please add a for attribute using af-for`, this.hostElement);
  }
  setLabelText() {
    this.labelText = `${this.afLabel} ${this.requiredText}`;
    if (!this.afLabel) {
      logger.warn(`digi-form-label must have a label. Please add a label using af-label`, this.hostElement);
    }
  }
  handleSlotVisibility() {
    this.hasActionSlot = !!this.hostElement.querySelector('[slot="actions"]');
  }
  get requiredText() {
    return this.afRequired && !this.afAnnounceIfOptional
      ? ` (${this.afRequiredText})`
      : !this.afRequired && this.afAnnounceIfOptional
        ? ` (${this.afAnnounceIfOptionalText})`
        : '';
  }
  render() {
    return (h("div", { class: "digi-form-label" }, h("div", { class: "digi-form-label__label-group" }, this.afFor && this.afLabel && (h("label", { class: "digi-form-label__label", htmlFor: this.afFor, id: this.afId }, this.labelText)), this.hasActionSlot && (h("div", { class: "digi-form-label__actions" }, h("slot", { name: "actions" })))), this.afDescription && (h("p", { class: "digi-form-label__description" }, this.afDescription))));
  }
  get hostElement() { return getElement(this); }
  static get watchers() { return {
    "afLabel": ["validateLabel", "setLabelText"],
    "afFor": ["validateFor"],
    "afRequired": ["setLabelText"],
    "afAnnounceIfOptional": ["setLabelText"]
  }; }
};
FormLabel.style = formLabelCss;

const formValidationMessageCss = ".sc-digi-form-validation-message-h{--digi--form-validation-message--icon-color--success:var(--digi--color--icons--success);--digi--form-validation-message--icon-color--warning:var(--digi--color--icons--warning);--digi--form-validation-message--icon-color--error:var(--digi--color--icons--danger)}.sc-digi-form-validation-message-h .digi-form-validation-message.sc-digi-form-validation-message{display:flex;flex-wrap:wrap;align-items:center}.sc-digi-form-validation-message-h .digi-form-validation-message--success.sc-digi-form-validation-message{--ICON-COLOR:var(--digi--form-validation-message--icon-color--success)}.sc-digi-form-validation-message-h .digi-form-validation-message--warning.sc-digi-form-validation-message{--ICON-COLOR:var(--digi--form-validation-message--icon-color--warning)}.sc-digi-form-validation-message-h .digi-form-validation-message--error.sc-digi-form-validation-message{--ICON-COLOR:var(--digi--form-validation-message--icon-color--error)}.sc-digi-form-validation-message-h .digi-form-validation-message__icon.sc-digi-form-validation-message{color:var(--ICON-COLOR);-webkit-margin-end:var(--digi--gutter--icon);margin-inline-end:var(--digi--gutter--icon)}.sc-digi-form-validation-message-h .digi-form-validation-message__icon.sc-digi-form-validation-message>*.sc-digi-form-validation-message{--digi--icon--width:1.25em;--digi--icon--height:1.25em;--digi--icon--color:var(--ICON-COLOR);display:flex}.sc-digi-form-validation-message-h .digi-form-validation-message__text.sc-digi-form-validation-message{flex:1;font-family:var(--digi--global--typography--font-family--default);font-size:var(--digi--typography--tag--font-size--desktop);color:var(--digi--color--text--primary)}";

const FormValidationMessage = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.afVariation = FormValidationMessageVariation.SUCCESS;
  }
  get cssModifiers() {
    return {
      'digi-form-validation-message--success': this.afVariation === FormValidationMessageVariation.SUCCESS,
      'digi-form-validation-message--error': this.afVariation === FormValidationMessageVariation.ERROR,
      'digi-form-validation-message--warning': this.afVariation === FormValidationMessageVariation.WARNING
    };
  }
  get icon() {
    switch (this.afVariation) {
      case FormValidationMessageVariation.SUCCESS:
        return 'check-circle';
      case FormValidationMessageVariation.ERROR:
        return 'exclamation-circle-filled';
      case FormValidationMessageVariation.WARNING:
        return 'exclamation-triangle-warning';
    }
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-form-validation-message': true }, this.cssModifiers) }, h("div", { class: "digi-form-validation-message__icon", "aria-hidden": "true" }, h("digi-icon", { afName: this.icon })), h("span", { class: "digi-form-validation-message__text" }, h("slot", null))));
  }
  get hostElement() { return getElement(this); }
};
FormValidationMessage.style = formValidationMessageCss;

export { FormLabel as digi_form_label, FormValidationMessage as digi_form_validation_message };
