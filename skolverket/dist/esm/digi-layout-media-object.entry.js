import { r as registerInstance, h } from './index-7f342a75.js';
import { L as LayoutMediaObjectAlignment } from './layout-media-object-alignment.enum-f3156fc7.js';

const layoutMediaObjectCss = ".sc-digi-layout-media-object-h{--digi--layout-media-object--gutter:var(--digi--gutter--largest);--digi--layout-media-object--alignment:flex-start;--digi--layout-media-object--flex-wrap:wrap}.sc-digi-layout-media-object-h .digi-layout-media-object.sc-digi-layout-media-object{display:flex;align-items:var(--digi--layout-media-object--alignment);gap:var(--digi--layout-media-object--gutter);flex-wrap:var(--digi--layout-media-object--flex-wrap)}.sc-digi-layout-media-object-h .digi-layout-media-object--alignment-center.sc-digi-layout-media-object{--digi--layout-media-object--alignment:center}.sc-digi-layout-media-object-h .digi-layout-media-object--alignment-end.sc-digi-layout-media-object{--digi--layout-media-object--alignment:flex-end}.sc-digi-layout-media-object-h .digi-layout-media-object--alignment-stretch.sc-digi-layout-media-object{--digi--layout-media-object--alignment:stretch}.digi-layout-media-object__last.sc-digi-layout-media-object{flex:1}";

const LayoutMediaObject = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.afAlignment = LayoutMediaObjectAlignment.START;
  }
  get cssModifiers() {
    return {
      [`digi-layout-media-object--alignment-${this.afAlignment}`]: !!this.afAlignment
    };
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-layout-media-object': true }, this.cssModifiers) }, h("div", { class: "digi-layout-media-object__first" }, h("slot", { name: "media" })), h("div", { class: "digi-layout-media-object__last" }, h("slot", null))));
  }
};
LayoutMediaObject.style = layoutMediaObjectCss;

export { LayoutMediaObject as digi_layout_media_object };
