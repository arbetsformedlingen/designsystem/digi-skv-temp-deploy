var FormSelectValidation;
(function (FormSelectValidation) {
  FormSelectValidation["SUCCESS"] = "success";
  FormSelectValidation["ERROR"] = "error";
  FormSelectValidation["WARNING"] = "warning";
  FormSelectValidation["NEUTRAL"] = "neutral";
})(FormSelectValidation || (FormSelectValidation = {}));

var FormSelectVariation;
(function (FormSelectVariation) {
  FormSelectVariation["SMALL"] = "small";
  FormSelectVariation["MEDIUM"] = "medium";
  FormSelectVariation["LARGE"] = "large";
})(FormSelectVariation || (FormSelectVariation = {}));

export { FormSelectValidation as F, FormSelectVariation as a };
