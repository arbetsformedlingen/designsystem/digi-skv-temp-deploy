import { r as registerInstance, c as createEvent, h } from './index-7f342a75.js';
import { r as randomIdGenerator } from './randomIdGenerator.util-a9066813.js';

const navigationSidebarButtonCss = ".sc-digi-navigation-sidebar-button-h{--digi--navigation-sidebar-button--display:inline-flex}.sc-digi-navigation-sidebar-button-h .digi-navigation-sidebar-button.sc-digi-navigation-sidebar-button{display:var(--digi--navigation-sidebar-button--display)}";

const NavigationSidebarButton = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.afOnToggle = createEvent(this, "afOnToggle", 7);
    this.isActive = false;
    this.afText = undefined;
    this.afAriaLabel = undefined;
    this.afId = randomIdGenerator('digi-navigation-sidebar-button');
  }
  toggleHandler() {
    this.isActive = !this.isActive;
    this.afOnToggle.emit(this.isActive);
  }
  render() {
    return (h("digi-button", { id: this.afId, class: "digi-navigation-sidebar-button", "af-variation": "function", "af-aria-label": this.afAriaLabel, onClick: () => this.toggleHandler() }, this.afText, h("digi-icon", { slot: "icon", afName: `bars` })));
  }
};
NavigationSidebarButton.style = navigationSidebarButtonCss;

export { NavigationSidebarButton as digi_navigation_sidebar_button };
