import { r as registerInstance, h } from './index-7f342a75.js';
import { M as MediaFigureAlignment } from './media-figure-alignment.enum-79554408.js';

const mediaFigureCss = ".sc-digi-media-figure-h .digi-media-figure__figcaption.sc-digi-media-figure{font-family:var(--digi--global--typography--font-family--default);font-size:var(--digi--typography--body--font-size--mobile);color:var(--digi--color--text--neutral);margin-top:var(--digi--gutter--smaller)}.digi-media-figure--align-start.sc-digi-media-figure .digi-media-figure__figcaption.sc-digi-media-figure{text-align:start}.digi-media-figure--align-center.sc-digi-media-figure .digi-media-figure__figcaption.sc-digi-media-figure{text-align:center}.digi-media-figure--align-end.sc-digi-media-figure .digi-media-figure__figcaption.sc-digi-media-figure{text-align:end}";

const MediaFigure = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.afFigcaption = undefined;
    this.afAlignment = MediaFigureAlignment.START;
  }
  get cssModifiers() {
    return {
      'digi-media-figure--align-start': this.afAlignment === 'start',
      'digi-media-figure--align-center': this.afAlignment === 'center',
      'digi-media-figure--align-end': this.afAlignment === 'end',
    };
  }
  render() {
    return (h("figure", { class: Object.assign({ 'digi-media-figure': true }, this.cssModifiers) }, h("div", { class: "digi-media-figure__image" }, h("slot", null)), this.afFigcaption && (h("figcaption", { class: "digi-media-figure__figcaption" }, this.afFigcaption))));
  }
};
MediaFigure.style = mediaFigureCss;

export { MediaFigure as digi_media_figure };
