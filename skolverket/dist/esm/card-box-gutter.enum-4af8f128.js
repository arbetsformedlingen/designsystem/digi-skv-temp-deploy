var CardBoxGutter;
(function (CardBoxGutter) {
  CardBoxGutter["REGULAR"] = "regular";
  CardBoxGutter["NONE"] = "none";
})(CardBoxGutter || (CardBoxGutter = {}));

export { CardBoxGutter as C };
