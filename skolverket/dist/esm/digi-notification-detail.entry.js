import { r as registerInstance, h } from './index-7f342a75.js';
import { N as NotificationDetailVariation } from './notification-detail-variation.enum-06ceda96.js';
import './button-size.enum-8999fdcb.js';
import './button-type.enum-95f504f6.js';
import './button-variation.enum-3cc46e30.js';
import './expandable-accordion-variation.enum-9166e001.js';
import './calendar-week-view-heading-level.enum-05020882.js';
import './form-radiobutton-variation.enum-60c58c7d.js';
import './code-block-variation.enum-42e2bb44.js';
import './code-example-variation.enum-fbd04208.js';
import './code-variation.enum-ae00e62c.js';
import './form-checkbox-variation.enum-ad992b3e.js';
import './form-file-upload-variation.enum-882cb26e.js';
import './form-input-search-variation.enum-04106f0e.js';
import './form-input-type.enum-58a900d7.js';
import './form-input-variation.enum-07b4b98a.js';
import './form-select-variation.enum-fbd25aa0.js';
import './form-textarea-variation.enum-9d7b381e.js';
import './form-validation-message-variation.enum-5784e9ac.js';
import './layout-block-variation.enum-dbd50b66.js';
import './layout-columns-variation.enum-26e6db94.js';
import './layout-container-variation.enum-d17100f2.js';
import './layout-media-object-alignment.enum-f3156fc7.js';
import './link-external-variation.enum-f6d7a12f.js';
import './link-internal-variation.enum-8c3b0dd3.js';
import './link-variation.enum-81c1b9cf.js';
import './loader-spinner-size.enum-4999231f.js';
import './media-figure-alignment.enum-79554408.js';
import './navigation-context-menu-item-type.enum-178d1050.js';
import './navigation-sidebar-variation.enum-443b2b03.js';
import './navigation-vertical-menu-variation.enum-37c685d3.js';
import './progress-step-variation.enum-1f263b15.js';
import './progress-steps-variation.enum-fb681025.js';
import './progressbar-variation.enum-de87bf7f.js';
import './tag-size.enum-2927e760.js';
import './typography-meta-variation.enum-1eb26125.js';
import './typography-time-variation.enum-79f1b0de.js';
import { T as TypographyVariation } from './typography-variation.enum-b073e180.js';
import './util-breakpoint-observer-breakpoints.enum-8088d0a3.js';

const notificationDetailCss = ".sc-digi-notification-detail-h{--digi--notification-detail--border-color--info:var(--digi--color--border--informative);--digi--notification-detail--border-color--warning:var(--digi--color--border--warning);--digi--notification-detail--border-color--danger:var(--digi--color--border--danger)}.sc-digi-notification-detail-h .sc-digi-notification-detail-s>[slot^=heading]{--digi--heading-4--font-size:var(--digi--typography--heading-4--font-size--mobile);--digi--heading-4--line-height:var(--digi--typography--heading-4--line-height--mobile);font-family:var(--digi--global--typography--font-family--default);font-size:var(--digi--heading-4--font-size);line-height:var(--digi--heading-4--line-height);font-weight:var(--digi--typography--heading-4--font-weight--mobile);-webkit-margin-after:0;margin-block-end:0}@media (min-width: 48rem){.sc-digi-notification-detail-h .sc-digi-notification-detail-s>[slot^=heading]{--digi--heading-4--font-size:var(--digi--typography--heading-4--font-size--desktop);--digi--heading-4--line-height:var(--digi--typography--heading-4--line-height--desktop)}}@media (min-width: 62rem){.sc-digi-notification-detail-h .sc-digi-notification-detail-s>[slot^=heading]{--digi--heading-4--font-size:var(--digi--typography--heading-4--font-size--desktop-large);--digi--heading-4--line-height:var(--digi--typography--heading-4--line-height--desktop-large)}}.digi-notification-detail.sc-digi-notification-detail{-webkit-border-start:var(--digi--border-width--secondary) solid var(--digi--color--border--secondary);border-inline-start:var(--digi--border-width--secondary) solid var(--digi--color--border--secondary);border-inline-start-color:var(--BORDER-COLOR);background:var(--digi--color--background--notification-info)}.digi-notification-detail--variation-info.sc-digi-notification-detail{--BORDER-COLOR:var(--digi--notification-detail--border-color--info);--ICON-COLOR:var(--digi--notification-detail--border-color--info)}.digi-notification-detail--variation-danger.sc-digi-notification-detail{--BORDER-COLOR:var(--digi--notification-detail--border-color--danger);--ICON-COLOR:var(--digi--notification-detail--border-color--danger)}.digi-notification-detail--variation-warning.sc-digi-notification-detail{--BORDER-COLOR:var(--digi--notification-detail--border-color--warning);--ICON-COLOR:var(--digi--notification-detail--border-color--warning)}.digi-notification-detail__inner.sc-digi-notification-detail{display:grid;grid-template-columns:min-content 1fr;grid-template-areas:\"icon content\";gap:calc(var(--digi--gutter--icon) * 2);padding:var(--digi--gutter--medium) var(--digi--gutter--larger);-webkit-padding-end:calc(var(--digi--gutter--larger) * 2);padding-inline-end:calc(var(--digi--gutter--larger) * 2)}@media (max-width: 47.9375rem){.digi-notification-detail__inner.sc-digi-notification-detail{justify-content:space-between;align-items:center}}.digi-notification-detail__icon.sc-digi-notification-detail{grid-area:icon}.digi-notification-detail__icon.sc-digi-notification-detail digi-icon.sc-digi-notification-detail{--digi--icon--color:var(--ICON-COLOR);--digi-icon--fixed-width:32px;--digi-icon--fixed-height:32px}.digi-notification-detail__content.sc-digi-notification-detail{grid-area:content}.digi-notification-detail__text.sc-digi-notification-detail{--digi--typography--body--font-size--desktop:var(--digi--global--typography--font-size--smaller);--digi--typography--body--font-size--mobile:var(--digi--global--typography--font-size--smaller);--digi--typography--body--line-height--desktop:var(--digi--global--typography--line-height--smaller);--digi--typography--body--line-height--mobile:var(--digi--global--typography--line-height--smaller)}";

const NotificationDetail = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.afVariation = NotificationDetailVariation.INFO;
  }
  get cssModifiers() {
    return {
      [`digi-notification-detail--variation-${this.afVariation}`]: !!this.afVariation
    };
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-notification-detail': true }, this.cssModifiers) }, h("div", { class: "digi-notification-detail__inner" }, h("div", { class: "digi-notification-detail__icon" }, h("digi-icon", { afName: `notification-${this.afVariation}`, "aria-hidden": "true" })), h("div", { class: "digi-notification-detail__content" }, h("slot", { name: "heading" }), h("digi-typography", { class: "digi-notification-detail__text", "af-variation": TypographyVariation.SMALL }, h("slot", null))))));
  }
};
NotificationDetail.style = notificationDetailCss;

export { NotificationDetail as digi_notification_detail };
