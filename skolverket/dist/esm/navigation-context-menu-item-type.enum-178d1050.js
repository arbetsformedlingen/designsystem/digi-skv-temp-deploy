var NavigationContextMenuItemType;
(function (NavigationContextMenuItemType) {
  NavigationContextMenuItemType["LINK"] = "link";
  NavigationContextMenuItemType["BUTTON"] = "button";
})(NavigationContextMenuItemType || (NavigationContextMenuItemType = {}));

export { NavigationContextMenuItemType as N };
