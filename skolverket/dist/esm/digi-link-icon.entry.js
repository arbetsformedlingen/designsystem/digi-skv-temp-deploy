import { r as registerInstance, h } from './index-7f342a75.js';

const linkIconCss = ".sc-digi-link-icon-h .sc-digi-link-icon-s>a{font-family:var(--digi--global--typography--font-family--default);text-decoration:none;color:var(--digi--color--text--secondary);font-size:var(--digi--global--typography--font-size--large);letter-spacing:calc(var(--digi--global--typography--font-size--large) / 100 * -1);font-weight:var(--digi--global--typography--font-weight--semibold);font-weight:var(--digi--global--typography--font-weight--regular);display:inline-flex;letter-spacing:unset;flex-direction:row;align-items:center;font-size:var(--digi--global--typography--font-size--smaller);gap:0.5em}.sc-digi-link-icon-h .sc-digi-link-icon-s>a:hover{color:var(--digi--global--color--profile--purple--dark);text-decoration:underline}.sc-digi-link-icon-h .sc-digi-link-icon-s>a:focus{outline:none;color:var(--digi--color--text--secondary)}.sc-digi-link-icon-h .sc-digi-link-icon-s>a:focus-visible{outline:var(--digi--border-width--secondary) solid var(--digi--color--border--focus)}.sc-digi-link-icon-h .sc-digi-link-icon-s>a:visited{color:var(--digi--color--text--secondary)}@media (min-width: 62rem){.sc-digi-link-icon-h .sc-digi-link-icon-s>a{gap:0;font-size:0.75rem;flex-direction:column;justify-content:center;line-height:1}}";

const LinkIcon = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
  }
  render() {
    return (h("span", null, h("slot", null)));
  }
};
LinkIcon.style = linkIconCss;

export { LinkIcon as digi_link_icon };
