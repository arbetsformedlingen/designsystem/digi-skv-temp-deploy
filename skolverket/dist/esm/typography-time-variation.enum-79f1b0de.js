var TypographyTimeVariation;
(function (TypographyTimeVariation) {
  TypographyTimeVariation["PRIMARY"] = "primary";
  TypographyTimeVariation["PRETTY"] = "pretty";
  TypographyTimeVariation["DISTANCE"] = "distance";
})(TypographyTimeVariation || (TypographyTimeVariation = {}));

export { TypographyTimeVariation as T };
