import { r as registerInstance, c as createEvent, h } from './index-7f342a75.js';
import { r as randomIdGenerator } from './randomIdGenerator.util-a9066813.js';

const navigationTabCss = ".sc-digi-navigation-tab-h{--digi--navigation-tab--box-shadow--focus:solid 2px var(--digi--color--border--secondary)}.sc-digi-navigation-tab-h .digi-navigation-tab.sc-digi-navigation-tab:focus-visible{box-shadow:var(--digi--navigation-tab--box-shadow--focus);outline:none}";

const NavigationTab = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.afOnToggle = createEvent(this, "afOnToggle", 7);
    this.afAriaLabel = undefined;
    this.afId = randomIdGenerator('digi-navigation-tab');
    this.afActive = undefined;
  }
  toggleHandler(activeTab) {
    if (activeTab || null) {
      this.afOnToggle.emit(activeTab);
    }
  }
  render() {
    return (h("div", { class: "digi-navigation-tab", tabindex: "0", role: "tabpanel", id: this.afId, "aria-label": this.afAriaLabel, hidden: !this.afActive }, h("slot", null)));
  }
  static get watchers() { return {
    "afActive": ["toggleHandler"]
  }; }
};
NavigationTab.style = navigationTabCss;

export { NavigationTab as digi_navigation_tab };
