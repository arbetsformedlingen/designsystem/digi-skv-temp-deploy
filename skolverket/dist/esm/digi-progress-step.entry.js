import { r as registerInstance, h, g as getElement } from './index-7f342a75.js';
import { r as randomIdGenerator } from './randomIdGenerator.util-a9066813.js';
import { P as ProgressStepHeadingLevel, a as ProgressStepStatus, b as ProgressStepVariation } from './progress-step-variation.enum-1f263b15.js';

const progressStepCss = ".sc-digi-progress-step-h{--digi--progress-step--indicator--color--primary:var(--digi--color--icons--success);--digi--progress-step--indicator--color--secondary:var(--digi--color--icons--secondary);--digi--progress-step--heading--font-size:var(--digi--typography--heading-3--font-size--desktop);--digi--progress-step--heading--font-weight:var(--digi--typography--heading-3--font-weight--desktop)}.sc-digi-progress-step-h .digi-progress-step.sc-digi-progress-step{--INDICATOR--LINE--COLOR:var(--digi--color--border--primary);font-family:var(--digi--global--typography--font-family--default);display:flex;gap:var(--digi--padding--medium);flex-direction:row}.sc-digi-progress-step-h .digi-progress-step--primary.sc-digi-progress-step{--INDICATOR--CIRCLE--COLOR:var(--digi--progress-step--indicator--color--primary)}.sc-digi-progress-step-h .digi-progress-step--secondary.sc-digi-progress-step{--INDICATOR--CIRCLE--COLOR:var(--digi--progress-step--indicator--color--secondary)}.sc-digi-progress-step-h .digi-progress-step--done.sc-digi-progress-step{--INDICATOR--LINE--COLOR:var(--INDICATOR--CIRCLE--COLOR)}.sc-digi-progress-step-h .digi-progress-step--done.sc-digi-progress-step .digi-progress-step__indicator--circle.sc-digi-progress-step{background-color:var(--INDICATOR--CIRCLE--COLOR)}.sc-digi-progress-step-h .digi-progress-step--current.sc-digi-progress-step .digi-progress-step__indicator--circle.sc-digi-progress-step{background-color:var(--INDICATOR--CIRCLE--COLOR);box-shadow:inset 0 0 0 2px var(--INDICATOR--CIRCLE--COLOR), inset 0 0 0 4px white}.sc-digi-progress-step-h .digi-progress-step--upcoming.sc-digi-progress-step{--INDICATOR--CIRCLE--COLOR:var(--INDICATOR--LINE--COLOR)}.sc-digi-progress-step-h .digi-progress-step__indicator.sc-digi-progress-step{display:flex;align-items:center;flex-basis:26px;flex-shrink:0;flex-direction:column;transform:translateY(2px);margin-left:2px}.sc-digi-progress-step-h .digi-progress-step__indicator--circle.sc-digi-progress-step{height:26px;width:100%;border-radius:13px;display:block;flex-shrink:0;box-shadow:inset 0 0 0 2px var(--INDICATOR--CIRCLE--COLOR)}.sc-digi-progress-step-h .digi-progress-step__indicator--line.sc-digi-progress-step{width:2px;flex-grow:1;display:block;background-color:var(--INDICATOR--LINE--COLOR)}.sc-digi-progress-step-h .digi-progress-step__content.sc-digi-progress-step{max-width:var(--digi--paragraph-width--medium);padding-bottom:var(--digi--padding--largest)}.sc-digi-progress-step-h .digi-progress-step__content--heading.sc-digi-progress-step{font-weight:var(--digi--progress-step--heading--font-weight);font-size:var(--digi--progress-step--heading--font-size);margin:0;margin-bottom:var(--digi--padding--smaller)}.sc-digi-progress-step-h .digi-progress-step--last.sc-digi-progress-step .digi-progress-step__indicator--line.sc-digi-progress-step{display:none}";

const ProgressStep = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.afHeading = undefined;
    this.afHeadingLevel = ProgressStepHeadingLevel.H2;
    this.afStepStatus = ProgressStepStatus.UPCOMING;
    this.afVariation = ProgressStepVariation.PRIMARY;
    this.afIsLast = false;
    this.afId = randomIdGenerator('digi-progress-step');
  }
  render() {
    return (h("div", { "aria-current": this.afStepStatus == ProgressStepStatus.CURRENT && 'step', role: "listitem", class: {
        'digi-progress-step': true,
        [`digi-progress-step--${this.afStepStatus}`]: true,
        'digi-progress-step--last': this.afIsLast,
        'digi-progress-step--primary': this.afVariation == ProgressStepVariation.PRIMARY,
        'digi-progress-step--secondary': this.afVariation == ProgressStepVariation.SECONDARY
      } }, h("span", { class: "digi-progress-step__indicator" }, h("span", { class: "digi-progress-step__indicator--circle" }), h("span", { class: "digi-progress-step__indicator--line" })), h("div", { class: "digi-progress-step__content" }, h(this.afHeadingLevel, { class: "digi-progress-step__content--heading", id: `${this.afId}--heading` }, this.afHeading), h("digi-typography", null, h("slot", null)))));
  }
  get hostElement() { return getElement(this); }
};
ProgressStep.style = progressStepCss;

export { ProgressStep as digi_progress_step };
