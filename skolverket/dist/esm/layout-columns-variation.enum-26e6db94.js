var LayoutColumnsElement;
(function (LayoutColumnsElement) {
  LayoutColumnsElement["DIV"] = "div";
  LayoutColumnsElement["UL"] = "ul";
  LayoutColumnsElement["OL"] = "ol";
})(LayoutColumnsElement || (LayoutColumnsElement = {}));

var LayoutColumnsVariation;
(function (LayoutColumnsVariation) {
  LayoutColumnsVariation["THREE"] = "three";
  LayoutColumnsVariation["TWO"] = "two";
})(LayoutColumnsVariation || (LayoutColumnsVariation = {}));

export { LayoutColumnsElement as L, LayoutColumnsVariation as a };
