var ListLinkLayout;
(function (ListLinkLayout) {
  ListLinkLayout["BLOCK"] = "block";
  ListLinkLayout["INLINE"] = "inline";
})(ListLinkLayout || (ListLinkLayout = {}));

var ListLinkType;
(function (ListLinkType) {
  ListLinkType["UNORDERED"] = "ul";
  ListLinkType["ORDERED"] = "ol";
})(ListLinkType || (ListLinkType = {}));

var ListLinkVariation;
(function (ListLinkVariation) {
  ListLinkVariation["REGULAR"] = "regular";
  ListLinkVariation["COMPACT"] = "compact";
})(ListLinkVariation || (ListLinkVariation = {}));

export { ListLinkLayout as L, ListLinkType as a, ListLinkVariation as b };
