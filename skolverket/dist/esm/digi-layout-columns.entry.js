import { r as registerInstance, h } from './index-7f342a75.js';
import { L as LayoutColumnsElement, a as LayoutColumnsVariation } from './layout-columns-variation.enum-26e6db94.js';

const layoutColumnsCss = ".sc-digi-layout-columns-h{--digi--layout-columns--gap--column:var(--digi--gutter--largest);--digi--layout-columns--gap--row:var(--digi--gutter--largest);--digi--layout-columns--min-width:0}.sc-digi-layout-columns-h .digi-layout-columns.sc-digi-layout-columns{display:grid;grid-column-gap:var(--digi--layout-columns--gap--column);grid-row-gap:var(--digi--layout-columns--gap--row);grid-template-columns:repeat(auto-fit, minmax(var(--digi--layout-columns--min-width), 1fr))}.sc-digi-layout-columns-h .digi-layout-columns--two.sc-digi-layout-columns{--digi--layout-columns--min-width:calc(\n    50% - var(--digi--layout-columns--gap--column)\n  )}.sc-digi-layout-columns-h .digi-layout-columns--three.sc-digi-layout-columns{--digi--layout-columns--min-width:calc(\n    33% - var(--digi--layout-columns--gap--column)\n  )}";

const LayoutColumns = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.afElement = LayoutColumnsElement.DIV;
    this.afVariation = undefined;
  }
  get cssModifiers() {
    return {
      'digi-layout-columns--two': this.afVariation === LayoutColumnsVariation.TWO,
      'digi-layout-columns--three': this.afVariation === LayoutColumnsVariation.THREE,
    };
  }
  render() {
    return (h(this.afElement, { class: Object.assign({ 'digi-layout-columns': true }, this.cssModifiers) }, h("slot", null)));
  }
};
LayoutColumns.style = layoutColumnsCss;

export { LayoutColumns as digi_layout_columns };
