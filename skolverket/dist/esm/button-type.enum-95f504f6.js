var ButtonType;
(function (ButtonType) {
  ButtonType["BUTTON"] = "button";
  ButtonType["SUBMIT"] = "submit";
  ButtonType["RESET"] = "reset";
})(ButtonType || (ButtonType = {}));

export { ButtonType as B };
