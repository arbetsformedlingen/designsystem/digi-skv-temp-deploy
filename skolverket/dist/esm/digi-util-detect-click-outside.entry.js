import { r as registerInstance, c as createEvent, h, g as getElement } from './index-7f342a75.js';
import { r as randomIdGenerator } from './randomIdGenerator.util-a9066813.js';
import { d as detectClickOutside } from './detectClickOutside.util-ca9d6cb5.js';
import './detectClosest.util-2d3999b7.js';

const UtilDetectClickOutside = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.afOnClickOutside = createEvent(this, "afOnClickOutside", 7);
    this.afOnClickInside = createEvent(this, "afOnClickInside", 7);
    this.afDataIdentifier = randomIdGenerator('data-digi-util-detect-click-outside');
    this.$el.setAttribute(this.afDataIdentifier, '');
  }
  clickHandler(e) {
    const target = e.target;
    const selector = `[${this.afDataIdentifier}]`;
    if (detectClickOutside(target, selector)) {
      this.afOnClickOutside.emit(e);
    }
    else {
      this.afOnClickInside.emit(e);
    }
  }
  render() {
    return h("slot", null);
  }
  get $el() { return getElement(this); }
};

export { UtilDetectClickOutside as digi_util_detect_click_outside };
