var CardBoxWidth;
(function (CardBoxWidth) {
  CardBoxWidth["REGULAR"] = "regular";
  CardBoxWidth["FULL"] = "full";
})(CardBoxWidth || (CardBoxWidth = {}));

export { CardBoxWidth as C };
