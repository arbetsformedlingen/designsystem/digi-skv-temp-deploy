import { r as registerInstance, c as createEvent, h, g as getElement } from './index-7f342a75.js';
import { r as randomIdGenerator } from './randomIdGenerator.util-a9066813.js';

const UtilMutationObserver = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.afOnChange = createEvent(this, "afOnChange", 7);
    this._observer = null;
    this._isObserving = false;
    this.afId = randomIdGenerator('digi-util-mutation-observer');
    this.afOptions = {
      attributes: false,
      childList: true,
      subtree: false
    };
  }
  afOptionsWatcher(newValue) {
    this._afOptions =
      typeof newValue === 'string' ? JSON.parse(newValue) : newValue;
  }
  componentWillLoad() {
    this.afOptionsWatcher(this.afOptions);
    this.initObserver();
  }
  disconnectedCallback() {
    if (this._isObserving) {
      this.removeObserver();
    }
  }
  initObserver() {
    this._observer = new MutationObserver((mutationsList) => {
      for (const mutation of mutationsList) {
        if (mutation.type === 'childList' || mutation.type === 'attributes') {
          this.changeHandler(mutation);
        }
      }
    });
    this._observer.observe(this.$el, this._afOptions);
    this._isObserving = true;
  }
  removeObserver() {
    this._observer.disconnect();
    this._isObserving = false;
  }
  changeHandler(e) {
    this.afOnChange.emit(e);
  }
  render() {
    h("div", { id: this.afId, ref: (el) => (this.$el = el) }, h("slot", null));
  }
  get $el() { return getElement(this); }
  static get watchers() { return {
    "afOptions": ["afOptionsWatcher"]
  }; }
};

export { UtilMutationObserver as digi_util_mutation_observer };
