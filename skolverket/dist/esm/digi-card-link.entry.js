import { r as registerInstance, h, g as getElement } from './index-7f342a75.js';
import './button-size.enum-8999fdcb.js';
import './button-type.enum-95f504f6.js';
import './button-variation.enum-3cc46e30.js';
import './expandable-accordion-variation.enum-9166e001.js';
import './calendar-week-view-heading-level.enum-05020882.js';
import './form-radiobutton-variation.enum-60c58c7d.js';
import './code-block-variation.enum-42e2bb44.js';
import './code-example-variation.enum-fbd04208.js';
import './code-variation.enum-ae00e62c.js';
import './form-checkbox-variation.enum-ad992b3e.js';
import './form-file-upload-variation.enum-882cb26e.js';
import './form-input-search-variation.enum-04106f0e.js';
import './form-input-type.enum-58a900d7.js';
import './form-input-variation.enum-07b4b98a.js';
import './form-select-variation.enum-fbd25aa0.js';
import './form-textarea-variation.enum-9d7b381e.js';
import './form-validation-message-variation.enum-5784e9ac.js';
import './layout-block-variation.enum-dbd50b66.js';
import './layout-columns-variation.enum-26e6db94.js';
import './layout-container-variation.enum-d17100f2.js';
import './layout-media-object-alignment.enum-f3156fc7.js';
import './link-external-variation.enum-f6d7a12f.js';
import './link-internal-variation.enum-8c3b0dd3.js';
import './link-variation.enum-81c1b9cf.js';
import './loader-spinner-size.enum-4999231f.js';
import './media-figure-alignment.enum-79554408.js';
import './navigation-context-menu-item-type.enum-178d1050.js';
import './navigation-sidebar-variation.enum-443b2b03.js';
import './navigation-vertical-menu-variation.enum-37c685d3.js';
import './progress-step-variation.enum-1f263b15.js';
import './progress-steps-variation.enum-fb681025.js';
import './progressbar-variation.enum-de87bf7f.js';
import './tag-size.enum-2927e760.js';
import './typography-meta-variation.enum-1eb26125.js';
import './typography-time-variation.enum-79f1b0de.js';
import { T as TypographyVariation } from './typography-variation.enum-b073e180.js';
import './util-breakpoint-observer-breakpoints.enum-8088d0a3.js';

const cardLinkCss = ".sc-digi-card-link-h{--digi--card-link--padding:var(--digi--padding--largest);display:grid}.sc-digi-card-link-h .sc-digi-card-link-s>[slot^=link-heading]{display:flex;-webkit-margin-after:0.5rem;margin-block-end:0.5rem}.sc-digi-card-link-h .sc-digi-card-link-s>[slot^=link-heading] a{font-family:var(--digi--global--typography--font-family--default);text-decoration:none;color:var(--digi--color--text--secondary);font-size:var(--digi--global--typography--font-size--large);letter-spacing:calc(var(--digi--global--typography--font-size--large) / 100 * -1);font-weight:var(--digi--global--typography--font-weight--semibold)}.sc-digi-card-link-h .sc-digi-card-link-s>[slot^=link-heading] a:hover{color:var(--digi--global--color--profile--purple--dark);text-decoration:underline}.sc-digi-card-link-h .sc-digi-card-link-s>[slot^=link-heading] a:focus{outline:none;color:var(--digi--color--text--secondary)}.sc-digi-card-link-h .sc-digi-card-link-s>[slot^=link-heading] a:focus-visible{outline:var(--digi--border-width--secondary) solid var(--digi--color--border--focus)}.sc-digi-card-link-h .sc-digi-card-link-s>[slot^=link-heading] a:visited{color:var(--digi--color--text--secondary)}@media (min-width: 48rem){.sc-digi-card-link-h .sc-digi-card-link-s>[slot^=link-heading] a{font-size:var(--digi--global--typography--font-size--larger);letter-spacing:calc(var(--digi--global--typography--font-size--larger) / 100 * -1);line-height:1.44}}.sc-digi-card-link-h .sc-digi-card-link-s>[slot^=link-heading] a:hover{color:var(--digi--global--color--profile--purple--dark);text-decoration:underline}.sc-digi-card-link-h .sc-digi-card-link-s>[slot^=link-heading] a::after{background-color:currentColor;padding:0 0.5em;-webkit-mask-image:url(\"data:image/svg+xml;charset=utf-8, %3Csvg%0A%09width%3D%2224%22%0A%09height%3D%2224%22%0A%09viewBox%3D%220%200%2024%2024%22%0A%09xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%0A%3E%0A%09%3Cpath%0A%09%09d%3D%22M288.917%2C415.517%2C287.5%2C414.1l3.96-3.96-3.96-3.96%2C1.414-1.414%2C5.373%2C5.374Z%22%0A%09%09transform%3D%22translate(-278.17%20-398.103)%22%0A%09%2F%3E%0A%3C%2Fsvg%3E\");mask-image:url(\"data:image/svg+xml;charset=utf-8, %3Csvg%0A%09width%3D%2224%22%0A%09height%3D%2224%22%0A%09viewBox%3D%220%200%2024%2024%22%0A%09xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%0A%3E%0A%09%3Cpath%0A%09%09d%3D%22M288.917%2C415.517%2C287.5%2C414.1l3.96-3.96-3.96-3.96%2C1.414-1.414%2C5.373%2C5.374Z%22%0A%09%09transform%3D%22translate(-278.17%20-398.103)%22%0A%09%2F%3E%0A%3C%2Fsvg%3E\");-webkit-mask-repeat:no-repeat;mask-repeat:no-repeat;-webkit-mask-position:center;mask-position:center;-webkit-clip-path:padding-box inset(0.25em 0);clip-path:padding-box inset(0.25em 0);content:\"\";-webkit-mask-size:cover;mask-size:cover}.sc-digi-card-link-h .sc-digi-card-link-s>[slot^=link-heading] a::before{content:\"\";position:absolute;top:0;left:0;width:100%;height:100%;border-radius:var(--digi--border-radius--primary)}.sc-digi-card-link-h .sc-digi-card-link-s>[slot^=link-heading] a:focus-visible{outline:none}.sc-digi-card-link-h .sc-digi-card-link-s>[slot^=link-heading] a:focus-visible::before{content:\"\";outline:var(--digi--focus-outline)}.sc-digi-card-link-h .sc-digi-card-link-s>[slot^=link-heading] a svg{width:1.3em;height:1.3em}.sc-digi-card-link-h .sc-digi-card-link-s>img[slot^=image]{display:block;width:100%;height:auto}.sc-digi-card-link-h .digi-card-link.sc-digi-card-link{display:grid;grid-template-rows:auto 1fr;position:relative;border-radius:var(--digi--border-radius--primary);background:var(--digi--color--background--primary)}.sc-digi-card-link-h .digi-card-link__image.sc-digi-card-link{border-radius:var(--digi--border-radius--primary) var(--digi--border-radius--primary) 0 0;overflow:hidden;aspect-ratio:16/9;background-color:var(--digi--color--background--tertiary)}.sc-digi-card-link-h .digi-card-link__text.sc-digi-card-link{position:relative}.digi-card-link__content.sc-digi-card-link{display:flex;padding:var(--digi--card-link--padding);border:var(--digi--border-width--tertiary) var(--digi--border-style--primary) var(--digi--global--color--profile--purple--opacity15);border-radius:0 0 var(--digi--border-radius--primary) var(--digi--border-radius--primary)}.digi-card-link--image-false.sc-digi-card-link .digi-card-link__content.sc-digi-card-link{border-radius:var(--digi--border-radius--primary);grid-row:1/3}.digi-card-link--image-true.sc-digi-card-link .digi-card-link__content.sc-digi-card-link{-webkit-border-before:none;border-block-start:none}";

const CardLink = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.hasImage = undefined;
  }
  componentWillLoad() {
    this.setHasImage();
  }
  componentWillUpdate() {
    this.setHasImage();
  }
  setHasImage() {
    this.hasImage = !!this.hostElement.querySelector('[slot="image"]');
  }
  get cssModifiers() {
    return {
      [`digi-card-link--image-${this.hasImage}`]: true
    };
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-card-link': true }, this.cssModifiers) }, this.hasImage && (h("div", { class: "digi-card-link__image" }, h("slot", { name: "image" }))), h("digi-typography", { afVariation: TypographyVariation.LARGE, class: "digi-card-link__content" }, h("div", { class: "digi-card-link__heading" }, h("slot", { name: "link-heading" })), h("div", { class: "digi-card-link__text" }, h("slot", null)))));
  }
  get hostElement() { return getElement(this); }
};
CardLink.style = cardLinkCss;

export { CardLink as digi_card_link };
