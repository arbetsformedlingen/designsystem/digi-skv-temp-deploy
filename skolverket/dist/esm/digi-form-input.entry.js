import { r as registerInstance, c as createEvent, h, g as getElement } from './index-7f342a75.js';
import { r as randomIdGenerator } from './randomIdGenerator.util-a9066813.js';
import { a as FormInputType, F as FormInputButtonVariation } from './form-input-type.enum-58a900d7.js';
import { a as FormInputVariation, F as FormInputValidation } from './form-input-variation.enum-07b4b98a.js';

const formInputCss = ".sc-digi-form-input-h{--digi--form-input--height--small:var(--digi--input-height--small);--digi--form-input--height--medium:var(--digi--input-height--medium);--digi--form-input--height--large:var(--digi--input-height--large);--digi--form-input--border-radius:var(--digi--border-radius--input);--digi--form-input--padding:0 var(--digi--gutter--medium);--digi--form-input--background--empty:var(--digi--color--background--input-empty);--digi--form-input--background--neutral:var(--digi--color--background--input);--digi--form-input--background--success:var(--digi--color--background--success-2);--digi--form-input--background--warning:var(--digi--color--background--warning-2);--digi--form-input--background--error:var(--digi--color--background--danger-2);--digi--form-input--border--neutral:var(--digi--border-width--input-regular) solid;--digi--form-input--border--error:var(--digi--border-width--input-validation) solid;--digi--form-input--border--success:var(--digi--border-width--input-validation) solid;--digi--form-input--border--warning:var(--digi--border-width--input-validation) solid;--digi--form-input--border-color--neutral:var(--digi--color--border--neutral-3);--digi--form-input--border-color--success:var(--digi--color--border--success);--digi--form-input--border-color--warning:var(--digi--color--border--neutral-3);--digi--form-input--border-color--error:var(--digi--color--border--danger)}.sc-digi-form-input-h .digi-form-input.sc-digi-form-input{display:flex;flex-direction:column;gap:0.4em}.sc-digi-form-input-h .digi-form-input--small.sc-digi-form-input{--HEIGHT:var(--digi--form-input--height--small)}.sc-digi-form-input-h .digi-form-input--medium.sc-digi-form-input{--HEIGHT:var(--digi--form-input--height--medium)}.sc-digi-form-input-h .digi-form-input--large.sc-digi-form-input{--HEIGHT:var(--digi--form-input--height--large)}.sc-digi-form-input-h .digi-form-input--neutral.sc-digi-form-input{--BORDER:var(--digi--form-input--border--neutral);--BORDER-COLOR:var(--digi--form-input--border-color--neutral);--BACKGROUND:var(--digi--form-input--background--neutral)}.sc-digi-form-input-h .digi-form-input--empty.sc-digi-form-input:not(:focus-within){--BACKGROUND:var(--digi--form-input--background--empty)}.sc-digi-form-input-h .digi-form-input--success.sc-digi-form-input{--BORDER:var(--digi--form-input--border--success);--BORDER-COLOR:var(--digi--form-input--border-color--success);--BACKGROUND:var(--digi--form-input--background--success)}.sc-digi-form-input-h .digi-form-input--warning.sc-digi-form-input{--BORDER:var(--digi--form-input--border--warning);--BORDER-COLOR:var(--digi--form-input--border-color--warning);--BACKGROUND:var(--digi--form-input--background--warning)}.sc-digi-form-input-h .digi-form-input--error.sc-digi-form-input{--BORDER:var(--digi--form-input--border--error);--BORDER-COLOR:var(--digi--form-input--border-color--error);--BACKGROUND:var(--digi--form-input--background--error)}.digi-form-input__input-wrapper.sc-digi-form-input{display:flex}.digi-form-input__input-wrapper.sc-digi-form-input-s>[slot^=button] button,.digi-form-input__input-wrapper .sc-digi-form-input-s>[slot^=button] button{--MIN-HEIGHT:100%}.digi-form-input--button-variation-primary .digi-form-input__input-wrapper.sc-digi-form-input-s>[slot^=button] button,.digi-form-input--button-variation-primary .digi-form-input__input-wrapper .sc-digi-form-input-s>[slot^=button] button{border-start-start-radius:0;border-end-start-radius:0}.digi-form-input--button-variation-secondary.digi-form-input--has-button-true.sc-digi-form-input .digi-form-input__input-wrapper.sc-digi-form-input{gap:var(--digi--gutter--medium);flex-wrap:wrap}.digi-form-input__input.sc-digi-form-input{flex:1;height:var(--HEIGHT);font-family:var(--digi--global--typography--font-family--default);font-size:var(--digi--typography--body--font-size--desktop);padding:var(--digi--form-input--padding);color:var(--digi--color--text--primary);background:var(--BACKGROUND);border:var(--BORDER);border-color:var(--BORDER-COLOR);border-radius:var(--digi--form-input--border-radius);box-sizing:border-box}.digi-form-input__input.sc-digi-form-input:focus-visible{box-shadow:var(--digi--focus-shadow);outline:var(--digi--focus-outline)}.digi-form-input--button-variation-primary.digi-form-input--has-button-true.sc-digi-form-input .digi-form-input__input.sc-digi-form-input{border-end-end-radius:0;border-start-end-radius:0}";

const FormInput = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.afOnChange = createEvent(this, "afOnChange", 7);
    this.afOnBlur = createEvent(this, "afOnBlur", 7);
    this.afOnKeyup = createEvent(this, "afOnKeyup", 7);
    this.afOnFocus = createEvent(this, "afOnFocus", 7);
    this.afOnFocusout = createEvent(this, "afOnFocusout", 7);
    this.afOnInput = createEvent(this, "afOnInput", 7);
    this.afOnDirty = createEvent(this, "afOnDirty", 7);
    this.afOnTouched = createEvent(this, "afOnTouched", 7);
    this.hasActiveValidationMessage = false;
    this.hasButton = undefined;
    this.dirty = false;
    this.touched = false;
    this.afLabel = undefined;
    this.afLabelDescription = undefined;
    this.afType = FormInputType.TEXT;
    this.afButtonVariation = FormInputButtonVariation.PRIMARY;
    this.afAutofocus = undefined;
    this.afVariation = FormInputVariation.MEDIUM;
    this.afName = undefined;
    this.afId = randomIdGenerator('digi-form-input');
    this.afMaxlength = undefined;
    this.afMinlength = undefined;
    this.afRequired = undefined;
    this.afRequiredText = undefined;
    this.afAnnounceIfOptional = false;
    this.afAnnounceIfOptionalText = undefined;
    this.value = undefined;
    this.afValue = undefined;
    this.afValidation = FormInputValidation.NEUTRAL;
    this.afValidationText = undefined;
    this.afRole = undefined;
    this.afAutocomplete = undefined;
    this.afMin = undefined;
    this.afMax = undefined;
    this.afList = undefined;
    this.afStep = undefined;
    this.afDirname = undefined;
    this.afAriaAutocomplete = undefined;
    this.afAriaActivedescendant = undefined;
    this.afAriaLabelledby = undefined;
    this.afAriaDescribedby = undefined;
  }
  onValueChanged(value) {
    this.afValue = value;
  }
  onAfValueChanged(value) {
    this.value = value;
  }
  afValidationTextWatch() {
    this.setActiveValidationMessage();
  }
  /**
   * Hämtar en referens till inputelementet. Bra för att t.ex. sätta fokus programmatiskt.
   * @en Returns a reference to the input element. Handy for setting focus programmatically.
   */
  async afMGetFormControlElement() {
    return this._input;
  }
  componentWillLoad() {
    this.afValue ? (this.value = this.afValue) : (this.afValue = this.value);
    this.setActiveValidationMessage();
    this.setHasButton();
  }
  componentWillUpdate() {
    this.setHasButton();
  }
  setActiveValidationMessage() {
    this.hasActiveValidationMessage = !!this.afValidationText;
  }
  get cssModifiers() {
    return {
      'digi-form-input--small': this.afVariation === FormInputVariation.SMALL,
      'digi-form-input--medium': this.afVariation === FormInputVariation.MEDIUM,
      'digi-form-input--large': this.afVariation === FormInputVariation.LARGE,
      'digi-form-input--neutral': this.afValidation === FormInputValidation.NEUTRAL,
      'digi-form-input--success': this.afValidation === FormInputValidation.SUCCESS,
      'digi-form-input--error': this.afValidation === FormInputValidation.ERROR,
      'digi-form-input--warning': this.afValidation === FormInputValidation.WARNING,
      [`digi-form-input--button-variation-${this.afButtonVariation}`]: !!this.afButtonVariation,
      [`digi-form-input--has-button-${this.hasButton}`]: true,
      'digi-form-input--empty': !this.afValue &&
        (!this.afValidation || this.afValidation === FormInputValidation.NEUTRAL)
    };
  }
  blurHandler(e) {
    if (!this.touched) {
      this.afOnTouched.emit(e);
      this.touched = true;
    }
    this.afOnBlur.emit(e);
  }
  changeHandler(e) {
    this.setActiveValidationMessage();
    this.afOnChange.emit(e);
  }
  focusHandler(e) {
    this.afOnFocus.emit(e);
  }
  focusoutHandler(e) {
    this.afOnFocusout.emit(e);
  }
  keyupHandler(e) {
    this.afOnKeyup.emit(e);
  }
  inputHandler(e) {
    this.afValue = this.value =
      this.afType === FormInputType.NUMBER
        ? parseFloat(e.target.value)
        : e.target.value;
    if (!this.dirty) {
      this.afOnDirty.emit(e);
      this.dirty = true;
    }
    this.setActiveValidationMessage();
    this.afOnInput.emit(e);
  }
  setHasButton() {
    this.hasButton = !!this.hostElement.querySelector('[slot="button"]');
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-form-input': true }, this.cssModifiers) }, h("digi-form-label", { class: "digi-form-input__label", afFor: this.afId, afLabel: this.afLabel, afDescription: this.afLabelDescription, afRequired: this.afRequired, afAnnounceIfOptional: this.afAnnounceIfOptional, afRequiredText: this.afRequiredText, afAnnounceIfOptionalText: this.afAnnounceIfOptionalText }), h("div", { class: "digi-form-input__input-wrapper" }, h("input", { class: "digi-form-input__input", ref: (el) => (this._input = el), onBlur: (e) => this.blurHandler(e), onChange: (e) => this.changeHandler(e), onFocus: (e) => this.focusHandler(e), onFocusout: (e) => this.focusoutHandler(e), onKeyUp: (e) => this.keyupHandler(e), onInput: (e) => this.inputHandler(e), "aria-activedescendant": this.afAriaActivedescendant, "aria-describedby": this.afAriaDescribedby, "aria-labelledby": this.afAriaLabelledby, "aria-autocomplete": this.afAriaAutocomplete, "aria-invalid": this.afValidation != FormInputValidation.NEUTRAL ? 'true' : 'false', autocomplete: this.afAutocomplete, autofocus: this.afAutofocus ? this.afAutofocus : null, maxLength: this.afMaxlength, minLength: this.afMinlength, max: this.afMax, min: this.afMin, step: this.afStep, list: this.afList, role: this.afRole, dirName: this.afDirname, required: this.afRequired, id: this.afId, name: this.afName, type: this.afType, value: this.afValue }), h("slot", { name: "button" })), h("div", { class: "digi-form-input__footer" }, h("div", { class: "digi-form-input__validation", "aria-atomic": "true", role: "alert", id: `${this.afId}--validation-message` }, this.hasActiveValidationMessage &&
      this.afValidation != FormInputValidation.NEUTRAL && (h("digi-form-validation-message", { class: "digi-form-input__validation-message", "af-variation": this.afValidation }, this.afValidationText))))));
  }
  get hostElement() { return getElement(this); }
  static get watchers() { return {
    "value": ["onValueChanged"],
    "afValue": ["onAfValueChanged"],
    "afValidationText": ["afValidationTextWatch"]
  }; }
};
FormInput.style = formInputCss;

export { FormInput as digi_form_input };
