import { r as registerInstance, h } from './index-7f342a75.js';
import { L as LayoutBlockContainer, a as LayoutBlockVariation } from './layout-block-variation.enum-dbd50b66.js';

const layoutBlockCss = ".sc-digi-layout-block-h{--digi--layout-block--background--primary:var(--digi--color--background--primary);--digi--layout-block--background--secondary:var(--digi--color--background--secondary);--digi--layout-block--background--tertiary:var(--digi--color--background--neutral-4);--digi--layout-block--background--symbol:var(--digi--color--background--neutral-5);--digi--layout-block--background--profile:var(--digi--color--background--inverted-1);display:block}.sc-digi-layout-block-h .digi-layout-block.sc-digi-layout-block{background:var(--BACKGROUND)}.sc-digi-layout-block-h .digi-layout-block--transparent.sc-digi-layout-block{--BACKGROUND:transparent}.sc-digi-layout-block-h .digi-layout-block--primary.sc-digi-layout-block{--BACKGROUND:var(--digi--layout-block--background--primary)}.sc-digi-layout-block-h .digi-layout-block--secondary.sc-digi-layout-block{--BACKGROUND:var(--digi--layout-block--background--secondary)}.sc-digi-layout-block-h .digi-layout-block--tertiary.sc-digi-layout-block{--BACKGROUND:var(--digi--layout-block--background--tertiary)}.sc-digi-layout-block-h .digi-layout-block--symbol.sc-digi-layout-block{--BACKGROUND:var(--digi--layout-block--background--symbol)}.sc-digi-layout-block-h .digi-layout-block--profile.sc-digi-layout-block{--BACKGROUND:var(--digi--layout-block--background--profile)}";

const LayoutBlock = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this._container = LayoutBlockContainer.STATIC;
    this.afVariation = LayoutBlockVariation.PRIMARY;
    this.afContainer = LayoutBlockContainer.STATIC;
    this.afVerticalPadding = undefined;
    this.afMarginTop = undefined;
    this.afMarginBottom = undefined;
  }
  containerChangeHandler() {
    this._container = this.afContainer;
  }
  componentWillLoad() {
    this.containerChangeHandler();
  }
  get cssModifiers() {
    return {
      'digi-layout-block--transparent': this.afVariation === LayoutBlockVariation.TRANSPARENT,
      'digi-layout-block--primary': this.afVariation === LayoutBlockVariation.PRIMARY,
      'digi-layout-block--secondary': this.afVariation === LayoutBlockVariation.SECONDARY,
      'digi-layout-block--tertiary': this.afVariation === LayoutBlockVariation.TERTIARY,
      'digi-layout-block--symbol': this.afVariation === LayoutBlockVariation.SYMBOL,
      'digi-layout-block--profile': this.afVariation === LayoutBlockVariation.PROFILE
    };
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-layout-block': true }, this.cssModifiers) }, this._container === LayoutBlockContainer.NONE ? (h("slot", null)) : (h("digi-layout-container", { afVariation: this._container, "af-vertical-padding": this.afVerticalPadding, "af-margin-top": this.afMarginTop, "af-margin-bottom": this.afMarginBottom }, h("slot", null)))));
  }
  static get watchers() { return {
    "afContainer": ["containerChangeHandler"]
  }; }
};
LayoutBlock.style = layoutBlockCss;

export { LayoutBlock as digi_layout_block };
