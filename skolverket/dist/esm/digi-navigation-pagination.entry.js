import { r as registerInstance, c as createEvent, h } from './index-7f342a75.js';
import { r as randomIdGenerator } from './randomIdGenerator.util-a9066813.js';
import { B as ButtonSize } from './button-size.enum-8999fdcb.js';
import './button-type.enum-95f504f6.js';
import { B as ButtonVariation } from './button-variation.enum-3cc46e30.js';
import './expandable-accordion-variation.enum-9166e001.js';
import './calendar-week-view-heading-level.enum-05020882.js';
import './form-radiobutton-variation.enum-60c58c7d.js';
import './code-block-variation.enum-42e2bb44.js';
import './code-example-variation.enum-fbd04208.js';
import './code-variation.enum-ae00e62c.js';
import './form-checkbox-variation.enum-ad992b3e.js';
import './form-file-upload-variation.enum-882cb26e.js';
import './form-input-search-variation.enum-04106f0e.js';
import './form-input-type.enum-58a900d7.js';
import './form-input-variation.enum-07b4b98a.js';
import './form-select-variation.enum-fbd25aa0.js';
import './form-textarea-variation.enum-9d7b381e.js';
import './form-validation-message-variation.enum-5784e9ac.js';
import './layout-block-variation.enum-dbd50b66.js';
import './layout-columns-variation.enum-26e6db94.js';
import './layout-container-variation.enum-d17100f2.js';
import './layout-media-object-alignment.enum-f3156fc7.js';
import './link-external-variation.enum-f6d7a12f.js';
import './link-internal-variation.enum-8c3b0dd3.js';
import './link-variation.enum-81c1b9cf.js';
import './loader-spinner-size.enum-4999231f.js';
import './media-figure-alignment.enum-79554408.js';
import './navigation-context-menu-item-type.enum-178d1050.js';
import './navigation-sidebar-variation.enum-443b2b03.js';
import './navigation-vertical-menu-variation.enum-37c685d3.js';
import './progress-step-variation.enum-1f263b15.js';
import './progress-steps-variation.enum-fb681025.js';
import './progressbar-variation.enum-de87bf7f.js';
import './tag-size.enum-2927e760.js';
import './typography-meta-variation.enum-1eb26125.js';
import './typography-time-variation.enum-79f1b0de.js';
import './typography-variation.enum-b073e180.js';
import './util-breakpoint-observer-breakpoints.enum-8088d0a3.js';

const navigationPaginationCss = ".digi-navigation-pagination.sc-digi-navigation-pagination{padding:var(--digi--gutter--largest);display:grid;grid-template-rows:min-content auto;grid-template-areas:\"result\" \"pagination\"}@media (min-width: 48rem){.digi-navigation-pagination.sc-digi-navigation-pagination{gap:var(--digi--gutter--medium)}}.digi-navigation-pagination__result.sc-digi-navigation-pagination{grid-area:result}@media (max-width: 47.9375rem){.digi-navigation-pagination__result.sc-digi-navigation-pagination{display:none}}.digi-navigation-pagination__pagination.sc-digi-navigation-pagination{grid-area:pagination;display:grid;grid-template-columns:1fr;grid-template-areas:\"select\" \"buttons\";gap:var(--digi--gutter--medium);justify-content:space-between;align-items:end}@media (min-width: 48rem){.digi-navigation-pagination__pagination.sc-digi-navigation-pagination{grid-template-columns:auto auto;grid-template-areas:\"select buttons\"}}.digi-navigation-pagination__buttons.sc-digi-navigation-pagination{grid-area:buttons;display:grid;grid-template-columns:repeat(2, 1fr);gap:var(--digi--gutter--icon);align-items:center}@media (min-width: 48rem){.digi-navigation-pagination__buttons.sc-digi-navigation-pagination{grid-template-columns:repeat(2, auto)}}.digi-navigation-pagination__button.sc-digi-navigation-pagination{grid-column:1}.digi-navigation-pagination__button--hidden.sc-digi-navigation-pagination{display:none}.digi-navigation-pagination__button.sc-digi-navigation-pagination:nth-child(2){grid-column:2}.digi-navigation-pagination__select.sc-digi-navigation-pagination{min-width:150px}.digi-navigation-pagination__aria-label.sc-digi-navigation-pagination{clip:rect(0 0 0 0);height:1px;padding:0;white-space:nowrap;width:1px;overflow:hidden;position:absolute}";

const NavigationPagination = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.afOnPageChange = createEvent(this, "afOnPageChange", 7);
    this.pages = [];
    this.currentPage = undefined;
    this.afInitActivePage = 1;
    this.afTotalPages = undefined;
    this.afCurrentResultStart = undefined;
    this.afId = randomIdGenerator('digi-navigation-pagination');
    this.afCurrentResultEnd = undefined;
    this.afTotalResults = 0;
    this.afResultName = 'träffar';
  }
  afInitActivePageChanged() {
    this.setCurrentPage(this.afInitActivePage, false);
  }
  /**
   * Kan användas för att manuellt sätta om den aktiva sidan.
   * @en Can be used to set the active page.
   */
  async afMSetCurrentPage(pageNumber) {
    this.setCurrentPage(pageNumber, false);
  }
  prevPage() {
    this.currentPage--;
    this.afOnPageChange.emit(this.currentPage);
  }
  nextPage() {
    this.currentPage++;
    this.afOnPageChange.emit(this.currentPage);
  }
  setCurrentPage(pageToSet = this.currentPage, emitEvent = true) {
    if (pageToSet === this.currentPage) {
      return;
    }
    this.currentPage =
      pageToSet <= 1
        ? 1
        : pageToSet <= this.afTotalPages
          ? pageToSet
          : this.afTotalPages;
    console.log(pageToSet, this.currentPage);
    if (emitEvent) {
      this.afOnPageChange.emit(this.currentPage);
    }
  }
  componentWillLoad() {
    this.setCurrentPage(this.afInitActivePage, false);
    this.updateTotalPages();
  }
  updateTotalPages() {
    if (this.afTotalPages) {
      this.pages = [...Array(this.afTotalPages)];
    }
  }
  isCurrentPage(currentPage, page) {
    if (currentPage === page || null) {
      return 'page';
    }
  }
  labelledby() {
    if (this.afTotalResults > 0) {
      return `
        ${this.afId}-aria-label
        ${this.afId}-result-show
        ${this.afId}-result-current
        ${this.afId}-result-of
        ${this.afId}-result-total
        ${this.afId}-result-name
      `;
    }
    else {
      return `${this.afId}-aria-label`;
    }
  }
  render() {
    return (h("div", { class: "digi-navigation-pagination" }, h("span", { id: `${this.afId}-aria-label`, "aria-hidden": "true", class: "digi-navigation-pagination__aria-label" }, "Paginering"), this.afTotalResults > 0 && (h("div", { class: {
        'digi-navigation-pagination__result': true,
        'digi-navigation-pagination__result--pages': this.pages.length > 1
      }, "aria-hidden": this.pages.length > 1 ? 'true' : 'false', id: `${this.afId}-result` }, h("digi-typography", null, h("span", { id: `${this.afId}-result-show` }, "Visar "), h("strong", { id: `${this.afId}-result-current` }, this.afCurrentResultStart, "-", this.afCurrentResultEnd), h("span", { id: `${this.afId}-result-of` }, " av "), h("strong", { id: `${this.afId}-result-total` }, this.afTotalResults), this.afResultName && (h("span", { id: `${this.afId}-result-name` }, ` ${this.afResultName}`))))), this.pages.length > 1 && (h("nav", { "aria-labelledby": this.labelledby(), class: {
        'digi-navigation-pagination__pagination': true
      } }, h("div", { class: "digi-navigation-pagination__select" }, h("digi-form-select", { afLabel: `Sida`, afValue: `${this.currentPage}`, onAfOnChange: (e) => {
        this.setCurrentPage(parseInt(e.detail.target.value), true);
      } }, this.pages.map((_, index) => (h("option", { value: `${index + 1}`, selected: index + 1 === this.currentPage }, index + 1, " av ", this.afTotalPages))))), h("div", { class: "digi-navigation-pagination__buttons" }, h("digi-button", { onClick: () => this.prevPage(), afVariation: ButtonVariation.SECONDARY, afSize: ButtonSize.LARGE, afFullWidth: true, class: {
        'digi-navigation-pagination__button digi-navigation-pagination__button--previous': true,
        'digi-navigation-pagination__button--keep-left': this.currentPage === this.pages.length,
        'digi-navigation-pagination__button--hidden': this.currentPage === 1
      } }, h("digi-icon", { slot: "icon", afName: `chevron-left` }), h("span", null, "F\u00F6reg\u00E5ende")), h("digi-button", { onClick: () => this.nextPage(), afVariation: ButtonVariation.SECONDARY, afSize: ButtonSize.LARGE, afFullWidth: true, class: {
        'digi-navigation-pagination__button digi-navigation-pagination__button--next': true,
        'digi-navigation-pagination__button--keep-right': this.currentPage === 1,
        'digi-navigation-pagination__button--hidden': this.currentPage === this.afTotalPages
      }, "af-variation": "secondary" }, h("digi-icon", { slot: "icon-secondary", afName: `chevron-right` }), h("span", null, "N\u00E4sta")))))));
  }
  static get watchers() { return {
    "afInitActivePage": ["afInitActivePageChanged"],
    "afTotalPages": ["updateTotalPages"]
  }; }
};
NavigationPagination.style = navigationPaginationCss;

export { NavigationPagination as digi_navigation_pagination };
