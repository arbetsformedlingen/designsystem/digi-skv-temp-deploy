import { r as registerInstance, h, g as getElement } from './index-7f342a75.js';
import { r as randomIdGenerator } from './randomIdGenerator.util-a9066813.js';
import { l as logger } from './logger.util-b54855d5.js';
import { a as NavigationVerticalMenuVariation, N as NavigationVerticalMenuActiveIndicatorSize } from './navigation-vertical-menu-variation.enum-37c685d3.js';

const navigationVerticalMenuCss = ".sc-digi-navigation-vertical-menu-h{--digi--navigation-vertical-menu--background:transparent;--digi--navigation-vertical-menu--item--border-top:solid var(--digi--border-width--primary) var(--digi--color--border--neutral-4)}.sc-digi-navigation-vertical-menu-h .digi-navigation-vertical-menu.sc-digi-navigation-vertical-menu{display:block;background:var(--digi--navigation-vertical-menu--background)}.sc-digi-navigation-vertical-menu-s>.digi-navigation-vertical-menu--indicator--secondary ul.digi-navigation-vertical-menu__list--root.secondary{--subnav-indicator--width:var(--subnav-indicator--width--xl)}.sc-digi-navigation-vertical-menu-s>.digi-navigation-vertical-menu--indicator--secondary ul.digi-navigation-vertical-menu__list--root.secondary .digi-navigation-vertical-menu-item{--digi--navigation-vertical-menu-item--active-indicator-width:var(--subnav-indicator--width--xl)}.sc-digi-navigation-vertical-menu-s ul.digi-navigation-vertical-menu__list--root{--subnav-indicator--left:var(--digi--padding--medium);--subnav-indicator--width:1px;--subnav-indicator--width--l:4px;--subnav-indicator--width--xl:6px;--subnav-indicator--background-color:var(--digi--color--border--neutral-4);--subnav-indicator--background-color--default:var(--digi--color--border--neutral-4);--subnav-indicator--background-color--current:var(--digi--color--border--neutral-1)}.sc-digi-navigation-vertical-menu-s ul.digi-navigation-vertical-menu__list--root,.sc-digi-navigation-vertical-menu-s ul.digi-navigation-vertical-menu__list--root ul,.sc-digi-navigation-vertical-menu-s ul.digi-navigation-vertical-menu__list--root li{list-style:none;padding:0;margin:0}.sc-digi-navigation-vertical-menu-s ul.digi-navigation-vertical-menu__list--root>li:not(:first-child){border-top:var(--digi--navigation-vertical-menu--item--border-top)}.sc-digi-navigation-vertical-menu-s ul.digi-navigation-vertical-menu__list--root>li ul digi-navigation-vertical-menu-item{--digi--navigation-vertical-menu-item--font-size:var(--digi--typography--tag--font-size--desktop)}.sc-digi-navigation-vertical-menu-s ul.digi-navigation-vertical-menu__list--root.primary>.digi-navigation-vertical-menu__item--active{padding-bottom:var(--digi--padding--small)}.sc-digi-navigation-vertical-menu-s ul.digi-navigation-vertical-menu__list--root.primary .digi-navigation-vertical-menu__item--current-level{--subnav-indicator--background-color:var(--subnav-indicator--background-color--current)}.sc-digi-navigation-vertical-menu-s ul.digi-navigation-vertical-menu__list--root.primary .digi-navigation-vertical-menu__item--current-level ul{--subnav-indicator--background-color:var(--subnav-indicator--background-color--default)}.sc-digi-navigation-vertical-menu-s ul.digi-navigation-vertical-menu__list--root.primary .digi-navigation-vertical-menu__item--current-level digi-navigation-vertical-menu-item{--digi--navigation-vertical-menu-item--active-indicator-width:4px}.sc-digi-navigation-vertical-menu-s ul.digi-navigation-vertical-menu__list--root.primary .digi-navigation-vertical-menu__item--active{position:relative}.sc-digi-navigation-vertical-menu-s ul.digi-navigation-vertical-menu__list--root.primary .digi-navigation-vertical-menu__item--active>ul{position:relative}.sc-digi-navigation-vertical-menu-s ul.digi-navigation-vertical-menu__list--root.primary .digi-navigation-vertical-menu__item--active>ul::before{content:\"\";display:inline-block;position:absolute;top:0;bottom:0;z-index:1;left:var(--subnav-indicator--left);width:var(--subnav-indicator--width);background-color:var(--subnav-indicator--background-color)}.sc-digi-navigation-vertical-menu-s ul.digi-navigation-vertical-menu__list--root.primary .digi-navigation-vertical-menu__item--active>ul .digi-navigation-vertical-menu-item{--digi--navigation-vertical-menu-item--active-indicator-left:var(--digi--padding--medium)}.sc-digi-navigation-vertical-menu-s ul.digi-navigation-vertical-menu__list--root.primary .digi-navigation-vertical-menu__item--active>ul ul{--subnav-indicator--left:var(--digi--padding--largest)}.sc-digi-navigation-vertical-menu-s ul.digi-navigation-vertical-menu__list--root.primary .digi-navigation-vertical-menu__item--active>ul ul .digi-navigation-vertical-menu-item{--digi--navigation-vertical-menu-item--active-indicator-left:var(--digi--padding--largest)}.sc-digi-navigation-vertical-menu-s ul.digi-navigation-vertical-menu__list--root.primary .digi-navigation-vertical-menu__item--active>ul ul ul{--subnav-indicator--left:calc(var(--digi--padding--largest-2) + var(--digi--padding--smaller))}.sc-digi-navigation-vertical-menu-s ul.digi-navigation-vertical-menu__list--root.primary .digi-navigation-vertical-menu__item--active>ul ul ul .digi-navigation-vertical-menu-item{--digi--navigation-vertical-menu-item--active-indicator-left:calc(var(--digi--padding--largest-2) + var(--digi--padding--smaller))}.sc-digi-navigation-vertical-menu-s ul.digi-navigation-vertical-menu__list--root.primary .digi-navigation-vertical-menu__item--active>ul ul ul ul{--subnav-indicator--left:calc(var(--digi--padding--largest-2) + var(--digi--padding--largest))}.sc-digi-navigation-vertical-menu-s ul.digi-navigation-vertical-menu__list--root.primary .digi-navigation-vertical-menu__item--active>ul ul ul ul .digi-navigation-vertical-menu-item{--digi--navigation-vertical-menu-item--active-indicator-left:calc(var(--digi--padding--largest-2) + var(--digi--padding--largest))}.sc-digi-navigation-vertical-menu-s ul.digi-navigation-vertical-menu__list--root.primary .digi-navigation-vertical-menu__item--active>ul ul ul ul ul{--subnav-indicator--left:calc(var(--digi--padding--largest-2) + var(--digi--padding--largest-2))}.sc-digi-navigation-vertical-menu-s ul.digi-navigation-vertical-menu__list--root.primary .digi-navigation-vertical-menu__item--active>ul ul ul ul ul .digi-navigation-vertical-menu-item{--digi--navigation-vertical-menu-item--active-indicator-left:calc(var(--digi--padding--largest-2) + var(--digi--padding--largest-2))}.sc-digi-navigation-vertical-menu-s ul.digi-navigation-vertical-menu__list--root.primary.digi-navigation-vertical-menu__item--current-level::before,.sc-digi-navigation-vertical-menu-s ul.digi-navigation-vertical-menu__list--root.primary .digi-navigation-vertical-menu__item--current-level::before{--subnav-indicator--width:var(--subnav-indicator--width--l)}.sc-digi-navigation-vertical-menu-s ul.digi-navigation-vertical-menu__list--root.primary ul .digi-navigation-vertical-menu-item{--digi--navigation-vertical-menu-item--padding:var(--digi--padding--small) var(--digi--padding--largest)}.sc-digi-navigation-vertical-menu-s ul.digi-navigation-vertical-menu__list--root.primary ul ul .digi-navigation-vertical-menu-item{--digi--navigation-vertical-menu-item--padding:var(--digi--padding--small) var(--digi--padding--medium) var(--digi--padding--small) calc(var(--digi--padding--largest-2) + var(--digi--padding--smaller))}.sc-digi-navigation-vertical-menu-s ul.digi-navigation-vertical-menu__list--root.primary ul ul ul .digi-navigation-vertical-menu-item{--digi--navigation-vertical-menu-item--padding:var(--digi--padding--small) var(--digi--padding--medium) var(--digi--padding--small) calc(var(--digi--padding--largest-2) + var(--digi--padding--largest))}.sc-digi-navigation-vertical-menu-s ul.digi-navigation-vertical-menu__list--root.primary ul ul ul ul .digi-navigation-vertical-menu-item{--digi--navigation-vertical-menu-item--padding:var(--digi--padding--small) var(--digi--padding--medium) var(--digi--padding--small) calc(var(--digi--padding--largest-2) + var(--digi--padding--largest-2))}.sc-digi-navigation-vertical-menu-s ul.digi-navigation-vertical-menu__list--root.secondary{--subnav-indicator--width:var(--subnav-indicator--width--l)}.sc-digi-navigation-vertical-menu-s ul.digi-navigation-vertical-menu__list--root.secondary>.digi-navigation-vertical-menu__item--active{position:relative}.sc-digi-navigation-vertical-menu-s ul.digi-navigation-vertical-menu__list--root.secondary>.digi-navigation-vertical-menu__item--active::before{content:\"\";display:inline-block;position:absolute;top:0;bottom:0;width:var(--subnav-indicator--width);background-color:var(--digi--color--background--success-3);z-index:1}.sc-digi-navigation-vertical-menu-s ul.digi-navigation-vertical-menu__list--root.secondary .digi-navigation-vertical-menu__item--deep-active{background-color:var(--digi--color--background--success-2)}.sc-digi-navigation-vertical-menu-s ul.digi-navigation-vertical-menu__list--root.secondary .digi-navigation-vertical-menu__item--deep-active>ul>li>digi-navigation-vertical-menu-item{--digi--navigation-vertical-menu-item--toggle-icon--background:var(--digi--color--background--primary);--digi--navigation-vertical-menu-item--toggle-icon--background--active:var(--digi--color--background--primary)}.sc-digi-navigation-vertical-menu-s ul.digi-navigation-vertical-menu__list--root.secondary ul .digi-navigation-vertical-menu-item{--digi--navigation-vertical-menu-item--padding:var(--digi--padding--small) var(--digi--padding--largest)}.sc-digi-navigation-vertical-menu-s ul.digi-navigation-vertical-menu__list--root.secondary ul ul .digi-navigation-vertical-menu-item{--digi--navigation-vertical-menu-item--padding:var(--digi--padding--small) var(--digi--padding--medium) var(--digi--padding--small) calc(var(--digi--padding--largest-2) + var(--digi--padding--smaller))}.sc-digi-navigation-vertical-menu-s ul.digi-navigation-vertical-menu__list--root.secondary ul ul ul .digi-navigation-vertical-menu-item{--digi--navigation-vertical-menu-item--padding:var(--digi--padding--small) var(--digi--padding--medium) var(--digi--padding--small) calc(var(--digi--padding--largest-2) + var(--digi--padding--largest))}.sc-digi-navigation-vertical-menu-s ul.digi-navigation-vertical-menu__list--root.secondary ul ul ul ul .digi-navigation-vertical-menu-item{--digi--navigation-vertical-menu-item--padding:var(--digi--padding--small) var(--digi--padding--medium) var(--digi--padding--small) calc(var(--digi--padding--largest-2) + var(--digi--padding--largest-2))}.sc-digi-navigation-vertical-menu-s li.digi-navigation-vertical-menu__item--highlight>digi-navigation-vertical-menu-item .digi-navigation-vertical-menu-item__button:first-child .digi-navigation-vertical-menu-item__text::after,.sc-digi-navigation-vertical-menu-s li.digi-navigation-vertical-menu__item--highlight>digi-navigation-vertical-menu-item .digi-navigation-vertical-menu-item__link:first-child .digi-navigation-vertical-menu-item__text::after{display:inline-flex;position:relative;align-items:center;justify-content:center;overflow:hidden;content:\"\";width:rem(10);height:rem(10);border-radius:rem(10);line-height:1;transform:translateY(-5px);margin-left:var(--digi--padding--smaller);background:var(--digi--color--background--success-2)}";

const NavigationVerticalMenu = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.listItems = [];
    this.afId = randomIdGenerator('digi-navigation-vertical-menu');
    this.afVariation = NavigationVerticalMenuVariation.PRIMARY;
    this.afActiveIndicatorSize = NavigationVerticalMenuActiveIndicatorSize.PRIMARY;
    this.afAriaLabel = undefined;
  }
  linkClickHandler(e) {
    this.setCurrentActiveLevel(e.target);
    this.setClasses(e.target);
  }
  setClasses(el) {
    const activeDeepClass = 'digi-navigation-vertical-menu__item--deep-active';
    const activeClass = 'digi-navigation-vertical-menu__item--active';
    const navList = this.hostElement.querySelector('.digi-navigation-vertical-menu__list');
    const activeNavItems = Array.from(navList.children).filter((item) => item.classList.contains(activeClass));
    activeNavItems.forEach((listItem) => {
      listItem.classList.add(activeDeepClass);
      setClassRecursive(listItem);
    });
    function setClassRecursive(listItem) {
      const arr = Array.from(listItem.children).filter((item) => item.tagName === 'UL');
      const list = arr[0];
      const activeListItems = Array.from(list.children).filter((item) => item.classList.contains(activeClass));
      if (activeListItems.length == 0) {
        removeClassRecursive(list);
        return;
      }
      activeListItems.forEach((item) => {
        item.classList.add(activeDeepClass);
        setClassRecursive(item);
      });
    }
    function removeClassRecursive(item, deepElement = true) {
      const parentEl = item.closest('li');
      if (!parentEl) {
        return;
      }
      if (!deepElement) {
        parentEl.classList.remove(activeDeepClass);
      }
      const deepParent = parentEl.closest('ul');
      if (!deepParent) {
        return;
      }
      removeClassRecursive(deepParent, false);
    }
    const removeUnselected = this.hostElement.querySelectorAll(`.${activeDeepClass}:not(.${activeClass})`);
    removeUnselected.forEach((item) => {
      item.classList.remove(activeDeepClass);
    });
    if (!el.matches('.digi-navigation-vertical-menu-item--has-subnav')) {
      removeClassRecursive(el);
    }
  }
  componentWillLoad() {
    this.getListItems();
    this.initNav();
    this.setLevelClass();
  }
  componentDidLoad() {
    this.initCurrentActiveLevel();
  }
  componentWillUpdate() {
    this.getListItems();
    this.initNav();
    this.setLevelClass();
  }
  initCurrentActiveLevel() {
    var elActiveSubnav = this.hostElement.querySelector('digi-navigation-vertical-menu-item[af-active-subnav="true"]');
    if (!!elActiveSubnav) {
      function getDeep(elActiveSubnav) {
        const deepEl = elActiveSubnav
          .closest('li')
          .querySelector('ul')
          .querySelector('digi-navigation-vertical-menu-item[af-active-subnav="true"]');
        return !!deepEl ? deepEl : null;
      }
      function checkRecursive(deepEl) {
        const elToCheck = getDeep(deepEl);
        if (!!elToCheck) {
          elActiveSubnav = elToCheck;
          checkRecursive(elToCheck);
        }
        else {
          elActiveSubnav = deepEl;
        }
      }
      checkRecursive(elActiveSubnav);
      this.setCurrentActiveLevel(elActiveSubnav);
    }
  }
  setCurrentActiveLevel(targetElement) {
    const currentLevelClass = 'digi-navigation-vertical-menu__item--current-level';
    const target = targetElement;
    const oldTarget = this.hostElement.querySelectorAll('.digi-navigation-vertical-menu__item--current-level');
    oldTarget.forEach((el) => {
      el.classList.remove(currentLevelClass);
    });
    let subnav = target.closest('li').querySelector('ul');
    if (target.matches('.digi-navigation-vertical-menu-item--has-subnav')) {
      subnav = target.closest('ul');
      subnav.classList.add(currentLevelClass);
      return;
    }
    if (subnav) {
      subnav.classList.add(currentLevelClass);
    }
    else {
      subnav = target.closest('ul');
      subnav.classList.add(currentLevelClass);
    }
  }
  setLevelClass() {
    this.hostElement.querySelector('ul').classList.remove('primary', 'secondary');
    this.hostElement
      .querySelector('ul')
      .classList.add('digi-navigation-vertical-menu__list--root', this.afVariation);
  }
  initNav() {
    const $ul = this.hostElement.querySelectorAll('ul');
    const $li = this.hostElement.querySelectorAll('li');
    $ul.forEach((item) => item.classList.add('digi-navigation-vertical-menu__list'));
    $li.forEach((item) => item.classList.add('digi-navigation-vertical-menu__item'));
  }
  async afMSetCurrentActiveLevel(e) {
    this.setCurrentActiveLevel(e);
    this.initNav();
    this.setClasses(e);
  }
  getListItems() {
    let menuItems = this.hostElement.children;
    if (!menuItems || menuItems.length <= 0) {
      logger.warn(`The slot contains no children elements.`, this.hostElement);
      return;
    }
    this.listItems = [...Array.from(menuItems)].map((filter) => {
      return {
        outerHTML: filter.outerHTML
      };
    });
  }
  get cssModifiers() {
    return {
      'digi-navigation-vertical-menu': false,
      'digi-navigation-vertical-menu--primary': this.afVariation === NavigationVerticalMenuVariation.PRIMARY,
      'digi-navigation-vertical-menu--secondary': this.afVariation === NavigationVerticalMenuVariation.SECONDARY,
      'digi-navigation-vertical-menu--indicator--secondary': this.afActiveIndicatorSize ===
        NavigationVerticalMenuActiveIndicatorSize.SECONDARY
    };
  }
  render() {
    return (h("nav", { class: Object.assign({ 'digi-navigation-vertical-menu': true }, this.cssModifiers), id: this.afId, "aria-label": this.afAriaLabel }, h("slot", null)));
  }
  get hostElement() { return getElement(this); }
};
NavigationVerticalMenu.style = navigationVerticalMenuCss;

export { NavigationVerticalMenu as digi_navigation_vertical_menu };
