import { r as registerInstance, h } from './index-7f342a75.js';
import { T as TypographyTimeVariation } from './typography-time-variation.enum-79f1b0de.js';

const TypographyTime = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.formatedDate = undefined;
    this.datetime = undefined;
    this.afDateTime = undefined;
    this.afVariation = TypographyTimeVariation.PRIMARY;
  }
  primaryFormat(date) {
    return date.toLocaleString('default', {
      year: 'numeric',
      month: '2-digit',
      day: '2-digit'
    });
  }
  prettyFormat(date) {
    return date.toLocaleString('default', {
      day: 'numeric',
      month: 'long',
      year: 'numeric'
    });
  }
  distanceFormat(date) {
    const oneDay = 1000 * 3600 * 24;
    const distance = Math.floor(Date.now() - date.getTime());
    let days = Math.floor(distance / oneDay);
    days = Math.abs(days);
    if (distance > oneDay) {
      return `För ${days} dagar sedan`;
    }
    if (distance < oneDay) {
      return days === 0 ? `Idag` : `Om ${days} dagar`;
    }
  }
  formatDate(dateToFormat) {
    const date = new Date(dateToFormat);
    switch (this.afVariation) {
      case TypographyTimeVariation.PRIMARY:
        this.formatedDate = this.primaryFormat(date);
        break;
      case TypographyTimeVariation.PRETTY:
        this.formatedDate = this.prettyFormat(date);
        break;
      case TypographyTimeVariation.DISTANCE:
        this.formatedDate = this.distanceFormat(date);
        break;
    }
    this.datetime = this.primaryFormat(date);
  }
  componentWillLoad() {
    this.formatDate(this.afDateTime);
  }
  render() {
    return (h("time", { dateTime: this.datetime, class: "digi-typography-time" }, this.formatedDate));
  }
  static get watchers() { return {
    "afDateTime": ["formatDate"]
  }; }
};

export { TypographyTime as digi_typography_time };
