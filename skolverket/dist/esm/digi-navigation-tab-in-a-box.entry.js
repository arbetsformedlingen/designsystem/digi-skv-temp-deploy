import { r as registerInstance, c as createEvent, h } from './index-7f342a75.js';
import './card-box-gutter.enum-4af8f128.js';
import { C as CardBoxWidth } from './card-box-width.enum-be3a34bc.js';
import './page-background.enum-013f61b0.js';
import './layout-grid-vertical-spacing.enum-cbd27319.js';
import './layout-stacked-blocks-variation.enum-412ac8dc.js';
import './list-link-variation.enum-1f6ee434.js';
import './navigation-breadcrumbs-variation.enum-1ca5e289.js';
import './notification-detail-variation.enum-06ceda96.js';
import './page-footer-variation.enum-6e6e5a39.js';
import './table-variation.enum-cd5a5aa0.js';

const slugify = (str) => str
  .toLowerCase()
  .trim()
  .replace(/å/g, 'a')
  .replace(/ä/g, 'a')
  .replace(/ö/g, 'o')
  .replace(/[^\w\s-]/g, '')
  .replace(/[\s_-]+/g, '-')
  .replace(/^-+|-+$/g, '');

const navigationTabInABoxCss = ".sc-digi-navigation-tab-in-a-box-h{display:contents}";

const NavigationTabInABox = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.afOnToggle = createEvent(this, "afOnToggle", 7);
    this.afAriaLabel = undefined;
    this.afId = undefined;
    this.afActive = undefined;
  }
  toggleHandler(activeTab) {
    if (activeTab || null) {
      this.afOnToggle.emit(activeTab);
    }
  }
  render() {
    return (h("digi-navigation-tab", { afAriaLabel: this.afAriaLabel, afId: this.afId || slugify(this.afAriaLabel), afActive: this.afActive, onAfOnToggle: (e) => this.afOnToggle.emit(e.detail) }, h("digi-card-box", { afWidth: CardBoxWidth.FULL }, h("digi-typography", null, h("slot", null)))));
  }
  static get watchers() { return {
    "afActive": ["toggleHandler"]
  }; }
};
NavigationTabInABox.style = navigationTabInABoxCss;

export { NavigationTabInABox as digi_navigation_tab_in_a_box };
