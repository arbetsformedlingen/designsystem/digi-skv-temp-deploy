var NavigationVerticalMenuActiveIndicatorSize;
(function (NavigationVerticalMenuActiveIndicatorSize) {
  NavigationVerticalMenuActiveIndicatorSize["PRIMARY"] = "primary";
  NavigationVerticalMenuActiveIndicatorSize["SECONDARY"] = "secondary";
})(NavigationVerticalMenuActiveIndicatorSize || (NavigationVerticalMenuActiveIndicatorSize = {}));

var NavigationVerticalMenuVariation;
(function (NavigationVerticalMenuVariation) {
  NavigationVerticalMenuVariation["PRIMARY"] = "primary";
  NavigationVerticalMenuVariation["SECONDARY"] = "secondary";
})(NavigationVerticalMenuVariation || (NavigationVerticalMenuVariation = {}));

export { NavigationVerticalMenuActiveIndicatorSize as N, NavigationVerticalMenuVariation as a };
