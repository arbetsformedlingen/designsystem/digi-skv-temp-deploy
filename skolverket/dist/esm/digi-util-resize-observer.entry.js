import { r as registerInstance, c as createEvent, h, g as getElement } from './index-7f342a75.js';

const utilResizeObserverCss = ".sc-digi-util-resize-observer-h{display:block}";

const UtilResizeObserver = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.afOnChange = createEvent(this, "afOnChange", 7);
    this._observer = null;
  }
  componentWillLoad() {
    this.initObserver();
  }
  disconnectedCallback() {
    this.removeObserver();
  }
  initObserver() {
    this._observer = new ResizeObserver(([entry]) => {
      this.afOnChange.emit(entry);
    });
    this._observer.observe(this.hostElement);
  }
  removeObserver() {
    this._observer.disconnect();
  }
  changeHandler(e) {
    this.afOnChange.emit(e);
  }
  render() {
    return h("slot", null);
  }
  get hostElement() { return getElement(this); }
};
UtilResizeObserver.style = utilResizeObserverCss;

export { UtilResizeObserver as digi_util_resize_observer };
