var ButtonVariation;
(function (ButtonVariation) {
  ButtonVariation["PRIMARY"] = "primary";
  ButtonVariation["SECONDARY"] = "secondary";
  ButtonVariation["FUNCTION"] = "function";
})(ButtonVariation || (ButtonVariation = {}));

export { ButtonVariation as B };
