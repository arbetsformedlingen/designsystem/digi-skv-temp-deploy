import { r as registerInstance, c as createEvent, h } from './index-7f342a75.js';
import { T as TagSize } from './tag-size.enum-2927e760.js';

const tagCss = ".sc-digi-tag-h{--digi--tag--display:inline-block;--digi--tag--margin-bottom:var(--digi--gutter--smaller)}.sc-digi-tag-h .digi-tag.sc-digi-tag{display:var(--digi--tag--display);margin-bottom:var(--digi--tag--margin-bottom)}";

const Tag = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.afOnClick = createEvent(this, "afOnClick", 7);
    this.afText = undefined;
    this.afNoIcon = false;
    this.afSize = TagSize.SMALL;
  }
  clickHandler(e) {
    this.afOnClick.emit(e);
  }
  render() {
    return (h("digi-button", { onAfOnClick: (e) => this.clickHandler(e), "af-variation": "secondary", "af-size": this.afSize, class: "digi-tag" }, this.afText, !this.afNoIcon && h("digi-icon", { afName: `x`, slot: "icon-secondary" })));
  }
};
Tag.style = tagCss;

export { Tag as digi_tag };
