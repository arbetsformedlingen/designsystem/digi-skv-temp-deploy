import { r as registerInstance, c as createEvent, h, g as getElement } from './index-7f342a75.js';
import { r as randomIdGenerator } from './randomIdGenerator.util-a9066813.js';
import { F as FormInputSearchVariation } from './form-input-search-variation.enum-04106f0e.js';
import { a as FormInputType, F as FormInputButtonVariation } from './form-input-type.enum-58a900d7.js';
import { B as ButtonType } from './button-type.enum-95f504f6.js';
import { d as detectClickOutside } from './detectClickOutside.util-ca9d6cb5.js';
import { d as detectFocusOutside } from './detectFocusOutside.util-5ce9ecfe.js';
import './detectClosest.util-2d3999b7.js';

const formInputSearchCss = ".sc-digi-form-input-search-h{width:100%}";

const FormInputSearch = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.afOnFocusOutside = createEvent(this, "afOnFocusOutside", 7);
    this.afOnFocusInside = createEvent(this, "afOnFocusInside", 7);
    this.afOnClickOutside = createEvent(this, "afOnClickOutside", 7);
    this.afOnClickInside = createEvent(this, "afOnClickInside", 7);
    this.afOnChange = createEvent(this, "afOnChange", 7);
    this.afOnBlur = createEvent(this, "afOnBlur", 7);
    this.afOnKeyup = createEvent(this, "afOnKeyup", 7);
    this.afOnFocus = createEvent(this, "afOnFocus", 7);
    this.afOnFocusout = createEvent(this, "afOnFocusout", 7);
    this.afOnInput = createEvent(this, "afOnInput", 7);
    this.afOnClick = createEvent(this, "afOnClick", 7);
    this.afLabel = undefined;
    this.afLabelDescription = undefined;
    this.afType = FormInputType.SEARCH;
    this.afButtonVariation = FormInputButtonVariation.PRIMARY;
    this.afName = undefined;
    this.afId = randomIdGenerator('digi-form-input-search');
    this.value = undefined;
    this.afValue = undefined;
    this.afAutocomplete = undefined;
    this.afAriaAutocomplete = undefined;
    this.afAriaActivedescendant = undefined;
    this.afAriaLabelledby = undefined;
    this.afAriaDescribedby = undefined;
    this.afVariation = FormInputSearchVariation.MEDIUM;
    this.afHideButton = undefined;
    this.afButtonType = ButtonType.SUBMIT;
    this.afButtonText = undefined;
    this.afButtonAriaLabel = undefined;
    this.afButtonAriaLabelledby = undefined;
    this.afAutofocus = undefined;
  }
  onValueChanged(value) {
    this.afValue = value;
  }
  onAfValueChanged(value) {
    this.value = value;
  }
  /**
   * Hämtar en referens till inputelementet. Bra för att t.ex. sätta fokus programmatiskt.
   * @en Returns a reference to the input element. Handy for setting focus programmatically.
   */
  async afMGetFormControlElement() {
    const formControlElement = await this._input.afMGetFormControlElement();
    return formControlElement;
  }
  blurHandler(e) {
    this.afOnBlur.emit(e);
  }
  changeHandler(e) {
    this.afValue = this.value = e.target.value;
    this.afOnChange.emit(e);
  }
  focusHandler(e) {
    this.afOnFocus.emit(e);
  }
  focusoutHandler(e) {
    this.afOnFocusout.emit(e);
  }
  keyupHandler(e) {
    this.afOnKeyup.emit(e);
  }
  inputHandler(e) {
    this.afOnInput.emit(e);
  }
  clickHandler(e) {
    this.afOnClick.emit(e);
  }
  clickOutsideListener(e) {
    const target = e.target;
    const selector = `[data-af-identifier="${this.afId}"]`;
    if (detectClickOutside(target, selector)) {
      this.afOnClickOutside.emit(e);
    }
    else {
      this.afOnClickInside.emit(e);
    }
  }
  focusOutsideListener(e) {
    const target = e.target;
    const selector = `[data-af-identifier="${this.afId}"]`;
    if (detectFocusOutside(target, selector)) {
      this.afOnFocusOutside.emit(e);
    }
    else {
      this.afOnFocusInside.emit(e);
    }
  }
  componentWillLoad() {
    this.afValue ? (this.value = this.afValue) : (this.afValue = this.value);
  }
  get cssModifiers() {
    return {
      'digi-form-input-search--small': this.afVariation === FormInputSearchVariation.SMALL,
      'digi-form-input-search--medium': this.afVariation === FormInputSearchVariation.MEDIUM,
      'digi-form-input-search--large': this.afVariation === FormInputSearchVariation.LARGE
    };
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-form-input-search': true }, this.cssModifiers), "data-af-identifier": this.afId }, h("digi-form-input", { class: {
        'digi-form-input-search__input': true,
        'digi-form-input-search__input--no-button': this.afHideButton
      }, ref: (el) => (this._input = el), onAfOnBlur: (e) => this.blurHandler(e), onAfOnChange: (e) => this.changeHandler(e), onAfOnFocus: (e) => this.focusHandler(e), onAfOnFocusout: (e) => this.focusoutHandler(e), onAfOnKeyup: (e) => this.keyupHandler(e), onAfOnInput: (e) => this.inputHandler(e), afLabel: this.afLabel, afLabelDescription: this.afLabelDescription, afAriaActivedescendant: this.afAriaActivedescendant, afAriaDescribedby: this.afAriaDescribedby, afAriaLabelledby: this.afAriaLabelledby, afAriaAutocomplete: this.afAriaAutocomplete, afAutocomplete: this.afAutocomplete, afName: this.afName, afType: this.afType, afValue: this.afValue, afVariation: this.afVariation, afAutofocus: this.afAutofocus ? this.afAutofocus : null, afButtonVariation: this.afButtonVariation }, !this.afHideButton && (h("digi-button", { class: "digi-form-input-search__button", onAfOnClick: (e) => this.clickHandler(e), afType: this.afButtonType, afAriaLabel: this.afButtonAriaLabel, afAriaLabelledby: this.afButtonAriaLabelledby, afSize: this.afVariation, slot: "button" }, h("digi-icon", { slot: "icon", afName: `search` }), this.afButtonText)))));
  }
  get hostElement() { return getElement(this); }
  static get watchers() { return {
    "value": ["onValueChanged"],
    "afValue": ["onAfValueChanged"]
  }; }
};
FormInputSearch.style = formInputSearchCss;

export { FormInputSearch as digi_form_input_search };
