import { r as registerInstance, h } from './index-7f342a75.js';
import './card-box-gutter.enum-4af8f128.js';
import { C as CardBoxWidth } from './card-box-width.enum-be3a34bc.js';
import './page-background.enum-013f61b0.js';
import './layout-grid-vertical-spacing.enum-cbd27319.js';
import './layout-stacked-blocks-variation.enum-412ac8dc.js';
import './list-link-variation.enum-1f6ee434.js';
import './navigation-breadcrumbs-variation.enum-1ca5e289.js';
import './notification-detail-variation.enum-06ceda96.js';
import './page-footer-variation.enum-6e6e5a39.js';
import './table-variation.enum-cd5a5aa0.js';

const pageBlockListsCss = ".digi-page-block-lists.sc-digi-page-block-lists{display:block;background:var(--digi--layout-page-block-lists--background, transparent);padding:var(--digi--responsive-grid-gutter) 0}.digi-page-block-lists--variation-start.sc-digi-page-block-lists,.digi-page-block-lists--variation-sub.sc-digi-page-block-lists{--digi--layout-page-block-lists--background:var(--digi--global--color--profile--apricot--opacity50)}.digi-page-block-lists--variation-section.sc-digi-page-block-lists{--digi--layout-page-block-lists--background:var(--digi--color--background--primary)}.digi-page-block-lists__inner.sc-digi-page-block-lists{display:grid;grid-template-columns:1fr 2fr;gap:var(--digi--responsive-grid-gutter)}@media (max-width: 47.9375rem){.digi-page-block-lists__inner.sc-digi-page-block-lists{grid-template-columns:1fr}}.digi-page-block-lists__inner.sc-digi-page-block-lists digi-layout-stacked-blocks.sc-digi-page-block-lists{width:100%}";

const PageBlockLists = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.afVariation = undefined;
  }
  get cssModifiers() {
    return {
      [`digi-page-block-lists--variation-${this.afVariation}`]: !!this.afVariation
    };
  }
  render() {
    return (h("digi-layout-container", { class: Object.assign({ 'digi-page-block-lists': true }, this.cssModifiers) }, h("digi-card-box", { afWidth: CardBoxWidth.FULL }, h("div", { class: "digi-page-block-lists__inner" }, h("digi-typography-heading-section", null, h("slot", { name: "heading" })), h("digi-layout-stacked-blocks", null, h("slot", null))))));
  }
  static get assetsDirs() { return ["public"]; }
};
PageBlockLists.style = pageBlockListsCss;

export { PageBlockLists as digi_page_block_lists };
