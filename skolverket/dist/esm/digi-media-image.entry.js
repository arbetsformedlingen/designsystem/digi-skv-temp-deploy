import { r as registerInstance, c as createEvent, h } from './index-7f342a75.js';

const mediaImageCss = ".digi-media-image.sc-digi-media-image{position:relative;display:inline-flex;max-width:100%;height:0px}.digi-media-image.digi-media-image--aspect-ratio.sc-digi-media-image{width:var(--digi--media-image--width);height:unset}.digi-media-image.digi-media-image--loaded.sc-digi-media-image{height:unset}.digi-media-image.digi-media-image--fullwidth.sc-digi-media-image{width:100%}.digi-media-image.digi-media-image--unlazy.sc-digi-media-image:not(.digi-media-image--aspect-ratio){height:unset}.digi-media-image--aspect-ratio.sc-digi-media-image .digi-media-image__observer.sc-digi-media-image{position:absolute;top:0;left:0;height:100%}.digi-media-image__padded-box.sc-digi-media-image{display:inline-block;padding-top:var(--digi--media-image--padded-box, 0);height:0}.digi-media-image__image.sc-digi-media-image{max-width:100%;height:auto;opacity:0.06;filter:saturate(0);transition:opacity 0.2s, filter 0.4s}.digi-media-image--loaded.sc-digi-media-image .digi-media-image__image.sc-digi-media-image{opacity:1;filter:saturate(1)}.digi-media-image--unlazy.sc-digi-media-image .digi-media-image__image.sc-digi-media-image{transition:none;opacity:1;filter:none}.digi-media-image--fullwidth.sc-digi-media-image .digi-media-image__image.sc-digi-media-image{width:100%}.digi-media-image--aspect-ratio.sc-digi-media-image .digi-media-image__image.sc-digi-media-image{position:absolute;top:0;left:0;width:100%;height:100%}";

const MediaImage = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.afOnLoad = createEvent(this, "afOnLoad", 7);
    this.isPlaceholderLoaded = false;
    this.hasSetSrc = false;
    this.hasAspectRatio = false;
    this.paddedBoxPadding = '0%';
    this.isLoaded = false;
    this.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNk+A8AAQUBAScY42YAAAAASUVORK5CYII=';
    this.srcset = undefined;
    this.afObserverOptions = {
      rootMargin: '200px',
      threshold: 0,
    };
    this.afSrc = undefined;
    this.afSrcset = undefined;
    this.afAlt = undefined;
    this.afTitle = undefined;
    this.afAriaLabel = undefined;
    this.afWidth = undefined;
    this.afHeight = undefined;
    this.afFullwidth = undefined;
    this.afUnlazy = false;
  }
  srcChangeHandler() {
    this.setImageSrc();
  }
  connectedCallback() {
    if (this.afUnlazy) {
      this.setImageSrc();
    }
    // Add padded box to wrapper if width and height props exists
    if (this.afWidth && this.afHeight) {
      this.hasAspectRatio = true;
      this.setPaddedBox();
    }
  }
  setImageSrc() {
    this.src = this.afSrc;
    this.srcset = this.afSrcset;
    this.hasSetSrc = true;
  }
  setPaddedBox() {
    this.paddedBoxPadding =
      (parseInt(this.afHeight, 10) / parseInt(this.afWidth, 10)) * 100 + '%';
  }
  intersectHandler() {
    if (this.afUnlazy) {
      return;
    }
    this.setImageSrc();
  }
  loadHandler(e) {
    if (this.afUnlazy) {
      return;
    }
    if (this.isPlaceholderLoaded) {
      this.isLoaded = true;
      this.afOnLoad.emit(e);
      return;
    }
    this.isPlaceholderLoaded = true;
  }
  get cssModifiers() {
    return {
      'digi-media-image--unlazy': this.afUnlazy,
      'digi-media-image--loaded': this.isLoaded,
      'digi-media-image--aspect-ratio': this.hasAspectRatio,
      'digi-media-image--fullwidth': this.afFullwidth
    };
  }
  get cssInlineStyles() {
    return {
      '--digi--media-image--padded-box': this.paddedBoxPadding,
    };
  }
  render() {
    return (h("host", null, h("div", { class: Object.assign({ 'digi-media-image': true }, this.cssModifiers), style: {
        '--digi--media-image--width': this.afWidth ? `${this.afWidth}px` : null
      } }, h("digi-util-intersection-observer", { class: "digi-media-image__observer", onAfOnIntersect: () => this.intersectHandler(), afOnce: true, afOptions: this.afObserverOptions }), this.hasAspectRatio && (h("div", { class: "digi-media-image__padded-box", style: this.cssInlineStyles })), h("img", { class: "digi-media-image__image", onLoad: (e) => this.loadHandler(e), src: this.src, alt: this.afAlt, "aria-label": this.afAriaLabel, height: this.afHeight, srcset: this.srcset, title: this.afTitle, width: this.afWidth, loading: this.afUnlazy ? 'eager' : 'lazy' }))));
  }
  static get watchers() { return {
    "afSrc": ["srcChangeHandler"]
  }; }
};
MediaImage.style = mediaImageCss;

export { MediaImage as digi_media_image };
