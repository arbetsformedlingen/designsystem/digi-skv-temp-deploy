import { r as registerInstance, c as createEvent, h, g as getElement } from './index-7f342a75.js';
import { r as randomIdGenerator } from './randomIdGenerator.util-a9066813.js';
import { l as logger } from './logger.util-b54855d5.js';
import { a as FormSelectVariation, F as FormSelectValidation } from './form-select-variation.enum-fbd25aa0.js';

const formSelectCss = ".sc-digi-form-select-h{--digi--form-select--background--empty:var(--digi--color--background--input-empty);--digi--form-select--background--neutral:var(--digi--color--background--input);--digi--form-select--background--success:var(--digi--color--background--success-2);--digi--form-select--background--warning:var(--digi--color--background--warning-2);--digi--form-select--background--error:var(--digi--color--background--danger-2);--digi--form-select--border-color--neutral:var(--digi--color--border--neutral-3);--digi--form-select--border-color--success:var(--digi--color--border--success);--digi--form-select--border-color--warning:var(--digi--color--border--neutral-3);--digi--form-select--border-color--error:var(--digi--color--border--danger);--digi--form-select--select-wrapper--position:relative;--digi--form-select--select--height--small:var(--digi--input-height--small);--digi--form-select--select--height--medium:var(--digi--input-height--medium);--digi--form-select--select--height--large:var(--digi--input-height--large);--digi--form-select--select--border-radius:var(--digi--border-radius--input);--digi--form-select--select--padding:0 var(--digi--gutter--medium);--digi--form-select--select--padding--inline-end:var(--digi--padding--large);--digi--form-select--select--border-width--neutral:var(--digi--border-width--input-regular);--digi--form-select--select--border-width--success:var(--digi--border-width--input-validation);--digi--form-select--select--border-width--error:var(--digi--border-width--input-validation);--digi--form-select--select--border-width--warning:var(--digi--border-width--input-validation);--digi--form-select--select--width:100%;--digi--form-select--select--font-size:var(--digi--typography--body--font-size--desktop);--digi--form-select--select-option--font-size:var(--digi--typography--body--font-size--desktop);--digi--form-select--label--margin-bottom:var(--digi--gutter--medium);--digi--form-select--icon--inline-end:0.75rem;--digi--form-select--icon--top:50%;--digi--form-select--icon--transform:translateY(-50%);--digi--form-select--icon--color:var(--digi--color--icons--primary);--digi--form-select--footer--margin-top:var(--digi--gutter--medium)}.sc-digi-form-select-h .digi-form-select.sc-digi-form-select{display:flex;flex-direction:column;gap:0.4em}.sc-digi-form-select-h .digi-form-select--neutral.sc-digi-form-select{--BORDER-WIDTH:var(--digi--form-select--select--border-width--neutral);--BORDER-COLOR:var(--digi--form-select--border-color--neutral);--BACKGROUND:var(--digi--form-select--background--neutral)}.sc-digi-form-select-h .digi-form-select--success.sc-digi-form-select{--BORDER-WIDTH:var(--digi--form-select--select--border-width--success);--BORDER-COLOR:var(--digi--form-select--border-color--success);--BACKGROUND:var(--digi--form-select--background--success)}.sc-digi-form-select-h .digi-form-select--warning.sc-digi-form-select{--BORDER-WIDTH:var(--digi--form-select--select--border-width--warning);--BORDER-COLOR:var(--digi--form-select--border-color--warning);--BACKGROUND:var(--digi--form-select--background--warning)}.sc-digi-form-select-h .digi-form-select--error.sc-digi-form-select{--BORDER-WIDTH:var(--digi--form-select--select--border-width--error);--BORDER-COLOR:var(--digi--form-select--border-color--error);--BACKGROUND:var(--digi--form-select--background--error)}.sc-digi-form-select-h .digi-form-select--small.sc-digi-form-select{--HEIGHT:var(--digi--form-select--select--height--small)}.sc-digi-form-select-h .digi-form-select--medium.sc-digi-form-select{--HEIGHT:var(--digi--form-select--select--height--medium)}.sc-digi-form-select-h .digi-form-select--large.sc-digi-form-select{--HEIGHT:var(--digi--form-select--select--height--large)}.sc-digi-form-select-h .digi-form-select--empty.sc-digi-form-select:not(:focus-within){--BACKGROUND:var(--digi--form-select--background--empty)}.sc-digi-form-select-h .digi-form-select__select.sc-digi-form-select{-webkit-appearance:none;-moz-appearance:none;appearance:none;width:var(--digi--form-select--select--width);padding:var(--digi--form-select--select--padding);-webkit-padding-end:var(--digi--form-select--select--padding--inline-end);padding-inline-end:var(--digi--form-select--select--padding--inline-end);height:var(--HEIGHT);border:var(--BORDER-WIDTH) solid;border-radius:var(--digi--form-select--select--border-radius);border-color:var(--BORDER-COLOR);cursor:pointer;font-size:var(--digi--form-select--select--font-size);color:var(--digi--color--text--primary);background:var(--BACKGROUND);font-family:var(--digi--global--typography--font-family--default)}.sc-digi-form-select-h .digi-form-select__select-wrapper.sc-digi-form-select{position:var(--digi--form-select--select-wrapper--position)}.sc-digi-form-select-h .digi-form-select__select-option.sc-digi-form-select{font-size:var(--digi--form-select--select-option--font-size)}@media (min-width: 48rem){.sc-digi-form-select-h .digi-form-select__select-option.sc-digi-form-select{--digi--form-select--select-option--font-size:var(--digi--typography--font-size--l)}}.sc-digi-form-select-h .digi-form-select__select.sc-digi-form-select::-ms-expand{display:none}.sc-digi-form-select-h .digi-form-select__select.sc-digi-form-select:focus-visible{box-shadow:0 0 0.1rem 0.05rem var(--digi--color--border--informative);box-shadow:var(--digi--focus-shadow);outline:var(--digi--focus-outline)}.sc-digi-form-select-h .digi-form-select__icon.sc-digi-form-select{position:absolute;inset-inline-end:var(--digi--form-select--icon--inline-end);inset-block-start:var(--digi--form-select--icon--top);transform:var(--digi--form-select--icon--transform);pointer-events:none;color:var(--digi--form-select--icon--color)}.digi-form-select--neutral.sc-digi-form-select .digi-form-select__footer.sc-digi-form-select{position:absolute}";

const FormSelect = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.afOnChange = createEvent(this, "afOnChange", 7);
    this.afOnFocus = createEvent(this, "afOnFocus", 7);
    this.afOnFocusout = createEvent(this, "afOnFocusout", 7);
    this.afOnBlur = createEvent(this, "afOnBlur", 7);
    this.afOnDirty = createEvent(this, "afOnDirty", 7);
    this.optionItems = [];
    this.dirty = false;
    this.touched = false;
    this.hasPlaceholder = false;
    this.afLabel = undefined;
    this.afRequired = undefined;
    this.afRequiredText = undefined;
    this.afPlaceholder = undefined;
    this.afAnnounceIfOptional = false;
    this.afAnnounceIfOptionalText = undefined;
    this.afId = randomIdGenerator('digi-form-select');
    this.afName = undefined;
    this.afDescription = undefined;
    this.value = undefined;
    this.afValue = undefined;
    this.afDisableValidation = undefined;
    this.afVariation = FormSelectVariation.MEDIUM;
    this.afValidation = FormSelectValidation.NEUTRAL;
    this.afValidationText = undefined;
    this.afStartSelected = undefined;
    this.afAutofocus = undefined;
  }
  onValueChanged(value) {
    this.afValue = value;
  }
  onAfValueChanged(value) {
    this.value = value;
  }
  getOptions() {
    this.checkIfPlaceholder();
    let options = this._observer.children;
    if (!options || options.length <= 0) {
      logger.warn(`The slot contains no option elements.`, this.hostElement);
      return;
    }
    this.optionItems = [...Array.from(options)]
      .filter((el) => el.tagName.toLowerCase() === 'option')
      .map((el) => {
      return {
        value: el['value'],
        text: el['text']
      };
    });
  }
  checkIfPlaceholder() {
    this.hasPlaceholder = !!this.afPlaceholder;
  }
  componentWillLoad() {
    this.afValue ? (this.value = this.afValue) : (this.afValue = this.value);
  }
  componentDidLoad() {
    this.getOptions();
    this.afStartSelected &&
      (this.afValue = this.optionItems[this.afStartSelected].value);
  }
  /**
   * Returnerar en referens till formulärkontrollelementet.
   * @en Returns a reference to the form control element.
   */
  async afMGetFormControlElement() {
    return this._select;
  }
  get cssModifiers() {
    return {
      'digi-form-select--small': this.afVariation === FormSelectVariation.SMALL,
      'digi-form-select--medium': this.afVariation === FormSelectVariation.MEDIUM,
      'digi-form-select--large': this.afVariation === FormSelectVariation.LARGE,
      'digi-form-select--neutral': this.afValidation === FormSelectValidation.NEUTRAL,
      'digi-form-select--success': this.afValidation === FormSelectValidation.SUCCESS,
      'digi-form-select--warning': this.afValidation === FormSelectValidation.WARNING,
      'digi-form-select--error': this.afValidation === FormSelectValidation.ERROR,
      'digi-form-select--empty': !this.afValidation || this.afValidation === FormSelectValidation.NEUTRAL
    };
  }
  changeHandler(e) {
    if (!this.dirty) {
      this.afOnDirty.emit(e);
      this.dirty = true;
    }
    this.value = this.afValue = e.target.value;
    this.afOnChange.emit(e);
  }
  focusHandler(e) {
    this.afOnFocus.emit(e);
  }
  focusoutHandler(e) {
    this.afOnFocusout.emit(e);
  }
  blurHandler(e) {
    this.afOnBlur.emit(e);
  }
  getSelectedItem(option) {
    return this.afValue == option.value;
  }
  showValidation() {
    return !this.afDisableValidation &&
      this.afValidation !== FormSelectValidation.NEUTRAL &&
      this.afValidationText
      ? true
      : false;
  }
  render() {
    return (h("div", { class: Object.assign({ 'digi-form-select': true }, this.cssModifiers) }, h("div", { class: "digi-form-select__label-group" }, h("digi-form-label", { afFor: this.afId, afLabel: this.afLabel, afId: `${this.afId}-label`, afDescription: this.afDescription, afRequired: this.afRequired, afAnnounceIfOptional: this.afAnnounceIfOptional, afRequiredText: this.afRequiredText, afAnnounceIfOptionalText: this.afAnnounceIfOptionalText })), h("div", { class: "digi-form-select__select-wrapper" }, h("select", { class: "digi-form-select__select", name: this.afName, id: this.afId, ref: (el) => (this._select = el), required: this.afRequired ? this.afRequired : null, onFocus: (e) => this.focusHandler(e), onFocusout: (e) => this.focusoutHandler(e), onBlur: (e) => this.blurHandler(e), onChange: (e) => this.changeHandler(e), autoFocus: this.afAutofocus ? this.afAutofocus : null }, this.hasPlaceholder && (h("option", { class: {
        'digi-form-select__select-option': true,
        'digi-form-select__select-option--initial-value': true
      }, disabled: true, selected: !this.afValue, value: "" }, this.afPlaceholder)), this.optionItems.map((option, index) => {
      return (h("option", { key: index, class: "digi-form-select__select-option", value: option.value, selected: this.getSelectedItem(option) }, option.text));
    })), h("digi-icon", { class: "digi-form-select__icon", slot: "icon", afName: `input-select-marker` })), h("digi-util-mutation-observer", { hidden: true, ref: (el) => (this._observer = el), onAfOnChange: () => this.getOptions() }, h("slot", null)), h("div", { class: "digi-form-select__footer" }, h("div", { "aria-atomic": "true", role: "alert", id: `${this.afId}--validation-message` }, this.showValidation() && (h("digi-form-validation-message", { "af-variation": this.afValidation }, this.afValidationText))))));
  }
  get hostElement() { return getElement(this); }
  static get watchers() { return {
    "value": ["onValueChanged"],
    "afValue": ["onAfValueChanged"]
  }; }
};
FormSelect.style = formSelectCss;

export { FormSelect as digi_form_select };
