var LayoutContainerVariation;
(function (LayoutContainerVariation) {
  LayoutContainerVariation["STATIC"] = "static";
  LayoutContainerVariation["FLUID"] = "fluid";
  LayoutContainerVariation["NONE"] = "none";
})(LayoutContainerVariation || (LayoutContainerVariation = {}));

export { LayoutContainerVariation as L };
