import { r as registerInstance, h } from './index-7f342a75.js';

const typographyPreambleCss = ".sc-digi-typography-preamble-h .digi-typography-preamble.sc-digi-typography-preamble{font-family:var(--digi--global--typography--font-family--default);font-size:var(--digi--typography--preamble--font-size--mobile);font-weight:var(--digi--typography--preamble--font-weight--desktop);line-height:var(--digi--typography--preamble--line-height--mobile);color:var(--digi--color--text--primary);max-width:var(--digi--paragraph-width--medium)}@media (min-width: 62rem){.sc-digi-typography-preamble-h .digi-typography-preamble.sc-digi-typography-preamble{font-size:var(--digi--typography--preamble--font-size--desktop);line-height:var(--digi--typography--preamble--line-height--desktop)}}";

const TypographyPreamble = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
  }
  render() {
    return (h("p", { class: "digi-typography-preamble" }, h("slot", null)));
  }
};
TypographyPreamble.style = typographyPreambleCss;

export { TypographyPreamble as digi_typography_preamble };
